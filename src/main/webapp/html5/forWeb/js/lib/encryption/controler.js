

/**
 * 接口对象加密2
 * @param successCallback
 * @param failCallback
 */

function encryptionH5(successCallback,failCallback,$http,method,url,obj){

    var TimesTamp =  new Date().getTime()+"";
    if(obj){
        obj.seqNo=TimesTamp;
    }
    var str=JSON.stringify(obj);
    var strMd5=method=='GET'?'&93b885adfe0da089cdf634904fd59f71':'&'+md5(str);
    var getObj="";
    if(method=='GET'){
        getObj=getUrlKeys(url.data)==''?'':getUrlKeys(url.data);
    }
    var hmac=method+'&'+url.action+'&'+TimesTamp+'&AccessKeyId=RVE0TTNKYVlpMEJI&SignatureMethod=HMAC-SHA1'+getObj+strMd5;

    var Signature;
    Signature= CryptoJS.HmacSHA1(hmac, "UVltSlA5MEJERU9hT0RXaFAxUGIxYU&").toString();
    // if(!sessionStorage.assesKey || sessionStorage.assesKey == "null"){
    //     querySecretKey($http).then(function(){
    //         Signature= CryptoJS.HmacSHA1(hmac, sessionStorage.assesKey+"&").toString();
    //     })
    // }else {
    //     Signature= CryptoJS.HmacSHA1(hmac, sessionStorage.assesKey+"&").toString();
    // }

    setTimeout(function(){
        $http.defaults.headers.put['Content-Type'] = 'application/json;charset=UTF-8';
        $http.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
        $http.defaults.transformRequest = [];
        if(method=="POST"){
            $http({
                method : method,
                url:url.data,
                data:str,
                headers:{
                    AccessKeyId:'RVE0TTNKYVlpMEJI',
                    Timestamp:TimesTamp,
                    Signature:Signature,
                    Token: getToken()
                }
            }).success(function(data, status, headers, config) {
                Biostime.common.hideLoading()
                successCallback(data, status, headers, config)
            }).error(function(data, status, headers, config) {
                Biostime.common.hideLoading()
                failCallback(data, status, headers, config)
            })
        }else {
            $http({
                method : method,
                url:url.data,
                headers:{
                    AccessKeyId:'RVE0TTNKYVlpMEJI',
                    Timestamp:TimesTamp,
                    Signature:Signature,
                    Token: getToken()
                }
            }).success(function(data, status, headers, config) {
                Biostime.common.hideLoading()
                successCallback(data, status, headers, config)
            }).error(function(data, status, headers, config) {
                Biostime.common.hideLoading()
                failCallback(data, status, headers, config)
            })
        }
    },500)

}

function FromEncryptionH5(successCallback,failCallback,$http,method,url,obj){
    var TimesTamp =  new Date().getTime()+"";
    if(obj){
        obj.seqNo=TimesTamp;
    }
    var strMd5='&93b885adfe0da089cdf634904fd59f71';
    var getObj="";
    if(method=='GET'){
        getObj=getUrlKeys(url.data)==''?'':getUrlKeys(url.data);
    }

    var hmac=method+'&'+url.action+'&'+TimesTamp+'&AccessKeyId=RVE0TTNKYVlpMEJI&SignatureMethod=HMAC-SHA1'+getObj+strMd5;
    var Signature
    Signature= CryptoJS.HmacSHA1(hmac, "UVltSlA5MEJERU9hT0RXaFAxUGIxYU&").toString();
    // if(!sessionStorage.assesKey || sessionStorage.assesKey == "null"){
    //     querySecretKey($http).then(function(){
    //         Signature= CryptoJS.HmacSHA1(hmac, sessionStorage.assesKey+"&").toString();
    //     })
    // }else {
    //     Signature= CryptoJS.HmacSHA1(hmac, sessionStorage.assesKey+"&").toString();
    // }
    console.log(hmac)
    setTimeout(function(){

        $http.defaults.transformRequest = [];
        $http({
            method : method,
            url:url.data,
            data:obj,
            transformRequest: angular.identity,
            headers:{
                AccessKeyId:'RVE0TTNKYVlpMEJI',
                Timestamp:TimesTamp,
                Signature:Signature,
                Token: getToken(),
                'Content-Type':undefined
            }
        }).success(function(data, status, headers, config) {
            Biostime.common.hideLoading()
            successCallback(data, status, headers, config)
        }).error(function(data, status, headers, config) {
            Biostime.common.hideLoading()
            failCallback(data, status, headers, config)
        })
    },500)
}

function querySecretKey($http){
    var TimesTamp = {
        "request": "RVE0TTNKYVlpMEJI",
        "seqNo": new Date().getTime(),
        "sourceSystem": "HYT",
        "version": "7.1.0"
    };
    var url=  BASICURL+"merchant-system/api/merchant/system/sign/querySecretKey";
    $http.defaults.headers.common='';
    $http.defaults.transformRequest = [
        function (data){
            return JSON.stringify(data)
        }
    ]
   return $http({
        method : "POST",
        url:url,
        data: TimesTamp,
        headers:{
            'Content-Type':'application/json;charset=UTF-8'
        }
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading()
       sessionStorage.assesKey=data.response
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading()

    })

}
function getUrlKeys(url) {
    /**获取全部参数并转为对象*/
    var reg_url = /^[^\?]+\?([\w\W]+)$/,
        reg_para = /([^&=]+)=([\w\W]*?)(&|$|#)/g,
        arr_url = reg_url.exec(url),
        obj = {};
    if (arr_url && arr_url[1]) {
        var str_para = arr_url[1], result;
        while ((result = reg_para.exec(str_para)) != null) {
            obj[result[1]] = result[2];
        }
    }
    /**排序的函数*/
    var newkey = Object.keys(obj).sort();
    /**先用Object内置类的keys方法获取要排序对象的属性名，再利用Array原型上的sort方法对获取的属性名进行排序，newkey是一个数组*/
    var newObj = {};/**创建一个新的对象，用于存放排好序的键值对*/
    for (var i = 0; i < newkey.length; i++) {
        /**遍历newkey数组,向新创建的对象中按照排好的顺序依次增加键值对*/
        newObj[newkey[i]] = obj[newkey[i]];
    }
    /**把对象写成参数&key=value*/
    var myURL='';
    angular.forEach(newObj,function(item,key){
        var link = '&'+ key + "=" + item;
        myURL += link;
    })
    return myURL;
}
function setObj(obj){
    var Timestamp = new Date().getTime()+"";
    var data={
            "seqNo": Timestamp,
            "sourceSystem" : "hyt",
            "version":"1",
            "token":getToken(),
            "request":obj
    }
     return data
}

function setParames(obj) {
    var query = '';
    var name, value, fullSubName, subName, subValue, innerObj, i;
    for (name in obj) {
        value = obj[name];
        if (value instanceof Array) {
            for (i = 0; i < value.length; ++i) {
                subValue = value[i];
                fullSubName = name + '[' + i + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query +=  encodeURIComponent(fullSubName) + '=' + encodeURIComponent(subValue) + '&';
            }
        } else if (value instanceof Object) {
            for (subName in value) {
                subValue = value[subName];
                fullSubName = name + '[' + subName + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query +=  encodeURIComponent(fullSubName) + '=' + encodeURIComponent(subValue)  + '&';
            }
        } else if (value !== undefined && value !== null) {
            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }
    }

    return query.length ? query.substr(0, query.length - 1) : query;
};



