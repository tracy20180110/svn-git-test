/**
 * 使用方法
 * html:
 * 页面底部引用：
 * <div id="selectfiles"></div>
 *<script src="../js/lib/OSS/plupload.full.min.js" type="text/javascript"></script>
 *<script src="../js/lib/OSS/oss.js" type="text/javascript"></script>
 * JS：
 * upLoadOnit(buttonName,num) num:第几个，buttonName：ID
 * $scope.toUploadPic=function(files,num){
 *      upLoadImgOSS($scope,$http,files,num)
 *  };
 *  删除队列是请同时删除uploader对应的队列 如： uploader.splice(index,1)
 * */

var  uploader=[];
function upLoadOnit(buttonName,num,size){
    uploader.push(new plupload.Uploader({
        browse_button :buttonName, //html  ID元素
        url : 'http://hhvideo-online.oss-cn-shanghai.aliyuncs.com/',
        multi_selection:false,
        filters: {
            mime_types: [ //只允许上传图片和zip文件
                {title: "Image files", extensions: "jpg,png,bmp,gif,webp,tiff"},
                /*{title: "Zip files", extensions: "zip,rar"},
                {title: "Apk files", extensions: "apk"}*/
            ],
            max_file_size: size, //最大只能上传10mb的文件
            //prevent_duplicates: true //不允许选取重复文件
          }
    })
    ) ;

    uploader[num].init();
    //绑定各种事件，并在事件监听函数中做你想做的事
    uploader[num].bind('FilesAdded',function(uploader,files){
        //每个事件监听函数都会传入一些很有用的参数，
        //我们可以利用这些参数提供的信息来做比如更新UI，提示上传进度等操作
                angular.element(document.getElementById('biostimeMKTCtrl')).scope().$$childTail.toUploadPic(files,num);
        // uploader.start();
    });
    uploader[num].bind('UploadProgress',function(uploader,file){
    });
    uploader[num].bind('FileUploaded',function(uploader,file,responseObject){
        if(responseObject.status==200){
            // alert("上传成功");
            //回调angular
            var scope=angular.element(document.getElementById('biostimeMKTCtrl')).scope().$$childTail;
            scope.uploaderCallBack(upLoadData);
            isLoading=false;
        }
    });
}


var isLoading=false,upLoadData=[];
function upLoadImgOSS($scope,$http,files,num,isMuti,type){//营销日历type:1
    console.log(files)
    if(isLoading&&!isMuti){
        Biostime.common.toastMessage("正在上传");
        return
    }
    isLoading=true;
    var queryData = {
        "imageUploadBusinessType": type,
        "fileName": files[0].name
    };
    $scope.OSSfiles=files[0];

    var url = {
        data:OSSURL+'merchant-system/api/mkt/system/oss/productOssSignature',
        action:"/merchant-system/api/mkt/system/oss/productOssSignature"
    };
    encryptionH5(function(data, status, headers, config){
        isLoading=false;
        if(data.code == 100){
            $scope.upLoadImgUrl=data.response.url;
            $scope.upLoadData=data.response;
            $scope.upLoadData.size=files[0].size;
            if(upLoadData.length!=0){
                for(var o=0;o<upLoadData.length;o++){
                    if(upLoadData[o].originalFileName==$scope.upLoadData.originalFileName){
                        upLoadData.splice(o,1)
                    }
                }
            }

            upLoadData.push($scope.upLoadData);
            new_multipart_params = {
                'Filename':$scope.upLoadData.fileName,//文件名称
                'key' : data.response.filePath+"/"+data.response.fileName, //保存路径
                'policy': data.response.policy,
                'OSSAccessKeyId': data.response.accessKeyId,
                'success_action_status' : '200', //让服务端返回200,不然，默认会返回204
                'signature': data.response.signature,
                'url':data.response.url
            };
            uploader[num].setOption({
                'url': data.response.uploadHost,
                'multipart_params': new_multipart_params
            });
            uploader[num].start();
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    },function(data, status){
        Biostime.common.toastMessage(data.desc);
        isLoading=false;
    },$http,"POST",url,setObj(queryData));
}


function importPointOSS($scope,$http,files,type){
    console.log(files);
    var TimesTamp =  new Date().getTime()+"";
    var url = {
        data:OSSURL+'merchant-system/api/mkt/system/oss/upload?'+$scope.token+'&imageUploadBusinessType='+type,
        action:"/merchant-system/api/mkt/system/oss/upload"
    };
    var fd = new FormData();
        fd.append('file',files[0]);
    var strMd5='&93b885adfe0da089cdf634904fd59f71';
    var getObj="";
    var hmac='POST&'+url.action+'&'+TimesTamp+'&AccessKeyId=RVE0TTNKYVlpMEJI&SignatureMethod=HMAC-SHA1'+getObj+strMd5;

    var Signature;
    Signature= CryptoJS.HmacSHA1(hmac, "UVltSlA5MEJERU9hT0RXaFAxUGIxYU&").toString();
    Biostime.common.showLoading();
    $http({
        method : "POST",
        url:url.data,
        data:fd,
        transformRequest: angular.identity,
        headers:{
            AccessKeyId:'RVE0TTNKYVlpMEJI',
            Timestamp:TimesTamp,
            Signature:Signature,
            'Content-Type': undefined
        }
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        isLoading=false;
        imgCallBack($scope,$http,data.response)
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading()

    })


}