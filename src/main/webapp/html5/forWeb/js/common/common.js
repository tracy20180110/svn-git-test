/**
 * 基础路径
 */

/*var BASICURL = location.href.indexOf('http://biostime-mkt.biostime.com/')>=0?"http://biostime-mkt.biostime.com/":"http://10.50.115.18:2023/";  //测试：10.50.115.18:2023
var useId =location.href.indexOf('http://biostime-mkt.biostime.com/')>=0? "http://192.168.234.2:2013/":"http://10.50.115.18:2010/"; //大区办事处公用接口 测试:10.50.115.18;生产:192.168.234.2 
var eventpvID =location.href.indexOf('http://biostime-mkt.biostime.com/')>=0?"http://pv.mama100.com/":'http://interface.mama100.cn/';	//埋点服务器
var SHAREURL = location.href.indexOf('http://biostime-mkt.biostime.com/')>=0?"http://www.mama100.com/":"http://weixin.mama100.cn/"; //ftf活动配置生成可供前端分享的海报
var YOUPAIYUN = location.href.indexOf('http://biostime-mkt.biostime.com/')>=0?"http://h.mama100.com/":"http://test-01.biostime.us/";//又拍云上传图片,获取签名接口地址
var OSSURL = location.href.indexOf('http://biostime-mkt.biostime.com/')>=0?"http://h.mama100.com/":"http://test-01.biostime.us/";
var addressURL = location.href.indexOf('http://biostime-mkt.biostime.com/')>=0?"https://biostime-mkt.biostime.com/biostime-terminal-gateway/":"http://10.50.115.4:9129/"; //根据SA编号获取地址
// var menuUrl = location.href.indexOf('http://mkt.biostime.com/')>=0?"http://mkt.biostime.com/":"http://test-01.biostime.us/";//获取用户当前页面下有效的菜单或按钮
*/
var BASICURL,useId,eventpvID,SHAREURL,YOUPAIYUN,OSSURL,addressURL;
//域名
var IP_CONSTANT={
    formal:"http://biostime-mkt.biostime.com/",         //生产http
    formalS:"https://biostime-mkt.biostime.com/",       //生产https
    test:"http://test-01.biostime.us/",     //测试http
    testS:"https://test-01.biostime.us/"    //测试https
};
//后台接口地址
var IP_INTERFACE={
    formal:"http://biostime-mkt.biostime.com/",
    formalS:"https://biostime-mkt.biostime.com/",       //生产https

    pv:"http://pv.mama100.com/",       //埋点生产http
    pvS:"https://pv.mama100.com/",       //埋点生产https
    // pvTest:"http://interface.mama100.cn/",       //埋点测试http
    pvTest:"http://interface.mama100.cn/",       //埋点测试http
    // pvTestS:"https://interface.mama100.cn/",       //埋点测试https
    pvTestS:"https://interface.mama100.cn/",       //埋点测试https

    share:"http://www.mama100.com/",				//ftf活动配置生成可供前端分享的海报
    shareS:"https://www.mama100.com/",	
    shareTest:"http://weixin.mama100.cn/",				
    shareTestS:"https://weixin.mama100.cn/",	

    hmm100:"http://h.mama100.com/",				
    hmm100S:"https://h.mama100.com/",		

    address:"http://biostime-mkt.biostime.com/biostime-terminal-gateway/",		//根据SA编号获取地址
    addressS:"https://biostime-mkt.biostime.com/biostime-terminal-gateway/",				

    test:"http://test-01.biostime.us/",                                               //测试http
    testS:"https://test-01.biostime.us/",                                               //测试https
};
//公共接口--大区办事处
var IP_COMMON = {
    formal:"http://biostime-mkt.biostime.com/",         //生产http
    formalS:"https://biostime-mkt.biostime.com/",       //生产https
    test:"http://test-01.biostime.us/",                 //测试http
    testS:"https://test-01.biostime.us/"                //测试https
};

if(location.href.indexOf(IP_CONSTANT.formal)>=0){
    //生产http
    BASICURL = IP_INTERFACE.formal;
    useId = IP_COMMON.formal;
    eventpvID = IP_INTERFACE.pv;
    SHAREURL = IP_INTERFACE.share;
    YOUPAIYUN = IP_INTERFACE.hmm100;
    OSSURL = IP_INTERFACE.hmm100;
    addressURL = IP_INTERFACE.address;
}else if(location.href.indexOf(IP_CONSTANT.formalS)>=0) {
    //生产https
    BASICURL = IP_INTERFACE.formalS;
    useId = IP_COMMON.formalS;
    eventpvID = IP_INTERFACE.pvS;
    SHAREURL = IP_INTERFACE.shareS;
    YOUPAIYUN = IP_INTERFACE.hmm100S;
    OSSURL = IP_INTERFACE.hmm100S;
    addressURL = IP_INTERFACE.addressS;
}else if(location.href.indexOf(IP_CONSTANT.test)>=0) {
    //测试http
    BASICURL = IP_INTERFACE.test;
    useId = IP_COMMON.test;
    eventpvID = IP_INTERFACE.pvTest;
    SHAREURL = IP_INTERFACE.shareTest;
    YOUPAIYUN = IP_INTERFACE.test;
    OSSURL = IP_INTERFACE.test;
    addressURL = IP_INTERFACE.test;
}else if(location.href.indexOf(IP_CONSTANT.testS)>=0) {
    //测试https
    BASICURL = IP_INTERFACE.testS;
    useId = IP_COMMON.testS;
    eventpvID = IP_INTERFACE.pvTestS;
    SHAREURL = IP_INTERFACE.shareTestS;
    YOUPAIYUN = IP_INTERFACE.testS;
    OSSURL = IP_INTERFACE.testS;
    addressURL = IP_INTERFACE.testS;
}else{
    BASICURL = IP_INTERFACE.test;
    useId = IP_COMMON.test;
    eventpvID = IP_INTERFACE.pvTest;
    SHAREURL = IP_INTERFACE.shareTest;
    YOUPAIYUN = IP_INTERFACE.test;
    OSSURL = IP_INTERFACE.test;
    addressURL = IP_INTERFACE.test;
};

/**
 * 获取当前路径
 * @returns
 */

function getRootPath(){
	var strFullPath=window.document.location.href;
	var strPath=window.document.location.pathname;
	var pos=strFullPath.indexOf(strPath);
	var prePath=strFullPath.substring(0,pos);
	var postPath=strPath.substring(0,strPath.substr(1).indexOf('/')+1);
	return(prePath+postPath);
}

/**
 * 方法:Array.remove(dx) 功能:根据元素位置值删除数组元素. 参数:元素值 返回:在原数组上修改数组
 */
Array.prototype.remove = function(dx) {
	if (isNaN(dx) || dx > this.length) {
		return false;
	}
	for ( var i = 0, n = 0; i < this.length; i++) {
		if (this[i] != this[dx]) {
			this[n++] = this[i];
		}
	}
	this.length -= 1;
};

/**
 * 用来处理时间戳
 */
Date.prototype.format = function(format) {
	var o = {
		"M+" : this.getMonth() + 1, // month
		"d+" : this.getDate(), // day
		"h+" : this.getHours(), // hour
		"m+" : this.getMinutes(), // minute
		"s+" : this.getSeconds(), // second
		"q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
		"S" : this.getMilliseconds()
		// millisecond
	};
	if (/(y+)/.test(format))
		format = format.replace(RegExp.$1, (this.getFullYear() + "")
			.substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(format))
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
				: ("00" + o[k]).substr(("" + o[k]).length));
	return format;
};


function getType(o)
{
	var _t;
	return ((_t = typeof(o)) == "object" ? o==null && "null" || Object.prototype.toString.call(o).slice(8,-1):_t).toLowerCase();
}

/**
 * 多维数组深克隆
 * @param destination 目标数组
 * @param source 原数组
 * @returns {Array} 克隆后的新数组
 */
function arrayDeepcopy(destination,source)
{
	for(var p in source)
	{
		if(getType(source[p])=="array"||getType(source[p])=="object")
		{
			destination[p]=getType(source[p])=="array"?[]:{};
			arguments.callee(destination[p],source[p]);
		}
		else
		{
			destination[p]=source[p];
		}
	}
}

/**
 * 打印控制台日志
 * @param obj 打印信息
 */
function consolePrint(obj) {
	if (window.console) {
		window.console.log(obj);
	}
}
/**
 * 判断数组是否包含某参数
 * @param array 目标数组
 * @param param 目标参数
 */
function contains(array,param){
	for(var i=0;i<array.length;i++) {
		if(array[i] == param)
			return true;
	}
	return false;
}

/**
 * 表格样式
 */
//function altRows(id){
//    if(document.getElementsByTagName){
//        var table = document.getElementById('alternatecolor');
//        var rows = table.getElementsByTagName("tr");
//        var li = table.getElementsByTagName("li");
//        for(i = 0; i < rows.length; i++){
//            if(i % 2 == 0){
//                rows[i].className = "evenRowcolor";
//            }else{
//                rows[i].className = "oddRowcolor";
//            }
//        }
//        for(i = 0; i < li.length; i++){
//            if(i % 2 == 0){
//                li[i].className = "oddRowcolor";
//            }else{
//                li[i].className = "evenRowcolor";
//            }
//        }
//    }
//}
//window.onload=function(){
//    altRows('alternatecolor');
//}
/**
 * 检测ie版本
 */

function getToken(){
   var token= Biostime.common.getQueryString('token');
    if(token){
        sessionStorage.token=token
    }else {
        token=sessionStorage.token
    }
    return token
}

function ieCheck(){
	var userAgent = navigator.userAgent,
		rMsie = /(msie\s|trident.*rv:)([\w.]+)/,
		rFirefox = /(firefox)\/([\w.]+)/,
		rOpera = /(opera).+version\/([\w.]+)/,
		rChrome = /(chrome)\/([\w.]+)/,
		rSafari = /version\/([\w.]+).*(safari)/;
	var browser;
	var version;
	var ua = userAgent.toLowerCase();
	function uaMatch(ua){
		var match = rMsie.exec(ua);
		if(match != null){
			return { browser : "IE", version : match[2] || "0" };
		}
		var match = rFirefox.exec(ua);
		if (match != null) {
			return { browser : match[1] || "", version : match[2] || "0" };
		}
		var match = rOpera.exec(ua);
		if (match != null) {
			return { browser : match[1] || "", version : match[2] || "0" };
		}
		var match = rChrome.exec(ua);
		if (match != null) {
			return { browser : match[1] || "", version : match[2] || "0" };
		}
		var match = rSafari.exec(ua);
		if (match != null) {
			return { browser : match[2] || "", version : match[1] || "0" };
		}
		if (match != null) {
			return { browser : "", version : "0" };
		}
	}
	var browserMatch = uaMatch(userAgent.toLowerCase());
	if (browserMatch.browser){
		browser = browserMatch.browser;
		version = browserMatch.version;
	}
// document.write(browser+version);
// alert(version)
	var ieEdition = rMsie && version;
	if(ieEdition < 11){
		alert("亲，为了您有更好的用户体验，建议使用IE11或以上版本的浏览器哦");
		window.open('ieTips.html')
	}
}
ieCheck();


/**
 * 异常监控
 */
function setTracker() {
    if( document.body) {
        //新建一个div元素节点
        var div=document.createElement("script");
        div.src=OSSURL+"merchant_h5/js/lib/tracker/tracker.js?system=MKT";
//把div元素节点添加到body元素节点中成为其子节点，但是放在body的现有子节点的最后
        document.body.appendChild(div);
//插入到最前面
        document.body.insertBefore(div, document.body.firstElementChild);
    }else {
        setTimeout(function (){setTracker()},1000)

    }
}
setTracker();

// document.write("<script type=\"text/javascript\" src=\""+OSSURL+"merchant_h5/js/lib/tracker/tracker.js?system=MKT\" crossorigin></script>");