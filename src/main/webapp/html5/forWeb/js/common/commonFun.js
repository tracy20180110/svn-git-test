/**
 * 记录公用方法
 */

/**
 * 埋点
 * @param $scope
 * @param $http
 * @param data
 */
function eventpv($scope, $http,data){
    var queryData = data;
    var url = eventpvID+"statistics/api/add"+"?token="+getToken();
    $http({
        method: 'POST',
        url: url,
        data:queryData,
        headers: {
            'Content-Type': 'application/json'
        }
    }).success(function(data, status, headers, config){

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
    });
};

