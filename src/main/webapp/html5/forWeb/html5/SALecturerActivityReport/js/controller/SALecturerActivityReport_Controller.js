/*******************************************************************************
 * @jsname:SALecturerActivityReport_Controller.js
 * @author:LonJintao
 * @date:2019-1-9
 * @use:SA讲师活动报表.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    //获取token
    if(Biostime.common.getQueryString('token')){
        $scope.token = "token="+Biostime.common.getQueryString('token')
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid')
    }

    //初始化
    $scope.lecturerName = '';

    $scope.lecturerTypeList = [
        {'name':'全部','value':''},
        {'name':'内部讲师','value':'0'},
        {'name':'外部专家','value':'1'}
    ];
    $scope.lecturerType = $scope.lecturerTypeList[0];

    //导出文件
    $scope.importFile = function() {
        if($scope.user.buCode == '10'){
            Biostime.common.toastMessage('该功能只用于BNC事业部');
            return
        }
        $scope.startDate = document.getElementById('startTime').value;
        $scope.endDate = document.getElementById('endTime').value;
        // var areaCode = $scope.area.areaCode?$scope.area.areaCode:'';
        // var officeCode = $scope.department.officeCode?$scope.department.officeCode:'';
        var areaOfficeCode = $scope.department.officeCode?$scope.department.officeCode:$scope.area.areaCode;
        if(Biostime.common.isEmpty($scope.infoList)){
            Biostime.common.toastMessage("查询列表不能为空！请先操作查询。");
        }else{
            window.location.href = BASICURL + "samanage/sa/lecturer/exportSaLecturerActList.action?areaOfficeCode="
            +areaOfficeCode + "&buCode="+$scope.buId.buCode
            +"&name="+$scope.lecturerName
            +"&type="+$scope.lecturerType.value
            +"&startDate="+$scope.startDate
            +"&endDate="+$scope.endDate
            +'&token='+Biostime.common.getQueryString('token');
        }

    };

    //线下活动导出
    $scope.offLineImport = function(item){
        window.location.href = BASICURL + "samanage/sa/lecturer/exportSaLecturerActDownList.action?lecturerId="
        +item.lecturerId
        +"&startDate="+item.startDate
        +"&endDate="+item.endDate
        +'&token='+Biostime.common.getQueryString('token');
    };
    //线上活动导出
    $scope.onLineImport = function(item){

    };

    //根据身份显示相应的大区和办事处、渠道
    identityVerification($scope, $http).then(
        function(){
            getSubordinateMenus($scope, $http);
        }
    );

    //事业部修改，大区跟着改变
    $scope.buChange = function(){
        buChange($scope);
    };

    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments($scope, $http);
    };

    //查询
    $scope.searchClick = function() {
        loadInfoList($scope, $http, 1);
    };
    // 分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadInfoList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadInfoList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 4;
        if ($scope.pagination.pageCount < 4) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
});

//根据身份，显示相应的大区和办事处、渠道
function identityVerification($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.user=data.loadUserBaseInfoBean;

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };
        
        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}

/**
 * 查询
 */
function loadInfoList ($scope, $http, pageNum) {
    if($scope.user.buCode == '10'){
        Biostime.common.toastMessage('该功能只用于BNC事业部');
        return
    }
    $scope.startTime = document.getElementById('startTime').value;
    $scope.endTime = document.getElementById('endTime').value;

    if(Biostime.common.isEmpty($scope.startTime) || Biostime.common.isEmpty($scope.endTime)){
        Biostime.common.toastMessage("活动起止日期不能为空！");
        return
    }

    $scope.pageNum = pageNum;
    Biostime.common.showLoading();
    var queryData = {
        "request":{
            "pageSize":20,
            "pageNo":pageNum,
            'buCode': $scope.buId.buCode,
            "areaOfficeCode": $scope.department.officeCode?$scope.department.officeCode:$scope.area.areaCode,
            "type": $scope.lecturerType.value,
            "name": $scope.lecturerName,
            "startDate":$scope.startTime||"",  // 开始时间
            "endDate":$scope.endTime||"",  // 结束时间
        }
    };
    // var url = "http://10.50.101.162:8080/samanage/sa/lecturer/querySaLecturerActList.do";
    var url = BASICURL + "samanage/sa/lecturer/querySaLecturerActList.action" + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        $scope.infoList = [];
        if(data.code == 100){
            if (Biostime.common.isEmpty(data.response.datas)) {
                Biostime.common.toastMessage("查无数据");
            }else{
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};

/**
 * 获取用户当前页面下有效的菜单或按钮
 */
function getSubordinateMenus($scope, $http) {
    $scope.token = Biostime.common.getQueryString("token");
    var urlPath = getCurrentUrlPath();
    var queryData = {
        "seqNo": new Date().getTime(),
        "sourceSystem": "merchant",
        "request": {
            "userId": $scope.user.userId,  // 必要参数,但可以值为空
            "token": $scope.token, // 必要参数
            "url": urlPath // 必要参数
        }
    };
    var url = useId + 'mkt-common/baseInfo/getSubordinateMenus.action';
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            $scope.buttonPermissionList = data.response;
            angular.forEach($scope.buttonPermissionList,function(item){
                if(item.code == 'daochu') {
                    $scope.daochu = true;
                };
            });
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};

/* 获取当前路径*/
function getCurrentUrlPath(){
    var currentUrl = document.location.toString();
    var index = currentUrl.lastIndexOf("?");
    return currentUrl.substring(0, index);
}