/*******************************************************************************
 * @jsname:index_controller.js
 * @author:wunan
 * @date:2017-02-28
 * @use:SA与门店管理.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);
var fd;
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    if(Biostime.common.getQueryString('token')) {
        $scope.token = "token=" + Biostime.common.getQueryString('token');
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid');
    };

    //初始化
    $scope.saCode = '';
    $scope.terminalCode = '';
    $scope.relation = '';
    $scope.isSubmit = true;

    //根据身份显示相应的大区和办事处、渠道
    identityVerification2($scope, $http);

    //事业部修改，大区跟着改变
    $scope.buChange = function(){
        buChange($scope);
    };

    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments2($scope, $http);
    };

    $scope.tabFun = function(sel) {
        $scope.sel = sel;
        if(sel == '2'){
            window.location.href="saTerminalManageAdd.html?"+$scope.token;
        }
    };

    $scope.search = function() {
        querySaTerminalManageList($scope, $http, 1);
    };
    //导出
    $scope.exportFn = function() {
        if($scope.user.buCode == '10'){
            Biostime.common.toastMessage('该功能只用于BNC事业部！');
            return
        }
        if(Biostime.common.isEmpty($scope.infoList)){
            Biostime.common.toastMessage('查询结果为空!请先操作查询');
            return
        }
        $scope.areaOfficeCode = $scope.department.officeCode!=''?$scope.department.officeCode:$scope.area.areaCode!=''?$scope.area.areaCode:'';
        var relation = $scope.relation?$scope.relation:'';
        window.location.href = BASICURL + "samanage/sa/terminal/new/exportSaTerminalManageNewList.action?"
        +'saCode='+$scope.saCode+'&terminalCode='+$scope.terminalCode + "&buCode="+$scope.buId.buCode+'&areaOfficeCode='+$scope.areaOfficeCode+'&statusCode='+relation + '&' + $scope.token;
        // window.location.href = "http://10.50.101.162:8080/" + "samanage/sa/terminal/new/exportSaTerminalManageNewList.action?"
        // +'saCode='+$scope.saCode+'&terminalCode='+$scope.terminalCode+'&areaOfficeCode='+$scope.areaOfficeCode+'&statusCode='+relation + '&' + $scope.token;
    };

    //分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        querySaTerminalManageList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        querySaTerminalManageList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        querySaTerminalManageList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            querySaTerminalManageList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        querySaTerminalManageList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 3;
        if ($scope.pagination.pageCount < 3) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
    //关闭
    $scope.close = function() {
        easyDialog.close();
        $scope.currentItem = {};
    };
    //操作
    $scope.easyDialogFun = function(tableItem) {
        $scope.currentItem = angular.copy(tableItem);
        easyDialog.open({
            container: 'changeId'
        });

        document.getElementById('begainDate').value = $scope.currentItem.startDate;
        document.getElementById('closeDate').value = $scope.currentItem.endDate;
       
    };

    //操作-确定
    $scope.sureFun = function() {
        saveSaTerminalRelation($scope, $http);
    };
});
/**
 * 查询接口
 */
function querySaTerminalManageList($scope, $http, pageNum) {
    if($scope.user.buCode == '10'){
        Biostime.common.toastMessage('该功能只用于BNC事业部！');
        return
    }
    $scope.areaOfficeCode = $scope.department.officeCode!=''?$scope.department.officeCode:$scope.area.areaCode!=''?$scope.area.areaCode:'';
    Biostime.common.showLoading();
    var queryData = {'request':{
        'buCode': $scope.buId.buCode,
        'areaOfficeCode':$scope.areaOfficeCode,//大区办事处
        'saCode':$scope.saCode,//SA编号
        'terminalCode':$scope.terminalCode,//关联门店
        'statusCode':$scope.relation,//有效关系(新  1未生效、2:有效、3失效)
        'pageNo': pageNum,
        'pageSize': 20
    }};
    // var url = "http://10.50.101.162:8080/" + 'samanage/sa/terminal/new/querySaTerminalManageNewList.action' + '?' + $scope.token;
    var url = BASICURL + 'samanage/sa/terminal/new/querySaTerminalManageNewList.action' + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
                $scope.pagination.currentPage = 1;
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};

/**
 * 编辑
 */
function saveSaTerminalRelation($scope, $http) {
    $scope.isSubmit = false;

    var bd = document.getElementById('begainDate').value;
    if(Biostime.common.isEmpty(bd)){
        $scope.isSubmit = true;
        Biostime.common.toastMessage('开始时间不能为空！');
        return
    };
    var cd = document.getElementById('closeDate').value;

    Biostime.common.showLoading();
    var queryData = {'request':{
        'id':$scope.currentItem.id,
        // 'areaOfficeCode':$scope.currentItem.areaOfficeCode,
        // 'saCode':$scope.currentItem.saCode,//SA编号
        // 'terminalCode':$scope.currentItem.terminalCode,//门店编码
        'createdBy':$scope.userId,//登陆人账号
        'createdName':$scope.userName,//登陆人名称
        'startDate':bd,
        'endDate':cd || ''
    }};
    // var url = "http://10.50.101.162:8080/" + 'samanage/sa/terminal/new/saveSaTerminalRelationNew.action' + '?' + $scope.token;
    var url = BASICURL + 'samanage/sa/terminal/new/saveSaTerminalRelationNew.action' + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            Biostime.common.toastMessage('更改成功');
            setTimeout(function(){
                easyDialog.close();
                $scope.isSubmit = true;
            },2000);
            querySaTerminalManageList($scope, $http, 1);
        }else{
            $scope.isSubmit = true;
            Biostime.common.toastMessage('修改失败，请联系产品运营同事！');
        };
    }).error(function(data, status, headers, config) {
        $scope.isSubmit = true;
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};

//根据身份，显示相应的大区和办事处、渠道、登录人id/姓名
function identityVerification2($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;
        $scope.user = data.loadUserBaseInfoBean;           //获取登录人id
        $scope.userId = data.loadUserBaseInfoBean.userId;           //获取登录人id
        $scope.userName = data.loadUserBaseInfoBean.userName;       //获取登录人姓名

        $scope.channel = $scope.channelList[0];
        for(var i=0; i<$scope.channelList.length; i++){
            if($scope.channelList[i].channelCode == "01"){
                $scope.channel = $scope.channelList[i];
                break;
            }
        }

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
};

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 * */
function getDepartments2($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
};