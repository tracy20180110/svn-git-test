/*******************************************************************************
 * @jsname:saTerminalImport_controller.js
 * @author:wunan
 * @date:2017-02-28
 * @use:SA与门店关系.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);
var fd;
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    if(Biostime.common.getQueryString('token')) {
        $scope.token = "token=" + Biostime.common.getQueryString('token');
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid');
    };
    document.getElementById('startDate').value = new Date().format('yyyy-MM-dd');
    document.getElementById('endDate').value = new Date().format('yyyy-MM-dd');
    $scope.uploadFileDisabled = false;
    $scope.selFun = function(sel) {
        $scope.sel = sel;
        document.getElementById('startDate').value = new Date().format('yyyy-MM-dd');
        document.getElementById('endDate').value = new Date().format('yyyy-MM-dd');
        if(sel == 1){
            querySaTerminalManageImportList($scope, $http, 1);
        };
        var file = document.getElementById('fileId');
        //清除上传文件
        if(file.outerHTML){
            file.outerHTML = file.outerHTML;
        }else{
            file.value="";
        };
        $scope.fileName = '';
        $scope.errorData = [];
    };
    $scope.cancel = function() {
        var file = document.getElementById('fileId');
        //清除上传文件
        if(file.outerHTML){
            file.outerHTML = file.outerHTML;
        }else{
            file.value="";
        };
        $scope.fileName = '';
        $scope.errorData = [];
    };
    //根据身份显示相应的大区和办事处、渠道
    identityVerification2($scope, $http);
    querySaTerminalManageImportList($scope, $http, 1);
    $scope.search = function() {
        querySaTerminalManageImportList($scope, $http, 1);
    };
    //导入文件
    $scope.uploadFile = function (files) {
        $scope.uploadFileDisabled = true;
        //登陆人所属大区办事处编号（如果办事处为空就拿大区）
        $scope.areaOfficeCode = $scope.departments[0].officeCode!=''?$scope.departments[0].officeCode:$scope.areas[0].areaCode!=''?$scope.areas[0].areaCode:'';
        var url = BASICURL + "samanage/sa/terminal/importSaTerminalManage.action";
//        var url = 'http://10.50.101.127:8080/' + "samanage/sa/terminal/importSaTerminalManage.do";
        fd = new FormData();
        var fileId = document.getElementById('fileId');
        if($scope.fileName == '') {
            Biostime.common.toastMessage('请导入数据！');
            return;
        };
        fd.append('file', fileId.files[0]);
        fd.append('createdBy', $scope.userId);
        fd.append('createdName', $scope.userName);
        fd.append('areaOfficeCode', $scope.areaOfficeCode);
        Biostime.common.showLoading();
        $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            Biostime.common.hideLoading();
            $scope.uploadFileDisabled = false;
            if(data.code != 100) {
                $scope.errorData = [];
                if(data.response==''||data.response==null) {
                    Biostime.common.toastMessage(data.desc);
                }else{
                    $scope.data = angular.copy(data.response);
                    angular.forEach($scope.data,function(item){
                        var obj = {
                            'description':item
                        };
                        $scope.errorData.push(obj);
                    });
                }
            }else{
                alert(JSON.stringify('文件导入成功：'+fileId.files[0].name));
                $scope.errorData = [];
            }
        }).error(function (data) {
            $scope.uploadFileDisabled = false;
            Biostime.common.hideLoading();
            Biostime.common.toastMessage('服务器错误，文件导入失败！');
        });
    };
    //导出
    $scope.export = function() {
        $scope.startDate = document.getElementById('startDate').value;
        $scope.endDate = document.getElementById('endDate').value;
        window.location.href = BASICURL + "samanage/sa/terminal/exportSaTerminalManageImportList.action?startDate="+$scope.startDate+'&endDate='+$scope.endDate + '&' + $scope.token;
    };
    //下载导入的文件
    $scope.downLoadImportFile = function(tableItem) {
        window.location.href =  tableItem.filePath + '?' + $scope.token;
    };
    //分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        querySaTerminalManageImportList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        querySaTerminalManageImportList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        querySaTerminalManageImportList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            querySaTerminalManageImportList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        querySaTerminalManageImportList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 3;
        if ($scope.pagination.pageCount < 3) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
    //获取上传文件的名称第二步
    $scope.toImport = function(file){
        $scope.orderFile = file;
        $scope.$apply(function(){
            $scope.fileName =$scope.orderFile.name;
        });
    };
    //关闭
    $scope.close = function() {
        easyDialog.close();
    };
});
/**
 * 获取上传文件的名称第一步
 */
function fileChange(target){
    var file =target.files[0];
    if (file) {
        angular.element(document.getElementById('biostimeMKTCtrl')).scope().toImport(file);
    } else {

    }
}
/**
 * 查询接口
 */
function querySaTerminalManageImportList($scope, $http, pageNum) {
    $scope.startDate = document.getElementById('startDate').value;
    $scope.endDate = document.getElementById('endDate').value;
    Biostime.common.showLoading();
    var queryData = {'request':{
        'startDate':$scope.startDate,
        'endDate':$scope.endDate,
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + 'samanage/sa/terminal/querySaTerminalManageImportList.action' + '?' + $scope.token;
//    var url = 'http://10.50.101.127:8080/samanage/sa/terminal/querySaTerminalManageImportList.do';
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
                $scope.pagination.currentPage = 1;
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};
//根据身份，显示相应的大区和办事处、渠道、登录人id/姓名
function identityVerification2($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.areas=data.loadUserAreaBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;
        $scope.userId = data.loadUserBaseInfoBean.userId;           //获取登录人id
        $scope.userName = data.loadUserBaseInfoBean.userName;       //获取登录人姓名

        $scope.channel = $scope.channelList[0];
        for(var i=0; i<$scope.channelList.length; i++){
            if($scope.channelList[i].channelCode == "01"){
                $scope.channel = $scope.channelList[i];
                break;
            }
        }

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areas.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        $scope.area=$scope.areas[0];

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
};