/*******************************************************************************
 * @jsname:index_controller.js
 * @author:wunan
 * @date:2017-02-28
 * @use:SA与门店管理.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);
var fd;
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    if(Biostime.common.getQueryString('token')) {
        $scope.token = "token=" + Biostime.common.getQueryString('token');
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid');
    };
    //初始化
    $scope.type = '';
    $scope.saCode = '';
    $scope.terminalCode = '';
    $scope.relationTerminalCode = '';
    $scope.noChangeTime = false;
    $scope.isSubmit = true;
    //根据身份显示相应的大区和办事处、渠道
    identityVerification2($scope, $http);

    //事业部修改，大区跟着改变
    $scope.buChange = function(){
        buChange($scope);
    };

    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments2($scope, $http);
    };
    $scope.search = function() {
        querySaTerminalManageList($scope, $http, 1);
    };
    //导出
    $scope.exportFn = function() {
        if($scope.user.buCode == '10'){
            Biostime.common.toastMessage('该功能只用于BNC事业部！');
            return
        }
        $scope.areaOfficeCode = $scope.department.officeCode!=''?$scope.department.officeCode:$scope.area.areaCode!=''?$scope.area.areaCode:'';
        relation = $scope.relation?$scope.relation:'';
        relationTerminal = $scope.relationTerminal?$scope.relationTerminal:'';
        window.location.href = BASICURL + "samanage/sa/terminal/new/exportSaTerminalNewManageList.action?"
        +'saCode='+$scope.saCode+'&terminalCode='+$scope.terminalCode + "&buCode="+$scope.buId.buCode+'&areaOfficeCode='+$scope.areaOfficeCode+'&relationTerminalCode='+$scope.relationTerminalCode+'&relation='+relation+'&relationTerminal='+relationTerminal + '&' + $scope.token;
        // window.location.href = "http://10.50.101.162:8080/" + "samanage/sa/terminal/new/exportSaTerminalNewManageList.do?"+'saCode='+$scope.saCode+'&terminalCode='+$scope.terminalCode+'&areaOfficeCode='+$scope.areaOfficeCode+'&relationTerminalCode='+$scope.relationTerminalCode?$scope.relationTerminalCode:''+'&relation='+$scope.relation?$scope.relation:''+'&relationTerminal='+$scope.relationTerminal?$scope.relationTerminal:'';
    };

    //分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        querySaTerminalManageList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        querySaTerminalManageList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        querySaTerminalManageList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            querySaTerminalManageList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        querySaTerminalManageList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 3;
        if ($scope.pagination.pageCount < 3) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
    //关闭
    $scope.close = function() {
        easyDialog.close();
        $scope.currentItem = {};
    };
    //操作
    $scope.easyDialogFun = function(tableItem,type) {
        $scope.easyDialogType = type;
        $scope.currentItem = angular.copy(tableItem);
        if($scope.easyDialogType == 0) {
            var today = new Date().getTime();
            if(new Date($scope.currentItem.startDate).getTime() <= today){
                $scope.dateDisabled = true;
            }else{
                $scope.dateDisabled = false;
            }
            easyDialog.open({
                container: 'easyDialogShowId'
            })
            document.getElementById('startDate').value = $scope.currentItem.startDate;
            document.getElementById('endDate').value = $scope.currentItem.endDate;
        }else if($scope.easyDialogType == 1){
            easyDialog.open({
                container: 'deletedId'
            })
        }else if($scope.easyDialogType == 2 || $scope.easyDialogType == 3){
            easyDialog.open({
                container: 'changeId'
            });
            // $scope.isSubmit = false;  
            if(Biostime.common.isEmpty($scope.currentItem.terminalCode)){
                $scope.currentItem.terminalCode = '';
                $scope.currentItem.terminalName = '';
            }
            if(Biostime.common.isEmpty($scope.currentItem.relationTerminalCode)){
                $scope.currentItem.relationTerminalCode = '';
                $scope.currentItem.relationTerminalName = '';
            }
            $scope.oldTerminalCode = $scope.currentItem.terminalCode;
            $scope.oldTerminalName = $scope.currentItem.terminalName;
            $scope.oldRelationTerminalCode = $scope.currentItem.relationTerminalCode;
            $scope.oldRelationTerminalName = $scope.currentItem.relationTerminalName;

            $scope.noChangeTime = false;
            if($scope.easyDialogType == 2){
                $scope.noChangeTime = true;
                document.getElementById('begainDate').value = $scope.currentItem.startDate;
                document.getElementById('closeDate').value = $scope.currentItem.endDate;
                document.getElementById('close_Date').value = $scope.currentItem.endDate;
            }
            if($scope.easyDialogType == 3){
                var today = new Date();
                var date = today.getDate();
                var nextDay = new Date(today.setDate(date+1)).format('yyyy-MM-dd');
                document.getElementById('begainDate').value = nextDay;
                document.getElementById('closeDate').value = '';
                document.getElementById('close_Date').value = '';
            }
            var today = new Date();
            var date = today.getDate();
            var nextDay = new Date(today.setDate(date+1)).format('yyyy-MM-dd');
            if($scope.currentItem.startDate<nextDay){
                $scope.ctShow = 0;
            }else{
                $scope.ctShow = 1;
            }
        };
    };

    //根据查询门店编号、门店名称
    $scope.checkTerminalCode = function (type,val) {
        if(type == 1){
            if($scope.currentItem.terminalCode != "" && $scope.currentItem.terminalCode != undefined){
                getTerminal($scope, $http, type, val);
            }else{
                $scope.currentItem.terminalCode = '';
                $scope.currentItem.terminalName = '';
            }
        }else if(type == 2){
            if($scope.currentItem.relationTerminalCode != "" && $scope.currentItem.relationTerminalCode != undefined){
                getTerminal($scope, $http, type, val);
            }else{
                $scope.currentItem.relationTerminalCode = '';
                $scope.currentItem.relationTerminalName = '';
            }
        }
    };
    $scope.changeTerminalCode = function (type) {
        var today = new Date();
        var date = today.getDate();
        var nextDay = new Date(today.setDate(date+1)).format('yyyy-MM-dd');
        document.getElementById('begainDate').value = nextDay;
        $scope.currentItem.relationTerminalName = '';
        if(type == 1){
            $scope.currentItem.terminalName = '';
            $scope.currentItem.relationTerminalCode = '';
            document.getElementById('closeDate').value = '';
            document.getElementById('close_Date').value = '';
        }
        $scope.noChangeTime = false;
        if(type == 2){
            if($scope.currentItem.terminalCode == $scope.oldTerminalCode && $scope.currentItem.startDate<=today.format('yyyy-MM-dd')){
                $scope.noChangeTime = true;
                document.getElementById('begainDate').value = $scope.currentItem.startDate;
            }
        }
    };

    //操作-确定
    $scope.sureFun = function() {
        if($scope.easyDialogType == 0) {
            editSaTerminalManage($scope, $http);
        }else if($scope.easyDialogType == 1){
            deleteSaTerminalManage($scope, $http);
        }else if($scope.easyDialogType == 2 || $scope.easyDialogType == 3){
            /*if($scope.oldTerminalCode == $scope.currentItem.terminalCode && $scope.oldRelationTerminalCode == $scope.currentItem.relationTerminalCode && document.getElementById('begainDate').value == $scope.currentItem.startDate){
                if($scope.ctShow == 0 && document.getElementById('closeDate').value == $scope.currentItem.endDate || $scope.ctShow == 1 && document.getElementById('close_Date').value == $scope.currentItem.endDate){
                    $scope.isSubmit = false;
                    console.log($scope.isSubmit)
                }
            }else{
                saveSaTerminalRelation($scope, $http);
            }*/
            saveSaTerminalRelation($scope, $http);
        }
    };
});
/**
 * 查询接口
 */
function querySaTerminalManageList($scope, $http, pageNum) {
    if($scope.user.buCode == '10'){
        Biostime.common.toastMessage('该功能只用于BNC事业部！');
        return
    }
    $scope.areaOfficeCode = $scope.department.officeCode!=''?$scope.department.officeCode:$scope.area.areaCode!=''?$scope.area.areaCode:'';
    Biostime.common.showLoading();
    var queryData = {'request':{
        'buCode': $scope.buId.buCode,
        'areaOfficeCode':$scope.areaOfficeCode,//大区办事处
        'saCode':$scope.saCode,//SA编号
        'terminalCode':$scope.terminalCode,//门店编码
        'relationTerminalCode':$scope.relationTerminalCode,//关联连锁门店编号
        'relation':$scope.relation,//有效关系(新  0:无,1：是,2:否)
        'relationTerminal':$scope.relationTerminal,//是否关联门店 1：是,2:否
        'pageNo': pageNum,
        'pageSize': 20
    }};
    // var url = "http://10.50.101.162:8080/" + 'samanage/sa/terminal/new/querySaTerminalNewManageList.do';
    var url = BASICURL + 'samanage/sa/terminal/new/querySaTerminalNewManageList.action' + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
                $scope.pagination.currentPage = 1;
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};
/**
 * 编辑
 */
function editSaTerminalManage($scope, $http) {
    Biostime.common.showLoading();
    var queryData = {'request':{
        'id':$scope.currentItem.id,
        'createdBy':$scope.userId,
        'createdName':$scope.userName,
        'startDate':document.getElementById('startDate').value,
        'endDate':document.getElementById('endDate').value
    }};
    var url = BASICURL + 'samanage/sa/terminal/editSaTerminalManage.action' + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            Biostime.common.toastMessage(data.desc);
            setTimeout(function(){
                easyDialog.close();
            },500);
            querySaTerminalManageList($scope, $http, 1);
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};
/**
 * 删除
 */
function deleteSaTerminalManage($scope, $http) {
    Biostime.common.showLoading();
    var queryData = {'request':{
        'id':$scope.currentItem.id,
        'createdBy':$scope.userId,
        'createdName':$scope.userName
    }};
    var url = BASICURL + 'samanage/sa/terminal/deleteSaTerminalManage.action' + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            Biostime.common.toastMessage(data.desc);
            setTimeout(function(){
                easyDialog.close();
            },500);
            querySaTerminalManageList($scope, $http, 1);
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};

/**
 * 变更/绑定门店
 */
function saveSaTerminalRelation($scope, $http) {
    $scope.isSubmit = false;
    if(Biostime.common.isEmpty($scope.currentItem.terminalCode)){
        $scope.isSubmit = true;
        Biostime.common.toastMessage('请填写1店的编号！');
        return
    }
    // if(Biostime.common.isEmpty($scope.currentItem.relationTerminalCode)){
    //     Biostime.common.toastMessage('请填写连锁门店的编号！');
    //     return
    // }
    if($scope.currentItem.terminalCode == $scope.currentItem.relationTerminalCode){
        Biostime.common.toastMessage('1店与关联连锁门店冲突！');
        $scope.isSubmit = true;
        $scope.currentItem.relationTerminalCode = '';
        $scope.currentItem.relationTerminalName = '';
        return
    }

    var bd = document.getElementById('begainDate').value;
    if(Biostime.common.isEmpty(bd)){
        $scope.isSubmit = true;
        Biostime.common.toastMessage('关系开始时间不能为空！');
        return
    }
    
    if($scope.oldTerminalCode == $scope.currentItem.terminalCode && $scope.oldRelationTerminalCode == $scope.currentItem.relationTerminalCode && document.getElementById('begainDate').value == $scope.currentItem.startDate){
        if($scope.ctShow == 0 && document.getElementById('closeDate').value == $scope.currentItem.endDate || $scope.ctShow == 1 && document.getElementById('close_Date').value == $scope.currentItem.endDate){
            Biostime.common.toastMessage('成功');
            setTimeout(function(){
                easyDialog.close();
                $scope.isSubmit = true;
            },500);
            return
        }
    }

    Biostime.common.showLoading();
    var queryData = {'request':{
        'id':$scope.currentItem.id,
        'areaOfficeCode':$scope.currentItem.areaOfficeCode,
        'saCode':$scope.currentItem.saCode,//SA编号
        'terminalCode':$scope.currentItem.terminalCode,//门店编码
        'relationTerminalCode':$scope.currentItem.relationTerminalCode,//关联连锁门店编号
        'createdBy':$scope.userId,//登陆人账号
        'createdName':$scope.userName,//登陆人名称
        'startDate':document.getElementById('begainDate').value,
        'endDate':$scope.ctShow == 0?document.getElementById('closeDate').value:document.getElementById('close_Date').value
    }};
    // var url = "http://10.50.101.162:8080/" + 'samanage/sa/terminal/new/saveSaTerminalRelation.action' + '?' + $scope.token;
    var url = BASICURL + 'samanage/sa/terminal/new/saveSaTerminalRelation.action' + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            Biostime.common.toastMessage(data.desc);
            setTimeout(function(){
                easyDialog.close();
                $scope.isSubmit = true;
            },500);
            querySaTerminalManageList($scope, $http, 1);
        }else{
            $scope.isSubmit = true;
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        $scope.isSubmit = true;
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};


//查询门店编号、门店名称
function getTerminal($scope, $http, type, val){
    Biostime.common.showLoading();
    var queryData = {
        "terminalCode": val
    };
    // var url = "http://10.50.101.162:8080/" + "samanage/ftf/activity/queryTerminal.action" + '?' + $scope.token;
    var url = BASICURL + "samanage/ftf/activity/queryTerminal.action" + '?' + $scope.token;
    $http({
        method: 'post',
        url: url,
        params: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if(data.response != null){
                if(type==1){
                    $scope.currentItem.terminalCode = data.response.code;
                    $scope.currentItem.terminalName = data.response.shortName;
                    if($scope.easyDialogType == 2 && $scope.currentItem.terminalCode == $scope.oldTerminalCode){
                        document.getElementById('begainDate').value = $scope.currentItem.startDate;
                        document.getElementById('closeDate').value = $scope.currentItem.endDate;
                        $scope.currentItem.relationTerminalCode = $scope.oldRelationTerminalCode;
                        $scope.currentItem.relationTerminalName = $scope.oldRelationTerminalName;
                        var today = new Date();
                        if($scope.currentItem.startDate<=today.format('yyyy-MM-dd')){
                            $scope.noChangeTime = true;
                        }else{
                            $scope.noChangeTime = false;
                        }
                    }
                }else if(type==2){
                    $scope.currentItem.relationTerminalCode = data.response.code;
                    $scope.currentItem.relationTerminalName = data.response.shortName;
                }
            }else{
                if(type==1){
                    // $scope.currentItem.terminalCode = '';
                    $scope.currentItem.terminalName = '';
                    Biostime.common.toastMessage('1店编码不存在');
                }else if(type==2){
                    // $scope.currentItem.relationTerminalCode = '';
                    $scope.currentItem.relationTerminalName = '';
                    Biostime.common.toastMessage('关联连锁门店编码不存在');
                }
            }
        }else{
            // $scope.currentItem.terminalCode = '';
            // $scope.currentItem.terminalName = '';
            // $scope.currentItem.relationTerminalCode = '';
            // $scope.currentItem.relationTerminalName = '';
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}

//根据身份，显示相应的大区和办事处、渠道、登录人id/姓名
function identityVerification2($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;
        $scope.userId = data.loadUserBaseInfoBean.userId;           //获取登录人id
        $scope.userName = data.loadUserBaseInfoBean.userName;       //获取登录人姓名

        $scope.channel = $scope.channelList[0];
        for(var i=0; i<$scope.channelList.length; i++){
            if($scope.channelList[i].channelCode == "01"){
                $scope.channel = $scope.channelList[i];
                break;
            }
        }

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }

        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
};

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
}

/***
 * 根据选择大区刷选办事处
 * */
function getDepartments2($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
};