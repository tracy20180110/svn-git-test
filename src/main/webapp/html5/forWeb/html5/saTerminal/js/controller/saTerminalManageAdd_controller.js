/*******************************************************************************
 * @jsname:index_controller.js
 * @author:wunan
 * @date:2017-02-28
 * @use:SA与门店管理.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);
var fd;
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    if(Biostime.common.getQueryString('token')) {
        $scope.token = "token=" + Biostime.common.getQueryString('token');
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid');
    };
    //初始化
    $scope.saCode = '';
    $scope.terminalCode = '';
    $scope.isSubmit = true;
    $scope.saTips = '';
    $scope.terminalTips = '';
    //根据身份显示相应的大区和办事处、渠道
    identityVerification2($scope, $http);
    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments2($scope, $http);
    };

    $scope.tabFun = function(sel) {
        $scope.sel = sel;
        if(sel == '1'){
            window.location.href="saTerminalManageNew.html?"+$scope.token;
        }
    };

    //根据查询门店编号、门店名称
    $scope.checkTerminalCode = function (type,val) {
        if(!Biostime.common.isEmpty(val)){
            getTerminal($scope, $http, type, val);
        }else{
            if(type == 1){
                $scope.saCode = '';
                $scope.saName = '';
                $scope.saTips = '请输入SA编号';
            }else if(type == 2){
                $scope.terminalCode = '';
                $scope.terminalName = '';
                $scope.terminalTips = '请输入门店编号';
            }
        }
    };

    //保存
    $scope.save = function (){
        saveSaTerminalRelationNew($scope,$http);
    };

});

//查询门店编号、门店名称
function getTerminal($scope, $http, type, val){
    Biostime.common.showLoading();
    var queryData;
    if(type == 1){
        queryData = {'request':{
            'saCode': val
        }};
    }else{
        queryData = {'request':{
            'terminalCode': val
        }};
    }
    // var url = "http://10.50.101.162:8080/" + "samanage/sa/terminal/new/queryTerminalInfo.action" + '?' + $scope.token;
    var url = BASICURL + "samanage/sa/terminal/new/queryTerminalInfo.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if(!Biostime.common.isEmpty(data.response)){
                if(type==1){
                    // $scope.saCode = data.response.saCode;
                    $scope.saName = data.response.saName;
                    $scope.saTips = '';
                }else if(type==2){
                    // $scope.terminalCode = data.response.terminalCode;
                    $scope.terminalName = data.response.terminalName;
                    $scope.terminalTips = '';
                }
            }
        }else{
            if(type==1){
                $scope.saName = '';
                $scope.saTips = data.desc;
            }else if(type==2){
                $scope.terminalName = '';
                $scope.terminalTips = data.desc;
            }
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
};

//保存
function saveSaTerminalRelationNew($scope,$http){
    if($scope.user.buCode == '10'){
        Biostime.common.toastMessage('该功能只用于BNC事业部！');
        return
    }
    $scope.isSubmit = false;

    if(Biostime.common.isEmpty($scope.saCode)){
        $scope.isSubmit = true;
        Biostime.common.toastMessage('SA编号不能为空！');
        return
    }
    if(!Biostime.common.isEmpty($scope.saTips)){
        $scope.isSubmit = true;
        Biostime.common.toastMessage($scope.saTips);
        return
    };
    if(Biostime.common.isEmpty($scope.terminalCode)){
        $scope.isSubmit = true;
        Biostime.common.toastMessage('绑定门店编号不能为空！');
        return
    }
    if(!Biostime.common.isEmpty($scope.terminalTips)){
        $scope.isSubmit = true;
        Biostime.common.toastMessage($scope.terminalTips);
        return
    }

    var bd = document.getElementById('begainDate').value;
    if(Biostime.common.isEmpty(bd)){
        $scope.isSubmit = true;
        Biostime.common.toastMessage('关系开始时间不能为空！');
        return
    };
    var cd = document.getElementById('closeDate').value;

    Biostime.common.showLoading();
    var queryData = {'request':{
        'saCode': $scope.saCode,
        'terminalCode': $scope.terminalCode,
        'createdBy': $scope.userId,
        'createdName': $scope.userName,
        'startDate': bd,
        'endDate': cd || ''
    }};
    // var url = "http://10.50.101.162:8080/" + 'samanage/sa/terminal/new/saveSaTerminalRelationNew.action' + '?' + $scope.token;
    var url = BASICURL + 'samanage/sa/terminal/new/saveSaTerminalRelationNew.action' + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            Biostime.common.toastMessage('创建成功！');
            setTimeout(function(){
                $scope.isSubmit = true;
                $scope.tabFun(1);
            },2000);
        }else{
            $scope.isSubmit = true;
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        $scope.isSubmit = true;
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
}

//根据身份，显示相应的大区和办事处、渠道、登录人id/姓名
function identityVerification2($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        $scope.buList = data.loadUserBuBeanList;
        // $scope.areas=data.loadUserAreaBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;
        $scope.user = data.loadUserBaseInfoBean;           //获取登录人id
        $scope.userId = data.loadUserBaseInfoBean.userId;           //获取登录人id
        $scope.userName = data.loadUserBaseInfoBean.userName;       //获取登录人姓名

        $scope.channel = $scope.channelList[0];
        for(var i=0; i<$scope.channelList.length; i++){
            if($scope.channelList[i].channelCode == "01"){
                $scope.channel = $scope.channelList[i];
                break;
            }
        }

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        // $scope.area=$scope.areas[0];
        buChange($scope);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
};

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 * */
function getDepartments2($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
};