
/** 定义指令模块 **/
define(['./module'], function(directives) {
	'use strict';
	/*路演活动配置--活动查询--修改门店--导入*/
	directives.directive('changeUpload', function() {
		return {
			restrict: 'AE',
			scope: {
				uploadStore: '&'
			},
			template: '<div class="fl por ovh"><input type="button" id="storeBtn" style="padding:0; position: absolute; top: 0; left: 0; background: none; border: none;color: #fff; width:108px; height: 30px; line-height: 30px;" value="导入门店"><input type="file" name="file" id="file" ng-disabled="changeDisabled" style="position: absolute; top: 0; left: 0; opacity: 0;height: 34px;" accept=".xlsx,.xls"></div>',
			replace: true,
			link: function(scope, ele, attrs) {
				ele.bind('click', function() {
					$('#file').val('');
				});
				ele.bind('change', function() {
					var file = ele[0].children[1].files;
					if (file) {
						scope.$parent.uploadFile({'files':file});
					}else{
						Biostime.common.toastMessage('导入文件不能为空!')
					}
				});
			}
		}
	});

});