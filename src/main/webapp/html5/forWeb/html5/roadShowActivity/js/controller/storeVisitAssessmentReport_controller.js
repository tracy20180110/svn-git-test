/* 
* @Author: lonjintao
* @Date:   2019-06-05 10:20:35
* @Last Modified by:   Marte
* @Last Modified time: 2019-12-31 13:28:25
*/

var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    //获取token
    if(Biostime.common.getQueryString('token')){
        $scope.token = "token="+Biostime.common.getQueryString('token')
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid')
    }

    //根据身份显示相应的渠道
    identityVerification($scope, $http).then(function(){
        queryTerminalVisitPostList($scope, $http, '1');
        queryTerminalVisitPostList($scope, $http, '2');
    });

    //事业部修改，大区跟着改变
    $scope.buChange = function(){
        buChange($scope);
    };

    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments($scope, $http);
    };
    

    //初始化
    $scope.selNum = 0;
    $scope.isMore = false;
    $scope.name = '';
    $scope.account = '';
    
    $scope.selFun = function(num){
        $scope.selNum = num;
        // identityVerification($scope, $http);
        $scope.buId = $scope.buList[0];
        buChange($scope);
        $scope.name = '';
        $scope.account = '';
        $scope.postName = $scope.terminalVisitPostList[0];
        document.getElementById('startDate').value = '';
        document.getElementById('endDate').value = '';
        $scope.quarter = $scope.quarterList[0];
        $scope.infoList = [];
        // $scope.pagination = {};
        $scope.pagination.displayPageNum = 4;
        delete $scope.pagination.pageCount; 
        delete $scope.pagination.pageNums; 
        delete $scope.pagination.totalCount; 
    };

    //查询
    $scope.searchClick = function () {
        loadInfoList($scope, $http, 1);
    };

    //导出
    $scope.exportExcel = function () {
        if($scope.selNum == 0){
            if($scope.user.buCode == '10'){
                Biostime.common.toastMessage('该功能只用于BNC事业部');
                return
            }
            if(Biostime.common.isEmpty($scope.infoList)){
                Biostime.common.toastMessage('查询列表不能为空！请先操作查询。')
                return
            }
            //大区办事处
            var areaofficeCode = $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode;
            window.location.href = BASICURL + 'samanage/terminal/visit/exportTerminalVisitList.action?'+$scope.token+"&buCode="+$scope.buId.buCode+"&areaOfficeCode="+areaofficeCode+"&name="+$scope.name+"&account="+$scope.account+"&postCode="+$scope.postName.value+"&startMonth="+$scope.startMonth.replace('-','')+"&endMonth="+$scope.endMonth.replace('-','');
        }else if($scope.selNum == 1){
            if($scope.user.buCode == '10'){
                Biostime.common.toastMessage('该功能只用于BNC事业部');
                return
            }
            if(Biostime.common.isEmpty($scope.infoList)){
                Biostime.common.toastMessage('查询列表不能为空！请先操作查询。')
                return
            }
            //大区办事处
            var areaofficeCode = $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode;
            window.location.href = BASICURL + 'samanage/terminal/visit/exportQuarterTerminalVisitList.action?'+$scope.token+"&buCode="+$scope.buId.buCode+"&areaOfficeCode="+areaofficeCode+"&name="+$scope.name+"&account="+$scope.account+"&postCode="+$scope.postName.value+"&quarter="+$scope.quarter.key;
        }
    };

    // 分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadInfoList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadInfoList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //账号管理-跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 4;
        if ($scope.pagination.pageCount < 4) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;

});

//根据身份，显示相应的大区和办事处、渠道,登录人信息
function identityVerification($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;

        //获取登录人信息
        $scope.user = data.loadUserBaseInfoBean;
        $scope.userInfo = data.loadUserBaseInfoBean.userId + '/' + data.loadUserBaseInfoBean.userName;
        $scope.createdByName = data.loadUserBaseInfoBean.userName;
        $scope.createdBy = data.loadUserBaseInfoBean.userId;

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = angular.copy(data.loadUserAreaBeanList);
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll);
        }
        $scope.department=$scope.departments[0];
        buChange($scope);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
};

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];

    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}

/**
 * 查询门店拜访岗位列表/季度
 * @param $scope
 * @param $http
 */
function queryTerminalVisitPostList($scope, $http, type) {
    Biostime.common.showLoading();
    var queryData = {'request':{
        "type":type                             //1、查岗位列表、2：查季度列表
    }};
    var url = BASICURL + "samanage/terminal/visit/queryTerminalVisitPostList.action?" + $scope.token;
    // var url = "http://10.50.101.162:8080/samanage/terminal/visit/queryTerminalVisitPostList.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (Biostime.common.isEmpty(data.response)) {
                //Biostime.common.toastMessage("查无数据");
                $scope.terminalVisitPostList = [];
                $scope.quarterList = [];
            }else if (data.response.length > 0) {
                if(type == '1'){//岗位列表
                    $scope.terminalVisitPostList = data.response;
                    var obj = {'key':'全部','value':''};
                    $scope.terminalVisitPostList.unshift(obj);
                    $scope.postName = $scope.terminalVisitPostList[0];
                }else if(type == '2'){//季度列表
                    $scope.quarterList = data.response;
                    $scope.quarter = $scope.quarterList[0];
                }
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
};

/**
 * 查询
 * @param $scope
 * @param $http
 * @param pageNum
 */
function loadInfoList($scope, $http, pageNum) {
    var queryData;
    var url;
    if($scope.selNum == 0){
        if($scope.user.buCode == '10'){
            Biostime.common.toastMessage('该功能只用于BNC事业部');
            return
        }
        $scope.startMonth = document.getElementById('startDate').value;
        $scope.endMonth = document.getElementById('endDate').value;

        if(Biostime.common.isEmpty($scope.startMonth) || Biostime.common.isEmpty($scope.endMonth)){
            Biostime.common.toastMessage('查询时间不能为空');
            return
        };

        queryData = {'request':{
            "buCode":$scope.buId.buCode,
            "areaOfficeCode":$scope.department.officeCode ? $scope.department.officeCode : $scope.area.areaCode,
            "name":$scope.name,                             //姓名
            "account":$scope.account,                       //工号
            "postCode":$scope.postName.value,               //岗位
            "startMonth":$scope.startMonth.replace('-',''), //开始月份
            "endMonth":$scope.endMonth.replace('-',''),     //结束月份
            'pageNo': pageNum,
            'pageSize': 20
        }};
        url = BASICURL + "samanage/terminal/visit/queryTerminalVisitList.action?" + $scope.token;
        // url = "http://10.50.101.162:8080/samanage/terminal/visit/queryTerminalVisitList.action?"+$scope.token;
    }else if($scope.selNum == 1){
        if($scope.user.buCode == '10'){
            Biostime.common.toastMessage('该功能只用于BNC事业部');
            return
        }
        queryData = {'request':{
            "buCode":$scope.buId.buCode,
            "areaOfficeCode":$scope.department.officeCode ? $scope.department.officeCode : $scope.area.areaCode,
            "name":$scope.name,                             //姓名
            "account":$scope.account,                       //工号
            "postCode":$scope.postName.value,               //岗位
            "quarter":$scope.quarter.key,                       //季度
            'pageNo': pageNum,
            'pageSize': 20
        }};
        url = BASICURL + "samanage/terminal/visit/queryQuarterTerminalVisitList.action?" + $scope.token;
        // url = "http://10.50.101.162:8080/samanage/terminal/visit/queryQuarterTerminalVisitList.action?"+$scope.token;
    };

    Biostime.common.showLoading();
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (Biostime.common.isEmpty(data.response.datas)) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
