/**
 * 页面路由跳转配置
 */
define(['./app'], function (app) {
	'use strict';

	return app.config(['$routeProvider','$controllerProvider',
		function ($routeProvider,$controllerProvider) {
			$routeProvider.when('/view1/:id', {
				templateUrl: 'views/activitySearch.html',
				controller: 'activitySearch_controller',
				resolve: {
                    keyName: function($q) {
                        var deferred = $q.defer();
                        require(['/../controller/activitySearch_controller'], function(controllers) {

                            $controllerProvider.register('activitySearch_controller', controllers.templateCtrl1);

                            deferred.resolve();
                        });
                        return deferred.promise;
                    }
                }
			});
            
            $routeProvider.when('/view2/:id', {
                templateUrl: 'views/addActivity.html?bust=' + (new Date()).getTime(),
                controller: 'addActivity_controller',
                resolve: {
                    keyName: function($q) {
                        var deferred = $q.defer();
                        require(['/../controller/addActivity_controller'], function(controllers) {

                            $controllerProvider.register('addActivity_controller', controllers.templateCtrl2);

                            deferred.resolve();
                        });
                        return deferred.promise;
                    }
                }
            });
    }]);
});