/*******************************************************************************
 * @jsname:activityDetail_controller.js
 * @author:WangDongping
 * @date:2017-1-5
 * @use:营销上报明细.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);

biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    //获取token
    if(Biostime.common.getQueryString('token')){
        $scope.token = "token="+Biostime.common.getQueryString('token')
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid')
    }
    
    //根据身份显示相应的大区和办事处、渠道，登录人信息
    identityVerification($scope, $http).then(function(){
        getSubordinateMenus($scope, $http);
        getImgTypeList($scope,$http);
    });

    //事业部修改，大区跟着改变
    $scope.buChange = function(){
        buChange($scope);
    };

    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments($scope, $http);
    };

    //初始化
    $scope.terminalCode = "";
    $scope.relationTerminalCode = "";
    $scope.actId = "";
    $scope.createdBy = "";
    $scope.reportBy = "";
    $scope.infoList = "";
    $scope.auditDisabled = true;
    //是否补录列表
    $scope.isRecordedList = [
        {'name':"不限",'value':""},
        {'name':"否",'value':"0"},
        {'name':"是",'value':"1"}
    ];
    $scope.isRecorded = $scope.isRecordedList[0];
    //补录原因列表
    $scope.recordedCauseList = [
        {'name':"全部",'value':""},
        {'name':"网络原因未能及时上报",'value':"1"},
        {'name':"营销通系统原因未能及时上报",'value':"2"},
        {'name':"其他",'value':"3"}
    ];
    $scope.recordedCause = $scope.recordedCauseList[0];

    $scope.salesQuery = '';

    //查询路演活动明细
    $scope.searchClick = function () {
        loadInfoList($scope, $http,1);
    };

    //是否按促销员维度查询
    $scope.isSalesManQuery = function (){
        if($scope.salesMan){
            $scope.salesQuery = '1';
        }else{
            $scope.salesQuery = '';
        }
        $scope.infoList = [];
        $scope.pagination.displayPageNum = 4;
        delete $scope.pagination.pageCount; 
        delete $scope.pagination.pageNums; 
        delete $scope.pagination.totalCount;
    };

    //导出
    $scope.exportExcel = function () {
        if(Biostime.common.isEmpty($scope.infoList)){
            Biostime.common.toastMessage('查询列表不能为空！请先操作查询。');
            return
        }else{
            //大区办事处
            $scope.areaofficeCode = $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode;
            $scope.actId = $scope.actId == null ? "" : $scope.actId;

            window.location.href = BASICURL + 'samanage/actRoadShow/exportActRoadShowDetail.action?brand='+$scope.brand.value
                + '&buCode=' + $scope.buId.buCode + '&areaofficeCode=' + $scope.areaofficeCode + '&suffice=' + $scope.sufficeStatus.value
                + '&startDate=' + $scope.dateBegin3 + '&endDate=' + $scope.dateOver3 + '&actId=' + $scope.actId
                + '&createdBy=' + $scope.createdBy + '&reportBy=' + $scope.reportBy
                + '&terminalCode=' + $scope.terminalCode
                + '&relationTerminalCode=' + $scope.relationTerminalCode
                + '&reportStartDate=' + $scope.dateBegin1 + '&reportEndDate=' + $scope.dateOver1 + '&salesQuery=' + $scope.salesQuery
                + '&' + $scope.token;
    }
    };

    //审批 弹出框
    $scope.showAudit = function (id,type) {
        //初始化
        $scope.isSuffice = $scope.sufficeList[0].value;
        $scope.auditMemo = '';
        $scope.auditType = type;
        if($scope.auditType=='all'&&$scope.auditDisabled==true){
            return;
        }else{
            $scope.actDetailId = id;
        };
        //判断是否有勾选，无勾选提示切不可继续操作
        $scope.checkboxs = false;
        angular.forEach($scope.infoList,function(infoList){
            if(infoList.checkboxSel!=undefined&&infoList.checkboxSel){
                $scope.checkboxs = true;//说明有勾选
            }
        });
        if($scope.checkboxs == false && $scope.auditType=='all'){
            Biostime.common.toastMessage('请选中后再审核！');
            return;
        };

        //开启窗口
        easyDialog.open({
            container: 'auditId'
        });
    };

    //全选
    $scope.allSel = function() {
        if($scope.checkboxSelAll){
            angular.forEach($scope.infoList,function(item){
                if(!item.auditTime){
                    item.checkboxSel = true;
                }
            });
        }else{
            angular.forEach($scope.infoList,function(item){
                if(!item.auditTime){
                    item.checkboxSel = false;
                }
            });
        }
    };

    //当取消某一个单选时，全选去掉勾选
    $scope.checkboxSel = function(item){
        $scope.checkbox = false;
        if(!$scope.auditDisabled){
            angular.forEach($scope.infoList,function(infoList){
                if(infoList.checkboxSel!=undefined&&!infoList.checkboxSel){
                    $scope.checkbox = true;//说明没有全选
                }
            });
            if($scope.checkbox){
                $scope.checkboxSelAll = false;
            }else{
                $scope.checkboxSelAll = true;
            };
        };
    };

    //关闭 审批
    $scope.close = function() {
        easyDialog.close();
    };

    //确认 审批
    $scope.submitAudit = function () {
        audit($scope, $http);
    };

    //根据终端编码，获取终端名称
    $scope.checkTerminal = function(){
        if(Biostime.common.isEmpty($scope.recordTerminalCode)){
            $scope.recordTerminalName = "";
            return;
        }
        if(Biostime.common.isEmpty($scope.code)){
            Biostime.common.toastMessage("请先输入补录活动ID");
            return;
        }
        if($scope.activityDis){
            Biostime.common.toastMessage($scope.activityDesc);
            return;
        }
        checkTerminalCode($scope,$http);
    };

    //根据上报人工号，获得上报人名称
    $scope.checkCreatedBy = function(){
        if(Biostime.common.isEmpty($scope.recordCreatedBy)){
            $scope.createdName = "";
            return;
        }
        if(Biostime.common.isEmpty($scope.recordTerminalCode)){
            Biostime.common.toastMessage("请先输入终端编码");
            return;
        }
        checkCreatedBy($scope, $http);
    };

    //初始化
    //品牌（1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie）
    $scope.brandList = [
        {'name':"全部",'value':""},
        {'name':"合生元",'value':"1"},
        {'name':"素加",'value':"2"},
        {'name':"葆艾",'value':"3"},
        {'name':"HT",'value':"21706"},
        {'name':"合生元可贝思",'value':"36706"},
        {'name':"SWISSE",'value':"26509"},
        {'name':"Dodie",'value':"33206"},
        {'name':"其他",'value':"0"}
    ];
    $scope.brand = $scope.brandList[0];

    //审核状态（审核状态 0：未审核，1：已审核）
    $scope.sufficeStatusList = [
        {'name':"全部",'value':""},
        {'name':"已审核",'value':"1"},
        {'name':"未审核",'value':"0"}
    ];
    $scope.sufficeStatus = $scope.sufficeStatusList[0];

    //短信类型
    $scope.sufficeList=[
        {'name':"合格",'value':"1"},
        {'name':"不合格",'value':"0"}
    ];
    $scope.isSuffice = $scope.sufficeList[0].value;

    //图片地址
    $scope.imgSrcGPOne = 'images/add.png';
    $scope.imgSrcGPTwo = 'images/add.png';
    $scope.imgSrcGPThree = 'images/add.png';

    // 分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadInfoList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadInfoList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //账号管理-跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 4;
        if ($scope.pagination.pageCount < 4) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;

    //展示大图
    $scope.bigpic_show = false;

    $scope.showBigpic = function(index,item){
        $scope.bigpic_show = true;
        $scope.index = index;
        $scope.currentItem = item;
        show($scope,$scope.index,$scope.currentItem);
    };
    
    $scope.closePic = function(){
        $scope.bigpic_show = false;
    };

    $scope.prevPic = function(){
        $scope.index--;
        if($scope.index<0){
            $scope.index = $scope.currentItem.imgList.length-1;
        };
        show($scope,$scope.index,$scope.currentItem);
    };

    $scope.nextPic = function(){
        $scope.index++;
        if($scope.index>$scope.currentItem.imgList.length-1){
            $scope.index = 0;
        };
        show($scope,$scope.index,$scope.currentItem);
    }

});
//根据身份，显示相应的大区和办事处、渠道,登录人信息
function identityVerification($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;

        //获取登录人信息
        $scope.userId = data.loadUserBaseInfoBean.userId;
        $scope.userName = data.loadUserBaseInfoBean.userName;

        //渠道
        if($scope.channelList.length > 1){
            var channelAll={
                channelCode:'',
                channelName:"全部"
            };
            $scope.channelList.splice(0,0,channelAll);
        }
        $scope.channel = $scope.channelList[0];

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);

        //添加埋点
        var date = new Date();
        var has = md5('6'+'60702'+date.getTime()+$scope.userId);
        var dataMd = {
            "platform":6,
            "point_code":'60702',
            "created_time":date.getTime(),
            "customer_id":$scope.userId,
            "sign":has
        };
        eventpv($scope,$http,dataMd);
    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}
/**
 * 查询路演活动明细
 * @param $scope
 * @param $http
 * @param pageNum
 */
function loadInfoList($scope, $http, pageNum) {
    $scope.checkboxSelAll = false;

    //活动上报日期
    $scope.dateBegin1 = document.getElementById("dateBegin1").value;
    $scope.dateOver1 = document.getElementById("dateOver1").value;
    //执行时间 
    $scope.dateBegin3 = document.getElementById("dateBegin3").value;
    $scope.dateOver3 = document.getElementById("dateOver3").value;

    if(Biostime.common.isEmpty($scope.dateBegin1) && Biostime.common.isEmpty($scope.dateOver1) && Biostime.common.isEmpty($scope.dateBegin3) && Biostime.common.isEmpty($scope.dateOver3)){
            Biostime.common.toastMessage("执行时间或活动上报日期不能为空!");
            return;
    }else if(!Biostime.common.isEmpty($scope.dateBegin1) && Biostime.common.isEmpty($scope.dateOver1)){
        Biostime.common.toastMessage("请选择活动上报日期的结束时间");
        return;
    }else if(Biostime.common.isEmpty($scope.dateBegin1) && !Biostime.common.isEmpty($scope.dateOver1)){
        Biostime.common.toastMessage("请选择活动上报日期的开始时间");
        return;
    }else if(!Biostime.common.isEmpty($scope.dateBegin3) && Biostime.common.isEmpty($scope.dateOver3)){
        Biostime.common.toastMessage("请选择执行时间的结束时间");
        return;
    }else if(Biostime.common.isEmpty($scope.dateBegin3) && !Biostime.common.isEmpty($scope.dateOver3)){
        Biostime.common.toastMessage("请选择执行时间的开始时间");
        return;
    }; 

    if($scope.dateOver3 == undefined){
        $scope.dateOver3 = "";
    }
    if($scope.dateOver1 == undefined){
        $scope.dateOver1 = "";
    }

    //20180510-当开始时间选择2018-04-25之前的日期，那么结束日期只能选2018-04-25前的日期；
    $scope.beforeDate = false;
    var beforeDate = '2018-04-25';
    var beginDate = '';
    var overDate = '';
    beforeDate = new Date(beforeDate).getTime();
    if(!Biostime.common.isEmpty($scope.dateBegin3)&&!Biostime.common.isEmpty($scope.dateOver3)){
        beginDate = new Date($scope.dateBegin3).getTime();
        overDate = new Date($scope.dateOver3).getTime();
        if(beginDate <= beforeDate&&overDate>beforeDate){
            $scope.beforeDate = true;
        };
    };
    if($scope.beforeDate == true){
        var btnFn = function () {
            easyDialog.close();
            return false;
        };
        easyDialog.open({
            container: {
                header: '提示',
                content: '2018-04-26之前/后的数据不可交叉查询，如：3月1-5月1日数据需拆分两个时间段查询：3月1日-4月25日以及4月26日-5月1日.',
                yesFn: btnFn,
                noFn: true
            }
        });
        $('#easyDialogWrapper').width(440);
        $('#easyDialogYesBtn').css('margin-left','70px');
        return;
    };

    var beginDate = '';
    var overDate = '';
    if(!Biostime.common.isEmpty($scope.dateBegin1)&&!Biostime.common.isEmpty($scope.dateOver1)){
        beginDate = new Date($scope.dateBegin1).getTime();
        overDate = new Date($scope.dateOver1).getTime();
        if(beginDate <= beforeDate&&overDate>beforeDate){
            $scope.beforeDate = true;
        };
    };
    if(!Biostime.common.isEmpty($scope.dateBegin3)&&!Biostime.common.isEmpty($scope.dateBegin1)){
        if(new Date($scope.dateBegin3).getTime()<=beforeDate&&new Date($scope.dateBegin1).getTime()>beforeDate){
            $scope.beforeDate = true;
        }else if(new Date($scope.dateBegin3).getTime()>beforeDate&&new Date($scope.dateBegin1).getTime()<=beforeDate){
            $scope.beforeDate = true;
        };
    };
    if($scope.beforeDate == true){
        var btnFn = function () {
            easyDialog.close();
            return false;
        };
        easyDialog.open({
            container: {
                header: '提示',
                content: '2018-04-26之前/后的数据不可交叉查询，如：3月1-5月1日数据需拆分两个时间段查询：3月1日-4月25日以及4月26日-5月1日.',
                yesFn: btnFn,
                noFn: true
            }
        });
        $('#easyDialogWrapper').width(440);
        $('#easyDialogYesBtn').css('margin-left','70px');
        return;
    };

    $scope.pageNum = pageNum;
    Biostime.common.showLoading();
    var queryData = {'request':{
        "buCode": $scope.buId.buCode,
        "areaofficeCode": $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode,  //大区办事处编码
        "brandCode": $scope.brand.value,                            //品牌
        "suffice": $scope.sufficeStatus.value,                  //审核状态
        "startDate":$scope.dateBegin3,                          //执行时间起
        "endDate":$scope.dateOver3,                             //执行时间止
        "reportStartDate":$scope.dateBegin1,                    //活动上报日期起
        "reportEndDate":$scope.dateOver1,                       //活动上报日期止
        "actId":$scope.actId,                                   //活动ID
        "createdBy":$scope.createdBy,                           //活动创建人工号
        "reportBy":$scope.reportBy,                           //上报人工号
        "terminalCode":$scope.terminalCode,                     //门店编码
        "relationTerminalCode":$scope.relationTerminalCode,                     //连锁门店编码
        "channelCode": $scope.channel.channelCode,                              //渠道
        "imageMarkType": $scope.imageMarkType.name == "全部" ? "" : $scope.imageMarkType.name,          //图片类别
        "actMakeup": $scope.isRecorded.value,                          //是否补录
        "makeupMemo": $scope.recordedCause.value,                        //补录原因
        "salesQuery": $scope.salesQuery,
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + "samanage/actRoadShow/queryActRoadShowDetailList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
                $scope.auditDisabled = true;
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
                $scope.auditDisabled = false;
                $scope.auditList = false;
                angular.forEach($scope.infoList,function(infoList){
                    infoList.imgList = [];
                    if(!infoList.auditTime){
                        $scope.auditList = true;
                    };
                    if(infoList.img1){
                        infoList.imgList.push(infoList.img1);
                    }; 
                    if(infoList.img2){
                        infoList.imgList.push(infoList.img2);
                    }; 
                    if(infoList.img3){
                        infoList.imgList.push(infoList.img3);
                    }; 
                    if(infoList.img4){
                        infoList.imgList.push(infoList.img4);
                    }; 
                    if(infoList.img5){
                        infoList.imgList.push(infoList.img5);
                    }; 
                    if(infoList.img6){
                        infoList.imgList.push(infoList.img6);
                    };
                });
                if($scope.auditList==false){
                    $scope.auditDisabled = true;
                };
            };
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            };
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 审批
 * @param $scope
 * @param $http
 */
function audit($scope, $http) {
    if($scope.auditType=='all'){
        //渠道遍历
        $scope.actDetailId = "";
        for(var i=0; i < $scope.infoList.length; i++){
            if($scope.infoList[i].checkboxSel == true&&$scope.infoList[i].id!='') {
                $scope.actDetailId += $scope.infoList[i].id + ",";
            }
        };
        //去除最后一个逗号
        $scope.actDetailId = $scope.actDetailId.substring(0,$scope.actDetailId.length-1);
    };
    Biostime.common.showLoading();
    var queryData = {'request':{
        "actDetailId": $scope.actDetailId,                      //活动明细ID
        "isSuffice": $scope.isSuffice,                          //是否合格
        "auditMemo": $scope.auditMemo,                          //审批备注
        "userId": $scope.userId,                                //登录人id
        "createdName":$scope.userName                           //登录人名称
    }};
    var url = BASICURL + "samanage/actRoadShow/auditActRoadShowDetail.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            Biostime.common.toastMessage("审批成功");
            easyDialog.close();              //关闭弹出框
            //清空审批内容
            $scope.auditMemo = "";
            $scope.isSuffice = $scope.sufficeList[0].value;
            loadInfoList($scope, $http, $scope.pageNum);   //重新查询
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}

/**
 * 根据终端编码，获得终端名称
 * @param $scope
 * @param $http
 */
function checkTerminalCode($scope, $http){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "channelCode": $scope.recordChannelCode,                           //活动渠道
        "areaOfficeCode": $scope.recordAreaOfficeCode,                     //活动大区办事处
        "terminalCode": $scope.recordTerminalCode                          //终端编码
    }};
    var url = BASICURL + "samanage/actRoadShow/checkActTerminal.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            $scope.terminalDis = false;
            $scope.terminalDesc = "";
            $scope.recordTerminalName = data.response;                  //终端名称
        }else{
            $scope.terminalDis = true;
            $scope.terminalDesc = data.desc;                            //记录终端错误信息
            $scope.recordTerminalName = "";
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    });
}
/**
 * 根据上报人工号，获得上报人名称
 * @param $scope
 * @param $http
 */
function checkCreatedBy($scope, $http){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "createdBy": $scope.recordCreatedBy,                               //上报人工号
        "terminalCode": $scope.recordTerminalCode                          //终端编码
    }};
    var url = BASICURL + "samanage/actRoadShow/checkAccountTerminal.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            $scope.createdByDis = false;
            $scope.createdByDesc = "";
            $scope.createdName = data.response;                         //上报人姓名
        }else{
            $scope.createdByDis = true;
            $scope.createdByDesc = data.desc;                           //记录上报人工号错误信息
            $scope.createdName = "";
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    });
}

/**
 * 获取用户当前页面下有效的菜单或按钮
 */
function getSubordinateMenus($scope, $http) {
    var token = Biostime.common.getQueryString("token");
    var urlPath = getCurrentUrlPath();
    var queryData = {
        "seqNo": new Date().getTime(),
        "sourceSystem": "merchant",
        "request": {
            "userId": $scope.userId,  // 必要参数,但可以值为空
            "token": token, // 必要参数
            "url": urlPath // 必要参数
        }
    };
    var url = useId + 'mkt-common/baseInfo/getSubordinateMenus.action' + '?token=' + token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            $scope.buttonPermissionList = data.response;
            $scope.auditShow = false;
            angular.forEach($scope.buttonPermissionList,function(item){
                if(item.code == 'piliangshenghe') {
                    $scope.auditShow = true;
                };
                if(item.code == 'shenpi') {
                    $scope.approval = true;
                };
                if(item.code == 'queryBySalesMan') {
                    $scope.queryBySalesMan = true;
                };
            });
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};

/* 获取当前路径*/
function getCurrentUrlPath(){
    var currentUrl = document.location.toString();
    var index = currentUrl.lastIndexOf("?");
    return currentUrl.substring(0, index);
}

//展示大图
function show ($scope,index,item){
    $scope.bigpic = item.imgList[index];
}


//图片类型查询
function getImgTypeList($scope,$http){
    Biostime.common.showLoading();
    var queryData = {
        "request":{
            // 'name':'',
            // 'status':''
        }
    };
    var url = BASICURL + "samanage/act/report/queryWebImgTypeList.action?"+$scope.token;
    // var url ="http://10.50.101.162:8080/samanage/act/report/queryAppImgTypeList.action?"+$scope.token;
    $http({
        method: 'POST',
        data:queryData,
        url: url
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            // console.log(data)
            $scope.imageMarkTypeList = data.response;
            var all = {'id':0,'name':'全部'};
            $scope.imageMarkTypeList.unshift(all);
            $scope.imageMarkType = $scope.imageMarkTypeList[0];
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}