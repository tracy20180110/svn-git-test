/*******************************************************************************
 * @jsname:activitySearch_controller.js
 * @author:WangDongping
 * @date:2017-1-4
 * @use:路演活动查询.js
 ******************************************************************************/
define(['angular'], function (angular) {
    'use strict';

    var templateCtrl1=function ($scope, $http, $route, $routeParams,$rootScope,ngDialog) {
        //获取token
        if(Biostime.common.getQueryString('token')) {
            $scope.token = "token=" + Biostime.common.getQueryString('token');
        }else{
            $scope.token ="userId="+Biostime.common.getQueryString('userid');
        };
        $scope.createdBy = '';
        
        //仅用于页面样式展示
        $scope.selFun = function(selNum) {
            $scope.selNum = selNum;
        };

        //获取用户当前页面下有效的菜单或按钮,添加权限
        $scope.addActivities = '';
        $scope.startUsing = '';
        $scope.stopUsing = '';
        $scope.edit = '';
        $scope.approval = '';

        //根据身份显示相应的大区和办事处、渠道
        identityVerification1($scope, $http)
        .then(function(){
            getSubordinateMenus($scope, $http);
        })
        .then(function(){
            
            //四级权限缓存
            sessionStorage.setItem('addActivities', $scope.addActivities);
            sessionStorage.setItem('startUsing', $scope.startUsing);
            sessionStorage.setItem('stopUsing', $scope.stopUsing);
            sessionStorage.setItem('edit', $scope.edit);
            sessionStorage.setItem('approval', $scope.approval);
        });

        //事业部修改，大区跟着改变
        $scope.buChange = function(){
            buChange($scope);
        };

        //事业部修改，大区跟着改变
        $scope.areaChangeEdit = function(){
            buChange2($scope);
        };

        //大区修改，办事处跟着改变
        $scope.areaChange = function(){
            getDepartments1($scope, $http);
        };
        
        //大区修改，办事处跟着改变---编辑
        $scope.areaChangeEdit = function(){
            getDepartmentsEdit($scope, $http);
        };
        
        //查询路演活动
        $scope.searchClick = function () {
            loadActivityList($scope, $http, 1);
        };

        //修改活动状态
        $scope.changeStatus = function ($event, tableItem, status) {
            if(confirm("确认修改活动状态？")) {
                changeActivityStatus($scope, $http, tableItem.id, status);
            }
        };

        //全品牌
        $scope.brandFun = function(item) {
            if(item.value == '') {
                if($scope.brandListEdit[0].brand==true) {
                    angular.forEach($scope.brandListEdit,function(brandListEdit){
                        brandListEdit.brand = true;
                    });
                    angular.forEach($scope.qrCodeList, function (qr){
                        qr.show = true;
                        qr.sel = true;
                    });
                    $scope.showQRCode = true;
                    $scope.showSKU = true;
                    $scope.productName = '';
                    if(Biostime.common.isEmpty($scope.productListCopy)){
                        //获取swisse产品信息
                        getSwisseProduct1($scope, $http).then(function (success) {
                            $scope.allClear();
                            getType1($scope, $http,0);
                            return;
                        },function(error){});
                    }
                    $scope.productList = angular.copy($scope.productListCopy);
                    $scope.allClear();
                }else{
                    angular.forEach($scope.brandListEdit,function(brandListEdit){
                        brandListEdit.brand = false;
                    });
                    $scope.showSKU = false;
                    angular.forEach($scope.qrCodeList,function (qr){
                        qr.show = false;
                        qr.sel = false;
                    });
                    $scope.showQRCode = false;
                };
            }else{
                if(item.brand == false) {
                    $scope.brandListEdit[0].brand = false;
                }
                if(item.value == '26509' && item.brand == true){
                    $scope.showSKU = true;
                    $scope.productName = '';
                    if(Biostime.common.isEmpty($scope.productListCopy)){
                        //获取swisse产品信息
                        getSwisseProduct1($scope, $http).then(function (success) {
                            $scope.allClear();
                            getType1($scope, $http,0);
                            return;
                        },function(error){});
                    }
                    $scope.productList = angular.copy($scope.productListCopy);
                    $scope.allClear();
                }else if(item.value == '26509' && item.brand == false){
                    $scope.showSKU = false;
                    $scope.allClear();
                }

                var num = 0;
                angular.forEach($scope.brandListEdit, function (brand){
                    if((brand.value == '21706' || brand.value == '33206') && brand.brand){
                        num++;
                    }
                })
                if(num>0){
                    $scope.showQRCode = true;
                }else{
                    $scope.showQRCode = false;
                }
                angular.forEach($scope.qrCodeList, function (qr){
                    if(qr.value == item.value){
                        if(item.brand){
                            qr.show = true;
                        }else{
                            qr.show = false;
                            qr.sel = false;
                        }
                    }
                })
            };
            getType1($scope, $http,0);
        };

        //获取活动类型
        $scope.getType = function(num){
            if(num==0){     //品牌改变，如是Swisse，显示参与SKU
                // if($scope.brandEdit.value == '26509'){
                //     $scope.showSKU = true;
                //     $scope.productName = '';
                //     if(Biostime.common.isEmpty($scope.productListCopy)){
                //         //获取swisse产品信息
                //         getSwisseProduct1($scope, $http).then(function (success) {
                //             $scope.allClear();
                //             getType1($scope, $http, 0);
                //             return;
                //         },function(error){});
                //     }
                //     $scope.productList = angular.copy($scope.productListCopy);
                //     $scope.allClear();
                // }else{
                //     $scope.showSKU = false;
                // }
                // getType1($scope, $http, 0);
            }else if(num==1){     //渠道改变，如有婴线，显示门店类型
                // $scope.showTerminal = false;
                // for(var i=0; i<$scope.terminalList.length; i++) {
                //     $scope.terminalList[i].terminal=false;
                // };
                // $scope.terminalTypeCode = "";
                // $scope.terminalTypeName = "";
                // for(var i=0; i < $scope.channelListEdit.length; i++){
                //     if($scope.channelListEdit[i].channel == true && $scope.channelListEdit[i].channelCode == '01') {
                //         $scope.showTerminal = true;
                //     }
                // }
                getType1($scope, $http, 0);
            }else if(num ==2){
                getType1($scope, $http, 0);
            }else if(num ==3){      //列表含【全部】
                getType1($scope, $http, 1);
            }

        };

        //搜索sku
        $scope.searchProduct = function (name) {
            $scope.productList = [];
            for(var i=0; i<$scope.productListCopy.length; i++){
                if($scope.productListCopy[i].name.match(name) != null){
                    $scope.productList.push($scope.productListCopy[i]);
                }
            }
            for(var i=0; i<$scope.productList.length; i++){
                for(var j=0; j<$scope.selectedList.length; j++){
                    if($scope.productList[i].code == $scope.selectedList[j].code){
                        $scope.productList[i].sel = true;
                    }
                }
            }
        };

        //全选参与sku
        $scope.toCheckProductAll = function(){
            $scope.selectedList = [];
            $scope.productAll = $scope.productAll ? false : true;
            angular.forEach($scope.productList,function(item,index){
                $scope.productList[index].sel = $scope.productAll;
                $scope.nameListSel(index);   
            });
        };

        //选择参与sku
        $scope.nameListSel = function(index){
            if($scope.productList[index].sel == true){
                $scope.selectedList.push($scope.productList[index]);
                if($scope.selectedList.length == $scope.productList.length){
                    $scope.productAll = true;
                }
            }else{
                for(var i=0; i<$scope.selectedList.length; i++){
                    if($scope.selectedList[i].code == $scope.productList[index].code){
                        $scope.selectedList.splice(i, 1);
                        $scope.productAll = false;
                    }
                }
            }
        };

        //删除已选中的内容
        $scope.delFun = function(item){
            for(var i=0; i<$scope.selectedList.length; i++){
                if($scope.selectedList[i].code == item.code){
                    $scope.selectedList.splice(i, 1);
                }
            }
            for(var j=0; j<$scope.productList.length; j++){
                if($scope.productList[j].code == item.code){
                    $scope.productList[j].sel = false;
                }
            }
            $scope.productAll = false;
        };

        //全部清空-名单类型
        $scope.allClear = function() {
            angular.forEach($scope.productList, function (item) {
                item.sel = false;
            });
            $scope.selectedList = [];
            $scope.productAll = false;
        };

        $scope.actId = "";
        //导出
        $scope.exportExcel = function () {
            //活动日期
            $scope.startDate = document.getElementById("startDate").value;
            $scope.endDate = document.getElementById("endDate").value;
            if($scope.endDate == undefined){
                $scope.endDate = "";
            }

            //大区办事处
            $scope.areaofficeCode = $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode;
            $scope.actId = $scope.actId == null ? "" : $scope.actId;
            window.location.href = BASICURL + 'samanage/actRoadShow/exportActRoadShow.action?brandCode='+$scope.brand.value + '&buCode=' + $scope.buId.buCode
                + '&areaofficeCode=' + $scope.areaofficeCode + '&channelCode=' + $scope.channel.channelCode
                + '&configActTypeCode=' + $scope.type.code + '&status=' + $scope.status.value
                + '&startDate=' + $scope.startDate + '&endDate=' + $scope.endDate + '&actId=' + $scope.actId + '&createdBy=' + $scope.createdBy + '&' + $scope.token;
        };
        
        //修改门店 弹出框
        $scope.showChange = function (id) {
            //开启窗口
            easyDialog.open({
                container: 'changeId'
            });
            $scope.exportActId = id;
        };

        //查看门店 弹出框
        $scope.showCheck = function (id) {
            //开启窗口
            easyDialog.open({
                container: 'checkId'
            });
            $scope.exportActId = id;
        };

        //编辑 弹出框
        $scope.showEdit = function (item) {
            // initial1($scope,$http);
            //考核类型
            $scope.assess = item.khTypeCode;
            //大区
            for(var i=0; i<$scope.areasEdit.length; i++){
                //全部
                if(item.areaName == null){
                    $scope.areaEdit = $scope.areasEdit[0];
                    break;
                }else if(item.areaName == $scope.areasEdit[i].areaName){
                    $scope.areaEdit = $scope.areasEdit[i];
                    break;
                }
            }
            //办事处
            for(var i=0; i<$scope.departmentsEdit.length; i++){
                //全部
                if(item.officeName == null||item.officeName == ''){
                    $scope.departmentEdit = $scope.departmentsEdit[0];
                    break;
                }else if(item.officeName == $scope.departmentsEdit[i].officeName){
                    $scope.departmentEdit = $scope.departmentsEdit[i];
                    break;
                }
            }
            //品牌
            if(item.brandCode!=''&&item.brandCode!=null){
                var brandTemp = item.brandCode.split(',');
                for(var j=0; j<brandTemp.length; j++){
                    if(brandTemp.indexOf('26509') >= 0 ){
                        $scope.showSKU = true;
                        $scope.productName = '';
                    }else{
                        $scope.showSKU = false;
                    }
                    for(var i=0; i<$scope.brandListEdit.length; i++){
                        if($scope.brandListEdit[i].value == brandTemp[j]){
                            $scope.brandListEdit[i].brand = true;
                        }
                    }
                    for(var k=0; k<$scope.qrCodeList.length; k++){
                        if($scope.qrCodeList[k].value == brandTemp[j]){
                            $scope.qrCodeList[k].show = true;
                        }
                    }
                }
            }

            //二维码
            if(!Biostime.common.isEmpty(item.qrCode) && $scope.assess=='1'){
                $scope.showQRCode = true;
                var qrList = item.qrCode.split(',');
                for(var i=0; i<qrList.length; i++){
                    for(var j=0; j<$scope.qrCodeList.length; j++){
                        if($scope.qrCodeList[j].value == qrList[i]){
                            $scope.qrCodeList[j].sel = true;
                            $scope.qrCodeList[j].show = true;
                        }
                    }
                }
            }

            //小程序
            if(item.signMiniCode == '有'){
                $scope.spType = $scope.spTypeList[0].value;
            }else{
                $scope.spType = $scope.spTypeList[1].value;
            }

            //参与sku
            if(item.swId != null){
                getSwisseProduct1($scope, $http, item.swId.split(','));
            }

            //渠道
            if(item.channelCode!=''&&item.channelCode!=null){
                var channelTemp = item.channelCode.split(',');
                for(var i=0; i<$scope.channelListEdit.length; i++){
                    $scope.channelListEdit[i].channel = false;
                    for(var j=0; j<channelTemp.length; j++){
                        if($scope.channelListEdit[i].channelCode == channelTemp[j]){
                            $scope.channelListEdit[i].channel = true;
                        }
                    }
                }
            }
            $scope.showTerminal = false;
            for(var i=0; i < $scope.channelListEdit.length; i++){
                if($scope.channelListEdit[i].channel == true && $scope.channelListEdit[i].channelCode == '01') {
                    $scope.showTerminal = true;
                }
            }
            //门店类型
            if($scope.showTerminal == true){
                var terminalTemp = item.configTerminalTypeCode.split(',');
                for(var i=0; i<$scope.terminalList.length; i++){
                    for(var j=0; j<terminalTemp.length; j++){
                        if($scope.terminalList[i].value == terminalTemp[j]){
                            $scope.terminalList[i].terminal = true;
                        }
                    }
                }
            }
            //活动类型
            getType1($scope, $http, 0).then(function (success) {
                for(var i=0; i<$scope.typeListEdit.length; i++){
                    if(item.configActTypeCode == $scope.typeListEdit[i].code){
                        $scope.typeEdit = $scope.typeListEdit[i];
                    }
                }
            },function(error){});

            //活动名称
            $scope.actNameEdit = item.actName;
            //活动日期
            document.getElementById("startDateEdit").value = item.startDate;
            document.getElementById("endDateEdit").value = item.endDate;

            //图片上传类型
            if(item.actReportType == '0'){
                $scope.imgType = $scope.imgTypeList[0].value;
                $scope.imgTypeList[0].isSelect = true;
                $scope.imgTypeValue = $scope.imgTypeList[0].value;
                $scope.imgTypeName = $scope.imgTypeList[0].name;
            }else{
                $scope.imgType = $scope.imgTypeList[1].value;
                $scope.imgTypeList[1].isSelect = true;
                $scope.imgTypeValue = $scope.imgTypeList[1].value;
                $scope.imgTypeName = $scope.imgTypeList[1].name;
            }
            
            //开启窗口
            easyDialog.open({
                container: 'editId'
            });
            $scope.editId = item.id;
        };

        //改变适用上报类型
        $scope.typeFn = function (val){
            // console.log($scope.assess);
            if(val == 2){
                //是否需要小程序码
                $scope.spType = $scope.spTypeList[1].value;
                return
            }else{
                angular.forEach($scope.brandListEdit, function (item,index){
                    if(item.value == '0'){
                        item.brand = false;
                    }
                })
            }
            angular.forEach($scope.qrList, function (item){
                item.sel = false;
            })
        };
        
        //关闭 弹出框
        $scope.close = function() {
            easyDialog.close();
        };

        //保存
        $scope.saveClick = function () {
            editActivity($scope, $http);
        };
        //取消 -- 清空所有填写内容
        $scope.cancel = function () {
            if(confirm("确认取消？取消后数据将被清空")){
                initial1($scope,$http);
            }
        };

        //查看门店&&修改门店  导出
        $scope.exportTerminalExcel = function () {
            window.location.href = BASICURL + 'samanage/actRoadShow/exportActRoadShowTerminal.action?actId='+$scope.exportActId + '&' + $scope.token;
            easyDialog.close();
        };

        //修改门店--导入文件
        $scope.uploadFile = function (files) {
            var r = confirm("确认要导入门店吗？");
            if(r == false){
                return;
            }
            var channelCount = 0;
            //渠道
            for(var i=0; i<$scope.channelList.length; i++){
                if(channelCount == 0){
                    $scope.importChannelCode = $scope.channelList[i].channelCode;
                }else{
                    $scope.importChannelCode = $scope.importChannelCode + ',' + $scope.channelList[i].channelCode;
                }
                channelCount++;
            }
            Biostime.common.showLoading();
            $scope.changeDisabled = true;    //按钮灰掉
            var url = BASICURL + "samanage/actRoadShow/importActRoadShowTerminal.action" + '?' + $scope.token;
            var fd = new FormData();
            fd.append('file', files.files[0]);
            fd.append('actId', $scope.exportActId);
            fd.append('userId', $scope.userId);
            fd.append('createdName', $scope.userName);
            fd.append('areaOfficeCode', $scope.areaOfficeCode);
            fd.append('channelCode', $scope.importChannelCode);

            $http.post(url, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                Biostime.common.hideLoading();
                if(data.code != 100) {
                    alert(JSON.stringify('文件导入失败：'+files.files[0].name+'，'+data.desc.replace(/null/, '请重新上传')));
                }else{
                    alert(JSON.stringify('文件导入成功：'+files.files[0].name));
                }

            }).error(function (data) {
                Biostime.common.hideLoading();
                Biostime.common.toastMessage('服务器错误，文件导入失败！');
            });
            $scope.changeDisabled = false;    //按钮恢复
            easyDialog.close();
        };
        //初始化
        //品牌（1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie）
        $scope.brandList = [
            {'name':"全部",'value':""},
            {'name':"合生元",'value':"1"},
            {'name':"素加",'value':"2"},
            {'name':"葆艾",'value':"3"},
            {'name':"HT",'value':"21706"},
            {'name':"合生元可贝思",'value':"36706"},
            {'name':"SWISSE",'value':"26509"},
            {'name':"Dodie",'value':"33206"},
            {'name':"其他",'value':"0"}
        ];
        $scope.brand = $scope.brandList[0];
        //品牌---编辑
        //品牌（1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie）
        $scope.brandListEdit = [
            {'name':"全部",'value':""},
            {'name':"合生元",'value':"1"},
            // {'name':"素加",'value':"2"},
            // {'name':"葆艾",'value':"3"},
            {'name':"HT",'value':"21706"},
            {'name':"合生元可贝思",'value':"36706"},
            {'name':"SWISSE",'value':"26509"},
            {'name':"Dodie",'value':"33206"},
            {'name':'其他','value':'0'}
        ];

        //活动状态（活动状态 1-启用 2-停用3-已结束 4-未开启）
        $scope.statusList = [
            {'name':"全部",'value':""},
            {'name':"启用",'value':"1"},
            {'name':"停用",'value':"2"},
            {'name':"已过期",'value':"3"},
            {'name':"未开启",'value':"4"}
        ];
        $scope.status = $scope.statusList[0];

        //是否申请官微二维码
        $scope.qrCodeList = [
            {'name':"HT",'value':"21706"},
            {'name':"Dodie",'value':"33206"}
        ];

        //考核类型：路演活动，物料陈列
        $scope.assessList = [
            {'name': "活动上报", 'value': "1"},
            {'name': "陈列上报", 'value': "2"}
        ];

        //门店类型 1：区域重点连锁,2：其他,3:婴KA
        $scope.terminalList = [
            {'name':"全国重点连锁",'value':"3"},
            {'name':"大区重点连锁",'value':"1"},
            {'name':"办事处重点连锁",'value':"4"},
            {'name':"其他",'value':"2"}
        ];

        //图片上传类型
        $scope.imgTypeList = [
            {'name':"否",'value':"0"},
            {'name':"是",'value':"1"}
        ];
        // $scope.imgType = $scope.imgTypeList[0].value;
        
        //签到小程序码
        $scope.spTypeList = [
            {'name':"需要",'value':"1"},
            {'name':"不需要",'value':"0"}
        ];
        // $scope.spType = $scope.spTypeList[1].value;

        // 分页
        $scope.pagination = {};
        $scope.pagination.displayPageNum = 4;
        $scope.pagination.onFirstPage = function() {
            $scope.pagination.currentPage = 1;
            loadActivityList($scope, $http, 1);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-页数
        $scope.pagination.onGoPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadActivityList($scope, $http, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-上一页
        $scope.pagination.onUPPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadActivityList($scope, $http, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-下一页
        $scope.pagination.onNextPage = function() {
            if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
                $scope.pagination.currentPage++;
                loadActivityList($scope, $http,
                    $scope.pagination.currentPage);
                $scope.pagination.pageNums = $scope.pagination
                    .getPageNums();
            }
        };
        //账号管理-跳转页
        $scope.pagination.JumpPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadActivityList($scope, $http, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-获取页码数据
        $scope.pagination.getPageNums = function() {
            var tempPageNums = [];
            var startPage = 1;
            var endPage = 4;
            if ($scope.pagination.pageCount < 4) {
                endPage = $scope.pagination.pageCount;
            }
            if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
                if ($scope.pagination.currentPage < 3) {
                    startPage = 1;
                } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                    startPage = $scope.pagination.pageCount
                        - $scope.pagination.displayPageNum + 1;
                } else {
                    startPage = $scope.pagination.currentPage - 2;
                }
                endPage = startPage
                    + $scope.pagination.displayPageNum - 1;
                if (endPage > $scope.pagination.pageCount)
                    endPage = $scope.pagination.pageCount;
            }

            for ( var i = startPage; i <= endPage; i++) {
                tempPageNums.push(i);
            }
            return tempPageNums;
        };
        $scope.pagination.currentPage = 1;

    };

    return {
        templateCtrl1: templateCtrl1
    }
});

//根据身份，显示相应的大区和办事处、渠道,获取登录人信息
function identityVerification1($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        $scope.buList = data.loadUserBuBeanList;
        $scope.buListEdit = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;

        //初始化编辑
        $scope.channelListEdit=angular.copy($scope.channelList);
        for(var m=0; m<$scope.channelListEdit.length; m++){
            if($scope.channelListEdit[m].channelCode == "08"){
                $scope.channelListEdit.splice(m,1);
            }
        }

        if($scope.channelList.length > 1){
            var channelAll={
                channelCode:'',
                channelName:"全部"
            };
            $scope.channelList.splice(0,0,channelAll);
        }
        $scope.channel = $scope.channelList[0];

        // for(var i=0; i<$scope.imgTypeList.length; i++){
        //     if($scope.imgTypeList[i].value == "1"){
        //         $scope.imgTypeList[i].isSelect = true;
        //         $scope.imgTypeValue = $scope.imgTypeList[i].value;
        //         $scope.imgTypeName = $scope.imgTypeList[i].name;
        //         break;
        //     }
        // }

        //根据渠道 显示活动类型
        getType1($scope,$http,1);

        //获取登录人信息
        $scope.userId = data.loadUserBaseInfoBean.userId;
        $scope.userName = data.loadUserBaseInfoBean.userName;
        if(data.loadUserBaseInfoBean.officeCode == null && data.loadUserBaseInfoBean.areaCode == null){
            $scope.areaOfficeCode = "";
        }else if(data.loadUserBaseInfoBean.officeCode == null && data.loadUserBaseInfoBean.areaCode != null){
            $scope.areaOfficeCode = data.loadUserBaseInfoBean.areaCode;
        }else if(data.loadUserBaseInfoBean.officeCode != null){
            $scope.areaOfficeCode = data.loadUserBaseInfoBean.officeCode;
        }

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = angular.copy(data.loadUserAreaBeanList);
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };
        $scope.buIdEdit = $scope.buListEdit[0];
        $scope.areaAll2 = angular.copy(data.loadUserAreaBeanList);
        if($scope.buListEdit.length > 1){
            $scope.buDisabledEdit=false;
        }else{
            $scope.buDisabledEdit=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
            $scope.areaEditDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.areaAll2.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
            $scope.departmentEditDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
            $scope.departmentEditDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);
        //初始化编辑
        // $scope.areasEdit=angular.copy($scope.areas);
        $scope.departmentsEdit=angular.copy($scope.departments);
        buChange2($scope);

        //针对页面埋点
        var date = new Date();
        var has = md5('6'+'6070100'+date.getTime()+$scope.userId);
        var dataMd = {
            "platform":6, //0:mama100Wechat，1:mama100app，2:SwisseWechat，HYTapp，3:HYTwechat，4:DodieWechat，5:HealthyTimesWechat，6:PC
            "point_code":"6070100",  //具体详情见主题类型编码表
            "created_time": date.getTime(),
            "customer_id":$scope.userId,  //用户会员的ID标识
            "sign":has
        };
        eventpv($scope,$http,dataMd);
        
    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange2($scope) {
    $scope.areasEdit=[];
    angular.forEach($scope.areaAll2,function(item){
        if(Biostime.common.isEmpty($scope.buIdEdit.buId)){
            $scope.areasEdit.push(item);
        }else if($scope.buIdEdit.buId==item.buId||item.areaId==''){
            $scope.areasEdit.push(item);
        };
    });
    $scope.areaEdit=$scope.areasEdit[0];
    if(!$scope.areaEditDisabled){
        $scope.departmentEditDisabled=true;
    }
    $scope.departmentEdit = $scope.departmentsEdit[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments1($scope, $http){
    $scope.departmentDisabled=false;
    if($scope.area.areaCode != ""){
        $scope.departments=[{
            areaId:'',
            officeId:'',
            officeCode:'',
            officeName:"全部"
        }];
    }else{
        $scope.departmentDisabled=true;
    }
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}
/***
 * 根据选择大区刷选办事处---编辑
 *
 * */
function getDepartmentsEdit($scope, $http){
    $scope.departmentEditDisabled=false;
    $scope.departmentsEdit=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.areaEdit.areaId==$scope.officeAll[i].areaId){
            $scope.departmentsEdit.push($scope.officeAll[i]);
        }
    }
    $scope.departmentEdit=$scope.departmentsEdit[0];
}
/**
 * 查询路演活动
 * @param $scope
 * @param $http
 * @param pageNum
 */
function loadActivityList($scope, $http, pageNum) {
    $scope.startDate = document.getElementById("startDate").value;
    $scope.endDate = document.getElementById("endDate").value;

    Biostime.common.showLoading();
    var queryData = {'request':{
        "buCode":$scope.buId.buCode,                          
        "areaCode":$scope.area.areaId,                          //大区（ID）
        "officeCode":$scope.department.officeId,              //办事处（ID）
        // "areaofficeCode": $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode,  //大区办事处编码
        "brandCode": $scope.brand.value,                        //品牌
        "channelCode": $scope.channel.channelCode,              //渠道
        "configActTypeCode": $scope.type.code,                  //活动类型
        "actId":$scope.actId,                                   //活动ID
        "status":$scope.status.value,                           //活动状态
        "startDate":$scope.startDate,                           //活动日期起
        "endDate":$scope.endDate,                               //活动日期止
        "createdBy":$scope.createdBy,                           //创建人工号
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + "samanage/actRoadShow/queryActRoadShowList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = angular.copy(data.response.datas);
                if($scope.userId!=''&&$scope.userId!=null&&$scope.userId!=undefined){
                    angular.forEach($scope.infoList,function(infoList){
                        infoList.operateShow = false;
                        if($scope.userId==infoList.createdBy){
                            infoList.operateShow = true;
                        };
                    });
                }
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 修改活动状态
 * @param $scope
 * @param $http
 * @param id
 * @param status  改变后的状态
 */
function changeActivityStatus($scope, $http, id, status){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "id": id,                                               //活动id
        "status": status,                                       //改变后的状态
        "userId": $scope.userId,                                //登录人id
        "userName": $scope.userName                             //登录人姓名
    }};
    var url = BASICURL + "samanage/actRoadShow/changeStatusActRoadShow.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            Biostime.common.toastMessage("修改状态成功");
            loadActivityList($scope, $http, 1);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 编辑活动
 * @param $scope
 * @param $http
 */
function editActivity($scope, $http) {
    $scope.startDateEdit = document.getElementById("startDateEdit").value;
    $scope.endDateEdit = document.getElementById("endDateEdit").value;
    
    //参与sku
    if($scope.showSKU == true) {
        $scope.swisseId = [];
        for (var j = 0; j < $scope.selectedList.length; j++) {
            $scope.swisseId.push($scope.selectedList[j].code);
        }
    }

    //是否申请二维码
    var qrCodeList = [];
    if($scope.showQRCode){
        angular.forEach($scope.qrCodeList, function (item){
            if(item.show && item.sel){
                var qr = {'key':item.value,'value':item.name};
                qrCodeList.push(qr);
            }
        })
    }

    if(!checkForm1($scope)){
        return;
    }
    Biostime.common.showLoading();
    var queryData;
    if($scope.showSKU == true){
        queryData = {'request':{
            "id":$scope.editId,                                 //活动id
            "userId":$scope.userId,                             //登录人id
            "userName":$scope.userName,                         //登录人姓名
            "khType":$scope.assess,                             //考核类型
            "areaOfficeCode": $scope.departmentEdit.officeCode != "" ? $scope.departmentEdit.officeCode : $scope.areaEdit.areaCode != "" ? $scope.areaEdit.areaCode : $scope.buIdEdit.buCode,  //大区办事处编码
            "swisseId": $scope.swisseId,                        //参与SKU
            "actConfigId": $scope.typeEdit.code,                    //活动类型
            "actName":$scope.actNameEdit,                           //活动名称
            "startDate":$scope.startDateEdit,                       //活动日期起
            "endDate":$scope.endDateEdit,                           //活动日期止
            "brandCode":$scope.saveBrandCode,                     //品牌编码
            "qrCodeList":qrCodeList,                            //是否申请官微二维码
            "channelCode":$scope.channelCodeEdit,                   //渠道编码
            "channelName":$scope.channelNameEdit,                   //渠道名称
            "terminalTypeCode": $scope.terminalTypeCode,            //门店类型编码
            "terminalTypeName": $scope.terminalTypeName,             //门店类型名称
            "signMiniCode": $scope.spType,             //是否需要签到小程序码
            "actReportType":$scope.imgType                        //是否选着图片上传
        }};
    }else{
        queryData = {'request':{
            "id":$scope.editId,                                 //活动id
            "userId":$scope.userId,                             //登录人id
            "userName":$scope.userName,                         //登录人姓名
            "khType":$scope.assess,                             //考核类型
            "areaOfficeCode": $scope.departmentEdit.officeCode != "" ? $scope.departmentEdit.officeCode : $scope.areaEdit.areaCode != "" ? $scope.areaEdit.areaCode : $scope.buIdEdit.buCode,  //大区办事处编码
            "actConfigId": $scope.typeEdit.code,                    //活动类型
            "actName":$scope.actNameEdit,                           //活动名称
            "startDate":$scope.startDateEdit,                       //活动日期起
            "endDate":$scope.endDateEdit,                            //活动日期止
            "brandCode":$scope.saveBrandCode,                     //品牌编码
            "qrCodeList":qrCodeList,                            //是否申请官微二维码
            "channelCode":$scope.channelCodeEdit,                   //渠道编码
            "channelName":$scope.channelNameEdit,                   //渠道名称
            "terminalTypeCode": $scope.terminalTypeCode,            //门店类型编码
            "terminalTypeName": $scope.terminalTypeName,             //门店类型名称
            "signMiniCode": $scope.spType,             //是否需要签到小程序码
            "actReportType":$scope.imgType                        //是否选着图片上传
        }};
    }
    var url = BASICURL + "samanage/actRoadShow/saveActRoadShow.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            alert("保存成功");
            easyDialog.close();
            // loadActivityList($scope, $http, 1);
            selFun('#view1/1',0);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 检查表单
 * @param $scope
 * @returns {boolean}
 */
function checkForm1($scope){
    if($scope.showSKU == true && $scope.selectedList.length == 0){
        Biostime.common.toastMessage("品牌选择Swisse时，请选择参与SKU");
        return false;
    }
    $scope.channelTrue = false;
    angular.forEach($scope.channelListEdit,function(channelListEdit) {
        if(channelListEdit.channel == true) {
            $scope.channelTrue = true;
        }
    });
    if($scope.channelTrue == false) {
        Biostime.common.toastMessage("渠道不能为空，请检查");
        return false;
    }
    // if($scope.showTerminal == true){
    //     $scope.terminalTrue = false;
    //     angular.forEach($scope.terminalList,function(terminalList) {
    //         if(terminalList.terminal == true) {
    //             $scope.terminalTrue = true;
    //         }
    //     });
    //     if($scope.terminalTrue == false) {
    //         Biostime.common.toastMessage("门店类型不能为空，请检查");
    //         return false;
    //     }
    // }
    if($scope.typeListEdit.length==0){
        Biostime.common.toastMessage("请在关系维护页面增加【活动类型】，再编辑活动");
        return false;
    }
    if(Biostime.common.isEmpty($scope.actNameEdit)){
        Biostime.common.toastMessage("活动名称不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.startDateEdit)){
        Biostime.common.toastMessage("活动日期开始时间不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.endDateEdit)){
        Biostime.common.toastMessage("活动日期结束时间不能为空，请检查");
        return false;
    }
    return true;
}
//初始化 品牌、活动类型、活动日期的开始时间
function initial1($scope, $http){
    $scope.departmentsEdit = $scope.departments;
    $scope.areasEdit = $scope.areas;
    $scope.departmentEdit=$scope.departmentsEdit[0];
    $scope.areaEdit=$scope.areasEdit[0];
    // getDepartments1($scope, $http);
    
    //是否需要小程序码
    $scope.spType = $scope.spTypeList[1].value;

    //图片类型
    for(var i=0; i<$scope.imgTypeList.length; i++){
        if($scope.imgTypeList[i].value == "1"){
            $scope.imgTypeList[i].isSelect = true;
            $scope.imgTypeValue = $scope.imgTypeList[i].value;
            $scope.imgTypeName = $scope.imgTypeList[i].name;
            break;
        }
    }

    //渠道
    angular.forEach($scope.channelListEdit,function(channelListEdit){
        channelListEdit.channel = false;
    });
    //渠道下拉框  初始化
    // $scope.channel = $scope.channelListEdit[0];
    // for(var i=0; i<$scope.channelListEdit.length; i++){
    //     if($scope.channelListEdit[i].channelCode == "01"){
    //         $scope.channelListEdit[i].channel = true;
    //         break;
    //     }
    // }

    //初始化  考核类型、品牌、活动类型、活动日期的开始时间
    $scope.assess = $scope.assessList[0].value;
    
    $scope.brandAll = false;
    for(var i=0; i<$scope.brandListEdit.length; i++) {
        $scope.brandListEdit[i].brand=false; 
        if($scope.brandListEdit[i].value == '26509'){
            $scope.showSKU = true;
            $scope.productName = '';
            $scope.allClear();
        }else{
            $scope.showSKU = false;
            $scope.allClear();
        }
    }
    $scope.brandCode = '';
    $scope.brandName = '';
    $scope.saveBrandCode = [];
    $scope.saveBrandName = [];

    $scope.showQRCode = false;
    for(var i=0; i<$scope.qrCodeList.length; i++) {
        $scope.qrCodeList[i].show=false; 
        $scope.qrCodeList[i].sel=false; 
    }

    $scope.showTerminal = false;
    for(var i=0; i < $scope.channelListEdit.length; i++){
        if($scope.channelListEdit[i].channel == true && $scope.channelListEdit[i].channelCode == '01') {
            $scope.showTerminal = true;
        }
    }
    for(var i=0; i<$scope.terminalList.length; i++) {
        $scope.terminalList[i].terminal=false;
    }

    getType1($scope, $http, 0).then(function(){
        $scope.typeEdit = $scope.typeListEdit[0];
    });
    document.getElementById("startDateEdit").value = new Date().format("yyyy-MM-dd");

    $scope.actNameEdit = "";
    document.getElementById("endDateEdit").value = "";
}
/**
 * 检查是否是整数
 */
function keyPress() {
    var keyCode = event.keyCode;
    if (keyCode >= 48 && keyCode <= 57) {
        event.returnValue = true;
    }else {
        event.returnValue = false;
    }
}
/**
 * 获取活动类型
 * @param $scope
 * @param $http
 * @param isAll  1:(查询条件)，0：（编辑）
 */
function getType1($scope, $http, isAll){
    Biostime.common.showLoading();

    var queryData;
    if(isAll == 1){
        queryData = {'request':{
            "brandCode":$scope.brand.value,                           //品牌编码
            "channelCode":$scope.channel.channelCode,                 //渠道编码
            "type": 1                                                 //1:门店类型；2.swisse列表
        }};
    }else if(isAll == 0){
        //考核类型
        var assessType;
        if($scope.assess == '1'){
            assessType = '1';
        }else{
            assessType = '2';
        };
        //品牌
        $scope.brandCode = "";
        $scope.brandName = "";
        $scope.saveBrandCode = [];
        $scope.saveBrandName = [];
        for(var i=1; i < $scope.brandListEdit.length; i++){
            if($scope.brandListEdit[i].brand == true) {
                $scope.brandCode += $scope.brandListEdit[i].value + ",";
                $scope.brandName += $scope.brandListEdit[i].name + ",";
                $scope.saveBrandCode.push($scope.brandListEdit[i].value);
                $scope.saveBrandName.push($scope.brandListEdit[i].name);
            }
        };
        $scope.brandCode = $scope.brandCode.substring(0,$scope.brandCode.length-1);
        $scope.brandName = $scope.brandName.substring(0,$scope.brandName.length-1);
        if($scope.saveBrandCode.length == $scope.brandListEdit.length-1){
            $scope.brandListEdit[0].brand = true;
        };

        //渠道
        $scope.channelCodeEdit = "";
        $scope.channelNameEdit = "";
        for(var i=0; i < $scope.channelListEdit.length; i++){
            if($scope.channelListEdit[i].channel == true) {
                $scope.channelCodeEdit += $scope.channelListEdit[i].channelCode + ",";
                $scope.channelNameEdit += $scope.channelListEdit[i].channelName + ",";
            }
        }
        $scope.channelCodeEdit = $scope.channelCodeEdit.substring(0,$scope.channelCodeEdit.length-1);
        $scope.channelNameEdit = $scope.channelNameEdit.substring(0,$scope.channelNameEdit.length-1);

        //适用门店
        $scope.terminalTypeCode = "";
        $scope.terminalTypeName = "";
        for(var m=0; m < $scope.terminalList.length; m++){
            if($scope.terminalList[m].terminal == true) {
                $scope.terminalTypeCode += $scope.terminalList[m].value + ",";
                $scope.terminalTypeName += $scope.terminalList[m].name + ",";
            }
        }
        $scope.terminalTypeCode = $scope.terminalTypeCode.substring(0,$scope.terminalTypeCode.length-1);
        $scope.terminalTypeName = $scope.terminalTypeName.substring(0,$scope.terminalTypeName.length-1);

        queryData = {'request':{
            "brandCode":$scope.brandCode,                           //品牌编码
            "channelCode":$scope.channelCodeEdit,                         //渠道编码
            "terminalTypeCode": $scope.terminalTypeCode,                  //门店类型编码
            "type": 1,                                                     //1:门店类型；2.swisse列表
            "reportTypeCode":assessType
        }};
    }

    var url = BASICURL + "samanage/actRoadShow/querySwActTypeList.action?"+$scope.token;
    return $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if(isAll == 0){
                $scope.typeListEdit = angular.copy(data.response);
                if($scope.typeListEdit.length==0){
                    alert("请在关系维护页面增加【活动类型】，再编辑活动");
                }
                // $scope.typeEdit = $scope.typeListEdit[0];
            }else if(isAll == 1){
                $scope.typeList = angular.copy(data.response);
                var typeAll={
                    code:'',
                    name:"全部"
                };
                $scope.typeList.splice(0,0,typeAll);
                $scope.type = $scope.typeList[0];
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 获取Swisse产品名称
 * @param $scope
 * @param $http
 */
function getSwisseProduct1($scope, $http, swId){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "type": 2                                   //1:门店类型；2.swisse列表
    }};

    var url = BASICURL + "samanage/actRoadShow/querySwActTypeList.action?"+$scope.token;
    return $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            $scope.selectedList = [];
            $scope.productList = angular.copy(data.response);
            $scope.productListCopy = angular.copy(data.response);
            if(swId){
                for(var i=0; i<$scope.productList.length; i++){
                    for(var j=0; j<swId.length; j++){
                        if($scope.productList[i].code == swId[j]){
                            $scope.productList[i].sel = true;
                            $scope.selectedList.push($scope.productList[i]);
                        }
                    }
                }
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}


/**
 * 获取用户当前页面下有效的菜单或按钮
 */
function getSubordinateMenus($scope, $http) {
    // $scope.token = Biostime.common.getQueryString("token");
    var token = Biostime.common.getQueryString("token");
    var urlPath = getCurrentUrlPath();
    var queryData = {
        "seqNo": new Date().getTime(),
        "sourceSystem": "merchant",
        "request": {
            "userId": $scope.userId,  // 必要参数,但可以值为空
            "token": token, // 必要参数
            "url": urlPath // 必要参数
        }
    };
    var url = useId + 'mkt-common/baseInfo/getSubordinateMenus.action' + '?' + $scope.token;
    return $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            $scope.buttonPermissionList = data.response;
            angular.forEach($scope.buttonPermissionList,function(buttonPermissionList){
                //新增活动
                if(buttonPermissionList.code=="xinzenghuodong"){
                    $scope.addActivities = '1';
                };   
                //启用
                if(buttonPermissionList.code=="qiyong"){
                    $scope.startUsing = '1';
                };
                //停用
                if(buttonPermissionList.code=="tingyong"){
                    $scope.stopUsing = '1';
                };
                //编辑
                if(buttonPermissionList.code=='bianji'){
                    $scope.edit = '1';
                };
                //审批
                if(buttonPermissionList.code=="shenpi"){
                    $scope.approval = '1';
                };

                if(buttonPermissionList.code == 'shangbaopeizhi') {
                    $scope.imgTypeShow = true;
                };
            });
            if($scope.addActivities == '1'){
                $('.xinzenghuodong').css('display','block');
            }else{
                $('.xinzenghuodong').css('display','none');
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};

/* 获取当前路径*/
function getCurrentUrlPath(){
    var currentUrl = document.location.toString();
    var index = currentUrl.lastIndexOf("?");
    return currentUrl.substring(0, index);
}