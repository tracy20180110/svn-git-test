/*******************************************************************************
 * @jsname:addActivity_controller.js
 * @author:WangDongping
 * @date:2017-1-3
 * @use:新增活动.js
 ******************************************************************************/
define(['angular'], function (angular) {
    'use strict';

    var templateCtrl2=function ($scope, $http, $route, $routeParams,$rootScope,ngDialog) {
        //获取token
        if(Biostime.common.getQueryString('token')) {
            $scope.token = "token=" + Biostime.common.getQueryString('token');
        }else{
            $scope.token ="userId="+Biostime.common.getQueryString('userid');
        }
       
       //初始化  品牌、活动类型、活动日期的开始时间
        //品牌（1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie，36706：合生元可贝思）
        $scope.brandList = [
            {'name':"全部",'value':""},
            {'name':"合生元",'value':"1"},
            //{'name':"素加",'value':"2"},
            //{'name':"葆艾",'value':"3"},
            {'name':"HT",'value':"21706"},
            {'name':"合生元可贝思",'value':"36706"},
            {'name':"SWISSE",'value':"26509"},
            {'name':"Dodie",'value':"33206"},
            // {'name':'其他','value':'0'}
        ];
        // $scope.brand = $scope.brandList[0];

        //是否申请官微二维码
        $scope.qrCodeList = [
            {'name':"HT",'value':"21706"},
            {'name':"Dodie",'value':"33206"}
        ];

        //考核类型：路演活动，物料陈列
        $scope.assessList = [
            {'name': "活动上报", 'value': "1"},
            {'name': "陈列上报", 'value': "2"}
        ];
        $scope.assess = $scope.assessList[0].value;

        //门店类型 1：区域重点连锁,2：其他,3:婴KA
        $scope.terminalList = [
            {'name':"全国重点连锁",'value':"3"},
            {'name':"大区重点连锁",'value':"1"},
            {'name':"办事处重点连锁",'value':"4"},
            {'name':"其他",'value':"2"}
        ];

        //图片上传类型
        $scope.imgTypeList = [
            {'name':"否",'value':"0"},
            {'name':"是",'value':"1"}
        ];
        $scope.imgType = $scope.imgTypeList[0].value;

        //签到小程序码
        $scope.spTypeList = [
            {'name':"需要",'value':"1"},
            {'name':"不需要",'value':"0"}
        ];
        $scope.spType = $scope.spTypeList[1].value;
        
        //根据身份显示相应的大区和办事处、渠道
        identityVerification2($scope, $http)
        .then(function(){
            getSubordinateMenus($scope, $http);
        })
        .then(function(){
            //针对页面埋点
            var date = new Date();
            var has = md5('6'+'6070101'+date.getTime()+$scope.userId);
            var dataMd = {
                "platform":6, //0:mama100Wechat，1:mama100app，2:SwisseWechat，HYTapp，3:HYTwechat，4:DodieWechat，5:HealthyTimesWechat，6:PC
                "point_code":"6070101",  //具体详情见主题类型编码表
                "created_time": date.getTime(),
                "customer_id":$scope.userId,  //用户会员的ID标识
                "sign":has
            };
            eventpv($scope,$http,dataMd);
        });

        //活动日期 开始时间默认当天日期
        document.getElementById("startDate").value = new Date().format("yyyy-MM-dd");

        //仅用于页面样式展示
        $scope.selFun = function(selNum) {
            $scope.selNum = selNum;
        };

        //根据考核类型改变活动类型
        $scope.typeFn = function (val){
            // console.log($scope.assess);
            initial($scope,$http);
            if(val == 2){
                $scope.brandList.push({'name':'其他','value':'0'});
                return
            }else{
                angular.forEach($scope.brandList, function (item,index){
                    if(item.value == '0'){
                        // item.brand = false;
                        $scope.brandList.splice(index,1);
                    }
                })
            }
        };

        //事业部修改，大区跟着改变
        $scope.buChange = function(){
            buChange($scope);
        };
        
        //大区修改，办事处跟着改变
        $scope.areaChange = function(){
            getDepartments2($scope, $http);
        };

        //全品牌
        $scope.brandFun = function(item) {
            if(item.value == '') {
                if($scope.brandList[0].brand==true) {
                    angular.forEach($scope.brandList,function(brandList){
                        brandList.brand = true;

                    });
                    angular.forEach($scope.qrCodeList, function (qr){
                        qr.show = true;
                        qr.sel = true;
                    });
                    $scope.showQRCode = true;
                    $scope.showSKU = true;
                    $scope.productName = '';
                    if(Biostime.common.isEmpty($scope.productListCopy)){
                        //获取swisse产品信息
                        getSwisseProduct2($scope, $http).then(function (success) {
                            $scope.allClear();
                            getType2($scope, $http);
                            return;
                        },function(error){});
                    }
                    $scope.productList = angular.copy($scope.productListCopy);
                    $scope.allClear();
                }else{
                    angular.forEach($scope.brandList,function(brandList){
                        brandList.brand = false;
                    });
                    $scope.showSKU = false;
                    angular.forEach($scope.qrCodeList,function (qr){
                        qr.show = false;
                        qr.sel = false;
                    });
                    $scope.showQRCode = false;
                };
            }else{
                if(item.brand == false) {
                    $scope.brandList[0].brand = false;
                }
                if(item.value == '26509'){
                    if(item.brand){
                        $scope.showSKU = true;
                        $scope.productName = '';
                        if(Biostime.common.isEmpty($scope.productListCopy)){
                            //获取swisse产品信息
                            getSwisseProduct2($scope, $http).then(function (success) {
                                $scope.allClear();
                                getType2($scope, $http);
                                return;
                            },function(error){});
                        }
                        $scope.productList = angular.copy($scope.productListCopy);
                        $scope.allClear();
                    }else{
                        $scope.showSKU = false;
                        $scope.allClear();
                    }
                }
                
                var num = 0;
                angular.forEach($scope.brandList, function (brand){
                    if((brand.value == '21706' || brand.value == '33206') && brand.brand){
                        num++;
                    }
                })
                if(num>0){
                    $scope.showQRCode = true;
                }else{
                    $scope.showQRCode = false;
                }
                angular.forEach($scope.qrCodeList, function (qr){
                    if(qr.value == item.value){
                        if(item.brand){
                            qr.show = true;
                        }else{
                            qr.show = false;
                            qr.sel = false;
                        }
                    }
                })
            };
            getType2($scope, $http);
        };

        //获取活动类型
        $scope.getType = function(num){//品牌改变，如是Swisse，显示参与SKU
            if(num==0){  
                // for(var i=0; i<$scope.brandList.length; i++) {
                //     // $scope.brandList[i].brand=false;
                //     if($scope.brandList[i].value == '26509' && $scope.brandList[i].brand == true){
                //         $scope.showSKU = true;
                //         $scope.productName = '';
                //         if(Biostime.common.isEmpty($scope.productListCopy)){
                //             //获取swisse产品信息
                //             getSwisseProduct2($scope, $http).then(function (success) {
                //                 $scope.allClear();
                //                 getType2($scope, $http);
                //                 return;
                //             },function(error){});
                //         }
                //         $scope.productList = angular.copy($scope.productListCopy);
                //         $scope.allClear();
                //     }else{
                //         $scope.showSKU = false;
                //     }
                // };
            }else if(num==1){     //渠道改变，如有婴线，显示门店类型
                // $scope.showTerminal = false;
                // for(var i=0; i<$scope.terminalList.length; i++) {
                //     $scope.terminalList[i].terminal=false;
                // };
                // $scope.terminalTypeCode = "";
                // $scope.terminalTypeName = "";
                // for(var i=0; i < $scope.channelList.length; i++){
                //     if($scope.channelList[i].channel == true && $scope.channelList[i].channelCode == '01') {
                //         $scope.showTerminal = true;
                //     }
                // }
            }
            getType2($scope, $http);
        };

        //保存
        $scope.saveClick = function () {
            addActivity($scope, $http);
        };
        //取消 -- 清空所有填写内容
        $scope.cancel = function () {
            if(confirm("确认取消？取消后数据将被清空")){
                initial($scope,$http);
            }
        };

        //搜索sku
        $scope.searchProduct = function (name) {
            $scope.productList = [];
            for(var i=0; i<$scope.productListCopy.length; i++){
                if($scope.productListCopy[i].name.match(name) != null){
                    $scope.productList.push($scope.productListCopy[i]);
                }
            }
            for(var i=0; i<$scope.productList.length; i++){
                for(var j=0; j<$scope.selectedList.length; j++){
                    if($scope.productList[i].code == $scope.selectedList[j].code){
                        $scope.productList[i].sel = true;
                    }
                }
            }
        };

        //全选参与sku
        $scope.toCheckProductAll = function(){
            $scope.selectedList = [];
            $scope.productAll = $scope.productAll ? false : true;
            angular.forEach($scope.productList,function(item,index){
                $scope.productList[index].sel = $scope.productAll;
                $scope.nameListSel(index);   
            });
        };

        //选择参与sku
        $scope.nameListSel = function(index){
            if($scope.productList[index].sel == true){
                $scope.selectedList.push($scope.productList[index]);
                if($scope.selectedList.length == $scope.productList.length){
                    $scope.productAll = true;
                }
            }else{
                for(var i=0; i<$scope.selectedList.length; i++){
                    if($scope.selectedList[i].code == $scope.productList[index].code){
                        $scope.selectedList.splice(i, 1);
                        $scope.productAll = false;
                    }
                }
            }
        };

        //删除已选中的内容
        $scope.delFun = function(item){
            for(var i=0; i<$scope.selectedList.length; i++){
                if($scope.selectedList[i].code == item.code){
                    $scope.selectedList.splice(i, 1);
                }
            }
            for(var j=0; j<$scope.productList.length; j++){
                if($scope.productList[j].code == item.code){
                    $scope.productList[j].sel = false;
                }
            }
            $scope.productAll = false;
        };

        //全部清空-名单类型
        $scope.allClear = function() {
            angular.forEach($scope.productList, function (item) {
                item.sel = false;
            });
            $scope.selectedList = [];
            $scope.productAll = false;
        };

    };

    return {
        templateCtrl2: templateCtrl2
    }
});
//初始化 品牌、活动类型、活动日期的开始时间
function initial($scope,$http){
    //渠道
    angular.forEach($scope.channelList,function(channelList){
        channelList.channel = false;
    });

    //图片类型
    $scope.imgType = $scope.imgTypeList[0].value;

    //是否需要小程序码
    $scope.spType = $scope.spTypeList[1].value;

    // identityVerification2($scope, $http);
    //渠道下拉框  初始化
    $scope.channel = $scope.channelList[0];
    for(var i=0; i<$scope.channelList.length; i++){
        if($scope.channelList[i].channelCode == "01"){
            $scope.channelList[i].channel = true;
            break;
        }
    }
    //初始化  考核类型、品牌、活动类型、活动日期的开始时间
    // $scope.assess = $scope.assessList[0].value;
    // $scope.brand = $scope.brandList[0];

    $scope.brandAll = false;
    for(var i=0; i<$scope.brandList.length; i++) {
        $scope.brandList[i].brand=false; 
        if($scope.brandList[i].value == '26509'){
            $scope.showSKU = true;
            $scope.productName = '';
            $scope.allClear();
        }else{
            $scope.showSKU = false;
        }
    }
    $scope.brandCode = '';
    $scope.brandName = '';
    $scope.saveBrandCode = [];
    $scope.saveBrandName = [];

    $scope.showQRCode = false;
    for(var i=0; i<$scope.qrCodeList.length; i++) {
        $scope.qrCodeList[i].show=false; 
        $scope.qrCodeList[i].sel=false; 
    }

    for(var i=0; i<$scope.terminalList.length; i++) {
        $scope.terminalList[i].terminal=false;
    }

    getType2($scope, $http);

    document.getElementById("startDate").value = new Date().format("yyyy-MM-dd");

    $scope.actName = "";
    document.getElementById("endDate").value = "";
}
//根据身份，显示相应的大区和办事处、渠道、登录人id/姓名
function identityVerification2($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;
        $scope.userId = data.loadUserBaseInfoBean.userId;           //获取登录人id
        $scope.userName = data.loadUserBaseInfoBean.userName;       //获取登录人姓名

        //渠道
        angular.forEach($scope.channelList,function(channelList){
            channelList.channel = false;
        });
        for(var m=0; m<$scope.channelList.length; m++){
            if($scope.channelList[m].channelCode == "08"){
                $scope.channelList.splice(m,1);
            }
        }
        $scope.channel = $scope.channelList[0];
        for(var i=0; i<$scope.channelList.length; i++){
            if($scope.channelList[i].channelCode == "01"){
                $scope.channelList[i].channel = true;
                break;
            }
        }

        // $scope.showTerminal = false;
        // for(var i=0; i < $scope.channelList.length; i++){
        //     if($scope.channelList[i].channel == true && $scope.channelList[i].channelCode == '01') {
        //         $scope.showTerminal = true;
        //     }
        // }

        for(var i=0; i<$scope.imgTypeList.length; i++){
            if($scope.imgTypeList[i].value == "1"){
                $scope.imgTypeList[i].isSelect = true;
                $scope.imgTypeValue = $scope.imgTypeList[i].value;
                $scope.imgTypeName = $scope.imgTypeList[i].name;
                break;
            }
        }

        //获取活动类型
        getType2($scope, $http);

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);
    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments2($scope, $http){
    $scope.departmentDisabled=false;
    if($scope.area.areaCode != ""){
        $scope.departments=[{
            areaId:'',
            officeId:'',
            officeCode:'',
            officeName:"全部"
        }];
    }else{
        $scope.departmentDisabled=true;
    }
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}
/**
 * 新增活动
 * @param $scope
 * @param $http
 */
function addActivity($scope, $http) {
    $scope.startDate = document.getElementById("startDate").value;
    $scope.endDate = document.getElementById("endDate").value;

    //品牌
    $scope.saveBrandCode = [];
    $scope.saveBrandName = [];
    for(var i=1; i < $scope.brandList.length; i++){
        if($scope.brandList[i].brand == true) {
            $scope.saveBrandCode.push($scope.brandList[i].value);
            $scope.saveBrandName.push($scope.brandList[i].name);
        }
    }
    
    //参与sku
    if($scope.showSKU == true){
        $scope.swisseId = [];
        for(var j=0; j<$scope.selectedList.length; j++){
            $scope.swisseId.push($scope.selectedList[j].code);
        }
    }

    //是否申请二维码
    var qrCodeList = [];
    if($scope.showQRCode){
        angular.forEach($scope.qrCodeList, function (item){
            if(item.show && item.sel){
                var qr = {'key':item.value,'value':item.name};
                qrCodeList.push(qr);
            }
        })
    }

    if(!checkForm($scope)){
        return;
    }
    Biostime.common.showLoading();
    var queryData;
    if($scope.showSKU == true){
        queryData = {'request':{
            "userId":$scope.userId,                             //登录人id
            "userName":$scope.userName,                         //登录人姓名
            "khType":$scope.assess,                             //考核类型
            "areaOfficeCode": $scope.department.officeCode != "" ? $scope.department.officeCode : $scope.area.areaCode != "" ? $scope.area.areaCode : $scope.buId.buCode,  //大区办事处编码
            "swisseId": $scope.swisseId,                        //参与SKU
            "actConfigId": $scope.type.code,                    //活动类型
            "actName":$scope.actName,                           //活动名称
            "startDate":$scope.startDate,                       //活动日期起
            "endDate":$scope.endDate,                            //活动日期止
            "brandCode":$scope.saveBrandCode,                           //品牌编码
            "qrCodeList":qrCodeList,                            //是否申请官微二维码
            "channelCode":$scope.channelCode,                         //渠道编码
            "channelName":$scope.channelName,                         //渠道名称
            "terminalTypeCode": $scope.terminalTypeCode,            //门店类型编码
            "terminalTypeName": $scope.terminalTypeName,             //门店类型名称
            "signMiniCode": $scope.spType,             //是否需要签到小程序码
            "actReportType":$scope.imgType                        //是否选着图片上传
        }};
    }else{
        queryData = {'request':{
            "userId":$scope.userId,                             //登录人id
            "userName":$scope.userName,                         //登录人姓名
            "khType":$scope.assess,                             //考核类型
            "areaOfficeCode": $scope.department.officeCode != "" ? $scope.department.officeCode : $scope.area.areaCode != "" ? $scope.area.areaCode : $scope.buId.buCode,  //大区办事处编码
            "actConfigId": $scope.type.code,                    //活动类型
            "actName":$scope.actName,                           //活动名称
            "startDate":$scope.startDate,                       //活动日期起
            "endDate":$scope.endDate,                            //活动日期止
            "brandCode":$scope.saveBrandCode,                           //品牌编码
            "qrCodeList":qrCodeList,                            //是否申请官微二维码
            "channelCode":$scope.channelCode,                         //渠道编码
            "channelName":$scope.channelName,                         //渠道名称
            "terminalTypeCode": $scope.terminalTypeCode,            //门店类型编码
            "terminalTypeName": $scope.terminalTypeName,             //门店类型名称
            "signMiniCode": $scope.spType,             //是否需要签到小程序码
            "actReportType":$scope.imgType                        //是否选着图片上传
        }};
    }

    var url = BASICURL + "samanage/actRoadShow/saveActRoadShow.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            alert("保存成功");
            // window.location.href=BASICURL+"/samanage/forWeb/html5/roadShowActivity/activitySetIndex.html?token="+$scope.token;
            selFun('#view1/1',0);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 检查表单
 * @param $scope
 * @returns {boolean}
 */
function checkForm($scope){
    if($scope.showSKU == true && $scope.selectedList.length == 0){
        Biostime.common.toastMessage("品牌选择Swisse时，请选择参与SKU");
        return false;
    }
    $scope.channelTrue = false;
    angular.forEach($scope.channelList,function(channelList) {
        if(channelList.channel == true) {
            $scope.channelTrue = true;
        }
    });
    if($scope.channelTrue == false) {
        Biostime.common.toastMessage("渠道不能为空，请检查");
        return false;
    }
    if($scope.saveBrandCode.length == 0){
        Biostime.common.toastMessage("品牌不能为空，请检查");
        return false;
    }
    // if($scope.showTerminal == true){
    //     $scope.terminalTrue = false;
    //     angular.forEach($scope.terminalList,function(terminalList) {
    //         if(terminalList.terminal == true) {
    //             $scope.terminalTrue = true;
    //         }
    //     });
    //     if($scope.terminalTrue == false) {
    //         Biostime.common.toastMessage("门店类型不能为空，请检查");
    //         return false;
    //     }
    // }
    var terminalCount = 0;
    for(var m=0; m < $scope.terminalList.length; m++){
        if($scope.terminalList[m].terminal == false) {
            terminalCount++;
        }
    }
    if(terminalCount == $scope.terminalList.length){
        Biostime.common.toastMessage("请至少勾选一个适用门店");
        return false;
    }
    if($scope.typeList.length==0){
        Biostime.common.toastMessage("请在关系维护页面增加【活动/陈列类型】，再新增活动");
        return false;
    }
    if(Biostime.common.isEmpty($scope.actName)){
        if($scope.assess == 1){
            Biostime.common.toastMessage("活动名称不能为空，请检查");
        }else{
            Biostime.common.toastMessage("陈列名称不能为空，请检查");
        }
        return false;
    }
    var reg = /^[A-Za-z0-9\u4e00-\u9fa5]+$/gi;
    if(!Biostime.common.isEmpty($scope.actName)&&!reg.test($scope.actName)){
        if($scope.assess == 1){
            Biostime.common.toastMessage("活动名称只能输入数字、字母、中文，请检查");
        }else{
            Biostime.common.toastMessage("陈列名称只能输入数字、字母、中文，请检查");
        }
        return false;
    }
    if(Biostime.common.isEmpty($scope.startDate)){
        Biostime.common.toastMessage("活动日期开始时间不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.endDate)){
        Biostime.common.toastMessage("活动日期结束时间不能为空，请检查");
        return false;
    }
    
    return true;
}
/**
 * 获取活动类型
 * @param $scope
 * @param $http
 */
function getType2($scope, $http){
    Biostime.common.showLoading();

    //考核类型
    var assessType;
    if($scope.assess == '1'){
        assessType = '1';
    }else{
        assessType = '2';
    };

    //品牌
    $scope.brandCode = "";
    $scope.brandName = "";
    $scope.saveBrandCode = [];
    $scope.saveBrandName = [];
    for(var i=1; i < $scope.brandList.length; i++){
        if($scope.brandList[i].brand == true) {
            $scope.brandCode += $scope.brandList[i].value + ",";
            $scope.brandName += $scope.brandList[i].name + ",";
            $scope.saveBrandCode.push($scope.brandList[i].value);
            $scope.saveBrandName.push($scope.brandList[i].name);
        }
    };
    $scope.brandCode = $scope.brandCode.substring(0,$scope.brandCode.length-1);
    $scope.brandName = $scope.brandName.substring(0,$scope.brandName.length-1);
    if($scope.saveBrandCode.length == $scope.brandList.length-1){
        $scope.brandList[0].brand = true;
    };

    //渠道
    $scope.channelCode = "";
    $scope.channelName = "";
    for(var i=0; i < $scope.channelList.length; i++){
        if($scope.channelList[i].channel == true) {
            $scope.channelCode += $scope.channelList[i].channelCode + ",";
            $scope.channelName += $scope.channelList[i].channelName + ",";
        }
    }
    $scope.channelCode = $scope.channelCode.substring(0,$scope.channelCode.length-1);
    $scope.channelName = $scope.channelName.substring(0,$scope.channelName.length-1);

    //适用门店
    $scope.terminalTypeCode = "";
    $scope.terminalTypeName = "";
    for(var m=0; m < $scope.terminalList.length; m++){
        if($scope.terminalList[m].terminal == true) {
            $scope.terminalTypeCode += $scope.terminalList[m].value + ",";
            $scope.terminalTypeName += $scope.terminalList[m].name + ",";
        }
    }
    $scope.terminalTypeCode = $scope.terminalTypeCode.substring(0,$scope.terminalTypeCode.length-1);
    $scope.terminalTypeName = $scope.terminalTypeName.substring(0,$scope.terminalTypeName.length-1);

    var queryData = {'request':{
        "brandCode":$scope.brandCode,                           //品牌编码
        "channelCode":$scope.channelCode,                         //渠道编码
        "terminalTypeCode": $scope.terminalTypeCode,              //门店类型编码
        "type": 1,                                                 //1:门店类型；2.swisse列表
        "reportTypeCode":assessType
    }};

    var url = BASICURL + "samanage/actRoadShow/querySwActTypeList.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            $scope.typeList = data.response;
            $scope.type = $scope.typeList[0];
            if($scope.typeList.length==0){
                alert("请在关系维护页面增加【活动类型】，再新增活动");
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 获取Swisse产品名称
 * @param $scope
 * @param $http
 */
function getSwisseProduct2($scope, $http){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "type": 2                                   //1:门店类型；2.swisse列表
    }};

    var url = BASICURL + "samanage/actRoadShow/querySwActTypeList.action?"+$scope.token;
    return $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            $scope.productList = angular.copy(data.response);
            $scope.productListCopy = angular.copy(data.response);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}

/**
 * 获取用户当前页面下有效的菜单或按钮
 */
function getSubordinateMenus($scope, $http) {
    // $scope.token = Biostime.common.getQueryString("token");
    var token = Biostime.common.getQueryString("token");
    var urlPath = getCurrentUrlPath();
    var queryData = {
        "seqNo": new Date().getTime(),
        "sourceSystem": "merchant",
        "request": {
            "userId": $scope.userId,  // 必要参数,但可以值为空
            "token": token, // 必要参数
            "url": urlPath // 必要参数
        }
    };
    var url = useId + 'mkt-common/baseInfo/getSubordinateMenus.action' + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            $scope.buttonPermissionList = data.response;
            $scope.imgTypeShow = false;
            angular.forEach($scope.buttonPermissionList,function(item){
                if(item.code == 'shangbaopeizhi') {
                    $scope.imgTypeShow = true;
                };
            });
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};

/* 获取当前路径*/
function getCurrentUrlPath(){
    var currentUrl = document.location.toString();
    var index = currentUrl.lastIndexOf("?");
    return currentUrl.substring(0, index);
}