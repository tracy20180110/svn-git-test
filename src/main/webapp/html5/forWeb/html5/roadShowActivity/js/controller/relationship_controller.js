/*******************************************************************************
 * @jsname:relationship_controller.js
 * @author:WangDongping
 * @date:2017-1-5
 * @use:关系维护.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    //获取token
    if(Biostime.common.getQueryString('token')){
        $scope.token = "token="+Biostime.common.getQueryString('token')
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid')
    }

    //根据身份显示相应的渠道
    identityVerification($scope, $http);

    //新增关系 弹出框
    $scope.showNew = function () {
        $scope.clear();
        //开启窗口
        easyDialog.open({
            container: 'newId'
        });
    };

    //改变适用上报类型
    $scope.typeFn = function (val){
        // console.log($scope.assess);
        if(val == 2){
            $scope.brandList.push({'name':'其他','value':'0'});
            return
        }else{
            angular.forEach($scope.brandList, function (item,index){
                if(item.value == '0'){
                    item.brand = false;
                    $scope.brandList.splice(index,1);
                }
            })
        }
    };

    $scope.clear = function () {
        $scope.addActTypeName = "";
        for(var i=0; i<$scope.brandList.length; i++){
            $scope.brandList[i].brand = false;
        }
        for(var j=0; j<$scope.channelList.length; j++){
            $scope.channelList[j].channel = false;
        }
        for(var m=0; m<$scope.terminalList.length; m++){
            $scope.terminalList[m].terminal = false;
        }
        // for(var k=0; k<$scope.assessList.length; k++){
        //     $scope.assessList[k].assess = false;
        // }
        $scope.assess = $scope.assessList[0].value;
    };

    //新增关系 关闭
    $scope.close = function() {
        easyDialog.close();
        $scope.clear();
    };

    //查询
    $scope.searchClick = function () {
        loadInfoList($scope, $http, 1);
    };

    //导出
    $scope.exportExcel = function () {
        if(Biostime.common.isEmpty($scope.actTypeName)){
            $scope.actTypeName = "";
        }
        window.location.href = BASICURL + 'samanage/actRoadShow/config/exportActRoadShowConfigList.action?'+$scope.token+'&status='+$scope.status.value
            + '&actTypeName=' + $scope.actTypeName;
    };

    //当选择【婴线】渠道呈现“适用门店”
    // $scope.isShowTerminal = function () {
    //     $scope.showTerminal = false;
    //     for(var i=0; i < $scope.channelList.length; i++){
    //         if($scope.channelList[i].channel == true && $scope.channelList[i].channelCode == '01') {
    //             $scope.showTerminal = true;
    //         }
    //     }
    // };

    //状态修改
    $scope.changeStatus = function (id, statusCode) {
        //停用
        if(statusCode == 1){
            if(confirm("请确定是否停用？")){
                changeStatus($scope, $http, id);
            }
        }else if(statusCode == 2){
            if(confirm("请确定是否启用？")){
                changeStatus($scope, $http, id);
            }
        }

    };

    //新增关系
    $scope.save = function (num) {
        addRelationship($scope, $http, num);
    };

    //初始化
    //品牌（1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie）
    $scope.brandList = [
        {'name':"合生元",'value':"1"},
        //{'name':"素加",'value':"2"},
        //{'name':"葆艾",'value':"3"},
        {'name':"HT",'value':"21706"},
        {'name':"合生元可贝思",'value':"36706"},
        {'name':"SWISSE",'value':"26509"},
        {'name':"Dodie",'value':"33206"},
        // {'name':'其他','value':'0'}
    ];
    $scope.brand = $scope.brandList[0];

    //活动状态（活动状态 1-启用 2-停用）
    $scope.statusList = [
        {'name':"全部",'value':""},
        {'name':"启用",'value':"1"},
        {'name':"停用",'value':"2"}
    ];
    $scope.status = $scope.statusList[0];

    //门店类型 1：区域重点连锁,2：其他,3:婴KA
    $scope.terminalList = [
        {'name':"全国重点连锁",'value':"3"},
        {'name':"大区重点连锁",'value':"1"},
        {'name':"办事处重点连锁",'value':"4"},
        {'name':"其他",'value':"2"}
    ];

    //适用上报类型：活动上报，陈列上报
    $scope.assessList = [
        {'name': "活动上报", 'value': "1"},
        {'name': "陈列上报", 'value': "2"}
    ];
    $scope.assess = $scope.assessList[0].value;

    // 分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadInfoList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadInfoList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //账号管理-跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 4;
        if ($scope.pagination.pageCount < 4) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;

});
//根据身份，显示相应的大区和办事处、渠道,登录人信息
function identityVerification($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        $scope.channelList=data.loadUserChannelBeanList;
        angular.forEach($scope.channelList,function(item,index) {
            if(item.channelCode == '08') {
                $scope.channelList.splice(index, 1);
            };
        });
        $scope.userInfo = data.loadUserBaseInfoBean.userId + '/' + data.loadUserBaseInfoBean.userName;
        $scope.createdByName = data.loadUserBaseInfoBean.userName;
        $scope.createdBy = data.loadUserBaseInfoBean.userId;

        //添加埋点
        var date = new Date();
        var has = md5('6'+'60702'+date.getTime()+$scope.userId);
        var dataMd = {
            "platform":6,
            "point_code":'60702',
            "created_time":date.getTime(),
            "customer_id":$scope.userId,
            "sign":has
        };
        eventpv($scope,$http,dataMd);
    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/**
 * 查询路演活动关系列表
 * @param $scope
 * @param $http
 * @param pageNum
 */
function loadInfoList($scope, $http, pageNum) {
    Biostime.common.showLoading();
    var queryData = {'request':{
        "status":$scope.status.value,                             //活动状态
        "actTypeName":$scope.actTypeName,                         //活动类型
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + "samanage/actRoadShow/config/queryActRoadShowConfigList.action?" + $scope.token;
    // var url = "http://10.50.101.162:8080/samanage/actRoadShow/config/queryActRoadShowConfigList.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 修改状态
 * @param $scope
 * @param $http
 * @param id
 */
function changeStatus($scope, $http, id){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "id":id,
        "updatedBy":$scope.userInfo                        //登陆人工号/名称  例如：12804/王小二
    }};
    var url = BASICURL + "samanage/actRoadShow/config/editStatusActRoadShowConfig.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            Biostime.common.toastMessage("修改状态成功");
            loadInfoList($scope, $http, 1);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    });
}
/**
 * 路演活动关系--新增
 * @param $scope
 * @param $http
 * @param num 1:保存并新增；0：保存
 */
function addRelationship($scope, $http, num){
    if(!checkForm($scope)){
        return;
    }
    Biostime.common.showLoading();

    //渠道
    $scope.channelCode = [];
    for(var i=0; i < $scope.channelList.length; i++){
        if($scope.channelList[i].channel == true) {
            $scope.channelCode.push($scope.channelList[i].channelCode);
        }
    }

    //品牌
    $scope.brand = [];
    for(var j=0; j < $scope.brandList.length; j++){
        if($scope.brandList[j].brand == true) {
            $scope.brand.push($scope.brandList[j].value);
        }
    }

    //适用门店
    $scope.terminal = [];
    for(var m=0; m < $scope.terminalList.length; m++){
        if($scope.terminalList[m].terminal == true) {
            $scope.terminal.push($scope.terminalList[m].value);
        }
    }

    //适用上报类型
    var assess = [];
    assess.push($scope.assess);

    // var map = new Map();
    // map.set('1', $scope.brand);//品牌
    // map.set('2', $scope.channelCode);//渠道
    // map.set('3', $scope.terminal);//适用门店
    // map.set('4', $scope.assess);//适用上报类型
    // console.log(map)
    // var queryData = {'request':{
    //     "actTypeName":$scope.addActTypeName,
    //     "brand":$scope.brand,
    //     "channel":$scope.channelCode,
    //     "terminal":$scope.terminal,
    //     "createdBy": $scope.createdBy,
    //     "createdByName": $scope.createdByName
    // }};
    var queryData = {'request':{
        "actTypeName":$scope.addActTypeName,
        "createdBy": $scope.createdBy,
        "createdByName": $scope.createdByName,
        "actConfigMap": {
            '1': $scope.brand,//品牌
            '2': $scope.channelCode,//渠道
            '3': $scope.terminal,//适用门店
            '4': assess//适用上报类型
        }
    }}
    var url = BASICURL + "samanage/actRoadShow/config/saveActRoadShowConfig.action?"+$scope.token;
    // var url = "http://10.50.101.162:8080/samanage/actRoadShow/config/saveActRoadShowConfig.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            Biostime.common.toastMessage("新增成功");
            if(num==1){
                $scope.clear();
            }else if(num==0){
                $scope.close();
                loadInfoList($scope, $http, 1);
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    });
}
//检查表单
function checkForm($scope){
    var brandCount = 0;
    var channelCount = 0;
    var terminalCount = 0;
    // var assessCount = 0;
    for(var j=0; j < $scope.brandList.length; j++){
        if($scope.brandList[j].brand == false) {
           brandCount++;
        }
    }
    if(brandCount == $scope.brandList.length){
        Biostime.common.toastMessage("请至少勾选一个适用品牌");
        return false;
    }
    for(var i=0; i < $scope.channelList.length; i++){
        if($scope.channelList[i].channel == false) {
            channelCount++;
        }
    }
    if(channelCount == $scope.channelList.length){
        Biostime.common.toastMessage("请至少勾选一个适用渠道");
        return false;
    }
    for(var m=0; m < $scope.terminalList.length; m++){
        if($scope.terminalList[m].terminal == false) {
            terminalCount++;
        }
    }
    if(terminalCount == $scope.terminalList.length){
        Biostime.common.toastMessage("请至少勾选一个适用门店");
        return false;
    }
    // for(var k=0; k < $scope.assessList.length; k++){
    //     if($scope.assessList[k].assess == false) {
    //        assessCount++;
    //     }
    // }
    // if(assessCount == $scope.assessList.length){
    //     Biostime.common.toastMessage("请至少勾选一个适用上报类型");
    //     return false;
    // }
    if(Biostime.common.isEmpty($scope.addActTypeName)){
        Biostime.common.toastMessage("活动/陈列类型不能为空，请检查");
        return false;
    }
    return true;
}
