/* 
* @Author: LongJintao
* @Date:   2018-08-16 16:23:27
* @Last Modified by:   Marte
* @Last Modified time: 2018-10-12 16:05:27
*/

var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http, $filter) {
    //获取token
    if(Biostime.common.getQueryString('token')) {
        $scope.token = "token=" + Biostime.common.getQueryString('token');
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid');
    };

    //根据身份显示相应的大区和办事处、渠道，登录人信息
    identityVerification($scope, $http);

    $scope.keyword = '';

    //查询
    $scope.searchFun = function (){
        loadInfoList($scope, $http, 1);
    };

    //状态
    $scope.statusList = [
        {'name':'全部','value':''},
        {'name':'停用','value':'2'},
        {'name':'启用','value':'1'}
    ];
    $scope.status = $scope.statusList[0];

    //更改状态
    $scope.changeStatus = function (item,type){
        statusChange($scope, $http, item, type)
    }

    //新增
    $scope.addFun = function (){
        $scope.imgTypeAdd = '';
        easyDialog.open({
            container: 'selectAddId'
        });
    };

    //通用关闭
    $scope.close = function() {
        easyDialog.close();
    };

    //保存
    $scope.saveClick = function () {
        // console.log($scope.imgTypeAdd)
        if(Biostime.common.isEmpty($scope.imgTypeAdd)){
            Biostime.common.toastMessage("图片类型不能为空，请检查");
            return
        }else{
            addType($scope, $http);
        }
    };

    // 分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadInfoList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadInfoList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //账号管理-跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 4;
        if ($scope.pagination.pageCount < 4) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
});

//根据身份，显示相应的大区和办事处、渠道,登录人信息
function identityVerification($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.areas=data.loadUserAreaBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;

        //获取登录人信息
        $scope.userId = data.loadUserBaseInfoBean.userId;
        $scope.userName = data.loadUserBaseInfoBean.userName;

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
};


//查询明细
function loadInfoList($scope, $http, pageNum) {
    Biostime.common.showLoading();
    var queryData = {
        "request":{
            'name':$scope.keyword,
            'status':$scope.status.value,
            'pageNo':pageNum,
            'pageSize':20
        }
    };
    var url = BASICURL + "samanage/act/report/queryActRePortImgTypeList.action?"+$scope.token;
    // var url = "http://10.50.101.162:8080/samanage/act/report/queryActRePortImgTypeList.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response.datas == null||data.response.datas == '') {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            };
            $scope.pageNum = pageNum;
            //分页
            if(pageNum==1) {
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
                $scope.pagination.currentPage = 1;
            };
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
};

//新增保存
function addType ($scope, $http){
    Biostime.common.showLoading();
    var Data = {"request":{
        "name":$scope.imgTypeAdd,
        "status":'1',
        "userId":$scope.userId,
        "userName":$scope.userName,
        }
    }
    var url = BASICURL + "samanage/act/report/saveActRePortImgType.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        data: Data
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            $scope.close();
            Biostime.common.toastMessage('保存成功！')
            loadInfoList($scope, $http, $scope.pagination.currentPage);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
};

//更改状态
function statusChange ($scope, $http, item, type){
    Biostime.common.showLoading();
    var Data = {
        "request":{
            "id":item.id,
            "status":type,
            "userId":$scope.userId,
            "userName":$scope.userName
        }
    }
    var url = BASICURL + "samanage/act/report/saveActRePortImgType.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        data: Data
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            loadInfoList($scope, $http, $scope.pagination.currentPage);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}