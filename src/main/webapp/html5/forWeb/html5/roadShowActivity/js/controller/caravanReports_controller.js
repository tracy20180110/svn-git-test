/*******************************************************************************
 * @jsname:caravanReports_controller.js
 * @author:WangDongping
 * @date:2017-1-5
 * @use:活动考核报表.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    //获取token
    if(Biostime.common.getQueryString('token')){
        $scope.token = "token="+Biostime.common.getQueryString('token')
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid')
    }
    
    //初始化
    $scope.terminalCode = '';
    $scope.relationTerminalCode = '';
    $scope.actName = '';
    $scope.actId = '';

    //品牌（1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie）
    $scope.brandList = [
        {'name':"全部",'value':""},
        {'name':"合生元",'value':"1"},
        {'name':"素加",'value':"2"},
        {'name':"葆艾",'value':"3"},
        {'name':"HT",'value':"21706"},
        {'name':"合生元可贝思",'value':"36706"},
        {'name':"SWISSE",'value':"26509"},
        {'name':"Dodie",'value':"33206"},
        {'name':"其他",'value':"0"},
    ];
    $scope.brand = $scope.brandList[0];

    //导出文件
    $scope.importFile = function() {
        $scope.dateBegin3 = document.getElementById("dateBegin3").value;
        $scope.dateOver3 = document.getElementById("dateOver3").value;
        if(Biostime.common.isEmpty($scope.dateBegin3)){
            Biostime.common.toastMessage("请选择执行时间的开始时间");
            return;
        }else if(Biostime.common.isEmpty($scope.dateOver3)){
            Biostime.common.toastMessage("请选择执行时间的结束时间");
            return;
        };

        //大区办事处
        $scope.areaofficeCode = $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode;
        $scope.actId = $scope.actId == null ? "" : $scope.actId;
        window.location.href = BASICURL + 'samanage/actRoadShow/exportReportActRoadShow.action?&areaofficeCode=' + $scope.areaofficeCode +'&buCode='+$scope.buId.buCode
            +'&channelCode='+$scope.channel.channelCode
            + '&startDate=' + $scope.dateBegin3 + '&endDate=' + $scope.dateOver3 + '&actId=' + $scope.actId
            + '&createdBy=' + $scope.createdBy + '&terminalCode=' + $scope.terminalCode + '&relationTerminalCode=' + $scope.relationTerminalCode + '&' + $scope.token;
    };

    //根据身份显示相应的大区和办事处、渠道
    identityVerification($scope, $http);

    //事业部修改，大区跟着改变
    $scope.buChange = function(){
        buChange($scope);
    };

    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments($scope, $http);
    };

    //查询
    $scope.searchClick = function() {
        loadInfoList($scope, $http, 1);
    };

   /* //搜索条件展开/收缩
    var searchMore = document.getElementById('searchMore');
    $scope.MoreName='展开';
    searchMore.style.height='82px';
    searchMore.style.overflow='hidden';
    $scope.imgsrc='http://img2.mama100.com/share_mkt_pc/images/donw.png';
    $scope.showMore = function(){
        if(searchMore.offsetHeight<120){
            searchMore.style.height='auto';
            searchMore.style.overflow='none';
            $scope.imgsrc='http://img2.mama100.com/share_mkt_pc/images/up.png';
            $scope.MoreName='收起';
        }else{
            searchMore.style.height='82px';
            searchMore.style.overflow='hidden';
            $scope.imgsrc='http://img2.mama100.com/share_mkt_pc/images/donw.png';
            $scope.MoreName='展开';
        };
    };*/

    // 分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadInfoList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadInfoList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 4;
        if ($scope.pagination.pageCount < 4) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
});


//根据身份，显示相应的大区和办事处、渠道
function identityVerification($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        $scope.userId = data.loadUserBaseInfoBean.userId;           //获取登录人id
        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;

        if($scope.channelList.length > 1){
            var channelAll={
                channelCode:'',
                channelName:"全部"
            };
            $scope.channelList.splice(0,0,channelAll);
        }
        $scope.channel = $scope.channelList[0];

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };


        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);

        //添加埋点
        var date = new Date();
        var has = md5('6'+'60703'+date.getTime()+$scope.userId);
        var dataMd = {
            "platform":6,
            "point_code":'60703',
            "created_time":date.getTime(),
            "customer_id":$scope.userId,
            "sign":has
        }
        eventpv($scope,$http,dataMd);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}

/**
 * 查询
 */
function loadInfoList ($scope, $http, pageNum) {
    $scope.dateBegin3 = document.getElementById("dateBegin3").value;
    $scope.dateOver3 = document.getElementById("dateOver3").value;

    if(Biostime.common.isEmpty($scope.dateBegin3)){
        Biostime.common.toastMessage("请选择执行时间的开始时间");
        return;
    }else if(Biostime.common.isEmpty($scope.dateOver3)){
       Biostime.common.toastMessage("请选择执行时间的结束时间");
       return;
    };

    $scope.pageNum = pageNum;
    Biostime.common.showLoading();
    var queryData = {'request':{
        'actId':$scope.actId,
        'brandCode':$scope.brand.value,
        'terminalCode':$scope.terminalCode,
        'relationTerminalCode':$scope.relationTerminalCode,
        'channelCode':$scope.channel.channelCode,
        'buCode':$scope.buId.buCode,
        'areaofficeCode': $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode,  //大区办事处编码
        "startDate":$scope.dateBegin3,                          //执行时间起
        "endDate":$scope.dateOver3,                             //执行时间止
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + "samanage/actRoadShow/queryReportActRoadShowList.action" + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};