/* 
* @Author: LongJintao
* @Date:   2018-10-12 15:59:55
* @Last Modified by:   Marte
* @Last Modified time: 2019-12-25 20:04:43
*/

var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http, $filter) {
    //获取token
    if(Biostime.common.getQueryString('token')) {
        $scope.token = "token=" + Biostime.common.getQueryString('token');
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid');
    };

    //根据身份显示相应的大区和办事处、渠道，登录人信息
    identityVerification($scope, $http);

    //初始化
    $scope.terminalCode = "";
    $scope.actId = "";
    $scope.createdBy = '';
    $scope.tableTitleHide = false;
    
    //是否补录列表
    $scope.isRecordedList = [
        {'name':"不限",'value':""},
        {'name':"否",'value':"0"},
        {'name':"是",'value':"1"}
    ];
    $scope.isRecorded = $scope.isRecordedList[0];
    //补录原因列表
    $scope.recordedCauseList = [
        {'name':"全部",'value':""},
        {'name':"网络原因未能及时上报",'value':"1"},
        {'name':"营销通系统原因未能及时上报",'value':"2"},
        {'name':"其他",'value':"3"}
    ];
    $scope.recordedCause = $scope.recordedCauseList[0];

    //事业部修改，大区跟着改变
    $scope.buChange = function(){
        buChange($scope);
    };

    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments($scope, $http);
    };

    //查询
    $scope.searchFun = function (){
        loadInfoList($scope, $http, 1);
    };

    //导出
    $scope.exportExcel = function () {
        //活动上报日期
        $scope.dateBegin1 = document.getElementById("dateBegin1").value;
        $scope.dateOver1 = document.getElementById("dateOver1").value;

        if(Biostime.common.isEmpty($scope.dateBegin1) && Biostime.common.isEmpty($scope.dateOver1)){
            Biostime.common.toastMessage("活动上报日期不能为空!");
            return;
        }else if(!Biostime.common.isEmpty($scope.dateBegin1) && Biostime.common.isEmpty($scope.dateOver1)){
            Biostime.common.toastMessage("请选择活动上报日期的结束时间");
            return;
        }else if(Biostime.common.isEmpty($scope.dateBegin1) && !Biostime.common.isEmpty($scope.dateOver1)){
            Biostime.common.toastMessage("请选择活动上报日期的开始时间");
            return;
        }

        //大区办事处
        $scope.areaofficeCode = $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode;
        $scope.actId = $scope.actId == null ? "" : $scope.actId;
        window.location.href = BASICURL + 'samanage/acteLitesReport/exportActElitesReportDetailList.action?' + $scope.token + '&buCode=' + $scope.buId.buCode + '&areaofficeCode=' + $scope.areaofficeCode + '&channelCode=' + $scope.channel.channelCode +'&actId=' + $scope.actId + '&terminalCode=' + $scope.terminalCode + '&reportStartDate=' + $scope.dateBegin1 + '&reportEndDate=' + $scope.dateOver1 + '&createdBy=' + $scope.createdBy + '&actMakeup=' + $scope.isRecorded.value;
    };

    //通用关闭
    $scope.close = function() {
        easyDialog.close();
    };

    //展示大图
    $scope.bigpic_show = false;

    $scope.showBigpic = function(index,item){
        $scope.bigpic_show = true;
        $scope.index = index;
        $scope.currentItem = item;
        show($scope,$scope.index,$scope.currentItem);
    };
    
    $scope.closePic = function(){
        $scope.bigpic_show = false;
    };

    $scope.prevPic = function(){
        $scope.index--;
        if($scope.index<0){
            $scope.index = $scope.currentItem.imgList.length-1;
        };
        show($scope,$scope.index,$scope.currentItem);
    };

    $scope.nextPic = function(){
        $scope.index++;
        if($scope.index>$scope.currentItem.imgList.length-1){
            $scope.index = 0;
        };
        show($scope,$scope.index,$scope.currentItem);
    }

    // 分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadInfoList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadInfoList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //账号管理-跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //账号管理-获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 4;
        if ($scope.pagination.pageCount < 4) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
});

//根据身份，显示相应的大区和办事处、渠道,登录人信息
function identityVerification($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;

        //获取登录人信息
        $scope.userId = data.loadUserBaseInfoBean.userId;
        $scope.userName = data.loadUserBaseInfoBean.userName;

        //渠道
        if($scope.channelList.length > 1){
            var channelAll={
                channelCode:'',
                channelName:"全部"
            };
            $scope.channelList.splice(0,0,channelAll);
        }
        $scope.channel = $scope.channelList[0];

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);

        //添加埋点
        // var date = new Date();
        // var has = md5('6' + '60702' + date.getTime() + $scope.userId);
        // var dataMd = {
        //     "platform":6,
        //     "point_code":'60702',
        //     "created_time":date.getTime(),
        //     "customer_id":$scope.userId,
        //     "sign":has
        // };
        // eventpv($scope,$http,dataMd);
    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}


//查询明细
function loadInfoList($scope, $http, pageNum) {

    // if(Biostime.common.isEmpty($scope.actId)){
    //     Biostime.common.toastMessage("活动ID不能为空!");
    //     return;
    // }
    // if(Biostime.common.isEmpty($scope.terminalCode)){
    //     Biostime.common.toastMessage("门店编码不能为空!");
    //     return;
    // }

    $scope.dateBegin1 = document.getElementById("dateBegin1").value;
    $scope.dateOver1 = document.getElementById("dateOver1").value;

    if(Biostime.common.isEmpty($scope.dateBegin1) && Biostime.common.isEmpty($scope.dateOver1)){
            Biostime.common.toastMessage("活动上报日期不能为空!");
            return;
    }else if(!Biostime.common.isEmpty($scope.dateBegin1) && Biostime.common.isEmpty($scope.dateOver1)){
        Biostime.common.toastMessage("请选择活动上报日期的结束时间");
        return;
    }else if(Biostime.common.isEmpty($scope.dateBegin1) && !Biostime.common.isEmpty($scope.dateOver1)){
        Biostime.common.toastMessage("请选择活动上报日期的开始时间");
        return;
    };

    //查询时，事业部为anc的隐藏 活动期间活动精英积分新客均分
    $scope.tableTitleHide = false;
    if($scope.buId.buCode=='10'){
        $scope.tableTitleHide = true;
    };

    $scope.pageNum = pageNum;
    
    Biostime.common.showLoading();
    var queryData = {'request':{
        "buCode": $scope.buId.buCode,
        "areaofficeCode": $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode,  //大区办事处编码
        "channelCode": $scope.channel.channelCode,                              //渠道
        "terminalCode":$scope.terminalCode,                     //门店编码
        "actId":$scope.actId,                                   //活动ID
        "reportStartDate":$scope.dateBegin1,                    //活动上报日期起
        "reportEndDate":$scope.dateOver1,                       //活动上报日期止
        "createdBy":$scope.createdBy,                           //活动上报人工号
        "actMakeup": $scope.isRecorded.value,                          //是否补录
        "makeupMemo": $scope.recordedCause.value,                        //补录原因
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + "samanage/acteLitesReport/queryActElitesReportDetailList.action?"+$scope.token;
    // var url = "http://10.50.101.162:8080/samanage/acteLitesReport/queryActElitesReportDetailList.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response.datas == null||data.response.datas == '') {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
                angular.forEach($scope.infoList,function(infoList){
                    infoList.imgList = [];
                    if(!infoList.auditTime){
                        $scope.auditList = true;
                    };
                    if(infoList.img1){
                        infoList.imgList.push(infoList.img1);
                    }; 
                    if(infoList.img2){
                        infoList.imgList.push(infoList.img2);
                    }; 
                    if(infoList.img3){
                        infoList.imgList.push(infoList.img3);
                    }; 
                    if(infoList.img4){
                        infoList.imgList.push(infoList.img4);
                    }; 
                    if(infoList.img5){
                        infoList.imgList.push(infoList.img5);
                    }; 
                    if(infoList.img6){
                        infoList.imgList.push(infoList.img6);
                    };
                });
            };
            $scope.pageNum = pageNum;
            //分页
            if(pageNum==1) {
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
                $scope.pagination.currentPage = 1;
            };
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
};

//展示大图
function show ($scope,index,item){
    $scope.bigpic = item.imgList[index];
};