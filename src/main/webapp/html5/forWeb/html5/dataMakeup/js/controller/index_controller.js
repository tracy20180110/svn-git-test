/*******************************************************************************
 * @jsname:index_controller.js
 * @author:wunan
 * @date:2017-02-28
 * @use:数据补录.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
} ]);
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    if(Biostime.common.getQueryString('token')) {
        $scope.token = "token=" + Biostime.common.getQueryString('token');
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid');
    };
    document.getElementById('startDate').value = new Date().format('yyyy-MM-dd');
    document.getElementById('endDate').value = new Date().format('yyyy-MM-dd');
    $scope.selFun = function(sel) {
        $scope.sel = sel;
        document.getElementById('startDate').value = new Date().format('yyyy-MM-dd');
        document.getElementById('endDate').value = new Date().format('yyyy-MM-dd');
        if(sel == 1){
            searchMktImportInfo($scope, $http, 1);
        };
        var file = document.getElementById('fileId');
        //清除上传文件
        if(file.outerHTML){
            file.outerHTML = file.outerHTML;
        }else{
            file.value="";
        };
        $scope.fileName = '';
        $scope.errorData = [];
    };
    $scope.cancel = function() {
        var file = document.getElementById('fileId');
        //清除上传文件
        if(file.outerHTML){
            file.outerHTML = file.outerHTML;
        }else{
            file.value="";
        };
        $scope.fileName = '';
        $scope.errorData = [];
    };
    searchMktImportInfo($scope, $http, 1);
    $scope.search = function() {
        searchMktImportInfo($scope, $http, 1);
    };
    //导入文件
    $scope.uploadFile = function () {
        var url = BASICURL + "samanage/menberDetail/importNewMember.action?token="+Biostime.common.getQueryString('token');
        var fd = new FormData();
        var fileId = document.getElementById('fileId');
        if($scope.fileName == '') {
            Biostime.common.toastMessage('请导入数据！');
            return;
        };
        fd.append('excel', fileId.files[0]);
        Biostime.common.showLoading();
        $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            Biostime.common.hideLoading();
            if(data.code != 100) {
                $scope.errorData = [];
                if(data.response==''||data.response == null||data.response.descriptions=='') {
                    Biostime.common.toastMessage(data.desc);
                }else{
                    $scope.data = angular.copy(data.response.descriptions);
                    angular.forEach($scope.data,function(item){
                        var obj = {
                            'description':item
                        };
                        $scope.errorData.push(obj);
                    });
                }
            }else{
                alert(JSON.stringify('文件导入成功：'+fileId.files[0].name));
                $scope.errorData = [];
            }
        }).error(function (data) {
            Biostime.common.hideLoading();
            Biostime.common.toastMessage('服务器错误，文件导入失败！');
        });
    };
    //下载导入文件
    $scope.downLoaduploadFile = function(tableItem) {
        exportExcel($scope, $http, tableItem);
//        window.location.href = BASICURL + 'samanage/menberDetail/exportExcel.action?token='+Biostime.common.getQueryString('token')+'&filePath='+tableItem.filePath+'&fileName='+tableItem.fileName;
    };
    //下载模板
    $scope.downLoad = function() {
        window.location.href = BASICURL + "samanage/menberDetail/downloadTemplate.action";
    };
    //分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        searchMktImportInfo($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        searchMktImportInfo($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        searchMktImportInfo($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            searchMktImportInfo($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        searchMktImportInfo($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 3;
        if ($scope.pagination.pageCount < 3) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
    $scope.pagination.totalCount = 0;
    //获取上传文件的名称第二步
    $scope.toImport = function(file){
        $scope.orderFile = file;
        $scope.$apply(function(){
            $scope.fileName =$scope.orderFile.name;
        });
    };
});
/**
 * 获取上传文件的名称第一步
 */
function fileChange(target){
    var file =target.files[0];
    if (file) {
        angular.element(document.getElementById('biostimeMKTCtrl')).scope().toImport(file);
    } else {

    }
};
/**
 * 查询接口
 */
function searchMktImportInfo($scope, $http, pageNum) {
    $scope.startTime = document.getElementById('startDate').value;
    $scope.endTime = document.getElementById('endDate').value;
    Biostime.common.showLoading();
    var queryData = {'request':{
        'areaCode':$scope.area?$scope.area.areaCode:'',
        'officeCode':$scope.department?$scope.department.officeCode:'',
        'startTime':$scope.startTime,
        'endTime':$scope.endTime,
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + 'samanage/menberDetail/searchMktImportInfo.action?token='+Biostime.common.getQueryString('token');;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
                $scope.pagination.currentPage = 1;
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};
/**
 * 下载导出文件
 */
function exportExcel($scope, $http, tableItem) {
    Biostime.common.showLoading();
    var url = BASICURL + 'samanage/menberDetail/exportExcel.action?token='+Biostime.common.getQueryString('token')+'&filePath='+tableItem.filePath+'&fileName='+tableItem.fileName;
    $http({
        method : 'GET',
        url:url
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(!data.code) {
            window.location.href = BASICURL + 'samanage/menberDetail/exportExcel.action?token='+Biostime.common.getQueryString('token')+'&filePath='+tableItem.filePath+'&fileName='+tableItem.fileName;
        }else{
            if(data.code == 100){

            }else{
                Biostime.common.toastMessage(data.desc);
            };
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};