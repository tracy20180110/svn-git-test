/**
 * 基础路径
 */
var BASICURL = "http://10.50.115.18:2023/";  //测试
var useId =location.href.indexOf('http://biostime-mkt.biostime.com/')>=0? "http://192.168.234.2:2013/":"http://10.50.115.18:2010/"; //测试:10.50.115.18;生产:192.168.234.2

/**
 * 获取当前路径
 * @returns
 */

function getRootPath(){
    var strFullPath=window.document.location.href;
    var strPath=window.document.location.pathname;
    var pos=strFullPath.indexOf(strPath);
    var prePath=strFullPath.substring(0,pos);
    var postPath=strPath.substring(0,strPath.substr(1).indexOf('/')+1);
    return(prePath+postPath);
}

/**
 * 方法:Array.remove(dx) 功能:根据元素位置值删除数组元素. 参数:元素值 返回:在原数组上修改数组
 */
Array.prototype.remove = function(dx) {
    if (isNaN(dx) || dx > this.length) {
        return false;
    }
    for ( var i = 0, n = 0; i < this.length; i++) {
        if (this[i] != this[dx]) {
            this[n++] = this[i];
        }
    }
    this.length -= 1;
};

/**
 * 用来处理时间戳
 */
Date.prototype.format = function(format) {
    var o = {
        "M+" : this.getMonth() + 1, // month
        "d+" : this.getDate(), // day
        "h+" : this.getHours(), // hour
        "m+" : this.getMinutes(), // minute
        "s+" : this.getSeconds(), // second
        "q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
        "S" : this.getMilliseconds()
        // millisecond
    };
    if (/(y+)/.test(format))
        format = format.replace(RegExp.$1, (this.getFullYear() + "")
            .substr(4 - RegExp.$1.length));
    for ( var k in o)
        if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                : ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};


function getType(o)
{
    var _t;
    return ((_t = typeof(o)) == "object" ? o==null && "null" || Object.prototype.toString.call(o).slice(8,-1):_t).toLowerCase();
}

/**
 * 多维数组深克隆
 * @param destination 目标数组
 * @param source 原数组
 * @returns {Array} 克隆后的新数组
 */
function arrayDeepcopy(destination,source)
{
    for(var p in source)
    {
        if(getType(source[p])=="array"||getType(source[p])=="object")
        {
            destination[p]=getType(source[p])=="array"?[]:{};
            arguments.callee(destination[p],source[p]);
        }
        else
        {
            destination[p]=source[p];
        }
    }
}

/**
 * 打印控制台日志
 * @param obj 打印信息
 */
function consolePrint(obj) {
    if (window.console) {
        window.console.log(obj);
    }
}
/**
 * 判断数组是否包含某参数
 * @param array 目标数组
 * @param param 目标参数
 */
function contains(array,param){
    for(var i=0;i<array.length;i++) {
        if(array[i] == param)
            return true;
    }
    return false;
}

/**
 * 表格样式
 */
//function altRows(id){
//    if(document.getElementsByTagName){
//        var table = document.getElementById('alternatecolor');
//        var rows = table.getElementsByTagName("tr");
//        var li = table.getElementsByTagName("li");
//        for(i = 0; i < rows.length; i++){
//            if(i % 2 == 0){
//                rows[i].className = "evenRowcolor";
//            }else{
//                rows[i].className = "oddRowcolor";
//            }
//        }
//        for(i = 0; i < li.length; i++){
//            if(i % 2 == 0){
//                li[i].className = "oddRowcolor";
//            }else{
//                li[i].className = "evenRowcolor";
//            }
//        }
//    }
//}
//window.onload=function(){
//    altRows('alternatecolor');
//}
/**
 * 检测ie版本
 */

function ieCheck(){
    var userAgent = navigator.userAgent,
        rMsie = /(msie\s|trident.*rv:)([\w.]+)/,
        rFirefox = /(firefox)\/([\w.]+)/,
        rOpera = /(opera).+version\/([\w.]+)/,
        rChrome = /(chrome)\/([\w.]+)/,
        rSafari = /version\/([\w.]+).*(safari)/;
    var browser;
    var version;
    var ua = userAgent.toLowerCase();
    function uaMatch(ua){
        var match = rMsie.exec(ua);
        if(match != null){
            return { browser : "IE", version : match[2] || "0" };
        }
        var match = rFirefox.exec(ua);
        if (match != null) {
            return { browser : match[1] || "", version : match[2] || "0" };
        }
        var match = rOpera.exec(ua);
        if (match != null) {
            return { browser : match[1] || "", version : match[2] || "0" };
        }
        var match = rChrome.exec(ua);
        if (match != null) {
            return { browser : match[1] || "", version : match[2] || "0" };
        }
        var match = rSafari.exec(ua);
        if (match != null) {
            return { browser : match[2] || "", version : match[1] || "0" };
        }
        if (match != null) {
            return { browser : "", version : "0" };
        }
    }
    var browserMatch = uaMatch(userAgent.toLowerCase());
    if (browserMatch.browser){
        browser = browserMatch.browser;
        version = browserMatch.version;
    }
// document.write(browser+version);
// alert(version)
    var ieEdition = rMsie && version;
    if(ieEdition < 11){
        alert("亲，为了您有更好的用户体验，建议使用IE11或以上版本的浏览器哦");
        window.open('ieTips.html')
    }
}
ieCheck();