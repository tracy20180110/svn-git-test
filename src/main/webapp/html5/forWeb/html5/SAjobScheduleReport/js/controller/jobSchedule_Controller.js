/********************************************
 * @jsName:jobSchedule_Controller.js
 * @author:Huangminquan
 * @date:2016-8-19
 * @use:SA工作排班报表
 *******************************************/
var ngApp = angular.module('ngApp', []).config(
    [ '$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    } ]);
var val;
var time = new Date();
var num = "0123456789";
var value = "",i; //value为随机的4位数
for(j=1;j<=4;j++){
    i = parseInt(10*Math.random());
    value = value + num.charAt(i);
}
ngApp.controller('jobSchedule_Controller',['$scope','$http','$timeout',function($scope,$http,$timeout) {
       $scope.token = Biostime.common.getQueryString("token");
       //搜索
       $scope.search = function(){
           search($scope,$http)
       }
       //导出
       $scope.output = function (){
           output($scope)
       }

    //根据身份显示相应的大区和办事处
    identityVerification($scope, $http);

    //事业部修改，大区跟着改变
    $scope.buChange = function(){
        buChange($scope);
    };

    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments($scope, $http);
    };

    //分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination.getPageNums();
    };
    //上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination.getPageNums();
    };
    //下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadList($scope, $http, $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination.getPageNums();
        }
    };
    //跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination.getPageNums();
    };
    // 获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 8;
        if ($scope.pagination.pageCount < 8) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 5) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 4;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
}])

/**
 * 搜索
 * @param $http
 */
function search($scope,$http){
    if(checkFrom($scope)){
        loadList($scope,$http,1);
    }
}

/**
 * 导出
 * @param $http
 */
function output($scope){
    if($scope.user.buCode == '10'){
        Biostime.common.toastMessage('该功能只用于BNC事业部');
        return
    }
    var startTime = document.getElementById("startTime").value;
    var endTime = document.getElementById("endTime").value;
    var areaCode = "";
    var officeCode = "";
    if($scope.area != undefined){
        areaCode = $scope.area.areaCode
    }
    if($scope.department != undefined){
        officeCode = $scope.department.officeCode
    }
    window.location.href = BASICURL + "samanage/jobScheduling/exportJobScheduling.action?token="+$scope.token+"&salesAccountNo="+$scope.salesAccountNo+"&areaCode="+areaCode+"&officeCode="+officeCode+"&startMonth="+startTime+"&endMonth="+endTime + "&buCode="+$scope.buId.buCode;
}



/**
 * 首次加载
 * @param $http
 */
function loadList($scope,$http,page,num){
    if($scope.user.buCode == '10'){
        Biostime.common.toastMessage('该功能只用于BNC事业部');
        return
    }
    var startTime = document.getElementById("startTime").value;
    var endTime = document.getElementById("endTime").value;
    Biostime.common.showLoading();
    var urlApi =  BASICURL + "samanage/jobScheduling/searchJobScheduling.action?token="+$scope.token;//测试地址
    var data = {
        "request":{
            "pageSize":20,
            "pageNo":page,
            "saJobSchedulingBean":{
                "salesAccountNo":$scope.salesAccountNo,
                'buCode': $scope.buId.buCode,
                "areaCode": $scope.area.areaCode,
                "officeCode":  $scope.department.officeCode,
                "startMonth": startTime,
                "endMonth":endTime
            }
        }
    }
    $http({
        method: 'POST',
        url: urlApi,
        data: data,
        headers:{
            'Content-Type': 'application/json;charset=UTF-8'
        }
    }).success(function(data, stauts, config, headers){
        Biostime.common.hideLoading();
        if(data.code == 100){
            $scope.productListInfo = data.response.datas;
            $scope.pagination.pageCount = data.response.totalPage;
            $scope.pagination.totalCount = data.response.totalCount;
            $scope.pagination.pageNums = $scope.pagination.getPageNums();
            $scope.pagination.currentPage = page;
            if(data.response.datas == null){
                if(num == undefined){
                     Biostime.common.toastMessage("暂无数据");
                }
                $scope.paginationShow = false;
            }else{
                $scope.paginationShow = true;
            }
        }else{
            Biostime.common.toastMessage(data.code+":"+data.desc)
        }
    }).error(function(data, status, config, headers){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}

/**
 * 验证
 * @param $http
 */
function checkFrom($scope){
    var startTime =document.getElementById("startTime").value;
    var endTime =document.getElementById("endTime").value;
    var startCount = new Date(startTime);
    var endCount = new Date(endTime);
    var totalCount =  Math.abs(endCount-startCount)/1000/60/60/24;
    if(startTime == undefined || startTime == ""){
        Biostime.common.toastMessage("请选择开始时间");
        return false;
    }
    if(endTime == undefined || endTime == ""){
        Biostime.common.toastMessage("请选择结束时间");
        return false;
    }
    if(startTime > endTime ){
        Biostime.common.toastMessage("开始时间不能大于结束时间");
        return false;
    }
    if(totalCount>92){
        Biostime.common.toastMessage("时间范围不可超过三个月");
        return false;
    }
    if($scope.salesAccountNo != undefined || $scope.salesAccountNo != ""){
        if(isNaN($scope.salesAccountNo)){
            Biostime.common.toastMessage("育婴工号格式错误");
            return false;
        }
    }
    return true;
}


//根据身份，显示相应的大区和办事处
function identityVerification($scope, $http){
    var token
    if(Biostime.common.getQueryString('token')){
        token="?token="+Biostime.common.getQueryString('token')
    }else{
        token="?userId="+Biostime.common.getQueryString('userid')
    }
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;
        $scope.user=data.loadUserBaseInfoBean;

        //添加埋点
        var date = new Date();
        var has = md5('6' + '60305' + date.getTime() + $scope.user.userId);
        var dataMd = {
            "platform":6,
            "point_code":'60305',
            "created_time":date.getTime(),
            "customer_id":$scope.user.userId,
            "sign":has
        }
        eventpv($scope,$http,dataMd);

        if($scope.channelList.length>1){
            var channelAll={
                channelCode:'',
                channelName:"全部"
            };
            $scope.channelList.splice(0,0,channelAll)
        }

        $scope.channelChange= $scope.channelList[0];

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);
    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }]
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i])
        }
    }
    $scope.department=$scope.departments[0]
}
