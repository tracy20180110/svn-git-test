/*******************************************************************************
 * @jsname:editCourse_controller.js
 * @author:WangDongping
 * @date:2017-2-16
 * @use:编辑课程.js
 ******************************************************************************/
define(['angular','ZeroClipboard'], function (angular,ZeroClipboard) {'use strict';
    window['ZeroClipboard'] = ZeroClipboard;

    var templateCtrl7=function ($scope, $http, $route, $routeParams,$rootScope,ngDialog) {
        //获取token
        if(Biostime.common.getQueryString('token')){
            $scope.token = "token="+Biostime.common.getQueryString('token')
        }else{
            $scope.token ="userId="+Biostime.common.getQueryString('userid')
        }
        
        //仅用于页面样式展示
        $scope.selFun = function(selNum) {
            $scope.selNum = selNum;
        };

        //初始化
        //图片是否改变
        $scope.isChangeImg = false;

        //活动类型
        $scope.actTypeList = [
            {'name':'SA-FTF','value':'0'},
            {'name':'门店-FTF','value':'1'},
            {'name':'学术推广','value':'2'},
            {'name':'内训','value':'3'},
            {'name':"Dodie专场-FTF",'value':"4"}
        ];
        angular.forEach($scope.actTypeList,function(actTypeList){
            actTypeList.actType = false;
        });

        //呈现应用
        $scope.appTypeList = [
            {'name':'营销通APP','value':'1'},
            {'name':'妈妈100APP','value':'2'},
            {'name':'合生元妈妈100微信','value':'3'}
        ];
        angular.forEach($scope.appTypeList,function(appTypeList){
            appTypeList.appType = false;
            appTypeList.notChange = false;
        });

        //获取sessionStorage中的数据
        if(Biostime.common.isEmpty(JSON.parse(sessionStorage.editData))){
            //为空
        }else{
            //不为空
            $scope.editData = JSON.parse(sessionStorage.editData);
            $scope.name = $scope.editData.name;
            $scope.id = $scope.editData.id;
            $scope.imgSrc = $scope.editData.imgPath;
            $scope.note =  $scope.editData.initNote;
            $scope.synops =  $scope.editData.initSynops.replace(/(\n)/g, "").replace(/(\t)/g, "").replace(/(\r)/g, "").replace(/<\/?[^>]*>/g, "").replace(/\s*/g, "");
            $scope.appType =  $scope.editData.appType;
            $scope.appTypeName = $scope.editData.appTypeName;
            $scope.recomGroups =  $scope.editData.recomGroups;

            $scope.actType =  $scope.editData.actType;

            if(Biostime.common.isEmpty($scope.appType)){
               /* //呈现应用
                $scope.appTypeList = [
                    {'name':'营销通APP','value':'1'},
                    {'name':'妈妈100APP','value':'2'},
                    {'name':'合生元妈妈100微信','value':'3'}
                ];
                angular.forEach($scope.appTypeList,function(appTypeList){
                    appTypeList.appType = false;
                });*/
            }else{
                //呈现应用
                $scope.appType = $scope.appType.split(',');
                angular.forEach($scope.appType,function(item){
                    angular.forEach($scope.appTypeList,function(items){
                        if(item == items.value) {
                            items.appType = true;
                            items.notChange = false;
                        };
                    });
                });
            }
            if(!Biostime.common.isEmpty($scope.actType)){
                //活动类型
                $scope.actType = $scope.actType.split(',');
                angular.forEach($scope.actType,function(item){
                    angular.forEach($scope.actTypeList,function(items){
                        if(item == items.value) {
                            items.actType = true;
                        };
                    });
                });
            };

            //默认选中营销通APP 且灰掉
            $scope.item = $scope.appTypeList[0];
            $scope.appTypeList[0].appType = true;
            $scope.appTypeList[0].notChange = true;

        }


        //推荐产品  弹出框
        $scope.showProduct = function () {
            //开启窗口
            easyDialog.open({
                container: 'productId'
            });
            //动态设置弹出框右侧ul高度，使其overflow:auto有效。
            // $scope.ulHeight = $("#dialogContent").height();
            if(!Biostime.common.isEmpty($scope.productId)){
                //记录productId
                $scope.oldProductId = angular.copy($scope.productId);
                //记录列表显示
                for(var i=0; i<$scope.productList.length; i++){
                    for(var j=0; j<$scope.productList[i].list.length; j++){
                        $scope.productList[i].show = false;
                        if($scope.productList[i].list[j].productId == $scope.productId){
                            $scope.productList[i].show = true;
                            break;
                        }
                    }
                }
            }
        };

        //关闭 弹出框
        $scope.close = function() {
            if(!Biostime.common.isEmpty($scope.oldProductId)){
                $scope.productId = angular.copy($scope.oldProductId);
            }else{
                for(var i=0; i<$scope.productList.length; i++){
                    $scope.productList[i].show = false;
                }
                $scope.productList[0].show = true;
                $scope.productId="";                                //推荐产品
                $scope.productName = "";                            //产品名称
            }
            easyDialog.close();
        };

        //显示对应大品类里的商品名称，并修改a样式
        $scope.showProductDetail = function (num) {
            for(var i=0; i<$scope.productList.length; i++){
                $scope.productList[i].show = false;
            }
            $scope.productList[num].show = true;
        };

        //推荐产品  弹出框 确定
        $scope.makeSure = function () {
            if(Biostime.common.isEmpty($scope.productId)){
                Biostime.common.toastMessage("推荐产品不能为空，请检查");
                return;
            }

            for(var i=0; i<$scope.productList.length; i++){
                for(var j=0; j<$scope.productList[i].list.length; j++){
                    if($scope.productList[i].list[j].productId == $scope.productId){
                        $scope.productName = $scope.productList[i].list[j].productName;
                        break;
                    }
                }
            }
            easyDialog.close();
        };

        //保存
        $scope.saveClick = function () {
            edtiCourse($scope, $http);
        };

        //取消
        $scope.cancel = function () {
            if(confirm("确认取消？取消后返回课程查询页面")){
                window.location.href="courseIndex.html?"+$scope.token;
            }
        };
        //由directive 获取上传的图片以及图片地址
        $scope.getFile = function (file, imgSrc, type) {
            if(type == '0') {
                $scope.isChangeImg = true;                      //图片改变
                $scope.uploadFile = file;                       //文件
                $scope.imgSrc = imgSrc;                         //base64格式
                $scope.imgPath = imgSrc.replace("data:image/","");
                $scope.imgPath = $scope.imgPath.replace(";base64,","");
                $scope.fileName = file.files[0].name;           //文件名称
            };
        };

        //清除图片
        $scope.clearImg = function(type) {
            if(type == '0') {
                $scope.imgSrc = 'images/add.png';
                $scope.uploadFile = "";
                $scope.imgPath = "";                                 //图片
                $scope.fileName = "";
            };
        };

        //初始化
        //根据身份显示相应的大区和办事处、渠道
        identityVerification7($scope, $http);
        //获取产品列表
        loadProduct($scope, $http);
    };

    return {
        templateCtrl7: templateCtrl7
    }
});

//获取登录人信息
function identityVerification7($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.userId = data.loadUserBaseInfoBean.userId;           //获取登录人id
        $scope.userName = data.loadUserBaseInfoBean.userName;       //获取登录人姓名

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}
/**
 * 获取推荐产品列表
 * @param $scope
 * @param $http
 */
function loadProduct($scope, $http){
    Biostime.common.showLoading();
    var queryData = {'request':{
    }};

    var url = BASICURL + "samanage/ftf/curriculum/queryLecturerCode.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if(data.response.length > 0){
                $scope.productList = data.response;
                for(var i=0; i<$scope.productList.length; i++){
                    $scope.productList[i].show = false;
                }
                $scope.productList[0].show = true;
            }else{
                Biostime.common.toastMessage("暂无产品");
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 新增课程
 * @param $scope
 * @param $http
 */
function edtiCourse($scope, $http) {
    //呈现应用遍历
    $scope.appType = "";
    $scope.appTypeName = "";
    for(var i=0; i < $scope.appTypeList.length; i++){
        if($scope.appTypeList[i].appType == true) {
            $scope.appType += $scope.appTypeList[i].value + ",";
            $scope.appTypeName += $scope.appTypeList[i].name + ",";
        }
    };
    //去除最后一个逗号
    $scope.appType = $scope.appType.substring(0,$scope.appType.length-1);
    $scope.appTypeName = $scope.appTypeName.substring(0,$scope.appTypeName.length-1);

    //活动类型
    $scope.actType = "";
    $scope.actTypeName = "";
    for(var i=0; i < $scope.actTypeList.length; i++){
        if($scope.actTypeList[i].actType == true) {
            $scope.actType += $scope.actTypeList[i].value + ",";
            $scope.actTypeName += $scope.actTypeList[i].name + ",";
        }
    };
    //去除最后一个逗号
    $scope.actType = $scope.actType.substring(0,$scope.actType.length-1);
    $scope.actTypeName = $scope.actTypeName.substring(0,$scope.actTypeName.length-1);

    // $scope.synops = $scope.ue1.getContent();                   //课程简介
    // $scope.note = $scope.ue2.getContent();                     //课程笔记
    //校验提交表单
    if(!checkForm($scope)){
        return;
    }
    var queryData;
    Biostime.common.showLoading();
        //不更新图片，不传imgPath
    queryData = {'request':{
        "id": $scope.id,                                            //id
        "imgPath": $scope.isChangeImg == false?'':$scope.imgPath,   //图片地址
        "upFileName": $scope.isChangeImg == false?'':$scope.fileName,   //图片名称
        "name": $scope.name,                                        //课程名称
        "appType": $scope.appType,                                  //课程类型
        "appTypeName": $scope.appTypeName,                          //课程类型

        //"productId": $scope.productId,                            //推荐产品
        "recomGroups":$scope.recomGroups,                           //推荐人群
        "synops":$scope.synops,                                     //课程简介
        "note":$scope.note,                                         //课程笔记
        "userId":$scope.userId,                                     //登录人Id
        "userName":$scope.userName,                                 //登录人名称

//        "type":$scope.type,                                         //课程类型
        "actType":$scope.actType,                                   //活动类型
        "actTypeName":$scope.actTypeName                            //活动类型
    }};
    var url = BASICURL + "samanage/ftf/curriculum/saveFtfCurriculum.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            alert("修改课程成功");
            sessionStorage.isEdit = "false";
            window.location.href="courseIndex.html?"+$scope.token;
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    });
}
/**
 * 检查表单
 * @param $scope
 * @returns {boolean}
 */
function checkForm($scope){
    if(Biostime.common.isEmpty($scope.name)){
        Biostime.common.toastMessage("课程名称不能为空，请检查");
        return false;
    };
    if($scope.isChangeImg==true && Biostime.common.isEmpty($scope.uploadFile)){
        Biostime.common.toastMessage("主视觉图不能为空，请检查");
        return false;
    };
    if($scope.isChangeImg==false && $scope.imgSrc == 'images/add.png'){
        Biostime.common.toastMessage("主视觉图不能为空，请检查");
        return false;
    };
    if($scope.isChangeImg==false && $scope.imgSrc == null){
        Biostime.common.toastMessage("主视觉图不能为空，请检查");
        return false;
    };
    if(Biostime.common.isEmpty($scope.appType)){
        Biostime.common.toastMessage("呈现应用不能为空，请检查");
        return false;
    };
    if(Biostime.common.isEmpty($scope.actType)){
        Biostime.common.toastMessage("活动类型不能为空，请检查");
        return false;
    };
    if(Biostime.common.isEmpty($scope.recomGroups)){
        Biostime.common.toastMessage("推荐人群不能为空，请检查");
        return false;
    };
    if(Biostime.common.isEmpty($scope.synops)){
        Biostime.common.toastMessage("课程简介不能为空，请检查");
        return false;
    }
   
    return true;
}