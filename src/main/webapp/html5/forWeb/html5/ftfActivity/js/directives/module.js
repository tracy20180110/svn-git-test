/**
** 定义控制器模块myApp.controllers并在app.js里引入
**/
define(['angular'], function(angular) {
	'use strict';
	return angular.module('myApp.directives', []);
});