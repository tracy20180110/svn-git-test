/*******************************************************************************
 * @jsname:activitySearch_controller.js
 * @author:WangDongping
 * @date:2017-2-8
 * @use:活动查询.js
 ******************************************************************************/
define(['angular'], function (angular) {
    'use strict';

    var templateCtrl5=function ($scope, $http, $route, $routeParams,$rootScope,ngDialog, $sce) {
        //获取token
        if(Biostime.common.getQueryString('token')){
            $scope.token = "token="+Biostime.common.getQueryString('token')
        }else{
            $scope.token ="userId="+Biostime.common.getQueryString('userid')
        }
        // $scope.token = "token=19ae6373-47d5-4ce3-8110-18cce3fd56c5";

        //仅用于页面样式展示
        $scope.selFun = function(selNum) {
            $scope.selNum = selNum;
        };

        //事业部修改，大区跟着改变
        $scope.buChange = function(){
            buChange($scope);
        };

        //大区修改，办事处跟着改变
        $scope.areaChange = function(){
            getDepartments($scope, $http);
        };

        //活动查询
        $scope.searchClick = function () {
            loadActivityList($scope, $http, 1);
        };

        //修改活动状态
        $scope.changeStatus = function (id, status) {
            if(confirm("确认修改活动状态？")){
                changeActivityStatus($scope, $http, id, status, 1);
            }
        };

        //查看海报
        $scope.checkPoster = function (str){
            window.location.href = str;
        };

        //查看
        $scope.checkInfo = function (item) {
            $scope.item = item;
            $scope.searchShow = false;
            $scope.detailShow = true;
            $scope.item.activityFlow = $sce.trustAsHtml($scope.item.activityFlow);
            $scope.item.participationMode = $sce.trustAsHtml($scope.item.participationMode);
        };

        var nowTime = new Date();
        nowTime = new Date(nowTime.toLocaleDateString());
        $scope.nextDay = nowTime.setDate(nowTime.getDate()+1);
        //修改编辑
        findSubordinateRegionInfo($scope, $http,'',1);
        $scope.reviseInfo = function (item){
            easyDialog.open({
               container:'revise'
            });
            if(item.type=='SA-FTF'){
                $scope.type = '0';
            }else if(item.type=='门店-FTF'){
                $scope.type = '1';
            }else if(item.type=='学术推广'){
                $scope.type = '2';
            }else if(item.type=='内训'){
                $scope.type = '3';
            }else if(item.type=='Dodie专场-FTF'){
                $scope.type = '4';
            }
            $scope.item = item;
            $scope.courseId = item.curriculumId;
            $scope.courseInfo = item.curriculumName;

            //讲师类型
            $scope.teacherTypeList = [
                {'name':'内部讲师','value':'1'},
                {'name':'兼职讲师','value':'3'}
            ];

            $scope.expertTypeList = [
                {'name':'--请选择--','value':''},
                {'name':'大会主席','value':2},
                {'name':'会议讲者','value':3}
            ];
            if(item.lecturerAccount){
                $scope.teacherType = '1';
                $scope.teacherCode = item.lecturerCode;
                $scope.lecturerAccount = item.lecturerAccount;
                $scope.teacherName = item.lecturerName;
                $scope.teacherInfo = item.lecturerName+'/'+item.lecturerAccount;
                $scope.lecturerRank = item.lecturerRank;
                if(item.lecturerExpertType){
                    for(var j=0;j<$scope.expertTypeList.length;j++){
                        if(item.lecturerExpertType == $scope.expertTypeList[j].name){
                            $scope.NBexpertType = $scope.expertTypeList[j].value;
                            break;
                        }
                    }
                }else{
                    $scope.NBexpertType = $scope.expertTypeList[0].value;
                }
            }else if(item.jzLecturerAccount){
                $scope.teacherType = '3';
                $scope.ptTeacherCode = item.jzLecturerCode?item.jzLecturerCode:'';
                $scope.ptTeacherAccount = item.jzLecturerAccount;
                $scope.ptTeacherName = item.jzLecturerName;
                $scope.ptTeacherInfo = item.jzLecturerName+'/'+item.jzLecturerAccount;
            }
            $scope.externalExpertsList = [
                {'jia':true,'jian':false}
            ];
            var externalCode = [];
            if(item.externalCode){
                var externalCode = item.externalCode.split(',');
            }
            var _externalName = [];
            if(item.externalName){
                var externalName = item.externalName.split(',');
                for(var i=0;i<externalName.length;i++){
                    var num = externalName[i].indexOf('-');
                    if(num > 0){
                        var str = externalName[i].substr(0,num);
                        _externalName.push(str);
                    }else{
                        _externalName.push(externalName[i]);
                    }
                }
            }
            var _externalExpertType = [];
            if(item.externalExpertType){
                var externalExpertType = item.externalExpertType.split(',');
                for(var i=0;i<externalExpertType.length;i++){
                    for(var j=0;j<$scope.expertTypeList.length;j++){
                        if(externalExpertType[i] == $scope.expertTypeList[j].name){
                            _externalExpertType.push($scope.expertTypeList[j].value);
                            break;
                        }
                    }
                }
            }
            var externalRank = [];
            if(item.externalRank){
                var externalRank = item.externalRank.split(',');
            }
            if(externalCode[0]){
                $scope.externalExpertsList = [
                    {'teacherCode':externalCode[0],'teacherName':_externalName[0],'teacherInfo':_externalName[0]+'/'+externalCode[0],'expertType':(_externalExpertType[0]?_externalExpertType[0]:''),'lecturerRank':(externalRank[0]?externalRank[0]:''),'jia':true,'jian':false}
                ]
            }
            if(externalCode[1]){
                $scope.externalExpertsList = [
                    {'teacherCode':externalCode[0],'teacherName':_externalName[0],'teacherInfo':_externalName[0]+'/'+externalCode[0],'expertType':(_externalExpertType[0]?_externalExpertType[0]:''),'lecturerRank':(externalRank[0]?externalRank[0]:''),'jia':false,'jian':true},
                    {'teacherCode':externalCode[1],'teacherName':_externalName[1],'teacherInfo':_externalName[1]+'/'+externalCode[1],'expertType':(_externalExpertType[1]?_externalExpertType[1]:''),'lecturerRank':(externalRank[1]?externalRank[1]:''),'jia':true,'jian':true}
                ]
            }
            if(externalCode[2]){
                $scope.externalExpertsList = [
                    {'teacherCode':externalCode[0],'teacherName':_externalName[0],'teacherInfo':_externalName[0]+'/'+externalCode[0],'expertType':(_externalExpertType[0]?_externalExpertType[0]:''),'lecturerRank':(externalRank[0]?externalRank[0]:''),'jia':false,'jian':true},
                    {'teacherCode':externalCode[1],'teacherName':_externalName[1],'teacherInfo':_externalName[1]+'/'+externalCode[1],'expertType':(_externalExpertType[1]?_externalExpertType[1]:''),'lecturerRank':(externalRank[1]?externalRank[1]:''),'jia':false,'jian':true},
                    {'teacherCode':externalCode[2],'teacherName':_externalName[2],'teacherInfo':_externalName[2]+'/'+externalCode[2],'expertType':(_externalExpertType[2]?_externalExpertType[2]:''),'lecturerRank':(externalRank[2]?externalRank[2]:''),'jia':false,'jian':true}
                ]
            }

            $scope.beginDate = item.actDate+' '+item.startTime;
            $scope.finishDate = item.actDate+' '+item.endTime;
            $scope.address = item.address;
            angular.forEach($scope.provinceOption,function (item1){
                if(item1.regionName==item.province){
                    $scope.province = item1;
                    findSubordinateRegionInfo($scope, $http,item1.regionCode,2).then(function(){
                        angular.forEach($scope.cityOption,function (item2){
                            if(item2.regionName==item.city){
                                $scope.city = item2;
                                findSubordinateRegionInfo($scope, $http,item2.regionCode,3).then(function(){
                                    angular.forEach($scope.areaOption,function (item3){
                                        if(item3.regionName==item.district){
                                            $scope.district = item3;
                                        }
                                    })
                                });
                            }
                        })
                    });
                }
            });
        }
        //保存修改
        $scope.reviseSave = function(){
            saveRevise($scope,$http,$scope.item)
        }
        //取消
        $scope.closeRevise = function(){
            $scope.beginDate = '';
            $scope.finishDate = '';
            easyDialog.close();
            
            $scope.teacherType = '1';
            $scope.teacherId = '';
            $scope.teacherCode = '';
            $scope.teacherName = '';
            $scope.teacherSynops = '';
            $scope.lecturerRank = '';
            $scope.lecturerAccount = '';
            $scope.teacherInfo = '';

            $scope.ptTeacherCode = '';
            $scope.ptTeacherAccount = '';
            $scope.ptTeacherName = '';
            $scope.ptTeacherInfo = '';
        }

         //load出对应课程信息
        $scope.courseFun = function (ev, keyword) {
            // $scope.isSelectCourse = false;
            $scope.courseShow = true;
            $scope.courseId = '';
            if(keyword == undefined){
                keyword = "";
            }
            getCurriculum($scope, $http, keyword);
        };
        //失去焦点隐藏
        $scope.blurFn = function (ev){
            $scope.teacherList = [];
            $scope.courseShow = false;
            $scope.teacherShow = false;
            $scope.ptTeacherShow = false;
            angular.forEach($scope.externalExpertsList,function (item){
                item.teacherShow = false;
            })
        };
        //选择相应课程
        $scope.courseSel = function(item) {
            // $scope.isSelectCourse = true;
            $scope.courseInfo = item.name;
            var synops = item.synops;//课程简介
            synops = synops.replace(/(\n)/g, "").replace(/(\t)/g, "").replace(/(\r)/g, "").replace(/<\/?[^>]*>/g, "").replace(/\s*/g, "");
            $scope.courseIntroduce = synops;
            $scope.courseRecomGroups = item.recomGroups;//推荐人群
            $scope.courseId = item.id;
            $scope.previewUrl = item.imgPath;
        };

        //外部专家
        $scope.externalExpertsList = [
            {'jia':true,'jian':false}
        ];

        //load出对应讲师信息
        $scope.teacherFun = function (type, keyword) {
            // $scope.isSelectTeacher = false;
            $scope.teacherShow = true;
            $scope.teacherId = '';
            if(Biostime.common.isEmpty(keyword)){
                keyword = "";
                $scope.teacherCode = '';
                $scope.teacherName = '';
                $scope.lecturerRank = '';
            }
            getTeacher($scope,$http,'0',keyword);
        };
        //选择相应讲师
        $scope.teacherSel = function(item) {
            // $scope.isSelectTeacher = true;
            $scope.teacherId = item.id;
            $scope.teacherCode = item.code;
            $scope.teacherName = item.name;
            var synops = item.synops;
            synops = synops.replace(/(\n)/g, "").replace(/(\t)/g, "").replace(/(\r)/g, "").replace(/<\/?[^>]*>/g, "").replace(/\s*/g, "");
            $scope.teacherSynops = synops;
            $scope.lecturerRank = item.rank;
            $scope.lecturerAccount = item.lecturerAccount;
            if(item.lecturerAccount){
                $scope.teacherInfo = item.name+"/"+item.lecturerAccount;
            }else{
                $scope.teacherInfo = item.name;
            }
        };

        //load出对应讲师信息（兼职讲师）
        $scope.ptTeacherFun = function (type, keyword) {
            // $scope.isSelectTeacher = false;
            $scope.ptTeacherShow = true;
            $scope.ptTeacherId = '';
            if(Biostime.common.isEmpty(keyword)){
                keyword = "";
                $scope.ptTeacherCode = '';
                $scope.ptTeacherName = '';
                $scope.ptTeacherRank = '';
                $scope.ptTeacherAccount = '';
            }
            getTeacher($scope,$http,'3',keyword);
        };
        //选择相应讲师（兼职讲师）
        $scope.ptTeacherSel = function(item) {
            // $scope.isSelectTeacher = true;
            $scope.ptTeacherId = item.id;
            $scope.ptTeacherCode = item.code;
            $scope.ptTeacherName = item.name;
            var synops = item.synops;
            synops = synops.replace(/(\n)/g, "").replace(/(\t)/g, "").replace(/(\r)/g, "").replace(/<\/?[^>]*>/g, "").replace(/\s*/g, "");
            $scope.ptTeacherSynops = synops;
            $scope.ptTeacherRank = item.rank;
            $scope.ptTeacherAccount = item.lecturerAccount;
            if(item.lecturerAccount){
                $scope.ptTeacherInfo = item.name+"/"+item.lecturerAccount;
            }else{
                $scope.ptTeacherInfo = item.name;
            }
        };

        // load出对应讲师信息(外部专家)
        $scope.externalExpertsFn = function(ev,item,keyword){
            item.teacherShow = true;
            item.teacherId = '';
            if(Biostime.common.isEmpty(keyword)){
                keyword = "";
                item.teacherCode = '';
                item.teacherName = '';
                item.lecturerRank = '';
            }
            getTeacher($scope,$http,'1',keyword);
        };
        //选择相应讲师(外部专家)
        $scope.externalExpertsSel = function(item,oItem) {
            // $scope.isSelectTeacher = true;
            item.teacherInfo = oItem.name+"/"+oItem.code;
            item.teacherId = oItem.id;
            item.teacherCode = oItem.code;
            item.teacherName = oItem.name;
            var synops = oItem.synops;
            synops = synops.replace(/(\n)/g, "").replace(/(\t)/g, "").replace(/(\r)/g, "").replace(/<\/?[^>]*>/g, "").replace(/\s*/g, "");
            item.teacherSynops = synops;
            item.lecturerRank = oItem.rank;
        };
        //加
        $scope.jiaFn = function(index,item) {
            if(index==0){
                item.jia = false;
                item.jian = true;
                $scope.externalExpertsList.push({'jia':true,'jian':true});
            }else if(index==1){
                item.jia = false;
                $scope.externalExpertsList.push({'jia':false,'jian':true});
            }
        };
        //减
        $scope.jianFn = function(index,item) {
            $scope.externalExpertsList.splice(index,1);
            if($scope.externalExpertsList.length==2){
                $scope.externalExpertsList[1].jia = true;
                $scope.externalExpertsList[1].jian = true;
            }else if($scope.externalExpertsList.length==1){
                $scope.externalExpertsList[0].jia = true;
                $scope.externalExpertsList[0].jian = false;
            }
        };

        //选择省市区
        $scope.changReginInfo=function(regionCode,type){
            findSubordinateRegionInfo($scope, $http,regionCode,type);
        };

        //返回
        $scope.back = function () {
            $scope.searchShow = true;
            $scope.detailShow = false;
        };
        
        //导出
        $scope.exportExcel = function () {
            if($scope.user.buCode == '10'){
                Biostime.common.toastMessage('该功能只用于BNC事业部');
                return
            }
            //活动日期
            $scope.startDate = document.getElementById("startDate").value;
            $scope.endDate = document.getElementById("endDate").value;
           /* if(Biostime.common.isEmpty($scope.startDate)){
                Biostime.common.toastMessage("活动日期开始时间不能为空，请检查");
                return;
            }*/
            if($scope.endDate == undefined){
                $scope.endDate = "";
            }

            if($scope.curriculumName == undefined){
                $scope.curriculumName = "";
            }
            if($scope.lecturerCode == undefined){
                $scope.lecturerCode = "";
            }
            if($scope.partTimeLecturerCode == undefined){
                $scope.partTimeLecturerCode = "";
            }
            if($scope.saCode == undefined){
                $scope.saCode = "";
            }

            //大区办事处
            $scope.areaofficeCode = $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode;

            window.location.href = BASICURL + 'samanage/ftf/activity/exportFtfActivityList.action?' + $scope.token + '&curriculumName='+$scope.curriculumName
                + '&startTime=' + $scope.startDate + '&endTime=' + $scope.endDate + '&status=' + $scope.status.value
                + '&lecturerCode=' + $scope.lecturerCode + '&jzLecturerAccount=' + $scope.partTimeLecturerCode + '&saCode=' + $scope.saCode + '&areaOfficeCode=' + $scope.areaofficeCode + '&type=' + $scope.actType.value + '&buCode=' + $scope.buId.buCode;
        };
        //初始化
        //获取登录人信息
        identityVerification5($scope, $http);
        $scope.searchShow = true;
        $scope.detailShow = false;

        //活动类型（""：全部，0:SA-FTF,1:门店-FTF,2:学术推广,3：内训,4：Dodie专场-FTF,5：线上社群）
        $scope.actTypeList = [
            {'name':"全部",'value':""},
            {'name':"SA-FTF",'value':"0"},
            {'name':"门店-FTF",'value':"1"},
            {'name':"学术推广",'value':"2"},
            {'name':"内训",'value':"3"},
            {'name':"线上社群",'value':"5"},
            // {'name':"Dodie专场-FTF",'value':"4"}
        ];
        $scope.actType = $scope.actTypeList[0];

        //活动状态（""：全部，1：启用，0:停用）
        $scope.statusList = [
            {'name':"全部",'value':""},
            {'name':"启用",'value':"1"},
            {'name':"停用",'value':"0"},
            {'name':"过期",'value':"2"}
        ];
        $scope.status = $scope.statusList[0];

        //通用关闭
        $scope.close = function() {
            easyDialog.close();
        };

        //针对已过期的活动支持增加【添加链接】，支持多次编辑链接--201708版本
        $scope.addLink = function(tableItem) {
            $scope.linkId = tableItem.id;
            if(tableItem.videoLink != ''&&tableItem.videoLink!=null) {
                $scope.videoLink = tableItem.videoLink;
            }else{
                $scope.videoLink = '';
            };
            easyDialog.open({
               container:'addLinkId'
            });
        };

        //添加链接
        $scope.sureAdd = function() {
            changeActivityStatus($scope, $http, $scope.linkId, '', 2);
        };

        // 分页
        $scope.pagination = {};
        $scope.pagination.displayPageNum = 4;
        $scope.pagination.onFirstPage = function() {
            $scope.pagination.currentPage = 1;
            loadActivityList($scope, $http, 1);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-页数
        $scope.pagination.onGoPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadActivityList($scope, $http, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-上一页
        $scope.pagination.onUPPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadActivityList($scope, $http, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-下一页
        $scope.pagination.onNextPage = function() {
            if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
                $scope.pagination.currentPage++;
                loadActivityList($scope, $http,
                    $scope.pagination.currentPage);
                $scope.pagination.pageNums = $scope.pagination
                    .getPageNums();
            }
        };
        //账号管理-跳转页
        $scope.pagination.JumpPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadActivityList($scope, $http, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-获取页码数据
        $scope.pagination.getPageNums = function() {
            var tempPageNums = [];
            var startPage = 1;
            var endPage = 4;
            if ($scope.pagination.pageCount < 4) {
                endPage = $scope.pagination.pageCount;
            }
            if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
                if ($scope.pagination.currentPage < 3) {
                    startPage = 1;
                } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                    startPage = $scope.pagination.pageCount
                        - $scope.pagination.displayPageNum + 1;
                } else {
                    startPage = $scope.pagination.currentPage - 2;
                }
                endPage = startPage
                    + $scope.pagination.displayPageNum - 1;
                if (endPage > $scope.pagination.pageCount)
                    endPage = $scope.pagination.pageCount;
            }

            for ( var i = startPage; i <= endPage; i++) {
                tempPageNums.push(i);
            }
            return tempPageNums;
        };
        $scope.pagination.currentPage = 1;
    };

    return {
        templateCtrl5: templateCtrl5
    }
});
//获取登录人信息
function identityVerification5($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        //获取登录人信息
        $scope.user = data.loadUserBaseInfoBean;
        $scope.userId = data.loadUserBaseInfoBean.userId;
        $scope.userName = data.loadUserBaseInfoBean.userName;
        $scope.buList = data.loadUserBuBeanList;
        // $scope.areaCode = data.loadUserBaseInfoBean.areaCode;
        // $scope.officeCode = data.loadUserBaseInfoBean.officeCode;

        // $scope.areas=data.loadUserAreaBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;

        if($scope.channelList.length > 1){
            var channelAll={
                channelCode:'',
                channelName:"全部"
            };
            $scope.channelList.splice(0,0,channelAll);
        }
        $scope.channel = $scope.channelList[0];

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);

        //针对页面埋点
        var date = new Date();
        var has = md5('6'+'6060100'+date.getTime()+$scope.userId);
        var dataMd = {
            "platform":6, //0:mama100Wechat，1:mama100app，2:SwisseWechat，HYTapp，3:HYTwechat，4:DodieWechat，5:HealthyTimesWechat，6:PC
            "point_code":"6060100",  //具体详情见主题类型编码表
            "created_time": date.getTime(),
            "customer_id":$scope.userId,  //用户会员的ID标识
            "sign":has
        };
        eventpv($scope,$http,dataMd);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments($scope, $http){
    $scope.departmentDisabled=false;
    if($scope.area.areaCode != ""){
        $scope.departments=[{
            areaId:'',
            officeId:'',
            officeCode:'',
            officeName:"全部"
        }];
    }else{
        $scope.departmentDisabled=true;
    }
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}

/**
 * 查询活动
 * @param $scope
 * @param $http
 * @param pageNum
 */
function loadActivityList($scope, $http, pageNum) {
    if($scope.user.buCode == '10'){
        Biostime.common.toastMessage('该功能只用于BNC事业部');
        return
    }
    //大区办事处
    $scope.areaofficeCode = $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode;
    //活动日期
    $scope.startDate = document.getElementById("startDate").value;
    $scope.endDate = document.getElementById("endDate").value;
    /*if(Biostime.common.isEmpty($scope.startDate)){
        Biostime.common.toastMessage("活动日期开始时间不能为空，请检查");
        return;
    }*/

    Biostime.common.showLoading();
    var queryData = {'request':{
        "token": Biostime.common.getQueryString('token')?Biostime.common.getQueryString('token'):Biostime.common.getQueryString('userid'),
        "buCode":$scope.buId.buCode,
        "areaOfficeCode":$scope.areaofficeCode,
        'type':$scope.actType.value,
        "curriculumName": $scope.curriculumName,              //活动名称
        "startTime": $scope.startDate,                        //活动日期--开始
        "endTime": $scope.endDate,                            //活动日期--结束
        "status": $scope.status.value,                        //活动状态（0：停用；1：启用；2：过期）
        "lecturerCode": $scope.lecturerCode,                  //讲师姓名
        "jzLecturerAccount": $scope.partTimeLecturerCode,                  //兼职讲师
        "saCode": $scope.saCode,                              //SA编号
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + "samanage/ftf/activity/queryFtfActivityList.action" + '?' + $scope.token;
    // var url = "http://10.50.101.162:8080/samanage/ftf/activity/queryFtfActivityList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
                angular.forEach($scope.infoList, function (item){
                    var time = new Date(item.actDate).format("yyyy/MM/dd");
                    time = new Date(time);
                    item.timeStamp = time.getTime();
                    item.external ='';
                    if(item.externalCode){
                        var externalCodeArr = item.externalCode.split(',');
                        var externalNameArr = item.externalName.split(',');
                        for(var i=0;i<externalCodeArr.length;i++){
                            item.external += externalCodeArr[i]+'/'+externalNameArr[i]+','
                        }
                        item.external = item.external.substring(0,item.external.length-1);
                    }
                })
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 修改状态
 * @param $scope
 * @param $http
 * @param id
 * @param status  改变后的状态
 */
function changeActivityStatus($scope, $http, id, status, type){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "id": id,                                               //活动id
        "status": status,                                       //改变后的状态
        "userId": $scope.userId,                                //登录人id
        "userName": $scope.userName,                            //登录人姓名

        "methodType": type,                                           //type:1-改状态  2-添加链接或者改链接
        "videoLink": $scope.videoLink                           //链接
    }};
    var url = BASICURL + "samanage/ftf/activity/changeStatusFtfActivity.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if(type == 1) {
                Biostime.common.toastMessage("修改状态成功");
            }else{
                Biostime.common.toastMessage("操作成功");
                easyDialog.close();
            };
            loadActivityList($scope, $http, 1);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}

/**
 * 获得课程名称列表
 * @param $scope
 * @param $http
 * @param keyword
 */
function getCurriculum($scope, $http, keyword){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "status":1,
        "actType":$scope.type,
        "name":keyword
    }};
    var url = BASICURL + "samanage/ftf/curriculum/queryFtfCurriculumList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response.datas == null) {
                Biostime.common.toastMessage("无匹配的课程");
                $scope.curriculumList = [];
            }else if (data.response.datas.length > 0) {
                $scope.curriculumList = data.response.datas;
                $scope.curriculum = $scope.curriculumList[0];
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
};

/**
 * 获得课程讲师列表
 * @param $scope
 * @param $http
 * @param keyword
 */
function getTeacher($scope, $http, type, keyword){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "token":Biostime.common.getQueryString('token')?Biostime.common.getQueryString('token'):Biostime.common.getQueryString('userid'),
        "lecturerType":type,
        "lecturerName":keyword
    }};
    // var url = "http://10.50.101.162:8080/samanage/ftf/activity/queryLecturerList.do";
    var url = BASICURL + "samanage/ftf/activity/queryLecturerList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (Biostime.common.isEmpty(data.response)) {
                // Biostime.common.toastMessage("无匹配的讲师");
                $scope.teacherList = [];
            }else if (data.response.length > 0) {
                $scope.teacherList = data.response;
                for(var i=0; i<$scope.teacherList.length; i++){
                    if(type != 2){
                        if($scope.teacherList[i].lecturerAccount){
                            $scope.teacherList[i].teacherInfo = $scope.teacherList[i].name + '/' + $scope.teacherList[i].lecturerAccount;
                        }else{
                            $scope.teacherList[i].teacherInfo = $scope.teacherList[i].name;
                        }
                    }else{
                        $scope.teacherList[i].teacherInfo = $scope.teacherList[i].name + '/' + $scope.teacherList[i].code;
                    }
                }
                $scope.teacher = $scope.teacherList[0];
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}

//选择省市区
function findSubordinateRegionInfo($scope, $http,regionCode,type){

    var url = useId+"mkt-common/baseInfo/findSubordinateRegionInfo.action?regionCode="+regionCode+"&tier="+type;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        var provinceSelect;
        var citySelect;
        var areaSelect;

        if(type==1){
            $scope.provinceOption=data.response;
            $scope.cityOption = [];
            $scope.areaOption = [];
            provinceSelect={
                regionCode:'',
                regionName:'请选择省份',
                tier:'',
                parentRegionCode:''
            };
            $scope.provinceOption.splice(0,0,provinceSelect);
            $scope.province = $scope.provinceOption[0];

            citySelect={
                regionCode:'',
                regionName:'请选择市',
                tier:'',
                parentRegionCode:''
            };
            $scope.cityOption.splice(0,0,citySelect);
            $scope.city = $scope.cityOption[0];

            areaSelect={
                regionCode:'',
                regionName:'请选择区',
                tier:'',
                parentRegionCode:''
            };
            $scope.areaOption.splice(0,0,areaSelect);
            $scope.district = $scope.areaOption[0];

        }else  if(type==2){
            $scope.cityOption=data.response;
            citySelect={
                regionCode:'',
                regionName:'请选择市',
                tier:'',
                parentRegionCode:''
            };
            $scope.cityOption.splice(0,0,citySelect);
            $scope.city = $scope.cityOption[0];

            areaSelect={
                regionCode:'',
                regionName:'请选择区',
                tier:'',
                parentRegionCode:''
            };
            $scope.areaOption.splice(0,0,areaSelect);
            $scope.district = $scope.areaOption[0];
            
        }else  if(type==3){
            $scope.areaOption=data.response;
            areaSelect={
                regionCode:'',
                regionName:'请选择区',
                tier:'',
                parentRegionCode:''
            };
            $scope.areaOption.splice(0,0,areaSelect);
            $scope.district = $scope.areaOption[0];
        }

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
};

//保存修改
function saveRevise($scope,$http,item){
    if(!checkForm($scope)){
        return
    };
    var ftfActLecturer;
    if($scope.teacherType == '1'){
        ftfActLecturer = [
            {'lecturerName':$scope.teacherName,'lecturerCode':$scope.teacherCode,'lecturerRank':$scope.lecturerRank,'expertType':$scope.NBexpertType,'lecturerAccount':$scope.lecturerAccount,'lecturerType':1}
        ];
    }else if($scope.teacherType == '3'){
        ftfActLecturer = [
            {'lecturerName':$scope.ptTeacherName,'lecturerCode':$scope.ptTeacherCode,'lecturerRank':'','expertType':'','lecturerAccount':$scope.ptTeacherAccount,'lecturerType':3}
        ];
    };
    angular.forEach($scope.externalExpertsList, function (item){
        if(item.teacherCode){
            ftfActLecturer.push({'lecturerName':item.teacherName,'lecturerCode':item.teacherCode,'lecturerRank':item.lecturerRank,'expertType':(item.expertType?item.expertType:''),'lecturerType':2});
        }
    });
    
    // console.log($scope.actDate,$scope.startTime,$scope.endTime)
    Biostime.common.showLoading();
    var queryData = {'request':{
        "id":item.id,                                           //活动id
        "curriculumId": $scope.courseId,                        //课程名称Id
        // "lecturerRank":$scope.lecturerRank,                     //讲师头衔
        // "lecturerId": $scope.teacherId,                         //讲师Id
        "actDate":$scope.actDate,                               //活动日期
        "startTime":$scope.startTime,                           //活动开始时间
        "endTime":$scope.endTime,                               //活动结束时间
        "province":$scope.province.regionName,                  //省
        "city":$scope.city.regionName,                          //市
        "district":$scope.district.regionName,                  //区
        "address":$scope.address,                                  //活动地址
        "ftfActLecturer":ftfActLecturer
    }};
    // var url = "http://10.50.101.162:8080/" + "samanage/ftf/activity/saveFtfActivity.action" + '?' + $scope.token;
    var url = BASICURL + "samanage/ftf/activity/saveFtfActivity.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            easyDialog.close();
            loadActivityList($scope, $http, $scope.pagination.currentPage);
            Biostime.common.toastMessage('修改成功');
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        $scope.saveDisabled = false;
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    });
};

/**
 * 检查表单
 * @param $scope
 * @returns {boolean}
 */
function checkForm($scope){
    $scope.beginDate =  document.getElementById("beginDate").value;
    $scope.finishDate =  document.getElementById("finishDate").value;

    if(Biostime.common.isEmpty($scope.courseInfo)){
        Biostime.common.toastMessage("课程名称不能为空，请检查");
        return false;
    }
    if($scope.teacherType=='1' && Biostime.common.isEmpty($scope.teacherInfo)){
        Biostime.common.toastMessage("内部讲师不能为空，请检查");
        return false;
    }
    if($scope.teacherType=='1' && Biostime.common.isEmpty($scope.lecturerRank)){
        Biostime.common.toastMessage("内部讲师头衔不能为空，请检查");
        return false;
    }
    if($scope.type == '2' && $scope.teacherType=='1' && Biostime.common.isEmpty($scope.NBexpertType)){
        Biostime.common.toastMessage("内部讲师专家支持类型不能为空，请检查");
        return false;
    }
    if($scope.teacherType=='3' && Biostime.common.isEmpty($scope.ptTeacherInfo)){
        Biostime.common.toastMessage("兼职讲师不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.beginDate)){
        Biostime.common.toastMessage("活动开始时间不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.finishDate)){
        Biostime.common.toastMessage("活动结束时间不能为空，请检查");
        return false;
    }
    $scope.startDateTemp = getDateForStringDate($scope.beginDate).format("yyyy-MM-dd");
    $scope.endDateTemp = getDateForStringDate($scope.finishDate).format("yyyy-MM-dd");

    if($scope.startDateTemp != $scope.endDateTemp){
        Biostime.common.toastMessage("活动开始时间与结束时间需在同一天，请检查");
        return false;
    }
    if(Date.parse(getDateForStringDate($scope.beginDate)) < Date.parse(new Date())){
        Biostime.common.toastMessage("活动开始时间不能小于当前时间，请检查");
        return false;
    }

    //将活动时间  分割成  日期、开始时间、结束时间
    $scope.actDate = new Date(getDateForStringDate($scope.beginDate)).format("yyyy-MM-dd");
    $scope.startTime = new Date(getDateForStringDate($scope.beginDate)).format("hh:mm");
    $scope.endTime = new Date(getDateForStringDate($scope.finishDate)).format("hh:mm");
    $scope.year = new Date(getDateForStringDate($scope.beginDate)).format("yyyy");
    $scope.month = new Date(getDateForStringDate($scope.beginDate)).format("MM");
    $scope.day = new Date(getDateForStringDate($scope.beginDate)).format("dd");
    $scope.actTime = $scope.year + "年" + $scope.month + "月" + $scope.day + "日   " + $scope.startTime + "-" + $scope.endTime;

    if(Biostime.common.isEmpty($scope.province.regionCode)){
        Biostime.common.toastMessage("请选择省");
        return false;
    }
    if(Biostime.common.isEmpty($scope.city.regionCode)){
        Biostime.common.toastMessage("请选择市");
        return false;
    }
    // if(Biostime.common.isEmpty($scope.district)){
    //     Biostime.common.toastMessage("请选择区");
    //     return false;
    // }
    if(Biostime.common.isEmpty($scope.address)){
        Biostime.common.toastMessage("请填写具体街道地址");
        return false;
    }
    return true;
};

/* js中使用new Date(str)创建时间对象不兼容firefox和ie的解决方式*/
function getDateForStringDate(strDate){
    //切割年月日与时分秒称为数组
    var s = strDate.split(" ");
    var s1 = s[0].split("-");
    var s2 = s[1].split(":");
    if(s2.length==2){
        s2.push("00");
    }
    return new Date(s1[0],s1[1]-1,s1[2],s2[0],s2[1],s2[2]);
}