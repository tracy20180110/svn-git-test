//当DOM结构加载完成后，bootstrap.js文件将会命令AngularJS启动起来并继续执行
define(['angular','require','app','routes'
], function(angular,require) {
	'use strict';
	require(['domReady!'],function(document){
		angular.bootstrap(document,['myApp']);
	});
});