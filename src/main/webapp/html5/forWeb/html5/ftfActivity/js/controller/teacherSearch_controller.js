/*******************************************************************************
 * @jsname:teacherSearch_controller.js
 * @author:WangDongping
 * @date:2017-1-20
 * @use:讲师查询.js
 ******************************************************************************/
define(['angular'], function (angular) {
    'use strict';

    var templateCtrl3=function ($scope, $http, $route, $routeParams,$rootScope,ngDialog,$sce) {
        //获取token
        if(Biostime.common.getQueryString('token')){
            $scope.token = "token="+Biostime.common.getQueryString('token')
        }else{
            $scope.token ="userId="+Biostime.common.getQueryString('userid')
        }
        // $scope.token = "token=19ae6373-47d5-4ce3-8110-18cce3fd56c5";

        //仅用于页面样式展示
        $scope.selFun = function(selNum) {
            $scope.selNum = selNum;
        };

        //讲师查询
        $scope.searchClick = function () {
            loadTeacherList($scope, $http, 1);
        };

        //修改讲师状态
        $scope.changeStatus = function (id, status) {
            if(confirm("确认修改讲师状态？")){
                changeTeacherStatus($scope, $http, id, status);
            }
        };

        //讲师简介  弹出框
        $scope.showSynops = function (synops) {
            //开启窗口
            easyDialog.open({
                container: 'infoId'
            });
            $scope.synopsDetail = $sce.trustAsHtml(synops);
        };
        //讲师编辑  弹出框
        $scope.showEdit = function (item) {
            //开启窗口
            easyDialog.open({
                container: 'editId'
            });

            if(item.type == "育儿讲师"){
                $scope.type = 1;
            }else if(item.type == "育儿专家"){
                $scope.type = 0;
            }
            $scope.teacherInfo = item.name + "/" + item.code;
            $scope.teacherName = item.name;
            $scope.teacherCode = item.code;
            $scope.synops = item.synops;
            $scope.teacherId = item.id;
        };

        //关闭 弹出框
        $scope.close = function() {
            easyDialog.close();
        };

        //保存编辑
        $scope.saveClick = function () {
            editTeacher($scope, $http);
        };
        //取消
        $scope.cancel = function () {
            if(confirm("确认取消？取消后返回讲师查询页面")){
                $scope.synops = "";
                easyDialog.close();
            }
        };

        //导出
        $scope.exportExcel = function () {
            if($scope.name == undefined){
                $scope.name = "";
            }
            if($scope.code == undefined){
                $scope.code = "";
            }

            window.location.href = BASICURL + 'samanage/ftf/lecturer/exportFtfLecturerList.action?'+$scope.token+'&name='+$scope.name
                + '&type=' + $scope.searchType.value + '&status=' + $scope.status.value + '&code=' + $scope.code;
        };

        //导入
        $scope.uploadFile = function (files) {
            Biostime.common.showLoading();
            var url = BASICURL + "samanage/ftf/lecturer/importLecturerCheck.action"+"?"+$scope.token;
            var fd = new FormData();
            fd.append('file', files.files[0]);
            fd.append('userId', $scope.userId);
            fd.append('userName', $scope.userName);
            $http.post(url, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                Biostime.common.hideLoading();
                if(data.code != 100) {
                    alert(JSON.stringify('文件导入失败：'+files.files[0].name));
                }else{
                    alert(JSON.stringify('文件导入成功：'+files.files[0].name));
                }
            }).error(function (data) {
                Biostime.common.toastMessage('服务器错误，文件导入失败！');
            });
        };


        //初始化
        //获取登录人信息
        identityVerification3($scope, $http);
        //讲师状态（""：全部，1：启用，0:停用）
        $scope.statusList = [
            {'name':"全部",'value':""},
            {'name':"启用",'value':"1"},
            {'name':"停用",'value':"0"}
        ];
        $scope.status = $scope.statusList[0];

        //讲师类型（""：全部，1：育儿讲师，0:育儿专家）
        $scope.typeSearchList = [
            {'name':"全部",'value':""},
            {'name':"育儿讲师",'value':"1"},
            {'name':"育儿专家",'value':"0"}
        ];
        $scope.searchType = $scope.typeSearchList[0];

        //讲师类型（1：育儿讲师，0:育儿专家）
        $scope.typeList = [
            {'name':"育儿专家",'value':"0"},
            {'name':"育儿讲师",'value':"1"}
        ];

        // 分页
        $scope.pagination = {};
        $scope.pagination.displayPageNum = 4;
        $scope.pagination.onFirstPage = function() {
            $scope.pagination.currentPage = 1;
            loadTeacherList($scope, $http, 1);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-页数
        $scope.pagination.onGoPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadTeacherList($scope, $http, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-上一页
        $scope.pagination.onUPPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadTeacherList($scope, $http, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-下一页
        $scope.pagination.onNextPage = function() {
            if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
                $scope.pagination.currentPage++;
                loadTeacherList($scope, $http,
                    $scope.pagination.currentPage);
                $scope.pagination.pageNums = $scope.pagination
                    .getPageNums();
            }
        };
        //账号管理-跳转页
        $scope.pagination.JumpPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadTeacherList($scope, $http, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-获取页码数据
        $scope.pagination.getPageNums = function() {
            var tempPageNums = [];
            var startPage = 1;
            var endPage = 4;
            if ($scope.pagination.pageCount < 4) {
                endPage = $scope.pagination.pageCount;
            }
            if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
                if ($scope.pagination.currentPage < 3) {
                    startPage = 1;
                } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                    startPage = $scope.pagination.pageCount
                        - $scope.pagination.displayPageNum + 1;
                } else {
                    startPage = $scope.pagination.currentPage - 2;
                }
                endPage = startPage
                    + $scope.pagination.displayPageNum - 1;
                if (endPage > $scope.pagination.pageCount)
                    endPage = $scope.pagination.pageCount;
            }

            for ( var i = startPage; i <= endPage; i++) {
                tempPageNums.push(i);
            }
            return tempPageNums;
        };
        $scope.pagination.currentPage = 1;
    };

    return {
        templateCtrl3: templateCtrl3
    }
});
//获取登录人信息
function identityVerification3($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        //获取登录人信息
        $scope.userId = data.loadUserBaseInfoBean.userId;
        $scope.userName = data.loadUserBaseInfoBean.userName;

        //控制导入权限
        $scope.uploadShow = false;
        var obj = ['4370','12580','5073','2439'];
        angular.forEach(obj,function(item){
            if(item == $scope.userId) {
                $scope.uploadShow = true;
            };
        });

        //针对页面埋点
        var date = new Date();
        var has = md5('6'+'6060200'+date.getTime()+$scope.userId);
        var dataMd = {
            "platform":6, //0:mama100Wechat，1:mama100app，2:SwisseWechat，HYTapp，3:HYTwechat，4:DodieWechat，5:HealthyTimesWechat，6:PC
            "point_code":"6060200",  //具体详情见主题类型编码表
            "created_time": date.getTime(),
            "customer_id":$scope.userId,  //用户会员的ID标识
            "sign":has
        };
        eventpv($scope,$http,dataMd);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
};
/**
 * 查询讲师
 * @param $scope
 * @param $http
 * @param pageNum
 */
function loadTeacherList($scope, $http, pageNum) {
    Biostime.common.showLoading();
    var queryData = {'request':{
        "name": $scope.name,                                  //讲师名称
        "type": $scope.searchType.value,                            //讲师类型（0：育儿专家；1：育儿讲师）
        "status": $scope.status.value,                        //讲师状态（0：停用；1：启用）
        "code": $scope.code,                                  //讲师编号
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + "samanage/ftf/lecturer/queryFtfLecturerList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
};
/**
 * 修改讲师状态
 * @param $scope
 * @param $http
 * @param id
 * @param status  改变后的状态
 */
function changeTeacherStatus($scope, $http, id, status){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "id": id,                                               //活动id
        "status": status,                                       //改变后的状态
        "userId": $scope.userId,                                //登录人id
        "userName": $scope.userName                             //登录人姓名
    }};
    var url = BASICURL + "samanage/ftf/lecturer/changeStatusFtfLecturer.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            Biostime.common.toastMessage("修改状态成功");
            loadTeacherList($scope, $http, 1);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
};
/**
 * 编辑讲师
 * @param $scope
 * @param $http
 */
function editTeacher($scope, $http){
    if(Biostime.common.isEmpty($scope.synops)){
        Biostime.common.toastMessage("讲师简介不能为空，请检查");
        return;
    }

    Biostime.common.showLoading();
    var queryData = {'request':{
        "id": $scope.teacherId,                                 //讲师id
        "type": $scope.type,                                    //讲师类型
        "name": $scope.teacherName,                             //讲师名称
        "code": $scope.teacherCode,                             //讲师Id
        "synops":$scope.synops,                                 //讲师简介
        "userId":$scope.userId,                                 //登录人Id
        "userName":$scope.userName                              //登录人名称
    }};
    var url = BASICURL + "samanage/ftf/lecturer/saveFtfLecturer.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            alert("修改成功");
            easyDialog.close();
            loadTeacherList($scope, $http, 1);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
};