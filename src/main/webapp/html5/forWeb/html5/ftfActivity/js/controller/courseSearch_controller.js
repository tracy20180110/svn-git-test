/*******************************************************************************
 * @jsname:courseSearch_controller.js
 * @author:WangDongping
 * @date:2017-2-7
 * @use:课程查询.js
 ******************************************************************************/
define(['angular'], function (angular) {
    'use strict';

    var templateCtrl1=function ($scope, $http, $route, $routeParams,$rootScope,ngDialog,$sce) {
        //获取token
        if(Biostime.common.getQueryString('token')){
            $scope.token = "token="+Biostime.common.getQueryString('token')
        }else{
            $scope.token ="userId="+Biostime.common.getQueryString('userid')
        }
        
        //仅用于页面样式展示
        $scope.selFun = function(selNum) {
            $scope.selNum = selNum;
        };

        //课程查询
        $scope.searchClick = function () {
            loadCourseList($scope, $http, $sce, 1);
        };

        //修改课程状态
        $scope.changeStatus = function (id, status) {
            if(confirm("确认修改课程状态？")){
                changeCourseStatus($scope, $http, $sce, id, status);
            }
        };

        //导出
        $scope.exportExcel = function () {
            if($scope.courseName == undefined){
                $scope.courseName = "";
            }

            window.location.href = BASICURL + 'samanage/ftf/curriculum/exportFtfCurriculumList.action?'+$scope.token+'&name='+$scope.courseName
                + '&status=' + $scope.status.value;
        };
        
        //查看课程简介 弹出框
        $scope.showSynops = function (synops) {
            //开启窗口
            easyDialog.open({
                container: 'infoId'
            });
            $scope.synops = synops;
        };

        //查看课程笔记 弹出框
        $scope.showNote = function (note) {
            //开启窗口
            easyDialog.open({
                container: 'noteId'
            });
            $scope.note = note;
        };

        $scope.showEdit = function (tableItem){
            sessionStorage.editData = JSON.stringify(tableItem) ;
            window.location.href="courseIndex.html?"+$scope.token+"#/editCourse/1";
        };

        //关闭 弹出框
        $scope.close = function() {
            easyDialog.close();
        };
        
        //初始化
        //获取登录人信息
        identityVerification1($scope, $http);
        //课程状态（""：全部，1：启用，0:停用）
        $scope.statusList = [
            {'name':"全部",'value':""},
            {'name':"启用",'value':"1"},
            {'name':"停用",'value':"0"}
        ];
        $scope.status = $scope.statusList[0];

        // 分页
        $scope.pagination = {};
        $scope.pagination.displayPageNum = 4;
        $scope.pagination.onFirstPage = function() {
            $scope.pagination.currentPage = 1;
            loadCourseList($scope, $http, $sce, 1);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-页数
        $scope.pagination.onGoPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadCourseList($scope, $http, $sce, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-上一页
        $scope.pagination.onUPPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadCourseList($scope, $http, $sce, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-下一页
        $scope.pagination.onNextPage = function() {
            if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
                $scope.pagination.currentPage++;
                loadCourseList($scope, $http, $sce,
                    $scope.pagination.currentPage);
                $scope.pagination.pageNums = $scope.pagination
                    .getPageNums();
            }
        };
        //账号管理-跳转页
        $scope.pagination.JumpPage = function(pageNum) {
            $scope.pagination.currentPage = pageNum;
            loadCourseList($scope, $http, $sce, pageNum);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        };
        //账号管理-获取页码数据
        $scope.pagination.getPageNums = function() {
            var tempPageNums = [];
            var startPage = 1;
            var endPage = 4;
            if ($scope.pagination.pageCount < 4) {
                endPage = $scope.pagination.pageCount;
            }
            if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
                if ($scope.pagination.currentPage < 3) {
                    startPage = 1;
                } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                    startPage = $scope.pagination.pageCount
                        - $scope.pagination.displayPageNum + 1;
                } else {
                    startPage = $scope.pagination.currentPage - 2;
                }
                endPage = startPage
                    + $scope.pagination.displayPageNum - 1;
                if (endPage > $scope.pagination.pageCount)
                    endPage = $scope.pagination.pageCount;
            }

            for ( var i = startPage; i <= endPage; i++) {
                tempPageNums.push(i);
            }
            return tempPageNums;
        };
        $scope.pagination.currentPage = 1;

    };

    return {
        templateCtrl1: templateCtrl1
    }
});
//获取登录人信息
function identityVerification1($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        //获取登录人信息
        $scope.userId = data.loadUserBaseInfoBean.userId;
        $scope.userName = data.loadUserBaseInfoBean.userName;

        //针对页面埋点
        var date = new Date();
        var has = md5('6'+'6060300'+date.getTime()+$scope.userId);
        var dataMd = {
            "platform":6, //0:mama100Wechat，1:mama100app，2:SwisseWechat，HYTapp，3:HYTwechat，4:DodieWechat，5:HealthyTimesWechat，6:PC
            "point_code":"6060300",  //具体详情见主题类型编码表
            "created_time": date.getTime(),
            "customer_id":$scope.userId,  //用户会员的ID标识
            "sign":has
        };
        eventpv($scope,$http,dataMd);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/**
 * 查询课程
 * @param $scope
 * @param $http
 * @param $sce
 * @param pageNum
 */
function loadCourseList($scope, $http, $sce, pageNum) {
    Biostime.common.showLoading();
    var queryData = {'request':{
        "name": $scope.courseName,                                  //课程名称
        "status": $scope.status.value,                              //课程状态
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + "samanage/ftf/curriculum/queryFtfCurriculumList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
                for(var i=0; i<$scope.infoList.length; i++){
                    $scope.infoList[i].initSynops = $scope.infoList[i].synops;
                    $scope.infoList[i].initNote = $scope.infoList[i].note;
                    $scope.infoList[i].synops =$sce.trustAsHtml($scope.infoList[i].synops);
                    $scope.infoList[i].note =$sce.trustAsHtml($scope.infoList[i].note);
                }
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 修改课程状态
 * @param $scope
 * @param $http
 * @param $sce
 * @param id
 * @param status  改变后的状态
 */
function changeCourseStatus($scope, $http, $sce, id, status){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "id": id,                                               //活动id
        "status": status,                                       //改变后的状态
        "userId": $scope.userId,                                //登录人id
        "userName": $scope.userName                             //登录人姓名
    }};
    var url = BASICURL + "samanage/ftf/curriculum/changeStatusFtfCurriculum.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            Biostime.common.toastMessage("修改状态成功");
            loadCourseList($scope, $http, $sce, 1);
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}

