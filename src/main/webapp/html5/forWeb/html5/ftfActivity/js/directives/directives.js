
/** 定义指令模块 **/
define(['./module'], function(directives) {
	'use strict';
	/*新增课程--主视觉图--上传图片*/
	directives.directive('imgUpload', function() {
		return {
			restrict: 'AE',
			scope: {
				uploadStore: '&'
			},
            template: '<div class="ovh"><input type="button" class="poa" style="opacity: 0; width: 40px; height: 40px; opacity: 0; top: 25px; left: 74px;"><input class="poa" type="file" style="width: 40px; height: 40px; opacity: 0; top: 25px; left: 74px;" name="" id="fileId1" accept="image/jpeg,image/jpg,image/png,image/gif"></div>',
			replace: true,
			link: function(scope, ele, attrs) {
				ele.bind('click', function() {
					$('#file').val('');
				});
				ele.bind('change', function() {
					var file = ele[0].children[1].files;
					if(file[0].size > 5242880){
						alert("图片大小不大于5M");
						file = null;
						return false;
					}

					var postfix = file[0].name.substring(file[0].name.lastIndexOf(".")+1).toLowerCase();
					if(postfix !="jpg" && postfix !="png" && postfix != "jpeg"){
						alert("图片仅支持png/jpg/jpeg类型的文件");
						file = null;
						scope.$apply();
						return false;
					}

					scope.reader = new FileReader();	//创建一个FileReader接口
					if (file) {
						//获取图片（for预览图片）
						scope.reader.readAsDataURL(file[0]);	//FileReader的方法，把图片转成base64
						scope.reader.onload = function(ev) {
							scope.$apply(function(){
								scope.thumb = {
									imgSrc : ev.target.result       //接收base64
								};
								//传文件和图片地址回页面
								scope.$parent.getFile({'files':file},scope.thumb.imgSrc,'0');
							});
						};

					}else{
						Biostime.common.toastMessage('上传图片不能为空!')
					}
				});
			}
		}
	});
	directives.directive('imgUpload1', function() {
		return {
			restrict: 'AE',
			scope: {
				uploadStore: '&'
			},
			template: '<div class="ovh"><input type="button" class="poa" style="opacity: 0; width: 40px; height: 40px; opacity: 0; top: 25px; left: 56px;"><input class="poa" type="file" style="width: 40px; height: 40px; opacity: 0; top: 25px; left: 56px;" name="" id="fileId1" accept="image/jpeg,image/jpg,image/png,image/gif"></div>',
			replace: true,
			link: function(scope, ele, attrs) {
				ele.bind('click', function() {
					$('#file').val('');
				});
				ele.bind('change', function() {
					var file = ele[0].children[1].files;
					if(file[0].size > 5242880){
						alert("图片大小不大于5M");
						file = null;
						return false;
					}

					var postfix = file[0].name.substring(file[0].name.lastIndexOf(".")+1).toLowerCase();
					if(postfix !="jpg" && postfix !="png" && postfix != "jpeg"){
						alert("图片仅支持png/jpg/jpeg类型的文件");
						file = null;
						scope.$apply();
						return false;
					}

					scope.reader = new FileReader();	//创建一个FileReader接口
					if (file) {
						//获取图片（for预览图片）
						scope.reader.readAsDataURL(file[0]);	//FileReader的方法，把图片转成base64
						scope.reader.onload = function(ev) {
							scope.$apply(function(){
								scope.thumb = {
									imgSrc : ev.target.result       //接收base64
								};
								//传文件和图片地址回页面
								scope.$parent.getFile({'files':file},scope.thumb.imgSrc,'1');
							});
						};

					}else{
						Biostime.common.toastMessage('上传图片不能为空!')
					}
				});
			}
		}
	});
	directives.directive('imgUpload2', function() {
		return {
			restrict: 'AE',
			scope: {
				uploadStore: '&'
			},
			template: '<div class="ovh"><input type="button" class="poa" style="opacity: 0; width: 40px; height: 40px; opacity: 0; top: 25px; left: 56px;"><input class="poa" type="file" style="width: 40px; height: 40px; opacity: 0; top: 25px; left: 56px;" name="" id="fileId1" accept="image/jpeg,image/jpg,image/png,image/gif"></div>',
			replace: true,
			link: function(scope, ele, attrs) {
				ele.bind('click', function() {
					$('#file').val('');
				});
				ele.bind('change', function() {
					var file = ele[0].children[1].files;
					if(file[0].size > 5242880){
						alert("图片大小不大于5M");
						file = null;
						return false;
					}

					var postfix = file[0].name.substring(file[0].name.lastIndexOf(".")+1).toLowerCase();
					if(postfix !="jpg" && postfix !="png" && postfix != "jpeg"){
						alert("图片仅支持png/jpg/jpeg类型的文件");
						file = null;
						scope.$apply();
						return false;
					}

					scope.reader = new FileReader();	//创建一个FileReader接口
					if (file) {
						//获取图片（for预览图片）
						scope.reader.readAsDataURL(file[0]);	//FileReader的方法，把图片转成base64
						scope.reader.onload = function(ev) {
							scope.$apply(function(){
								scope.thumb = {
									imgSrc : ev.target.result       //接收base64
								};
								//传文件和图片地址回页面
								scope.$parent.getFile({'files':file},scope.thumb.imgSrc,'2');
							});
						};

					}else{
						Biostime.common.toastMessage('上传图片不能为空!')
					}
				});
			}
		}
	});
    directives.directive("reportUpload",function(){
        return{
            //通过设置项来定义
            restrict: 'AE',
            scope: {
                uploadStore: '&'
            },
            template: '<div class="fl por secButtonBox ovh"><input type="button" style="width: 100px; height:30px; color: #fff; position: absolute; top: 0; left: 0; background: none; border: none;" value="讲师认证导入"><input type="file" name="file" id="file" style="position: absolute; top: 0; left: 0; width: 100px; height: 30px; opacity: 0;" accept=".xlsx,.xls"></div>',
            //template:"<div><h3>hello world</h3></div>",
            replace: true,
            link: function (scope, ele, attrs) {
                ele.bind('click', function () {
                    $('#file').val('');
                });
                ele.bind('change', function () {
                    var file = ele[0].children[1].files;
                    if (file) {
                        scope.$parent.uploadFile({'files': file});
                        ele[0].children[1].outerHTML=ele[0].children[1].outerHTML;
                    } else {

                    }
                });
            }
        };
    });

	directives.directive('logofileModel',function() {
		return {
			restrict: 'AE',
			scope: {
				uploadStore: '&'
			},
			template: '<input type="file" style="opacity: 0;" name="uploadfile" id="file" accept="image/*">',
			replace: true,
			link: function(scope, ele, attrs) {
					ele.bind('click', function() {
						this.value = '';
					});
					ele.bind('change', function() {
						var file = this.files;
						// if(file[0].size > 20480){
						// 	alert("图片大小不大于20kb");
						// 	file = null;
						// 	return false;
						// }

						// var postfix = file[0].name.substring(file[0].name.lastIndexOf(".")+1).toLowerCase();
						// if(postfix !="png"){
						// 	alert("图片仅支持png类型的文件");
						// 	file = null;
						// 	scope.$apply();
						// 	return false;
						// }

						scope.reader = new FileReader();	//创建一个FileReader接口
						if (file) {
							//创建canvas用于压缩图片
							var image = new Image();  
							var logo_canvas = document.createElement("canvas"); 
							logo_canvas.width = 210;
							logo_canvas.height = 60;
							var ctx = logo_canvas.getContext('2d'); 

							//获取图片（for预览图片）
							scope.reader.readAsDataURL(file[0]);	//FileReader的方法，把图片转成base64
							scope.reader.onload = function(ev) {
								image.src = ev.target.result;
								// scope.$apply(function(){
								// 	scope.thumb = {
								// 		imgSrc : ev.target.result       //接收base64
								// 	};
								// 	//传文件和图片地址回页面
								// 	scope.$parent.getFile({'files':file},scope.thumb.imgSrc,'0');
								// });
							};
							image.onload = function() {   
	                            ctx.drawImage(image, 0, 0, 210, 60); 
	                            var imgSrc = logo_canvas.toDataURL(); 
	                            //计算压缩后大小
								var str=imgSrc.substring(22);
								var equalIndex= str.indexOf('=');
								if(str.indexOf('=')>0){
									str=str.substring(0, equalIndex);
								}
			                    var strLength=str.length;
			                    var fileLength=parseInt(strLength-(strLength/8)*2); //压缩后大小
			                    if(fileLength > 20480){
									alert("图片大小不大于20kb");
									file = null;
									return false;
								}
	                            scope.$parent.getFile({'files':file},imgSrc,'0'); 
	                        };
						}else{
							Biostime.common.toastMessage('上传图片不能为空!')
						}
					});
			}
		};
	});

	directives.directive('teacherfileModel',function() {
		return {
			restrict: 'AE',
			scope: {
				uploadStore: '&'
			},
			template: '<input type="file" style="opacity: 0;" name="uploadfile" id="file" accept="image/*">',
			replace: true,
			link: function(scope, ele, attrs) {
					ele.bind('click', function() {
						this.value = '';
					});
					ele.bind('change', function() {
						var file = this.files;
						// if(file[0].size > 102400){
						// 	alert("图片大小不大于100kb");
						// 	file = null;
						// 	return false;
						// }

						// var postfix = file[0].name.substring(file[0].name.lastIndexOf(".")+1).toLowerCase();
						// if(postfix !="jpg" && postfix !="png" && postfix != "jpeg"){
						// 	alert("图片仅支持png/jpg/jpeg类型的文件");
						// 	file = null;
						// 	scope.$apply();
						// 	return false;
						// }

						scope.reader = new FileReader();	//创建一个FileReader接口
						if (file) {
							//创建canvas用于压缩图片
							var image = new Image();  
							var teacher_canvas = document.createElement("canvas"); 
							teacher_canvas.width = 180;
							teacher_canvas.height = 180;
							var ctx = teacher_canvas.getContext('2d'); 

							//获取图片（for预览图片）
							scope.reader.readAsDataURL(file[0]);	//FileReader的方法，把图片转成base64
							scope.reader.onload = function(ev) {
								image.src = ev.target.result;
								// scope.$apply(function(){
								// 	scope.thumb = {
								// 		imgSrc : ev.target.result       //接收base64
								// 	};
								// 	//传文件和图片地址回页面
								// 	scope.$parent.getFile({'files':file},scope.thumb.imgSrc,'0');
								// });
							};
							image.onload = function() {   
	                            ctx.drawImage(image, 0, 0, 180, 180); 
	                            var imgSrc = teacher_canvas.toDataURL(); 
	                            //计算压缩后大小
								var str=imgSrc.substring(22);
								var equalIndex= str.indexOf('=');
								if(str.indexOf('=')>0){
									str=str.substring(0, equalIndex);
								}
			                    var strLength=str.length;
			                    var fileLength=parseInt(strLength-(strLength/8)*2); //压缩后大小
			                    if(fileLength > 5242880){
									alert("图片大小不大于5M");
									file = null;
									return false;
								}
	                            scope.$parent.getFile({'files':file},imgSrc,'1'); 
	                        };
						}else{
							Biostime.common.toastMessage('上传图片不能为空!')
						}
					});
			}
		};
	});
	
	directives.directive('activityfileModel1',function() {
		return {
			restrict: 'AE',
			scope: {
				uploadStore: '&'
			},
			template: '<input type="file" style="opacity: 0;" name="uploadfile" id="file" accept="image/*">',
			replace: true,
			link: function(scope, ele, attrs) {
					ele.bind('click', function() {
						this.value = '';
					});
					ele.bind('change', function() {
						var file = this.files;
						// if(file[0].size > 307200){
						// 	alert("图片大小不大于300kb");
						// 	file = null;
						// 	return false;
						// }

						// var postfix = file[0].name.substring(file[0].name.lastIndexOf(".")+1).toLowerCase();
						// if(postfix !="png"){
						// 	alert("图片仅支持png类型的文件");
						// 	file = null;
						// 	scope.$apply();
						// 	return false;
						// }

						scope.reader = new FileReader();	//创建一个FileReader接口
						if (file) {
							//创建canvas用于压缩图片
							var image = new Image();  
							var activity1_canvas = document.createElement("canvas"); 
							activity1_canvas.width = 270;
							activity1_canvas.height = 290;
							var ctx = activity1_canvas.getContext('2d'); 

							//获取图片（for预览图片）
							scope.reader.readAsDataURL(file[0]);	//FileReader的方法，把图片转成base64
							scope.reader.onload = function(ev) {
								image.src = ev.target.result;
								// scope.$apply(function(){
								// 	scope.thumb = {
								// 		imgSrc : ev.target.result       //接收base64
								// 	};
								// 	//传文件和图片地址回页面
								// 	scope.$parent.getFile({'files':file},scope.thumb.imgSrc,'0');
								// });
							};
							image.onload = function() {   
	                            ctx.drawImage(image, 0, 0, 270, 290); 
	                            var imgSrc = activity1_canvas.toDataURL(); 
	                            //计算压缩后大小
								var str=imgSrc.substring(22);
								var equalIndex= str.indexOf('=');
								if(str.indexOf('=')>0){
									str=str.substring(0, equalIndex);
								}
			                    var strLength=str.length;
			                    var fileLength=parseInt(strLength-(strLength/8)*2); //压缩后大小
			                    if(fileLength > 5242880){
									alert("图片大小不大于5M");
									file = null;
									return false;
								}
	                            scope.$parent.getFile({'files':file},imgSrc,'2'); 
	                        };
						}else{
							Biostime.common.toastMessage('上传图片不能为空!')
						}
					});
			}
		};
	});
	
	directives.directive('activityfileModel2',function() {
		return {
			restrict: 'AE',
			scope: {
				uploadStore: '&'
			},
			template: '<input type="file" style="opacity: 0;" name="uploadfile" id="file" accept="image/*">',
			replace: true,
			link: function(scope, ele, attrs) {
					ele.bind('click', function() {
						this.value = '';
					});
					ele.bind('change', function() {
						var file = this.files;
						// if(file[0].size > 307200){
						// 	alert("图片大小不大于300kb");
						// 	file = null;
						// 	return false;
						// }

						// var postfix = file[0].name.substring(file[0].name.lastIndexOf(".")+1).toLowerCase();
						// if(postfix !="png"){
						// 	alert("图片仅支持png类型的文件");
						// 	file = null;
						// 	scope.$apply();
						// 	return false;
						// }

						scope.reader = new FileReader();	//创建一个FileReader接口
						if (file) {
							//创建canvas用于压缩图片
							var image = new Image();  
							var activity2_canvas = document.createElement("canvas"); 
							activity2_canvas.width = 270;
							activity2_canvas.height = 290;
							var ctx = activity2_canvas.getContext('2d'); 

							//获取图片（for预览图片）
							scope.reader.readAsDataURL(file[0]);	//FileReader的方法，把图片转成base64
							scope.reader.onload = function(ev) {
								image.src = ev.target.result;
								// scope.$apply(function(){
								// 	scope.thumb = {
								// 		imgSrc : ev.target.result       //接收base64
								// 	};
								// 	//传文件和图片地址回页面
								// 	scope.$parent.getFile({'files':file},scope.thumb.imgSrc,'0');
								// });
							};
							image.onload = function() {   
	                            ctx.drawImage(image, 0, 0, 270, 290); 
	                            var imgSrc = activity2_canvas.toDataURL(); 
	                            //计算压缩后大小
								var str=imgSrc.substring(22);
								var equalIndex= str.indexOf('=');
								if(str.indexOf('=')>0){
									str=str.substring(0, equalIndex);
								}
			                    var strLength=str.length;
			                    var fileLength=parseInt(strLength-(strLength/8)*2); //压缩后大小
			                    if(fileLength > 5242880){
									alert("图片大小不大于5M");
									file = null;
									return false;
								}
	                            scope.$parent.getFile({'files':file},imgSrc,'3'); 
	                        };
						}else{
							Biostime.common.toastMessage('上传图片不能为空!')
						}
					});
			}
		};
	});

	directives.directive('prizeModel',function() {
		return {
			restrict: 'AE',
			scope: {
				uploadStore: '&'
			},
			template: '<input type="file" style="opacity: 0;" name="uploadfile" id="prizeFile" accept="image/*">',
			replace: true,
			link: function(scope, ele, attrs) {
					ele.bind('click', function() {
						this.value = '';
					});
					ele.bind('change', function() {
						var file = this.files;
						// if(file[0].size > 307200){
						// 	alert("图片大小不大于300kb");
						// 	file = null;
						// 	return false;
						// }

						// var postfix = file[0].name.substring(file[0].name.lastIndexOf(".")+1).toLowerCase();
						// if(postfix !="png"){
						// 	alert("图片仅支持png类型的文件");
						// 	file = null;
						// 	scope.$apply();
						// 	return false;
						// }

						scope.reader = new FileReader();	//创建一个FileReader接口
						if (file) {
							//创建canvas用于压缩图片
							var image = new Image();  
							var prize_canvas = document.createElement("canvas"); 
							prize_canvas.width = 270;
							prize_canvas.height = 376;
							var ctx = prize_canvas.getContext('2d'); 

							//获取图片（for预览图片）
							scope.reader.readAsDataURL(file[0]);	//FileReader的方法，把图片转成base64
							scope.reader.onload = function(ev) {
								image.src = ev.target.result;
								// scope.$apply(function(){
								// 	scope.thumb = {
								// 		imgSrc : ev.target.result       //接收base64
								// 	};
								// 	//传文件和图片地址回页面
								// 	scope.$parent.getFile({'files':file},scope.thumb.imgSrc,'0');
								// });
							};
							image.onload = function() {   
	                            ctx.drawImage(image, 0, 0, 270, 376); 
	                            var imgSrc = prize_canvas.toDataURL(); 
	                            //计算压缩后大小
								var str=imgSrc.substring(22);
								var equalIndex= str.indexOf('=');
								if(str.indexOf('=')>0){
									str=str.substring(0, equalIndex);
								}
			                    var strLength=str.length;
			                    var fileLength=parseInt(strLength-(strLength/8)*2); //压缩后大小
			                    if(fileLength > 5242880){
									alert("图片大小不大于5M");
									file = null;
									return false;
								}
	                            scope.$parent.getFile({'files':file},imgSrc,'4'); 
	                        };
						}else{
							Biostime.common.toastMessage('上传图片不能为空!')
						}
					});
			}
		};
	});
	

});