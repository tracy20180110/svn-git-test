/*******************************************************************************
 * @jsname:addActivity_controller.js
 * @author:WangDongping
 * @date:2017-1-20
 * @use:新增活动.js
 ******************************************************************************/
define(['angular','ZeroClipboard'], function (angular,ZeroClipboard) {'use strict';
    window['ZeroClipboard'] = ZeroClipboard;

    var templateCtrl6=function ($scope, $http, $route, $routeParams,$rootScope,ngDialog,$sce) {

        //获取token
        if(Biostime.common.getQueryString('token')){
            $scope.token = "token="+Biostime.common.getQueryString('token')
        }else{
            $scope.token ="userId="+Biostime.common.getQueryString('userid')
        }
        // $scope.token = "token=19ae6373-47d5-4ce3-8110-18cce3fd56c5";

        //初始化
        $scope.groupType = 1;
        $scope.shoppingGuideID = '';
        $scope.logoShow = false;
        $scope.saveDisabled = false;

        //场地类型:院内班-1;酒店班-2;院外班-3;SA专场-4;整合场-5
        $scope.fieldData1 = [
            {'name':'院内班','value':'1'},
            {'name':'院外班','value':'3'}
        ];
        $scope.fieldData2 = [
            {'name':'SA专场','value':'4'},
            {'name':'整合场','value':'5'}
        ];
        $scope.fieldList = angular.copy($scope.fieldData1);
        $scope.field = $scope.fieldList[0].value;

        // 这里默认空，保存的时候提示请选择新客计算方式
        $scope.newCustomerCalculation = '';

        //消费者类型
        $scope.consumerTypeList = [
            // {'name':'--请选择--','value':''},
            {'name':'孕妈班','value':'1'},
            {'name':'宝妈班','value':'2'}
        ];
        // $scope.consumerType = $scope.consumerTypeList[0].value;

        $scope.expertTypeList = [
            {'name':'专家讲座','value':1,'notSel':true},
            {'name':'大会主席','value':2},
            {'name':'会议讲者','value':3}
        ];
        $scope.expertType = $scope.expertTypeList[0].value;
        $scope.expertTypeDisabled = true;

        //外部专家
        $scope.externalExpertsList = [
            {'jia':true,'jian':false,'expertType':''}
        ];
        
        $scope.shareCopyWriting = '妈妈100邀请您一起参加合生元活动吧！';
        $scope.receiveRules = '';

        $scope.description1 = '';
        $scope.description1_num = 0;
        $scope.$watch('description1', function (newValue, oldValue) {
            $scope.description1_num = newValue.length;
        });
        $scope.description2 = '';
        $scope.description2_num = 0;
        $scope.$watch('description2', function (newValue, oldValue) {
            $scope.description2_num = newValue.length;
        });
        $scope.receiveRules_num = 0;
        $scope.$watch('receiveRules', function (newValue, oldValue) {
            $scope.receiveRules_num = newValue.length;
        });
        getTemplate($scope,$http);
        $scope.player = document.getElementById('musicPlayer');

        //仅用于页面样式展示
        $scope.stepNum = 1;
        $scope.stepFun = function(stepNum) {
            if($scope.actPage == '0'){
                return
            };
            if($scope.saveDisabled == false){
                if($scope.stepNum==1 && (stepNum==2 || stepNum==3 || stepNum==4) && !checkFormAll($scope)){
                    return
                }else if($scope.stepNum==1 && stepNum==4){
                    return
                }else if($scope.stepNum==2 && stepNum==4){
                    return
                }else if($scope.stepNum==3 && stepNum==4 && !checkPage($scope)){
                    return
                }else if($scope.stepNum==3 && stepNum==4 && $scope.saveDisabled == false){
                    return
                }else{
                    $scope.stepNum = stepNum;  
                }
            }else{
                $scope.stepNum = stepNum;
            }
        };

        // 上一步
        $scope.prevStep = function(){
            $scope.stepNum--;
        };
        //下一步
        $scope.nextStep = function(){
            if($scope.saveDisabled == false){
                if($scope.stepNum==1 && !checkFormAll($scope)){
                    return
                }else{
                    $scope.stepNum++;
                }
            }else{
                $scope.stepNum++;
            }

            if($scope.teacherType == '3'){
                //清除内部讲师
                $scope.teacherId = '';
                $scope.teacherCode = '';
                $scope.teacherName = '';
                $scope.teacherSynops = '';
                $scope.lecturerRank = '';
                $scope.lecturerAccount = '';
                $scope.teacherInfo = '';
            }
        };

        // 保存
        $scope.saveActivityFn = function(style){
            if(style == 0){
                if(!checkFormAll($scope)){
                    return
                }else{
                    if($scope.saveDisabled){
                        alert('请不要重复点击保存');
                        return;
                    };
                    saveActivity($scope,$http,0);
                }
            }else{
                if(!checkFormAll($scope) || !checkPage($scope)){
                    return
                }else{
                    if($scope.saveDisabled){
                        alert('请不要重复点击保存');
                        return;
                    };
                    saveActivity($scope,$http,1);
                    
                }
            }
        };

        // 保存海报到本地
        $scope.saveImg = function(){
            var url = $scope.posterImageUrl;
            var a = document.createElement('a')
            var event = new MouseEvent('click')
            a.download = '线下课堂海报' + $scope.activityId;
            a.href = url;
            a.dispatchEvent(event);
        };

        // 返回活动查询列表
        $scope.saveBack = function(){
            window.location.href="activityIndex.html?"+$scope.token;
        };

        //初始化
        //品牌（00：全品牌，01：合生元，02:素加，03:葆艾，04：HT，05：SWISSE，06：Dodie）
        $scope.brandList = [
            {'name':"全品牌",'value':"00"},
            {'name':"合生元",'value':"01"},
//            {'name':"素加",'value':"02"},
//            {'name':"葆艾",'value':"03"},
            {'name':"HT",'value':"04"},
            {'name':"SWISSE",'value':"05"},
            {'name':"Dodie",'value':"06"}
        ];
        angular.forEach($scope.brandList,function(brandList){
            brandList.brand = false;
        });
        //全品牌
        $scope.brandFun = function(item) {
            if(item.value == '00') {
                if($scope.brandList[0].brand==true) {
                    angular.forEach($scope.brandList,function(brandList){
                        brandList.brand = true;
                    });
                }else{
                    angular.forEach($scope.brandList,function(brandList){
                        brandList.brand = false;
                    });
                };
            }else{
                if(item.brand == false) {
                    $scope.brandList[0].brand = false;
                }
                var num = 0;
                for(var i=1;i<$scope.brandList.length;i++){
                    if($scope.brandList[i].brand == true){
                        num++;
                    }
                }
                if(num == $scope.brandList.length-1){
                    $scope.brandList[0].brand = true;
                }
            };
        };

        //关联品类
        $scope.categoryList = [
            {'name':'合生元奶粉','value':'1'},
            {'name':'可贝思羊奶粉','value':'36706'},
            {'name':'益生菌','value':'201'},
            {'name':'Swisse','value':'26509'},
            {'name':'HT','value':'21706'},
            {'name':'Dodie','value':'33206'}
        ];
        angular.forEach($scope.categoryList,function(brandList){
            brandList.category = false;
        });

        //FTF主题
        $scope.themeList = [
            {'name':'全品场FTF','value':'1'},
            {'name':'Dodie专场FTF','value':'2'},
            {'name':'羊奶专场FTF','value':'3'}
        ];

        //讲师类型
        $scope.teacherTypeList = [
            {'name':'内部讲师','value':'1'},
            {'name':'兼职讲师','value':'3'}
        ];
        $scope.teacherType = $scope.teacherTypeList[0].value;

        //活动类型（0:SA-FTF,1:门店-FTF,2:学术推广,3：内训,4：Dodie专场-FTF,5：线上社群）
        $scope.typeList = [ 
            {'name':"SA-FTF",'value':"0"},
            // {'name':"门店-FTF",'value':"1"},
            {'name':"学术推广",'value':"2"},
            // {'name':"内训",'value':"3"},
            {'name':"线上社群",'value':"5"},
            // {'name':"Dodie专场-FTF",'value':"4"}
        ];
        $scope.type = $scope.typeList[0].value;
        //是否选择课程名称、课程讲师的标识
        $scope.isSelectCourse = false;
        $scope.isSelectTeacher = false;

        //活动类型改变时，课程清空
        $scope.typeChange = function() {
            $scope.courseInfo = '';
            $scope.field = '1';
            $scope.consumerType = '';
            if($scope.type == "2"){
                $scope.expertTypeDisabled = false;
                // $scope.expertType = $scope.expertTypeList[1].value;
                $scope.expertTypeList[0] = {'name':'--请选择--','value':''},
                $scope.expertType = $scope.expertTypeList[0].value;
                $scope.NBexpertType = $scope.expertTypeList[0].value;
                angular.forEach($scope.externalExpertsList, function (item){
                    item.expertType = '';
                })
            }else{
                $scope.expertTypeDisabled = true;
                $scope.expertTypeList[0] = {'name':'专家讲座','value':1},
                $scope.expertType = $scope.expertTypeList[0].value;
                $scope.NBexpertType = '';
            };

            if($scope.type == "0"){
                $scope.fieldList = angular.copy($scope.fieldData1);
                $scope.field = $scope.fieldList[0].value;
            }else if($scope.type == "5"){
                $scope.fieldList = angular.copy($scope.fieldData2);
                $scope.field = $scope.fieldList[0].value;
            };
        };

        //是否生成活动页面 '1':是,'0':否
        $scope.actPage = '1';

        //选择模板
        $scope.templateSelectFn = function(item){
            angular.forEach($scope.templateList,function(template){
                template.isSelect = false;
            });
            $scope.currentTemplate = item;
            item.isSelect = true;
            $scope.ylNum = 1;
            $scope.pageNum = 1;

            $scope.pageYL = {'background-image':'url(./images/template/' + $scope.currentTemplate.id + '_' + $scope.ylNum + '.png)'};
            $scope.left_arrow = {'background-image':'url(./images/arrow_l1.png)'};
            $scope.right_arrow = {'background-image':'url(./images/arrow_r2.png)'};
            var bgUrl = './images/template/' + 'bg' + $scope.currentTemplate.id + '_' + '1' + '.png';
            var bg = 'url(' + bgUrl + ')';
            $scope.bg = {"background-image":bg};
            $scope.posterBgUrl = './images/' + 'share' + $scope.currentTemplate.id + '.png';

            if(item.id=='1' || item.id=='3'){
                $scope.topPosition1 = {'top':'285px','padding':'0 20px'};
            }else if(item.id=='2'){
                $scope.topPosition1 = {'top':'180px','padding':'0 20px'};
            }else if(item.id=='4'){
                $scope.topPosition1 = {'top':'210px','padding':'0 28px'};
            };
            if(item.id=='1'){
                $scope.topPosition2 = {'margin-top':'170px'};
                $scope.topPosition3 = {'margin-top':'85px'};
            }else if(item.id=='2' || item.id=='3'){
                $scope.topPosition2 = {'margin-top':'100px'};
                $scope.topPosition3 = {'margin-top':'50px'}
            }else if(item.id=='4'){
                $scope.topPosition2 = {'margin-top':'80px'};
                $scope.topPosition3 = {'margin-top':'50px'}
            };
            if(item.id=='4'){
                $scope.prizePOS = {'margin-top':'54px'};
            }else{
                $scope.prizePOS = {'margin-top':'95px'};
            }

            $scope.musicShow = true;
            $scope.music = '1';
            if(item.id==1){
                // $scope.currentMusicUrl = $sce.trustAsResourceUrl($scope.currentTemplate.bgMusic);
                $scope.musicUrl = './images/music1.png';
                $scope.currentMusicUrl = $sce.trustAsResourceUrl('./images/music/music1.mp3');
            }else if(item.id==2){
                $scope.musicUrl = './images/music1.png';
                $scope.currentMusicUrl = $sce.trustAsResourceUrl('./images/music/music2.mp3');
            }else if(item.id==3){
                $scope.musicUrl = './images/music3.png';
                $scope.currentMusicUrl = $sce.trustAsResourceUrl('./images/music/music3.mp3');
            }else if(item.id==4){
                $scope.musicUrl = './images/music1.png';
                $scope.currentMusicUrl = $sce.trustAsResourceUrl('./images/music/music3.mp3');
            };;

            //默认活动图片、描述
            if(item.id=='2'){
                $scope.imgSrcGPTwo = 'https://img3.mama100.com/site/mobile/img/merchant/mkt/ftf/2018/07/31/2018073117493885486.png';
                $scope.imgSrcGPThree = 'https://img3.mama100.com/site/mobile/img/merchant/mkt/ftf/2018/07/31/20180731174942149606.png';
                $scope.description1 = '法式亲子瑜伽动教学，亲密育儿&完美修复。';
                $scope.description2 = '权威专家现场授课，科学育儿巧避雷区。';
            }else if(item.id=='3'){
                $scope.imgSrcGPTwo = 'https://img3.mama100.com/site/mobile/img/merchant/mkt/ftf/2018/07/31/20180731175409163915.png';
                $scope.imgSrcGPThree = 'https://img3.mama100.com/site/mobile/img/merchant/mkt/ftf/2018/07/31/20180731175414200289.png';
                $scope.description1 = '孕产操互动教学挺着大肚子也能优雅自信。';
                $scope.description2 = '权威专家现场授课轻松掌握孕期护理知识';
            }else{
                $scope.imgSrcGPTwo = ''
                $scope.imgSrcGPThree = '';
                $scope.description1 = '';
                $scope.description2 = '';
            }
            $scope.description1_num = $scope.description1.length;   
            $scope.description2_num = $scope.description2.length; 
        };

        //选择页数
        $scope.ylNumFn = function(type){
            if(type == 0){
                $scope.ylNum--;
                if($scope.ylNum <= 1){
                    $scope.ylNum = 1;
                }
            }else{
                $scope.ylNum++;
                if($scope.currentTemplate.id==1){
                    if($scope.ylNum >= 2){
                        $scope.ylNum = 2;
                    }
                }else{
                    if($scope.ylNum >= 4){
                        $scope.ylNum = 4;
                    }
                }
            }
            // $scope.ylNum = ylNum;
            $scope.pageYL = {'background-image':'url(./images/template/' + $scope.currentTemplate.id + '_' + $scope.ylNum + '.png)'};
            if($scope.ylNum == 1){
                $scope.left_arrow = {'background-image':'url(./images/arrow_l1.png)'}
            }else{
                $scope.left_arrow = {'background-image':'url(./images/arrow_l2.png)'}
            };
            if($scope.ylNum == 4 || ($scope.currentTemplate.id==1 && $scope.ylNum == 2)){
                $scope.right_arrow = {'background-image':'url(./images/arrow_r1.png)'}
            }else{
                $scope.right_arrow = {'background-image':'url(./images/arrow_r2.png)'}
            }
        };
        $scope.pageFn = function(type){
            if(type == 0){
                $scope.pageNum--;
            }else{
                $scope.pageNum++;
            }
            // $scope.pageNum = pageNum;
            var bgUrl = './images/template/' + 'bg' + $scope.currentTemplate.id + '_' + $scope.pageNum + '.png';
            var bg = 'url(' + bgUrl + ')';
            $scope.bg = {"background-image":bg};
        };

        //是否使用背景音乐
        $scope.isPlayFn = function(){
            if($scope.music == '0'){
                $scope.musicShow = false;
                $scope.player.pause();
                $scope.player.load();
                $scope.currentMusicUrl = '';
            }else{
                $scope.musicShow = true;
                if($scope.currentTemplate.id==1 || $scope.currentTemplate.id==2 || $scope.currentTemplate.id==4){
                    $scope.musicUrl = './images/music1.png';
                }else{
                    $scope.musicUrl = './images/music3.png';
                }
            }
        };

        //根据身份显示相应的大区和办事处、渠道
        identityVerification6($scope, $http).then(function(){
            //针对页面埋点
            var date = new Date();
            var has = md5('6'+'6060101'+date.getTime()+$scope.userId);
            var dataMd = {
                "platform":6, //0:mama100Wechat，1:mama100app，2:SwisseWechat，HYTapp，3:HYTwechat，4:DodieWechat，5:HealthyTimesWechat，6:PC
                "point_code":"6060101",  //具体详情见主题类型编码表
                "created_time": date.getTime(),
                "customer_id":$scope.userId,  //用户会员的ID标识
                "sign":has
            };
            eventpv($scope,$http,dataMd);
        });

        //事业部修改，大区跟着改变
        $scope.buChange = function(){
            buChange($scope);
        };
        //大区修改，办事处跟着改变
        $scope.areaChange = function(){
            getDepartments6($scope, $http);
        };

        //选择省市区
        findSubordinateRegionInfo($scope, $http,'',1);
        $scope.changReginInfo=function(regionCode,type){
            findSubordinateRegionInfo($scope, $http,regionCode,type);
        };

        //由directive 获取上传的图片以及图片地址
        $scope.uploadFileGPZero = '';                       
        $scope.imgSrcGPZero = '';
        $scope.imgPathGPZero = '';
        $scope.fileNameGPZero = '';

        $scope.uploadFileGPOne = '';                      
        $scope.imgSrcGPOne = '';
        $scope.imgPathGPOne = '';
        $scope.fileNameGPOne = '';

        $scope.uploadFileGPTwo = '';                      
        $scope.imgPathGPTwo = '';
        $scope.fileNameGPTwo = '';
        
        $scope.uploadFileGPThree = '';
        $scope.imgPathGPThree = '';
        $scope.fileNameGPThree = '';

        $scope.prizeFileNameList = [];
        $scope.prizeFileImgList = [];
        $scope.prizeYouPaiImgList = [];
        $scope.getFile = function (file, imgSrc, type) {
            $scope.imgNo = type;
            // console.log(file)
            if(type=='0'){
                $scope.uploadFileGPZero = file;                       //文件
                $scope.imgSrcGPZero = imgSrc;                         //base64格式
                $scope.imgPathGPZero = imgSrc.replace("data:image/","");
                $scope.imgPathGPZero = $scope.imgPathGPZero.replace(";base64,","");
                $scope.fileNameGPZero = file.files[0].name;           //文件名称

                $scope.activityId = $scope.fileNameGPZero;
                $scope.base64 = $scope.imgSrcGPZero;
                //getSignature($scope,$http,1);//获取又拍云签名上传
                $scope.logoShow = true;
            }else if(type=='1'){
                $scope.uploadFileGPOne = file;                       //文件
                $scope.imgSrcGPOne = imgSrc;                         //base64格式
                $scope.imgPathGPOne = imgSrc.replace("data:image/","");
                $scope.imgPathGPOne = $scope.imgPathGPOne.replace(";base64,","");
                $scope.fileNameGPOne = file.files[0].name;           //文件名称

                $scope.activityId = $scope.fileNameGPOne;
                $scope.base64 = $scope.imgSrcGPOne;
                // getSignature($scope,$http,2);//获取又拍云签名上传
            }else if(type=='2'){
                $scope.uploadFileGPTwo = file;                       //文件
                $scope.imgSrcGPTwo = imgSrc;                         //base64格式
                $scope.imgPathGPTwo = imgSrc.replace("data:image/","");
                $scope.imgPathGPTwo = $scope.imgPathGPTwo.replace(";base64,","");
                $scope.fileNameGPTwo = file.files[0].name;           //文件名称

                $scope.activityId = $scope.fileNameGPTwo;
                $scope.base64 = $scope.imgSrcGPTwo;
                //getSignature($scope,$http,3);//获取又拍云签名上传
            }else if(type=='3'){
                $scope.uploadFileGPThree = file;                       //文件
                $scope.imgSrcGPThree = imgSrc;                         //base64格式
                $scope.imgPathGPThree = imgSrc.replace("data:image/","");
                $scope.imgPathGPThree = $scope.imgPathGPThree.replace(";base64,","");
                $scope.fileNameGPThree = file.files[0].name;           //文件名称

                $scope.activityId = $scope.fileNameGPThree;
                $scope.base64 = $scope.imgSrcGPThree;
                //getSignature($scope,$http,4);//获取又拍云签名上传
            }else if(type=='4'){
                $scope.activityId = file.files[0].name;
                $scope.base64 = imgSrc;
                // getSignature($scope,$http,5);//获取又拍云签名上传
                
                $scope.prizeFileNameList.push(file.files[0].name);
                $scope.prizeFileImgList.push(imgSrc);
            }
            // importPointOSS($scope,$http,file.files,'saAct');//OSS上传
            importPointOSS($scope,$http,dataURLtoFile($scope.base64,$scope.activityId),'saAct');
        };

        //清除图片
        $scope.clearImg = function(index) {
            $scope.prizeFileNameList.splice(index,1);
            $scope.prizeFileImgList.splice(index,1);
            // uploader[4].splice(index,1);
            $scope.prizeYouPaiImgList.splice(index,1);
        };

        //右滑动
        var prizeUl = document.getElementById('prizeUl'); 
        $scope.rightSwipper = function(){
            if(prizeUl.style.marginLeft == '-127.5px'){
                prizeUl.style.marginLeft = '-255px';
            }else if(prizeUl.style.marginLeft == '0px'){
                prizeUl.style.marginLeft = '-127.5px';
            }else{
                return
            }
        };

        //左滑动
        $scope.leftSwipper = function(){
            if(prizeUl.style.marginLeft == '-255px'){
                prizeUl.style.marginLeft = '-127.5px';
            }else if(prizeUl.style.marginLeft == '-127.5px'){
                prizeUl.style.marginLeft = '0px';
            }else{
                return
            }
        };


        //load出对应课程信息
        $scope.courseFun = function (ev, keyword) {
            // $scope.isSelectCourse = false;
            $scope.courseShow = true;
            $scope.courseId = '';
            if(keyword == undefined){
                keyword = "";
            }
            getCurriculum($scope, $http, keyword);
        };
        //失去焦点隐藏
        $scope.blurFn = function (ev){
            $scope.teacherList = [];
            $scope.courseShow = false;
            $scope.teacherShow = false;
            $scope.ptTeacherShow = false;
            angular.forEach($scope.externalExpertsList,function (item){
                item.teacherShow = false;
            })
        };
        //选择相应课程
        $scope.courseSel = function(item) {
            // $scope.isSelectCourse = true;
            $scope.courseInfo = item.name;
            var synops = item.synops;//课程简介
            synops = synops.replace(/(\n)/g, "").replace(/(\t)/g, "").replace(/(\r)/g, "").replace(/<\/?[^>]*>/g, "").replace(/\s*/g, "");
            $scope.courseIntroduce = synops;
            $scope.courseRecomGroups = item.recomGroups;//推荐人群
            $scope.courseId = item.id;
            $scope.previewUrl = item.imgPath;
        };

        //load出对应讲师信息
        $scope.teacherFun = function (type, keyword) {
            // $scope.isSelectTeacher = false;
            $scope.teacherShow = true;
            $scope.teacherId = '';
            if(Biostime.common.isEmpty(keyword)){
                keyword = "";
                $scope.teacherCode = '';
                $scope.teacherName = '';
                $scope.lecturerRank = '';
                $scope.lecturerAccount = '';
            }
            getTeacher($scope,$http,'0',keyword);
        };
        //选择相应讲师
        $scope.teacherSel = function(item) {
            // $scope.isSelectTeacher = true;
            $scope.teacherId = item.id;
            $scope.teacherCode = item.code;
            $scope.teacherName = item.name;
            var synops = item.synops;
            synops = synops.replace(/(\n)/g, "").replace(/(\t)/g, "").replace(/(\r)/g, "").replace(/<\/?[^>]*>/g, "").replace(/\s*/g, "");
            $scope.teacherSynops = synops;
            $scope.lecturerRank = item.rank;
            $scope.lecturerAccount = item.lecturerAccount;
            if(item.lecturerAccount){
                $scope.teacherInfo = item.name+"/"+item.lecturerAccount;
            }else{
                $scope.teacherInfo = item.name;
            }
        };

        //load出对应讲师信息（兼职讲师）
        $scope.ptTeacherFun = function (type, keyword) {
            // $scope.isSelectTeacher = false;
            $scope.ptTeacherShow = true;
            $scope.ptTeacherId = '';
            if(Biostime.common.isEmpty(keyword)){
                keyword = "";
                $scope.ptTeacherCode = '';
                $scope.ptTeacherName = '';
                $scope.ptTeacherRank = '';
                $scope.ptTeacherAccount = '';
            }
            getTeacher($scope,$http,'3',keyword);
        };
        //选择相应讲师（兼职讲师）
        $scope.ptTeacherSel = function(item) {
            // $scope.isSelectTeacher = true;
            $scope.ptTeacherId = item.id;
            $scope.ptTeacherCode = item.code;
            $scope.ptTeacherName = item.name;
            var synops = item.synops;
            synops = synops.replace(/(\n)/g, "").replace(/(\t)/g, "").replace(/(\r)/g, "").replace(/<\/?[^>]*>/g, "").replace(/\s*/g, "");
            $scope.ptTeacherSynops = synops;
            $scope.ptTeacherRank = item.rank;
            $scope.ptTeacherAccount = item.lecturerAccount;
            if(item.lecturerAccount){
                $scope.ptTeacherInfo = item.name+"/"+item.lecturerAccount;
            }else{
                $scope.ptTeacherInfo = item.name;
            }
        };

        // load出对应讲师信息(外部专家)
        $scope.externalExpertsFn = function(ev,item,keyword){
            item.teacherShow = true;
            item.teacherId = '';
            if(Biostime.common.isEmpty(keyword)){
                keyword = "";
                item.teacherCode = '';
                item.teacherName = '';
                item.lecturerRank = '';
            }
            getTeacher($scope,$http,'1',keyword);
        };
        //选择相应讲师(外部专家)
        $scope.externalExpertsSel = function(item,oItem) {
            // $scope.isSelectTeacher = true;
            item.teacherInfo = oItem.name+"/"+oItem.code;
            item.teacherId = oItem.id;
            item.teacherCode = oItem.code;
            item.teacherName = oItem.name;
            var synops = oItem.synops;
            synops = synops.replace(/(\n)/g, "").replace(/(\t)/g, "").replace(/(\r)/g, "").replace(/<\/?[^>]*>/g, "").replace(/\s*/g, "");
            item.teacherSynops = synops;
            item.lecturerRank = oItem.rank;
        };
        //加
        $scope.jiaFn = function(index,item) {
            if(index==0){
                item.jia = false;
                item.jian = true;
                $scope.externalExpertsList.push({'jia':true,'jian':true,'expertType':''});
            }else if(index==1){
                item.jia = false;
                $scope.externalExpertsList.push({'jia':false,'jian':true,'expertType':''});
            }
        };
        //减
        $scope.jianFn = function(index,item) {
            $scope.externalExpertsList.splice(index,1);
            if($scope.externalExpertsList.length==2){
                $scope.externalExpertsList[1].jia = true;
                $scope.externalExpertsList[1].jian = true;
            }else if($scope.externalExpertsList.length==1){
                $scope.externalExpertsList[0].jia = true;
                $scope.externalExpertsList[0].jian = false;
            }
        };

        //根据SA编号，查询门店编号、门店名称
        $scope.checkSACode = function () {
            if($scope.saCode != "" && $scope.saCode != undefined){
                getSaTerminal($scope, $http);
            }
        };
        //根据查询门店编号、门店名称
        $scope.checkTerminalCode = function () {
            if($scope.terminalCode != "" && $scope.terminalCode != undefined){
                getTerminal($scope, $http);
            }
        };
        
        //取消
        $scope.cancel = function () {
            if(confirm("确认取消？取消后数据将被清空")){
                reset($scope,$http);
            }
        };


        //验证导购ID是否存在
        $scope.shoppingGuideFun = function(){
            if($scope.shoppingGuideID == ''){
                $scope.dataDesc = '';
                $scope.shoppingGuideIdError = false;
                return;
            };
            queryShoppingGuideId($scope,$http);
        };

        //导购ID变化时清空错误提示
        $scope.shoppingGuideChange = function(){
            $scope.dataDesc = '';
            $scope.shoppingGuideIdError = false;
        };

        //通用关闭
        $scope.close = function(){
            easyDialog.close();
        };

    };
    return {
        templateCtrl6: templateCtrl6
    }
});
//根据身份，显示相应的大区和办事处、渠道,获取登录人信息
function identityVerification6($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;

        //获取登录人信息
        $scope.user = data.loadUserBaseInfoBean;
        $scope.userId = data.loadUserBaseInfoBean.userId;
        $scope.userName = data.loadUserBaseInfoBean.userName;
        $scope.accountId = data.loadUserBaseInfoBean.id;
        if(data.loadUserBaseInfoBean.officeCode == null && data.loadUserBaseInfoBean.areaCode == null){
            $scope.areaOfficeCode = "";
        }else if(data.loadUserBaseInfoBean.officeCode == null && data.loadUserBaseInfoBean.areaCode != null){
            $scope.areaOfficeCode = data.loadUserBaseInfoBean.areaCode;
        }else if(data.loadUserBaseInfoBean.officeCode != null){
            $scope.areaOfficeCode = data.loadUserBaseInfoBean.officeCode;
        }

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
            $scope.areaEditDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
            $scope.departmentEditDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
            $scope.departmentEditDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);
    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments6($scope, $http){
    $scope.departmentDisabled=false;
    if($scope.area.areaCode != ""){
        $scope.departments=[{
            areaId:'',
            officeId:'',
            officeCode:'',
            officeName:"全部"
        }];
    }else{
        $scope.departmentDisabled=true;
    }
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}

//选择省市区
function findSubordinateRegionInfo($scope, $http,regionCode,type){

    var url = useId+"mkt-common/baseInfo/findSubordinateRegionInfo.action?regionCode="+regionCode+"&tier="+type;
    return $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        var provinceSelect;
        var citySelect;
        var areaSelect;

        if(type==1){
            $scope.provinceOption=data.response;
            $scope.cityOption = [];
            $scope.areaOption = [];
            provinceSelect={
                regionCode:'',
                regionName:'请选择省份',
                tier:'',
                parentRegionCode:''
            };
            $scope.provinceOption.splice(0,0,provinceSelect);
            $scope.province = $scope.provinceOption[0];

            citySelect={
                regionCode:'',
                regionName:'请选择市',
                tier:'',
                parentRegionCode:''
            };
            $scope.cityOption.splice(0,0,citySelect);
            $scope.city = $scope.cityOption[0];

            areaSelect={
                regionCode:'',
                regionName:'请选择区',
                tier:'',
                parentRegionCode:''
            };
            $scope.areaOption.splice(0,0,areaSelect);
            $scope.district = $scope.areaOption[0];

        }else  if(type==2){
            $scope.cityOption=data.response;
            citySelect={
                regionCode:'',
                regionName:'请选择市',
                tier:'',
                parentRegionCode:''
            };
            $scope.cityOption.splice(0,0,citySelect);
            $scope.city = $scope.cityOption[0];

            areaSelect={
                regionCode:'',
                regionName:'请选择区',
                tier:'',
                parentRegionCode:''
            };
            $scope.areaOption.splice(0,0,areaSelect);
            $scope.district = $scope.areaOption[0];
            
        }else  if(type==3){
            $scope.areaOption=data.response;
            areaSelect={
                regionCode:'',
                regionName:'请选择区',
                tier:'',
                parentRegionCode:''
            };
            $scope.areaOption.splice(0,0,areaSelect);
            $scope.district = $scope.areaOption[0];
        }

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}
/**
 * 获得课程讲师列表
 * @param $scope
 * @param $http
 * @param keyword
 */
function getTeacher($scope, $http, type, keyword){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "token":Biostime.common.getQueryString('token')?Biostime.common.getQueryString('token'):Biostime.common.getQueryString('userid'),
        "lecturerType":type,
        "lecturerName":keyword
    }};
    // var url = "http://10.50.101.162:8080/samanage/ftf/activity/queryLecturerList.do";
    var url = BASICURL + "samanage/ftf/activity/queryLecturerList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (Biostime.common.isEmpty(data.response)) {
                // Biostime.common.toastMessage("无匹配的讲师");
                $scope.teacherList = [];
            }else if (data.response.length > 0) {
                $scope.teacherList = data.response;
                for(var i=0; i<$scope.teacherList.length; i++){
                    if(type != 2){
                        if($scope.teacherList[i].lecturerAccount){
                            $scope.teacherList[i].teacherInfo = $scope.teacherList[i].name + '/' + $scope.teacherList[i].lecturerAccount;
                        }else{
                            $scope.teacherList[i].teacherInfo = $scope.teacherList[i].name;
                        }
                    }else{
                        $scope.teacherList[i].teacherInfo = $scope.teacherList[i].name + '/' + $scope.teacherList[i].code;
                    }
                }
                $scope.teacher = $scope.teacherList[0];
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 获得课程名称列表
 * @param $scope
 * @param $http
 * @param keyword
 */
function getCurriculum($scope, $http, keyword){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "status":1,
        "actType":$scope.type,
        "name":keyword
    }};
    var url = BASICURL + "samanage/ftf/curriculum/queryFtfCurriculumList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (Biostime.common.isEmpty(data.response.datas)) {
                // Biostime.common.toastMessage("无匹配的课程");
                $scope.curriculumList = [];
            }else if (data.response.datas.length > 0) {
                $scope.curriculumList = data.response.datas;
                $scope.curriculum = $scope.curriculumList[0];
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 获得活动模板列表
 * @param $scope
 * @param $http
 * @param keyword
 */
function getTemplate($scope, $http){
    Biostime.common.showLoading();
    var url = BASICURL + "samanage/ftf/activity/queryFtfActivityTemplateList.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if (data.response == null) {
                Biostime.common.toastMessage("无匹配的模板");
                $scope.templateList = [];
            }else if (data.response.length > 0) {
                $scope.templateList = data.response;
                $scope.templateSelectFn($scope.templateList[0]);
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 根据门店编号，查询门店编号、门店名称
 * @param $scope
 * @param $http
 */
function getTerminal($scope, $http){
    Biostime.common.showLoading();
    var queryData = {
        "terminalCode": $scope.terminalCode
    };
    var url = BASICURL + "samanage/ftf/activity/queryTerminal.action" + '?' + $scope.token;
    $http({
        method: 'post',
        url: url,
        params: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if(data.response != null){
                $scope.isTerminalCode = true;
                $scope.terminalCode = data.response.code;
                $scope.terminalName = data.response.name;
            }else{
                $scope.isTerminalCode = false;
                $scope.terminalName="门店编码不存在";
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}

/**
 * 验证导购ID是否存在
 * @param $scope
 * @param $http
 */
function queryShoppingGuideId($scope, $http){
    $scope.shoppingGuideIdError = false;
    $scope.dataDesc = '';
    var queryData = {
        "shoppingGuideId": $scope.shoppingGuideID
    };
    var url = BASICURL + "samanage/ftf/activity/queryShoppingGuideId.action?"+$scope.token;
    $http({
        method: 'POST',
        url: url,
        params: queryData
    }).success(function (data, status, headers, config) {
        if(data.code == 200) {
            $scope.dataDesc = data.desc;
        }else if(data.code != 100&&data.code != 200){
            Biostime.common.toastMessage(data.desc);
        };
        if(data.code!=100) {
            $scope.shoppingGuideIdError = true;
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 根据SA编号，查询门店编号、门店名称
 * @param $scope
 * @param $http
 */
function getSaTerminal($scope, $http){
    Biostime.common.showLoading();
    var queryData = {
        "saCode": $scope.saCode
    };
    var url = BASICURL + "samanage/ftf/activity/querySaTerminal.action" + '?' + $scope.token;
    $http({
        method: 'GET',
        url: url,
        params: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            if(data.response != null){
                $scope.isSaTerminalCode = true;
                $scope.saTerminalName = data.response.name;
                // $scope.terminalCode = data.response.code;
                // $scope.terminalName = data.response.ctName;
                getAddress($scope, $http)
            }else{
                $scope.isSaTerminalCode = false;
                // $scope.terminalCode = '';
                // $scope.terminalName = '';
                $scope.saTerminalName="SA编码不存在";
                findSubordinateRegionInfo($scope, $http,'',1);
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 根据SA编号，查询省市区
 * @param $scope
 * @param $http
 */
function getAddress ($scope, $http){
    Biostime.common.showLoading();
    var queryData = {
        "terminalCode": $scope.saCode
    };
    var url = addressURL + 'terminal2.0/v1/terminal-service-oacallback/api/terminal/getTerminalRegion' + '?' + $scope.token;
    // var url = 'http://10.50.115.4:9129/terminal2.0/v1/terminal-service-oacallback/api/terminal/getTerminalRegion';
    $http({
        method: 'GET',
        url: url,
        params: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data) {
            // console.log(data)
            if(data.provinceName){
                angular.forEach($scope.provinceOption,function (item){
                    if(data.provinceName == item.regionName){
                        $scope.province = item;
                        findSubordinateRegionInfo($scope, $http,item.regionCode,2).then(function(){
                            if(data.cityName){
                                angular.forEach($scope.cityOption,function (item2){
                                    if(data.cityName == item2.regionName){
                                        $scope.city = item2;
                                        findSubordinateRegionInfo($scope, $http,item2.regionCode,3).then(function(){
                                            if(data.districtName){
                                                angular.forEach($scope.areaOption,function (item3){
                                                    if(data.districtName == item3.regionName){
                                                        $scope.district = item3;
                                                    }
                                                })
                                            }else{
                                                $scope.district = $scope.areaOption[0];
                                            }
                                        })
                                    }
                                })
                            }else{
                                findSubordinateRegionInfo($scope, $http,'',2);
                            }
                        })
                    }
                })
            }else{
                findSubordinateRegionInfo($scope, $http,'',1);
            }
        }else{
            findSubordinateRegionInfo($scope, $http,'',1);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}

/**
 * 保存活动
 * @param $scope
 * @param $http
 */
function saveActivity($scope, $http, style){
    if($scope.user.buCode == '10'){
        Biostime.common.toastMessage('该功能只用于BNC事业部');
        return
    }
    $scope.saveDisabled = true;
    Biostime.common.showLoading();
    var queryData;
    var ftfActLecturer;
    if($scope.teacherType == '1'){
        ftfActLecturer = [
            {'lecturerName':$scope.teacherName,'lecturerCode':$scope.teacherCode,'lecturerRank':$scope.lecturerRank,'expertType':$scope.NBexpertType,'lecturerAccount':$scope.lecturerAccount,'lecturerType':1}
        ];
    }else if($scope.teacherType == '3'){
        ftfActLecturer = [
            {'lecturerName':$scope.ptTeacherName,'lecturerCode':$scope.ptTeacherCode,'lecturerRank':'','expertType':'','lecturerAccount':$scope.ptTeacherAccount,'lecturerType':3}
        ];
    }
    angular.forEach($scope.externalExpertsList, function (item){
        if(item.teacherCode){
            ftfActLecturer.push({'lecturerName':item.teacherName,'lecturerCode':item.teacherCode,'lecturerRank':item.lecturerRank,'expertType':item.expertType,'lecturerType':2});
        }
    });
    if(style == 0){
        queryData = {
            'request':{
                "createPage": '0',//是否生成页面
                "brand": $scope.category,                                  //关联品类code
                "brandName": $scope.categoryName,                          //关联品类name
                "ftfTheme": $scope.ftfTheme,                          //关联品牌
                "areaOfficeCode": $scope.department.officeCode != "" ? $scope.department.officeCode : $scope.area.areaCode != "" ? $scope.area.areaCode : $scope.buId.buCode,  //大区办事处编码
                "type":$scope.type,                                     //活动类型
                "expertType":$scope.type=='2'?$scope.expertType:1,    //专家支持类型
                "saCode":$scope.saCode,                                 //SA编号
                "terminalCode":$scope.terminalCode,                     //门店编号
                "curriculumId": $scope.courseId,                        //课程名称Id
                // "lecturerRank":$scope.lecturerRank,                      //讲师头衔
                // "lecturerId": $scope.teacherId,                         //讲师Id
                "actDate":$scope.actDate,                               //活动日期
                "startTime":$scope.startTime,                           //活动开始时间
                "endTime":$scope.endTime,                               //活动结束时间
                "province":$scope.province.regionName,                  //省
                "city":$scope.city.regionName,                          //市
                "district":$scope.district.regionName,                  //区
                "address":$scope.road,                               //活动地址
                "sharingCopywriting":$scope.shareCopyWriting,           //分享文案
                "userId":$scope.userId,                                 //登录人Id
                "userName":$scope.userName,                             //登录人名称
                "ftfActLecturer":ftfActLecturer,
                "newCustomerCalculation":$scope.newCustomerCalculation         //新客计算方式
            }
        }
    }else{
        var list = [];
        if($scope.currentTemplate.id == 1){
            list = [
                {"key":"logImg","value":$scope.logImg},                             //Log图片
                {"key":"isPlayMusic","value":$scope.music},                         //是否播放背景音乐
            ];
        }else{
            var prizeImgList = '';
            for(var i=0; i<$scope.prizeYouPaiImgList.length;i++){
                prizeImgList += "{'serNo':'"+(i+1)+"','prizeImgPath':'"+$scope.prizeYouPaiImgList[i]+"'},"
            };
            // for(var i=0; i<$scope.prizeFileImgList.length;i++){
            //     prizeImgList += "{'serNo':'"+(i+1)+"','prizeImgPath':'"+$scope.prizeFileImgList[i]+"'},"
            // };
            var str = prizeImgList.substring(0,prizeImgList.length-1);
            str = "["+str+"]";

            if(Biostime.common.isEmpty($scope.actImgOne)){
                $scope.actImgOne = $scope.imgSrcGPTwo;
            };
            if(Biostime.common.isEmpty($scope.actImgTwo)){
                $scope.actImgTwo = $scope.imgSrcGPThree;
            };

            list = [
                {"key":"logImg","value":$scope.logImg},                             //Log图片
                {"key":"isPlayMusic","value":$scope.music},                         //是否播放背景音乐
                {"key":"lecturerImg","value":$scope.lecturerImg},                   //讲师头像

                {"key":"actInfo",                                                   //往期活动信息
                "value":
                "[{'serNo':'1','actDes':'" + $scope.description1 + "','actImgPath':'" + $scope.actImgOne + "'},{'serNo':'2','actDes':'" + $scope.description2 + "','actImgPath':'"+$scope.actImgTwo+"'}]"
                }, 

                {"key":"prizeImg","value":str},                            //活动奖品图片
                {"key":"receiveRules","value":$scope.receiveRules}                  //奖品领取规则
            ];
        }
        queryData = {'request':{
            "createPage": '1',//是否生成页面
            "brand": $scope.category,                                  //关联品类code
            "brandName": $scope.categoryName,                          //关联品类name
            "ftfTheme": $scope.ftfTheme,                          //关联品牌
            "areaOfficeCode": $scope.department.officeCode != "" ? $scope.department.officeCode : $scope.area.areaCode != "" ? $scope.area.areaCode : $scope.buId.buCode,  //大区办事处编码
            "type":$scope.type,                                     //活动类型
            "expertType":$scope.type=='2'?$scope.expertType:1,    //专家支持类型
            "saCode":$scope.saCode,                                 //SA编号
            "terminalCode":$scope.terminalCode,                     //门店编号
            "curriculumId": $scope.courseId,                        //课程名称Id
            // "lecturerRank":$scope.lecturerRank,                      //讲师头衔
            // "lecturerId": $scope.teacherId,                         //讲师Id
            "actDate":$scope.actDate,                               //活动日期
            "startTime":$scope.startTime,                           //活动开始时间
            "endTime":$scope.endTime,                               //活动结束时间
            "province":$scope.province.regionName,                  //省
            "city":$scope.city.regionName,                          //市
            "district":$scope.district.regionName,                  //区
            "address":$scope.road,                               //活动地址
            "sharingCopywriting":$scope.shareCopyWriting,           //分享文案
            "userId":$scope.userId,                                 //登录人Id
            "userName":$scope.userName,                             //登录人名称
            "actTemplateId":$scope.currentTemplate.id,              //模板ID
            "listKeyValue":list,                                        //模板内容
            "ftfActLecturer":ftfActLecturer,
            "newCustomerCalculation":$scope.newCustomerCalculation         //新客计算方式
        }};
    }
    //type-活动类型（0:SA-FTF,1:门店-FTF,2:学术推广,3：内训,4：Dodie专场-FTF,5：线上社群）
    if($scope.type=='0' || $scope.type=='5'){
        queryData.request.placeType = $scope.field;
    }
    if($scope.type=='0' || $scope.type=='1' || $scope.type=='4' || $scope.type=='5'){
        queryData.request.consumerType = $scope.consumerType;
    }
    // var url = "http://10.50.101.162:8080/" + "samanage/ftf/activity/saveFtfActivity.action" + '?' + $scope.token;
    var url = BASICURL + "samanage/ftf/activity/saveFtfActivity.action" + '?' + $scope.token;
    // console.log(queryData);
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        if(data.code == 100) {
            $scope.activityId = data.response.id;
            $scope.ftfQrCodeUrl = data.response.ftfQrCodeUrl;
            if(style == 0){
                // $scope.saveDisabled = true;
                setTimeout(function(){
                    Biostime.common.hideLoading();
                    window.location.href="activityIndex.html?"+$scope.token;
                },2000)
            }else{
                Biostime.common.hideLoading();
                /*
                 * 保存获取到activityId后，得到链接，合成海报，上传海报链接
                 */
                //分享海报上的二维码地址 d_activityId活动Id d_tempId模板ID d_termCodeSA编号
                var codeUrl = SHAREURL + "wmall/ftf/localActivity/qr_index.html?d_activityId="+$scope.activityId+"&d_tempId="+$scope.currentTemplate.id+"&d_termCode="+$scope.saCode;
                //合成二维码
                qrcode(codeUrl);
                //合成海报,调用又拍云上传图片接口,上传海报链接
                merge($scope,$http,$scope.currentTemplate.id);
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        $scope.saveDisabled = false;
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    });
}

/**
 * 检查表单
 * @param $scope
 * @returns {boolean}
 */
function checkFormAll($scope){

    /*//品牌类型
    $scope.brand = "";
    $scope.brandName = "";
    for(var i=0; i < $scope.brandList.length; i++){
        if($scope.brandList[i].brand == true) {
            $scope.brand += $scope.brandList[i].value + ",";
            $scope.brandName += $scope.brandList[i].name + ",";
        }
    };
    //去除最后一个逗号
    $scope.brand = $scope.brand.substring(0,$scope.brand.length-1);
    $scope.brandName = $scope.brandName.substring(0,$scope.brandName.length-1);

    if(Biostime.common.isEmpty($scope.district)){
        $scope.district = "";
    };*/

    //关联品类
    $scope.category = "";
    $scope.categoryName = "";
    for(var i=0; i < $scope.categoryList.length; i++){
        if($scope.categoryList[i].category == true) {
            $scope.category += $scope.categoryList[i].value + ",";
            $scope.categoryName += $scope.categoryList[i].name + ",";
        }
    };
    //去除最后一个逗号
    $scope.category = $scope.category.substring(0,$scope.category.length-1);
    $scope.categoryName = $scope.categoryName.substring(0,$scope.categoryName.length-1);

    if(Biostime.common.isEmpty($scope.district)){
        $scope.district = "";
    };

    //拼接地址
    $scope.address = $scope.province.regionName + $scope.city.regionName + $scope.district.regionName + $scope.road;

    $scope.startDate =  document.getElementById("startDate").value;
    $scope.endDate =  document.getElementById("endDate").value;

    /*if(Biostime.common.isEmpty($scope.brand)){
        Biostime.common.toastMessage("品牌不能为空，请检查");
        return false;
    }*/
    if(Biostime.common.isEmpty($scope.category)){
        Biostime.common.toastMessage("关联品类不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.ftfTheme)){
        Biostime.common.toastMessage("FTF主题不能为空，请检查");
        return false;
    }
    if(($scope.type=='0' || $scope.type=='1' || $scope.type=='4' || $scope.type=='5') && Biostime.common.isEmpty($scope.consumerType)){
        Biostime.common.toastMessage("消费者类型不能为空，请检查");
        return false;
    }
    if($scope.type != 2 && Biostime.common.isEmpty($scope.expertType)){
        Biostime.common.toastMessage("专家支持类型不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.saCode)){
        Biostime.common.toastMessage("SA编号不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.terminalCode)){
        Biostime.common.toastMessage("门店编号不能为空，请检查");
        return false;
    }

    if($scope.isSaTerminalCode == false){
        Biostime.common.toastMessage("请输入正确的SA编码");
        return false;
    }
    if($scope.isTerminalCode == false){
        Biostime.common.toastMessage("请输入正确的门店编码");
        return false;
    }
    if(Biostime.common.isEmpty($scope.courseInfo)){
        Biostime.common.toastMessage("课程名称不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.courseId)){
        Biostime.common.toastMessage("请选择列表中的课程名称");
        return false;
    }
    if($scope.teacherType=='1' && Biostime.common.isEmpty($scope.teacherInfo)){
        Biostime.common.toastMessage("内部讲师不能为空，请检查");
        return false;
    }
    if($scope.teacherType=='1' && Biostime.common.isEmpty($scope.lecturerRank)){
        Biostime.common.toastMessage("内部讲师头衔不能为空，请检查");
        return false;
    }
    if($scope.type == 2 && $scope.teacherType=='1' && Biostime.common.isEmpty($scope.NBexpertType)){
        Biostime.common.toastMessage("内部讲师专家支持类型不能为空，请检查");
        return false;
    }
    if($scope.teacherType=='3' && Biostime.common.isEmpty($scope.ptTeacherInfo)){
        Biostime.common.toastMessage("兼职讲师不能为空，请检查");
        return false;
    }
    if($scope.teacherType=='1' && Biostime.common.isEmpty($scope.teacherCode)){
        Biostime.common.toastMessage("请选择列表中的内部讲师");
        return false;
    }
    if($scope.teacherType=='3' && Biostime.common.isEmpty($scope.ptTeacherCode)){
        Biostime.common.toastMessage("请选择列表中的兼职讲师");
        return false;
    }
    // if($scope.isSelectTeacher == false){
    //     Biostime.common.toastMessage("课程讲师需选择已存在的讲师，请检查");
    //     return false;
    // }
    if(Biostime.common.isEmpty($scope.startDate)){
        Biostime.common.toastMessage("活动开始时间不能为空，请检查");
        return false;
    }
    if(Biostime.common.isEmpty($scope.endDate)){
        Biostime.common.toastMessage("活动结束时间不能为空，请检查");
        return false;
    }
    $scope.startDateTemp = getDateForStringDate($scope.startDate).format("yyyy-MM-dd");
    $scope.endDateTemp = getDateForStringDate($scope.endDate).format("yyyy-MM-dd");

    if($scope.startDateTemp != $scope.endDateTemp){
        Biostime.common.toastMessage("活动开始时间与结束时间需在同一天，请检查");
        return false;
    }
    var today = new Date().format("yyyy-MM-dd")+' 23:59:59';
    if(Date.parse(getDateForStringDate($scope.startDate)) < Date.parse(today)){
        Biostime.common.toastMessage("活动开始时间只能选择当天之后的，请检查");
        return false;
    }
    if(Date.parse(getDateForStringDate($scope.endDate)) < Date.parse(today)){
        Biostime.common.toastMessage("活动结束时间只能选择当天之后的，请检查");
        return false;
    }

    //将活动时间  分割成  日期、开始时间、结束时间
    $scope.actDate = new Date(getDateForStringDate($scope.startDate)).format("yyyy-MM-dd");
    $scope.startTime = new Date(getDateForStringDate($scope.startDate)).format("hh:mm");
    $scope.endTime = new Date(getDateForStringDate($scope.endDate)).format("hh:mm");
    $scope.year = new Date(getDateForStringDate($scope.startDate)).format("yyyy");
    $scope.month = new Date(getDateForStringDate($scope.startDate)).format("MM");
    $scope.day = new Date(getDateForStringDate($scope.startDate)).format("dd");
    $scope.actTime = $scope.year + "年" + $scope.month + "月" + $scope.day + "日   " + $scope.startTime + "-" + $scope.endTime;

    if(Biostime.common.isEmpty($scope.province.regionCode)){
        Biostime.common.toastMessage("请选择省");
        return false;
    }
    if(Biostime.common.isEmpty($scope.city.regionCode)){
        Biostime.common.toastMessage("请选择市");
        return false;
    }
    // if(Biostime.common.isEmpty($scope.district)){
    //     Biostime.common.toastMessage("请选择区");
    //     return false;
    // }
    if(Biostime.common.isEmpty($scope.road)){
        Biostime.common.toastMessage("请填写具体街道地址");
        return false;
    }
    if(Biostime.common.isEmpty($scope.shareCopyWriting)){
        Biostime.common.toastMessage("请填写分享文案");
        return false;
    }
    if(Biostime.common.isEmpty($scope.newCustomerCalculation)){
        Biostime.common.toastMessage("请选择新客计算方式");
        return false;
    }
    return true;
};

function checkPage($scope){
    if($scope.currentTemplate.id !=1){
        if(Biostime.common.isEmpty($scope.imgSrcGPTwo) || Biostime.common.isEmpty($scope.imgSrcGPThree)){
            Biostime.common.toastMessage("请上传活动照片");
            return false;
        };
        if(Biostime.common.isEmpty($scope.description1) || Biostime.common.isEmpty($scope.description2)){
            Biostime.common.toastMessage("请填写活动描述");
            return false;
        };
        if(Biostime.common.isEmpty($scope.receiveRules)){
            Biostime.common.toastMessage("请填写奖品描述");
            return false;
        };
        if(Biostime.common.isEmpty($scope.prizeFileImgList)){
            Biostime.common.toastMessage("请上传奖品图片");
            return false;
        }
    }
    return true;
};

/* js中使用new Date(str)创建时间对象不兼容firefox和ie的解决方式*/
function getDateForStringDate(strDate){
    //切割年月日与时分秒称为数组
    var s = strDate.split(" ");
    var s1 = s[0].split("-");
    var s2 = s[1].split(":");
    if(s2.length==2){
        s2.push("00");
    }
    return new Date(s1[0],s1[1]-1,s1[2],s2[0],s2[1],s2[2]);
}
/**
 * 合成海报图片
 * @param $scope
 * @param $http
 */
function merge($scope,$http,type){
    var bgImg = new Image();
    var qrcodeImg = new Image();
    var logoImg = new Image();
    var logoImg2 = new Image();
    var posterCtx = document.getElementById("posterCanvas").getContext("2d");

    //获取图片的实际路径
    bgImg.src = jQuery('#img').attr('src');
    logoImg.src = jQuery('#img2').attr('src');
    if(type == 4){
        logoImg2.src = jQuery('#img3').attr('src');
    }
    //二维码生成图片，插入画布中
    var qrcode = jQuery("#qrcodeArea canvas")[0];
    qrcodeImg.src = qrcode.toDataURL("image/png");
    $scope.qrcodeImg = qrcodeImg.src;

    bgImg.onload = function(){
        posterCtx.drawImage(bgImg,0,0,750,1334);

        posterCtx.fillStyle = 'rgba(225,225,225,0)';
        if(type == 4){
            if($scope.imgSrcGPZero){
                posterCtx.fillRect(150,100,210,60);
                posterCtx.drawImage(logoImg2,150,100,210,60);
                posterCtx.fillRect(420,100,180,60);
                posterCtx.drawImage(logoImg,420,100,180,60);
            }else{
                posterCtx.fillRect(225,100,210,60);
                posterCtx.drawImage(logoImg2,225,100,210,60);
            }
        }else{
            posterCtx.fillRect(510,50,210,60);
            posterCtx.drawImage(logoImg,510,50,210,60);
        }

        posterCtx.fillStyle = "#ffffff";
        if(type==2){
            posterCtx.fillRect(265,580,220,220);
            posterCtx.drawImage(qrcodeImg,285,600,180,180);
        }else{
            posterCtx.fillRect(265,900,220,220);
            posterCtx.drawImage(qrcodeImg,285,920,180,180);
        }


        if(type==1){
            posterCtx.fillStyle = "#3d568e";
            posterCtx.font = "bold 26px Microsoft YaHei";
            if($scope.courseInfo.length<=25){
                posterCtx.fillText("活动主题：" + $scope.courseInfo,100,686);
            }else{
                posterCtx.fillText($scope.courseInfo.substring(0,25),100,686);
                posterCtx.fillText($scope.courseInfo.substring(25),100,716);
            }
        }

        if(type==3){
            posterCtx.fillStyle = "#d76ca1";
        }else{
            posterCtx.fillStyle = "#3d568e";
        }
        posterCtx.font = "26px Microsoft YaHei";

        if(type==2){
            posterCtx.fillText("活动时间：" + $scope.year + "年" + $scope.month + "月" + $scope.day + "日   " + $scope.startTime + "-" + $scope.endTime, 100, 410);
            if($scope.address.length<=16){
                posterCtx.fillText("活动地点：" + $scope.address, 100, 444);
            }else if($scope.address.length <=32){
                posterCtx.fillText("活动地点：" + $scope.address.substring(0,16),100,444);
                posterCtx.fillText($scope.address.substring(16),232,478);
            }else{
                posterCtx.fillText("活动地点：" + $scope.address.substring(0,16),100,444);
                posterCtx.fillText($scope.address.substring(16,32),232,478);
            }
        }else if(type==3){
            posterCtx.fillText("活动时间：" + $scope.year + "年" + $scope.month + "月" + $scope.day + "日   " + $scope.startTime + "-" + $scope.endTime, 100, 750);
            if($scope.address.length<=16){
                posterCtx.fillText("活动地点：" + $scope.address, 100, 780);
            }else if($scope.address.length <=32){
                posterCtx.fillText("活动地点：" + $scope.address.substring(0,16),100,780);
                posterCtx.fillText($scope.address.substring(16),232,810);
            }else{
                posterCtx.fillText("活动地点：" + $scope.address.substring(0,16),100,780);
                posterCtx.fillText($scope.address.substring(16,32),232,810);
            }
        }else if(type==4){
            posterCtx.fillText("活动时间：" + $scope.year + "年" + $scope.month + "月" + $scope.day + "日   " + $scope.startTime + "-" + $scope.endTime, 150, 630);
            if($scope.address.length<=16){
                posterCtx.fillText("活动地点：" + $scope.address, 150, 660);
            }else if($scope.address.length <=32){
                posterCtx.fillText("活动地点：" + $scope.address.substring(0,16),150,660);
                posterCtx.fillText($scope.address.substring(16),282,690);
            }else{
                posterCtx.fillText("活动地点：" + $scope.address.substring(0,16),150,660);
                posterCtx.fillText($scope.address.substring(16,32),282,690);
            }
        }

        //合成图片
        var posterCanvas = document.getElementById("posterCanvas");
        $scope.base64 = posterCanvas.toDataURL("image/jpeg");
        $scope.posterImageUrl = posterCanvas.toDataURL("image/jpeg");
        //获取又拍云签名，并上传又拍云
        // getSignature($scope,$http,6);
        
        $scope.imgNo = '5';
        // importPointOSS($scope,$http,convertBase64UrlToBlob($scope.base64),'saAct');
        var PosterName = 'weixinPoster_'+$scope.year + "年" + $scope.month + "月" + $scope.day + "日" + $scope.startTime + "-" + $scope.endTime;
        importPointOSS($scope,$http,dataURLtoFile($scope.base64,PosterName),'saAct');
    }
}
/**
 * 生成二维码
 * @param codeUrl
 */
function qrcode(codeUrl){
    jQuery('#qrcodeArea').qrcode({
        text: utf16to8(codeUrl),//二维码包含的内容，默认只支持英文内容,中文会乱码，通过utf16to8转码可支持中文
        render: "canvas",//渲染方式可选择canvas或table,默认是canvas, canvas方式支持右键图片下载
        width: 256,//宽度，默认是256
        height: 256,//高度，默认是256，建议宽度和高度保存一致，否则不容易被识别
        typeNumber: -1,//计算模式，默认是-1出二维码
        background: "#fff",//背景颜色，默认为白色
        foreground: "#000"//前景颜色，默认为黑色
    });
    //隐藏二维码
    jQuery('#qrcodeArea').hide();
}
//字符串转换成UTF-8，解决生成二维码，链接含中文乱码问题
function utf16to8(str) {
    var out, i, len, c;
    out = "";
    len = str.length;
    for (i = 0; i < len; i++) {
        c = str.charCodeAt(i);
        if ((c >= 0x0001) && (c <= 0x007F)) {
            out += str.charAt(i);
        } else if (c > 0x07FF) {
            out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
            out += String.fromCharCode(0x80 | ((c >> 6) & 0x3F));
            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
        } else {
            out += String.fromCharCode(0xC0 | ((c >> 6) & 0x1F));
            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
        }
    }
    return out;
}
/**
 * 获得上传又拍云的签名
 * @param $scope
 * @param $http
 */
function getSignature($scope,$http,type){
    Biostime.common.showLoading();
    var name;
    if(type==1){
        name = 'weixinLogo_';
    }else if(type==2){
        name = 'weixinTeacher_';
    }else if(type==3){
        name = 'weixinActivity1_';
    }else if(type==4){
        name = 'weixinActivity2_';
    }else if(type==5){
        name = 'weixinPrize_'
    }else if(type==6){
        name = 'weixinPoster_'
    };
    var queryData = {
        "seqNo": new Date().getTime(),
        "sourceSystem": "MKT",
        "version": "1.6.0",
        "request":{
            "businessModule": "FTF",
            "path": "",
            "fileNames": [{
                "id":1,                                          //排序
                "name":name + $scope.activityId       //文件名称
            }]
        }
    };
    var url = YOUPAIYUN + "merchant-server/external/upyun/signature";
    $http({
        method: 'post',
        url: url,
        data: queryData
    }).success(function(data, status, headers, config){
        if(data.code == 100){
            $scope.signs = data.response.signs;
            if(type==1){
                $scope.logImg = data.response.signs[0].url;
            }else if(type==2){
                $scope.lecturerImg = data.response.signs[0].url;
            }else if(type==3){
                $scope.actImgOne = data.response.signs[0].url;
            }else if(type==4){
                $scope.actImgTwo = data.response.signs[0].url;
            }else if(type==5){
                $scope.prizeYouPaiImgList.push(data.response.signs[0].url);
            }else if(type==6){
                $scope.PosterUrl = data.response.signs[0].url;
            };
            uploadYouPai($scope,$http);
        }
    }).error(function (data, status, header, config) {
        $scope.saveDisabled = false;
        if(type==1){
            getSignature($scope,$http,1)
        }else if(type==2){
            getSignature($scope,$http,2)
        }else if(type==3){
            getSignature($scope,$http,3)
        }else if(type==4){
            getSignature($scope,$http,4)
        }else if(type==5){
            getSignature($scope,$http,5)
        }else if(type==6){
            getSignature($scope,$http,6)
        };
    });
}
/**
 * 上传生成图片到又拍云
 * @param $scope
 * @param $http
 */
function uploadYouPai($scope, $http){
    var url = "http://v0.api.upyun.com/mama100app-mobile";
    var fd = new FormData();
    fd.append("policy", $scope.signs[0].policy);
    fd.append("authorization", $scope.signs[0].authorization);
    fd.append("file",convertBase64UrlToBlob($scope.base64));

    $http.post(url, fd, {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    }).success(function (data) {
        Biostime.common.hideLoading();
        //上传
        if($scope.PosterUrl){
            uploadImgUrl($scope, $http);
        }
    }).error(function (data) {
        $scope.saveDisabled = false;
    });
};

/**
 * 上传海报链接
 * @param $scope
 * @param $http
 */
function uploadImgUrl($scope, $http){
    Biostime.common.showLoading();
    var queryData = {'request':{
        "id": $scope.activityId,                                //活动Id
        "posterImageUrl": $scope.PosterUrl                            //海报链接
    }};
    var url = BASICURL + "samanage/ftf/activity/changeImageFtfActivity.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            $scope.saveDisabled = true;
            $scope.templateList = $scope.templateList.slice($scope.currentTemplate.id-1, $scope.currentTemplate.id);
            $scope.musicShow = false;
            $scope.stepNum = 4;
            $scope.player.pause();
            $scope.player.load();
            $scope.currentMusicUrl = '';
            // $scope.saveDisabled = false;
            // alert("保存成功");
            // window.location.href="activityIndex.html?"+$scope.token;
        }else{
            $scope.saveDisabled = false;
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        $scope.saveDisabled = false;
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    });
};

/**
 * 将以base64的图片url数据转换为Blob
 * @param urlData   用url方式表示的base64图片数据
 * @returns {Blob}  文件流
 */
function convertBase64UrlToBlob(urlData){
    var bytes = window.atob(urlData.split(',')[1]);  //去掉url的头，并转换成byte

    //处理异常，将ASCII码小于0的转换为大于0
    var ab = new ArrayBuffer(bytes.length);
    var ia = new Uint8Array(ab);
    for(var i=0; i<bytes.length; i++){
        ia[i] = bytes.charCodeAt(i);
    }

    return new Blob([ab],{type:'image/png'});
};

// 将base64转换成file对象
function dataURLtoFile (dataurl, filename) {
    let arr = dataurl.split(',');
    let mime = arr[0].match(/:(.*?);/)[1];
    let suffix = mime.split('/')[1];
    let bstr = atob(arr[1]);
    let n = bstr.length;
    let u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return {0:new File([u8arr], `${filename}.${suffix}`, {type: mime}),length:1}
};

function imgCallBack($scope,$http,url){
    // console.log(url);
    if($scope.imgNo == 0){
        $scope.logImg = url;
    }else if($scope.imgNo == 1){
        $scope.lecturerImg = url;
    }else if($scope.imgNo == 2){
        $scope.actImgOne = url;
    }else if($scope.imgNo == 3){
        $scope.actImgTwo = url;
    }else if($scope.imgNo == 4){
        $scope.prizeYouPaiImgList.push(url);
    }else if($scope.imgNo == 5){
        $scope.PosterUrl = url;
        if($scope.PosterUrl){
            uploadImgUrl($scope, $http);
        }
    };
}