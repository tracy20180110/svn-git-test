/**
 * 页面路由跳转配置
 */
define(['./app'], function (app) {
	'use strict';

	return app.config(['$routeProvider','$controllerProvider',
		function ($routeProvider,$controllerProvider) {
			$routeProvider.when('/view1/:id', {
				templateUrl: 'views/courseSearch.html',
				controller: 'courseSearch_controller',
				resolve: {
                    keyName: function($q) {
                        var deferred = $q.defer();
                        require(['/../controller/courseSearch_controller'], function(controllers) {

                            $controllerProvider.register('courseSearch_controller', controllers.templateCtrl1);

                            deferred.resolve();
                        });
                        return deferred.promise;
                    }
                }
			});
            
            $routeProvider.when('/view2/:id', {
                templateUrl: 'views/addCourse.html?bust=' + (new Date()).getTime(),
                controller: 'addCourse_controller',
                resolve: {
                    keyName: function($q) {
                        var deferred = $q.defer();
                        require(['/../controller/addCourse_controller'], function(controllers) {

                            $controllerProvider.register('addCourse_controller', controllers.templateCtrl2);

                            deferred.resolve();
                        });
                        return deferred.promise;
                    }
                }
            });

            $routeProvider.when('/view3/:id', {
                templateUrl: 'views/teacherSearch.html?bust=' + (new Date()).getTime(),
                controller: 'teacherSearch_controller',
                resolve: {
                    keyName: function($q) {
                        var deferred = $q.defer();
                        require(['/../controller/teacherSearch_controller'], function(controllers) {

                            $controllerProvider.register('teacherSearch_controller', controllers.templateCtrl3);

                            deferred.resolve();
                        });
                        return deferred.promise;
                    }
                }
            });

            $routeProvider.when('/view4/:id', {
                templateUrl: 'views/addTeacher.html?bust=' + (new Date()).getTime(),
                controller: 'addTeacher_controller',
                resolve: {
                    keyName: function($q) {
                        var deferred = $q.defer();
                        require(['/../controller/addTeacher_controller'], function(controllers) {

                            $controllerProvider.register('addTeacher_controller', controllers.templateCtrl4);

                            deferred.resolve();
                        });
                        return deferred.promise;
                    }
                }
            });
            
            $routeProvider.when('/view5/:id', {
                templateUrl: 'views/activitySearch.html?bust=' + (new Date()).getTime(),
                controller: 'activitySearch_controller',
                resolve: {
                    keyName: function($q) {
                        var deferred = $q.defer();
                        require(['/../controller/activitySearch_controller'], function(controllers) {

                            $controllerProvider.register('activitySearch_controller', controllers.templateCtrl5);

                            deferred.resolve();
                        });
                        return deferred.promise;
                    }
                }
            });
            
            $routeProvider.when('/view6/:id', {
                templateUrl: 'views/addActivity.html?bust=' + (new Date()).getTime(),
                controller: 'addActivity_controller',
                resolve: {
                    keyName: function($q) {
                        var deferred = $q.defer();
                        require(['/../controller/addActivity_controller'], function(controllers) {

                            $controllerProvider.register('addActivity_controller', controllers.templateCtrl6);

                            deferred.resolve();
                        });
                        return deferred.promise;
                    }
                }
            });

            $routeProvider.when('/editCourse/:id', {
                templateUrl: 'views/editCourse.html?bust=' + (new Date()).getTime(),
                controller: 'editCourse_controller',
                resolve: {
                    keyName: function($q) {
                        var deferred = $q.defer();
                        require(['/../controller/editCourse_controller'], function(controllers) {

                            $controllerProvider.register('editCourse_controller', controllers.templateCtrl7);

                            deferred.resolve();
                        });
                        return deferred.promise;
                    }
                }
            });
    }]);
});