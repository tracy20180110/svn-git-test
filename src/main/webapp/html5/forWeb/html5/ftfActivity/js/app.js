//定义主模块myApp,并引入控制器模块,路由器模块
define(['angular', 'controller/indexCtrl', 'angular-route', 'directives/indexCtrl'], function(angular) {
	return angular.module('myApp', ['myApp.controllers', 'ngRoute','myApp.directives']).config(
		['$interpolateProvider',
			function($interpolateProvider) {
				$interpolateProvider.startSymbol('[[');
				$interpolateProvider.endSymbol(']]');
			}
		]);
    return angular.module('ngBlur', ['myApp.controllers', 'ngRoute','myApp.directives']).config(
        ['$parse',
            function($parse) {
                return function(scope, element, attr) {
                    var fn = $parse(attr['ngBlur']);
                    element.bind('blur', function(event) {
                        scope.$apply(function() {
                            fn(scope, {$event:event});
                        });
                    });
                }
            }
        ]);



    });
