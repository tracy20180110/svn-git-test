requirejs.config({
	//设置模块名称(可以随意指定)和js文件路径的映射
	paths: {
		'angular': '../../../js/lib/angular-1.5.8/angular.min',
		'angular-sanitize':'../../../js/lib/angular-1.5.8/angular-sanitize.min',
		'require': '../../../js/lib/requirejs/require-min',
		'angular-route': '../../../js/lib/angular-1.5.8/angular-route.min',
		'domReady': '../../../js/lib/domReady/domReady.min',
		'common': '../../../js/lib/common',
		'ngDialog': '../../../js/lib/ngDialog/ngDialog.min',
        'jquery-ui-i18n': '../../../js/lib/jquery/jquery-ui-i18n',
        'date-picker': '../../../js/lib/calendar/WdatePicker',
        'jquery1.7': '../../../js/lib/jquery/jquery-1.7.2.min',
        'WdatePicker': '../../../js/lib/calendar/WdatePicker',
		'ZeroClipboard': '../../../js/lib/ueditor/third-party/zeroclipboard/ZeroClipboard',
        'offCode': 'offCode'
	},
	//指定模块名称和它的依赖数组
	shim: {
		'angular': {
			exports: 'angular'
		},
		'angular-route': {
            deps: ['angular']
        },
		'angular-sanitize': {
			deps: ['angular'],
			exports: 'angular-sanitize'
		}
	},
	//注意到其中的deps：['bootstrap']，就是告诉我们要先加载bootstrap.js文件。
	deps: ['bootstrap'],
	urlArgs: "bust=" + (new Date()).getTime() //防止读取缓存，生产用
	// urlArgs: "bust=20160704103056"//防止读取缓存，调试用
});