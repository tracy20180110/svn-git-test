/*******************************************************************************
 * @jsname:addTeacher_controller.js
 * @author:WangDongping
 * @date:2017-1-20
 * @use:新增讲师.js
 ******************************************************************************/
define(['angular'], function (angular) {
    'use strict';

    var templateCtrl4=function ($scope, $http, $route, $routeParams,$rootScope,ngDialog) {
        //获取token
        if(Biostime.common.getQueryString('token')){
            $scope.token = "token="+Biostime.common.getQueryString('token')
        }else{
            $scope.token ="userId="+Biostime.common.getQueryString('userid')
        }
        // $scope.token = "token=0297f591-81cd-4663-918d-844b0f4bde67";

        //仅用于页面样式展示
        $scope.selFun = function(selNum) {
            $scope.selNum = selNum;
        };
     
        //保存
        $scope.saveClick = function () {
            addTeacher($scope, $http);
        };
        //取消
        $scope.cancel = function () {
            if(confirm("确认取消？取消后数据将被清空")){
                $scope.type = $scope.typeList[0].value;
                $scope.teacherShow = false;
                $scope.expertShow = true;
                $scope.teacherInfo = "";
                $scope.expertInfo = "";
                $scope.showError = false;
                $scope.synops = "";
            }
        };

        //清空讲师信息
        $scope.resetRoleName = function () {
            if($scope.type == 1){
                $scope.teacherShow = true;
                $scope.expertShow = false;
            }else if($scope.type == 0){
                $scope.teacherShow = false;
                $scope.expertShow = true;
            }
            $scope.teacherInfo = "";
            $scope.teacherList = [];
            $scope.expertInfo = "";
            $scope.showError = false;
        };

        //load出对应讲师信息
        $scope.teacherFun = function (keyword) {
            $scope.isSelectTeacher = false;
            if(keyword != "" && keyword != undefined){
                loadRoleInfo($scope, $http, keyword);
            }
        };

        //选择相应讲师
        $scope.teacherSel = function(item) {
            $scope.isSelectTeacher = true;
            $scope.teacherInfo = item.keyword;
            $scope.teacherName = item.name;
            $scope.teacherCode = item.code;
        };
        
        //检查专家是否已经存在
        $scope.expertFun = function(keyword){
            if(keyword != "" && keyword != undefined){
                $scope.expertInfo = keyword;
                loadRoleInfo($scope, $http, keyword);
            }
        };

        //初始化
        //获取登录人信息
        identityVerification4($scope, $http);
        //讲师类型（1：育儿讲师，0:育儿专家）
        $scope.typeList = [
            {'name':"育儿专家",'value':"0"},
            {'name':"育儿讲师",'value':"1"}
        ];
        $scope.type = $scope.typeList[0].value;
        $scope.teacherShow = false;
        $scope.expertShow = true;
        //专家校验 提示信息，默认不显示
        $scope.showError = false;
    };

    return {
        templateCtrl4: templateCtrl4
    }
});
//获取登录人信息
function identityVerification4($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        //获取登录人信息
        $scope.userId = data.loadUserBaseInfoBean.userId;
        $scope.userName = data.loadUserBaseInfoBean.userName;

        //针对页面埋点
        var date = new Date();
        var has = md5('6'+'6060201'+date.getTime()+$scope.userId);
        var dataMd = {
            "platform":6, //0:mama100Wechat，1:mama100app，2:SwisseWechat，HYTapp，3:HYTwechat，4:DodieWechat，5:HealthyTimesWechat，6:PC
            "point_code":"6060201",  //具体详情见主题类型编码表
            "created_time": date.getTime(),
            "customer_id":$scope.userId,  //用户会员的ID标识
            "sign":has
        };
        eventpv($scope,$http,dataMd);

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}
/**
 * 获取讲师信息
 * @param $scope
 * @param $http
 * @param keyword
 */
function loadRoleInfo($scope, $http, keyword) {
    var queryData = {
        "type":$scope.type,      //讲师类型  1:育儿讲师； 0：育儿专家
        "keyword": keyword       //讲师信息关键字
    };
    var url = BASICURL + "samanage/ftf/lecturer/queryLecturerCode.action" + '?' + $scope.token;
    $http({
        method: 'GET',
        url: url,
        params: queryData
    }).success(function (data, status, headers, config) {
        if(data.code == 100) {
            if(data.response.length > 0 && data.response != null){
                if($scope.type == 1){
                    $scope.teacherList = data.response;
                }else if($scope.type == 0){
                    if(data.response[0].name == keyword || data.response[0].keyword == keyword){
                        $scope.showError = true;
                        $scope.teacherName = data.response[0].name;
                        $scope.teacherCode = "";
                    }else{
                        $scope.showError = false;
                        $scope.teacherName = keyword;
                        $scope.teacherCode = "";
                    }
                }
            }else{
                if($scope.type == 0){
                    $scope.showError = false;
                    $scope.teacherName = keyword;
                    $scope.teacherCode = "";
                }
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
/**
 * 新增讲师
 * @param $scope
 * @param $http
 */
function addTeacher($scope, $http){
    //讲师类型  1:育儿讲师； 0：育儿专家
    if($scope.type == 1){
        if(Biostime.common.isEmpty($scope.teacherInfo)){
            Biostime.common.toastMessage("讲师信息不能为空，请检查");
            return;
        }
        if($scope.isSelectTeacher == false){
            Biostime.common.toastMessage("讲师需选择在岗人员，请检查");
            return;
        }
    }else if($scope.type == 0){
        if(Biostime.common.isEmpty($scope.expertInfo)){
            Biostime.common.toastMessage("讲师信息不能为空，请检查");
            return;
        }
    }

    if(Biostime.common.isEmpty($scope.synops)){
        Biostime.common.toastMessage("讲师简介不能为空，请检查");
        return;
    }

    Biostime.common.showLoading();
    var queryData = {'request':{
        "type": $scope.type,                                    //讲师类型
        "name": $scope.teacherName,                             //讲师名称
        "code": $scope.teacherCode,                             //讲师Id
        "synops":$scope.synops,                                 //讲师简介
        "userId":$scope.userId,                                 //登录人Id
        "userName":$scope.userName                              //登录人名称
    }};
    var url = BASICURL + "samanage/ftf/lecturer/saveFtfLecturer.action" + '?' + $scope.token;
    $http({
        method: 'POST',
        url: url,
        data: queryData
    }).success(function (data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100) {
            alert(data.response);
            window.location.href="teacherIndex.html?"+$scope.token;
        }else{
            Biostime.common.toastMessage(data.desc);
        }
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(status);
    })
}
