/*******************************************************************************
 * @jsname:saSplitFlowReport_Controller.js
 * @author:zhuhaitao
 * @date:2017-1-5
 * @use:SA分流报表.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    //获取token
    if(Biostime.common.getQueryString('token')){
        $scope.token = "token="+Biostime.common.getQueryString('token')
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid')
    }

    //初始化
    $scope.saCode = '';
    $scope.customerId = '';
    $scope.mobilePhone = '';
    $scope.terminalCode = '';
    $scope.nursingConsultant = '';
    //1店所属渠道：
    $scope.terminalChannelList = [
        {'channelCode':'','channelName':'全部'},
        {'channelCode':'01','channelName':'婴线'},
        {'channelCode':'02','channelName':'商超'},
        {'channelCode':'03','channelName':'药线'},
        {'channelCode':'13','channelName':'美妆'}
    ];
    $scope.terminalChannel = $scope.terminalChannelList[0]; 
    //连锁店所属渠道：
    $scope.chainChannelList = [
        {'channelCode':'','channelName':'全部'},
        {'channelCode':'01','channelName':'婴线'},
        {'channelCode':'02','channelName':'商超'},
        {'channelCode':'03','channelName':'药线'},
        {'channelCode':'13','channelName':'美妆'}
    ];
    $scope.chainChannel = $scope.chainChannelList[0];

    //导出文件
    $scope.importFile = function() {
        $scope.startDate = document.getElementById('startTime').value;
        $scope.endDate = document.getElementById('endTime').value;

        window.location.href = BASICURL + "samanage/splitFlow/exportSaSplitFlow.action?areaCode="+$scope.area.areaCode+"&officeCode="+$scope.department.officeCode+"&saCode="+$scope.saCode+"&customerId="+$scope.customerId
            +"&mobilePhone="+$scope.mobilePhone+"&terminalCode="+$scope.terminalCode+"&nursingConsultant="+$scope.nursingConsultant+"&startTime="+$scope.startDate+"&endTime="+$scope.endDate+'&token='+Biostime.common.getQueryString('token')+"&terminalChannelCode="+$scope.terminalChannel.channelCode+"&chainChannelCode="+$scope.chainChannel.channelCode;

    };

    //根据身份显示相应的大区和办事处、渠道
    identityVerification($scope, $http);
    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments($scope, $http);
    };

    //查询
    $scope.searchClick = function() {
        loadInfoList($scope, $http, 1);
    };
    // 分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadInfoList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadInfoList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 4;
        if ($scope.pagination.pageCount < 4) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
});

//根据身份，显示相应的大区和办事处、渠道
function identityVerification($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){

        $scope.areas=data.loadUserAreaBeanList;
        $scope.departments=data.loadUserOfficeBeanList;
        $scope.channelList=data.loadUserChannelBeanList;
        $scope.user=data.loadUserBaseInfoBean;

        //添加埋点
        var date = new Date();
        var has = md5('6' + '60304' + date.getTime() + $scope.user.userId);
        var dataMd = {
            "platform":6,
            "point_code":'60304',
            "created_time":date.getTime(),
            "customer_id":$scope.user.userId,
            "sign":has
        }
        eventpv($scope,$http,dataMd);

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areas.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        $scope.area=$scope.areas[0];

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}
/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}

/**
 * 查询
 */
function loadInfoList ($scope, $http, pageNum) {
    $scope.startTime = document.getElementById('startTime').value;
    $scope.endTime = document.getElementById('endTime').value;

    $scope.pageNum = pageNum;
    Biostime.common.showLoading();
    var queryData = {
        "request":{
            "pageSize":20,
            "pageNo":pageNum,
            "nursingConsultant":$scope.nursingConsultant,
            "areaCode": $scope.area.areaCode,
            "officeCode": $scope.department.officeCode,
            "saCode": $scope.saCode,
            "customerId": $scope.customerId,
            "mobilePhone": $scope.mobilePhone,
            "terminalCode": $scope.terminalCode,
            "terminalChannelCode": $scope.terminalChannel.channelCode,
            "chainChannelCode": $scope.chainChannel.channelCode,
            "startTime":$scope.startTime||"",  // 开始时间
            "endTime":$scope.endTime||"",  // 结束时间
        }
    };
    var url = BASICURL + "samanage/splitFlow/searchSaSplitFlow.action?token="+Biostime.common.getQueryString('token');//测试地址
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        };
    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};