/*******************************************************************************
 * @jsname:saWxCustomerReport_Controller.js
 * @author:WangXiaoWei
 * @date:2017-1-5
 * @use:SA接触报表.js
 ******************************************************************************/
var biostimeMKTCtrl = angular.module('biostimeMKTCtrl', []);
biostimeMKTCtrl.config([ '$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
biostimeMKTCtrl.controller("biostimeMKTCtrl", function($scope, $http) {
    //获取token
    if(Biostime.common.getQueryString('token')){
        $scope.token = "token="+Biostime.common.getQueryString('token')
    }else{
        $scope.token ="userId="+Biostime.common.getQueryString('userid')
    }

    //初始化
    $scope.saCode = '';
    $scope.customerId = '';
    $scope.customerMobile = '';

    //导出文件
    $scope.importFile = function() {
        if($scope.user.buCode == '10'){
            Biostime.common.toastMessage('该功能只用于BNC事业部');
            return
        }
        $scope.startDate = document.getElementById('startTime').value;
        $scope.endDate = document.getElementById('endTime').value;

        //大区办事处
        $scope.areaOfficeCode = $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode;

        if(Biostime.common.isEmpty($scope.startDate)){
            Biostime.common.toastMessage('开始时间不能为空，请检查');
            return;
        }
        if(Biostime.common.isEmpty($scope.endDate)){
            Biostime.common.toastMessage('结束时间不能为空，请检查');
            return;
        }

        window.location.href = BASICURL + "samanage/sa/event/exportSaEventDetailList.action?" + $scope.token + "&areaOfficeCode="+$scope.areaOfficeCode+"&saCode="+$scope.saCode+"&customerId="+$scope.customerId
            +"&customerMobile="+$scope.customerMobile+"&systemSourse="+$scope.systemSourse.value+"&eventSourse="+$scope.eventSourse.value+"&startDate="+$scope.startDate+"&endDate="+$scope.endDate + "&buCode="+$scope.buId.buCode;

    };

    //课程状态（""：全部，1：启用，0:停用）
    $scope.systemSourseList = [
        {'name':"全部",'value':""},
        {'name':"营销通APP",'value':"2"},
        {'name':"合生元妈妈100微信",'value':"4"},
        //{'name':"营运管理平台",'value':"7"}
        {'name':"小程序",'value':"8"}
    ];
    $scope.systemSourse = $scope.systemSourseList[0];
    $scope.eventSourseSourseList = [
        {'name':"全部",'value':""}
    ];
    $scope.eventSourse = $scope.eventSourseSourseList[0];
    $scope.eventSourseDisabled = true;

    //系统来源联动
    $scope.systemSourseFun = function() {
        $scope.eventSourseSourseList = [];
        if($scope.systemSourse.value != '') {
            $scope.eventSourseDisabled = false;
        }else{
            $scope.eventSourseDisabled = true;
        };
        if($scope.systemSourse.value == '') {
            var obj = [
                {'name':"全部",'value':""}
            ];
            $scope.eventSourseSourseList = obj;
            $scope.eventSourse = $scope.eventSourseSourseList[0];
        }else if($scope.systemSourse.value == '2'){
            var obj = [
                {'name':"全部",'value':""},
                // {'name':"SA发展会员",'value':"1"},
                // {'name':"FTF签到",'value':"3"},
                // {'name':"SA回访",'value':"4"}
                {'name':"SA微信新会员",'value':"17"},
                {'name':"FTF签到",'value':"3"}

            ];
            $scope.eventSourseSourseList = obj;
            $scope.eventSourse = $scope.eventSourseSourseList[0];
        }else if($scope.systemSourse.value == '4'){
            var obj = [
                {'name':"全部",'value':""},
                // {'name':"线下课程注册",'value':"1"},
                // {'name':"线下课程签到",'value':"3"},
                // {'name':"新妈尊享礼",'value':"5"},
                // {'name':"百分妈妈课堂领券",'value':"6"},
                // {'name':"微信绑定新会员",'value':"4"}
                {'name':"微信绑定新会员",'value':"4"},
                {'name':"报到有好礼",'value':"18"}
            ];
            $scope.eventSourseSourseList = obj;
            $scope.eventSourse = $scope.eventSourseSourseList[0];
        }

        /*else if($scope.systemSourse.value == '7'){
            var obj = [
                {'name':"全部",'value':""},
                {'name':"SA数据补录",'value':"1"}
            ];
            $scope.eventSourseSourseList = obj;
            $scope.eventSourse = $scope.eventSourseSourseList[0];
        }*/

        else if($scope.systemSourse.value == '8'){
            var obj = [
                {'name':"全部",'value':""},
                {'name':"注册",'value':"15"},
                {'name':"签到",'value':"16"},
            ];
            $scope.eventSourseSourseList = obj;
            $scope.eventSourse = $scope.eventSourseSourseList[0];
        }
    };

    //根据身份显示相应的大区和办事处、渠道
    identityVerification($scope, $http);
    //事业部修改，大区跟着改变
    $scope.buChange = function(){
        buChange($scope);
    };
    //大区修改，办事处跟着改变
    $scope.areaChange = function(){
        getDepartments($scope, $http);
    };

    //查询
    $scope.searchClick = function() {
        loadInfoList($scope, $http, 1);
    };
    // 分页
    $scope.pagination = {};
    $scope.pagination.displayPageNum = 4;
    $scope.pagination.onFirstPage = function() {
        $scope.pagination.currentPage = 1;
        loadInfoList($scope, $http, 1);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //页数
    $scope.pagination.onGoPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //上一页
    $scope.pagination.onUPPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //下一页
    $scope.pagination.onNextPage = function() {
        if ($scope.pagination.currentPage < $scope.pagination.pageCount) {
            $scope.pagination.currentPage++;
            loadInfoList($scope, $http,
                $scope.pagination.currentPage);
            $scope.pagination.pageNums = $scope.pagination
                .getPageNums();
        }
    };
    //跳转页
    $scope.pagination.JumpPage = function(pageNum) {
        $scope.pagination.currentPage = pageNum;
        loadInfoList($scope, $http, pageNum);
        $scope.pagination.pageNums = $scope.pagination
            .getPageNums();
    };
    //获取页码数据
    $scope.pagination.getPageNums = function() {
        var tempPageNums = [];
        var startPage = 1;
        var endPage = 4;
        if ($scope.pagination.pageCount < 4) {
            endPage = $scope.pagination.pageCount;
        }
        if ($scope.pagination.pageCount > $scope.pagination.displayPageNum) {
            if ($scope.pagination.currentPage < 3) {
                startPage = 1;
            } else if ($scope.pagination.currentPage + 4 > $scope.pagination.pageCount) {
                startPage = $scope.pagination.pageCount
                    - $scope.pagination.displayPageNum + 1;
            } else {
                startPage = $scope.pagination.currentPage - 2;
            }
            endPage = startPage
                + $scope.pagination.displayPageNum - 1;
            if (endPage > $scope.pagination.pageCount)
                endPage = $scope.pagination.pageCount;
        }

        for ( var i = startPage; i <= endPage; i++) {
            tempPageNums.push(i);
        }
        return tempPageNums;
    };
    $scope.pagination.currentPage = 1;
});


//根据身份，显示相应的大区和办事处、渠道
function identityVerification($scope, $http){
    var url = useId+"mkt-common/baseInfo/findLoadUserExtendInfo.action"+"?"+$scope.token;
    $http({
        method: 'GET',
        url: url
    }).success(function(data, status, headers, config){
        $scope.user = data.loadUserBaseInfoBean;           //获取登录人id
        $scope.userId = data.loadUserBaseInfoBean.userId;           //获取登录人id
        $scope.buList = data.loadUserBuBeanList;
        $scope.departments=data.loadUserOfficeBeanList;

        //判断事业部
        $scope.buId = $scope.buList[0];
        $scope.areaAll = data.loadUserAreaBeanList;
        if($scope.buList.length > 1){
            $scope.buDisabled=false;
        }else{
            $scope.buDisabled=true;
        };

        //判断是否总部人员/大区人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.areaCode)){
            $scope.areaDisabled=true;
        }else{
            //总部
            var areasAll={
                areaId:'',
                officeId:'',
                areaCode:'',
                areaName:"全部"
            };
            $scope.areaAll.splice(0,0,areasAll);
            $scope.officeAll=data.loadUserOfficeBeanList;
            $scope.departmentDisabled=true;
        }
        //判断是否办事处人员
        if(!Biostime.common.isEmpty(data.loadUserBaseInfoBean.officeCode)){
            $scope.departmentDisabled=true;
        }else{
            var departmentsAll={
                areaId:'',
                officeId:'',
                officeCode:'',
                officeName:"全部"
            };
            $scope.departments.splice(0,0,departmentsAll)
        }
        $scope.department=$scope.departments[0];
        buChange($scope);

       /* //添加埋点
        var date = new Date();
        var has = md5('6' + '60306' + date.getTime() + $scope.userId);
        var dataMd = {
            "platform":6,
            "point_code":'60306',
            "created_time":date.getTime(),
            "customer_id":$scope.userId,
            "sign":has
        }
        eventpv($scope,$http,dataMd);*/

    }).error(function(data, status, headers, config){
        Biostime.common.hideLoading();
        Biostime.common.showErrorMessage(data.desc);
    });
}

/***
 * 根据选择事业部刷选大区
 *
 * */
function buChange($scope) {
    $scope.areas=[];
    angular.forEach($scope.areaAll,function(item){
        if(Biostime.common.isEmpty($scope.buId.buId)){
            $scope.areas.push(item);
        }else if($scope.buId.buId==item.buId||item.areaId==''){
            $scope.areas.push(item);
        };
    });
    $scope.area=$scope.areas[0];
    if(!$scope.areaDisabled){
        $scope.departmentDisabled=true;
    }
    $scope.department = $scope.departments[0];
}

/***
 * 根据选择大区刷选办事处
 *
 * */
function getDepartments($scope, $http){
    $scope.departmentDisabled=false;
    $scope.departments=[{
        areaId:'',
        officeId:'',
        officeCode:'',
        officeName:"全部"
    }];
    for(var i=0;i<$scope.officeAll.length;i++){
        if($scope.area.areaId==$scope.officeAll[i].areaId){
            $scope.departments.push($scope.officeAll[i]);
        }
    }
    $scope.department=$scope.departments[0];
}

/**
 * 查询
 */
function loadInfoList ($scope, $http, pageNum) {
    if($scope.user.buCode == '10'){
        Biostime.common.toastMessage('该功能只用于BNC事业部');
        return
    }
    $scope.startDate = document.getElementById('startTime').value;
    $scope.endDate = document.getElementById('endTime').value;

    if(Biostime.common.isEmpty($scope.startDate)){
        Biostime.common.toastMessage('开始时间不能为空，请检查');
        return;
    }
    if(Biostime.common.isEmpty($scope.endDate)){
        Biostime.common.toastMessage('结束时间不能为空，请检查');
        return;
    }
    $scope.pageNum = pageNum;
    Biostime.common.showLoading();
    var queryData = {'request':{
        'saCode': $scope.saCode,
        'customerId': $scope.customerId,
        'customerMobile': $scope.customerMobile,
        'systemSourse': $scope.systemSourse.value,
        'eventSourse': $scope.eventSourse.value,
        'buCode': $scope.buId.buCode,
        'areaOfficeCode': $scope.department.officeCode == "" ? $scope.area.areaCode : $scope.department.officeCode,  //大区办事处编码
        'startDate': $scope.startDate,
        'endDate': $scope.endDate,
        'pageNo': pageNum,
        'pageSize': 20
    }};
    var url = BASICURL + "samanage/sa/event/querySaEventDetailList.action" + '?' + $scope.token;
    $http({
        method : 'POST',
        url:url,
        data:queryData
    }).success(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        if(data.code == 100){
            if (data.response.datas == null) {
                Biostime.common.toastMessage("查无数据");
                $scope.infoList = [];
            }else if (data.response.datas.length > 0) {
                $scope.infoList = data.response.datas;
            }
            //分页
            if(pageNum==1) {
                $scope.pagination.currentPage = 1;
                $scope.pagination.pageCount = data.response.totalPage;
                $scope.pagination.totalCount = data.response.totalCount;
                $scope.pagination.pageNums = $scope.pagination.getPageNums();
            }
        }else{
            Biostime.common.toastMessage(data.desc);
        };

    }).error(function(data, status, headers, config) {
        Biostime.common.hideLoading();
        Biostime.common.toastMessage(data.desc);
    });
};