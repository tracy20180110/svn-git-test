﻿var ngApp = angular.module("ngApp",[]).config(
    [ '$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    } ])
if(Biostime.common.getQueryString('month') &&Biostime.common.getQueryString('month')!="false"){
    var passMonth = Biostime.common.getQueryString("month");
}else{
    var nYear = new Date().getFullYear();
    var nMonth = new Date().getMonth()+1;
    if(nMonth>10){
        var passMonth = nYear+"-"+nMonth;
    }else{
        var passMonth = nYear+"-0"+nMonth;
    }
}
var curYear = passMonth.split("-")[0];
var curMonth = passMonth.split("-")[1];
if(curMonth <"10"){
    curMonth = curMonth.split("")[1];
}
ngApp.controller("jobScheduleController",['$scope','$http','$timeout',function($scope,$http,$timeout){
    $scope.edit = false;
    $scope.save = false;
    $scope.editTable = false;
    $scope.wxMor = '';
    $scope.account = Biostime.common.getQueryString("account");
    $scope.month = Biostime.common.getQueryString("month");
    $scope.officeCode = Biostime.common.getQueryString("officeCode");
    $scope.name = decodeURI(Biostime.common.getQueryString("name"));
    $scope.saCodeAftError =false;
    $scope.saCodeMorError =false;

    //选择排班
    $scope.saveSchedule = function(obj){
        saveSchedule($scope,$http,obj);
    }
    //选择休息
    $scope.saveRest = function(obj){
        saveRest($scope,$http,obj);
    }
    $scope.allCancle = function(obj){
        allCancle($scope,$http,obj)
    }
    //编辑按钮
    $scope.editBtn = function(){
        editBtn($scope,$http)
    }
    //保存按钮
    $scope.saveBtn = function(){
        saveBtn($scope,$http)
    }
    //根据SA编号匹配SA名称（上午）
    $scope.bySaCodeMor = function(){
            bySaCodeMor($scope,$http)
    }
    //根据SA编号匹配SA名称（下午）
    $scope.bySaCodeAft = function(){
          bySaCodeAft($scope, $http)
    }
    //如果是排班则显示编辑信息
    $scope.showEdit = function(obj){
        showEdit($scope,$http,obj)
    }
    $scope.loadInfoList = function(){
        loadInfo($scope,$http);
    }
    loadInfo($scope,$http);

//    if($scope.edit == true){
//        $scope.ifEdit = true;
//    }else{
//        $scope.ifEdit = false;
//    }

}])
var today     = new Date();
var thisDate  = today.getDate();
var thisMonth = today.getMonth();
var thisYear  = today.getFullYear();
var thisDay   = today.getDay();  //星期

var currentMonth    = curMonth;
var currentYear     = curYear;
//var currentYear     = thisYear;
//var currentMonth    = thisMonth;

var Month_InChinese = ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'];
var events          =[
    {
        date:'20151231',
        event:""
    },
    {
        date:'20151103',
        event:""
    },
    {
        date:'20151203',
        event:""
    }
];

//设置表头的年月
function setDate(){
//    var month =1+currentMonth;
    var month = curMonth;
    document.getElementsByClassName('calendar-title-date')[0].innerHTML = currentYear + "年" + month + "月";
}

function Init(){
//    var firstDayOfTheMonth = new Date(currentYear,currentMonth,1);//上个月最大天数+1即当月第一天
    var firstDayOfTheMonth = new Date(currentYear,curMonth-1,1);
    var strHTML = "<tr><th>日</th><th>一</th><th>二</th><th>三</th><th>四</th><th>五</th><th>六</th></tr>";
    //加入上月空白节点
    for(var i=0;i<firstDayOfTheMonth.getDay();i++){
        strHTML+="<td></td>";
    }

    //得到当月的最大天数
//    var tempDate = new Date(currentYear,currentMonth+1,0);
    var tempDate = new Date(currentYear,curMonth,0);
    var numOfDays =  tempDate.getDate();

    //增加要显示的天数
    for(var j = 1;j<=numOfDays;j++){
        tempDate = new Date(currentYear,curMonth-1,j);
        if(tempDate.getDate()==thisDate&&tempDate.getMonth()==thisMonth&&tempDate.getFullYear()==thisYear)
            strHTML +="<td class='today' onclick='showOrHide(this,"+j+")'>" + j + "<div class='alertbox' style='display:none;'>" +
                "<label class='work'>" +
                "<div class='checkboxOne'><input type='checkbox' name='check' onclick='checkIndex(this)' id="+j+" /><label for="+j+"></label></div>排班" +
                "</label>" +
                "<label class='rest'>" +
                "<div class='checkboxOne'><input type='checkbox' name='check' onclick='checkIndex(this)' id="+'a'+j+" /><label for="+'a'+j+"></label></div>休息" +
                "</label>" +
                "</div>" +
                "</td>";
        else strHTML += "<td onclick='showOrHide(this,"+j+")'>" + j +"<div class='alertbox' style='display:none;'>" +
                                        "<label class='work'>" +
                                             "<div class='checkboxOne'><input type='checkbox' name='check' onclick='checkIndex(this)' id="+j+" /><label for="+j+"></label></div>排班" +
                                        "</label>" +
                                        "<label class='rest'>" +
                                        "<div class='checkboxOne'><input type='checkbox' name='check' onclick='checkIndex(this)' id="+'a'+j+" /><label for="+'a'+j+"></label></div>休息" +
                                        "</label>" +
                                     "</div>" +
                         "</td>";
        if(tempDate.getDay()===6) strHTML +="</tr><tr>";
    }
    strHTML +="</tr>";
    document.getElementById("calendar").innerHTML = strHTML;
//    check();
}

//    //为有活动的日子增加样式
function importantDay() {
    var month,day,year;
    for (var i = 0; i < events.length; i++) {
        //目标--年月日
        year  = parseInt(events[i].date.substring(0, 4));
        month = parseInt(events[i].date.substring(4, 6));
        day   = parseInt(events[i].date.substring(6,8));
        if (year == currentYear && month == currentMonth + 1) {
            var tds = document.getElementsByTagName('td');
            var len = tds.length;
            for (var j = 0; j < len; j++)
                if (parseInt(tds[j].innerHTML) == day) {
                    tds[j].innerHTML ="<a href='#'>" +day +"</a>";
                    tds[j].style= 'color:#333; border:1px solid gray; ';
                }
        }

    }
}

function lastMonth(){
//    if(currentMonth==0){
//        --currentYear;
//        currentMonth = 11;
//    }else currentMonth--;
        if(curMonth-1==0){
            --currentYear;
            --curMonth;
            curMonth = 12;
    }else curMonth--;
    setDate();
    Init();
    importantDay();
//    check();
        angular.element(document.getElementById('jobScheduleController')).scope().loadInfoList();
}

function nextMonth(){
//    if(currentMonth==11){
//        ++currentYear;
//        currentMonth = 0;
//    }else ++currentMonth;
        if(curMonth-1==11){
            ++currentYear;
            ++curMonth;
            curMonth = 1;
    }else ++curMonth;
    setDate();
    Init();
    importantDay();
//    check();
        angular.element(document.getElementById('jobScheduleController')).scope().loadInfoList();
}

function sortEvent(){
    var len = events.length;
    for (var i = 0; i <len-1; i++) {
        for(var j=i+1;j<len;j++){
            if(parseInt(events[i].date)>parseInt(events[j].date))
                swap(events[i],events[j]);
        }
    }
    function swap(a,b){
        var temp={date:"",event:""};
        temp.date = a.date;
        temp.event = a.event;
        a.date = b.date;
        a.event = b.event;
        b.date = temp.date;
        b.content = temp.content;
    }

}

function setEvent(){
    var IntToday = parseInt(""+thisYear + (1+thisMonth) + thisDate);
    // alert("INTTODAY"+IntToday);
    var len = events.length;
    for (var i = 0; i <len; i++) {
        if(parseInt(events[i].date)>=IntToday){
            document.getElementsByClassName('recentEvent-Day')[0].innerHTML = events[i].date.substring(6,8);
            document.getElementsByClassName('recentEvent-Month')[0].innerHTML =
                Month_InChinese[parseInt(events[i].date.substring(4,6))-1];
            document.getElementsByClassName('recentEvent-content')[0].innerHTML =
                '<p><a href="">' + events[i].event + ' >>></a></p>';
            return;
        }
    }
}

//点击checkbox
function checkIndex(obj){
    var td =  obj.parentNode.parentNode.parentNode.parentNode;
    var obj1 = obj.parentNode.parentNode.parentNode.children[0].children[0].childNodes[0];
    var obj2 =  obj.parentNode.parentNode.parentNode.children[1].children[0].childNodes[0];
    var obj1Id = obj1.id;
    var obj2Id = obj2.id;
    if(obj.id == obj1Id && obj1.checked == true){//选择排班
        obj2.checked = false;
        td.style.background = "#36c6ff";
        td.style.color = "#fff";
        angular.element(document.getElementById('jobScheduleController')).scope().saveSchedule(obj);
    }
    else if(obj.id== obj2Id && obj2.checked == true){//选择休息
        obj1.checked = false;
        td.style.background = "#43e507";
        td.style.color = "#fff";
        angular.element(document.getElementById('jobScheduleController')).scope().saveRest(obj);
    }else if(obj1.checked == false && obj2.checked == false){
        if(obj.id == obj1.id){
            obj1.checked = true;
            obj2.checked = false;
            td.style.background = "#36c6ff";
            td.style.color = "#fff";
        }else{
            obj2.checked = true;
            obj1.checked = false;
            td.style.background = "#43e507";
            td.style.color = "#fff";
        }
//        td.style.background = "none";
//        td.style.color = "#444444";
        angular.element(document.getElementById('jobScheduleController')).scope().allCancle(obj);
    }
}
var selectDate = "";
//隐藏或显示排班选择
function showOrHide(obj,index){
    var alertBox = obj.children[0];
    var td = document.getElementById("calendar").getElementsByTagName("td");
    var num = 0;

    var date1 = new Date();
    var today = date1.format('yyyy/MM/dd');

    var date2 = new Date(date1);
    date2.setDate(date1.getDate()+15);
    var halfMonday1 = date2.getFullYear()+"/"+(date2.getMonth()+1)+"/"+date2.getDate();//15天后的日子
    var halfMonday = new Date((new Date(halfMonday1)).getTime()).format('yyyy/MM/dd');

    selectDate = obj.innerText;
    if(selectDate.length>2){
        selectDate = selectDate.split("排")[0];
    }
    var date = document.getElementById("date").innerHTML; //当前选择的年月日
    var year = date.split("年")[0];
    var month = date.split("年")[1].split("月")[0];
    var day = selectDate;
    var selectedDate = new Date(new Date(year+"/"+month+"/"+day).getTime()).format('yyyy/MM/dd');

    if(Biostime.common.getQueryString('ifmanagement') &&Biostime.common.getQueryString('ifmanagement')!="false"){
        if(obj.children[0].children[0].children[0].children[0].checked == true){//选择排班
            obj.children[0].children[1].children[0].children[0].checked = false;
            angular.element(document.getElementById('jobScheduleController')).scope().showEdit(obj);//查当天排班
        }else{
            angular.element(document.getElementById('jobScheduleController')).scope().allCancle(obj);
        }
    }else{
        if(selectedDate<halfMonday && selectedDate>=today){
            if(alertBox.style.display == "block"){
                alertBox.style.display = "none";
            }else{
                alertBox.style.display = "block";
                if(obj.children[0].children[0].children[0].children[0].checked == true){//选择排班
                    obj.children[0].children[1].children[0].children[0].checked = false;
                    angular.element(document.getElementById('jobScheduleController')).scope().showEdit(obj);//查当天排班
                }else{
                    angular.element(document.getElementById('jobScheduleController')).scope().allCancle(obj);
                }
            }
            for(var i=0;i<td.length;i++){
                if(td[i].innerHTML == ""){}else{
                    num++;
                    td[i].index = num;
                }
                if(td[i].index != undefined){
                    if(td[i].index == index){
//               alertBox.style.display ="block";
                    }else{
                        td[i].children[0].style.display="none";
                    }
                }
            }
        }else{
        }
    }

}

Init();
setDate();
importantDay();

//初始化时，先将events按时间排序
sortEvent();
//显示下一个事件
setEvent();
//首次加载
function loadInfo($scope,$http){
    $scope.edit = false;
    $scope.save = false;
    $scope.editTable = false;
    var nowTimeList = document.getElementById("date").innerHTML;
    var nowYear = nowTimeList.split("年")[0];
    var nowMonth = nowTimeList.split("年")[1].split("月")[0];
    Biostime.common.showLoading();
    var UrlApi =BASICURL1+"jobScheduling/getJobSchedulingStatus.do";
    $http({
        method: 'GET',
        url: UrlApi,
        params:{
            "salesAccountNo":$scope.account,
            "month":nowYear+"-"+nowMonth
        }
    }).success(
        function (data, status, headers, config) {
            Biostime.common.hideLoading();
            if (data.code == 100) {
                if(data.response != null){
                    $scope.infoList = data.response;
                    var td = document.getElementById("calendar").getElementsByTagName("td");
                    var dayList =[];
                    for(var j=0;j<$scope.infoList.length;j++){
                        var listDate = new Date($scope.infoList[j].companyDate).format('yyyy-MM-dd');
                        var list = listDate.split("-");
                        var listDay = list[2];
                        if(listDay<"10"){
                            listDay = listDay.split("")[1];
                        }
                        dayList.push({"day":listDay,"status":$scope.infoList[j].status,"id":$scope.infoList[j].id,"companyDate":$scope.infoList[j].companyDate})
                    }
                    $scope.responseList = dayList;
                    for(var i =0;i<td.length;i++){
                        for(var k=0;k<dayList.length;k++){
                            if(td[i].innerText == dayList[k].day){
                                if(dayList[k].status == "1"){
                                    td[i].style.background = "#36c6ff";
                                    td[i].style.color = "#fff";
                                    td[i].children[0].children[0].children[0].children[0].checked = true;
                                }else{
                                    td[i].style.background = "#43e507";
                                    td[i].style.color = "#fff";
                                    td[i].children[0].children[1].children[0].children[0].checked = true;
                                }
                            }
                        }
                    }
                }
            } else {
                Biostime.common.toastMessage(data.code + ":" + data.desc);
            }

        }).error(function (data, status, headers, config) {
            Biostime.common.hideLoading();
            Biostime.common.showErrorMessage(status);
        });
}

//编辑
function editBtn($scope,$http){
       $scope.edit = false;
       $scope.save = true;
       $scope.ifEdit = false;
}
//保存
function saveBtn($scope,$http){
    bySaCodeMor($scope,$http);
    bySaCodeAft($scope,$http);
    Biostime.common.showLoading();
    setTimeout(function(){
        if($scope.saCodeMorError == true || $scope.saCodeAftError == true){
              Biostime.common.hideLoading();
//            alert("请填写正确的SA编号");
        }else{
            if(checkForm($scope)){
                setTimeout(function(){
                    var nowTimeList = document.getElementById("date").innerHTML;
                    var nowYear = nowTimeList.split("年")[0];
                    var nowMonth = nowTimeList.split("年")[1].split("月")[0];
                    $scope.edit = true;
                    $scope.save = false;
                    $scope.ifEdit = true;
                    var UrlApi =BASICURL1+"jobScheduling/saveJobScheduling.do";
                    if($scope.saCodeMor == "" && $scope.saCodeAft =="" ){
                        var postData = {
                            "request": {
                                "salesAccountNo": $scope.account,
                                "companyDate": nowYear + "-" + nowMonth + "-" + $scope.day,
                                "saJobSchedulingBeans": []
                            }
                        }
                    }
                    if($scope.saCodeMor !="" && $scope.saCodeAft == ""){
                        var postData = {
                            "request": {
                                "salesAccountNo": $scope.account,
                                "companyDate": nowYear + "-" + nowMonth + "-" + $scope.day,
                                "saJobSchedulingBeans": [
                                    {
                                        "saCode": $scope.saCodeMor,
                                        "saName": $scope.saNameMor,
                                        "weixinCustomerCount": $scope.weixinCustomerCountMor,
                                        "arriveShopCount": $scope.arriveShopCountMor,
                                        "saNewCustomerCount": $scope.saNewCustomerCountMor,
                                        "remarks": $scope.remarksMor,
                                        "timeBucket": "0"
                                    }
                                ]
                            }
                        }
                    }
                    if($scope.saCodeMor == "" && $scope.saCodeAft != ""){
                        var postData = {
                            "request": {
                                "salesAccountNo": $scope.account,
                                "companyDate": nowYear + "-" + nowMonth + "-" + $scope.day,
                                "saJobSchedulingBeans": [
                                    {
                                        "saCode":$scope.saCodeAft,
                                        "saName":$scope.saNameAft,
                                        "weixinCustomerCount":$scope.weixinCustomerCountAft,
                                        "arriveShopCount":$scope.arriveShopCountAft,
                                        "saNewCustomerCount":$scope.saNewCustomerCountAft,
                                        "remarks":$scope.remarksAft,
                                        "timeBucket":"1"
                                    }
                                ]
                            }
                        }
                    }
                    if($scope.saCodeMor != "" && $scope.saCodeAft != ""){
                        //            if($scope.saCodeMorError == true && $scope.saCodeAftError == true){
                        var postData ={
                            "request":{
                                "salesAccountNo":$scope.account,
                                "companyDate":nowYear+"-"+nowMonth+"-"+$scope.day,
                                "saJobSchedulingBeans":[{
                                    "saCode":$scope.saCodeMor,
                                    "saName":$scope.saNameMor,
                                    "weixinCustomerCount":$scope.weixinCustomerCountMor,
                                    "arriveShopCount":$scope.arriveShopCountMor,
                                    "saNewCustomerCount":$scope.saNewCustomerCountMor,
                                    "remarks":$scope.remarksMor,
                                    "timeBucket":"0"
                                },{
                                    "saCode":$scope.saCodeAft,
                                    "saName":$scope.saNameAft,
                                    "weixinCustomerCount":$scope.weixinCustomerCountAft,
                                    "arriveShopCount":$scope.arriveShopCountAft,
                                    "saNewCustomerCount":$scope.saNewCustomerCountAft,
                                    "remarks":$scope.remarksAft,
                                    "timeBucket":"1"
                                }]
                            }
                        }
                        //            }
                    }
                    $http({
                        method: 'POST',
                        url: UrlApi,
                        data: postData,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).success(
                        function (data, status, headers, config) {
                            Biostime.common.hideLoading();
                            if (data.code == 100) {
                                setTimeout(function(){
                                    Biostime.common.toastMessage("保存成功！");
                                },1000)
                            } else {
                                Biostime.common.toastMessage(data.code + ":" + data.desc);
                            }

                        }).error(function (data, status, headers, config) {
                            Biostime.common.hideLoading();
                            Biostime.common.showErrorMessage(status);
                        });
                },1000)
            }
        }
    },500)
}
//根据sa编号查询SA名称(上午)
function bySaCodeMor($scope,$http){
    if(isNaN($scope.saCodeMor)){
//        Biostime.common.toastMessage("SA编号只可输入正整数或格式不正确");
        alert("上午SA编号只可输入正整数或格式不正确");
        $scope.saCodeMor =""
        $scope.saNameMor = "";
//        $scope.nonSave = true;
        $scope.saCodeMorError = true;
    }else{
        Biostime.common.showLoading();
        var UrlApi = BASICURL1+"jobScheduling/getSaNameBySaCode.do";
        $http({
            method: 'GET',
            url: UrlApi,
            params:{"saCode":$scope.saCodeMor}
        }).success(
            function (data, status, headers, config) {
                Biostime.common.hideLoading();
                if (data.code == 100) {
                    if(data.response == null){
                        $scope.saNameMor = "";
                        if($scope.saCodeMor != ""){
                            alert("请填写正确的上午排班SA编号");
                            $scope.saCodeMorError = true;
//                            $scope.saCodeMor = ""
//                            $scope.nonSave = true;
                        }else{
//                            $scope.nonSave = false;
                            $scope.saCodeMorError = false;
//                            $scope.nonSave = false;
                        }
//                        if(($scope.saCodeAftError ==true && $scope.saCodeMorError == true) || $scope.saCodeMor == ""){
//                            $scope.saCodeMorError = true;
//                            if($scope.saCodeAftError ==true && $scope.saCodeMorError == true){
//                                $scope.nonSave = false;
//                            }
//                        }
                    }else {
//                        $scope.saCodeMorError = true;
//                        if( ($scope.saCodeAftError ==true && $scope.saCodeMorError == true) || $scope.saCodeAft == ""){
//                            $scope.nonSave = false;
//                        }
//                        $scope.nonSave = false;
                        $scope.saCodeMorError = false;
                        $scope.saNameMor = data.response.name;
                    }
                } else {
                    Biostime.common.toastMessage(data.code + ":" + data.desc);
                }

            }).error(function (data, status, headers, config) {
                Biostime.common.hideLoading();
                Biostime.common.showErrorMessage(status);
            });
    }
}

//根据sa编号查询SA名称(下午)
function bySaCodeAft($scope,$http){
    if(isNaN($scope.saCodeAft)) {
        alert("下午排班SA编号只可输入正整数或格式不正确");
        $scope.saCodeAft = ""
//        $scope.nonSave = true;
        $scope.saNameAft = "";
        $scope.saCodeAftError = true;
    }else{
    Biostime.common.showLoading();
        var UrlApi = BASICURL1+"jobScheduling/getSaNameBySaCode.do";
        $http({
            method: 'GET',
            url: UrlApi,
            params:{"saCode":$scope.saCodeAft}
        }).success(
            function (data, status, headers, config) {
                Biostime.common.hideLoading();
                if (data.code == 100) {
                    if(data.response == null){
                        $scope.saNameAft = "";
//                        $scope.saCodeAftError = true;
                        if($scope.saCodeAft != ""){
                             alert("请填写正确的下午排班SA编号");
//                             $scope.saCodeAft = "";
                             $scope.saCodeAftError = true;
//                             $scope.nonSave = true;
                        }else{
//                            $scope.nonSave = false;
                            $scope.saCodeAftError = false;
                        }
//                        if( ($scope.saCodeAftError ==true && $scope.saCodeMorError == true) || $scope.saCodeAft == ""){
//                            $scope.saCodeAftError = true
//                            if($scope.saCodeAftError ==true && $scope.saCodeMorError == true){
//                                $scope.nonSave = false;
//                            }
//                        }
                    }else{
//                        $scope.saCodeAftError = true;
//                        if( ($scope.saCodeAftError ==true && $scope.saCodeMorError == true) || $scope.saCodeMor == ""){
//                            $scope.nonSave = false;
//                        }
//                         $scope.nonSave = false;
                         $scope.saCodeAftError = false;
                         $scope.saNameAft = data.response.name;
                    }
                } else {
                    Biostime.common.toastMessage(data.code + ":" + data.desc);
                }

            }).error(function (data, status, headers, config) {
                Biostime.common.hideLoading();
                Biostime.common.showErrorMessage(status);
            });
    }
}
//选择排班
function saveSchedule($scope,$http,obj){
    var nowTimeList = document.getElementById("date").innerHTML;
    var nowYear = nowTimeList.split("年")[0];
    var nowMonth = nowTimeList.split("年")[1].split("月")[0];
    var today = obj.parentNode.parentNode.parentNode.parentNode.innerText;//今天的日期
    if(today.length>2){
        today = today.split("排")[0];
    };
    if($scope.responseList != undefined){
        for(var i =0;i<$scope.responseList.length;i++){
            if($scope.responseList[i].day == today){
               var todayId = $scope.responseList[i].id;
            }
        }
    }
    if(todayId == undefined){
        var todayId = ""
    }
    Biostime.common.showLoading();
    var UrlApi =BASICURL1+"jobScheduling/saveJobSchedulingStatus.do";
    var postData ={
        "request":{
            "id":todayId,
            "salesAccountNo":$scope.account,
            "companyDate":nowYear+"-"+nowMonth+"-"+today,
            "status":"1"
        }
    }
    $http({
        method: 'POST',
        url: UrlApi,
        data: postData,
        headers: {
            'Content-Type': 'application/json'
        }
    }).success(
        function (data, status, headers, config) {
            Biostime.common.hideLoading();
            if (data.code == 100) {
                loadInfo($scope,$http);
            } else {
                Biostime.common.toastMessage(data.code + ":" + data.desc);
            }

        }).error(function (data, status, headers, config) {
            Biostime.common.hideLoading();
            Biostime.common.showErrorMessage(status);
        });
    $scope.edit = true;
    $scope.editTable = true;
    $scope.ifEdit = true;
    $scope.$apply();
}
//选择休息
function saveRest($scope,$http,obj){
    var nowTimeList = document.getElementById("date").innerHTML;
    var nowYear = nowTimeList.split("年")[0];
    var nowMonth = nowTimeList.split("年")[1].split("月")[0];
    var today = obj.parentNode.parentNode.parentNode.parentNode.innerText;//今天的日期
    if(today.length>2){
        today = today.split("排")[0];
    };
    if($scope.responseList != undefined){
        for(var i =0;i<$scope.responseList.length;i++){
            if($scope.responseList[i].day == today){
                var todayId = $scope.responseList[i].id;
            }
        }
    }
    if(todayId == undefined){
        todayId = ""
    }
    Biostime.common.showLoading();
    var UrlApi =BASICURL1+"jobScheduling/saveJobSchedulingStatus.do";
    var postData ={
        "request":{
            "id":todayId,
            "salesAccountNo":$scope.account,
            "companyDate":nowYear+"-"+nowMonth+"-"+today,
            "status":"0"
        }
    }
    $http({
        method: 'POST',
        url: UrlApi,
        data: postData,
        headers: {
            'Content-Type': 'application/json'
        }
    }).success(
        function (data, status, headers, config) {
            Biostime.common.hideLoading();
            if (data.code == 100) {
                    loadInfo($scope,$http);
            } else {
                Biostime.common.toastMessage(data.code + ":" + data.desc);
            }

        }).error(function (data, status, headers, config) {
            Biostime.common.hideLoading();
            Biostime.common.showErrorMessage(status);
        });
    $scope.edit = true;
    $scope.editTable = true;
    $scope.ifEdit = true;
    $scope.$apply();
    $scope.edit = false;
    $scope.save = false;
    $scope.editTable = false;
    $scope.ifEdit = false;
    $scope.saNameMor = '';
    $scope.saNameAft = '';
    $scope.$apply();
}
function allCancle($scope,$http,obj){
    $scope.edit = false;
    $scope.save = false;
    $scope.editTable = false;
    $scope.ifEdit = false;
    $scope.saNameMor = '';
    $scope.saNameAft = '';
    $scope.$apply();
}
//
function showEdit($scope,$http,obj){
    if(obj.innerText.length>2){
        $scope.day = obj.innerText.split("排")[0]
    }else{
        $scope.day = obj.innerText;
    }
    var nowTimeList = document.getElementById("date").innerHTML;
    var nowYear = nowTimeList.split("年")[0];
    var nowMonth = nowTimeList.split("年")[1].split("月")[0];
    var nowDay = obj.innerText.split("排")[0];
    Biostime.common.showLoading();
    var UrlApi = BASICURL1+"jobScheduling/getJobScheduling.do";
    $http({
        method: 'GET',
        url: UrlApi,
        params:{"salesAccountNo":$scope.account,"companyDate":nowYear+"-"+nowMonth+"-"+nowDay}
    }).success(
        function (data, status, headers, config) {
            Biostime.common.hideLoading();
            if (data.code == 100) {
                $scope.saCodeMor = "";
                $scope.saNameMor = "";
                $scope.weixinCustomerCountMor = "";
                $scope.arriveShopCountMor = "";
                $scope.saNewCustomerCountMor = "";
                $scope.remarksMor = "";
                $scope.saCodeAft ="";
                $scope.saNameAft = "";
                $scope.weixinCustomerCountAft = "";
                $scope.arriveShopCountAft = "";
                $scope.saNewCustomerCountAft = "";
                $scope.remarksAft = "";
                if(data.response != null){
                   $scope.infoList = data.response;
                   for(var i =0;i< $scope.infoList.length;i++){
                       if( $scope.infoList[i].timeBucket =="0"){
                           $scope.saCodeMor = $scope.infoList[i].saCode;
                           $scope.saNameMor = $scope.infoList[i].saName;
                           $scope.weixinCustomerCountMor = $scope.infoList[i].weixinCustomerCount;
                           $scope.arriveShopCountMor = $scope.infoList[i].arriveShopCount;
                           $scope.saNewCustomerCountMor = $scope.infoList[i].saNewCustomerCount;
                           $scope.remarksMor = $scope.infoList[i].remarks;
                       }else{
                           $scope.saCodeAft = $scope.infoList[i].saCode;
                           $scope.saNameAft = $scope.infoList[i].saName;
                           $scope.weixinCustomerCountAft = $scope.infoList[i].weixinCustomerCount;
                           $scope.arriveShopCountAft = $scope.infoList[i].arriveShopCount;
                           $scope.saNewCustomerCountAft = $scope.infoList[i].saNewCustomerCount;
                           $scope.remarksAft =  $scope.infoList[i].remarks;
                       }
                   }
                }else{
                    $scope.saCodeMor = "";
                    $scope.saNameMor = "";
                    $scope.weixinCustomerCountMor = "";
                    $scope.arriveShopCountMor = "";
                    $scope.saNewCustomerCountMor = "";
                    $scope.remarksMor = "";
                    $scope.saCodeAft ="";
                    $scope.saNameAft = "";
                    $scope.weixinCustomerCountAft = "";
                    $scope.arriveShopCountAft = "";
                    $scope.saNewCustomerCountAft = "";
                    $scope.remarksAft = "";
                }
            } else {
                Biostime.common.toastMessage(data.code + ":" + data.desc);
            }

        }).error(function (data, status, headers, config) {
            Biostime.common.hideLoading();
            Biostime.common.showErrorMessage(status);
        });
    if(Biostime.common.getQueryString('ifmanagement') &&Biostime.common.getQueryString('ifmanagement')!="false"){
        $scope.nonEdit = true;
        $scope.edit = true;
        $scope.editTable = true;
        $scope.ifEdit = true;
        $scope.save = false;
    }else{
        $scope.edit = true;
        $scope.editTable = true;
        $scope.ifEdit = true;
        $scope.save = false;
        $scope.$apply();
    }
}

function checkNAN(obj){
    if(isNaN(obj.value)){
        Biostime.common.toastMessage("只可输入正整数");
        obj.value ="";
    }
//    if(!Biostime.common.isInt(obj.value)){
//        Biostime.common.toastMessage("只可输入正整数");
//        obj.value ="";
//    }
}

function checkForm($scope){
    if(!Biostime.common.isInt($scope.weixinCustomerCountMor) && $scope.weixinCustomerCountMor != "" && $scope.weixinCustomerCountMor != null){
        Biostime.common.toastMessage("微信会员只可输入正整数");
        return false;
    }
    if(!Biostime.common.isInt($scope.weixinCustomerCountAft) && $scope.weixinCustomerCountAft != "" && $scope.weixinCustomerCountAft != null){
        Biostime.common.toastMessage("微信会员只可输入正整数");
        return false;
    }
    if(!Biostime.common.isInt($scope.arriveShopCountMor) && $scope.arriveShopCountMor != "" && $scope.arriveShopCountMor != null){
        Biostime.common.toastMessage("到店数只可输入正整数");
        return false;
    }
    if(!Biostime.common.isInt($scope.arriveShopCountAft) && $scope.arriveShopCountAft != "" && $scope.arriveShopCountAft != null){
        Biostime.common.toastMessage("到店数只可输入正整数");
        return false;
    }
    if(!Biostime.common.isInt($scope.saNewCustomerCountMor) && $scope.saNewCustomerCountMor != "" && $scope.saNewCustomerCountMor != null){
        Biostime.common.toastMessage("SA新客只可输入正整数");
        return false;
    }
    if(!Biostime.common.isInt($scope.saNewCustomerCountAft) && $scope.saNewCustomerCountAft != "" && $scope.saNewCustomerCountAft != null){
        Biostime.common.toastMessage("SA新客只可输入正整数");
        return false;
    }
return true;
}