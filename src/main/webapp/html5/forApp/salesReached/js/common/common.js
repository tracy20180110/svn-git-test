//var BASICURL = "http://10.50.8.151:8080/biostime-salesmanage/";//测试
//var BASICURL1 = "http://192.168.115.33:1002/samanage/";//测试环境
var BASICURL = location.href.indexOf("y.mama100.com")>= 0 ? "http://y.mama100.com/samanage/" : "http://192.168.115.33:1002/biostime-salesmanage/"
var BASICURL1 = location.href.indexOf("y.mama100.com")>= 0 ? "http://y.mama100.com/samanage/" : "http://192.168.115.33:1002/samanage/"
/*******************************************************************************
 * 获取参数值
 * 
 * @param name
 *            参数名称
 * @returns 参数值
 */
function getQueryString(name)
{
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}

/*
* 时间格式
*
* */
Date.prototype.format = function(format){
    var o = {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(), //day
        "h+" : this.getHours(), //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3), //quarter
        "S" : this.getMilliseconds() //millisecond
    }

    if(/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    }

    for(var k in o) {
        if(new RegExp("("+ k +")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
        }
    }
    return format;
};
