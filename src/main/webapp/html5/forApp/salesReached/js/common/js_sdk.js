/**
 * 此js文件最后引入
 * **/

//初始化数据
//分享到朋友圈
var shareAppMessage = {
    title : '妈妈100微信商城', // 分享标题
    link : "http://www.mama100.com/wmall/home.html", // 分享链接
    desc : "妈妈100微信购买奶粉方便、快捷,还有更多优惠等你拿,点击进入商城",
    imgUrl : "http://www.mama100.com/wmall/images/logo.jpg",// 分享图标
    success : function() {
        // 用户确认分享后执行的回调函数
    },
    cancel : function() {
        // 用户取消分享后执行的回调函数
    }
};

// 分享到朋友圈
var shareTimeline = {
    title: '妈妈100微信商城', // 分享标题
    link: "http://www.mama100.com/wmall/home.html", // 分享链接
    imgUrl: "http://www.mama100.com/wmall/images/logo.jpg",// 分享图标
    success: function () {
        // 用户确认分享后执行的回调函数
    },
    cancel: function () {
        // 用户取消分享后执行的回调函数
    }
};

var shareQQ = {
    title : '妈妈100微信商城', // 分享标题
    link : "http://www.mama100.com/wmall/home.html", // 分享链接
    desc : "妈妈100微信购买奶粉方便、快捷,还有更多优惠等你拿,点击进入商城",
    imgUrl : "http://www.mama100.com/wmall/images/logo.jpg",// 分享图标
    success : function() {
        // 用户确认分享后执行的回调函数
    },
    cancel : function() {
        // 用户取消分享后执行的回调函数
    }
};

 var isDone = false;
var isSetShareTimelineFunc = false;
var isSetShareAppMessageFunc = false;
var isSetShareQQFunc = false;
var sha,AppId;
$(document).ready(function(){

      $.ajax({
            url:BASICURL + "/service/jsticket",
            success:function(data){

                appid(data.jsticket)
            }
        });
    function appid(jsticket){

        $.ajax({
            url:BASICURL + "/service/appId",
            success:function(data){
                sha = hex_sha1('jsapi_ticket='+jsticket+'&noncestr='+'TdeaEHa0gN4K3RAV'+'&timestamp=1447234097&url='+window.location.href);

                AppId=data.appId;

                wx.config({
                    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                    appId: AppId, // 必填，公众号的唯一标识
                    timestamp: 1447234097, // 必填，生成签名的时间戳
                    nonceStr: 'TdeaEHa0gN4K3RAV', // 必填，生成签名的随机串
                    signature: sha,// 必填，签名，见附录1
                    jsApiList: [
                        'checkJsApi',
                        'onMenuShareTimeline',
                        'onMenuShareAppMessage',
                        'onMenuShareQQ',
                        'onMenuShareWeibo',
                        'hideMenuItems',
                        'showMenuItems',
                        'hideAllNonBaseMenuItem',
                        'showAllNonBaseMenuItem',
                        'translateVoice',
                        'startRecord',
                        'stopRecord',
                        'onRecordEnd',
                        'playVoice',
                        'pauseVoice',
                        'stopVoice',
                        'uploadVoice',
                        'downloadVoice',
                        'chooseImage',
                        'previewImage',
                        'uploadImage',
                        'downloadImage',
                        'getNetworkType',
                        'openLocation',
                        'getLocation',
                        'hideOptionMenu',
                        'showOptionMenu',
                        'closeWindow',
                        'scanQRCode',
                        'chooseWXPay',
                        'openProductSpecificView',
                        'addCard',
                        'chooseCard',
                        'openCard'
                    ]
                });
                if(isSetShareTimelineFunc){
                    wx.onMenuShareTimeline(shareTimeline);
                }
                if(isSetShareAppMessageFunc){
                    wx.onMenuShareAppMessage(shareAppMessage);
                }
                if(isSetShareQQFunc){
                    wx.onMenuShareQQ(shareQQ);
                }
            }
        });
    }


});



var ShareSDK = {};
function ShareSDK() {

};
ShareSDK.checkJsApi = function(){
    wx.checkJsApi({
        jsApiList: ['checkJsApi',
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'hideMenuItems',
            'showMenuItems',
            'hideAllNonBaseMenuItem',
            'showAllNonBaseMenuItem',
            'translateVoice',
            'startRecord',
            'stopRecord',
            'onRecordEnd',
            'playVoice',
            'pauseVoice',
            'stopVoice',
            'uploadVoice',
            'downloadVoice',
            'chooseImage',
            'previewImage',
            'uploadImage',
            'downloadImage',
            'getNetworkType',
            'openLocation',
            'getLocation',
            'hideOptionMenu',
            'showOptionMenu',
            'closeWindow',
            'scanQRCode',
            'chooseWXPay',
            'openProductSpecificView',
            'addCard',
            'chooseCard',
            'openCard'
        ], // 需要检测的JS接口列表，所有JS接口列表见附录2,
        success: function(res) {
            // 以键值对的形式返回，可用的api值true，不可用为false
            // 如：{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
            return res;
        }
    });
};

/**
 * 分享到朋友圈
 * */
ShareSDK.shareTimelineFunc = function (title,link,imgUrl,successCallback,cancelCallback){
    //分享到朋友圈
    shareTimeline = {
        title: title, // 分享标题
        link: link, // 分享链接
        imgUrl: imgUrl,// 分享图标
        success: successCallback,
        cancel: cancelCallback
    };
    isSetShareTimelineFunc=true;
    if(isDone){
        wx.onMenuShareTimeline(shareTimeline);
    }
}

/**
 *分享到微信好友
 * */
ShareSDK.shareAppMessageFunc = function (title,link,desc,imgUrl,successCallback,cancelCallback){
    shareAppMessage = {
        title : title, // 分享标题
        link : link, // 分享链接
        desc : desc,
        imgUrl : imgUrl,// 分享图标
        success : successCallback,
        cancel : cancelCallback
    };
    isSetShareAppMessageFunc=true;
    if(isDone){
        wx.onMenuShareAppMessage(shareAppMessage);
    }
}

/**
 * 分享到qq
 * */
ShareSDK.shareQQFunc = function (title,link,desc,imgUrl,successCallback,cancelCallback){
    shareAppMessage = {
        title : title, // 分享标题
        link : link, // 分享链接
        desc : desc,
        imgUrl : imgUrl,// 分享图标
        success : successCallback,
        cancel : cancelCallback
    };
    isSetShareQQFunc=true;
    if(isDone){
        wx.onMenuShareQQ(shareQQ);
    }
}

/**
 * 隐藏菜单
 * */
ShareSDK.hideMenu = function(){
    wx.hideOptionMenu();
};
/**
 * 显示菜单
 * */
ShareSDK.showMenu = function(){
    wx.showOptionMenu();
};
/**
 * 关闭窗口
 * */
ShareSDK.close = function (){
    wx.closeWindow();
}
/**
 * 获取网络状态
 * */
ShareSDK.getNetworkType = function(){
    wx.getNetworkType({
        success: function (res) {
            return res.networkType;
        },
        fail: function (res) {
            alert(JSON.stringify(res));
        }
    });
};
/**
 * 在地图中打开坐标位置
 * */
ShareSDK.openLocation = function(latitude,longitude,name,address,scale,infoUrl){
    wx.openLocation({
        latitude: latitude,
        longitude: longitude,
        name: name,
        address: address,
        scale: scale,
        infoUrl: infoUrl
    });
};
/**
 * 获取地理位置
 * */
ShareSDK.getLocation = function(){
    wx.getLocation({
        success: function (res) {
            return res;
        },
        cancel: function (res) {
            alert('用户拒绝授权获取地理位置');
        }
    });
};

/**
 * 扫描二维码,微信直接打开结果
 * */
ShareSDK.scanQRCodeByWX = function(){
    wx.scanQRCode({
        desc: 'scanQRCode desc'
    });
};

/**
 * 扫描二维码,直接返回结果
 * */
ShareSDK.scanQRCode = function(){
    wx.scanQRCode({
        needResult: 1,
        desc: 'scanQRCode desc',
        success: function (res) {
            return res;
        }
    });
};

/**
 * 选择图片
 * */
ShareSDK.uploadImage = function(){
    wx.chooseImage({
        success: function (res) {
            return res;
        }
    });
};

/**
 * 预览图片
 * */
ShareSDK.previewImage = function(urls,index){
    if(index==undefined){
        index = 0;
    }
    wx.previewImage({
        current: urls[index],
        urls: urls
    });
};