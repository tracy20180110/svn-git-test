var ngApp = angular.module("ngApp",[]).config(
    [ '$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    } ])

var lastSixsMonth = [];
function getLastSixsMonth(){
    var today = new Date();
    if(today.getDate()>25){
        today.setMonth(today.getMonth()+2);
        for(var i =0;i<6;i++){
            var lastMonth = today.setMonth(today.getMonth()-1);
            if(today.getMonth()+1 >9){
                lastSixsMonth[i] = today.getFullYear() + "-" + Number(today.getMonth()+1);
            }else{
                lastSixsMonth[i] = today.getFullYear() + "-0" + Number(today.getMonth()+1);
            }
        }
        return lastSixsMonth;
    }else{
        today.setMonth(today.getMonth()+1);
        for(var i =0; i<6;i++){
            var lastMonth = today.setMonth(today.getMonth()-1);
            if(today.getMonth()+1>9){
                lastSixsMonth[i] = today.getFullYear() + "-" + Number(today.getMonth()+1);
            }else{
                lastSixsMonth[i] = today.getFullYear() + "-0" + Number(today.getMonth()+1);
            }
        }
        return lastSixsMonth;
    }
}
getLastSixsMonth();
ngApp.controller("salesReachedSaleDetails_controller",['$scope','$http','$timeout',function($scope,$http,$timeout){
    $scope.devid = Biostime.common.getQueryString("devid");
    $scope.tokenVer = Biostime.common.getQueryString("tokenVer");
    $scope.token = Biostime.common.getQueryString("token");
    $scope.opno = Biostime.common.getQueryString("opno");
    $scope.officeCode = Biostime.common.getQueryString("officeCode");
    $scope.detailDate = getLastSixsMonth();
    $scope.nowDate = $scope.detailDate[0];
    if(Biostime.common.getQueryString('month') &&Biostime.common.getQueryString('month')!="false"){
        $scope.nowDate = Biostime.common.getQueryString("month");
    }
    $scope.name = decodeURI(Biostime.common.getQueryString("name"));
    $scope.account =  Biostime.common.getQueryString("account");
    $scope.search = function(){
        search($scope,$http);
    }
    $scope.changeType = function(nowDate){
        loadList($scope,$http);
    }
    loadList($scope,$http);

//    var month = Biostime.common.getQueryString("month");
//    for(var i = 0; i<$scope.detailDate.length;i++){
//        if($scope.detailDate[i] == month[0]){
//            $scope.nowTime = $scope.detailDate[i];
//        }
//    }
}])

//读取数据
function loadList($scope,$http){
    Biostime.common.showLoading();
    var listUrl = BASICURL1+"saPrize/querySaPrizeDetailInfo.do";    //测试地址
    var postData ={
        "request":{
            "officeCode":$scope.officeCode,
            "month":$scope.nowDate,
            "account": $scope.account
        }
    }
    $http({
        method: 'POST',
        url: listUrl,
        data:postData,
        headers: {
            'Content-Type': 'application/json'
        }
    }).success(
        function (data, status, headers, config) {
            Biostime.common.hideLoading();
            if (data.code == 100) {
                $scope.infoList = data.response[0];
            } else {
                Biostime.common.toastMessage(data.code + ":" + data.desc);
            }

        }).error(function (data, status, headers, config) {
            Biostime.common.hideLoading();
            Biostime.common.showErrorMessage(status);
        });
}





