var ngApp = angular.module("ngApp",[]).config(
    [ '$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    } ])
var time = new Date();
var num = "0123456789";
var value = "",i; //value为随机的4位数
for(j=1;j<=4;j++){
    i = parseInt(10*Math.random());
    value = value + num.charAt(i);
}
var lastSixsMonth = [];
function getLastSixsMonth(){
    var today = new Date();
    if(today.getDate()>25){
        today.setMonth(today.getMonth()+2);
        for(var i =0;i<6;i++){
            var lastMonth = today.setMonth(today.getMonth()-1);
            if(today.getMonth()+1 >9){
                lastSixsMonth[i] = today.getFullYear() + "-" + Number(today.getMonth()+1);
            }else{
                lastSixsMonth[i] = today.getFullYear() + "-0" + Number(today.getMonth()+1);
            }
        }
        return lastSixsMonth;
    }else{
        today.setMonth(today.getMonth()+1);
        for(var i =0; i<6;i++){
            var lastMonth = today.setMonth(today.getMonth()-1);
            if(today.getMonth()+1>9){
                lastSixsMonth[i] = today.getFullYear() + "-" + Number(today.getMonth()+1);
            }else{
                lastSixsMonth[i] = today.getFullYear() + "-0" + Number(today.getMonth()+1);
            }
        }
        return lastSixsMonth;
    }
}
getLastSixsMonth();

ngApp.controller("salesReachedForManagement_controller",['$scope','$http','$timeout',function($scope,$http,$timeout){
    $scope.devid = Biostime.common.getQueryString("devid");
    $scope.tokenVer = Biostime.common.getQueryString("tokenVer");
    $scope.token = Biostime.common.getQueryString("token");
    $scope.opno = Biostime.common.getQueryString("opno");
    $scope.areaOfficeCode = Biostime.common.getQueryString("areaOfficeCode");
    $scope.changeDestrict = function(region){
        if(region != null){
             $scope.office = $scope.region.officeBeanList[0];
        }
    }
    $scope.view = function(index){
        view($scope,$http,index);
    }
    $scope.search = function(){
        search($scope,$http)
    }
    $scope.dateList = getLastSixsMonth();
    $scope.nowTime = $scope.dateList[0];
    if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
        if(sessionStorage.setItem){
            var month = sessionStorage.getItem("month");
            if(month != null){
                $scope.nowTime = month;
            }
        }
    }
    getArea($scope,$http);
//    setTimeout(function(){
//        loadList($scope,$http);
//    },800)
}])

/**
 * 查看
 * @param $http
 */
function view($scope,$http,index){
    if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
         sessionStorage.setItem("month",$scope.nowTime);
//        return document.location="objc://openWindows/"+"http://10.50.8.151:8080/biostime-salesmanage/forApp/salesReached/salesReachSaleDetails.html?month="+$scope.nowTime+"&officeCode="+ $scope.office.code+"&account="+$scope.infoList[index].account+"&name="+$scope.infoList[index].name; //cmd代表objective-c中的的方法名，parameter1自然就是参数了
        window.location.href = "salesReachSaleDetails.html?month="+$scope.nowTime+"&officeCode="+ $scope.office.code+"&account="+$scope.infoList[index].account+"&name="+$scope.infoList[index].name;
        //commonPlugin.ScanCode(null, null, $scope.type)
    } else if (/(Android)/i.test(navigator.userAgent)) {
        javascript:appjscall.openWindows("http://y.mama100.com/samanage/forApp/salesReached/salesReachSaleDetails.html?month="+$scope.nowTime+"&officeCode="+ $scope.office.code+"&account="+$scope.infoList[index].account+"&name="+$scope.infoList[index].name,"SA销量达成");
    } else {
        window.location.href = "salesReachSaleDetails.html?month="+$scope.nowTime+"&officeCode="+ $scope.office.code+"&account="+$scope.infoList[index].account+"&name="+$scope.infoList[index].name;
    }
//    sessionStorage.setItem("name",$scope.infoList[index].name);
//    window.location.href = "salesReachSaleDetails.html?month="+$scope.nowTime+"&officeCode="+ $scope.office.code+"&account="+$scope.infoList[index].account;
}


/**
 *查询
 * @param $http
 */
function search($scope,$http){
    if(checkForm($scope)){
         loadList($scope,$http);
    }
}

function loadList($scope,$http){
    Biostime.common.showLoading();
    var loadList =  BASICURL1+ "saPrize/querySaPrizeList.do";//测试地址
    var postData = {
        "request":{
            "officeCode":$scope.office.code,
            "month": $scope.nowTime
        }
    }
    $http({
        method: 'POST',
        url: loadList,
        data: postData,
        headers: {
            'Content-Type': 'application/json'
        }
    }).success(
        function (data, status, headers, config) {
            Biostime.common.hideLoading();
            if (data.code == 100) {
                if(data.response.length == 0){
                    Biostime.common.toastMessage("暂无数据");
                }
                $scope.infoList = data.response;

            } else {
                Biostime.common.toastMessage(data.code + ":" + data.desc);
            }

        }).error(function (data, status, headers, config) {
            Biostime.common.hideLoading();
            Biostime.common.showErrorMessage(status);
        });
}

function getArea($scope,$http){
    Biostime.common.showLoading();
    var AreaUrl = BASICURL1+"areaOffice/queryAreaOffice.do"
    var postData = {
        "request":{
            "areaOfficeCode":$scope.areaOfficeCode
        }
    }
    $http({
        method: 'POST',
        url: AreaUrl,
        data:postData,
        headers: {
            'Content-Type': 'application/json'
        }
    }).success(
        function (data, status, headers, config) {
            Biostime.common.hideLoading();
            if (data.code == 100) {
                $scope.area = data.response;
                $scope.region = $scope.area[0];
                $scope.office = $scope.region.officeBeanList[0];
                loadList($scope,$http);
            } else {
                Biostime.common.toastMessage(data.code + ":" + data.desc);
            }
        }).error(function (data, status, headers, config) {
            Biostime.common.hideLoading();
            Biostime.common.showErrorMessage(status);
        });
}

function checkForm($scope){
    if($scope.region == "" || $scope.region == undefined){
        Biostime.common.toastMessage("请选择区域");
        return false;
    }
    if($scope.office == "" || $scope.office == undefined){
        Biostime.common.toastMessage("请选择办事处");
        return false;
    }
    return true;
}
