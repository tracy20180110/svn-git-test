"use strict";
APP.directive('globalMessages', function() {
    return {
        restrict: 'EA',
        templateUrl: './globalMessage.html',
        replace: true,
        controller:function($scope,$interval){
        	$scope.active=false;
        	$scope.time=0;//0==disable,-1=show until being replace or shutdown manuly,320=showtime, millsecond.
        	$scope.template="info";
        	$scope.height=function(){
        				return (document.querySelector("messages>message.active")==null?0:document.querySelector("messages>message.active").offsetHeight)
        				},
        	$scope.content="消息";
        	$scope.interval=$interval(function(){
        				//console.log($scope.active,$scope.time)
        				if(!$scope.active){return}//do nothing if disabled
        				if($scope.time>0){
        					if($scope.time>1000){
        					$scope.time-=1000;
        					}else{$scope.time=0;
        					}
        				}
        				if($scope.time==0){
        					$scope.active=false;
        				}
        			},1000);
        			$scope.stop=function(){$interval.cancel($scope.interval);};
        			$scope.$on("setGlobalMessage",function(evt,obj){
        				//obj:{content:"hello world"}
        				//console.log(1,evt,obj,$scope.interval)
        				if(typeof(obj)=="undefined"){
        					$scope.active=false;
        				}else{
        				$scope.template=obj.template||"info";
        				$scope.time=obj.time||-1;
        				$scope.content=obj.content||"";
        				$scope.active=true;
        				}
        			});
        			$scope.$on("$destroy",function(){
        				 $scope.stop();
        			});
        }
    };
})