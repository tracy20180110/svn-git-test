package com.biostime.samanage.repository.saPrize;

import com.biostime.common.oraclerepo.BaseOracleRepository;
import com.biostime.samanage.bean.saPrize.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA. Describe:SA育婴顾问奖金 Date: 2016-6-23 Time: 16:53
 * User: 12804 Version:1.0
 */
public interface SaPrizeRepository extends BaseOracleRepository {

	/**
	 * 获取SA育婴顾问奖金
	 * 
	 * @param saPrizeReqBean
	 * @return
	 */
	public List<SaPrizeResBean> querySaPrizeList(SaPrizeReqBean saPrizeReqBean);

	/**
	 * 获取SA育婴顾问奖金
	 * 
	 * @param saPrizeDetailReqBean
	 * @return
	 */
	public List<SaPrizeDetailResBean> querySaPrizeDetailInfo(SaPrizeDetailReqBean saPrizeDetailReqBean);

	/**
	 * 获取SA考核指标信息，APP调用
	 * 
	 * @param saPrizeReqBean
	 * @return
	 */
	public SaAssessIndexResBean querySaAssessIndexInfo(SaPrizeReqBean saPrizeReqBean);

	/**
	 * 获取SA考核指标信息，APP调用
	 *
	 * @param saPrizeDetailReqBean
	 * @return
	 */
	public SaAssessIndexResBean querySaAssessIndexInfo(SaPrizeDetailReqBean saPrizeDetailReqBean);

	/**
	 * 是否是SA育婴顾问
	 * 
	 * @param saPrizeReqBean
	 * @return
	 */
	public boolean isSaConsultant(SaPrizeDetailReqBean saPrizeReqBean);

	/**
	 * 获取积分新客信息，APP调用
	 * 
	 * @param saPrizeReqBean
	 * @return
	 */
	public SaAssessIndexResBean queryIntegralNewCustInfo(SaPrizeReqBean saPrizeReqBean);

}
