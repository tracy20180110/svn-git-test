package com.biostime.samanage.repository.sign;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.sales.SalesSignResBean;
import com.biostime.samanage.bean.sign.SaSignMerchantReqBean;
import com.biostime.samanage.bean.sign.SaSignReqBean;
import com.biostime.samanage.bean.sign.SaSignResBean;
import com.biostime.samanage.bean.sign.SaSignTerminalResBean;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

/**
 * Describe:促销员打卡Repository
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface SaSignRepository {

    /**
     * 促销员打卡查询列表
     *
     * @param reqBean
     * @return
     */
    public Pager<SaSignResBean> querySaSignList(Pager<SaSignResBean> pager, SaSignReqBean reqBean) throws ParseException;

    /**
     * 促销员打卡查询列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaSignList(HttpServletResponse response, SaSignReqBean reqBean) throws Exception;

    /**
     * 促销员打卡(手机端)
     *
     * @param reqBean
     * @return
     */
    public void saveSaSign(SaSignMerchantReqBean reqBean) throws Exception;


    /**
     * 查询是否在指定的门店(手机端)
     *
     * @param reqBean
     * @return
     */
    public ServiceBean<List<SaSignTerminalResBean>> queryIsInTerminal(SaSignMerchantReqBean reqBean) throws Exception;


    int getSaSingCount(String createdBy, String terminalCode, Integer terminalNum);

    String getSaSingTerminalCode(String createdBy);

    int getSaSingCountOn(String createdBy);
}
