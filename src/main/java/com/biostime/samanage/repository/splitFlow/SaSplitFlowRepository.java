package com.biostime.samanage.repository.splitFlow;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.splitFlow.SaSplitFlowBean;
import com.biostime.samanage.domain.splitflow.BstOtoCustSa;

import java.text.ParseException;
import java.util.List;

/**
 * Created by zhuhaitao on 2017/3/2.
 */
public interface SaSplitFlowRepository {
    Pager<SaSplitFlowBean> searchSaSplitFlow(Pager<SaSplitFlowBean> pager, SaSplitFlowBean saSplitFlowBean) throws ParseException;

    List<SaSplitFlowBean> getSaSplitFlow(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobilePhone, String terminalCode, String startTime, String endTime, String terminalChannelCode, String chainChannelCode) throws ParseException;

    String getNursingConsultantBySACode(String saCode);

    BstOtoCustSa getBstOtoCustSAByCustomerId(String customerId);

    void updateBstOtoCustSA(BstOtoCustSa bstOtoCustSa);

    void saveBstOtoCustSA(BstOtoCustSa bstOtoCustSa) throws IllegalAccessException;

    String getRelationChainCodeBySACode(String sendCouponTerminal);
}
