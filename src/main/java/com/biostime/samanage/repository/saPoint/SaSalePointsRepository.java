package com.biostime.samanage.repository.saPoint;

import com.biostime.common.bean.Pager;
import com.biostime.common.oraclerepo.BaseOracleRepository;
import com.biostime.samanage.bean.saPoint.SaSalePointsBean;

import java.text.ParseException;
import java.util.List;

/**
 * SA销售积分
 */
public interface SaSalePointsRepository extends BaseOracleRepository {
    /**
     * 根据条件搜索
     *
     * @param babyConsultant
     * @param areaCode
     * @param officeCode
     * @param startDate
     * @param endDate
     * @param bccCode
     * @return
     * @throws ParseException
     */
    List<SaSalePointsBean> getSaSalePoints(String babyConsultant, String areaCode, String officeCode, String startDate, String endDate, String bccCode,String buCode) throws ParseException;

    /**
     * SA销售积分分页查询
     *
     * @param pager
     * @param saSalePointsBean
     * @return
     * @throws ParseException
     */
    Pager<SaSalePointsBean> searchSaSalePoints(Pager<SaSalePointsBean> pager, SaSalePointsBean saSalePointsBean) throws ParseException;
}
