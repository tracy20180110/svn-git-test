package com.biostime.samanage.repository.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.single.adset.SaAdSetExternalResBean;
import com.biostime.samanage.bean.single.adset.SaAdSetReqBean;
import com.biostime.samanage.bean.single.adset.SaAdSetResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA广告图设置Repository
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface SaAdSetRepository {

    /**
     * SA广告图设置查询列表
     * @param reqBean
     * @return
     */
    public Pager<SaAdSetResBean> querySaAdSetList(Pager<SaAdSetResBean> pager, SaAdSetReqBean reqBean);

    /**
     * SA广告图设置查询列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaAdSetList(HttpServletResponse response, SaAdSetReqBean reqBean) throws Exception;

    /**
     * SA广告图设置
     * @param reqBean
     * @return
     */
    public ServiceBean saveSaAdSet(SaAdSetReqBean reqBean) throws Exception;



    /**
     * SA广告图设置--状态修改
     * @param reqBean
     * @return
     */
    public void changeStatusSaAdSet(SaAdSetReqBean reqBean);

    /**
     * SA广告图设置--SA广告图片验证门店信息
     * @param reqBean
     * @return
     */
    public ServiceBean checkTerminalInfo(SaAdSetReqBean reqBean);


    /**
     * 查询SA广告图片（提供给C端调用）
     * @param saCode
     * @return
     */
    public SaAdSetExternalResBean querySaAdSetImage(String saCode) ;




}
