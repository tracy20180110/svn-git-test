package com.biostime.samanage.repository.single;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerReqBean;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA品项新客报表
 * Date: 2019-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaProductItemNewCustomerRepository {
    /**
     * SA品项新客报表
     * @param reqBean
     * @return
     */
    public Pager<SaProductItemNewCustomerResBean> querySaProductItemNewCustomerList(Pager<SaProductItemNewCustomerResBean> pager, SaProductItemNewCustomerReqBean reqBean);

    /**
     * SA品项新客报表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaProductItemNewCustomerList(HttpServletResponse response, SaProductItemNewCustomerReqBean reqBean) throws Exception;

}
