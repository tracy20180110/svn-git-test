package com.biostime.samanage.repository.single;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportReqBean;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA微信会员报表Repository
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaWxCustomerReportRepository {
    /**
     * SA微信会员报表列表
     * @param reqBean
     * @return
     */
    public Pager<SaWxCustomerReportResBean> querySaWxCustomerReportList(Pager<SaWxCustomerReportResBean> pager, SaWxCustomerReportReqBean reqBean);

    /**
     * SA微信会员报表列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaWxCustomerReportList(HttpServletResponse response, SaWxCustomerReportReqBean reqBean) throws Exception;

}
