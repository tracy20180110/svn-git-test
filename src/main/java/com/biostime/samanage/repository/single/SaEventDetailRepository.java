package com.biostime.samanage.repository.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.single.event.SaEventDetailReqBean;
import com.biostime.samanage.bean.single.event.SaEventDetailResBean;
import com.biostime.samanage.bean.single.event.SaEventReqBean;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA接触Repository
 * Date: 2017-07-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaEventDetailRepository {
    /**
     * SA接触列表-查询
     * @param reqBean
     * @return
     */
    public Pager<SaEventDetailResBean> querySaEventDetailList(Pager<SaEventDetailResBean> pager, SaEventDetailReqBean reqBean);

    /**
     * SA接触列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaEventDetailList(HttpServletResponse response, SaEventDetailReqBean reqBean) throws Exception;

    /**
     * 扫码注册绑定会员
     * @param map
     * @return
     */
    public void saveSaWxBindNewCustomer(Map map) throws Exception;

    /**
     * 妈妈100微信、营销通app接触签到
     * @param reqBean
     * @return
     */
    public ServiceBean saveSaEvent(SaEventReqBean reqBean) throws Exception;


}
