package com.biostime.samanage.repository.single;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActResBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerResBean;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface SaLecturerRepository {

      /**
     * SA讲师看板
     * @param reqBean
     * @return
     */
    public List<SaLecturerResBean> querySaLecturerInfo(SaLecturerReqBean reqBean) ;

    /**
     * SA指标
     * @param reqBean
     * @return
     */
    public List<BaseKeyValueBean> querySaIndex(SaLecturerReqBean reqBean) ;

    /**
     * SA讲师活动报表查詢
     * @param reqBean
     * @return
     */
    public Pager<SaLecturerActResBean> querySaLecturerActList(Pager<SaLecturerActResBean> pager, SaLecturerActReqBean reqBean);

    /**
     * SA讲师活动报表查詢导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaLecturerActList(HttpServletResponse response, SaLecturerActReqBean reqBean) throws Exception;
    /**
     * SA讲师活动报表查詢导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaLecturerActDownList(HttpServletResponse response, SaLecturerActReqBean reqBean) throws Exception;

}
