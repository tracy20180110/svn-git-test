package com.biostime.samanage.repository.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.caravan.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:大小篷车申请
 * Date: 2017-12-15
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface CaravanRepository {
    /**
     * 查询分销物料信息
     * @return
     */
    public List<CaravanCmaterialInfo> queryCaravanMaterielList(CaravanReqBean reqBean);


    /**
     * 大小篷车申请
     * @param reqBean
     * @return
     */
    public ServiceBean saveCaravan(CaravanReqBean reqBean,HttpServletRequest reqeust) throws Exception;

    /**
     * 大小篷车申请
     * @param reqBean
     * @return
     */
    public void saveCaravanImgAudit(CaravanImgAuditReqBean reqBean,HttpServletRequest reqeust) throws Exception;

    /**
     * 大小篷车申请OA审批
     * @param reqBean
     * @return
     */
    public ServiceBean caravanOaAudit(CaravanReqBean reqBean,HttpServletRequest reqeust);

    /**
     * 大小篷车状态更新
     * @param reqBean
     * @return
     */
    public void editCaravanStatus(CaravanReqBean reqBean,HttpServletRequest reqeust) throws Exception;

    /**
     * 更新大小篷车OA流程审批状态
     * @param reqBean
     * @return
     */
    public String editCaravanOaAuditStatus(CaravanReqBean reqBean) throws Exception;

    /**
     * 重新推送获取订单
     * @param reqBean
     * @return
     */
    public String caravanOrder(CaravanReqBean reqBean,HttpServletRequest reqeust) throws Exception;

    /**
     * 大小篷车列表
     * @param reqBean
     * @return
     */
    public Pager<CaravanListResBean> querySaCaravanList(Pager<CaravanListResBean> pager, CaravanListReqBean reqBean);

    /**
     * 大小篷车列表--导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaCaravanList(HttpServletResponse response, CaravanListReqBean reqBean) throws Exception;

    /**
     * 查询SA门店
     * @param reqBean
     * @return
     */
    public ServiceBean querySaTerminalInfo(CaravanReqBean reqBean);

    /**
     * 查询大小篷车信息
     * @param reqBean
     * @return
     */
    public CaravanReqBean queryCaravanInfo(CaravanReqBean reqBean);


    /**
     * 查询可更新的人员列表
     * @param reqBean
     * @return
     */
    public List<String> queryUpdateByList(CaravanListReqBean reqBean);


    /**
     * 更新申请人
     * @param reqBean
     * @return
     */
    public ServiceBean editUpdateBy(CaravanReqBean reqBean);

    /**
     * 查询大小篷车操作日志
     * @param reqBean
     * @return
     */
    public List<CaravanLogInfo> queryCaravanLogList(CaravanReqBean reqBean);

      /**
     * 查询大小篷车操作日志
     * @param reqBean
     * @return
     */
    public List<CaravanTpmCostInfo> queryTpmCostList(CaravanTpmCostInfo reqBean);
}
