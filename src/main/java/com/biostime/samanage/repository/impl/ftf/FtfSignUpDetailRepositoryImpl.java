package com.biostime.samanage.repository.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.ftf.sign.FtfSignUpDetailReqBean;
import com.biostime.samanage.bean.ftf.sign.FtfSignUpDetailResBean;
import com.biostime.samanage.repository.ftf.FtfSignUpDetailRepository;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动报名信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("ftfSignUpDetailRepository")
public class FtfSignUpDetailRepositoryImpl extends BaseOracleRepositoryImpl implements FtfSignUpDetailRepository {



    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(FtfSignUpDetailReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //课程名称
        String name = req.getName();
        if (StringUtil.isNotNullNorEmpty(name)) {
            paramsMap.put("name", name.trim());
        }
        //活动类型
        String type = req.getType();
        if (StringUtil.isNotNullNorEmpty(type)) {
            paramsMap.put("type", type.trim());
        }

        //SA编码
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }
        //门店编码
        String terminalCode = req.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode.trim());
        }
        //创建人工号
        String createdBy = req.getCreatedBy();
        if (StringUtil.isNotNullNorEmpty(createdBy)) {
            paramsMap.put("createdBy", createdBy.trim());
        }


        //活动开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //活动结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        return paramsMap;
    }
    /**
     * FTF活动报名信息
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<FtfSignUpDetailResBean> queryFtfSignUpDetailList(Pager<FtfSignUpDetailResBean> pager, FtfSignUpDetailReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfSignUpDetailRepositoryImpl.queryFtfSignUpDetailList", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, FtfSignUpDetailResBean.class);
    }

    /**
     * FTF活动报名信息导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportFtfSignUpDetailList(HttpServletResponse response, FtfSignUpDetailReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfSignUpDetailRepositoryImpl.queryFtfSignUpDetailList", freeMakerContext);
        List<FtfSignUpDetailResBean> bscarList = this.findAllForListBySql(sql,paramsMap,FtfSignUpDetailResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "FTF报名明细";

        th = new String[]{"大区","办事处","课程名称","活动日期","开始时间","结束时间","报名手机号码",
                         "报名时间","报名途径","SA编码","SA名称","门店编码","活动类型","活动状态","创建人工号","创建姓名"};

        colName = new String[] {"areaName","officeName","kcName","actDate","startTime","endTime",
                "mobile","singUpTime","formSysSourse","saCode","saName","terminalCode","type","status","createdBy","createdName"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
}
