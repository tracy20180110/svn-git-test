package com.biostime.samanage.repository.impl.single;

import cn.com.weaver.services.webservices.WorkflowService;
import cn.com.weaver.services.webservices.WorkflowServicePortType;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.caravan.*;
import com.biostime.samanage.domain.caravan.Caravan;
import com.biostime.samanage.domain.caravan.CaravanCmaterial;
import com.biostime.samanage.domain.caravan.CaravanImgAudit;
import com.biostime.samanage.domain.caravan.CaravanLog;
import com.biostime.samanage.repository.single.CaravanRepository;
import com.biostime.samanage.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import weaver.workflow.webservices.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Describe:大小篷车申请
 * Date: 2017-12-15
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("caravanRepository")
public class CaravanRepositoryImpl extends BaseOracleRepositoryImpl implements CaravanRepository {

    @Value( "${sa.caravan.oa}" )
    private String sa_caravan_oa;

    @Value( "${sa.caravan.dealer}" )
    private String sa_caravan_dealer;

    @Value( "${sa.caravan.dealer.tpm}" )
    private String sa_caravan_dealer_tpm;

    @Value( "${sa.oa.workflowid}" )
    private String sa_oa_workflowid;


    /**
     * 查询分销物料信息
     *
     * @return
     */
    @Override
    public List<CaravanCmaterialInfo> queryCaravanMaterielList(CaravanReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        Map map = new HashMap();  //申请类型 1：小篷车：2：大篷车   物料类型（1：儿科：2：产科）
        map.put("2-1",1);
        map.put("2-2",2);
        map.put("1-1",3);
        map.put("1-2",4);

        String key = String.valueOf(reqBean.getCaravanType()) + "-" + String.valueOf(reqBean.getCmaterialType());
        paramsMap.put("cmaterialType", String.valueOf(map.get(key)));

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("CaravanRepositoryImpl.queryCaravanMaterielList", freeMakerContext);
        System.out.println("----CaravanRepositoryImpl.queryCaravanMaterielList SQL----"+sql.toString());
        List<CaravanCmaterialInfo> caravanMaterielInfoList = this.findAllForListBySql(sql, paramsMap,CaravanCmaterialInfo.class);
        return caravanMaterielInfoList;
    }

    /**
     * 大小篷车申请
     *
     * @param reqBean
     * @return
     */
    @Override
    @Transactional
    public ServiceBean saveCaravan(CaravanReqBean reqBean,HttpServletRequest reqeust) throws Exception {

        ServiceBean serviceBean = new ServiceBean();
        try {

            //如果是本地制作，不做门店、类型的验证  1：当地制作：2：总部发货
            if (reqBean.getDeliveryMode() == 2) {
                //验证 同一个门店只能为大小篷车物料分为：大篷车-产科、大篷车-儿科；小篷车-产科、小篷车-儿科
                StringBuffer sb = new StringBuffer();
                sb.append(" from Caravan c where 1=1 and c.caravanType = :caravanType " +
                        "and c.terminalCode = :terminalCode " +
                        "and c.deliveryMode = 2 " +
                        "and c.procedureStatus != 10 " +
                        "and c.cmaterialType = :cmaterialType ");
                if (StringUtil.isNotNullNorEmpty(reqBean.getId())) {
                    sb.append(" and c.id <>:id");
                }
                Query query = super.createQuery(sb.toString());
                query.setParameter("caravanType", reqBean.getCaravanType());
                query.setParameter("terminalCode", reqBean.getTerminalCode());
                query.setParameter("cmaterialType", reqBean.getCmaterialType());
                if (StringUtil.isNotNullNorEmpty(reqBean.getId())) {
                    query.setParameter("id", Long.valueOf(reqBean.getId()));
                }
                Caravan caravan = (Caravan) query.uniqueResult();
                if (null != caravan) {
                    //说明同一个类型、门店、物料类型已经存在过关系
                    serviceBean.setCode(300);
                    serviceBean.setDesc("同一个申请类型、门店、物料类型已经存在");
                    return serviceBean;
                }
            }
            Caravan caravan = MapperUtils.map(reqBean, Caravan.class);
            if (StringUtil.isNotNullNorEmpty(reqBean.getId())) {  //更换、修改
                Caravan caravanBean = super.getHibernateTemplate().get(Caravan.class, Long.valueOf(reqBean.getId()));
                Date createdDate = caravanBean.getCreatedTime();
                caravan.setUpdatedTime(new Date());
                caravan.setCreatedTime(createdDate);

            }else {
                caravan.setCreatedTime(new Date());
                caravan.setStatus(1L);
            }

            if (reqBean.getDeliveryMode() == 1) { //1：当地制作
                caravan.setProvince("");
                caravan.setCity("");
                caravan.setDistrict("");
                caravan.setCmaterialType(null);
            }
            //保存成功，提交OA审批流程
            OaResult oaResult = submitOaAutdit(reqBean, caravan);
            caravan.setOaCode(reqBean.getOaCode());
            if ("0".equals(oaResult.getCode())) {   //提交OA流程成功
                caravan.setProcedureStatus(1L);  //流程状态 ：已提交
            }else{
                serviceBean.setCode(310);
                serviceBean.setDesc("提交OA审批失败");
                return serviceBean;
            }

            //切回数据源
            DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_MASTER.getDataSource());//手动切换数据源
            if (StringUtil.isNotNullNorEmpty(reqBean.getId())) {  //更换、修改
                super.getHibernateTemplate().merge(caravan);
                //删除之前的物料信息
                String sql = " delete from mama100_owner.mkt_sa_caravan_cmaterial t where 1=1 and t.caravan_id = '"+reqBean.getId()+"' ";
                Query query = super.createSQLQuery(sql);
                query.executeUpdate();
            }else {
                super.save(caravan, Caravan.KEY, "MAMA100_OWNER");
            }

            if (StringUtil.isNotNullNorEmpty(reqBean.getId())) {  //更换、修改
                reqBean.setId(String.valueOf(caravan.getId()));
            }
            reqBean.setId(String.valueOf(caravan.getId()));
            saveCaravanLog(reqBean, Long.valueOf(reqBean.getStatus()));

            //删除之前的图片
            String sql = " delete from mama100_owner.mkt_sa_caravan_img_audit t where 1=1 and t.caravan_id = '"+reqBean.getId()+"' ";
            Query query = super.createSQLQuery(sql);
            query.executeUpdate();

            if (reqBean.getDeliveryMode() == 2) {  //发货方式总部添加物料
                for (CaravanCmaterialInfo caravanCmaterialInfo : reqBean.getCaravanCmaterialInfoList()) {
                    CaravanCmaterial caravanCmaterial = new CaravanCmaterial();
                    caravanCmaterial.setNum(Long.valueOf(caravanCmaterialInfo.getNum()));
                    caravanCmaterial.setCreatedBy(caravanCmaterialInfo.getCreatedBy());
                    caravanCmaterial.setBoxNorms(caravanCmaterialInfo.getBoxNorms());
                    caravanCmaterial.setArrivalGoodsSime((DateUtils.stringToDate(caravanCmaterialInfo.getArrivalGoodsSime(),"yyyy-mm-dd")));
                    caravanCmaterial.setCmaterialCode(caravanCmaterialInfo.getCmaterialCode());
                    caravanCmaterial.setCmaterialName(caravanCmaterialInfo.getCmaterialName());
                    caravanCmaterial.setCmaterialId(Long.valueOf(caravanCmaterialInfo.getCmaterialId()));
                    caravanCmaterial.setImgBig(caravanCmaterialInfo.getImgBig());
                    caravanCmaterial.setImgMin(caravanCmaterialInfo.getImgMin());
                    caravanCmaterial.setOrderType(Long.valueOf(caravanCmaterialInfo.getOrderType()));
                    caravanCmaterial.setPrice(Double.parseDouble(caravanCmaterialInfo.getPrice()));
                    caravanCmaterial.setUnit(caravanCmaterialInfo.getUnit());
                    caravanCmaterial.setNcChange(Long.valueOf(caravanCmaterialInfo.getNcChange()));
//                    CaravanCmaterial caravanCmaterial = MapperUtils.map(caravanCmaterialInfo, CaravanCmaterial.class);
                    caravanCmaterial.setCreatedTime(new Date());
                    caravanCmaterial.setCaravanId(caravan.getId());
                    caravanCmaterial.setType(caravan.getCmaterialType());
                    caravanCmaterial.setNcchangeNum(caravanCmaterial.getNcChange() * caravanCmaterial.getNum());

                    //2019-12-3
                    caravanCmaterial.setCostId(caravanCmaterialInfo.getCostId());
                    caravanCmaterial.setCostName(caravanCmaterialInfo.getCostName());
                    super.save(caravanCmaterial, CaravanCmaterial.KEY, "MAMA100_OWNER");
                }
            }


        }catch(Exception e) {
            e.printStackTrace();
            serviceBean.setCode(501);
            serviceBean.setDesc("保存失败");
        }
        return serviceBean;
    }



    /**
     * 大小篷车申请OA审批   ----暂时未用到
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean caravanOaAudit(CaravanReqBean reqBean,HttpServletRequest reqeust)  {
        ServiceBean serviceBean = new ServiceBean();
        try {
            Caravan caravan = super.getHibernateTemplate().get(Caravan.class, Long.valueOf(reqBean.getId()));
            OaResult oaResult = submitOaAutdit(reqBean, caravan);

            caravan.setOaCode(reqBean.getOaCode());
            if ("0".equals(oaResult.getCode())) {   //提交OA流程成功
                caravan.setProcedureStatus(1L);  //流程状态 ：已提交
            } else {
                caravan.setProcedureStatus(3L);  //流程状态 ：3：提交OA流程失败
                serviceBean.setCode(Integer.valueOf(oaResult.getCode()));
                serviceBean.setDesc(oaResult.getDesc());
            }
            super.getHibernateTemplate().update(caravan);
        } catch (Exception e){
            e.printStackTrace();
            serviceBean.setCode(310);
            serviceBean.setDesc("提交OA审批失败");
        }
        return serviceBean;
    }

    /**
     * 大小篷车状态更新
     *
     * @param reqBean
     * @param reqeust
     * @return
     */
    @Override
    public void editCaravanStatus(CaravanReqBean reqBean, HttpServletRequest reqeust) throws Exception {
        // 更新问题
        Caravan caravan = super.getHibernateTemplate().get(Caravan.class, Long.valueOf(reqBean.getId()));

        /**申请状态（1：新增，2：更换，3：暂停，4：恢复）*/
        /** 操作记录（1：新增，2：更换，3：暂停，4：恢复，5：更改申请人） */

        if(null != reqBean.getStatus()){
            caravan.setStatus(reqBean.getStatus());
        }
        if(null != reqBean.getProcedureStatus()){
            caravan.setProcedureStatus(reqBean.getProcedureStatus());
        }
        if (null != reqBean.getStatus() && reqBean.getStatus() == 3) {  //暂停 、恢复
            caravan.setProcedureStatus(reqBean.getStatus() + 10);
        }
        if (null != reqBean.getStatus() && reqBean.getStatus() == 4) {  //恢复变已完结
            caravan.setProcedureStatus(8L);
        }
        caravan.setUpdatedTime(new Date());
        caravan.setUpdatedBy(reqBean.getUpdatedBy());
        super.getHibernateTemplate().update(caravan);   //更新审批状态

        if(null != reqBean.getStatus()){
            if( reqBean.getStatus() == 3 || reqBean.getStatus() == 4){
                saveCaravanLog(reqBean,Long.valueOf(reqBean.getStatus()));
            }
        }
    }

    public OaResult submitOaAutdit(CaravanReqBean reqBean,Caravan caravan) throws Exception{

//        String url = "http://10.50.115.128/services/WorkflowService?wsdl";   //测试
        //调用OA
        // 1.获取工号对应的OA用户ID
        String workCode = caravan.getCreatedBy();
        Map map = queryOaAccountInfo(workCode);  //查询OA人员对应的信息

        String Date = DateUtils.getNowMonthDate("yyyyMMdd");
        // 2.设置OA主要审批流程信息
        CaravanOaReqInfo caravanOaReqInfo = new CaravanOaReqInfo();
        caravanOaReqInfo.setApplyDate(Date);  //只能是年月日
        //设置appId

        int randomNum = (int)((Math.random()*9+1)*10000);  //随机五位数
//        String applyId = "Y03-" + Date + StringUtils.leftPad(String.valueOf(randomNum), 6, "0");
        String applyId = "Y03-" + Date + String.valueOf(randomNum);
        reqBean.setOaCode(applyId);
        caravanOaReqInfo.setRequestLevel("0");
        caravanOaReqInfo.setWorkflowid(sa_oa_workflowid);   //OA定义的
        if (null != map) {
            caravanOaReqInfo.setCreatorId(String.valueOf(map.get("USERID")));
            caravanOaReqInfo.setApplyUser(String.valueOf(map.get("USERID")));
            caravanOaReqInfo.setSubcompany(String.valueOf(map.get("SUBCOMPANY"))); //大区
            caravanOaReqInfo.setDepartment(String.valueOf(map.get("DEPARTMENT"))); //办事处
        }
        caravanOaReqInfo.setApplyId(applyId); //Y03-2018010800001
        caravanOaReqInfo.setCaravanType(String.valueOf(caravan.getCaravanType()));
        caravanOaReqInfo.setStatus(String.valueOf(caravan.getStatus()));
        caravanOaReqInfo.setTerminalCode(caravan.getTerminalCode() + reqBean.getTerminalName());
        caravanOaReqInfo.setDeliveryMode(String.valueOf(caravan.getDeliveryMode()));
        caravanOaReqInfo.setPutAddress(caravan.getPutAddress());
        caravanOaReqInfo.setPutSize(caravan.getPutSize());
        caravanOaReqInfo.setCmaterialType(String.valueOf(caravan.getCmaterialType()));
        caravanOaReqInfo.setProjectCooperation(caravan.getProjectCooperation());
        caravanOaReqInfo.setCostType(String.valueOf(caravan.getCostType()));
        caravanOaReqInfo.setConsignee(caravan.getConsignee());
        caravanOaReqInfo.setContactPhone(caravan.getContactPhone());
        caravanOaReqInfo.setMemo(caravan.getMemo());

        caravanOaReqInfo.setFileUpload(reqBean.getFileUpload());  //文件上传协议
        if(caravan.getWxCustomer() != null ){
            caravanOaReqInfo.setWxCustomer(String.valueOf(caravan.getWxCustomer()));
        }
        if(caravan.getNewCustomer() != null ){
            caravanOaReqInfo.setNewCustomer(String.valueOf(caravan.getNewCustomer()));
        }
        if(caravan.getEffectiveNewCustomer() != null ){
            caravanOaReqInfo.setEffectiveNewCustomer(String.valueOf(caravan.getEffectiveNewCustomer()));
        }
        if(caravan.getProvince() != null && caravan.getCity()!=null && caravan.getDistrict()!= null && caravan.getAddress()!=null){
            caravanOaReqInfo.setAddress(caravan.getProvince() + caravan.getCity() + caravan.getDistrict() + caravan.getAddress());
        }

        //设置OA审批流程
        WorkflowBaseInfo wbi = new WorkflowBaseInfo();
        wbi.setWorkflowId(sa_oa_workflowid);
        // 设置要提交申请的信息
        WorkflowMainTableInfo wmti = returnSaMainTable(caravanOaReqInfo);
        // 设置流程请求信息
        weaver.workflow.webservices.WorkflowRequestInfo wri = new weaver.workflow.webservices.WorkflowRequestInfo();
        wri.setCreatetype("0");
        wri.setCreatorId(caravanOaReqInfo.getCreatorId());
        wri.setRequestLevel(caravanOaReqInfo.getRequestLevel());
        wri.setRequestName("Y03.大小篷车申请流程");
        wri.setWorkflowMainTableInfo(wmti);
        wri.setWorkflowBaseInfo(wbi);

        if (reqBean.getDeliveryMode() == 2) {  //发货方式总部添加物料
            //物料明细列表
            List<CaravanOaDetailReqInfo> listDetailInfo = new ArrayList<CaravanOaDetailReqInfo>();
            for (CaravanCmaterialInfo caravanCmaterialInfo : reqBean.getCaravanCmaterialInfoList()) {
                CaravanOaDetailReqInfo caravanOaDetailReqInfo = new CaravanOaDetailReqInfo();
                caravanOaDetailReqInfo.setCmaterialName(caravanCmaterialInfo.getCmaterialName());
                caravanOaDetailReqInfo.setCmaterialCode(caravanCmaterialInfo.getCmaterialCode());
                caravanOaDetailReqInfo.setNum(String.valueOf(caravanCmaterialInfo.getNum()));
                caravanOaDetailReqInfo.setPrice(String.valueOf(caravanCmaterialInfo.getPrice()));
                listDetailInfo.add(caravanOaDetailReqInfo);
            }
            wri.setWorkflowDetailTableInfos(returnSaMainDetailTable(listDetailInfo));
        }
        System.out.println("---sa_oa_workflowid--"+sa_oa_workflowid);
        System.out.println("---sa_caravan_oa--"+sa_caravan_oa);
        WorkflowServicePortType workflowServicePortType = new WorkflowService(new URL(sa_caravan_oa)).getWorkflowServiceHttpPort();
        String code = workflowServicePortType.doCreateWorkflowRequest(wri, Integer.valueOf(caravanOaReqInfo.getCreatorId()));
        OaResult oaResult = OaTranslator.translate(code);
        System.out.println("----调用OA审批返回："+code+"   "+oaResult.getDesc());
        return oaResult;

    }

    public static  WorkflowMainTableInfo returnSaMainTable(CaravanOaReqInfo caravanOaReqInfo) throws IllegalAccessException {
        Field[] fields = caravanOaReqInfo.getClass().getFields();
        List<WorkflowRequestTableField> wdList = new ArrayList<WorkflowRequestTableField>();

        for (Field field : fields) {
            WorkflowRequestTableField wd = null;
            field.setAccessible(true);
            String value = (String) field.get(caravanOaReqInfo);
            String name = field.getName();
            wd = new WorkflowRequestTableField();
            wd.setFieldName(name);//申请单号
            wd.setFieldValue(value);
            wd.setView(true);//字段是否可见
            wd.setEdit(true);//字段是否可编辑
            wdList.add(wd);
        }

        WorkflowRequestTableField wrtf =  new WorkflowRequestTableField();
        wrtf.setFieldName("合作协议");
        wrtf.setFieldType("http:");


        WorkflowRequestTableRecord wrtri = new WorkflowRequestTableRecord();  // 把fieldList封装到TableRecord对象去
        ArrayOfWorkflowRequestTableField ad = new ArrayOfWorkflowRequestTableField();

        ad.setWorkflowRequestTableField(wdList);
        wrtri.setWorkflowRequestTableFields(ad);
        List<WorkflowRequestTableRecord> wdRecordList = new ArrayList<WorkflowRequestTableRecord>();
        wdRecordList.add(wrtri);
        ArrayOfWorkflowRequestTableRecord record = new ArrayOfWorkflowRequestTableRecord();
        record.setWorkflowRequestTableRecord(wdRecordList);   // 把tableRecordList封装到Array里去
        WorkflowMainTableInfo wmi = new WorkflowMainTableInfo();
        wmi.setRequestRecords(record);    // 把tableRecordList封装到主对象表里去
        return wmi;
    }

    public static  ArrayOfWorkflowDetailTableInfo returnSaMainDetailTable(List<CaravanOaDetailReqInfo> listDetailInfo) throws IllegalAccessException {
        List<WorkflowRequestTableRecord> tableRecordList = new ArrayList<WorkflowRequestTableRecord>();
        for(CaravanOaDetailReqInfo caravanOaDetailReqInfo:listDetailInfo){
            List<WorkflowRequestTableField> tableFieldList  = new ArrayList<WorkflowRequestTableField>();
            WorkflowRequestTableRecord wrtri = new WorkflowRequestTableRecord();
            Field[] fields = caravanOaDetailReqInfo.getClass().getFields();
            for (Field field : fields) {
                WorkflowRequestTableField tableField = new WorkflowRequestTableField();;
                field.setAccessible(true);
                String value = (String) field.get(caravanOaDetailReqInfo);
                String name = field.getName();
                tableField.setFieldName(name);//申请单号
                tableField.setFieldValue(value);
                tableField.setView(true);//字段是否可见
                tableField.setEdit(true);//字段是否可编辑

                tableFieldList.add(tableField);
            }
            ArrayOfWorkflowRequestTableField arrayTableField = new ArrayOfWorkflowRequestTableField();
            arrayTableField.setWorkflowRequestTableField(tableFieldList);
            wrtri.setWorkflowRequestTableFields(arrayTableField);
            tableRecordList.add(wrtri);

        }
        ArrayOfWorkflowRequestTableRecord tableRecord = new ArrayOfWorkflowRequestTableRecord();
        tableRecord.setWorkflowRequestTableRecord(tableRecordList);
        WorkflowDetailTableInfo wdti = new WorkflowDetailTableInfo();  //一个明细表
        wdti.setWorkflowRequestTableRecords(tableRecord);
        ArrayOfWorkflowDetailTableInfo detailTableInfo = new ArrayOfWorkflowDetailTableInfo();
        List<WorkflowDetailTableInfo> tableInfoList = detailTableInfo.getWorkflowDetailTableInfo();
        tableInfoList.add(wdti);
        return detailTableInfo;
    }
    /**
     * 更新大小篷车OA流程审批状态
     *
     * @param reqBean
     * @return
     */
    @Override
    public String editCaravanOaAuditStatus(CaravanReqBean reqBean) throws Exception {

        //根据OA流程单号 查询出对应的 大小篷车流程
        String oaCode = reqBean.getOaCode();
        String caravan_hql = " from Caravan c where 1=1 and c.oaCode = :oaCode ";
        List list = this.getHibernateTemplate().findByNamedParam(caravan_hql, "oaCode", oaCode);
        Caravan caravan = (Caravan)list.get(0);

        caravan.setProcedureStatus(reqBean.getProcedureStatus());
        super.getHibernateTemplate().update(caravan);   //更新审批状态

        if(reqBean.getProcedureStatus() == 2){
            return sendOrder(caravan);
        }
        return "";

    }

    /**
     * 重新推送获取订单
     *
     * @param reqBean
     * @param reqeust
     * @return
     */
    @Override
    public String caravanOrder(CaravanReqBean reqBean, HttpServletRequest reqeust) throws Exception {
        Caravan caravan =  super.getHibernateTemplate().get(Caravan.class, Long.valueOf(reqBean.getId()));
        return sendOrder(caravan);
    }

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryInfoParamsMap(CaravanListReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //门店编码
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }

        //类型
        String caravanType = req.getCaravanType();
        if (StringUtil.isNotNullNorEmpty(caravanType)) {
            paramsMap.put("caravanType", caravanType.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        return paramsMap;
    }


    /**
     * 大小篷车列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<CaravanListResBean> querySaCaravanList(Pager<CaravanListResBean> pager, CaravanListReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("CaravanRepositoryImpl.querySaCaravanList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, CaravanListResBean.class);
    }

    /**
     * 大小篷车列表--导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaCaravanList(HttpServletResponse response, CaravanListReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("CaravanRepositoryImpl.querySaCaravanList.list", freeMakerContext);
        List<CaravanListResBean> bscarList = this.findAllForListBySql(sql,paramsMap,CaravanListResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "大小篷车管理列表";

        th = new String[]{"大区","办事处","SA编码","SA名称","SA属性","物料类型","发货方式","申请类型","申请日期","申请人工号","申请人名称","OA流程编号","微信会员数量","新客","有效新客","状态","图片一","图片二","图片三"};

        colName = new String[] {"areaName","officeName","saCode","saName","saGrad","cmaterialTypeName","deliveryModeName","caravanTypeName","createdTime",
                "createdBy","createdName","oaCode","wxCustomer","newCustomer","effectiveNewCustomer","procedureStatusName","imgUrlOne","imgUrlTwo","imgUrlThree"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    public String sendOrder(Caravan caravan) {
        String msg = "";
        if((caravan.getProcedureStatus()==2 || caravan.getProcedureStatus() == 4 || caravan.getProcedureStatus() == 5) && caravan.getDeliveryMode()==2){  //发货方式（1：当地制作：2：总部发货）
           try {
               //调用分销
               CmaterialOrderInfo cmaterialOrderInfo = new CmaterialOrderInfo();
               cmaterialOrderInfo.setCreateBy(caravan.getCreatedBy());
               cmaterialOrderInfo.setCreateById(caravan.getCreatedBy());

               cmaterialOrderInfo.setReceiveAddress(caravan.getAddress());  //详细地址 不包含省市区
               cmaterialOrderInfo.setReceiveMan(caravan.getConsignee());  //收货人
               cmaterialOrderInfo.setReceivePhone(caravan.getContactPhone());  //联系电话
               cmaterialOrderInfo.setReceiveType("0");  //默认0 收货类型
               cmaterialOrderInfo.setRemark(caravan.getMemo());  //备注
               cmaterialOrderInfo.setType("0");   //默认0 订单类型
               cmaterialOrderInfo.setCostType(2); //默认TPM费用类型
//               cmaterialOrderInfo.setBudgetSubjectId(String.valueOf(caravan.getCostType()));    /** 费用类型（1：总部承担：2：办事处/大区承担） 2019-12-3 */
               //下单物料信息
               List<CmaterialOrderItemInfo> cmaterialOrderItemInfoList = new ArrayList<CmaterialOrderItemInfo>();

               //统计物料总价格
               Double price = 0.00;
               //查询对应的物料信息
               String cmaterial_hql = " from CaravanCmaterial c where 1=1 and c.caravanId = :caravanId ";
               List<CaravanCmaterial> caravanCmaterialList = (List<CaravanCmaterial>) this.getHibernateTemplate().findByNamedParam(cmaterial_hql, "caravanId", caravan.getId());
               for (CaravanCmaterial ccBean : caravanCmaterialList) {
                   CmaterialOrderItemInfo cmaterialOrderItemInfo = new CmaterialOrderItemInfo();
                   if (null != ccBean.getArrivalGoodsSime()) {
                       cmaterialOrderItemInfo.setArrivalGoodsTime(DateUtils.dateToString(ccBean.getArrivalGoodsSime(), "yyyy-MM-dd"));   //预计发货时间
                   }
                   cmaterialOrderItemInfo.setCmaterialId(String.valueOf(ccBean.getCmaterialId())); //物料ID
                   cmaterialOrderItemInfo.setCmaterialName(ccBean.getCmaterialName()); //物料名称
                   cmaterialOrderItemInfo.setCmaterialNcNum(String.valueOf(ccBean.getNcchangeNum())); //NC转换数量
                   cmaterialOrderItemInfo.setCmaterialUnit(ccBean.getUnit()); //单位
                   cmaterialOrderItemInfo.setNcChange(String.valueOf(ccBean.getNcChange())); //NC转化率
                   cmaterialOrderItemInfo.setPrice(String.valueOf(ccBean.getPrice()));   //价格
                   cmaterialOrderItemInfo.setBoxNorms(ccBean.getBoxNorms());  //装箱规格
                   cmaterialOrderItemInfo.setgNum(String.valueOf(ccBean.getNum()));
                   cmaterialOrderItemInfo.setUpdateBy(caravan.getCreatedBy());  //下单人工号
                   cmaterialOrderItemInfo.setUpdateTime(new Date());

                   cmaterialOrderItemInfo.setOrderType(String.valueOf(ccBean.getOrderType()));
                   cmaterialOrderItemInfo.setCmaterialPicture(ccBean.getImgBig());
                   cmaterialOrderItemInfo.setCostId(ccBean.getCostId());
                   cmaterialOrderItemInfoList.add(cmaterialOrderItemInfo);
                   Double n = ccBean.getPrice().doubleValue()*ccBean.getNum().doubleValue();
                   price = price.doubleValue() + n.doubleValue();
               }
               cmaterialOrderInfo.setCmaterialOrderItemInfoList(cmaterialOrderItemInfoList);

               cmaterialOrderInfo.setPayPrice(new BigDecimal(price.floatValue()));   //整单物料总价  自己算

               CmaterialOrderAddr orderAddr = new CmaterialOrderAddr();
               orderAddr.setAddr(caravan.getAddress());
               orderAddr.setProvince(caravan.getProvince());
               orderAddr.setCity(caravan.getCity());
               orderAddr.setDistrict(caravan.getDistrict());
               orderAddr.setStatus(1);  //默认写死1
               orderAddr.setCreateTime(new Date());
               orderAddr.setCreateBy(caravan.getCreatedBy());
               orderAddr.setMobile(caravan.getContactPhone()); //收货人手机


               SaCmaterialOrderRequest sa = new SaCmaterialOrderRequest();
               sa.setSign(EncodeDecodeUtil.encodeStr(MD5.md5("BIOSTIMEbmu6U6Gw" + DateUtils.dateToString(new Date(), "yyyyMMdd")) + "createCmaterialOrder", "des"));
               sa.setPlatform("33");
               sa.setOrderInfo(cmaterialOrderInfo);
               sa.setOrderAddr(orderAddr);

               String jsonParams = JsonUtils.toJson(sa);
               logger.info("jsonParams=  " + jsonParams);

               System.out.println("---sa_caravan_dealer--"+sa_caravan_dealer);
               String result = HttpUtils.httpPostWithJSON(sa_caravan_dealer, jsonParams);

               //如果Code 100 说明成功
               Map resultMap = JsonUtils.json2GenericObject(result, new TypeReference<Map>() {
               });
               String code = String.valueOf(resultMap.get("resultCode"));
               String desc = String.valueOf(resultMap.get("resultMessage"));
               logger.info("--分销返回--"+code+"----desc"+desc);

               if ("200".equals(code)) {
                   caravan.setProcedureStatus(6L); //待发货
                   caravan.setOrderId(desc);  //调用分销发货接口成功后，把返回的订单编号更新到对应的大小篷车申请信息中。
                   super.getHibernateTemplate().update(caravan);   //更新状态
               } else {
                   msg = "提交订单失败,请查看TPM费用是否不足。";
                   caravan.setProcedureStatus(5L);//:提交订单失败
                   super.getHibernateTemplate().update(caravan);   //更新状态
               }
           }catch (Exception e){
               e.printStackTrace();
               msg = "提交订单失败";
               caravan.setProcedureStatus(5L);//:提交订单失败
               super.getHibernateTemplate().update(caravan);   //更新状态
           }
        }else{
            //当地制作
            caravan.setProcedureStatus(7L);
            super.getHibernateTemplate().update(caravan);   //更新状态
        }

        return msg;
    }
    /**
     * 大小篷车上传竣工图片及审核
     *
     * @param reqBean
     * @param reqeust
     * @return
     */
    @Override
    public void saveCaravanImgAudit(CaravanImgAuditReqBean reqBean, HttpServletRequest reqeust) throws Exception {
        CaravanImgAudit caravanImgAudit = new CaravanImgAudit();
        if(null != reqBean.getImgAuditId()){
            caravanImgAudit =  super.getHibernateTemplate().get(CaravanImgAudit.class, reqBean.getImgAuditId());
            caravanImgAudit.setAuditStatus(reqBean.getAuditStatus());
            caravanImgAudit.setMemo(reqBean.getMemo());
            caravanImgAudit.setUpdatedTime(new Date());
            caravanImgAudit.setUpdatedBy(reqBean.getCreatedBy());
            super.getHibernateTemplate().update(caravanImgAudit);

            //更新状态
            Caravan caravan = super.getHibernateTemplate().get(Caravan.class, Long.valueOf(caravanImgAudit.getCaravanId()));
            if(1 == reqBean.getAuditStatus()){  //审核通过
                caravan.setProcedureStatus(8L);  //8：已完结，
            }else{ //审核不通过
                caravan.setProcedureStatus(9L); // 9：验收不通过
            }
            super.getHibernateTemplate().update(caravan);   //更新审批状态
        }else{

            //重新上传 删除之前的图片
            String sql = " delete from mama100_owner.mkt_sa_caravan_img_audit t where 1=1 and t.caravan_id = '"+reqBean.getCaravanId()+"' ";
            Query query = super.createSQLQuery(sql);
            query.executeUpdate();

            caravanImgAudit.setCaravanId(reqBean.getCaravanId());
            caravanImgAudit.setCreatedBy(reqBean.getCreatedBy());
            caravanImgAudit.setCreatedName(reqBean.getCreatedName());
            caravanImgAudit.setImgUrlOne(reqBean.getImgUrlOne());
            caravanImgAudit.setImgUrlTwo(reqBean.getImgUrlTwo());
            caravanImgAudit.setImgUrlThree(reqBean.getImgUrlThree());
            caravanImgAudit.setCreatedTime(new Date());
            super.save(caravanImgAudit, CaravanImgAudit.KEY, "MAMA100_OWNER");

            Caravan caravan = super.getHibernateTemplate().get(Caravan.class, Long.valueOf(reqBean.getCaravanId()));
            caravan.setProcedureStatus(7L);  //8：已完结，
            super.getHibernateTemplate().update(caravan);   //更新审批状态

        }
    }


    /**
     * 查询OA人员对应的信息
     * @param workCode
     * @return
     */
    public Map queryOaAccountInfo(String workCode){
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        String sql ="select\n" +
                        "  t.id userId,\n" +
                        "  t.subcompanyid1 subcompany,\n" +
                        "  t.departmentid department\n" +
                        "from oaadmin.hrmresource@oa_dblink t\n" +
                        "where t.workcode = :workCode";
        Query query = super.createSQLQuery(sql);
        query.setParameter("workCode", workCode.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map =  (Map)query.uniqueResult();
        return map;
    }


    /**
     * SA门店信息
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean querySaTerminalInfo(CaravanReqBean reqBean) {

        ServiceBean serviceBean = new ServiceBean();
        Map map = queryTerminalInfo(reqBean);
        if(null == map){
            serviceBean.setCode(110);
            serviceBean.setDesc("所填写门店编码不存在或状态不正确，请重新输入");
            return serviceBean;
        }
        String  statusCode= (String)map.get("STATUSCODE");
        if(!"01".equals(statusCode)){
            serviceBean.setCode(111);
            serviceBean.setDesc("所填写终端不存在或状态不正确，请重新输入");
            return serviceBean;
        }
        String  areaOfficeCode= (String)map.get("DEPARTMENTCODE");
        System.out.println("门店大区办事处"+areaOfficeCode+";登陆人大区办事处"+reqBean.getAreaOfficeCode());
        int  parentId= ((BigDecimal)map.get("PARENTID")).intValue();
        int  cId2= ((BigDecimal)map.get("CID2")).intValue();
        if(StringUtil.isNotNullNorEmpty(reqBean.getAreaOfficeCode())) {
            if (!(parentId == cId2 || areaOfficeCode.equals(reqBean.getAreaOfficeCode()))) {
                serviceBean.setCode(112);
                serviceBean.setDesc("所填写门店编码不在权限范围内，请重新输入");
                return serviceBean;
            }
        }
        String  channalCode= (String)map.get("CHANNELCODE");
        if(!"08".equals(channalCode)){
            serviceBean.setCode(113);
            serviceBean.setDesc("所填写终端为非SA渠道，请重新输入");
            return serviceBean;
        }

        serviceBean.setResponse(map.get("TERMINALNAME"));
        return serviceBean;
    }

    /**
     * 查询大小篷车信息
     *
     * @param reqBean
     * @return
     */
    @Override
    public CaravanReqBean queryCaravanInfo(CaravanReqBean reqBean) {

        Caravan caravan = super.getHibernateTemplate().get(Caravan.class, Long.valueOf(reqBean.getId()));
        CaravanReqBean caravanReqBean = MapperUtils.map(caravan,CaravanReqBean.class);
        String caravan_hql = " from CaravanCmaterial c where 1=1 and c.caravanId = :caravanId ";
        List<CaravanCmaterial> list = (List<CaravanCmaterial>) this.getHibernateTemplate().findByNamedParam(caravan_hql, "caravanId", Long.valueOf(reqBean.getId()));
        List<CaravanCmaterialInfo> caravanCmaterialInfoList = new ArrayList<CaravanCmaterialInfo>();
        for(CaravanCmaterial cc:list){
            CaravanCmaterialInfo ccInfo = new CaravanCmaterialInfo();
            ccInfo = MapperUtils.map(cc,CaravanCmaterialInfo.class);
            caravanCmaterialInfoList.add(ccInfo);
        }
        caravanReqBean.setCaravanCmaterialInfoList(caravanCmaterialInfoList);
        return caravanReqBean;
    }

    /**
     * 查询可更新的人员列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<String> queryUpdateByList(CaravanListReqBean reqBean) {

        String sql =
                "select ca.code||'/'||ca.name\n" +
                        "  FROM bst_mdm.common_account ca,\n" +
                        "       bst_mdm.common_account_list cal,\n" +
                        "       mama100_owner.common_department_terminal dt\n" +
                        "  where 1=1\n" +
                        "  and ca.status = '01'\n" +
                        "  and ca.source_id = '01'\n" +
                        "  and ca.id =  cal.account_id\n" +
                        "  and (cal.end_date>=sysdate or cal.end_date is null)\n" +
                        "  and cal.last_flag = 'Y'\n" +
                        "  and dt.department_code = cal.department_code\n" +
                        "  and dt.terminal_code = :saCode\n" ;

        Query query = super.createSQLQuery(sql);
        query.setParameter("saCode", String.valueOf(reqBean.getSaCode()));
        query.setResultTransformer(Transformers.TO_LIST);

        List<String> updateByList = query.list();
        return updateByList;
    }

    /**
     * 更新申请人
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean editUpdateBy(CaravanReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            Caravan caravan = super.getHibernateTemplate().get(Caravan.class, Long.valueOf(reqBean.getId()));
            caravan.setUpdatedBy(reqBean.getUpdatedBy());
            caravan.setCreatedBy(reqBean.getCreatedBy());
            caravan.setCreatedName(reqBean.getCreatedName());
            caravan.setUpdatedTime(new Date());

            super.getHibernateTemplate().update(caravan);   //更新审批状态
            String[] updateInfo = reqBean.getUpdatedBy().split("&");
            reqBean.setCreatedBy(updateInfo[0]);
            if(updateInfo.length > 1 ){
                reqBean.setCreatedName(updateInfo[1]);
            }else{
                reqBean.setCreatedName("");
            }
            saveCaravanLog(reqBean,5L);
        }catch (Exception e){
            e.printStackTrace();
            serviceBean.setCode(200);
            serviceBean.setDesc("更新申请人失败");
        }
        return serviceBean;
    }


    /**
     * 查询大小篷车操作日志
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<CaravanLogInfo> queryCaravanLogList(CaravanReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("caravanId", String.valueOf(reqBean.getId()));

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("CaravanRepositoryImpl.queryCaravanLogList", freeMakerContext);
        System.out.println("----CaravanRepositoryImpl.queryCaravanLogList SQL----"+sql.toString());
        List<CaravanLogInfo> caravanLogInfoList = this.findAllForListBySql(sql, paramsMap,CaravanLogInfo.class);
        return caravanLogInfoList;
    }

    /**
     * 查询大小篷车操作日志
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<CaravanTpmCostInfo> queryTpmCostList(CaravanTpmCostInfo reqBean) {
        List<CaravanTpmCostInfo> ctcInfoList = new ArrayList<>();
        try {

            String price = reqBean.getPrice();
            String nums = reqBean.getNums();
            String userAccount = reqBean.getUserAccount();
            String amount = String.valueOf(Double.valueOf(price)*Double.valueOf(nums));

            CaravanTpmQueryBean ctqBean = new CaravanTpmQueryBean();
            ctqBean.setActivityBrief(reqBean.getActivityBrief());
            ctqBean.setActivityCode(reqBean.getActivityCode());
            ctqBean.setAmount(amount);
            ctqBean.setUserAccount(userAccount);

            System.out.println("--TPM接口URL--" + sa_caravan_dealer_tpm);
            String jsonParams = JsonUtils.toJson(ctqBean);
            logger.info("--TPM接口URL--jsonParams=  " + jsonParams);

            System.out.println("---sa_caravan_dealer--"+sa_caravan_dealer_tpm);
            String result = HttpUtils.httpPostWithJSON(sa_caravan_dealer_tpm, jsonParams);

            //如果Code 100 说明成功
            List<HashMap<String, Object>> list = JsonUtils.json2List(result);

            for(HashMap<String, Object> hashMap:list){
                if(null != hashMap.get("ID")) {
                    CaravanTpmCostInfo caravanTpmCostInfo = new CaravanTpmCostInfo();
                    caravanTpmCostInfo.setId((String.valueOf((Integer) hashMap.get("ID"))));
                    caravanTpmCostInfo.setActivityCode((String) hashMap.get("ZUBIANHAO"));
                    caravanTpmCostInfo.setActivityBrief((String) hashMap.get("SHUOMING"));
                    caravanTpmCostInfo.setAmount((String.valueOf((Integer) hashMap.get("AMOUNT"))));
                    caravanTpmCostInfo.setActStartDate((String) hashMap.get("START_DATE"));
                    caravanTpmCostInfo.setActEndDate((String) hashMap.get("END_DATE"));
                    caravanTpmCostInfo.setAmountAvailable((String.valueOf(hashMap.get("AMOUNT_AVAILABLE"))));
                    ctcInfoList.add(caravanTpmCostInfo);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return ctcInfoList;
    }


    /**
     * 保存大小篷车操作日志
     *
     * @param reqBean
     * @return
     */
    public void saveCaravanLog(CaravanReqBean reqBean,Long type) {
        try {
            CaravanLog caravanLog = new CaravanLog();
            caravanLog.setType(type);
            caravanLog.setCreatedTime(new Date());
            caravanLog.setCaravanId(Long.valueOf(reqBean.getId()));
            caravanLog.setCreatedBy(reqBean.getCreatedBy());
            caravanLog.setCreatedName(reqBean.getCreatedName());
            super.save(caravanLog, CaravanLog.KEY, "MAMA100_OWNER");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * 查询门店信息
     *
     * @param reqBean
     * @return
     */
    public Map queryTerminalInfo(CaravanReqBean reqBean) {
        String areaOfficeCode = reqBean.getAreaOfficeCode().trim();
        StringBuffer sql = new StringBuffer();

                sql.append("select dt.department_code  departmentCode,\n" +
                "           t.terminal_channel_code  channelCode,\n" +
                "           max(to_char(t.short_name))  terminalName,\n" +
                "           max(t.status_code)  statusCode ,\n" +
                "           max(cd.parent_id) parentId,\n" +
                "           max(cd2.id) cId2\n" +
                "      from mama100_owner.crm_terminal               t,\n" +
                "           mama100_owner.common_department_terminal dt,\n" +
                "           mama100_owner.common_department cd,\n" +
                "           mama100_owner.common_department cd2\n" +
                "     where 1 = 1\n" +
                "       and dt.department_code = cd.code(+)\n" +
                "       and t.code = dt.terminal_code(+)\n" +
                "       and t.code = :terminalCode");
        if(StringUtil.isNotNullNorEmpty(reqBean.getAreaOfficeCode())) {
            sql.append(" and cd2.code = :areaOfficeCode ");
        }
        sql.append("  group by  dt.department_code, t.terminal_channel_code ");
        Query query = super.createSQLQuery(sql.toString());
        query.setParameter("terminalCode", reqBean.getTerminalCode().trim());
        if(StringUtil.isNotNullNorEmpty(reqBean.getAreaOfficeCode())) {
            query.setParameter("areaOfficeCode", areaOfficeCode);
        }
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map =  (Map)query.uniqueResult();
        return map;
    }

}
