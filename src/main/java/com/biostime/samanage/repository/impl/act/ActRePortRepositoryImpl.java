package com.biostime.samanage.repository.impl.act;

import com.alibaba.fastjson.JSON;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.general.CollectionUtil;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.actreport.*;
import com.biostime.samanage.bean.actroadshow.ActReportDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowListReqBean;
import com.biostime.samanage.domain.act.*;
import com.biostime.samanage.domain.ftf.FtfActReportAccount;
import com.biostime.samanage.repository.act.ActRePortRepository;
import com.biostime.samanage.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import net.sf.json.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Describe:活动上报
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Service("actRePortRepository")
public class ActRePortRepositoryImpl extends BaseOracleRepositoryImpl implements ActRePortRepository {

    @Value( "${mama100.ht.wx}" )
    private String mama100_ht;

    @Value( "${mama100.dodie.wx}" )
    private String mama100_dodie;

    @Value( "${mama100.act.mini.code}" )
    private String mama100_act_mini_code_url;

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * 获取上报的活动
     * @param activityReqBean
     * @return
     */
    @Override
    public List<ActRePortListResBean> queryActRePortList(ActRePortListReqBean activityReqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<ActRePortListResBean> activityResBean = new ArrayList<>();
        try {
            //类型名称
            String actConfigId = activityReqBean.getType();
            if (StringUtil.isNotNullNorEmpty(actConfigId)) {
                paramsMap.put("actConfigId", actConfigId);
            }
            //办事处编码
            String officeCode = activityReqBean.getOfficeCode();
            if (StringUtil.isNotNullNorEmpty(officeCode)) {
                //根本编码获取大区办事处ID
                Map map = getDepartmentByCode(officeCode);
                if(null != map){
                    int tier = ((BigDecimal)map.get("TIER")).intValue();
                    String areaId = String.valueOf(((BigDecimal)map.get("AREAID")).intValue());
                    String officeId = String.valueOf(((BigDecimal)map.get("OFFICEID")).intValue());
                    //层级3大区
                    if(3 == tier){
                        paramsMap.put("areaCode",areaId);
                    }
                    //层级4大区
                    if(4 == tier){
                        paramsMap.put("areaCode",officeId);
                        paramsMap.put("officeCode",areaId);
                    }

                }
            }

            //渠道
            String channelCode = activityReqBean.getChannelCode();
            if (StringUtil.isNotNullNorEmpty(channelCode)) {
                paramsMap.put("channelCode",channelCode );
            }

            //上报
            String backUpType = activityReqBean.getBackUpType();   //1：上报  2：补录
            if (StringUtil.isNotNullNorEmpty(backUpType)) {
                paramsMap.put("backUpType",backUpType );
            }

            //门店编码
            String terminalCode = activityReqBean.getTerminalCode();
           /* if("01".equals(channelCode)) {  //婴线 效验这个门店
                if (StringUtil.isNotNullNorEmpty(terminalCode)) {
                    paramsMap.put("terminalCode", terminalCode);
                }
            }*/

            //活动开始日期
            String startDate = activityReqBean.getStartDate();
            if (StringUtil.isNotNullNorEmpty(startDate)) {
                startDate = DateUtils.dateToString(DateUtils.stringToDate(startDate,"yyyy-MM-dd"),"yyyy-MM-dd");
                paramsMap.put("startDate", startDate.trim());
            }

            //活动结束日期
            String endDate = activityReqBean.getEndDate();
            if (StringUtil.isNotNullNorEmpty(endDate)) {
                endDate = DateUtils.dateToString(DateUtils.stringToDate(endDate,"yyyy-MM-dd"),"yyyy-MM-dd");
                paramsMap.put("endDate", endDate.trim());
            }

            //事业部编码
            String buCode = activityReqBean.getBuCode();
            if (StringUtil.isNotNullNorEmpty(buCode)) {
                paramsMap.put("buCode", buCode.trim());
            }


            //查询门店所属连锁分类
            String terminalType =  queryChainTerminal(terminalCode);

            paramsMap.put("terminalType", terminalType.trim());

            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("ActRePortRepositoryImpl.queryActRePortList", freeMakerContext);

            System.out.println("----ActRePortRepositoryImpl.queryActRePortList--"+JsonUtils.toJson(paramsMap));
            System.out.println("----ActRePortRepositoryImpl.queryActRePortList SQL----"+sql.toString());
            activityResBean = this.findAllForListBySql(sql, paramsMap,ActRePortListResBean.class);

            for(ActRePortListResBean bean :activityResBean){
                bean.setListBrand(queryActRePortBrandList(Long.valueOf(bean.getCode())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return activityResBean;
    }

    /**
     * 根本编码获取大区办事处ID
     * @param code
     * @return
     */
    public Map getDepartmentByCode(String code){
        String sql = " select cd.id areaId,cd.parent_id officeId,cd.tier from mama100_owner.common_department cd " +
                " where 1=1 " +
                " and cd.code = :code";
        Query query = super.createSQLQuery(sql);
        query.setParameter("code", code.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        return (Map)query.uniqueResult();

    }
    /**
     * 查询
     * @param actId
     * @return
     */
    public List<BaseKeyValueBean> queryActRePortBrandList(Long actId) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("actId", actId);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("ActRePortRepositoryImpl.queryActRePortBrandList", freeMakerContext);

        System.out.println("----ActRePortRepositoryImpl.queryActRePortBrandList SQL----"+sql.toString());
        List<BaseKeyValueBean> listBrand = this.findAllForListBySql(sql, paramsMap,BaseKeyValueBean.class);

        return listBrand;
    }
    /**
     * 提交活动上报
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean submitActRePort(ActRePortReqBean reqBean) {
        ServiceBean response = new ServiceBean();
        try {
            ActRePortDetail actRePortDetail = new ActRePortDetail();
            String terminalCode = reqBean.getTerminalCode();

            actRePortDetail.setActId(reqBean.getCode());
            actRePortDetail.setActMemo(reqBean.getMemo());
            actRePortDetail.setTerminalCode(terminalCode);
            actRePortDetail.setDeviceId(reqBean.getDeviceId());
            actRePortDetail.setCreatedTime(new Date());
            actRePortDetail.setType(reqBean.getMethodType());
            actRePortDetail.setCreatedBy(reqBean.getUserId());
            actRePortDetail.setCreatedName(reqBean.getCreatedName());

            //正常上报的BNC活动精英
            if("1".equals(reqBean.getBncElites()) && "1".equals(reqBean.getBackUpType())){

                //2020-02 需求 BNC活动精英
                //1、上报时间段的范围
                //2020-04 需求取消
               /* if(queryNowTimeIsreportTime()){
                    response.setCode(505);
                    response.setDesc("当前时间不允许上报");
                    return response;
                }*/

                //2、BNC活动精英所属的品牌
                //3、不同的品牌上报次数不一样
                if(queryActReportTimes(reqBean.getUserId())){
                    response.setCode(506);
                    response.setDesc("当天上报次数已达到上限");
                    return response;
                }
            }

            //补录信息
            //MethodType1：路演活动现场、2：SA上报 3、开班培训  BackUpType1：上报  2：补录
            if("1".equals(reqBean.getMethodType()) && "2".equals(reqBean.getBackUpType())) {
                actRePortDetail.setActMakeup(1);
                actRePortDetail.setMakeupMemo(Integer.valueOf(reqBean.getMakeupMemo()));
                actRePortDetail.setMakeupMemoOther(reqBean.getMakeupMemoOther());
            }else{
                actRePortDetail.setActMakeup(0);
            }

            int i = 0;
            String[] files = reqBean.getFiles();
            for (String imgPath : files) {
                i++;

                if("1".equals(reqBean.getMethodType())){ //路演活动上报
                    if (i == 1) {
                        actRePortDetail.setImagePath1(imgPath.split("&")[0]);
                        actRePortDetail.setImageMark1(imgPath.split("&")[1]);
                    } else if (i == 2) {
                        actRePortDetail.setImagePath2(imgPath.split("&")[0]);
                        actRePortDetail.setImageMark2(imgPath.split("&")[1]);
                    } else if (i == 3) {
                        actRePortDetail.setImagePath3(imgPath.split("&")[0]);
                        actRePortDetail.setImageMark3(imgPath.split("&")[1]);
                    } else if (i == 4) {
                        actRePortDetail.setImagePath4(imgPath.split("&")[0]);
                        actRePortDetail.setImageMark4(imgPath.split("&")[1]);
                    } else if (i == 5) {
                        actRePortDetail.setImagePath5(imgPath.split("&")[0]);
                        actRePortDetail.setImageMark5(imgPath.split("&")[1]);
                    } else if (i == 6) {
                        actRePortDetail.setImagePath6(imgPath.split("&")[0]);
                        actRePortDetail.setImageMark6(imgPath.split("&")[1]);
                    }
                }else{
                    if (i == 1) {
                        actRePortDetail.setImagePath1(imgPath);
                    } else if (i == 2) {
                        actRePortDetail.setImagePath2(imgPath);
                    } else if (i == 3) {
                        actRePortDetail.setImagePath3(imgPath);
                    } else if (i == 4) {
                        actRePortDetail.setImagePath4(imgPath);
                    } else if (i == 5) {
                        actRePortDetail.setImagePath5(imgPath);
                    } else if (i == 6) {
                        actRePortDetail.setImagePath6(imgPath);
                    }
                }
            }


            logger.info(this.getClass()+".submitActRePort.actRePortDetail{}", JSON.toJSON(actRePortDetail));

            //路演活动 V1.9 2018-03 所选活动的考核类型为“路演活动” 考核类型1:路演活动，2：物料陈列
            if("1".equals(reqBean.getMethodType()) && "1".equals(reqBean.getKhType())){

                //上报类型为路演活动 关联SA活动
                if(StringUtil.isNotNullNorEmpty(reqBean.getSaFtfActId())){
                    actRePortDetail.setSaFtfActId(Long.valueOf(reqBean.getSaFtfActId()));
                }
                String msg = checkReportDate(reqBean);
                if(StringUtil.isNotNullNorEmpty(msg)){
                    response.setCode(501);
                    response.setDesc(msg);
                }else{
                    //4）填写的报备日期不可有交叉，当同一个活动ID同一个门店报备时出现日期交叉需弹出提示【您的活动与已报备时间有交叉，是否覆盖？】
                    String actId = reqBean.getCode();
                    String flag = reqBean.getFlag();
                    String startDate = reqBean.getStartDate();
                    String endDate = reqBean.getEndDate();
                    Date sDate = DateUtils.stringToDate(startDate, "yyyy-MM-dd");
                    Date eDate = DateUtils.stringToDate(endDate, "yyyy-MM-dd");
                    HashMap<String, Object> paramsMap = new HashMap<String, Object>();
                    paramsMap.put("terminalCode", terminalCode);
                    paramsMap.put("actId", actId);
                    paramsMap.put("startDate", startDate);
                    paramsMap.put("endDate", endDate);
                    FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
                    String sql = SqlUtil.get("ActRePortRepositoryImpl.checkReportDate", freeMakerContext);
                    System.out.println("----ActRePortRepositoryImpl.checkReportDate SQL----" + sql.toString());
                    List<ActReportDate> actReportDateBean = this.findAllForListBySql(sql, paramsMap, ActReportDate.class);
                    if (actReportDateBean.size() > 0) {
                        if (actReportDateBean.size() == 1) {
                            //1:确定上报，2：取消不做任何操作
                            if ("1".equals(flag)) {
                                super.save(actRePortDetail, ActRePortDetail.KEY, "MAMA100_OWNER");
                                Long actReportDateId = Long.valueOf(actReportDateBean.get(0).getId());
                                ActReportDate ardBean = super.getHibernateTemplate().get(ActReportDate.class,actReportDateId);
                                ardBean.setStartDate(sDate);
                                ardBean.setEndDate(eDate);
                                ardBean.setActDetailId(Long.valueOf(actRePortDetail.getId()));
                                super.getHibernateTemplate().update(ardBean);
                            } else {
                                //这个编码不能变 前端有拿做判断的
                                response.setCode(666);
                                response.setDesc("该活动已有上报记录，是否再次上报？");
                                return response;
                            }
                        } else {
                            response.setCode(501);
                            response.setDesc("您的活动时间已上报，请重新填写");
                            return response;
                        }
                    }else {
                        super.save(actRePortDetail, ActRePortDetail.KEY, "MAMA100_OWNER");
                        //添加门店报备日期
                        ActReportDate actReportDate = new ActReportDate();
                        actReportDate.setActDetailId(Long.valueOf(actRePortDetail.getId()));
                        actReportDate.setStartDate(DateUtils.stringToDate(reqBean.getStartDate(), "yyyy-MM-dd"));
                        actReportDate.setEndDate(DateUtils.stringToDate(reqBean.getEndDate(), "yyyy-MM-dd"));
                        super.add(actReportDate, ActReportDate.KEY, "MAMA100_OWNER");
                    }
                }
            }else{

                super.save(actRePortDetail, ActRePortDetail.KEY, "MAMA100_OWNER");
            }

            //路演活动品牌
            if("1".equals(reqBean.getMethodType())) {

                String[] brandCodes = reqBean.getBrandCode();
                if(StringUtil.isNotNullNorEmpty(brandCodes)) {
                    if (brandCodes.length != 0) {
                        //先删除  在更新
                        String hql = " delete from ActRoadShowBrand arss where 1=1 and arss.actRoadId = :actShowId ";
                        Query query = super.createQuery(hql);
                        query.setParameter("actShowId", actRePortDetail.getId());
                        query.executeUpdate();

                        List<ActRoadShowBrand> actRoadShowBrandList = new ArrayList<ActRoadShowBrand>();
                        List<String> brandCodeList = Arrays.asList(brandCodes);
                        for (String brandCode : brandCodeList) {
                            ActRoadShowBrand actRoadShowBrand = new ActRoadShowBrand();
                            actRoadShowBrand.setActRoadId(actRePortDetail.getId());
                            actRoadShowBrand.setCode(Long.valueOf(brandCode));
                            actRoadShowBrand.setType(2L);
                            actRoadShowBrandList.add(actRoadShowBrand);
                        }
                        super.batchAdd(actRoadShowBrandList, ActRoadShowBrand.KEY, "MAMA100_OWNER");
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            response.setCode(500);
            response.setDesc(PropUtils.getProp(response.getCode()));
        }
        return response;
    }

    /**
     * 查询当前时间是否允许上报
     * @return
     */
    public boolean queryNowTimeIsreportTime(){
        //1、上报时间段的范围
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int hours = cal.getTime().getHours();

        //时间段的开始时间
        int start[] = {8,13,18};
        //时间段的结束时间
        int end[] = {10,15,20};
        boolean flag = true;
        for(int i = 0; i< start.length;i++){
            if(hours >= start[i] && hours < end[i]){
                flag = false;
            }
        }
        return flag;
    }

    /**
     * 查询上报次数
     * @return
     */
    public boolean queryActReportTimes(String createdBy){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        if (StringUtil.isNotNullNorEmpty(createdBy)) {
            paramsMap.put("createdBy", createdBy.trim());
        }
        FreeMakerContext freeMakerContext2 = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("ActRePortRepositoryImpl.queryActElitesReportTimes", freeMakerContext2);
        BaseKeyValueBean bvBean = this.findSingleBySql(sql,paramsMap,BaseKeyValueBean.class);
        boolean flag = true;

        //3个不同的品牌key，value对应 的是上报的次数
        Map<String,String> hashMap = new HashMap<>();
        hashMap.put("1","2");  //BNC合生元
        hashMap.put("33206","2"); //DODIE
        hashMap.put("21706","2"); //奶创

        if(null == bvBean ){
            flag = false;
        }else{
            int i = Integer.parseInt(hashMap.get(bvBean.getKey()));
            if(Integer.parseInt(bvBean.getValue()) < i){
                flag = false;
            }
        }
        return flag;
    }
    public String checkReportDate(ActRePortReqBean reqBean){
        try {
            /*
            2）报备的开始日期与结束日期必须在活动的开始与结束日期范围内，其中报备的开始日期必须大于等于活动的开始日期，报备的结束日期必须小于等于活动的结束日期；
            3）报备的开始日期与结束日期必须包括当天服务器日期；
            */
            String startDate = reqBean.getStartDate();
            String endDate = reqBean.getEndDate();
            //时间验证
            String dateMatches = "\\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[0-1])";
            //活动备案开始日期
            if (StringUtil.isNullOrEmpty(startDate)) {
                return "执行开始日期不能为空";
            } else if (!startDate.matches(dateMatches)) {
                return "不是有效的执行开始日期";
            }

            //活动备案开始日期
            if (StringUtil.isNullOrEmpty(endDate)) {
                return "执行结束日期不能为空";
            } else if (!endDate.matches(dateMatches)) {
                return "执行结束日期格式不合法";
            }

            Date sDate = DateUtils.stringToDate(startDate, "yyyy-MM-dd");
            Date eDate = DateUtils.stringToDate(endDate, "yyyy-MM-dd");
            //校验开始时间与结束时间，开始时间需小于结束时间
            if (null != sDate && eDate.before(sDate)) {
                return "执行开始日期不能大于执行结束日期";
            }


            if (StringUtil.isNotNullNorEmpty(reqBean.getCode())) {
                ActRoadShow arsBean = super.getHibernateTemplate().get(ActRoadShow.class, Long.valueOf(reqBean.getCode()));
                String asDate = DateUtils.dateToString(arsBean.getStartTime(), "yyyy-MM-dd");
                String aeDate = DateUtils.dateToString(arsBean.getEndTime(), "yyyy-MM-dd");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date actStartDate = sdf.parse(asDate);
                Date actEndDate = sdf.parse(aeDate);

                if (!((actStartDate.before(sDate) || actStartDate.equals(sDate)) &&
                        (actEndDate.after(sDate) || actEndDate.equals(sDate)))) {
                    return "执行时间不在活动日期范围内(" + asDate + "至" + aeDate + ")";
                }

                if (!((actStartDate.before(eDate) || actStartDate.equals(eDate)) &&
                        (actEndDate.after(eDate) || actEndDate.equals(eDate)))) {
                    return "执行时间不在活动日期范围内(" + asDate + "至" + aeDate + ")";
                }

                if("1".equals(reqBean.getMethodType()) && "1".equals(reqBean.getBackUpType())) { //路演活动上报 补录
                    //报备的开始日期与结束日期必须包括当天服务器日期；
                    try {
                        Date nowDate = sdf.parse(sdf.format(new Date()));
                        if (!((sDate.before(nowDate) || sDate.equals(nowDate)) &&
                                (eDate.after(nowDate) || eDate.equals(nowDate)))) {
                            return "执行时间必须包含当天";
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                        return "执行日期格式转换出错";
                    }
                }
            } else {
                return "请选择活动";
            }
        }catch (Exception e){
            e.printStackTrace();
            return "时间格式转换异常";
        }
        return "";
    }
    /**
     * 获取门店对应的渠道
     *
     * @param terminalCode
     * @return
     */
    @Override
    public ActRePortTypeResBean queryTerminalChannel(String terminalCode,String reportTypeCode,String buCode) throws Exception{

        String sql = " select t.department_code departmentCode,t.channel_code channelCode \n" +
                " from mama100_owner.common_department_terminal t ,mama100_owner.common_department cd \n" +
                " where 1=1 \n" +
                " and cd.relation_code2 = :buCode \n"+
                " and t.department_code = cd.code \n"+
                " and t.terminal_code = :terminalCode ";
        Query query = super.createSQLQuery(sql);
        query.setParameter("buCode", buCode.trim());
        query.setParameter("terminalCode", terminalCode.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map resultMap = (Map)query.uniqueResult();
        if(null != resultMap){
            ActRePortTypeResBean aplReq = new ActRePortTypeResBean();
            String channelCode = (String)resultMap.get("CHANNELCODE");
            aplReq.setChannelCode(channelCode);
            aplReq.setOfficeCode((String) resultMap.get("DEPARTMENTCODE"));

            List<ActRePortListResBean> types = new ArrayList<ActRePortListResBean>();
            HashMap<String, Object> paramsMap = new HashMap<String, Object>();
            paramsMap.put("channelCode", channelCode);

            if(StringUtil.isNotNullNorEmpty(reportTypeCode)){
                paramsMap.put("reportTypeCode", reportTypeCode);
            }

            //2019-01-03  活动关系配置，任意渠道都要关联门店查询活动类型
            // 新增活动类型 by w1038 2018-01-15 //婴线 效验这个门店
            /*if("01".equals(channelCode)) {
                paramsMap.put("terminalCode", terminalCode.trim());
            }*/


            //查询门店所属连锁分类
            String terminalType = queryChainTerminal(terminalCode);

            paramsMap.put("terminalType", terminalType.trim());

            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

            //2019-01-03 注释 活动关系配置，任意渠道都要关联门店查询活动类型
            /*String c_sql = SqlUtil.get("ActRePortRepositoryImpl.queryTerminalChannel.otherChannel", freeMakerContext);
            //婴线 效验这个门店
            if("01".equals(channelCode)) {
                c_sql = SqlUtil.get("ActRePortRepositoryImpl.queryTerminalChannel.babyChannel", freeMakerContext);
            }*/
            String c_sql = SqlUtil.get("ActRePortRepositoryImpl.queryTerminalChannel.babyChannel", freeMakerContext);
            System.out.println("----ActRePortRepositoryImpl.queryTerminalChannel ----"+c_sql.toString());
            types = this.findAllForListBySql(c_sql, paramsMap,ActRePortListResBean.class);

            aplReq.setTypes(types);
            return aplReq;
        }else{
            return null;
        }

    }

    /**
     * 获取图片类型列表（app）
     *
     * @param activityReqBean
     * @return
     */
    @Override
    public List<ActRePortImgTypeResBean> queryImgTypeList(ActRePortImgTypeReqBean activityReqBean) {
        StringBuffer hql = new StringBuffer(" from ActReportImgType t where 1=1 " );
        if (StringUtil.isNotNullNorEmpty(activityReqBean.getStatus())) {
            hql.append(" and t.status = :status ");
        }
        Query query = super.createQuery(hql.toString());
        if (StringUtil.isNotNullNorEmpty(activityReqBean.getStatus())) {
            query.setParameter("status", Integer.valueOf(activityReqBean.getStatus()));
        }
        List<ActRePortImgTypeResBean> list = query.list();
        return list;
    }

    /**
     * 路活动上报图片类型查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<ActRePortImgTypeResBean> queryActRePortImgTypeList(Pager<ActRePortImgTypeResBean> pager, ActRePortImgTypeReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //名称
        String name = reqBean.getName();
        if (StringUtil.isNotNullNorEmpty(name)) {
            paramsMap.put("name", name.trim());
        }

        String status = reqBean.getStatus();
        if (StringUtil.isNotNullNorEmpty(status)) {
            paramsMap.put("status", status.trim());
        }
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("ActRePortRepositoryImpl.queryActRePortImgTypeList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, ActRePortImgTypeResBean.class);
    }

    /**
     * 图片类型保存、编辑
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveActRePortImgType(ActRePortImgTypeReqBean reqBean) throws Exception {
        ServiceBean serviceBean = new ServiceBean();
        ActReportImgType actReportImgType = new ActReportImgType();
        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            actReportImgType = super.getHibernateTemplate().get(ActReportImgType.class, Long.valueOf(reqBean.getId()));
            actReportImgType.setUpdatedTime(new Date());
            actReportImgType.setUpdatedBy(reqBean.getUserId());
            actReportImgType.setUpdatedName(reqBean.getUserName());
        }else{
            actReportImgType.setCreatedTime(new Date());
            actReportImgType.setCreatedBy(reqBean.getUserId());
            actReportImgType.setCreatedName(reqBean.getUserName());
            actReportImgType.setName(reqBean.getName());
        }


        //状态 默认启用
        actReportImgType.setStatus(Integer.valueOf(reqBean.getStatus()));

        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            super.getHibernateTemplate().update(actReportImgType);
        }else {
            //验证活动类型名称是否重复
            String hql = " from ActReportImgType t where 1=1 and t.name = :name ";
            List<ActReportImgType> actReportImgTypeList = (List<ActReportImgType>) this.getHibernateTemplate().findByNamedParam(hql, "name", reqBean.getName());
            if (actReportImgTypeList.size() > 0){
                serviceBean.setCode(201);
                serviceBean.setDesc("类型名称已存在");
                return serviceBean;
            }

            super.save(actReportImgType, ActReportImgType.KEY, "MAMA100_OWNER");
        }
        return serviceBean;

    }

    /**
     * 获取上报关联的SA活动
     *
     * @param req
     * @return
     */
    @Override
    public List<BaseKeyValueBean> querySaFtfActList(ActRePortReqBean req) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        //SA编码
        String saCode = req.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }
        //活动开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            startDate = DateUtils.dateToString(DateUtils.stringToDate(startDate,"yyyy-MM-dd"),"yyyy-MM-dd");
            paramsMap.put("startDate", startDate.trim());
        }
        //活动结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            endDate = DateUtils.dateToString(DateUtils.stringToDate(endDate,"yyyy-MM-dd"),"yyyy-MM-dd");
            paramsMap.put("endDate", endDate.trim());

        }
        //活动现场ID
        String code = req.getCode();
        if (StringUtil.isNotNullNorEmpty(code)) {
            paramsMap.put("code", code.trim());
        }
        //大区
        String areaCode = req.getAreaCode();
        if (StringUtil.isNotNullNorEmpty(areaCode)) {
            paramsMap.put("areaCode", areaCode.trim());
        }
        //办事处
        String officeCode = req.getOfficeCode();
        if (StringUtil.isNotNullNorEmpty(officeCode)) {
            paramsMap.put("officeCode", officeCode.trim());
        }
        //渠道
        String channelCode = req.getChannelCode();
        if (StringUtil.isNotNullNorEmpty(channelCode)) {
            paramsMap.put("channelCode", channelCode.trim());
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("ActRePortRepositoryImpl.querySaFtfActList.list", freeMakerContext);
        if("1".equals(req.getQueryType())) {
            sql = SqlUtil.get("ActRePortRepositoryImpl.queryActRoadList.list", freeMakerContext);
        }
        System.out.println("----ActRePortRepositoryImpl.queryActRePortList SQL----" + sql.toString());
        List<BaseKeyValueBean> saFtfActList = this.findAllForListBySql(sql, paramsMap, BaseKeyValueBean.class);
        return saFtfActList;
    }

    /**
     * 活动现场及活动明细的查询、导出条件组装
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(ActRoadShowListReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaofficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }
        //渠道
        String channelCode = req.getChannelCode();
        if (StringUtil.isNotNullNorEmpty(channelCode)) {
            paramsMap.put("channelCode", channelCode.trim());
        }
        //活动ID
        String actId = req.getActId();
        if (StringUtil.isNotNullNorEmpty(actId)) {
            paramsMap.put("actId", actId.trim());
        }
        //门店编码
        String terminalCode = req.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode.trim());
        }
        //活动创建人工号
        String createdBy = req.getCreatedBy();
        if (StringUtil.isNotNullNorEmpty(createdBy)) {
            paramsMap.put("createdBy", createdBy.trim());
        }
        String reportStartDate = req.getReportStartDate();
        if (StringUtil.isNotNullNorEmpty(reportStartDate)) {
            paramsMap.put("reportStartDate", reportStartDate.trim());
        }
        String reportEndDate = req.getReportEndDate();
        if (StringUtil.isNotNullNorEmpty(reportEndDate)) {
            paramsMap.put("reportEndDate", reportEndDate.trim());
        }

        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        return paramsMap;
    }
    /**
     * 手机上报活动明细列表
     *
     * @param req
     * @return
     */
    @Override
    public ActReportDetailResBean queryActReportDetail(ActRoadShowListReqBean req) {
        HashMap<String, Object> paramsMap = queryParamsMap(req);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("ActRePortRepositoryImpl.queryActReportDetailNum.list", freeMakerContext);
        System.out.println("----ActRePortRepositoryImpl.queryActReportDetailNum.list SQL----"+sql.toString());
        List<ActReportDetailResBean> ardResBeanList = this.findAllForListBySql(sql, paramsMap,ActReportDetailResBean.class);
        ActReportDetailResBean ardResBean = new ActReportDetailResBean();
        if(!CollectionUtil.isEmptyList(ardResBeanList)){
            ardResBean = ardResBeanList.get(0);
        }

        FreeMakerContext freeMakerContext2 = new FreeMakerContext(paramsMap);
        String sql2 = SqlUtil.get("ActRePortRepositoryImpl.queryActReportDetail.list", freeMakerContext2);
        System.out.println("----ActRePortRepositoryImpl.queryActReportDetail.list SQL----"+sql2.toString());
        List<ActRoadShowDetailResBean> arsdList = this.findAllForListBySql(sql2, paramsMap,ActRoadShowDetailResBean.class);
        ardResBean.setArsdList(arsdList);
        return ardResBean;
    }

    /**
     * 手机上报活动明细详情列表
     * @param activityReqBean
     * @return
     */
    @Override
    public List<ActRoadShowDetailResBean> queryActReportDetailInfo(ActRoadShowListReqBean activityReqBean) {
        HashMap<String, Object> paramsMap = queryParamsMap(activityReqBean);
        FreeMakerContext freeMakerContext2 = new FreeMakerContext(paramsMap);
        String sql2 = SqlUtil.get("ActRePortRepositoryImpl.queryActReportDetailInfo.list", freeMakerContext2);
        List<ActRoadShowDetailResBean> arsdList = this.findAllForListBySql(sql2, paramsMap,ActRoadShowDetailResBean.class);
        return arsdList;
    }

    /**
     * 查询是否是活动精英
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean queryIsActElites(ActRoadShowListReqBean reqBean) {
        ServiceBean response = new ServiceBean();
        try {
                HashMap<String, Object> paramsMap = new HashMap<String, Object>();
                paramsMap.put("createdBy", reqBean.getCreatedBy());
                FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
                String sql = SqlUtil.get("ActRePortRepositoryImpl.queryIsActElites", freeMakerContext);
                System.out.println("----ActRePortRepositoryImpl.queryIsActElites SQL----" + sql.toString());
                int count = this.getCountBySql(sql,paramsMap);
                //0:登陆人非指定岗位  1：BNC活动精英  2：活动精英
                response.setResponse(count);
                return response;
        }catch (Exception e){
            e.printStackTrace();
            response.setCode(500);
            response.setDesc(PropUtils.getProp(response.getCode()));
        }
        return response;
    }

    /**
     * 查询活动精英所属办事处的门店
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<BaseKeyValueBean> queryActElitesByOfficeTerminal(ActRoadShowListReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<BaseKeyValueBean> baseKeyValueBeanList = new ArrayList<>();
        try {
            //创建名称
            String createdBy = reqBean.getCreatedBy();
            if (StringUtil.isNotNullNorEmpty(createdBy)) {
                paramsMap.put("createdBy", createdBy);
            }
            //创建名称
            String terminalCode = reqBean.getTerminalCode();
            if (StringUtil.isNotNullNorEmpty(terminalCode)) {
                paramsMap.put("terminalCode", terminalCode);
            }
            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("ActRePortRepositoryImpl.queryActElitesByOfficeTerminal", freeMakerContext);
            System.out.println("----ActRePortRepositoryImpl.queryActElitesByOfficeTerminal SQL----"+sql.toString());
            baseKeyValueBeanList = this.findAllForListBySql(sql, paramsMap,BaseKeyValueBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseKeyValueBeanList;
    }

    /**
     * 查询品牌官微二维码
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean queryReportBrandQrCodeList(ActRePortReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<>();

        //需要调用C端生成的二维码url
        Map<String,String> urlMap = new HashMap();
        Map<String,String> replyId = new HashMap();
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<ActRePortQrcodeResBean> actReaPortQrcodeResBean = new ArrayList<>();
        try {
            //创建名称
            String createdBy = reqBean.getUserId();
            if (StringUtil.isNotNullNorEmpty(createdBy)) {
                paramsMap.put("createdBy", createdBy);
            }

            //门店编码
            String terminalCode = reqBean.getTerminalCode();
            if (StringUtil.isNotNullNorEmpty(terminalCode)) {
                paramsMap.put("terminalCode", terminalCode);
            }

            //活动ID
            String actId = reqBean.getCode();
            if (StringUtil.isNotNullNorEmpty(actId)) {
                paramsMap.put("actId", actId);
            }else{
                serviceBean.setCode(201);
                serviceBean.setDesc("请选择活动");
                return serviceBean;
            }

            String q_sql = "  select to_char(code) code                   \n" +
                            "  from mama100_owner.mkt_sa_config  msc      \n" +
                            "  where 1 = 1                                  \n" +
                            "  and msc.tab_id = :tabId";
            Query query = super.createSQLQuery(q_sql);
            query.setParameter("tabId", actId.trim());
            List<String> list = (ArrayList)query.list();
            //判断是否为空
            if(CollectionUtils.isEmpty(list)){
                serviceBean.setCode(201);
                serviceBean.setDesc("活动未设置生成二维码品牌");
                return serviceBean;
            }
            //所选品牌
            String[] brandCode = reqBean.getBrandCode();
            String htCode = "21706";
            String dodieCode = "33206";

            if (StringUtil.isNotNullNorEmpty(brandCode)) {
                List brandList = Arrays.asList(brandCode);
                //是否包含  21706：HT，33206：Dodie
                if(brandList.contains(htCode) && list.contains(htCode)){
                    urlMap.put("1",mama100_ht);
                    replyId.put("1","383");//ht：383 Dodie:382
                    paramsMap.put("qrType","1");
                }
                if(brandList.contains(dodieCode) && list.contains(dodieCode)){
                    urlMap.put("2",mama100_dodie);
                    replyId.put("2","382");
                    paramsMap.put("qrType","2");
                }
                //如果查询条件包含2个品牌
                if(urlMap.size()==2){
                    List<String> brandlist = new ArrayList<>();
                    brandlist.add("1");
                    brandlist.add("2");
                    paramsMap.put("qrType", brandlist);
                }
            }else{
                //查询条件没有包含  21706：HT，33206：Dodie，直接返回
                serviceBean.setCode(201);
                serviceBean.setDesc("上报品牌为空");
                return serviceBean;
            }
            //如果品牌不包含 21706：HT，33206：Dodie
            if(CollectionUtil.isEmptyMap(urlMap)){
                serviceBean.setCode(208);
                serviceBean.setDesc("上报品牌没有HT或Dodie");
                return serviceBean;
            }
            //查询是否已经有生成过二维码
            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("ActRePortRepositoryImpl.queryActReportQrcode.list", freeMakerContext);
            System.out.println("----ActRePortRepositoryImpl.queryActReportQrcode.list SQL----"+sql.toString());
            actReaPortQrcodeResBean = this.findAllForListBySql(sql, paramsMap,ActRePortQrcodeResBean.class);

            //如果查询没有结果，就调用C端接口生成二维码 第一次不需要传qrCode
            if(CollectionUtils.isNotEmpty(actReaPortQrcodeResBean)) {
                for (ActRePortQrcodeResBean bean : actReaPortQrcodeResBean) {
                    //验证是否在有效期默认30天内
                    if ("0".equals(bean.getStatus())) {
                        //二维码失效，重新调用C端生成

                        Map resultMap = createMama100QrCode((String) urlMap.get(bean.getQrCodeType()), reqBean, (String) replyId.get(bean.getQrCodeType()), 2, bean.getQrCode());
                        if (CollectionUtil.isEmptyMap(resultMap)) {
                            serviceBean.setCode(202);
                            serviceBean.setDesc("查询二维码失败，请稍后重试");
                            return serviceBean;
                        }
                        ActReportQrCode actReportQrCode = updateActReportQrCode(resultMap, reqBean, Long.valueOf(bean.getId()), bean.getQrCodeType());
                        bean.setQrUrl(actReportQrCode.getQrUrl());
                        bean.setQrCode(actReportQrCode.getQrCode());
                        bean.setQrShortUrl(String.valueOf(actReportQrCode.getQrShortUrl()));
                        bean.setQrCodeType(String.valueOf(actReportQrCode.getQrType()));
                    }
                    //删除urlMap 表示不需要在新增
                    urlMap.remove(bean.getQrCodeType());
                }
            }
            for (Map.Entry<String, String> m : urlMap.entrySet()) {
                System.out.println("key:" + m.getKey() + " value:" + m.getValue());
                Map resultMap = createMama100QrCode(m.getValue(),reqBean,(String) replyId.get(m.getKey()),0,"");
                if(CollectionUtil.isEmptyMap(resultMap)){
                    serviceBean.setCode(202);
                    serviceBean.setDesc("查询二维码失败，请稍后重试");
                    return serviceBean;
                }
                ActReportQrCode actReportQrCode = updateActReportQrCode(resultMap,reqBean,null,m.getKey());
                ActRePortQrcodeResBean resBean = new ActRePortQrcodeResBean();
                resBean.setQrUrl(actReportQrCode.getQrUrl());
                resBean.setQrCode(actReportQrCode.getQrCode());
                resBean.setQrShortUrl(String.valueOf(actReportQrCode.getQrShortUrl()));
                resBean.setQrCodeType(String.valueOf(actReportQrCode.getQrType()));
                actReaPortQrcodeResBean.add(resBean);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        serviceBean.setResponse(actReaPortQrcodeResBean);
        return serviceBean;
    }

    /**
     * 查询活动上报签到小程序码
     *
     * @param reqBean
     * @return
     */
    @Override
    public ActRePortQrcodeResBean queryReportActSignMiniCode(ActRePortReqBean reqBean) {
        //根据活动id、门店查询表是否已经有数据
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //门店编码
        String terminalCode = reqBean.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode);
        }

        //活动ID
        String actId = reqBean.getCode();
        if (StringUtil.isNotNullNorEmpty(actId)) {
            paramsMap.put("actId", actId);
        }
        paramsMap.put("qrType","3");

        //查询是否已经有生成过二维码
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("ActRePortRepositoryImpl.queryActReportQrcode.list", freeMakerContext);
        System.out.println("----ActRePortRepositoryImpl.queryActReportQrcode.list SQL----"+sql.toString());
        List<ActRePortQrcodeResBean> actReaPortQrcodeResList  = this.findAllForListBySql(sql, paramsMap,ActRePortQrcodeResBean.class);
        ActRePortQrcodeResBean resBean = null;
        //有数据直接返回
        if(CollectionUtils.isNotEmpty(actReaPortQrcodeResList)) {
            return actReaPortQrcodeResList.get(0);
        }else{

            try {
                //根据活动ID 查询活动信息
                ActRoadShow arsBean = super.getHibernateTemplate().get(ActRoadShow.class, Long.valueOf(reqBean.getCode()));
                if("1".equals(String.valueOf(arsBean.getSignMiniCode()))) {
                    //没有调用C端接口得到小程序码，（小程序码有效性是永久的）在保存到表中
                    String miniCodeUrl = createMiniCode(reqBean, arsBean);
                    if (StringUtils.isNotEmpty(miniCodeUrl)) {
                        System.out.println("-------小程序码miniCodeUrl------" + miniCodeUrl);
                        ActReportQrCode actReportQrCode = new ActReportQrCode();
                        actReportQrCode.setTerminalCode(Long.valueOf(reqBean.getTerminalCode()));
                        actReportQrCode.setActId(Long.valueOf(reqBean.getCode()));
                        actReportQrCode.setQrType(Long.valueOf(3));
                        actReportQrCode.setQrUrl(miniCodeUrl);
                        actReportQrCode.setQrShortUrl(miniCodeUrl);
                        actReportQrCode.setCreatedBy(reqBean.getUserId());
                        actReportQrCode.setCreatedTime(new Date());
                        actReportQrCode.setCreatedName(reqBean.getCreatedName());

                        super.save(actReportQrCode, ActReportQrCode.KEY, "MAMA100_OWNER");
                        resBean = new ActRePortQrcodeResBean();
                        resBean.setId(reqBean.getCode());
                        resBean.setQrCode(miniCodeUrl);
                        resBean.setQrShortUrl(miniCodeUrl);
                        resBean.setQrCodeType("3");
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                logger.error("-------保存活动上报签到小程序码失败"+e.getMessage());
            }
        }
        return resBean;
    }

    /**
     * 提交活动上报
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveActRePortZb(ActRePortZhiBoReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            String dateStr = reqBean.getZbDate();
            reqBean.setZbDate("");
            ActRePortZhiBo actRePortZhiBo =  MapperUtils.map(reqBean,ActRePortZhiBo.class);
            actRePortZhiBo.setZbDate(DateUtils.stringToDate(dateStr,"yyyy-MM-dd"));
            actRePortZhiBo.setCreatedTime(new Date());
            super.save(actRePortZhiBo, ActRePortZhiBo.KEY, "MAMA100_OWNER");
            Long id = actRePortZhiBo.getId();
            if(null != reqBean.getAccountList()) {
                for (Long code : reqBean.getAccountList()) {
                    //上报中活动录入人员列表
                    FtfActReportAccount ftfActReportAccount = new FtfActReportAccount();
                    ftfActReportAccount.setCode(code);
                    ftfActReportAccount.setReportId(id);
                    ftfActReportAccount.setCreatedTime(new Date());
                    super.save(ftfActReportAccount, FtfActReportAccount.KEY, "MAMA100_OWNER");
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            logger.error("-------保存失败"+e.getMessage());
            serviceBean.setCode(505);
            serviceBean.setDesc("上报失败");
        }
        return serviceBean;
    }


    /**
     * 查询直播人员
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<BaseKeyValueBean> queryZhiBoAccount(BaseKeyValueBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<BaseKeyValueBean> baseKeyValueBeanList = new ArrayList<>();
        try {
            //名称/工号
            String key = reqBean.getKey();
            if (StringUtil.isNotNullNorEmpty(key)) {
                paramsMap.put("key", key);
            }

            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("ActRePortRepositoryImpl.queryZhiBoAccount", freeMakerContext);
            System.out.println("----ActRePortRepositoryImpl.queryZhiBoAccount SQL----"+sql.toString());
            baseKeyValueBeanList = this.findAllForListBySql(sql, paramsMap,BaseKeyValueBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseKeyValueBeanList;
    }


    /**
     * 查询活动精英所属办事处的门店
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<BaseKeyValueBean> queryBncActElitesTerminal(ActRoadShowListReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<BaseKeyValueBean> baseKeyValueBeanList = new ArrayList<>();
        try {
            //创建名称
            String createdBy = reqBean.getCreatedBy();
            if (StringUtil.isNotNullNorEmpty(createdBy)) {
                paramsMap.put("createdBy", createdBy);
            }
            //门店名称或编码
            String terminalCode = reqBean.getTerminalCode();
            if (StringUtil.isNotNullNorEmpty(terminalCode)) {
                paramsMap.put("terminalCode", terminalCode);
            }
            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("ActRePortRepositoryImpl.queryBncActElitesTerminal", freeMakerContext);
            System.out.println("----ActRePortRepositoryImpl.queryBncActElitesTerminal SQL----"+sql.toString());
            baseKeyValueBeanList = this.findAllForListBySql(sql, paramsMap,BaseKeyValueBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseKeyValueBeanList;
    }

    public ActReportQrCode updateActReportQrCode(Map resultMap,ActRePortReqBean reqBean,Long id,String type){
        //二维码编号
        String qrCode = String.valueOf(resultMap.get("qrCode"));
        //二维码链接
        String url = String.valueOf(resultMap.get("url"));
        //原始链接
        String shortUrl = String.valueOf(resultMap.get("shortUrl"));

        ActReportQrCode actReportQrCode = new ActReportQrCode();
        if(id != null) {
            //更新二维码相关信息
            actReportQrCode = super.getHibernateTemplate().get(ActReportQrCode.class, id);
            actReportQrCode.setQrCode(qrCode);
            actReportQrCode.setQrUrl(url);
            actReportQrCode.setQrShortUrl(shortUrl);
            actReportQrCode.setUpdateBy(reqBean.getUserId());
            actReportQrCode.setUpdateTime(new Date());
            actReportQrCode.setUpdateName(reqBean.getCreatedName());
            super.getHibernateTemplate().update(actReportQrCode);   //更新状态
        }else{
            actReportQrCode.setTerminalCode(Long.valueOf(reqBean.getTerminalCode()));
            actReportQrCode.setActId(Long.valueOf(reqBean.getCode()));
            actReportQrCode.setQrType(Long.valueOf(type));
            actReportQrCode.setQrCode(qrCode);
            actReportQrCode.setQrUrl(url);
            actReportQrCode.setQrShortUrl(shortUrl);
            actReportQrCode.setCreatedBy(reqBean.getUserId());
            actReportQrCode.setCreatedTime(new Date());
            actReportQrCode.setCreatedName(reqBean.getCreatedName());
            try {
                super.save(actReportQrCode, ActReportQrCode.KEY, "MAMA100_OWNER");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                logger.error("-------保存失败"+e.getMessage());
            }
        }
        return actReportQrCode;
    }
    /**
     * 创建二维码
     * @param url
     * @param reqBean
     * @return
     */
    private Map createMama100QrCode(String url,ActRePortReqBean reqBean,String replyId,int times,String qrCode) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("seqNo", com.biostime.common.util.date.DateUtils.makeDateSeqNo());
        paramMap.put("fromBiz", "act_report");
        paramMap.put("fromSystem", "mama100-sa");
        paramMap.put("paramType", "3");
        paramMap.put("callBack", "0");
        paramMap.put("replyId", replyId);  //ht：383 Dodie:382
        paramMap.put("replyType", "news");
        paramMap.put("paramCode", "terminalCode="+reqBean.getTerminalCode()+"&referrerEmpNo="+reqBean.getUserId()+"&guideId="+reqBean.getCode());
        if(2==times){ //第二次调用传qrCode参数
            paramMap.put("qrCode", qrCode);
        }
        Map mama100resMap = new HashMap();
        try {
            logger.info("createQrCode-sendHttpByPost reqBean paramMap , {}"+paramMap);
            JSONObject json = JSONObject.fromObject(paramMap);
            String res = HttpUtils.post(url, json.toString());
            logger.info("createQrCode-sendHttpByPost result , {}"+res);
            JSONObject obj1 = JSONObject.fromObject(res);
            if ("100".equals(obj1.get("code").toString())) {
                String objRes = obj1.get("data").toString();
                mama100resMap = JsonUtils.json2GenericObject(objRes, new TypeReference<Map>() {
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("生成上报品牌HT、Dodie二维码失败"+e.getMessage());
        }
        System.out.println("-----mama100resMap------"+mama100resMap);
        return mama100resMap;
    }

    /**
     * 创建小程序码
     * @param reqBean
     * @return
     */
    private String createMiniCode(ActRePortReqBean reqBean,ActRoadShow ars ){
        Map<String,Object> paramMap = new HashMap<>();

        paramMap.put("seqNo", com.biostime.common.util.date.DateUtils.makeDateSeqNo());
        paramMap.put("devid", "888");
        paramMap.put("accessToken",  "");
        paramMap.put("actId",  reqBean.getCode());
        paramMap.put("actType",  "report");
        paramMap.put("actName",  ars.getActName());
        paramMap.put("termType",  "2");
        paramMap.put("termCode",  reqBean.getTerminalCode());
        paramMap.put("createUser",  reqBean.getUserId());
        paramMap.put("createUserName",  reqBean.getCreatedName());
        paramMap.put("startTime",  DateUtils.dateToString(ars.getStartTime(),"yyyy-MM-dd")+" 00:00:00");
        paramMap.put("endTime",  DateUtils.dateToString(ars.getEndTime(),"yyyy-MM-dd")+" 23:59:59");

        String miniappCode = "";
        try {
            JSONObject json = JSONObject.fromObject(paramMap);
            String res = HttpUtils.sendHttpByPost(mama100_act_mini_code_url,json);
            logger.info("createMimiCode-sendHttpByPost result , {}"+res);
            JSONObject obj1 = JSONObject.fromObject(res);

            JSONObject resultJson = obj1.getJSONObject("response");

            if ("100".equals(resultJson.get("code").toString())) {
                miniappCode = resultJson.get("miniappCode").toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("生成签到小程序失败"+e.getMessage());
        }
        System.out.println("-----miniappCode------"+miniappCode);
        return miniappCode;
    }
    /**
     * 查询门店连锁分类信息
     * @param terminalCode
     * @return
     */
    public String queryChainTerminal(String terminalCode) throws Exception {
        //其他
        String terminalType = Constant.TERMINAL_TYPE_OTHER;
        try{
            String sql =
                    "select  chain.chain_type code, td.dic_name name \n" +
                            "  from mama100_owner.crm_terminal ct\n" +
                            "  left join mama100_owner.crm_terminal ctt\n" +
                            "    on ct.parent_code = ctt.code\n" +
                            "   and ct.terminaltype in (1, 2)\n" +
                            "  left join mama100_owner.crm_terminal cttt\n" +
                            "    on ctt.parent_code = cttt.code\n" +
                            "  left join mama100_owner.crm_terminal_chaintype chain\n" +
                            "    on chain.terminal_code = nvl(cttt.code, ctt.code)\n" +
                            "   and chain.end_time is null\n" +
                            "  left join mama100_owner.terminal_dictory td\n" +
                            "    on chain.chain_type = td.dic_code\n" +
                            "   and td.dic_type = 'C_T'\n" +
                            "   and ct.status_code = '01'\n" +
                            " where 1 = 1\n" +
                            "   and ct.code = :terminalCode ";

            StringBuffer sb = new StringBuffer();
            sb.append(sql);
            Query query = super.createSQLQuery(sb.toString()).addScalar("code", StringType.INSTANCE).addScalar("name", StringType.INSTANCE);
            query.setParameter("terminalCode", terminalCode.trim());
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            Map map = (Map)query.uniqueResult();

            //其他
            terminalType = Constant.TERMINAL_TYPE_OTHER;
            if(null != map){
                //终端连锁分类 1：全国重点连锁 2：大区重点连锁 3：办事处重点连锁
                //关系配置：'1','大区重点连锁','3','全国重点连锁','4','办事处连锁','2','其他'
                Map maps = new HashMap();
                maps.put("1","3");
                maps.put("2","1");
                maps.put("3","4");
                String qtype = (String)map.get("code");
                if(StringUtil.isNotNullNorEmpty(qtype)){
                    terminalType = (String)maps.get(qtype);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return terminalType;
    }

}
