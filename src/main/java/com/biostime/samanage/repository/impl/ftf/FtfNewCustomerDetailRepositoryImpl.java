package com.biostime.samanage.repository.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.ftf.report.FtfNewCustomerDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfNewCustomerDetailResBean;
import com.biostime.samanage.repository.ftf.FtfNewCustomerDetailRepository;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动新客信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("ftfNewCustomerDetailRepository")
public class FtfNewCustomerDetailRepositoryImpl extends BaseOracleRepositoryImpl implements FtfNewCustomerDetailRepository {



    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(FtfNewCustomerDetailReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //课程名称
        String name = req.getName();
        if (StringUtil.isNotNullNorEmpty(name)) {
            paramsMap.put("name", name.trim());
        }


        //SA编码
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }
        //手机号码
        String mobile = req.getMobile();
        if (StringUtil.isNotNullNorEmpty(mobile)) {
            paramsMap.put("mobile", mobile.trim());
        }
        //会员ID
        String customerId = req.getCustomerId();
        if (StringUtil.isNotNullNorEmpty(customerId)) {
            paramsMap.put("customerId", customerId.trim());
        }

        //活动开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //活动结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }


        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }

        return paramsMap;
    }
    /**
     * FTF活动新客信息
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<FtfNewCustomerDetailResBean> queryFtfNewCustomerDetailList(Pager<FtfNewCustomerDetailResBean> pager, FtfNewCustomerDetailReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfNewCustomerDetailRepositoryImpl.queryFtfNewCustomerDetailList", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, FtfNewCustomerDetailResBean.class);
    }

    /**
     * FTF活动新客信息导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportFtfNewCustomerDetailList(HttpServletResponse response, FtfNewCustomerDetailReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfNewCustomerDetailRepositoryImpl.queryFtfNewCustomerDetailList", freeMakerContext);
        List<FtfNewCustomerDetailResBean> bscarList = this.findAllForListBySql(sql,paramsMap,FtfNewCustomerDetailResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "FTF新客明细";

        th = new String[]{"大区","办事处","课程名称","内部讲师工号","内部讲师名称","兼职讲师工号","兼职讲师名称","兼职讲师归属","SA编码","会员ID","手机号码",
                         "会员姓名","购买时间","产品ID","产品名称","所属品项","是否首次购买","是否交叉品项","最近接触门店编码"};

        colName = new String[] {"areaName","officeName","kcName","lecturerAccount","lecturerName","jzLecturerAccount","jzLecturerName","lecturerByCompany","saCode","customerId","mobile",
                 "customerName","createdTime","productId","productName","categoryName","isSc","isJc","terminalCode"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
}
