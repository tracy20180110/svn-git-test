package com.biostime.samanage.repository.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.single.event.SaEventDetailReqBean;
import com.biostime.samanage.bean.single.event.SaEventDetailResBean;
import com.biostime.samanage.bean.single.event.SaEventReqBean;
import com.biostime.samanage.domain.event.SaEvent;
import com.biostime.samanage.repository.single.SaEventDetailRepository;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Describe:SA接触RepositoryImpl
 * Date: 2017-07-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("saEventDetailRepository")
public class SaEventDetailRepositoryImpl extends BaseOracleRepositoryImpl implements SaEventDetailRepository {


    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryInfoParamsMap(SaEventDetailReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //sa编号
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }

        //会员ID
        String customerId = req.getCustomerId();
        if (StringUtil.isNotNullNorEmpty(customerId)) {
            paramsMap.put("customerId", customerId.trim());
        }

        //手机号码
        String customerMobile = req.getCustomerMobile();
        if (StringUtil.isNotNullNorEmpty(customerMobile)) {
            paramsMap.put("customerMobile", customerMobile.trim());
        }

        //系统来源
        String systemSourse = req.getSystemSourse();
        if (StringUtil.isNotNullNorEmpty(systemSourse)) {
            paramsMap.put("systemSourse", systemSourse.trim());
        }

        //接触来源
        String eventSourse = req.getEventSourse();
        if (StringUtil.isNotNullNorEmpty(eventSourse)) {
            paramsMap.put("eventSourse", eventSourse.trim());
        }

        //接触开始时间
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //接触结束时间
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }

        return paramsMap;
    }

    /**
     * SA接触列表-查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaEventDetailResBean> querySaEventDetailList(Pager<SaEventDetailResBean> pager, SaEventDetailReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaEventDetailRepositoryImpl.querySaEventDetailList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaEventDetailResBean.class);
    }

    /**
     * SA接触明细列表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaEventDetailList(HttpServletResponse response, SaEventDetailReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaEventDetailRepositoryImpl.querySaEventDetailList.list", freeMakerContext);
        List<SaEventDetailResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaEventDetailResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "SA接触明细";

        th = new String[]{"大区","办事处","渠道","SA编码","SA名称","系统来源","接触来源","会员ID","手机号码","BCC工号","BCC名称",
                "育婴顾问工号","育婴顾问名称","接触时间"};

        colName = new String[] {"areaName","officeName","channelName","saCode","saName","systemSourse","eventSourse","customerId","customerMobile","bccCode","bccName",
                "yygwName","yygwCode","createdTime"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());
    }

    /**
     * 扫码注册绑定会员
     *
     * @param map
     * @return
     */
    @Override
    public void saveSaWxBindNewCustomer(Map map) throws Exception {
        //C端 扫码注册绑定会员
        if("AGGREGATION_PAGE".equalsIgnoreCase((String)map.get("redisterSource")) && 1==((Integer) map.get("termType"))) {
            SaEvent saEvent = new SaEvent();
            saEvent.setCustomerId((Long) map.get("customerId"));
            saEvent.setSaCode((String) map.get("termCode"));
            saEvent.setSrcType(4L);
            saEvent.setSystemSource(4L);
            saEvent.setCreatedTime(new Date());
            super.save(saEvent, SaEvent.KEY, "MAMA100_OWNER");
        }
        //C端 小程序注册
        if("miniapp4".equalsIgnoreCase((String)map.get("redisterSource")) && "1".equals((String) map.get("termType"))) {
            SaEvent saEvent = new SaEvent();
            saEvent.setCustomerId((Long) map.get("customerId"));
            saEvent.setSaCode((String) map.get("termCode"));
            saEvent.setSrcType(15L);
            saEvent.setSystemSource(8L);
            saEvent.setCreatedTime(new Date());
            super.save(saEvent, SaEvent.KEY, "MAMA100_OWNER");
        }
    }

    /**
     * 妈妈100微信、营销通app接触签到
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSaEvent(SaEventReqBean reqBean) throws Exception {
        ServiceBean serviceBean = new ServiceBean();
        SaEvent saEvent = new SaEvent();
        saEvent.setCustomerId(reqBean.getCustomerId());
        saEvent.setSaCode(reqBean.getSaCode());
        saEvent.setSrcType(reqBean.getSrcType());
        saEvent.setSystemSource(reqBean.getSystemSource());
        saEvent.setCreatedBy(reqBean.getCreatedBy());
        saEvent.setCreatedName(reqBean.getCreatedName());
        saEvent.setCreatedTime(new Date());
        super.save(saEvent, SaEvent.KEY, "MAMA100_OWNER");
        return serviceBean;
    }


}
