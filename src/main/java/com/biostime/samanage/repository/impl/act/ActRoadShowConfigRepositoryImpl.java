package com.biostime.samanage.repository.impl.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigReqBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigResBean;
import com.biostime.samanage.domain.act.ActRoadShowConfig;
import com.biostime.samanage.domain.act.ActRoadShowConfigGx;
import com.biostime.samanage.repository.act.ActRoadShowConfigRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.*;

/**
 * 路演活动配置
 * Created by dc on 2018/03/13
 */
@Service("actRoadShowConfigRepository")
public class ActRoadShowConfigRepositoryImpl extends BaseOracleRepositoryImpl implements ActRoadShowConfigRepository {

    /**
     * 路演活动及活动明细的查询、导出条件组装
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(ActRoadShowConfigReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();


        //类型名称
        String actTypeName = req.getActTypeName();
        if (StringUtil.isNotNullNorEmpty(actTypeName)) {
            paramsMap.put("actTypeName", actTypeName.trim());
        }

        //状态
        String status = req.getStatus();
        if (StringUtil.isNotNullNorEmpty(status)) {
            paramsMap.put("status", status);
        }

        return paramsMap;
    }

    /**
     * 路演活动--查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<ActRoadShowConfigResBean> queryActRoadShowConfigList(Pager<ActRoadShowConfigResBean> pager, ActRoadShowConfigReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("ActRoadShowConfigRepositoryImpl.queryActRoadShowConfigList", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, ActRoadShowConfigResBean.class);
    }

    /**
     * 路演活动--导出
     *
     * @param response
     * @param reqBean
     * @throws Exception
     */
    @Override
    public void exportActRoadShowConfigList(HttpServletResponse response, ActRoadShowConfigReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("ActRoadShowConfigRepositoryImpl.queryActRoadShowConfigList", freeMakerContext);
        List<ActRoadShowConfigResBean> bscarList = this.findAllForListBySql(sql,paramsMap,ActRoadShowConfigResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "活动现场关系配置列表";

        th = new String[]{"活动类型","适用上报类型","适用品牌","适用渠道","适用门店","状态","最近操作时间",
                "最近操作人","创建时间"};

        colName = new String[] {"actTypeName","reportTypeName","brandName","channelName",
                "terminalType","statusName","updatedTime",
                "updatedBy","createdTime"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(new String(report_name.toString() + ".xlsx"),"UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());
    }

    /**
     * 路演活动关系--状态修改
     *
     * @param reqBean
     * @return
     */
    @Override
    public void editStatusActRoadShowConfig(ActRoadShowConfigReqBean reqBean) {
        ActRoadShowConfig arscBean = super.get(ActRoadShowConfig.class, Long.valueOf(reqBean.getId()));
        int status = 1;
        if(arscBean.getStatus() == status){
            status = 2;
        }
        arscBean.setStatus(status);
        arscBean.setUpdatedTime(new Date());
        arscBean.setUpdatedBy(reqBean.getUpdatedBy());
        super.getHibernateTemplate().update(arscBean);
    }

    /**
     * 路演活动关系-新增
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveActRoadShowConfig(ActRoadShowConfigReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            //验证活动类型名称是否重复
            String hql = " from ActRoadShowConfig arsc where 1=1 and arsc.actTypeName = :actTypeName ";
            List<ActRoadShowConfig> actRoadShowConfigList = (List<ActRoadShowConfig>) this.getHibernateTemplate().findByNamedParam(hql, "actTypeName", reqBean.getActTypeName());
            if (actRoadShowConfigList.size() > 0){
                serviceBean.setCode(201);
                serviceBean.setDesc("活动类型名称已存在");
                return serviceBean;
            }
            ActRoadShowConfig arsc = new ActRoadShowConfig();
            arsc.setCreatedBy(reqBean.getCreatedBy());
            arsc.setCreatedName(reqBean.getCreatedByName());
            arsc.setActTypeName(reqBean.getActTypeName());
            arsc.setStatus(1);
            arsc.setCreatedTime(new Date());
            super.save(arsc, ActRoadShowConfig.KEY, "MAMA100_OWNER");

            /*// 品牌
            List<String> brandList = Arrays.asList(reqBean.getBrand());
            for(String brand:brandList){
                ActRoadShowConfigGx gx = new ActRoadShowConfigGx();
                gx.setActConfigId(arsc.getId());
                gx.setCode(brand);
                gx.setType(1L);
                gxList.add(gx);
            }
            // 渠道
            List<String> channelList = Arrays.asList(reqBean.getChannel());
            for(String channel:channelList){
                ActRoadShowConfigGx gx = new ActRoadShowConfigGx();
                gx.setActConfigId(arsc.getId());
                gx.setCode(channel);
                gx.setType(2L);
                gxList.add(gx);
            }
            // 门店
            List<String> terminalList = Arrays.asList(reqBean.getTerminal());
            for(String terminal:terminalList){
                ActRoadShowConfigGx gx = new ActRoadShowConfigGx();
                gx.setActConfigId(arsc.getId());
                gx.setCode(terminal);
                gx.setType(3L);
                gxList.add(gx);
            }*/

            List<ActRoadShowConfigGx> gxList = new ArrayList<ActRoadShowConfigGx>();

            //key:1 品牌, 2 渠道, 3 门店, 4 上报类型
            Map<String,List<String>> actConfigMap = reqBean.getActConfigMap();
            for (Map.Entry<String, List<String>> entry : actConfigMap.entrySet()) {
                List<String> vlist = entry.getValue();
                for(String code:vlist){
                    ActRoadShowConfigGx gx = new ActRoadShowConfigGx();
                    gx.setActConfigId(arsc.getId());
                    gx.setCode(code);
                    gx.setType(Long.valueOf(entry.getKey()));
                    gxList.add(gx);
                }
            }

            super.batchAdd(gxList, ActRoadShowConfigGx.KEY, "MAMA100_OWNER");
        }catch (Exception e){
            e.printStackTrace();
            serviceBean.setCode(202);
            serviceBean.setDesc("新增失败");

        }
        return serviceBean;
    }


}
