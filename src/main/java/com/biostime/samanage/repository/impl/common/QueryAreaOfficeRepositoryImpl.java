package com.biostime.samanage.repository.impl.common;

import com.biostime.common.datasource.DataSource;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.samanage.bean.common.AreaOfficeResBean;
import com.biostime.samanage.repository.common.QueryAreaOfficeRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取大区办事处
 * Date: 2016-6-22
 * Time: 9:57
 * User: 12804
 * Version:1.0
 */
@Repository
public class QueryAreaOfficeRepositoryImpl extends BaseOracleRepositoryImpl implements QueryAreaOfficeRepository {

    /**
     * 获取用户所属的大区办事处
     * @param areaOfficeCode
     * @return
     */
    @Override
    @DataSource(DataSourceEnum.ORACLE_READ_ONLY)
    public List<AreaOfficeResBean> getUserAreaOffice(String areaOfficeCode) {
        if("null".equals(areaOfficeCode)){
            areaOfficeCode = "";
        }
        List<AreaOfficeResBean> areaOfficeResBeanList = new ArrayList<AreaOfficeResBean>();
        List<AreaOfficeResBean> areaList = getAreaList(areaOfficeCode);  //大区List
        List<AreaOfficeResBean> officeList = null;                      //办事处List
        for(AreaOfficeResBean areaBean:areaList){
            AreaOfficeResBean areaOfficeResBean = new AreaOfficeResBean();
            areaOfficeResBean.setCode(areaBean.getCode());
            areaOfficeResBean.setName(areaBean.getName());
            if(StringUtils.isNotBlank(areaOfficeCode)){ //如果不是总部直接传入登录的办事处大区编码
                officeList = getOfficeList(areaOfficeCode);
            }else{
                officeList = getOfficeList(areaBean.getCode());
            }
            areaOfficeResBean.setOfficeBeanList(officeList);
            areaOfficeResBeanList.add(areaOfficeResBean);
        }
        return areaOfficeResBeanList;
    }


    /**
     * 类功能描述: 根据当前用户获取当前的大区集合
     *
     * @param areaCode
     * @return
     * @version 1.0
     * @author 12804
     * @createDate 2016年6月22日 下午6:03:20
     */
    public List<AreaOfficeResBean> getAreaList(String areaCode) {

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("areaCode", areaCode);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("QueryAreaOfficeRepositoryImpl.getAreaList", freeMakerContext);
        System.out.println("----QueryAreaOfficeRepositoryImpl.getAreaList SQL----"+sql.toString());
        List<AreaOfficeResBean> areaOfficeResBeanList = this.findAllForListBySql(sql, paramsMap,AreaOfficeResBean.class);
        return areaOfficeResBeanList;
    }

    /**
     * 类功能描述: 根据当前用户获取当前的大区集合
     *
     * @param officeCode
     * @return
     * @version 1.0
     * @author 7927
     * @createDate 2016年2月22日 下午6:03:20
     */
    public List<AreaOfficeResBean> getOfficeList(String officeCode) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("officeCode", officeCode);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("QueryAreaOfficeRepositoryImpl.getOfficeList", freeMakerContext);
        System.out.println("----QueryAreaOfficeRepositoryImpl.getOfficeList SQL----"+sql.toString());
        List<AreaOfficeResBean> areaOfficeResBeanList = this.findAllForListBySql(sql, paramsMap,AreaOfficeResBean.class);
        return areaOfficeResBeanList;
    }
}
