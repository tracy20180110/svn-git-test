package com.biostime.samanage.repository.impl.ftf;

import com.biostime.common.bean.ServiceBean;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.actreport.ActFeiChatListResBean;
import com.biostime.samanage.bean.ftf.sign.*;
import com.biostime.samanage.domain.act.ActFeiChat;
import com.biostime.samanage.domain.event.SaEvent;
import com.biostime.samanage.domain.ftf.FtfSignReportSales;
import com.biostime.samanage.repository.ftf.FtfActSignRepository;
import com.biostime.samanage.rpc.OrderRpcCall;
import com.biostime.samanage.util.DateUtils;
import com.mama100.order.rpc.bean.OrderGuideAmountBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF课程
 * Date: 2016-6-23
 * Time: 16:53
 * User: 12804
 * Version:1.0
 */

@Repository
public class FtfActSignRepositoryImpl extends BaseOracleRepositoryImpl implements FtfActSignRepository {


    @Autowired
    private OrderRpcCall orderRpc;
    /**
     * 活动列表
     * @param ftfActSignReqBean
     * @return
     */
    @Override
    public List<FtfActSignResBean> queryMobFtfActList(FtfActSignReqBean ftfActSignReqBean) {
        String type = ftfActSignReqBean.getType();
        String saCode = ftfActSignReqBean.getSaCode();
//        String officeCode = ftfActSignReqBean.getOfficeCode();

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("type", type);
        paramsMap.put("saCode",saCode);
//        paramsMap.put("officeCode",officeCode);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("FtfActSignRepositoryImpl.queryMobFtfActList", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.queryMobFtfActList SQL----"+sql.toString());
        List<FtfActSignResBean> ftfActSignResList = this.findAllForListBySql(sql, paramsMap,FtfActSignResBean.class);
        return ftfActSignResList;
    }

    /**
     * 签到
     * @param reqBean
     * @return
     */
    @Override
    @Transactional
    public ServiceBean saveMobSign(FtfActSignInfoReqBean reqBean) {
        ServiceBean response = new ServiceBean();
        String id = reqBean.getId();
        String customerId = reqBean.getCustomerId();
        //验证会员是否已经参加过该场活动
        Boolean ckFlag = checkCustomerIsFtfAct(id, customerId);
        if(ckFlag){
            response.setCode(1005);
            response.setDesc(PropUtils.getProp(response.getCode()));
        }else{
            //会员没有参加，则签到
            Boolean saveFlag = saveSignInfo(reqBean);
            if(saveFlag){

                //保存渠道行为信息    2017-10-11取消
                /*if(queryChannelCustomer(reqBean)){
                    //保存
                    addCrmCustTerminalChannel(reqBean);
                }*/
                //保存会员行为信息
                addHpMemberEventRec(reqBean);
                //签到成功。
                response.setCode(100);
                response.setDesc(PropUtils.getProp(response.getCode()));
            }else{
                //签到失败。
                response.setCode(1004);
                response.setDesc(PropUtils.getProp(response.getCode()));
            }
        }
        return response;
    }

    /**
     * 签到详情
     *
     * @param actId
     * @return
     */
    @Override
    public FtfActSignListBean querySignInfoList(String actId,String createdBy) {

        FtfActSignListBean ftfActSignListBean = new FtfActSignListBean();

        String sql =    "with base as\n" +
                        " (select fas.ftf_activity_info_id actId,\n" +
                        "         fas.customid,\n" +
                        "         fas.sign_in_time         signInTime\n" +
                        "    from mama100_owner.ftf_activity_sign_up fas\n" +
                        "   where 1 = 1\n" +
                        "     and fas.ftf_activity_info_id = :actId " +
                        "     and fas.is_sign_in = 1)\n" +
                        "select \n" +
                        "       nvl(s.signCount, 0) signCount,\n" +
                        "       nvl(reportSales.value, 0)+nvl(sale.value, 0) salesCount \n" +
//                        "       rtrim(to_char(nvl(reportSales.value, 0)+nvl(sale.value, 0),'FM999990.99'),to_char(0,'D')) salesCount \n" +
                        "  from (select fas.actId actId, count(fas.customid) signCount\n" +
                        "          from base fas\n" +
                        "         where 1 = 1\n" +
                        "         group by fas.actId) s,\n" +
                        "       (select t.act_id actId, " +
                        "           sum(t.sales_volume)  value\n" +
                        "          from mama100_owner.mkt_sa_ftf_sign_report_sales t\n" +
                        "         group by t.act_id) reportSales,\n" +
                        "       (select sum(fc.feichatYxNum) value, b.actId\n" +
                        "          from (select trunc(o1.ord_dt) ord_dt,\n" +
                        "                       o1.cust_id,\n" +
                        "                       sum(o1.sales_amount) feichatNum,\n" +
                        "                       sum(case\n" +
                        "                             when o1.order_status in (5, 6, 7, 9) then\n" +
                        "                              o1.sales_amount\n" +
                        "                             else\n" +
                        "                              0\n" +
                        "                           end) feichatYxNum\n" +
                        "                  from mama100_edw.tt_trd_feichat_orders o1,\n" +
                        "                       mama100_owner.mkt_sa_ftf_activity sfa\n" +
                        "                 where 1 = 1\n" +
                        "                 and trunc(o1.ord_dt) = sfa.act_date\n" +
                        "                 and sfa.id = :actId\n" +
                        "                 group by trunc(o1.ord_dt), o1.cust_id) fc,\n" +
                        "               base b\n" +
                        "         where 1 = 1\n" +
                        "           and b.customid = fc.cust_id(+)\n" +
                        "           and to_date(substr(b.signInTime, 0, 10), 'yyyy-MM-dd') = fc.ord_dt(+)\n" +
                        "         group by b.actId) sale,\n" +
                        "       mama100_owner.mkt_sa_ftf_activity act\n" +
                        " where 1 = 1\n" +
                        "   and act.id = s.actId(+)\n" +
                        "   and act.id = reportSales.actId(+)\n" +
                        "   and act.id = sale.actId(+)\n" +
                        "   and act.id = :actId ";


        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        Query query = super.createSQLQuery(sql).addScalar("signCount", StringType.INSTANCE).addScalar("salesCount", StringType.INSTANCE);
        query.setParameter("actId", actId.trim());
        ftfActSignListBean = (FtfActSignListBean) query.setResultTransformer(Transformers.aliasToBean(FtfActSignListBean.class)).uniqueResult();

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("actId", actId);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get("FtfActSignRepositoryImpl.querySignInfoList", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.querySignInfoList SQL----" + sqls.toString());
        List<FtfActSignInfoBean> signInfoBeanList = this.findAllForListBySql(sqls, paramsMap, FtfActSignInfoBean.class);


        /*//查询feichat销量 订单查询
        List<Long> customidList = new ArrayList<>();
        for (FtfActSignInfoBean bean : signInfoBeanList) {
            customidList.add(bean.getCustomid());
        }

        if(CollectionUtils.isNotEmpty(customidList)){
            //活动信息
            FtfActivity ftfActivity = super.getHibernateTemplate().get(FtfActivity.class, Long.valueOf(actId));

            Date assessmentMonth = DateUtils.getAssessmentMonth(ftfActivity.getActDate());
            Date amonth = DateUtils.stringToDate(DateUtils.dateToString(assessmentMonth, "yyyy-MM") + "-25", "yyyy-MM-dd");

            String strDate = DateUtils.dateToString(ftfActivity.getActDate(), "yyyy-MM-dd") + " " + ftfActivity.getStartTime()+":00";
            Date startDate = DateUtils.getAddReduceDate(DateUtils.stringToDate(strDate, "yyyy-MM-dd HH:mm:ss"), -3);
            Double validAmount = 0.0d;
            List<OrderGuideAmountBean> ogaList = orderRpc.queryOrderGuideAmount(createdBy, customidList, startDate, DateUtils.getTimesnight(ftfActivity.getActDate()), amonth);
            for (FtfActSignInfoBean bean : signInfoBeanList) {
                for (OrderGuideAmountBean oga : ogaList) {
                    if (bean.getCustomid().equals(oga.getCustomerId())) {
                        //支付金额
                        bean.setFcSalesAmount(String.valueOf(oga.getPaidAmount()));
                        //有效金额
                        bean.setFcEffectiveSalesAmount(String.valueOf(oga.getValidAmount()));
                        validAmount += oga.getValidAmount();
                    }
                }
            }
            ftfActSignListBean.setSalesCount(String.valueOf(Double.valueOf(ftfActSignListBean.getSalesCount()) + validAmount));

        }*/
        ftfActSignListBean.setSignInfoBeanList(signInfoBeanList);

        //一般贸易
        String sqls2 = SqlUtil.get("FtfActSignRepositoryImpl.tradeVolumeList", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.tradeVolumeList SQL----"+sqls2.toString());
        List<BaseKeyValueBean> tradeVolumeList = this.findAllForListBySql(sqls2, paramsMap,BaseKeyValueBean.class);
        ftfActSignListBean.setTradeVolumeList(tradeVolumeList);

        boolean flag = checkIsSign(actId);
        ftfActSignListBean.setIsSignBtn(String.valueOf(flag?1:0));
        return ftfActSignListBean;
    }

    /**
     * 查询FTF签到活动信息
     *
     * @param reqBean
     * @return
     */
    @Override
    public FtfSignMiniAppResBean querySignFtfActInfo(FtfSignMiniAppReqBean reqBean) {
        FtfSignMiniAppResBean  ftfSignMiniAppResBean = new FtfSignMiniAppResBean();

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("actId", reqBean.getActId().trim());

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        //如果是1就是SA-FTF活动的签到
        String sqls = SqlUtil.get("FtfActSignRepositoryImpl.querySignFtfActInfo.saftf", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.querySignFtfActInfo SQL----" + sqls.toString());
        ftfSignMiniAppResBean = this.findSingleBySql(sqls, paramsMap, FtfSignMiniAppResBean.class);

        if(StringUtil.isNotNullNorEmpty(reqBean.getCustomerId())){
            //验证会员是否已经参加过该场活动
            Boolean ckFlag = checkCustomerIsFtfAct(reqBean.getActId(), reqBean.getCustomerId());
            ftfSignMiniAppResBean.setSignStatus(ckFlag == true?"1":"0");
        }else{
            ftfSignMiniAppResBean.setSignStatus("0");
        }

        return ftfSignMiniAppResBean;
    }


    /**
     * 活动开始前、结束后30分钟可以签到
     *
     * @param actId
     * @return
     */
    @Override
    public boolean checkIsSign(String actId) {
        String sql =
                "select\n" +
                        " case\n" +
                        "   when to_date(t.start_time, 'hh24:mi') <=\n" +
                        "        to_date(to_char(sysdate + 30 / (24 * 60), 'hh24:mi'), 'hh24:mi') and\n" +
                        "        to_date(t.end_time, 'hh24:mi') >=\n" +
                        "        to_date(to_char(sysdate - 30 / (24 * 60), 'hh24:mi'), 'hh24:mi') then\n" +
                        "    1 \n" +
                        "   else\n" +
                        "    0 \n" +
                        " end as isShowSign\n" +
                        "  from mama100_owner.mkt_sa_ftf_activity t\n" +
                        " where 1 = 1 \n"+
                        "and t.id = :actId";
        Query query = super.createSQLQuery(sql);
        query.setParameter("actId", actId.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map resultMap = (Map)query.uniqueResult();
        int flag = ((BigDecimal)resultMap.get("ISSHOWSIGN")).intValue();
        return flag==0?false:true;
    }

    /**
     * 上传销量
     *
     * @param reqBean
     * @return
     */
    @Override
    public void saveMobSignReportSales(FtfActSignInfoReqBean reqBean) throws Exception {

        FtfSignReportSales fsrsBean = new FtfSignReportSales();
        fsrsBean.setActId(reqBean.getId());
        fsrsBean.setCreatedBy(reqBean.getUserId());
        fsrsBean.setCreatedTime(new Date());
        fsrsBean.setSalesVolume(reqBean.getSalesVolume());
        int i = 0;
        String[] files = reqBean.getFiles();
        for (String imgPath : files) {
            i++;
            if (i == 1) {
                fsrsBean.setImagePath1(imgPath);
            } else if (i == 2) {
                fsrsBean.setImagePath2(imgPath);
            } else if (i == 3) {
                fsrsBean.setImagePath3(imgPath);
            } else if (i == 4) {
                fsrsBean.setImagePath4(imgPath);
            } else if (i == 5) {
                fsrsBean.setImagePath5(imgPath);
            } else if (i == 6) {
                fsrsBean.setImagePath6(imgPath);
            }
        }
        super.save(fsrsBean, FtfSignReportSales.KEY, "MAMA100_OWNER");

    }

    /**
     * 小程序扫码签到
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSignMiniApp(FtfSignMiniAppReqBean reqBean) throws Exception {
        ServiceBean response = new ServiceBean();
        //验证签到时间
        boolean flag = checkIsSign(reqBean.getActId());
        if(flag) {
            //是否重复签到
            //验证会员是否已经参加过该场活动
            Boolean ckFlag = checkCustomerIsFtfAct(reqBean.getActId(), reqBean.getCustomerId());
            if(ckFlag){
                response.setCode(1005);
                response.setDesc(PropUtils.getProp(response.getCode()));
            }else{
                //会员没有参加，则签到
                FtfActSignInfoReqBean reqBean1 = new FtfActSignInfoReqBean();
                reqBean1.setId(reqBean.getActId());
                reqBean1.setCustomerId(reqBean.getCustomerId());
                reqBean1.setLatitude(reqBean.getLatitude());
                reqBean1.setLongitude(reqBean.getLongitude());
                reqBean1.setSourceSystem(reqBean.getSourceSystem());
                Boolean saveFlag = saveSignInfo(reqBean1);
                if(saveFlag){

                    SaEvent saEvent = new SaEvent();
                    saEvent.setCustomerId(Long.valueOf(reqBean.getCustomerId()));
                    saEvent.setSaCode(reqBean.getSaCode());

                    saEvent.setSrcType(16L);
                    saEvent.setSystemSource(8L);
                    saEvent.setCreatedTime(new Date());
                    super.save(saEvent, SaEvent.KEY, "MAMA100_OWNER");

                    //签到成功。
                    response.setCode(100);
                    response.setDesc(PropUtils.getProp(response.getCode()));
                }else{
                    //签到失败。
                    response.setCode(1004);
                    response.setDesc(PropUtils.getProp(response.getCode()));
                }
            }
        }else{
            response.setCode(1108);
            response.setDesc("活动已结束停止签到");
        }
        return response;
    }

    /**
     * feichat贸易销量
     *
     * @return
     */
    @Override
    public ServiceBean saveActFeiChatSale() throws Exception {
        ServiceBean response = new ServiceBean();
        FreeMakerContext freeMakerContext = new FreeMakerContext();
        String sql = SqlUtil.get("FtfActSignRepositoryImpl.queryActListInfo", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.queryActListInfo SQL----"+sql.toString());
        //查询昨天活动
        List<ActFeiChatListResBean> ftfActSignResList = this.findAllForListBySql(sql,ActFeiChatListResBean.class);

        //查询活动签到的会员明细
        FreeMakerContext freeMakerContext2 = new FreeMakerContext();
        String sql2 = SqlUtil.get("FtfActSignRepositoryImpl.queryActSignInfo", freeMakerContext2);
        System.out.println("----FtfActSignRepositoryImpl.queryActSignInfo SQL----"+sql2.toString());
        //查询昨天活动
        List<BaseKeyValueBean> signCustomerList = this.findAllForListBySql(sql2,BaseKeyValueBean.class);
        //查询
        for(ActFeiChatListResBean actBean :ftfActSignResList){

            System.out.println(actBean.getActId()+"--"+actBean.getActDate());
            List<Long> cutomerList = new ArrayList<>();
            for(BaseKeyValueBean signCustomerBean :signCustomerList){
                if(actBean.getActId().equals(signCustomerBean.getKey())){
                    cutomerList.add(Long.valueOf(signCustomerBean.getValue()));
                }
            }
            //判断是否为空
            if(CollectionUtils.isNotEmpty(cutomerList)) {
                actBean.setCustomerId(cutomerList);
                ActFeiChat afcBean = new ActFeiChat();

                Date actDate = DateUtils.stringToDate(actBean.getActDate(), "yyyy-MM-dd");
                Date assessmentMonth = DateUtils.getAssessmentMonth(actDate);
                Date amonth = DateUtils.stringToDate(DateUtils.dateToString(assessmentMonth, "yyyy-MM") + "-25", "yyyy-MM-dd");

                String strDate = actBean.getActDate() + " " + actBean.getStartTime() + ":00";
                Date startDate = DateUtils.getAddReduceDate(DateUtils.stringToDate(strDate, "yyyy-MM-dd HH:mm:ss"), -3);
                Double sale = 0.0d;
                Double totalSale = 0.0d;
                List<OrderGuideAmountBean> ogaList = orderRpc.queryOrderGuideAmount(actBean.getCreatedBy(), actBean.getCustomerId(), startDate, DateUtils.getTimesnight(actDate), amonth);
                for (OrderGuideAmountBean oga : ogaList) {
                    //支付金额
                    totalSale += oga.getPaidAmount();
                    //有效金额
                    sale += oga.getValidAmount();
                }
                afcBean.setActId(Long.valueOf(actBean.getActId()));
                afcBean.setSale(sale);
                afcBean.setTotalSale(totalSale);
                afcBean.setCreatedTime(new Date());
                super.save(afcBean, ActFeiChat.KEY, "MAMA100_OWNER");
            }
        }
        response.setCode(100);
        response.setDesc(PropUtils.getProp(response.getCode()));
        return response;
    }

    /**
     * 查询登陆人所属的SA门店
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<BaseKeyValueBean> querySaTerminal(BaseKeyValueBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<BaseKeyValueBean> baseKeyValueBeanList = new ArrayList<>();
        try {
            //创建名称
            String key = reqBean.getKey();
            if (StringUtil.isNotNullNorEmpty(key)) {
                paramsMap.put("key", key);
            }

            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("FtfActSignRepositoryImpl.querySaTerminal", freeMakerContext);
            System.out.println("----FtfActSignRepositoryImpl.querySaTerminal SQL----"+sql.toString());
            baseKeyValueBeanList = this.findAllForListBySql(sql, paramsMap,BaseKeyValueBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseKeyValueBeanList;
    }

    /**
     * 会员门店渠道信息
     * @param reqBean
     * @return
     */
    public boolean queryChannelCustomer(FtfActSignInfoReqBean reqBean){

        String customerId = reqBean.getCustomerId();
        //查询渠道08 的会员ID是否存在
        FreeMakerContext freeMakerContext = new FreeMakerContext();
        String sql = SqlUtil.get("FtfActSignRepositoryImpl.queryChannelCustomer.count", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.queryChannelCustomer SQL----"+sql.toString());

        Query query = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
        query.setParameter("customerId",customerId);
        int count = query.executeUpdate();
        return count>0?true:false;

    }
    /**
     * 保存会员门店渠道信息
     * @param reqBean
     * @return
     */
    public boolean addCrmCustTerminalChannel(FtfActSignInfoReqBean reqBean){

        String customerId = reqBean.getCustomerId();
        String saCode = reqBean.getSaCode();
        String createdBy = reqBean.getUserId();
        //查询渠道08 的会员ID是否存在
        FreeMakerContext freeMakerContext = new FreeMakerContext();
        String sql = SqlUtil.get("FtfActSignRepositoryImpl.addCrmCustTerminalChannel", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.addCrmCustTerminalChannel SQL----"+sql.toString());

        Query query = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
        query.setParameter("customerId",customerId);
        query.setParameter("saCode",saCode);
        query.setParameter("createdBy",createdBy);
        int count = query.executeUpdate();
        return count>0?true:false;
    }
    /**
     * 保存会员行为接触信息
     * @param reqBean
     * @return
     */
    public void addHpMemberEventRec(FtfActSignInfoReqBean reqBean){

        String customerId = reqBean.getCustomerId();
        String saCode = reqBean.getSaCode();
        String createdBy = reqBean.getUserId();
        //查询渠道08 的会员ID是否存在
        FreeMakerContext freeMakerContext = new FreeMakerContext();
        String sql = SqlUtil.get("FtfActSignRepositoryImpl.addHpMemberEventRec", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.addHpMemberEventRec SQL----"+sql.toString());

        Query query = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
        query.setParameter("customerId",customerId);
        query.setParameter("saCode",saCode);
        query.setParameter("createdBy",createdBy);
        query.executeUpdate();
    }
    /**
     * 保存签到信息
     * @param reqBean
     * @return
     */
    public boolean saveSignInfo(FtfActSignInfoReqBean reqBean){

        FreeMakerContext freeMakerContext = new FreeMakerContext();
        String sql = SqlUtil.get("FtfActSignRepositoryImpl.saveSignInfo", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.saveSignInfo SQL----"+sql.toString());

        Query query = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
        query.setParameter("actId", reqBean.getId());
        query.setParameter("customerId",reqBean.getCustomerId());
        query.setParameter("longitude",reqBean.getLongitude());
        query.setParameter("latitude",reqBean.getLatitude());
        query.setParameter("sysSource",reqBean.getSourceSystem());
        int count = query.executeUpdate();
        return count>0?true:false;
    }

    /**
     * 验证会员是否已经参加过该场活动
     * @param actId
     * @param customerId
     * @return
     */
    public boolean checkCustomerIsFtfAct(String actId,String customerId){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("actId", actId);
        paramsMap.put("customerId",customerId);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("FtfActSignRepositoryImpl.checkCustomerIsFtfAct.count", freeMakerContext);
        System.out.println("----FtfActSignRepositoryImpl.checkCustomerIsFtfAct SQL----"+sql.toString());
        int count = this.getCountBySql(sql,paramsMap);
        return count>0?true:false;
    }

}
