package com.biostime.samanage.repository.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.ftf.lecture.FtfLectureCodeResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerReqBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import com.biostime.samanage.domain.ftf.FtfLecturer;
import com.biostime.samanage.domain.ftf.FtfLecturerCheck;
import com.biostime.samanage.repository.ftf.FtfLecturerRepository;
import com.biostime.samanage.util.ExcelUtil;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF讲师ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository
public class FtfLecturerRepositoryImpl extends BaseOracleRepositoryImpl  implements FtfLecturerRepository {

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(FtfLecturerReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //类型
        String type = req.getType();
        if (StringUtil.isNotNullNorEmpty(type)) {
            paramsMap.put("type", type.trim());
        }
        //名称
        String name = req.getName();
        if (StringUtil.isNotNullNorEmpty(name)) {
            paramsMap.put("name", name.trim());
        }

        //状态
        String status = req.getStatus();
        if (StringUtil.isNotNullNorEmpty(status)) {
            paramsMap.put("status", status.trim());
        }
        //讲师编号
        String code = req.getCode();
        if (StringUtil.isNotNullNorEmpty(code)) {
            paramsMap.put("code", code.trim());
        }
        //讲师编号
        String keyword = req.getKeyword();
        if (StringUtil.isNotNullNorEmpty(keyword)) {
            paramsMap.put("keyword", keyword.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }

        return paramsMap;
    }
    /**
     * 获取FTF讲师列表
     * @param reqBean
     * @return
     */
    @Override
    public Pager<FtfLecturerResBean>  queryFtfLecturerList(Pager<FtfLecturerResBean> pager, FtfLecturerReqBean reqBean){
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfLecturerRepositoryImpl.queryFtfCourseList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, FtfLecturerResBean.class);
    }

    /**
     * FTF讲师编辑
     *
     * @param reqBean
     * @return
     */
    @Override
    public String saveFtfLecturer(FtfLecturerReqBean reqBean)  throws Exception {
        String msg = "";
        FtfLecturer ftfLecturer = new FtfLecturer();
        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            ftfLecturer = super.getHibernateTemplate().get(FtfLecturer.class, Long.valueOf(reqBean.getId()));
            ftfLecturer.setUpdatedTime(new Date());
            ftfLecturer.setUpdatedBy(reqBean.getUserId());
        }else{
            ftfLecturer.setCreatedTime(new Date());
            ftfLecturer.setCreatedBy(reqBean.getUserId());
            ftfLecturer.setCreatedName(reqBean.getUserName());
            //状态 默认启用
            ftfLecturer.setStatus(1);
        }
        ftfLecturer.setType(reqBean.getType());
        ftfLecturer.setSynops(reqBean.getSynops());
        ftfLecturer.setName(reqBean.getName());

        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            super.getHibernateTemplate().update(ftfLecturer);
        }else {
            if("0".equals(reqBean.getType())){ //育儿专家
                String str = "JS";
                String hql =    "select t.code                                          \n" +
                                "  from (select t.code                                  \n" +
                                "          from mama100_owner.mkt_sa_ftf_lecturer t     \n" +
                                "         where 1 = 1                                   \n" +
                                "           and t.type = 0                              \n" +
                                "         order by t.created_time desc) t               \n" +
                                " where rownum = 1";

                Query query = super.createSQLQuery(hql);
                //query.setMaxResults(1);// 必须在查询之前指定，使其返回单个对象
                String code = (String)query.uniqueResult();
                System.out.println("-----查询最后讲师编号------"+code);
                if(StringUtil.isNullOrEmpty(code)){
                    code = "1000";
                }else {
                    code = code.substring(2, code.length());
                }
                int i = Integer.valueOf(code);
                i++;
                ftfLecturer.setCode(str+ StringUtils.leftPad(String.valueOf(i), 4, '0'));
                msg = "讲师创建成功，讲师编号为"+ftfLecturer.getCode()+"!";
            }else{
                ftfLecturer.setCode(reqBean.getCode());
                msg = "讲师创建成功!";
            }
            String countHql = "select count(t.code) from FtfLecturer t where 1=1 and t.code = :code ";

            Query query = super.createQuery(countHql);
            query.setParameter("code",reqBean.getCode());
            int count = ((Number)query.uniqueResult()).intValue();
            if(count > 0){
                return "该讲师"+reqBean.getCode()+"已存在。";
            }
            super.save(ftfLecturer, FtfLecturer.KEY, "MAMA100_OWNER");
        }
        return msg;
    }



    /**
     * FTF讲师停用、启用
     * @param reqBean
     * @return
     */
    @Override
    public void changeStatusFtfLecturer(FtfLecturerReqBean reqBean) {
        FtfLecturer flBean = super.get(FtfLecturer.class, Long.valueOf(reqBean.getId()));
        flBean.setStatus(Integer.valueOf(reqBean.getStatus()));
        flBean.setUpdatedTime(new Date());
        flBean.setUpdatedBy(reqBean.getUserId());
        super.getHibernateTemplate().update(flBean);
    }

    /**
     * FTF讲师列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportFtfLecturerList(HttpServletResponse response, FtfLecturerReqBean reqBean) throws Exception{
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfLecturerRepositoryImpl.queryFtfCourseList.list", freeMakerContext);
        List<FtfLecturerResBean> bscarList = this.findAllForListBySql(sql,paramsMap,FtfLecturerResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "FTF讲师";

        th = new String[]{"大区","办事处","讲师姓名","讲师编号","讲师类型","讲师状态","讲师认证","讲师简介",
                "创建人工号","创建人姓名","创建时间"};

        colName = new String[] {"areaName","officeName","name",
                "code","type","status","ftfLecturerCheck",
                "synops","createdBy","createdName","createdTime"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
    /**
     * 获取讲师名称工号
     * @param keyword
     * @return
     */
    @Override
    public List<FtfLectureCodeResBean> queryLecturerCode(String keyword,String type) throws Exception {
        List<FtfLectureCodeResBean> list = new ArrayList<>();
        String sql = "";
        if(StringUtil.isNotNullNorEmpty(keyword)) {
            if(StringUtil.isNotNullNorEmpty(type) && "0".equals(type)) { //育儿专家
                sql =   "select t.keyword keyword, t.name name, t.code code                             \n" +
                                "  from (select t.name || '/' || t.code keyword,t.name name,t.code code \n" +
                                "          from mama100_owner.mkt_sa_ftf_lecturer t                     \n" +
                                "         where t.type = '0'    ) t                                     \n" +
                                " where  t.keyword like :keyword ";

            }else {

                sql =
                                "     select ca.name || '/' || ca.code keyword,         \n" +
                                "       ca.name name,                                   \n" +
                                "       ca.code code                                    \n" +
                                "    from bst_mdm.common_account ca                  \n" +
                                "    where ca.post_id in ('23','5','646','903','17',    \n" +
                                "   '18','114971','1042','355',                       \n" +
                                "   '783322','1278179','783323','449','31')           \n" +
                                "    and (ca.code like :keyword or ca.name like :keyword) ";
            }

            Query query = super.createSQLQuery(sql).addScalar("code", StringType.INSTANCE).addScalar("name", StringType.INSTANCE).addScalar("keyword", StringType.INSTANCE);
            query.setParameter("keyword", "%" + keyword.trim() + "%");
            list = query.setResultTransformer(Transformers.aliasToBean(FtfLectureCodeResBean.class)).list();
        }
        return list;
    }

    /**
     * 导入讲师-验证
     *
     * @param multipartFile
     * @param reqBean
     * @return
     */
    @Override
    public String importLecturerCheck(MultipartFile multipartFile, FtfLecturerReqBean reqBean) throws Exception {

        //存放不符合要求的信息提示。
        StringBuffer sb = new StringBuffer();
        String fileName = multipartFile.getOriginalFilename();
        boolean flag = false;
        if(fileName.endsWith(".xls")){
            flag = true;
        }
        InputStream inputXLS = multipartFile.getInputStream();
        ExcelUtil excelReader = new ExcelUtil();
        Map<Integer, String[]> map = excelReader.readExcelContent(inputXLS,flag);

        for (int i = 1; i <= map.size(); i++) {
            String[] str = map.get(i);
            int j = 0;

            //讲师编码
            String lecturerCode = str[j++].trim();
            if(lecturerCode.endsWith(".0")){
                lecturerCode = lecturerCode.substring(0,lecturerCode.indexOf("."));
            }

            //讲师编码
            String status = str[j++].trim();
            if("是".equals(status)){  //验证
                status = "1";
            }else{
                status = "0";
            }

            //查询讲师工号是否已经存在
            Query query = this.createQuery("from FtfLecturer fc where fc.code=:lecturerCode ");
            query.setParameter("lecturerCode", lecturerCode);
            FtfLecturer ftflecturer = (FtfLecturer) query.uniqueResult();

            if(null != ftflecturer){
                Query queryCk = this.createQuery("from FtfLecturerCheck fc where fc.lecturerCode=:lecturerCode ");
                queryCk.setParameter("lecturerCode", lecturerCode);
                FtfLecturerCheck ftfLecturerCheck = (FtfLecturerCheck) queryCk.uniqueResult();
                if(null != ftfLecturerCheck){
                    ftfLecturerCheck.setUpdatedBy(reqBean.getUserId());
                    ftfLecturerCheck.setUpdatedTime(new Date());
                }else{
                    ftfLecturerCheck = new FtfLecturerCheck();
                    ftfLecturerCheck.setLecturerCode(lecturerCode);
                    ftfLecturerCheck.setCreatedBy(reqBean.getUserId());
                    ftfLecturerCheck.setCreatedTime(new Date());
                }
                ftfLecturerCheck.setStatus(Integer.valueOf(status));


                if(null != ftfLecturerCheck.getId()){
                    super.getHibernateTemplate().update(ftfLecturerCheck);
                }else{
                    super.save(ftfLecturerCheck, FtfLecturerCheck.KEY, "MAMA100_OWNER");
                }
            }

        }
        return sb.toString();
    }


}
