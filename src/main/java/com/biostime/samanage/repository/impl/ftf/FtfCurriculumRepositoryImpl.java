package com.biostime.samanage.repository.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.ftf.Bnc6Product;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumProductResBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumReqBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumResBean;
import com.biostime.samanage.domain.ftf.FtfCurriculum;
import com.biostime.samanage.repository.ftf.FtfCurriculumRepository;
import com.biostime.samanage.util.UploadUtil;
import com.biostime.samanage.util.WsImageFileUtil;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF课程
 * Date: 2016-6-23
 * Time: 16:53
 * User: 12804
 * Version:1.0
 */
@Repository
public class FtfCurriculumRepositoryImpl extends BaseOracleRepositoryImpl implements FtfCurriculumRepository {

    /**
     * 路演活动及活动明细的查询、导出条件组装
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(FtfCurriculumReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //状态
        String status = req.getStatus();
        if (StringUtil.isNotNullNorEmpty(status)) {
            paramsMap.put("status", status.trim());
        }
        //名称
        String name = req.getName();
        if (StringUtil.isNotNullNorEmpty(name)) {
            paramsMap.put("name", name.trim());
        }

        //活动类型
        String actType = req.getActType();
        if (StringUtil.isNotNullNorEmpty(actType)) {
            paramsMap.put("actType", actType.trim());
        }

        return paramsMap;
    }


    /**
     * FTF课程列表-查询
     *
     * @param pager
     * @param ftfCurriculumReqBean
     * @return
     */
    @Override
    public Pager<FtfCurriculumResBean> queryFtfCurriculumList(Pager<FtfCurriculumResBean> pager, FtfCurriculumReqBean ftfCurriculumReqBean) {
        HashMap<String, Object> paramsMap = queryParamsMap(ftfCurriculumReqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfCurriculumRepositoryImpl.queryFtfCurriculumList", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);
        return this.pageDataBySql(pagerQuery, pager, FtfCurriculumResBean.class);
    }

    /**
     * FTF课程列表-导出
     *
     * @param response
     * @param reqBean
     * @throws Exception
     */
    @Override
    public void exportFtfCurriculumList(HttpServletResponse response, FtfCurriculumReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfCurriculumRepositoryImpl.queryFtfCurriculumList", freeMakerContext);
        List<FtfCurriculumResBean> bscarList = this.findAllForListBySql(sql,paramsMap,FtfCurriculumResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "FTF课程";

        th = new String[]{"课程ID","课程名称","课程状态","呈现应用","推荐人群","图片路径","创建人工号", "创建人姓名","创建时间"};

        colName = new String[] {"id","name","status",
                "appTypeName","recomGroups","imgPath",
                "createdBy","createdName","createdTime"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * FTF课程--状态修改
     *
     * @param reqBean
     * @return
     */
    @Override
    public void changeStatusFtfCurriculum(FtfCurriculumReqBean reqBean) {
        FtfCurriculum fcBean = super.get(FtfCurriculum.class, Long.valueOf(reqBean.getId()));
        fcBean.setStatus(Integer.valueOf(reqBean.getStatus()));
        fcBean.setUpdatedTime(new Date());
        fcBean.setUpdatedBy(reqBean.getUserId());
        super.getHibernateTemplate().update(fcBean);
    }

    /**
     * FTF课程--保存
     *
     * @param reqBean
     * @return
     */
    @Override
    public void saveFtfCurriculum(FtfCurriculumReqBean reqBean) throws Exception {
        FtfCurriculum ftfCurriculum = new FtfCurriculum();
        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            ftfCurriculum = super.getHibernateTemplate().get(FtfCurriculum.class, Long.valueOf(reqBean.getId()));
            ftfCurriculum.setUpdatedTime(new Date());
            ftfCurriculum.setUpdatedBy(reqBean.getUserId());
        }else{
            ftfCurriculum.setCreatedTime(new Date());
            ftfCurriculum.setCreatedBy(reqBean.getUserId());
            ftfCurriculum.setCreatedName(reqBean.getUserName());
            //状态 默认启用
            ftfCurriculum.setStatus(1);
        }

        //主视图图片


        String imagePath = UploadUtil.uploadImage(reqBean.getImgPath(), reqBean.getUpFileName(), WsImageFileUtil.SA_FTF_KC, reqBean.getProjectName(),reqBean.getUserId());
        if (StringUtil.isNotNullNorEmpty(reqBean.getImgPath()) && StringUtil.isNotNullNorEmpty(imagePath)) {
            ftfCurriculum.setImage(imagePath);
        }

        ftfCurriculum.setNote(reqBean.getNote());
        ftfCurriculum.setSynops(reqBean.getSynops());
        ftfCurriculum.setRecomGroups(reqBean.getRecomGroups());
        ftfCurriculum.setName(reqBean.getName());
        ftfCurriculum.setAppType(reqBean.getAppType());
        ftfCurriculum.setAppTypeName(reqBean.getAppTypeName());

        ftfCurriculum.setActTypeName(reqBean.getActTypeName());
        ftfCurriculum.setActType(reqBean.getActType());

        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            super.getHibernateTemplate().update(ftfCurriculum);
        }else {
            super.save(ftfCurriculum, FtfCurriculum.KEY, "MAMA100_OWNER");
        }

    }


    /**
     * 查询BNC6品商品
     *
     * @return
     */
    @Override
    public List<Bnc6Product> queryBnc6Product() throws Exception {
        List<Bnc6Product> bpList = new ArrayList<>();

        String sql =" select t.product_id productId, t.product_name productName,nvl(t.series,'HT') series "+
                    " from bst_mdm.t_products t "+
                    " where category_bnc_6_id = '203' " +
                    " and t.series <>'优选' " +
                    " and t.series <>'铂金' ";
        Query query = super.createSQLQuery(sql).addScalar("productId", StringType.INSTANCE).addScalar("productName", StringType.INSTANCE).addScalar("series", StringType.INSTANCE);
        Map<String,List<FtfCurriculumProductResBean>> maps = new HashMap<String,List<FtfCurriculumProductResBean>>();
        List<FtfCurriculumProductResBean> list = query.setResultTransformer(Transformers.aliasToBean(FtfCurriculumProductResBean.class)).list();
        for(FtfCurriculumProductResBean fcpBean:list){
            if(maps.containsKey(fcpBean.getSeries())){
                List<FtfCurriculumProductResBean> listVal = maps.get(fcpBean.getSeries());
                listVal.add(fcpBean);
            }else{
                List<FtfCurriculumProductResBean> listVal = new ArrayList<FtfCurriculumProductResBean>();
                listVal.add(fcpBean);
                maps.put(fcpBean.getSeries(), listVal);
                Bnc6Product bp = new Bnc6Product();
                bp.setSeries(fcpBean.getSeries());
                bp.setList(maps.get(bp.getSeries()));
                bpList.add(bp);
            }

        }

        return bpList;
    }

}
