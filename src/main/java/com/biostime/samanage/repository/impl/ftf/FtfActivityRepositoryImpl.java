package com.biostime.samanage.repository.impl.ftf;

import com.alibaba.fastjson.JSON;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.mc.config.MCQueueEnum;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.actreport.ActRePortListResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityReqBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityTemplateInfoResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityTemplateResBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoReqBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerInfoReqBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerInfoResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import com.biostime.samanage.domain.ftf.FtfActLecturer;
import com.biostime.samanage.domain.ftf.FtfActivity;
import com.biostime.samanage.domain.ftf.FtfActivityTemplateInfo;
import com.biostime.samanage.repository.ftf.FtfActivityRepository;
import com.biostime.samanage.service.ftf.FtfActivityService;
import com.biostime.samanage.util.DateUtils;
import com.biostime.samanage.util.HttpUtils;
import com.biostime.samanage.util.JsonUtils;
import com.biostime.samanage.util.MapperUtils;
import com.biostime.samanage.util.rabbitMQ.RabbitMQManager;
import com.fasterxml.jackson.core.type.TypeReference;
import net.sf.json.JSONObject;
import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF讲师ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Repository("ftfActivityRepository")
public class FtfActivityRepositoryImpl extends BaseOracleRepositoryImpl  implements FtfActivityRepository {


    @Value( "${mama100.lecturer}" )
    private String mama100_lecturer_url;

    @Value( "${mama100.qr.code}" )
    private String mama100_qr_code_url;

    @Autowired
    private RabbitMQManager rabbitMQManager;

    @Autowired
    private FtfActivityService ftfActivityService;
    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(FtfActivityReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //名称
        String name = req.getCurriculumName();
        if (StringUtil.isNotNullNorEmpty(name)) {
            paramsMap.put("name", name.trim());
        }
        //状态
        String status = req.getStatus();
        if (StringUtil.isNotNullNorEmpty(status)) {
            paramsMap.put("status", status.trim());
        }
        //讲师编号
        String lecturerCode = req.getLecturerCode();
        if (StringUtil.isNotNullNorEmpty(lecturerCode)) {
            paramsMap.put("lecturerCode", lecturerCode.trim());
        }

        //兼职讲师编号
        String jzLecturerAccount = req.getJzLecturerAccount();
        if (StringUtil.isNotNullNorEmpty(jzLecturerAccount)) {
            paramsMap.put("jzLecturerAccount", jzLecturerAccount.trim());
        }
        //SA编码
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }

        //活动开始日期
        String startDate = req.getStartTime();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //活动结束日期
        String endDate = req.getEndTime();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //2018-12-11
        //登陆人大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }
        //如果讲师名称为空，不组装查询条件
        String lecturerName = req.getLecturerName();
        if (StringUtil.isNotNullNorEmpty(lecturerName)) {
            paramsMap.put("lecturerName", lecturerName.trim());
        }

        //活动类型  //0:SA-FTF,1:门店-FTF,2:学术推广,3：内训
        String actType = req.getType();
        if (StringUtil.isNotNullNorEmpty(actType)) {
            paramsMap.put("actType", actType.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }

        return paramsMap;
    }


    /**
     * 获取讲师列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<FtfLecturerResBean> queryLecturerList(FtfActivityReqBean reqBean) {

        List<FtfLecturerInfoReqBean> filterStrList = new ArrayList<>();
        List<FtfLecturerInfoReqBean> filterStrList2 = new ArrayList<>();

        //如果讲师名称为空，不组装查询条件
        if (StringUtil.isNotNullNorEmpty(reqBean.getLecturerName())) {
            FtfLecturerInfoReqBean info = new FtfLecturerInfoReqBean();
            info.setDataType("String");
            info.setQueryColumn("expertName");
            info.setQueryType("like");
            info.setQueryValue(reqBean.getLecturerName());
            filterStrList.add(info);

            FtfLecturerInfoReqBean infoBean = new FtfLecturerInfoReqBean();
            infoBean.setDataType("String");
            infoBean.setQueryColumn("innerAccount");
            infoBean.setQueryType("like");
            infoBean.setQueryValue(reqBean.getLecturerName());

            filterStrList2.add(infoBean);
        }

        //如果讲师ID不为空，组装查询条件
        if (StringUtil.isNotNullNorEmpty(reqBean.getLecturerCode())) {
            FtfLecturerInfoReqBean info1 = new FtfLecturerInfoReqBean();
            info1.setDataType("String");
            info1.setQueryColumn("expertId");
            info1.setQueryType("=");
            info1.setQueryValue(reqBean.getLecturerCode());
            filterStrList.add(info1);
            filterStrList2.add(info1);
        }


        //讲师类型（内部讲师、外部讲师）
        if (StringUtil.isNotNullNorEmpty(reqBean.getLecturerType())) {
            FtfLecturerInfoReqBean info2 = new FtfLecturerInfoReqBean();
            info2.setDataType("int");
            info2.setQueryColumn("expertType");
            info2.setQueryType("=");
            info2.setQueryValue(reqBean.getLecturerType());
            filterStrList.add(info2);
            filterStrList2.add(info2);

            //如果是内部讲师，过滤工号为空的数据
            if("0".equals(reqBean.getLecturerType())) {
                FtfLecturerInfoReqBean info3 = new FtfLecturerInfoReqBean();
                info3.setDataType("String");
                info3.setQueryColumn("innerAccount");
                info3.setQueryType("<>");
                info3.setQueryValue("");
                filterStrList.add(info3);
                filterStrList2.add(info3);
            }
        }

        //启用讲师
        FtfLecturerInfoReqBean info4 = new FtfLecturerInfoReqBean();
        info4.setDataType("String");
        info4.setQueryColumn("state");
        info4.setQueryType("=");
        info4.setQueryValue("1");
        filterStrList.add(info4);
        filterStrList2.add(info4);

        List<FtfLecturerInfoResBean> fiResBeanList = getFtfLecturerInfo(JsonUtils.toJson(filterStrList),reqBean.getToken());
        List<FtfLecturerInfoResBean> fiResBeanList2 = getFtfLecturerInfo(JsonUtils.toJson(filterStrList2),reqBean.getToken());

        //C端接口不支持OR 现在分开查询，然后取并集

        List<FtfLecturerInfoResBean> unionList = new ArrayList<FtfLecturerInfoResBean>(CollectionUtils.union(fiResBeanList,fiResBeanList2));

        List<FtfLecturerResBean> list = new ArrayList<>();
        for(FtfLecturerInfoResBean bean:unionList){
            FtfLecturerResBean resBean = new FtfLecturerResBean();
            resBean.setType(bean.getExpertType());
            resBean.setStatus(bean.getState());
            resBean.setName(bean.getExpertName());
            resBean.setCode(bean.getExpertId());
            resBean.setRank(bean.getLevel());
            resBean.setFtfLecturerCheck(bean.getCertificate());
            resBean.setSynops(bean.getIntroduction());
            resBean.setLecturerAccount(bean.getInnerAccount());
            list.add(resBean);
        }
        return list;
    }

    public List<FtfLecturerInfoResBean> getFtfLecturerInfo(String jsonParams,String token){
        List<FtfLecturerInfoResBean> fiResBeanList = new ArrayList<>();
        try {
            String param = "model=course&className=CourseExpert&filterStr="+jsonParams+"&token="+token;
            logger.info("----param--"+param);
            String result = HttpUtils.sendPost(mama100_lecturer_url, param);

            fiResBeanList = JsonUtils.json2GenericObject(JsonUtils.getNode(result,"response").toString(), new TypeReference<List<FtfLecturerInfoResBean>>() {});
            System.out.println(fiResBeanList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fiResBeanList;
    }

    /**
     * 获取FTF活动列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<FtfActivityResBean> queryFtfActivityList(Pager<FtfActivityResBean> pager, FtfActivityReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfActivityRepositoryImpl.queryFtfActivityList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, FtfActivityResBean.class);
    }

    /**
     * FTF活动编辑
     *
     * @param reqBean
     * @return
     */
    @Override
    @Transactional(rollbackFor=Exception.class)
    public FtfActivity saveFtfActivity(FtfActivityReqBean reqBean) throws Exception{
        String actId = "";
        FtfActivity ftfActivity = new FtfActivity();
        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            ftfActivity = super.getHibernateTemplate().get(FtfActivity.class, Long.valueOf(reqBean.getId()));
            ftfActivity.setUpdatedTime(new Date());
            ftfActivity.setUpdatedBy(reqBean.getUserId());
        }else{

            ftfActivity.setCreatedTime(new Date());
            ftfActivity.setCreatedBy(reqBean.getUserId());
            ftfActivity.setCreatedName(reqBean.getUserName());
            //状态 默认启用
            ftfActivity.setStatus(1);

            ftfActivity.setBrand(reqBean.getBrand());
            ftfActivity.setBrandName(reqBean.getBrandName());
            ftfActivity.setAreaOfficeCode(reqBean.getAreaOfficeCode());

            ftfActivity.setSaCode(reqBean.getSaCode());
            ftfActivity.setTerminalCode(reqBean.getTerminalCode());
            ftfActivity.setType(reqBean.getType());


            //海报分享链接
            ftfActivity.setPosterImageUrl(reqBean.getPosterImageUrl());
            //视频链接
            ftfActivity.setVideoLink(reqBean.getVideoLink());
            //分享文案
            ftfActivity.setSharingCopywriting(reqBean.getSharingCopywriting());

            ftfActivity.setCreatePage(Long.valueOf(reqBean.getCreatePage()));
            //2018-12-11新增

            //2020-07-20新增
            ftfActivity.setNewCustomerCalculation(reqBean.getNewCustomerCalculation());

            ftfActivity.setSource(reqBean.getSource());

            if(StringUtil.isNotNullNorEmpty(reqBean.getExpertType())) {
                ftfActivity.setExpertType(Long.valueOf(reqBean.getExpertType()));
            }
            if(StringUtil.isNotNullNorEmpty(reqBean.getPlaceType())) {
                ftfActivity.setPlaceType(Long.valueOf(reqBean.getPlaceType()));
            }
            ftfActivity.setConsumerType(reqBean.getConsumerType());

            //2019-12-05
            ftfActivity.setFtfTheme(reqBean.getFtfTheme());
        }

        ftfActivity.setActDate(DateUtils.stringToDate(reqBean.getActDate(),"yyyy-MM-dd"));
        ftfActivity.setStartTime(reqBean.getStartTime());
        ftfActivity.setEndTime(reqBean.getEndTime());

        ftfActivity.setCurriculumId(Long.valueOf(reqBean.getCurriculumId()));
//        ftfActivity.setLecturerId(Long.valueOf(reqBean.getLecturerId()));
//        ftfActivity.setLecturerRank(reqBean.getLecturerRank());
        ftfActivity.setProvince(reqBean.getProvince());
        ftfActivity.setCity(reqBean.getCity());
        ftfActivity.setDistrict(reqBean.getDistrict());
        ftfActivity.setAddress(reqBean.getAddress());
/*
        ftfActivity.setActivityFlow(reqBean.getActivityFlow());
        ftfActivity.setParticipationMode(reqBean.getParticipationMode());
        String giftPicturesOne = UploadUtil.uploadImage(reqBean.getGiftPicturesOne(), reqBean.getGiftPicturesOneName(), WsImageFileUtil.SA_FTF_ACT_LP, reqBean.getProjectName(),reqBean.getUserId());
        if (StringUtil.isNotNullNorEmpty(reqBean.getGiftPicturesOne()) && StringUtil.isNotNullNorEmpty(giftPicturesOne)) {
            ftfActivity.setGiftPicturesOne(giftPicturesOne);
        }

        String giftPicturesTwo = UploadUtil.uploadImage(reqBean.getGiftPicturesTwo(), reqBean.getGiftPicturesTwoName(), WsImageFileUtil.SA_FTF_ACT_LP, reqBean.getProjectName(), reqBean.getUserId());
        if (StringUtil.isNotNullNorEmpty(reqBean.getGiftPicturesTwo()) && StringUtil.isNotNullNorEmpty(giftPicturesTwo)) {
            ftfActivity.setGiftPicturesTwo(giftPicturesTwo);
        }

        ftfActivity.setGroupType(Long.valueOf(reqBean.getGroupType()));
        ftfActivity.setGroupNums(reqBean.getGroupNums());
        ftfActivity.setShoppingGuideId(reqBean.getShoppingGuideID());

        //2017-12-07 新增活动团上限个数
        ftfActivity.setActGroupNums(Long.valueOf(reqBean.getActGroupNums()));*/


        //添加讲师信息
        FtfActLecturer ftfActLecturer = new FtfActLecturer();
        List<FtfActLecturer> reqFtfLecturerList = reqBean.getFtfActLecturer();


        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            super.getHibernateTemplate().update(ftfActivity);

            ftfActLecturer.setFtfActId(ftfActivity.getId());
            //内部：修改讲师只有内部
//            ftfActLecturer.setLecturerType(1L);
           List<FtfActLecturer> queryLecturerList = super.getHibernateTemplate().findByExample(ftfActLecturer);

    /*         if(CollectionUtils.isNotEmpty(queryLecturerList)){
                for(FtfActLecturer flBean:queryLecturerList){
                    for(FtfActLecturer bean :reqFtfLecturerList) {
                        if(bean.getLecturerType() == flBean.getLecturerType()){
                            flBean.setLecturerCode(bean.getLecturerCode());
                            flBean.setLecturerName(bean.getLecturerName());
                            flBean.setLecturerRank(bean.getLecturerRank());
                            flBean.setLecturerAccount(bean.getLecturerAccount());
                            flBean.setExpertType(bean.getExpertType());
                            super.getHibernateTemplate().update(flBean);
                        }
                    }
                }
            }else {*/
                System.out.println("-------queryLecturerList----"+queryLecturerList.size());
                //先删除在添加
                super.getHibernateTemplate().deleteAll(queryLecturerList);

                List<FtfActLecturer> ftfActLecturerList = new ArrayList<>();
                for(FtfActLecturer bean :reqFtfLecturerList) {
                    FtfActLecturer ftfActLecturerVo = new FtfActLecturer();
                    ftfActLecturerVo = MapperUtils.map(bean, FtfActLecturer.class);
                    ftfActLecturerVo.setFtfActId(ftfActivity.getId());
                    ftfActLecturerList.add(ftfActLecturerVo);
                }
                super.batchSave(ftfActLecturerList, FtfActLecturer.KEY, "MAMA100_OWNER");
//            }
        }else {
            super.save(ftfActivity, FtfActivity.KEY, "MAMA100_OWNER");
            actId = String.valueOf(ftfActivity.getId());
            String id = actId;

            //创建FTF签到小程序二维码
            String ftfQrCodeUrl = createMama100QrCode(actId);
            if(StringUtils.isNotBlank(ftfQrCodeUrl)) {
                ftfActivity.setFtfQrCodeUrl(ftfQrCodeUrl);
                super.getHibernateTemplate().update(ftfActivity);
            }else{
                //腾讯网络访问失败， 每5分钟调用一次，只要成功就关闭定时任务
                ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
                Runnable runnable = new Runnable()
                {
                    public void run() {
                        String qrUrl = ftfActivityService.changeActMama100QrCode(id);
                        System.out.println("--Runnable--qrUrl--"+qrUrl);
                        if(StringUtils.isNotBlank(qrUrl)){
                            System.out.println("---service.shutdown()--");
                            service.shutdown();
                        }
                    }
                };
                service.scheduleAtFixedRate(runnable, 10, 180, TimeUnit.SECONDS);

            }

            System.out.println("-----创建FTF签到小程序二维码-ftfQrCodeUrl-----活动ID"+ftfActivity.getId());
            List<FtfActLecturer> ftfActLecturerList = new ArrayList<>();
            for(FtfActLecturer bean :reqFtfLecturerList) {
                FtfActLecturer ftfActLecturerVo = new FtfActLecturer();
                ftfActLecturerVo = MapperUtils.map(bean, FtfActLecturer.class);
                ftfActLecturerVo.setFtfActId(ftfActivity.getId());
                ftfActLecturerList.add(ftfActLecturerVo);
            }
            super.batchSave(ftfActLecturerList, FtfActLecturer.KEY, "MAMA100_OWNER");

            if("1".equals(reqBean.getCreatePage())) {
                List<BaseKeyValueBean> listKeyValue = reqBean.getListKeyValue();
                List<FtfActivityTemplateInfo> listInfo = new ArrayList<>();
                for (BaseKeyValueBean bean : listKeyValue) {
                    if (StringUtil.isNotNullNorEmpty(bean.getValue())) {
                        //第二步配置活动模板
                        FtfActivityTemplateInfo templateInfo = new FtfActivityTemplateInfo();
                        templateInfo.setActTemplateId(Long.valueOf(reqBean.getActTemplateId()));
                        templateInfo.setFtfActId(ftfActivity.getId());

                        templateInfo.setCreatedBy(reqBean.getUserId());
                        templateInfo.setCreatedTime(new Date());
                        templateInfo.setTemplateKey(bean.getKey());
                        templateInfo.setTemplateValue(bean.getValue());
                        listInfo.add(templateInfo);
                    }
                }
                super.batchSave(listInfo, FtfActivityTemplateInfo.KEY, "MAMA100_OWNER");
            }

            //推送消息
            HashMap<String, String> msg = new HashMap<String, String>();
            msg.put("terminalCode", ftfActivity.getTerminalCode());
            msg.put("title", "开班培训关联");
            msg.put("content", "您创建了新的活动，请到开班培训关联");
            msg.put("sm", "ROAT");
            msg.put("accountId", reqBean.getAccountId());
            rabbitMQManager.syncSend(MCQueueEnum.MKT_SYS_NOTICE, msg);
        }
        return ftfActivity;
    }

    @Override
    public String changeActMama100QrCode(String actId){
        System.out.println("----changeActMama100QrCode-----actId----"+actId);
        //创建FTF签到小程序二维码
        FtfActivity ftfActivity = super.getHibernateTemplate().get(FtfActivity.class, Long.valueOf(actId));
        if(StringUtils.isEmpty(ftfActivity.getFtfQrCodeUrl())) {
            String ftfQrCodeUrl = createMama100QrCode(actId);
            System.out.println("-----changeActMama100QrCode---code--url--"+ftfQrCodeUrl);
            if (StringUtils.isNotEmpty(ftfQrCodeUrl)) {
                try{
                    ftfActivity.setFtfQrCodeUrl(ftfQrCodeUrl);
                    System.out.println("-----changeActMama100QrCode---update--start-");
                    super.getHibernateTemplate().update(ftfActivity);
                    System.out.println("-----changeActMama100QrCode---update--end--"+ftfActivity.getFtfQrCodeUrl());
                    return ftfActivity.getFtfQrCodeUrl();
                }catch (Exception e){
                    e.printStackTrace();
                }
                System.out.println("-----changeActMama100QrCode---update--end-");
                return null;
            }

        }
        return ftfActivity.getFtfQrCodeUrl();
    }

    /**
     * 获取讲师列表APP
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<FtfLecturerResBean> queryAppLecturerList(FtfActivityReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        String lecturerType = reqBean.getLecturerType();
        if (StringUtil.isNotNullNorEmpty(lecturerType)) {
            paramsMap.put("lecturerType", lecturerType.trim());
        }

        String lecturerName = reqBean.getLecturerName();

        if (StringUtil.isNotNullNorEmpty(lecturerName)) {
            if ("1".equals(reqBean.getLecturerType())) {
                paramsMap.put("outLecturerName", lecturerName.trim());
            } else {
                paramsMap.put("inLecturerName", lecturerName.trim());
            }
        }
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfActivityRepositoryImpl.queryAppLecturerList", freeMakerContext);
        System.out.println("----FtfActivityRepositoryImpl.queryAppLecturerList SQL----"+sql.toString());
        List<FtfLecturerResBean> list = this.findAllForListBySql(sql,paramsMap,FtfLecturerResBean.class);
        return list;
    }

    /**
     * 上报中的人员列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<AccountInfoResBean> queryAppReportAccountInfoList(AccountInfoReqBean reqBean) {

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        String keyword = reqBean.getKeyword();
        if (StringUtil.isNotNullNorEmpty(keyword)) {
            paramsMap.put("keyword", keyword.trim());
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfActivityRepositoryImpl.queryAppReportAccountInfoList", freeMakerContext);
        System.out.println("----FtfActivityRepositoryImpl.queryAppReportAccountInfoList SQL----"+sql.toString());
        List<AccountInfoResBean> list = this.findAllForListBySql(sql,paramsMap,AccountInfoResBean.class);
        return list;
    }

    /**
     * 创建小程序二维码
     * @param actId 活动id
     * @return
     */
    private String createMama100QrCode(String actId) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("seqNo", com.biostime.common.util.date.DateUtils.makeDateSeqNo());
        paramMap.put("fromBiz", "ftf");
        paramMap.put("fromSystem", "mama100-sa");
        paramMap.put("path", "subpackage/pages/signIn/signIn");
        paramMap.put("scene", "actId="+actId+"&actType=ftf");

        String miniappCode = "";
        try {
            JSONObject json = JSONObject.fromObject(paramMap);
            String res = HttpUtils.post(mama100_qr_code_url, json.toString());
            logger.info("createMimiCode-sendHttpByPost result , {}"+res);
            JSONObject obj1 = JSONObject.fromObject(res);
            if ("100".equals(obj1.get("code").toString())) {
                miniappCode = obj1.get("data").toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("生成签到小程序失败"+e.getMessage());
        }
        System.out.println("-----miniappCode------"+miniappCode);
        return miniappCode;
    }
    /**
     * FTF活动停用、启用
     *
     * @param reqBean
     * @return
     */
    @Override
    public void changeStatusFtfActivity(FtfActivityReqBean reqBean) {
        FtfActivity ftfActivity = super.get(FtfActivity.class, Long.valueOf(reqBean.getId()));
        if("2".equals(reqBean.getMethodType())){
            ftfActivity.setVideoLink(reqBean.getVideoLink());
        }else{
            ftfActivity.setStatus(Integer.valueOf(reqBean.getStatus()));
        }
        ftfActivity.setUpdatedTime(new Date());
        ftfActivity.setUpdatedBy(reqBean.getUserId());
        super.getHibernateTemplate().update(ftfActivity);
    }



    /**
     * FTF活动列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportFtfActivityList(HttpServletResponse response, FtfActivityReqBean reqBean)  throws Exception{
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfActivityRepositoryImpl.queryFtfActivityList.list", freeMakerContext);
        List<FtfActivityResBean> bscarList = this.findAllForListBySql(sql,paramsMap,FtfActivityResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "FTF活动";

        th = new String[]{"活动ID","大区","办事处","品类","FTF主题","课程名称","呈现平台","内部讲师工号","内部讲师名称","兼职讲师工号","兼职讲师名称","外部专家","活动日期",
                "开始时间","结束时间","SA编码","SA名称","门店编码","活动地址","活动类型","活动状态","报备平台","创建人","创建人名称","创建时间"};

        colName = new String[] {"id","areaName","officeName","brandName","ftfTheme","curriculumName","appTypeName","lecturerAccount","lecturerName","jzLecturerAccount","jzLecturerName","externalName",
                "actDate","startTime","endTime","saCode","saName","terminalCode","address","type","status",
                "source","createdBy","createdName","createdTime"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * 获取SA对应的门店
     * @param saCode
     * @return
     */
    @Override
    public ActRePortListResBean querySaTerminal(String saCode) throws Exception {
        ActRePortListResBean arplResBean = new ActRePortListResBean();
        if(StringUtil.isNotNullNorEmpty(saCode)){
            String sql =
                    "select a.terminal_code code,sa.name name,a.name ctName                                 \n" +
                            "  from (select t.sa_code,ct.name,t.terminal_code                               \n" +
                            "          from mkt_sa_terminal_manage t,mama100_owner.crm_terminal ct          \n" +
                            "         where 1 = 1                                                           \n" +
                            "           and t.status = 1                                                    \n" +
                            "           and t.start_date <= trunc(sysdate)                                  \n" +
                            "           and (t.end_date >= trunc(sysdate)  or t.end_date is null)           \n" +
                            "           and ct.status_code = '01'                                           \n" +
                            "           and t.terminal_code = ct.code ) a,                                  \n" +
                            "       mama100_owner.crm_terminal sa                                           \n" +
                            " where 1 = 1                                                                   \n" +
                            "   and sa.status_code = '01'                                                   \n" +
                            "   and sa.terminal_channel_code = '08'                                         \n" +
                            "   and sa.code = a.sa_code(+)                                                  \n" +
                            "   and sa.code = :saCode";
            Query query = super.createSQLQuery(sql).addScalar("code", StringType.INSTANCE).addScalar("name", StringType.INSTANCE).addScalar("ctName", StringType.INSTANCE);
            query.setParameter("saCode", saCode.trim());
            arplResBean = (ActRePortListResBean)query.setResultTransformer(Transformers.aliasToBean(ActRePortListResBean.class)).uniqueResult();
        }

        return arplResBean;
    }


    /**
     * 查询门店是否存在
     * @param terminalCode
     * @return
     */
    @Override
    public ActRePortListResBean queryTerminal(String terminalCode) throws Exception {
        ActRePortListResBean arplResBean = new ActRePortListResBean();
        if(StringUtil.isNotNullNorEmpty(terminalCode)){
            String sql =
                    "select ct.code, ct.name,ct.short_name shortName         \n" +
                            "  from mama100_owner.crm_terminal ct                    \n" +
                            "         where 1 = 1                                    \n" +
                            "   and ct.status_code = '01'                            \n" +
                            "   and ct.code = :terminalCode";
            Query query = super.createSQLQuery(sql).addScalar("code", StringType.INSTANCE).addScalar("name", StringType.INSTANCE).addScalar("shortName", StringType.INSTANCE);
            query.setParameter("terminalCode", terminalCode.trim());
            arplResBean = (ActRePortListResBean)query.setResultTransformer(Transformers.aliasToBean(ActRePortListResBean.class)).uniqueResult();
        }

        return arplResBean;
    }

    /**
     * 查询导购ID是否存在
     *
     * @param shoppingGuideId
     * @return
     */
    @Override
    public ServiceBean queryShoppingGuideId(String shoppingGuideId) throws Exception {
        ServiceBean serviceBean = new ServiceBean();
        String sql =
                "  select count(1)                              \n" +
                        "  from mama100_owner.snsec_act_guide  sag      \n" +
                        "  where 1 = 1                                  \n" +
                        "  and sag.code = :shoppingGuideId";
        Query query = super.createSQLQuery(sql);
        query.setParameter("shoppingGuideId", shoppingGuideId.trim());
        int count = ((BigDecimal)query.uniqueResult()).intValue();
        if(count == 0 ){
            serviceBean.setCode(200);
            serviceBean.setDesc("导购活动ID不存在。");
        }
        return serviceBean;
    }

    /**
     * FTF海报分享链接更改
     *
     * @param reqBean
     * @return
     */
    @Override
    public void changeImageFtfActivity(FtfActivityReqBean reqBean) {
        FtfActivity ftfActivity = super.get(FtfActivity.class, Long.valueOf(reqBean.getId()));

        ftfActivity.setPosterImageUrl(reqBean.getPosterImageUrl()); //海报分享链接
        ftfActivity.setUpdatedTime(new Date());
        ftfActivity.setUpdatedBy(reqBean.getUserId());
        super.getHibernateTemplate().update(ftfActivity);
    }

    /**
     * 获取FTF活动模板列表
     *
     * @return
     */
    @Override
    public List<FtfActivityTemplateResBean> queryFtfActivityTemplateList() {

        FreeMakerContext freeMakerContext = new FreeMakerContext();
        String sql= SqlUtil.get("FtfActivityRepositoryImpl.queryFtfActivityTemplateList.list", freeMakerContext);
        List<FtfActivityTemplateResBean> list = this.findAllForListBySql(sql,FtfActivityTemplateResBean.class);
        return list;
    }

    /**
     * 查询SA线下课堂模板详情
     *
     * @param templateId
     * @param actId
     * @return
     */
    @Override
    public Map querySaFtfActTemplateInfo(String templateId,String actId) {
        Map resultMap = new HashMap<>();
        HashMap paramsMap = new HashMap();
        // 活动Id
        if (StringUtil.isNotNullNorEmpty(actId)) {
            paramsMap.put("ftfActId", actId.trim());
        }
        if (StringUtil.isNotNullNorEmpty(templateId)) {
            paramsMap.put("ftfActTemplateId", templateId.trim());
        }
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("FtfActivityRepositoryImpl.querySaFtfActTemplateInfo.list", freeMakerContext);

        List<FtfActivityTemplateInfoResBean> list = this.findAllForListBySql(sql, paramsMap,
                FtfActivityTemplateInfoResBean.class);

        FtfActivityTemplateInfoResBean templateInfoResBean = new FtfActivityTemplateInfoResBean();
        if(null != list ){
            if(list.size()>0){
                templateInfoResBean = list.get(0);
            }

            String hql = "from FtfActivityTemplateInfo t where t.ftfActId = :ftfActId and t.actTemplateId = :actTemplateId";
            Query query = this.createQuery(hql);
            query.setLong("ftfActId", Long.valueOf(actId));
            query.setLong("actTemplateId", Long.valueOf(templateId));

            List<FtfActivityTemplateInfo> templateInfosList = query.list();
            Map mapB = new HashMap<>();
            if(null != templateInfosList ){
                if(templateInfosList.size()>0){
                    for (FtfActivityTemplateInfo info : templateInfosList) {
                        mapB.put(info.getTemplateKey(),info.getTemplateValue());
                    }
                }
            }

            //BeanMap(Object)  转成Map时 会多一个key=class 的属性，先转JSON 在转Map即可。

            Map mapA = new BeanMap(templateInfoResBean);
            mapB.putAll(mapA);
            String msg = JSONObject.fromObject(mapB).toString();
            resultMap = (Map) JSON.parse(msg);
        }
        return resultMap;
    }

}
