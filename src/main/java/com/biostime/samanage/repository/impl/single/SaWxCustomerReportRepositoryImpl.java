package com.biostime.samanage.repository.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportReqBean;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportResBean;
import com.biostime.samanage.repository.single.SaWxCustomerReportRepository;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA微信会员报表RepositoryImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("saWxCustomerReportRepository")
public class SaWxCustomerReportRepositoryImpl  extends BaseOracleRepositoryImpl implements SaWxCustomerReportRepository {


    private String format = "yyyy-mm-dd";

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(SaWxCustomerReportReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //BCC编码
        String bccCode = req.getBccCode();
        if (StringUtil.isNotNullNorEmpty(bccCode)) {
            paramsMap.put("bccCode", bccCode.trim());
        }
        //会员手机号码
        String customerMobile = req.getCustomerMobile();
        if (StringUtil.isNotNullNorEmpty(customerMobile)) {
            paramsMap.put("customerMobile", customerMobile.trim());
        }
        //会员ID
        String customerId = req.getCustomerId();
        if (StringUtil.isNotNullNorEmpty(customerId)) {
            paramsMap.put("customerId", customerId.trim());
        }
        //育婴顾问工号
        String yygwCode = req.getYygwCode();
        if (StringUtil.isNotNullNorEmpty(yygwCode)) {
            paramsMap.put("yygwCode", yygwCode.trim());
        }
        //SA编码
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }

        //活动开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //活动结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }

        return paramsMap;
    }
    /**
     * SA微信会员报表列表
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaWxCustomerReportResBean> querySaWxCustomerReportList(Pager<SaWxCustomerReportResBean> pager, SaWxCustomerReportReqBean reqBean){
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源

        Pager<SaWxCustomerReportResBean> saWxCustomerReportPage = new Pager<SaWxCustomerReportResBean>();
        try{
            HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

            //默认历史
            String sql= SqlUtil.get("SaWxCustomerReportRepositoryImpl.querySaWxCustomerReportList.hiostory.list", freeMakerContext);

            Date endDate = DateFormatUtils.ISO_DATE_FORMAT.parse(reqBean.getEndDate());
            String nowDateStr = DateFormatUtils.ISO_DATE_FORMAT.format(new Date());
            Date nowDate = DateFormatUtils.ISO_DATE_FORMAT.parse(nowDateStr);

            //如果结束时间大于等于当天，查询实时，否则查询历史表
            if(endDate.after(nowDate) || endDate.equals(nowDate)){
               sql= SqlUtil.get("SaWxCustomerReportRepositoryImpl.querySaWxCustomerReportList.list", freeMakerContext);
            }
            String countSql= "select count(*) from ("+sql+")";

            PagerQuery pagerQuery = new PagerQuery();
            pagerQuery.setSearchSql(sql);
            pagerQuery.setCountSql(countSql);
            pagerQuery.setParamsMap(paramsMap);

            saWxCustomerReportPage = this.pageDataBySql(pagerQuery, pager, SaWxCustomerReportResBean.class);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return saWxCustomerReportPage;
    }

    /**
     * SA微信会员报表列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaWxCustomerReportList(HttpServletResponse response, SaWxCustomerReportReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        //默认历史
        String sql= SqlUtil.get("SaWxCustomerReportRepositoryImpl.querySaWxCustomerReportList.hiostory.list", freeMakerContext);

        Date endDate = DateFormatUtils.ISO_DATE_FORMAT.parse(reqBean.getEndDate());
        String nowDateStr = DateFormatUtils.ISO_DATE_FORMAT.format(new Date());
        Date nowDate = DateFormatUtils.ISO_DATE_FORMAT.parse(nowDateStr);

        //如果结束时间大于等于当天，查询实时，否则查询历史表
        if(endDate.after(nowDate) || endDate.equals(nowDate)){
            sql= SqlUtil.get("SaWxCustomerReportRepositoryImpl.querySaWxCustomerReportList.list", freeMakerContext);
        }

        List<SaWxCustomerReportResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaWxCustomerReportResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "SA微信会员报表";

        th = new String[]{"大区","办事处","SA编码","SA名称","会员ID",
                "手机号码","宝宝生日/预产期", "BCC工号","BCC名称","育婴顾问工号","育婴顾问名称","育婴顾问归属","创建时间"};

        colName = new String[] {"areaName","officeName","saCode","saName",
                "customerId","customerMobile","birthDate", "bccCode","bccName","yygwCode","yygwName","yygwByCompany","createdTime"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
}
