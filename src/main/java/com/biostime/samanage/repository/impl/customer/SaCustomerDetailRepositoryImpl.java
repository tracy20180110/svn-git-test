package com.biostime.samanage.repository.impl.customer;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.customer.CustomerDetailBean;
import com.biostime.samanage.repository.customer.SaCustomerDetailRepository;
import com.biostime.samanage.util.DateUtils;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * SAk客户明细
 */
@Repository("saCustomerDetailRepository")
public class SaCustomerDetailRepositoryImpl extends BaseOracleRepositoryImpl implements SaCustomerDetailRepository {

    /**
     * 分页查询SA客户明细
     *
     * @param pager
     * @param customerDetailBean
     * @return
     */
    @Override
    public Pager<CustomerDetailBean> searchSaCustomerDetail(Pager<CustomerDetailBean> pager, CustomerDetailBean customerDetailBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String sqlStr = "SaCustomerDetailRepositoryImpl.searchSaCustomerDetail.list";
        if (null != customerDetailBean) {
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getNursingConsultant())) {
                paramsMap.put("nursingConsultant", customerDetailBean.getNursingConsultant());
            }
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getAreaCode())) {
                paramsMap.put("areaCode", customerDetailBean.getAreaCode());
            }
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getOfficeCode())) {
                paramsMap.put("officeCode", customerDetailBean.getOfficeCode());
            }
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getSaCode())) {
                paramsMap.put("saCode", customerDetailBean.getSaCode());
            }
            if (null != customerDetailBean.getCustomerId()) {
                paramsMap.put("customerId", customerDetailBean.getCustomerId());
            }
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getMobile())) {
                paramsMap.put("mobile", customerDetailBean.getMobile());
            }
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getProBcc())) {
                paramsMap.put("proBcc", customerDetailBean.getProBcc());
            }
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getSrcLocName())) {
                paramsMap.put("srcLocName", customerDetailBean.getSrcLocName());
            }
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getParentingInstructor())) {
                paramsMap.put("parentingInstructor", customerDetailBean.getParentingInstructor());
            }
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getBuCode())) {
                paramsMap.put("buCode", customerDetailBean.getBuCode());
            }
            /**
             *1段	14453
             2段	14548
             3段	14549
             4段	14454
             妈咪奶粉	14550
             */
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getStage())) {
                paramsMap.put("stage", customerDetailBean.getStage());
            }
            /**
             *【得悉途径】CF_ZY - 产科住院部 EK_ZY - 儿科住院部 CK_MZ - 产科门诊 EK_MZ - 儿科门诊 EBK_MZ - 儿保科门诊 YH_B - 孕妇班 MM_B - 妈妈班 YY_G - 游泳馆；
             *【积分来源】现有逻辑【1 POS机； 2 营销通；3 CRM；4 商家中心；5 微信；6 妈妈100APP； 7 母婴之家； 0 无】；
             * 呈现逻辑，下拉选项共4个，分别是全部、营销通APP、合生元妈妈100微信、妈妈100APP；
             * 全部 显示所有数据（1、2、3、4、5、6、7、0）；
             * 营销通APP，对应值（ 2、4）；
             * 合生元妈妈100微信 （5）；
             * 妈妈100APP （6）；
             **/
            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getRecSrc())) {
                List<String> recSrc = new ArrayList<String>();
                String src = customerDetailBean.getRecSrc();
                if ("24".equals(src)) {
                    recSrc.add("2");
                    recSrc.add("4");
                }
                if ("5".equals(src)) {
                    recSrc.add("5");
                }
                if ("6".equals(src)) {
                    recSrc.add("6");
                }
                paramsMap.put("recSrc", recSrc);
            }


            if (StringUtil.isNotNullNorEmpty(customerDetailBean.getStartTime())) {
                Calendar now = Calendar.getInstance();
                int day = now.get(Calendar.DAY_OF_MONTH);
                //当前日期大于25号`    `
                int tempDay = DateUtils.getTempDay(sdf, now, day);
                int startDay = Integer.parseInt(sdf.format(DateUtils.stringToDate(customerDetailBean.getStartTime(),"yyyy-MM-dd")));
                /**
                 * 如果开始时间小于目标时间，则查询历史明细表
                 */
                if (startDay < tempDay) {
                    sqlStr = "SaCustomerDetailRepositoryImpl.searchSaCustomerDetail.history.list";
                }
                paramsMap.put("startTime", customerDetailBean.getStartTime());
            }
                if (StringUtil.isNotNullNorEmpty(customerDetailBean.getEndTime())) {
                paramsMap.put("endTime", customerDetailBean.getEndTime());

            }
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        String sql = SqlUtil.get(sqlStr, freeMakerContext);

        String countSql = "select count(1) from ( " + sql + " )";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, CustomerDetailBean.class);

    }


    /**
     * 导出查询SA客户明细
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param proBcc
     * @param srcLocName
     * @param recSrc
     * @param startTime
     * @param endTime
     * @return
     * @throws ParseException
     */
    @Override
    public List<CustomerDetailBean> getSaCustomerDetail(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobile, String proBcc, String srcLocName, String recSrc, String startTime, String endTime, String stage,String buCode, String parentingInstructor) throws ParseException {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String sqlStr = "SaCustomerDetailRepositoryImpl.searchSaCustomerDetail.list";

        if (StringUtil.isNotNullNorEmpty(nursingConsultant)) {
            paramsMap.put("nursingConsultant", nursingConsultant);
        }
        if (StringUtil.isNotNullNorEmpty(areaCode)) {
            paramsMap.put("areaCode", areaCode);
        }
        if (StringUtil.isNotNullNorEmpty(officeCode)) {
            paramsMap.put("officeCode", officeCode);
        }
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode);
        }
        if (StringUtil.isNotNullNorEmpty(customerId)) {
            paramsMap.put("customerId", customerId);
        }
        if (StringUtil.isNotNullNorEmpty(mobile)) {
            paramsMap.put("mobile", mobile);
        }
        if (StringUtil.isNotNullNorEmpty(proBcc)) {
            paramsMap.put("proBcc", proBcc);
        }
        if (StringUtil.isNotNullNorEmpty(srcLocName)) {
            paramsMap.put("srcLocName", srcLocName);
        }
        if (StringUtil.isNotNullNorEmpty(recSrc)) {
            paramsMap.put("recSrc", recSrc);
        }
        if (StringUtil.isNotNullNorEmpty(stage)) {
            paramsMap.put("stage", stage);
        }
        if (StringUtil.isNotNullNorEmpty(parentingInstructor)) {
            paramsMap.put("parentingInstructor", parentingInstructor);
        }
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode);
        }

        if (StringUtil.isNotNullNorEmpty(startTime)) {
            Calendar now = Calendar.getInstance();
            int day = now.get(Calendar.DAY_OF_MONTH);
            //当前日期大于25号`    `
            int tempDay = DateUtils.getTempDay(sdf, now, day);
            int startDay = Integer.parseInt(sdf.format(DateUtils.stringToDate(startTime,"yyyy-MM-dd")));
            /**
             * 如果开始时间小于目标时间，则查询历史明细表
             */
            if (startDay < tempDay) {
                sqlStr = "SaCustomerDetailRepositoryImpl.searchSaCustomerDetail.history.list";
            }
            paramsMap.put("startTime", startTime);
        }
        if (StringUtil.isNotNullNorEmpty(endTime)) {
            paramsMap.put("endTime", endTime);

        }

        FreeMakerContext freeMakerContext1 = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get(sqlStr, freeMakerContext1);
        List<CustomerDetailBean> CustomerDetailBeanList = this.findAllForListBySql(sqls, paramsMap, CustomerDetailBean.class);
        return CustomerDetailBeanList;
    }
}
