package com.biostime.samanage.repository.impl.saPrize;

import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.samanage.bean.saPrize.*;
import com.biostime.samanage.repository.saPrize.SaPrizeRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA. Describe:SA工资列表 Date: 2016-6-23 Time: 17:26 User:
 * 12804 Version:1.0
 */
@Repository
public class SaPrizeRepositoryImpl extends BaseOracleRepositoryImpl implements SaPrizeRepository {

	@Override
	public List<SaPrizeResBean> querySaPrizeList(SaPrizeReqBean saPrizeReqBean) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("officeCode", saPrizeReqBean.getOfficeCode());
		paramsMap.put("salesMonth", saPrizeReqBean.getMonth());
		paramsMap.put("buCode", saPrizeReqBean.getBuCode());

		FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
		String sql = SqlUtil.get("SaPrizeRepositoryImpl.querySaPrizeList", freeMakerContext);
		System.out.println("----SaPrizeRepositoryImpl.querySaPrizeList SQL----" + sql.toString());
		List<SaPrizeResBean> saPrizeList = this.findAllForListBySql(sql, paramsMap, SaPrizeResBean.class);
		return saPrizeList;
	}

	@Override
	public List<SaPrizeDetailResBean> querySaPrizeDetailInfo(SaPrizeDetailReqBean saPrizeDetailReqBean) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("salesMonth", saPrizeDetailReqBean.getMonth());
		paramsMap.put("accountNo", saPrizeDetailReqBean.getAccount());

		FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
		String sql = SqlUtil.get("SaPrizeRepositoryImpl.querySaPrizeDetailInfo", freeMakerContext);
		System.out.println("----SaPrizeRepositoryImpl.querySaPrizeDetailInfo SQL----" + sql.toString());
		List<SaPrizeDetailResBean> saPrizeDetailList = this.findAllForListBySql(sql, paramsMap,
				SaPrizeDetailResBean.class);
		if (saPrizeDetailList.size() == 0) {
			SaPrizeDetailResBean saPrize = new SaPrizeDetailResBean();
			saPrizeDetailList.add(saPrize);
		}
		return saPrizeDetailList;
	}

	@Override
	public SaAssessIndexResBean querySaAssessIndexInfo(SaPrizeReqBean saPrizeReqBean) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("terminalCode", null);
		paramsMap.put("accountId", null);
		FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
		String sql = SqlUtil.get("SaPrizeRepositoryImpl.querySaAssessIndexDetailInfo", freeMakerContext);
		return this.findSingleBySql(sql, paramsMap, SaAssessIndexResBean.class);
	}

	@Override
	public SaAssessIndexResBean querySaAssessIndexInfo(SaPrizeDetailReqBean saPrizeDetailReqBean) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("salesMonth", saPrizeDetailReqBean.getMonth());
		paramsMap.put("accountNo", saPrizeDetailReqBean.getAccount());
		FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
		String sql = SqlUtil.get("SaPrizeRepositoryImpl.querySaAssessIndexDetailInfo", freeMakerContext);
		SaAssessIndexResBean saAssessIndexResBean = this.findSingleBySql(sql, paramsMap, SaAssessIndexResBean.class);
		return saAssessIndexResBean == null ? new SaAssessIndexResBean() : saAssessIndexResBean;
	}

	@Override
	public boolean isSaConsultant(SaPrizeDetailReqBean saPrizeDetailReqBean) {
		FreeMakerContext freeMakerContext = new FreeMakerContext();
		String sql = SqlUtil.get("SaPrizeRepositoryImpl.isSaConsultant", freeMakerContext);
		Query query = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
		query.setParameter("accountId", saPrizeDetailReqBean.getAccountId());
		Integer result = Integer.parseInt(query.uniqueResult().toString());
		return result > 0 ? true : false;
	}

	@Override
	public SaAssessIndexResBean queryIntegralNewCustInfo(SaPrizeReqBean saPrizeReqBean) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("terminalCode", null);
		paramsMap.put("accountId", null);
		FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
		String sql = SqlUtil.get("SaPrizeRepositoryImpl.queryIntegralNewCustInfo", freeMakerContext);
		return this.findSingleBySql(sql, paramsMap, SaAssessIndexResBean.class);
	}
}
