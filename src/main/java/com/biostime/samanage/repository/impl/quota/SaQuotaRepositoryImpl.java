package com.biostime.samanage.repository.impl.quota;

import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.quota.DoubleSaQuotaBean;
import com.biostime.samanage.bean.quota.SaQuotaTerminalBean;
import com.biostime.samanage.repository.quota.SaQuotaRepository;
import com.biostime.samanage.util.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

@Repository("saQuotaRepository")
public class SaQuotaRepositoryImpl extends BaseOracleRepositoryImpl implements SaQuotaRepository {
    @Override
    public SaQuotaTerminalBean getTerminalBySaCode(String saCode) {
        if (StringUtil.isNullOrEmpty(saCode)) {
            return null;
        }

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        paramsMap.put("saCode", saCode);


        FreeMakerContext freeMakerContext1 = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get("SaQuotaRepositoryImpl.getTerminalBySaCode", freeMakerContext1);
        List<SaQuotaTerminalBean> saQuotaTerminalBeans = this.findAllForListBySql(sqls, paramsMap, SaQuotaTerminalBean.class);
        if (CollectionUtils.isNotEmpty(saQuotaTerminalBeans)) {
            return saQuotaTerminalBeans.get(0);
        }
        return null;
    }

    @Override
    public DoubleSaQuotaBean getDoubleSaQuota(String saCode, String terminalCode, String startTime, String endTime, String chainCode) throws ParseException {
        if (StringUtil.isNullOrEmpty(terminalCode)) {
            return null;
        }
        DoubleSaQuotaBean doubleSaQuotaBean = new DoubleSaQuotaBean();
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        paramsMap.put("terminalCode", terminalCode);
        paramsMap.put("saCode", saCode);
        paramsMap.put("chainCode", chainCode);

        if (StringUtil.isNotNullNorEmpty(startTime)) {
            paramsMap.put("startTime", DateUtils.getTimesmorning(sdf.parse(startTime)));
        }
        if (StringUtil.isNotNullNorEmpty(endTime)) {
            paramsMap.put("endTime", DateUtils.getTimesnight(sdf.parse(endTime)));
        }
        FreeMakerContext freeMakerContext1 = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get("SaQuotaRepositoryImpl.getDoubleSaQuota", freeMakerContext1);
        List<DoubleSaQuotaBean> doubleSaQuotaBeans = this.findAllForListBySql(sqls, paramsMap, DoubleSaQuotaBean.class);
        if (CollectionUtils.isNotEmpty(doubleSaQuotaBeans)) {
            doubleSaQuotaBean = doubleSaQuotaBeans.get(0);
        }

        if (StringUtil.isNotNullNorEmpty(chainCode)) {
            String sqlc = SqlUtil.get("SaQuotaRepositoryImpl.getDoubleSaQuotaChain", freeMakerContext1);
            List<DoubleSaQuotaBean> doubleSaQuotaBeanList = this.findAllForListBySql(sqlc, paramsMap, DoubleSaQuotaBean.class);
            if (CollectionUtils.isNotEmpty(doubleSaQuotaBeans)) {
                doubleSaQuotaBean.setChainTotalCust(doubleSaQuotaBeanList.get(0).getChainTotalCust());
                doubleSaQuotaBean.setChainTotalPoint(doubleSaQuotaBeanList.get(0).getChainTotalPoint());
            }
        }
        return doubleSaQuotaBean;
    }
}
