package com.biostime.samanage.repository.impl.saPoint;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.saPoint.SaSalePointsBean;
import com.biostime.samanage.repository.saPoint.SaSalePointsRepository;
import com.biostime.samanage.util.DateUtils;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * SA销售积分
 */
@Repository("saSalePointsRepository")
public class SaSalePointsRepositoryImpl extends BaseOracleRepositoryImpl implements SaSalePointsRepository {

    /**
     * SA销售积分分页查询
     *
     * @param pager
     * @param saSalePointsBean
     * @return
     * @throws ParseException
     */
    @Override
    public Pager<SaSalePointsBean> searchSaSalePoints(Pager<SaSalePointsBean> pager, SaSalePointsBean saSalePointsBean) throws ParseException {

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String sqlStr = "SaSalePointsRepositoryImpl.searchSaSalePoints.list";
        if (null != saSalePointsBean) {
            if (StringUtil.isNotNullNorEmpty(saSalePointsBean.getBabyConsultant())) {
                paramsMap.put("babyConsultant", saSalePointsBean.getBabyConsultant());
            }
            if (StringUtil.isNotNullNorEmpty(saSalePointsBean.getAreaCode())) {
                paramsMap.put("areaCode", saSalePointsBean.getAreaCode());
            }
            if (StringUtil.isNotNullNorEmpty(saSalePointsBean.getOfficeCode())) {
                paramsMap.put("officeCode", saSalePointsBean.getOfficeCode());
            }
            if (StringUtil.isNotNullNorEmpty(saSalePointsBean.getBccCode())) {
                paramsMap.put("bccCode", saSalePointsBean.getBccCode());
            }
            if (StringUtil.isNotNullNorEmpty(saSalePointsBean.getBuCode())) {
                paramsMap.put("buCode", saSalePointsBean.getBuCode());
            }
            if (null != saSalePointsBean.getStartDate()) {
                Calendar now = Calendar.getInstance();
                int day = now.get(Calendar.DAY_OF_MONTH);
                //当前日期大于25号
                int tempDay = DateUtils.getTempDay(sdf, now, day);

                Date d = sdf1.parse(saSalePointsBean.getStartDate());
                int startDay = Integer.parseInt(sdf.format(d));
                /**
                 * 如果开始时间小于目标时间，则查询历史明细表
                 */
                if (startDay < tempDay) {
                    sqlStr = "SaSalePointsRepositoryImpl.searchSaSalePoints.history.list";
                }
                paramsMap.put("startTime", DateUtils.getTimesmorning(sdf1.parse(saSalePointsBean.getStartDate())));
            }
            if (null != saSalePointsBean.getEndDate()) {
                paramsMap.put("endTime", DateUtils.getTimesnight(sdf1.parse(saSalePointsBean.getEndDate())));

            }
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        String sql = SqlUtil.get(sqlStr, freeMakerContext);

        String countSql = "select count(1) from ( " + sql + " )";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaSalePointsBean.class);

    }

    /**
     * 根据条件搜索
     *
     * @param babyConsultant
     * @param areaCode
     * @param officeCode
     * @param startDate
     * @param endDate
     * @param bccCode
     * @return
     * @throws ParseException
     */

    @Override
    public List<SaSalePointsBean> getSaSalePoints(String babyConsultant, String areaCode, String officeCode, String startDate, String endDate, String bccCode,String buCode) throws ParseException {

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String sqlStr = "SaSalePointsRepositoryImpl.searchSaSalePoints.list";
        if (StringUtil.isNotNullNorEmpty(babyConsultant)) {
            paramsMap.put("babyConsultant", babyConsultant);
        }
        if (StringUtil.isNotNullNorEmpty(areaCode)) {
            paramsMap.put("areaCode", areaCode);
        }
        if (StringUtil.isNotNullNorEmpty(officeCode)) {
            paramsMap.put("officeCode", officeCode);
        }
        if (StringUtil.isNotNullNorEmpty(bccCode)) {
            paramsMap.put("bccCode", bccCode);
        }
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode);
        }

        if (StringUtil.isNotNullNorEmpty(startDate)) {
            Date s = sdf1.parse(startDate);
            Calendar now = Calendar.getInstance();
            int day = now.get(Calendar.DAY_OF_MONTH);
            //当前日期大于25号
            int tempDay = DateUtils.getTempDay(sdf, now, day);
            int startDay = Integer.parseInt(sdf.format(s));
            /**
             * 如果开始时间小于目标时间，则查询历史明细表
             */
            if (startDay < tempDay) {
                sqlStr = "SaSalePointsRepositoryImpl.searchSaSalePoints.history.list";
            }
            paramsMap.put("startTime", DateUtils.getTimesmorning(s));
        }
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            Date e = sdf1.parse(endDate);
            paramsMap.put("endTime", DateUtils.getTimesnight(e));
        }
        FreeMakerContext freeMakerContext1 = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get(sqlStr, freeMakerContext1);
        List<SaSalePointsBean> saSalePointsBeanList = this.findAllForListBySql(sqls, paramsMap, SaSalePointsBean.class);
        return saSalePointsBeanList;
    }

}
