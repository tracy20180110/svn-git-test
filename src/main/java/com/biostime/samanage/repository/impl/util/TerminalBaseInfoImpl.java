package com.biostime.samanage.repository.impl.util;

import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * Describe:门店基本信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository
public class TerminalBaseInfoImpl extends BaseOracleRepositoryImpl {
    /**
     * 获取门店大区办事编码
     *
     * @param terminalCode
     * @return
     */
    public Map getTerminalAreaOfficeCode(String terminalCode) {

        String sql = "select t.department_code departmentCode,t.channel_code channelCode " +
                " from mama100_owner.common_department_terminal t " +
                " where 1=1" +
                " and t.terminal_code = :terminalCode";
        Query query = super.createSQLQuery(sql);
        query.setParameter("terminalCode", terminalCode.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return (Map)query.uniqueResult();

    }
}
