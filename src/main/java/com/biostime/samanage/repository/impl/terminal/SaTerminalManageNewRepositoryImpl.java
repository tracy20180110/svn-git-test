package com.biostime.samanage.repository.impl.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.terminal.SaTerminalManageEditReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewResBean;
import com.biostime.samanage.domain.terminal.SaTerminalManage;
import com.biostime.samanage.domain.terminal.SaTerminalManageNew;
import com.biostime.samanage.repository.terminal.SaTerminalManageNewRepository;
import com.biostime.samanage.util.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Describe:SA与门店管理RepositoryImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Repository("saTerminalManageNewRepository")
public class SaTerminalManageNewRepositoryImpl extends BaseOracleRepositoryImpl implements SaTerminalManageNewRepository {

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryManageParamsMap(SaTerminalManageNewReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //sa编码
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }

        //门店编码
        String terminalCode = req.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode.trim());
        }

        //关系状态
        String statusCode = req.getStatusCode();
        if (StringUtil.isNotNullNorEmpty(statusCode)) {
            paramsMap.put("statusCode", statusCode.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        return paramsMap;
    }

    /**
     * SA与门店管理列表（新）
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaTerminalManageNewResBean> querySaTerminalManageNewList(Pager<SaTerminalManageNewResBean> pager, SaTerminalManageNewReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryManageParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalManageRepositoryImpl.querySaTerminalManageNewList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaTerminalManageNewResBean.class);
    }

    /**
     * 查询门店渠道、状态
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<SaTerminalManageNewResBean> queryTerminalInfo(SaTerminalManageNewReqBean reqBean) {
        ServiceBean<SaTerminalManageNewResBean> serviceBean = new ServiceBean<SaTerminalManageNewResBean>();
        String saCode = reqBean.getSaCode();
        String terminalCode = reqBean.getTerminalCode();
        int type = 2; //1 Sa编码 2：门店编码
        if(StringUtils.isNotEmpty(saCode) && StringUtils.isEmpty(terminalCode)){
            terminalCode = saCode;
            type = 1;
        }
        SaTerminalManageNewResBean bean = new SaTerminalManageNewResBean();
        Map map = queryTerminal(terminalCode);
        if(null == map){
            serviceBean.setCode(502);
            if(1==type) {
                serviceBean.setDesc("SA编号不存在");
            }else {
                serviceBean.setDesc("绑定门店编号不存在");
            }

            return serviceBean;
        }
        String terminalName = (String)map.get("terminalName");
        String statusCode = (String)map.get("statusCode");
        String channelCode = (String)map.get("channelCode");
         //SA
        if(!"01".equals(statusCode)){
            serviceBean.setCode(501);
            if(1==type) {
                serviceBean.setDesc("SA编号非正常状态");
            }else {
                serviceBean.setDesc("绑定门店编号非正常状态");
            }
            return serviceBean;
        }

        if(!"08".equals(channelCode) && 1==type) {
            serviceBean.setCode(502);
            serviceBean.setDesc("输入的SA编号非SA渠道");
            return serviceBean;
        }
        if("08".equals(channelCode) && 2==type){
            serviceBean.setCode(502);
            serviceBean.setDesc("输入的门店编号不是婴线、商超、药线渠道");
            return serviceBean;
        }

        //查询SA门店是否绑定超过3家=
        int count = querySaBindCount(saCode,1,"");
            if(count>2 && 1 == type){
            serviceBean.setCode(503);
            serviceBean.setDesc("该SA绑定门店已超3个，请解除后再试！");
            return serviceBean;
        }

        if(1==type){
            bean.setSaName(terminalName);
        }else {
            bean.setTerminalName(terminalName);
        }
        serviceBean.setResponse(bean);
        return serviceBean;
    }

    /**
     * 查询门店渠道、状态
     * @param code
     * @return
     */
    public Map queryTerminal(String code){
        String sql =    "select t.short_name            terminalName,\n" +
                "       t.status_code           statusCode,\n" +
                "       t.terminal_channel_code channelCode\n" +
                "  from mama100_owner.crm_terminal t, mama100_owner.crm_terminal_channel ctc\n" +
                " where 1 = 1\n" +
                "   and t.terminal_channel_code = ctc.code\n" +
                "   and t.terminal_channel_code in (01, 02, 03, 08) \n"+
                "   and t.code = :code";
        Query query = super.createSQLQuery(sql).addScalar("terminalName", StringType.INSTANCE).addScalar("statusCode", StringType.INSTANCE).addScalar("channelCode", StringType.INSTANCE);
        query.setParameter("code", code.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        return (Map)query.uniqueResult();
    }

    /**
     * 查询SA绑定的数量（有效或未生效状态记录大于等于3的不允许绑定）
     * @param code
     * @return
     */
    public int querySaBindCount(String code,int type,String saCode){
        StringBuffer sb =    new StringBuffer("select count(1) nums\n" +
                        "  from (select mstm.sa_code saCode,\n" +
                        "               case\n" +
                        "                 when mstm.start_date <= trunc(sysdate) and\n" +
                        "                      (mstm.end_date >= trunc(sysdate) or\n" +
                        "                      mstm.end_date is null) then\n" +
                        "                  '2' /*进行中*/\n" +
                        "                 when mstm.start_date > trunc(sysdate) then\n" +
                        "                  '1' /*未开始*/\n" +
                        "                 when mstm.end_date < trunc(sysdate) then\n" +
                        "                  '3'\n" +
                        "               end as statusCode\n" +
                        "          from mama100_owner.mkt_sa_terminal_manage_new mstm\n" +
                        "         where 1 = 1\n" );
        if(1==type) {
            sb.append("           and mstm.sa_code = '" + code + "') t\n");
        }else{
            sb.append("           and mstm.sa_code = '" + saCode + "' \n");
            sb.append("           and mstm.terminal_code = '" + code + "') t\n");
        }
        sb.append(               " where 1 = 1\n" );
        sb.append(                "   and t.statusCode in (1, 2)");

        return this.getCountBySql(sb.toString());
    }

    /**
     * 查询SA与门店是否在同一个办事处
     * @param saCode
     * @param terminalCode
     * @return
     */
    public int querySaTerminalDepartment(String saCode,String terminalCode){
        String sql =
                "select count(1) nums from mama100_owner.common_department_terminal saCt,\n" +
                        "              mama100_owner.common_department_terminal terCt\n" +
                        "where 1=1\n" +
                        "and saCt.Terminal_Code = '"+saCode+"'\n" +
                        "and terCt.Terminal_Code = '"+terminalCode+"'\n" +
                        "and saCt.Department_Code = terCt.Department_Code";

        return this.getCountBySql(sql);
    }
    /**
     * SA与门店管理列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaTerminalManageNewList(HttpServletResponse response, SaTerminalManageNewReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryManageParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalManageRepositoryImpl.querySaTerminalManageNewList.list", freeMakerContext);
        List<SaTerminalManageNewResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaTerminalManageNewResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "SA与门店管理";

        th = new String[]{"大区","办事处","SA编码","SA名称","关联门店编码","关联门店名称","有效关系","开始时间","结束时间","操作人工号","操作人名称"};

        colName = new String[] {"areaName","officeName","saCode","saName","terminalCode","terminalName","statusName","startDate","endDate","createdBy","createdName"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * SA与门店绑定（新）
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSaTerminalRelationNew(SaTerminalManageNewReqBean reqBean) throws Exception {
        ServiceBean serviceBean = new ServiceBean();
        try {
            SaTerminalManageNew saTerminalManageNew = new SaTerminalManageNew();
            String id = reqBean.getId();
            String saCode = reqBean.getSaCode();
            String terminalCode = reqBean.getTerminalCode();
            String startDate = reqBean.getStartDate();
            String endDate = reqBean.getEndDate();
            if (StringUtils.isNotBlank(id)) {
                saTerminalManageNew = super.get(SaTerminalManageNew.class, Long.valueOf(reqBean.getId()));
                saTerminalManageNew.setUpdatedBy(reqBean.getCreatedBy());
                saTerminalManageNew.setUpdatedName(reqBean.getCreatedName());
                saTerminalManageNew.setUpdatedTime(new Date());
            } else {

                saTerminalManageNew.setCreatedBy(reqBean.getCreatedBy());
                saTerminalManageNew.setCreatedName(reqBean.getCreatedName());
                saTerminalManageNew.setCreatedTime(new Date());
                saTerminalManageNew.setSaCode(saCode);
                saTerminalManageNew.setTerminalCode(terminalCode);
            }
            saTerminalManageNew.setStartDate(DateUtils.stringToDate(startDate, "yyyy-MM-dd"));
            if (StringUtils.isNotBlank(endDate)) {
                saTerminalManageNew.setEndDate(DateUtils.stringToDate(endDate, "yyyy-MM-dd"));
            }
            if (StringUtils.isNotBlank(id)) {
                super.getHibernateTemplate().update(saTerminalManageNew);
            } else {
                //产品要求取消 2019-12-20 叶俊豪
                /*int count = querySaTerminalDepartment(saCode,terminalCode);
                if(count == 0){
                    serviceBean.setCode(505);
                    serviceBean.setDesc("SA与门店必须在同一个办事处");
                    return serviceBean;
                }*/

                int bindCount = querySaBindCount(terminalCode,2,saCode);
                if(bindCount > 0 ){
                    serviceBean.setCode(503);
                    serviceBean.setDesc("该门店已绑定，勿重复！");
                    return serviceBean;
                }

                super.save(saTerminalManageNew, SaTerminalManage.KEY, "MAMA100_OWNER");
            }
        }catch (Exception e){
            serviceBean.setCode(500);
            serviceBean.setDesc("绑定失败");
        }
        return serviceBean;
    }

    /**
     * SA与门店管理-编辑
     * @param reqBean
     * @return
     */
    @Override
    public String editSaTerminalManage(SaTerminalManageEditReqBean reqBean) {
        return null;
    }


}
