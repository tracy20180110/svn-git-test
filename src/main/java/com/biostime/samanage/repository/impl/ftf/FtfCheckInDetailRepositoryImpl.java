package com.biostime.samanage.repository.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInAppGroupDetailResBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInAppIndividualDetailResBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInDetailReqBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInDetailResBean;
import com.biostime.samanage.repository.ftf.FtfCheckInDetailRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA. Describe:FTF活动签到信息 Date: 2016-11-23 Time: 11:37
 * User: 12804 Version:1.0
 */
@Repository("ftfCheckInDetailRepository")
public class FtfCheckInDetailRepositoryImpl extends BaseOracleRepositoryImpl implements FtfCheckInDetailRepository {

	/**
	 * 查询、导出条件
	 * 
	 * @param req
	 * @return
	 */
	public HashMap<String, Object> queryParamsMap(FtfCheckInDetailReqBean req) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();

		// 大区办事处
		String areaOfficeCode = req.getAreaOfficeCode();
		if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
			paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
		}

		// 课程名称
		String ckName = req.getCkName();
		if (StringUtil.isNotNullNorEmpty(ckName)) {
			paramsMap.put("ckName", ckName.trim());
		}

		// SA编码
		String saCode = req.getSaCode();
		if (StringUtil.isNotNullNorEmpty(saCode)) {
			paramsMap.put("saCode", saCode.trim());
		}
		// 手机号码
		String mobile = req.getMobile();
		if (StringUtil.isNotNullNorEmpty(mobile)) {
			paramsMap.put("mobile", mobile.trim());
		}
		// 会员ID
		String customerId = req.getCustomerId();
		if (StringUtil.isNotNullNorEmpty(customerId)) {
			paramsMap.put("customerId", customerId.trim());
		}

		// 活动开始日期
		String startDate = req.getStartDate();
		if (StringUtil.isNotNullNorEmpty(startDate)) {
			paramsMap.put("startDate", startDate.trim());
		}

		// 活动结束日期
		String endDate = req.getEndDate();
		if (StringUtil.isNotNullNorEmpty(endDate)) {
			paramsMap.put("endDate", endDate.trim());
		}

		//事业部编码
		String buCode = req.getBuCode();
		if (StringUtil.isNotNullNorEmpty(buCode)) {
			paramsMap.put("buCode", buCode.trim());
		}

		return paramsMap;
	}

	/**
	 * FTF活动签到信息
	 * 
	 * @param pager
	 * @param reqBean
	 * @return
	 */
	@Override
	public Pager<FtfCheckInDetailResBean> queryFtfCheckInDetailList(Pager<FtfCheckInDetailResBean> pager,
			FtfCheckInDetailReqBean reqBean) {
		DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());// 手动切换成只读数据源
		HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
		FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
		String sql = SqlUtil.get("FtfCheckInDetailRepositoryImpl.queryFtfCheckInDetailList", freeMakerContext);
		String countSql = "select count(*) from (" + sql + ")";

		PagerQuery pagerQuery = new PagerQuery();
		pagerQuery.setSearchSql(sql);
		pagerQuery.setCountSql(countSql);
		pagerQuery.setParamsMap(paramsMap);

		return this.pageDataBySql(pagerQuery, pager, FtfCheckInDetailResBean.class);
	}

	/**
	 * FTF个人签到信息查询
	 */
	@Override
	public List<FtfCheckInAppIndividualDetailResBean> queryFtfCheckInAppIndividualDetail(
			FtfCheckInDetailReqBean reqBean) {

		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		// 大区办事处
		String ftfActId = reqBean.getFtfActId();
		if (StringUtil.isNotNullNorEmpty(ftfActId)) {
			paramsMap.put("ftfActId", ftfActId.trim());
		}
		FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
		String sql = SqlUtil.get("FtfCheckInDetailRepositoryImpl.queryFtfCheckInAppIndividualDetail", freeMakerContext);
		List<FtfCheckInAppIndividualDetailResBean> individualDetailResBeans = this.findAllForListBySql(sql, paramsMap,
				FtfCheckInAppIndividualDetailResBean.class);
		return individualDetailResBeans;
	}

	/**
	 * FTF组团信息查询
	 */
	@Override
	public List<FtfCheckInAppGroupDetailResBean> queryFtfCheckInAppGroupDetail(FtfCheckInDetailReqBean reqBean) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		// 活动Id
		String ftfActId = reqBean.getFtfActId();
		if (StringUtil.isNotNullNorEmpty(ftfActId)) {
			paramsMap.put("ftfActId", ftfActId.trim());
		}
		String mobile = reqBean.getMobile();
		if (StringUtil.isNotNullNorEmpty(mobile)) {
			paramsMap.put("mobile", mobile.trim());
		}
		FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
		String sql = null;
		if(StringUtil.isNotNullNorEmpty(mobile)){
			// 针对手机号码查询单个团
			sql = SqlUtil.get("FtfCheckInDetailRepositoryImpl.queryFtfCheckInAppGroupDetailSingle", freeMakerContext);
		} else{
			// 查询全部团
			sql = SqlUtil.get("FtfCheckInDetailRepositoryImpl.queryFtfCheckInAppGroupDetailAll", freeMakerContext);
		}
		System.out.println("Query SQL:" +sql);
		List<FtfCheckInAppGroupDetailResBean> groupDetailResBeans = this.findAllForListBySql(sql, paramsMap,
				FtfCheckInAppGroupDetailResBean.class);
		return groupDetailResBeans;
	}

	/**
	 * FTF活动签到信息导出
	 *
	 * @param response
	 * @param reqBean
	 * @return
	 */
	@Override
	public void exportFtfCheckInDetailList(HttpServletResponse response, FtfCheckInDetailReqBean reqBean)
			throws Exception {
		DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());// 手动切换成只读数据源
		HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
		FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
		String sql = SqlUtil.get("FtfCheckInDetailRepositoryImpl.queryFtfCheckInDetailList", freeMakerContext);
		List<FtfCheckInDetailResBean> bscarList = this.findAllForListBySql(sql, paramsMap,
				FtfCheckInDetailResBean.class);

		String titleName[] = null;
		Integer titleRange[][] = null;
		String th[] = null;
		String colName[] = null;

		String report_name = "FTF签到明细";

		th = new String[] { "大区", "办事处", "课程名称", "SA编码", "会员ID", "手机号码", "签到时间", "最近接触门店编码", "系统来源", "签到来源", "活动类型" };

		colName = new String[] { "areaName", "officeName", "kcName", "saCode", "customerId", "mobile", "singInTime",
				"terminalCode", "formSysSourse", "signInSourse", "type" };

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition",
				"attachment; filename=" + URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

		ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

	}

	@Override
	public void receiveFtfCheckInAppPrizes(FtfCheckInDetailReqBean reqBean) {
		String ftfActId = reqBean.getFtfActId();
		String groupId = reqBean.getGroupId();
		FreeMakerContext freeMakerContext = new FreeMakerContext();
		String sql = SqlUtil.get("FtfCheckInDetailRepositoryImpl.receiveFtfCheckInAppPrizes", freeMakerContext);
		Query query = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
		query.setParameter("ftfActId", ftfActId);
		query.setParameter("groupId", groupId);
		query.executeUpdate();
	}

	@Override
	public String getReceiveStatus(String ftfActId, String groupId) {
		FreeMakerContext freeMakerContext = new FreeMakerContext();
		String sql = SqlUtil.get("FtfCheckInDetailRepositoryImpl.getReceiveStatus", freeMakerContext);
		Query query = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
		query.setParameter("ftfActId", ftfActId);
		query.setParameter("groupId", groupId);
		List<java.lang.Integer> list = query.list();
		return (list == null || list.size() == 0) ? null : String.valueOf(list.get(0));
	}
}
