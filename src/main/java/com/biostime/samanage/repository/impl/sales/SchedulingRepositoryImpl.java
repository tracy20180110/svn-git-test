package com.biostime.samanage.repository.impl.sales;

import com.biostime.common.bean.ServiceBean;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.sales.*;
import com.biostime.samanage.domain.sales.Scheduling;
import com.biostime.samanage.domain.sales.SchedulingTerminal;
import com.biostime.samanage.repository.sales.SchedulingRepository;
import com.biostime.samanage.util.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.*;

/**
 * Describe:工作排班
 * Date: 2019-03-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Service("schedulingRepository")
public class SchedulingRepositoryImpl extends BaseOracleRepositoryImpl implements SchedulingRepository {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * 查询排班详情
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<SchedulingResBean> querySchedulingInfo(SchedulingReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<SchedulingResBean> schedulingList = new ArrayList<>();
        try {
            //创建名称
            String createdBy = reqBean.getCreatedBy();
            if (StringUtil.isNotNullNorEmpty(createdBy)) {
                paramsMap.put("createdBy", createdBy);
            }

            //排班日期
            String schedulingTime = reqBean.getSchedulingTime();
            if (StringUtil.isNotNullNorEmpty(schedulingTime)) {
                paramsMap.put("schedulingTime", schedulingTime);
            }

            //月份
            String month = reqBean.getMonth();
            if (StringUtil.isNotNullNorEmpty(month)) {
                paramsMap.put("month", month);
            }

            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("SchedulingRepositoryImpl.querySchedulingInfo", freeMakerContext);

            System.out.println("----SchedulingRepositoryImpl.querySchedulingInfo SQL----"+sql.toString());
            schedulingList = this.findAllForListBySql(sql, paramsMap,SchedulingResBean.class);
            //把回的list 组装下数据，返回给前端
            if (CollectionUtils.isNotEmpty(schedulingList)) {
                for (SchedulingResBean bean: schedulingList){
                    if(StringUtils.isNotBlank(bean.getId())) {
                        bean.setTerminalInfo(querySchedulingTerminalInfo(bean.getId()));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return schedulingList;
    }

    /**
     * 查询排班门店详情
     *
     * @param schedulingId
     * @return
     */
    public List<BaseKeyValueBean> querySchedulingTerminalInfo(String schedulingId) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<BaseKeyValueBean> schedulingList = new ArrayList<>();
        try {
            //创建名称
            if (StringUtil.isNotNullNorEmpty(schedulingId)) {
                paramsMap.put("schedulingId", schedulingId);
            }

            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("SchedulingRepositoryImpl.querySchedulingTerminalInfo", freeMakerContext);

            System.out.println("----SchedulingRepositoryImpl.querySchedulingTerminalInfo SQL----"+sql.toString());
            schedulingList = this.findAllForListBySql(sql, paramsMap,BaseKeyValueBean.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return schedulingList;
    }

    /**
     * 查询排班管理详情
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<SchedulingManageResBean> querySchedulingManageInfo(SchedulingManageReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<SchedulingManageResBean> schedulingManageResBeanList = new ArrayList<>();
        try {
            //创建名称
            String createdBy = reqBean.getCreatedBy();
            if (StringUtil.isNotNullNorEmpty(createdBy)) {
                paramsMap.put("createdBy", createdBy);
            }

            //大区办事处
            String areaOfficeCode = reqBean.getAreaOfficeCode();
            if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
                paramsMap.put("areaOfficeCode", areaOfficeCode);
            }

            //渠道
            String channelCode = reqBean.getChannelCode();
            if (StringUtil.isNotNullNorEmpty(channelCode)) {
                paramsMap.put("channelCode", channelCode);
            }


            //渠道
            String buCode = reqBean.getBuCode();
            if (StringUtil.isNotNullNorEmpty(buCode)) {
                paramsMap.put("buCode", buCode);
            }

            //登陆人工号 --取消 不需要查下级
          /*  String account = reqBean.getAccount();
            if (StringUtil.isNotNullNorEmpty(account)) {
                paramsMap.put("account", account);
            }*/

            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("SchedulingRepositoryImpl.querySchedulingManageInfo", freeMakerContext);

            System.out.println("----SchedulingRepositoryImpl.querySchedulingManageInfo SQL----"+sql.toString());
            schedulingManageResBeanList = this.findAllForListBySql(sql, paramsMap,SchedulingManageResBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return schedulingManageResBeanList;
    }

    /**
     * 查询人员与终端
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<BaseKeyValueBean> queryAccountTerminal(SchedulingReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<BaseKeyValueBean> baseKeyValueBeanList = new ArrayList<>();
        try {
            //创建名称
            String createdBy = reqBean.getCreatedBy();
            if (StringUtil.isNotNullNorEmpty(createdBy)) {
                paramsMap.put("createdBy", createdBy);
            }

            String terminalCode = reqBean.getTerminalCode();
            if (StringUtil.isNotNullNorEmpty(terminalCode)) {
                paramsMap.put("terminalCode", terminalCode);
            }

            //排班日期
            String schedulingDate = reqBean.getSchedulingDate();
            if (StringUtil.isNotNullNorEmpty(schedulingDate)) {
                paramsMap.put("schedulingDate", schedulingDate);
            }


            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
            String sql = SqlUtil.get("SchedulingRepositoryImpl.queryAccountTerminal", freeMakerContext);

            System.out.println("----SchedulingRepositoryImpl.queryAccountTerminal SQL----"+sql.toString());
            baseKeyValueBeanList = this.findAllForListBySql(sql, paramsMap,BaseKeyValueBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseKeyValueBeanList;
    }

    /**
     * 保存排班
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveScheduling(SchedulingReqBean reqBean) {
        ServiceBean response = new ServiceBean();
        try {

            //同一天、同一个人 排班处理   如果存在记录，更新w
            Scheduling schedulingBean = new Scheduling();
            if(StringUtils.isNotBlank(reqBean.getId())){
                schedulingBean = super.get(Scheduling.class, Long.valueOf(reqBean.getId()));
            }
            schedulingBean.setCreatedBy(reqBean.getCreatedBy());
            schedulingBean.setSchedulingDate(DateUtils.stringToDate(reqBean.getSchedulingDate(),"yyyy-MM-dd"));
            boolean flag = false;
            List<Scheduling> stmList = this.getHibernateTemplate().findByExample(schedulingBean);
            if(CollectionUtils.isNotEmpty(stmList)) {
                flag = true;
            }

            schedulingBean.setStatus(reqBean.getStatus());
            schedulingBean.setMemo(reqBean.getMemo());
            if(flag) {
                schedulingBean.setUpdatedName(reqBean.getCreatedName());
                schedulingBean.setUpdatedTime(new Date());
                schedulingBean.setUpdatedBy(reqBean.getCreatedBy());
                super.getHibernateTemplate().update(schedulingBean);

            }else{
                schedulingBean.setCreatedName(reqBean.getCreatedName());
                schedulingBean.setCreatedTime(new Date());
                super.save(schedulingBean, Scheduling.KEY, "MAMA100_OWNER");
            }

            //先删除、在保存。
            String sql = " delete from mama100_owner.mkt_sa_scheduling_terminal t where 1=1 and t.scheduling_id = '"+schedulingBean.getId()+"' ";
            Query query = super.createSQLQuery(sql);
            query.executeUpdate();

            if(1 == reqBean.getStatus()) {
                List<SchedulingTerminal> schedulingTerminalList = new ArrayList<>();
                //门店拆分后 ，转list,然后在保存
                List<String> list = Arrays.asList(reqBean.getTerminalCode().split(","));

                if (CollectionUtils.isNotEmpty(list)) {
                    for (String terminalCode : list) {
                        SchedulingTerminal schedulingTerminal = new SchedulingTerminal();
                        schedulingTerminal.setSchedulingId(schedulingBean.getId());
                        schedulingTerminal.setTerminalCode(terminalCode);
                        schedulingTerminalList.add(schedulingTerminal);
                    }
                    super.batchAdd(schedulingTerminalList, SchedulingTerminal.KEY, "MAMA100_OWNER");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            response.setCode(500);
            response.setDesc(PropUtils.getProp(response.getCode()));
        }
            return response;
        }

    /**
     * 排班查询列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSchedulingList(HttpServletResponse response, SchedulingManageReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        //创建名称
        String createdBy = reqBean.getCreatedBy();
        if (StringUtil.isNotNullNorEmpty(createdBy)) {
            paramsMap.put("createdBy", createdBy);
        }

        //大区办事处
        String areaOfficeCode = reqBean.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode);
        }

        //大区办事处
        String channelCode = reqBean.getChannelCode();
        if (StringUtil.isNotNullNorEmpty(channelCode)) {
            paramsMap.put("channelCode", channelCode);
        }

        //门店
        String terminalCode = reqBean.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode);
        }

        //开始日期
        String startDate = reqBean.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate);
        }

        //结束日期
        String endDate = reqBean.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate);
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SchedulingRepositoryImpl.exportSchedulingList", freeMakerContext);
        List<SchedulingReportResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SchedulingReportResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "公司员工排班报表";

        th = new String[]{"月份","大区","办事处","渠道","排班人员工号","排班人员名称",
                "1日","2日","3日","4日","5日","6日",
                "7日","8日","9日","10日","11日","12日",
                "13日","14日","15日","16日","17日","18日",
                "19日","20日","21日","22日","23日","24日",
                "25日","26日","27日","28日","29日","30日","31日"
        };

        colName = new String[] {"month","areaName","officeName","channelName","createdBy","createdName",
                "a","b","c","e","f","g",
                "h","i","j","k","l","m",
                "n","o","p","q","r","s",
                "t","u","v","w","x","y",
                "z","aa","bb","cc","dd","ee","ff"
        };

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
}
