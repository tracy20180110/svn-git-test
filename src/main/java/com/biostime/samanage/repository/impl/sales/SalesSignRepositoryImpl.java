package com.biostime.samanage.repository.impl.sales;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.sales.SalesSignMerchantReqBean;
import com.biostime.samanage.bean.sales.SalesSignReqBean;
import com.biostime.samanage.bean.sales.SalesSignResBean;
import com.biostime.samanage.domain.sales.SalesSign;
import com.biostime.samanage.repository.sales.SalesSignRepository;
import com.biostime.samanage.util.LocationUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Describe:促销员打卡RepositoryImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("salesSignRepository")
public class SalesSignRepositoryImpl extends BaseOracleRepositoryImpl implements SalesSignRepository {

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryInfoParamsMap(SalesSignReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //打卡开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //打卡结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //渠道
        String channelCode = req.getChannelCode();
        if (StringUtil.isNotNullNorEmpty(channelCode)) {
            paramsMap.put("channelCode", channelCode.trim());
        }

        //打卡人
        String createdBy = req.getCreatedBy();
        if (StringUtil.isNotNullNorEmpty(createdBy)) {
            paramsMap.put("createdBy", createdBy.trim());
        }
        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }

        return paramsMap;
    }

    /**
     * 促销员打卡查询列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SalesSignResBean> querySalesSignList(Pager<SalesSignResBean> pager, SalesSignReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SalesSignRepositoryImpl.querySalesSignList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SalesSignResBean.class);
    }
    /**
     * 促销员打卡询列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSalesSignList(HttpServletResponse response, SalesSignReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SalesSignRepositoryImpl.querySalesSignList.list", freeMakerContext);
        List<SalesSignResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SalesSignResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "打卡分析报表";

        th = new String[]{"大区","办事处","渠道","打卡人工号","打卡人名称",
                "打卡门店1","门店名称1","上班打卡时间","上班位置一致性","下班打卡时间","下班位置一致性",
                "打卡门店2","门店名称2","上班打卡时间","上班位置一致性","下班打卡时间","下班位置一致性",
                "打卡门店2","门店名称2","上班打卡时间","上班位置一致性","下班打卡时间","下班位置一致性",};

        colName = new String[] {"areaName","officeName","channelName","createdBy","createdName",
                "terminalCodeUp","terminalNameUp","signTimeUp","statusUp","signTimeDown","statusDown",
                "terminalCodeUpTwo","terminalNameUpTwo","signTimeUpTwo","statusUpTwo","signTimeDownTwo","statusDownTwo",
                "terminalCodeUpThree","terminalNameUpThree","signTimeUpThree","statusUpThree","signTimeDownThree","statusDownThree"   };

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * 促销员打卡(手机端)
     * @param reqBean
     * @return
     */
    @Override
    public void saveSalesSign(SalesSignMerchantReqBean reqBean) throws Exception {

        SalesSign salesSign = new SalesSign();

        salesSign.setSignTime(new Date());
        salesSign.setTerminalCode(reqBean.getTerminalCode());
        salesSign.setType(Integer.valueOf(reqBean.getType()));
        salesSign.setLatitude(reqBean.getLatitude());
        salesSign.setLongitude(reqBean.getLongitude());
        salesSign.setStatus(reqBean.getStatus());
        salesSign.setChannelCode(reqBean.getChannelCode());

        salesSign.setDepartmentCode(reqBean.getDepartmentCode());
        salesSign.setType(reqBean.getType());

        salesSign.setCreatedBy(reqBean.getCreatedBy());
        salesSign.setCreatedTime(new Date());
        salesSign.setCreatedName(reqBean.getCreatedName());

        super.save(salesSign, SalesSign.KEY, "MAMA100_OWNER");
    }

    /**
     * 查询是否在指定的门店(手机端)
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean queryIsInTerminal(SalesSignMerchantReqBean reqBean) throws Exception {
        ServiceBean serviceBean = new ServiceBean();

        if(1==reqBean.getType()) {
            //根据当天创建时间 类型 判断是否已有记录
            Map smap = querySignNums(reqBean.getCreatedBy());
            if(null != smap){
                Integer count = (Integer)smap.get("signNums");
                //同一个人同一天最多不同3家门店 总打卡次数6次
                if(count >= 6){
                    serviceBean.setCode(208);
                    serviceBean.setDesc("一天只能打卡6次");
                    return serviceBean;
                }else{

                    //最后门店打卡类型 是否为下班
                    String type = (String)smap.get("signType");
                    if ("2".equals(type)) {
                        serviceBean.setCode(602);
                        serviceBean.setDesc("上班打卡");
                        return serviceBean;
                    }
                    if ("1".equals(type)) {
                        String terminalCode = (String)smap.get("terminalCode");
                        //同一家门店 打下班
                        if(reqBean.getTerminalCode().equals(terminalCode)){
                            serviceBean.setCode(603);
                            serviceBean.setDesc("下班打卡");
                            return serviceBean;
                        }else{
                            //不同门店打上班
                            serviceBean.setCode(209);
                            serviceBean.setDesc("门店"+terminalCode+"下班未打卡");
                            return serviceBean;
                        }
                    }
                }
            }else{
                serviceBean.setCode(602);
                serviceBean.setDesc("上班打卡");
                return serviceBean;
            }
        }else{
            //验证经纬度与打卡门店是否一致

            String sql2 = "select t.longitude longitude,t.latitude latitude from mama100_owner.crm_terminal t " +
                    "where 1=1" +
                    "and t.status_code = '01' "+
                    "and t.code = :terminalCode";
            Query query2 = super.createSQLQuery(sql2);
            query2.setParameter("terminalCode", reqBean.getTerminalCode().trim());
            query2.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            Map map = (Map)query2.uniqueResult();
            if(null == map){
                serviceBean.setCode(202);
                serviceBean.setDesc("门店未设置经纬度，是否继续打卡？");
                return serviceBean;
            }
            if(null == map.get("LONGITUDE") || null == map.get("LATITUDE")){
                serviceBean.setCode(202);
                serviceBean.setDesc("门店未设置经纬度，是否继续打卡？");
                return serviceBean;
            }


            Double longitude = Double.valueOf((String)map.get("LONGITUDE"));
            Double latitude = Double.valueOf((String)map.get("LATITUDE"));
            Double location = LocationUtils.getDistance(latitude, longitude, Double.valueOf(reqBean.getLatitude()), Double.valueOf(reqBean.getLongitude()));
            System.out.println("-------计算当前与门店的距离-------"+location+"米");
            if(location > 1000){  //大于1000米 则提示不在门店范围内
                serviceBean.setCode(202);
                serviceBean.setDesc("你所在的位置与所选门店不一致，是否继续打卡？");
            }
        }
        return serviceBean;

    }



    /**
     * 查询门店信息
     * @param createdBy
     * @return
     */
    public Map querySignNums(String createdBy) throws Exception {
        String sql =
                "select max(s.terminal_code) keep(dense_rank first order by s.id desc) terminalCode,\n" +
                        "       max(s.type) keep(dense_rank first order by s.id desc) signType,\n" +
                        "       count(1) signNums\n" +
                        "\n" +
                        "  from mama100_owner.mkt_sa_sales_sign s\n" +
                        " where 1 = 1\n" +
                        "   and s.created_by = :createdBy\n" +
                        "   and trunc(s.created_time) = trunc(sysdate)\n" +
                        " group by trunc(s.created_time), s.created_by";

        Query query = super.createSQLQuery(sql).addScalar("terminalCode", StringType.INSTANCE).addScalar("signType", StringType.INSTANCE).addScalar("signNums", IntegerType.INSTANCE);
        query.setParameter("createdBy", createdBy.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        return (Map)query.uniqueResult();
    }


}
