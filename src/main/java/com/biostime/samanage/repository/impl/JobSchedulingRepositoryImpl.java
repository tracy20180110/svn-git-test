package com.biostime.samanage.repository.impl;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.SaJobSchedulingBean;
import com.biostime.samanage.bean.SaJobSchedulingSalesAccountBean;
import com.biostime.samanage.domain.SaJobScheduling;
import com.biostime.samanage.domain.SaJobSchedulingStatus;
import com.biostime.samanage.domain.SaTerminal;
import com.biostime.samanage.repository.JobSchedulingRepository;
import com.biostime.samanage.util.DateUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 类功能描述: SA工作排班
 *
 * @author zhuhaitao
 * @version 1.0
 * @createDate 2016年6月23日 上午11:25:23
 */
@Repository("jobSchedulingRepository")
public class JobSchedulingRepositoryImpl extends BaseOracleRepositoryImpl implements JobSchedulingRepository {

    /**
     * 根据促销员编号和日期，查询当天排班
     */
    @Override
    public List<SaJobScheduling> getJobScheduling(String salesAccountNo, Date startTime, Date endTime) {
        Query query = this
                .createQuery("from SaJobScheduling po where po.salesAccountNo=:salesAccountNo and po.companyDate>=:startTime and po.companyDate<=:endTime");
        query.setParameter("salesAccountNo", Long.parseLong(salesAccountNo));
        query.setParameter("startTime", startTime);
        query.setParameter("endTime", endTime);
        List<SaJobScheduling> saIobSchedulingList = query.list();
        return saIobSchedulingList;
    }

    /**
     * 根据促销员编号和日期，删除当天排班数据
     */
    @Override
    public void deleteByCustomerIdAndCompanyDate(String salesAccountNo, Date companyDate) {
        String hql = "DELETE from SaJobScheduling po where po.salesAccountNo=:salesAccountNo and po.companyDate=:companyDate";
        Query query = this.createQuery(hql);
        query.setParameter("salesAccountNo", Long.parseLong(salesAccountNo));
        query.setParameter("companyDate", companyDate);
        query.executeUpdate();

    }

    /**
     * 保存工作排班
     */
    @Override
    public void saveJobScheduling(SaJobScheduling saJobScheduling) throws IllegalAccessException {
        super.save(saJobScheduling, SaJobScheduling.KEY);
    }

    /**
     * 根据办事处编号查询促销员排班列表
     */
    @Override
    public List<SaJobSchedulingSalesAccountBean> queryJobScheduling(String officeCode, Date startTime, Date endTime,String buCode) {

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        paramsMap.put("officeCode", officeCode);
        paramsMap.put("startTime", startTime);
        paramsMap.put("endTime", endTime);
        paramsMap.put("buCode", buCode);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get("JobSchedulingRepositoryImpl.queryJobScheduling", freeMakerContext);
        List<SaJobSchedulingSalesAccountBean> saList = this.findAllForListBySql(sqls, paramsMap,
                SaJobSchedulingSalesAccountBean.class);
        return saList;
    }

    /**
     * 根据SA编号查询SA名称
     */
    @Override
    public SaTerminal getSaNameBySaCode(String saCode) {
        Query query = this
                .createQuery("from SaTerminal po where po.code=:code and po.terminalChannelCode=:terminalChannelCode");
        query.setParameter("code", saCode);
        query.setParameter("terminalChannelCode", "08");
        SaTerminal sa = (SaTerminal) query.uniqueResult();
        return sa;
    }

    /**
     * 根据促销员编号查询当月排班状态信息
     */
    @Override
    public List<SaJobSchedulingStatus> getJobSchedulingStatus(String salesAccountNo, Date startTime, Date endTime) {
        Query query = this
                .createQuery("from SaJobSchedulingStatus po where po.salesAccountNo=:salesAccountNo and po.companyDate>=:startTime and po.companyDate<=:endTime");
        query.setParameter("salesAccountNo", Long.parseLong(salesAccountNo));
        query.setParameter("startTime", startTime);
        query.setParameter("endTime", endTime);
        List<SaJobSchedulingStatus> saIobSchedulingList = query.list();
        return saIobSchedulingList;
    }

    /**
     * 保存促销员排班状态信息
     */
    @Override
    public void saveJobSchedulingStatus(SaJobSchedulingStatus saJobSchedulingStatus) throws IllegalAccessException {
        if (null != saJobSchedulingStatus.getId())
            super.save(saJobSchedulingStatus);
        else
            super.save(saJobSchedulingStatus, SaJobSchedulingStatus.KEY);

    }

    /**
     * @param pager
     * @param saJobSchedulingBean
     * @return
     */
    @Override
    public Pager<SaJobSchedulingBean> searchJobScheduling(Pager<SaJobSchedulingBean> pager, SaJobSchedulingBean saJobSchedulingBean) throws ParseException {

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        if (null != saJobSchedulingBean) {
            if (null != saJobSchedulingBean.getSalesAccountNo()) {
                paramsMap.put("salesAccountNo", saJobSchedulingBean.getSalesAccountNo().toString());
            }
            if (StringUtil.isNotNullNorEmpty(saJobSchedulingBean.getAreaCode())) {
                paramsMap.put("areaCode", saJobSchedulingBean.getAreaCode());
            }
            if (StringUtil.isNotNullNorEmpty(saJobSchedulingBean.getOfficeCode())) {
                paramsMap.put("officeCode", saJobSchedulingBean.getOfficeCode());
            }
            if (StringUtil.isNotNullNorEmpty(saJobSchedulingBean.getStartMonth())) {
                Date startMonth = DateUtils.getTimesmorning(new SimpleDateFormat("yyyy-MM-dd").parse(saJobSchedulingBean.getStartMonth()));
                paramsMap.put("startMonth", startMonth);
            }
            if (StringUtil.isNotNullNorEmpty(saJobSchedulingBean.getEndMonth())) {
                Date endMonth = DateUtils.getTimesnight(new SimpleDateFormat("yyyy-MM-dd").parse(saJobSchedulingBean.getEndMonth()));
                paramsMap.put("endMonth", endMonth);
            }
            if (StringUtil.isNotNullNorEmpty(saJobSchedulingBean.getBuCode())) {
                paramsMap.put("buCode", saJobSchedulingBean.getBuCode());
            }
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        String sql = SqlUtil.get("JobSchedulingRepositoryImpl.searchJobScheduling.list", freeMakerContext);

        String countSql = SqlUtil.get("JobSchedulingRepositoryImpl.searchJobScheduling.count", freeMakerContext);

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaJobSchedulingBean.class);

    }

    /**
     * @param salesAccountNo
     * @param areaCode
     * @param officeCode
     * @param startMonth
     * @param endMonth
     * @return
     * @throws ParseException
     */

    @Override
    public List<SaJobSchedulingBean> getJobScheduling(String salesAccountNo, String areaCode, String officeCode, String startMonth, String endMonth,String buCode) throws ParseException {

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        if (StringUtil.isNotNullNorEmpty(salesAccountNo)) {
            paramsMap.put("salesAccountNo", salesAccountNo);
        }
        if (StringUtil.isNotNullNorEmpty(areaCode)) {
            paramsMap.put("areaCode", areaCode);
        }
        if (StringUtil.isNotNullNorEmpty(officeCode)) {
            paramsMap.put("officeCode", officeCode);
        }
        if (StringUtil.isNotNullNorEmpty(startMonth)) {
            Date startTime = DateUtils.getTimesmorning(new SimpleDateFormat("yyyy-MM-dd").parse(startMonth));
            paramsMap.put("startMonth", startTime);
        }
        if (StringUtil.isNotNullNorEmpty(endMonth)) {
            Date endTime = DateUtils.getTimesnight(new SimpleDateFormat("yyyy-MM-dd").parse(endMonth));
            paramsMap.put("endMonth", endTime);
        }
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode);
        }
        FreeMakerContext freeMakerContext1 = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get("JobSchedulingRepositoryImpl.searchJobScheduling.list", freeMakerContext1);
        List<SaJobSchedulingBean> saJobSchedulingBeanList = this.findAllForListBySql(sqls, paramsMap, SaJobSchedulingBean.class);
        return saJobSchedulingBeanList;
    }
}
