package com.biostime.samanage.repository.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.single.adset.SaAdSetExternalResBean;
import com.biostime.samanage.bean.single.adset.SaAdSetReqBean;
import com.biostime.samanage.bean.single.adset.SaAdSetResBean;
import com.biostime.samanage.domain.adset.SaAdSet;
import com.biostime.samanage.repository.single.SaAdSetRepository;
import com.biostime.samanage.util.DateUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Describe:SA广告图设置RepositoryImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("saAdSetRepository")
public class SaAdSetRepositoryImpl extends BaseOracleRepositoryImpl implements SaAdSetRepository {

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryInfoParamsMap(SaAdSetReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //打卡开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //打卡结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //渠道
        String channelCode = req.getChannelCode();
        if (StringUtil.isNotNullNorEmpty(channelCode)) {
            paramsMap.put("channelCode", channelCode.trim());
        }

        //广告类型
        String type = String.valueOf(req.getType());
        if (StringUtil.isNotNullNorEmpty(type)) {
            paramsMap.put("type", type.trim());
        }

        //门店编码
        String terminalCode = req.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode.trim());
        }

        //状态编码  //3：过期    2：未开启    1：使用中    0：失效
        String statusCode = req.getStatusCode();
        if (StringUtil.isNotNullNorEmpty(statusCode)) {
            paramsMap.put("statusCode", statusCode.trim());
        }

        return paramsMap;
    }

    /**
     * SA广告图设置查询列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaAdSetResBean> querySaAdSetList(Pager<SaAdSetResBean> pager, SaAdSetReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源

        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaAdSetRepositoryImpl.querySaAdSetList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaAdSetResBean.class);
    }
    /**
     * SA广告图设置询列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaAdSetList(HttpServletResponse response, SaAdSetReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaAdSetRepositoryImpl.querySaAdSetList.list", freeMakerContext);
        List<SaAdSetResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaAdSetResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "SA广告图设置询列表";

        th = new String[]{"连锁门店编码","连锁门店名称","门店编码","门店名称","广告类型","门店类型","广告图片",
                "状态","门店所属办大区","门店所属办事处","渠道","展示次数","点击次数","开始时间",
                "结束时间","创建人工号","创建人名称","创建时间"};

        colName = new String[] {"lsTerminalCode","lsTerminalName","terminalCode","terminalName","type","terminalType","image",
                "status","areaName","officeName","channelName","showCount","clickCount",
                "startDate","endTime","createdBy","createdName","createdTime"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * SA广告图设置(手机端)
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSaAdSet(SaAdSetReqBean reqBean) throws Exception {

        ServiceBean serviceBean = new ServiceBean();

        boolean flag = checkSaAdInfo(reqBean);
        String msg = "";
        if(!flag){
            if(2 == reqBean.getType()) {
                msg = "该终端编码已在时此时间段内配置广告位，请执行【失效】后再配置";
            }else{
                msg = "同一个类型在同一个时间段内仅可只有一个广告位";
            }
            serviceBean.setDesc(msg);
            serviceBean.setCode(114);
            return serviceBean;
        }else {

            SaAdSet saAdSet = new SaAdSet();
            if (StringUtil.isNotNullNorEmpty(reqBean.getId())) {
                saAdSet = super.getHibernateTemplate().get(SaAdSet.class, Long.valueOf(reqBean.getId()));
                saAdSet.setUpdatedTime(new Date());
                saAdSet.setUpdatedBy(reqBean.getCreatedBy());
            } else {
                saAdSet.setStatus(1);
                saAdSet.setCreatedTime(new Date());
                saAdSet.setCreatedBy(reqBean.getCreatedBy());
                saAdSet.setCreatedName(reqBean.getCreateName());
            }

            saAdSet.setType(reqBean.getType());
            saAdSet.setTerminalCode(reqBean.getTerminalCode());
            saAdSet.setAdInfo(reqBean.getAdInfo());
            saAdSet.setAdInfoType(reqBean.getAdInfoType());
            saAdSet.setImage(reqBean.getImage());
            saAdSet.setStartDate(DateUtils.stringToDate(reqBean.getStartDate(), "yyyy-MM-dd"));
            saAdSet.setEndDate(DateUtils.stringToDate(reqBean.getEndDate(), "yyyy-MM-dd"));


            if (StringUtil.isNotNullNorEmpty(reqBean.getId())) {
                super.getHibernateTemplate().saveOrUpdate(saAdSet);
            } else {
                super.save(saAdSet, SaAdSet.KEY, "MAMA100_OWNER");
            }

            return serviceBean;
        }

    }

    /**
     * SA广告图设置--状态修改
     *
     * @param reqBean
     * @return
     */
    @Override
    public void changeStatusSaAdSet(SaAdSetReqBean reqBean) {
        SaAdSet saAdSet = super.get(SaAdSet.class, Long.valueOf(reqBean.getId()));
        saAdSet.setStatus(Integer.valueOf(reqBean.getStatusCode()));
        saAdSet.setUpdatedTime(new Date());
        saAdSet.setUpdatedBy(reqBean.getCreatedBy());
        super.getHibernateTemplate().update(saAdSet);
    }

    /**
     * SA广告图设置--SA广告图片验证门店信息
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean checkTerminalInfo(SaAdSetReqBean reqBean) {

        ServiceBean serviceBean = new ServiceBean();
        Map map = queryTerminalInfo(reqBean);
        if(null == map){
            serviceBean.setCode(110);
            serviceBean.setDesc("所填写门店编码不存在或状态不正确，请重新输入");
            return serviceBean;
        }
        String  statusCode= (String)map.get("STATUSCODE");
        if(!"01".equals(statusCode)){
            serviceBean.setCode(111);
            serviceBean.setDesc("所填写终端不存在或状态不正确，请重新输入");
            return serviceBean;
        }
        String  areaOfficeCode= (String)map.get("DEPARTMENTCODE");
        System.out.println("门店大区办事处"+areaOfficeCode+";登陆人大区办事处"+reqBean.getAreaOfficeCode());
        if(StringUtil.isNotNullNorEmpty(reqBean.getAreaOfficeCode())) {
            if (!areaOfficeCode.startsWith(reqBean.getAreaOfficeCode())) {
                serviceBean.setCode(112);
                serviceBean.setDesc("所填写门店编码不在权限范围内，请重新输入");
                return serviceBean;
            }
        }
        String  channalCode= (String)map.get("CHANNELCODE");
        if("08".equals(channalCode)){
            serviceBean.setCode(113);
            serviceBean.setDesc("所填写终端为SA渠道，请重新输入");
            return serviceBean;
        }

        serviceBean.setResponse(map.get("TERMINALNAME"));
        return serviceBean;
    }

    /**
     * 查询门店信息
     *
     * @param reqBean
     * @return
     */
    public Map queryTerminalInfo(SaAdSetReqBean reqBean) {

        String sql = "select dt.department_code  departmentCode,\n" +
                "       t.terminal_channel_code  channelCode,\n" +
                "       to_char(t.short_name)  terminalName,         \n" +
                "       t.status_code  statusCode          \n" +
                "  from mama100_owner.crm_terminal               t,\n" +
                "       mama100_owner.common_department_terminal dt\n" +
                " where 1 = 1                       \n" +
                "   and t.code = dt.terminal_code(+)   \n" +
                "   and t.code = :terminalCode  ";
        Query query = super.createSQLQuery(sql);
        query.setParameter("terminalCode", reqBean.getTerminalCode().trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map =  (Map)query.uniqueResult();
        return map;
    }

    /**
     * 查询门店是否已经被使用信息
     *同一类型是否有交叉时间
     * @param reqBean
     * @return
     */
    public boolean checkSaAdInfo(SaAdSetReqBean reqBean) {
        StringBuffer sb = new StringBuffer();
        sb.append("select count(t.id) from mama100_owner.mkt_sa_ad_set t\n" +
                "   where 1=1                  \n" +
                "   and t.status = 1    \n" +
                "   and t.end_date >= trunc(sysdate)    \n" +
                "   and t.type = :type   \n" +
                "   and t.start_date <= to_date(:endDate,'yyyy-MM-dd') " +
                "   and t.end_date >= to_date(:startDate,'yyyy-MM-dd') " );
        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            sb.append(" and t.id <> "+reqBean.getId()+"");
        }
        if(StringUtil.isNotNullNorEmpty(reqBean.getTerminalCode())){
            sb.append(" and t.terminal_code =  "+reqBean.getTerminalCode().trim()+"");
        }
        Query query = super.createSQLQuery(sb.toString());
        query.setParameter("type", reqBean.getType());
        query.setParameter("startDate", reqBean.getStartDate().trim());
        query.setParameter("endDate", reqBean.getEndDate().trim());
        int count = ((Number)query.uniqueResult()).intValue();
        if(count > 0){
            return false;
        }
        return true;

    }



    /**
     * 查询SA广告图片（C端调用）
     *
     * @param saCode
     * @return
     */
    @Override
    public SaAdSetExternalResBean querySaAdSetImage(String saCode) {
        SaAdSetExternalResBean saAdBean = new SaAdSetExternalResBean();
        //如果门店为空
        if(StringUtil.isNotNullNorEmpty(saCode)){
            //根据saCode 查询对应的门店是否有对应的广告图，如果没有则获取广告类型为无门信息

            //根据SA查对应的门店
            String terminalCode = queryTerminalCodeToSa(saCode);
            if(StringUtil.isNotNullNorEmpty(terminalCode)){
                //查找门店对应的广告，如果多个 优先级如：单体-->连锁-->全国·
                saAdBean =  querySaAdImg(terminalCode,"2");
                if(null == saAdBean){  //查找连锁门店
                    String parentTerminalCode = queryParentTerminalCode(terminalCode);
                    //如果连锁门店为空 查询全国无门的
                    if(StringUtil.isNotNullNorEmpty(parentTerminalCode)){
                        saAdBean =  querySaAdImg(parentTerminalCode,"2");
                        if(null == saAdBean){  //查找全国无门店
                            return querySaAdImg("","1");
                        }
                    }else{
                        return querySaAdImg("","1");

                    }
                    return saAdBean;
                }
                return saAdBean;
            }else{
                return querySaAdImg("","0");
            }
        }else{
            return querySaAdImg("","0");
        }

    }

    /**
     * 查询无SA门店对应的终端（C端调用）
     * @return
     */

    public SaAdSetExternalResBean querySaAdImg(String terminalCode,String type){
        StringBuffer sb = new StringBuffer();
        sb.append("select t.image image,t.ad_info adInfo,t.ad_info_type adInfoType" +
                "   from mama100_owner.mkt_sa_ad_set t      \n" +
                "where 1=1                                  \n" +
                "  and t.status = 1                         \n" +
                "  and t.type = "+type+"                    \n" +
                "  and t.start_date <= trunc(sysdate)       \n" +
                "  and t.end_date >= trunc(sysdate)         ") ;
        
        // 2018-04-18 回滚上一个版本
        if(StringUtil.isNotNullNorEmpty(terminalCode)){
            sb.append(" and t.terminal_code = "+terminalCode+"");
        }

        Query query = super.createSQLQuery(sb.toString()).addScalar("image", StringType.INSTANCE).addScalar("adInfo", StringType.INSTANCE).addScalar("adInfoType", StringType.INSTANCE);
        return (SaAdSetExternalResBean)query.setResultTransformer(Transformers.aliasToBean(SaAdSetExternalResBean.class)).uniqueResult();

    }

    /**
     * 查询门店绑定的SA门店
     * @return
     */

    public String queryTerminalCodeToSa(String saCode){
        String sql =    "  select t.terminal_code terminalCode\n" +
                        "  from mkt_sa_terminal_manage t, mama100_owner.crm_terminal ct\n" +
                        " where 1 = 1\n" +
                        "   and t.status = 1\n" +
                "           and t.start_date <= trunc(sysdate)                                  \n" +
                "           and (t.end_date >= trunc(sysdate)  or t.end_date is null)           \n" +
                        "   and ct.status_code = '01'\n" +
                        "   and t.terminal_code = ct.code\n" +
                        "   and t.sa_code = :saCode";

        Query query = super.createSQLQuery(sql);
        query.setParameter("saCode", saCode.trim());
        System.out.println("---saCode---"+saCode);
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map =  (Map)query.uniqueResult();
        if(null == map){
            return "";
        }
        return (String)map.get("TERMINALCODE");
    }

    /**
     * 查询连锁门店
     * @return
     */

    public String queryParentTerminalCode(String terminalCode){
        String sql =    "  select t.parent_code saCode from mama100_owner.crm_terminal t \n" +
                " where 1 = 1\n" +
                "   and t.status_code = '01'\n" +
                "   and t.code = :terminalCode";

        Query query = super.createSQLQuery(sql);
        query.setParameter("terminalCode", terminalCode.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map =  (Map)query.uniqueResult();
        return (String)map.get("SACODE");
    }
}
