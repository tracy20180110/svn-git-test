package com.biostime.samanage.repository.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActResBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerResBean;
import com.biostime.samanage.repository.single.SaLecturerRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * @author 12804
 */
@Repository("saLecturerRepository")
public class SaLecturerRepositoryImpl extends BaseOracleRepositoryImpl implements SaLecturerRepository {
    /**
     * SA讲师看板
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<SaLecturerResBean> querySaLecturerInfo(SaLecturerReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        if(StringUtils.isNoneBlank(reqBean.getMonth())) {
            paramsMap.put("month", reqBean.getMonth());
        }
        if(StringUtils.isNoneBlank(reqBean.getLectureCode())) {
            paramsMap.put("lectureCode", reqBean.getLectureCode());
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("SaLecturerRepositoryImpl.querySaLecturerInfo", freeMakerContext);

        System.out.println("----SaLecturerRepositoryImpl.querySaLecturerInfo SQL----"+sql.toString());
        List<SaLecturerResBean> saLecturerList = this.findAllForListBySql(sql, paramsMap,SaLecturerResBean.class);

        return saLecturerList;
    }

    /**
     * SA指标
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<BaseKeyValueBean> querySaIndex(SaLecturerReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        if(StringUtils.isNoneBlank(reqBean.getMonth())) {
            paramsMap.put("month", reqBean.getMonth());
        }
        if(StringUtils.isNoneBlank(reqBean.getLectureCode())) {
            paramsMap.put("lectureCode", reqBean.getLectureCode());
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("SaLecturerRepositoryImpl.querySaIndex", freeMakerContext);

        System.out.println("----SaLecturerRepositoryImpl.querySaIndex SQL----"+sql.toString());
        List<BaseKeyValueBean> resBeanList = this.findAllForListBySql(sql, paramsMap,BaseKeyValueBean.class);

        return resBeanList;
    }

    /**
     * 活动现场及活动明细的查询、导出条件组装
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(SaLecturerActReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }


        //类型
        String type = req.getType();
        if (StringUtil.isNotNullNorEmpty(type)) {
            paramsMap.put("type", type.trim());
        }



        //开始时间
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //名称
        String name = req.getName();
        if (StringUtil.isNotNullNorEmpty(name)) {
            paramsMap.put("name", name.trim());
        }

        //讲师ID
        String lecturerId = req.getLecturerId();
        if (StringUtil.isNotNullNorEmpty(lecturerId)) {
            paramsMap.put("lecturerId", lecturerId.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        return paramsMap;
    }
    /**
     * SA讲师活动报表查詢
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaLecturerActResBean> querySaLecturerActList(Pager<SaLecturerActResBean> pager, SaLecturerActReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源

        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaLecturerRepositoryImpl.querySaLecturerActList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaLecturerActResBean.class);
    }

    /**
     * SA讲师活动报表查詢导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaLecturerActList(HttpServletResponse response, SaLecturerActReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaLecturerRepositoryImpl.querySaLecturerActList.list", freeMakerContext);
        List<SaLecturerActResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaLecturerActResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "讲师活动数据";

        th = new String[]{"讲师ID","专家/讲师姓名","手机号码","讲师类型","单位","头衔","工号","所属大区","所属办事处",
                "查询开始时间","查询结束时间", "场次汇总","SA-FTF场次","门店-FTF场次","学术推广场次","内训场次","社群活动场次"};

        colName = new String[] {"lecturerId","name","mobile","type","company","rank","code","areaName","officeName",
                "startDate","endDate", "allActTimes","saFtfTimes","terminalFtfTimes","learningTimes","nternalTimes","communityActNums"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"),"UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * SA讲师活动报表查詢导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaLecturerActDownList(HttpServletResponse response, SaLecturerActReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        paramsMap.remove("buCode");
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaLecturerRepositoryImpl.querySaLecturerActDownList.list", freeMakerContext);
        List<SaLecturerActResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaLecturerActResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "活动明细列表";

        th = new String[]{"讲师ID","专家/讲师姓名","手机号码","讲师类型","单位","头衔","工号","所属大区","所属办事处",
                "SA活动ID","活动类型", "活动名称","活动时间","活动地点","支持类型","签到人数","FTF品项新客"};

        colName = new String[] {"lecturerId","name","mobile","type","company","rank","code","areaName","officeName",
                "ftfActId","actType", "actName","actTime","actAdress","actExpertType","signNums","ftfBrandNums"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"),"UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
}
