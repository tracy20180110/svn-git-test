package com.biostime.samanage.repository.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerReqBean;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerResBean;
import com.biostime.samanage.repository.single.SaProductItemNewCustomerRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA微信会员报表RepositoryImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("saProductItemNewCustomerRepository")
public class SaProductItemNewCustomerRepositoryImpl extends BaseOracleRepositoryImpl implements SaProductItemNewCustomerRepository {


    private String format = "yyyy-mm-dd";

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(SaProductItemNewCustomerReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //会员ID
        String customerId = req.getCustomerId();
        if (StringUtil.isNotNullNorEmpty(customerId)) {
            paramsMap.put("customerId", customerId.trim());
        }
        //SA编码
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }
        //BCC编码
        String bccCode = req.getBccCode();
        if (StringUtil.isNotNullNorEmpty(bccCode)) {
            paramsMap.put("bccCode", bccCode.trim());
        }
        //育婴顾问工号
        String yygwCode = req.getYygwCode();
        if (StringUtil.isNotNullNorEmpty(yygwCode)) {
            paramsMap.put("yygwCode", yygwCode.trim());
        }

        //育儿指导师工号
        String yezdsCode = req.getYezdsCode();
        if (StringUtil.isNotNullNorEmpty(yezdsCode)) {
            paramsMap.put("yezdsCode", yezdsCode.trim());
        }
        //会员手机号码
        String mobile = req.getMobile();
        if (StringUtil.isNotNullNorEmpty(mobile)) {
            paramsMap.put("mobile", mobile.trim());
        }

        //积分来源
        String pointSource = req.getPointSource();
        if(StringUtils.isNotBlank(pointSource)) {
            List<String> recSrc = new ArrayList<String>();
            if ("2".equals(pointSource)) { //营销通APP 包含2、4
                recSrc.add("4");
            }
            recSrc.add(pointSource);
            if (CollectionUtils.isNotEmpty(recSrc)) {
                paramsMap.put("pointSource", recSrc);
            }
        }

        //奶粉阶段：
        String milkStage = req.getMilkStage();
        if (StringUtil.isNotNullNorEmpty(milkStage)) {
            paramsMap.put("milkStage", milkStage.trim());
        }


        //积分开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //积分结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        return paramsMap;
    }


    /**
     * SA品项新客报表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaProductItemNewCustomerResBean> querySaProductItemNewCustomerList(Pager<SaProductItemNewCustomerResBean> pager, SaProductItemNewCustomerReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源

        Pager<SaProductItemNewCustomerResBean> saProductItemNewCustomerResBean = new Pager<SaProductItemNewCustomerResBean>();
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        //默认历史
        String sql= SqlUtil.get("SaProductItemNewCustomerRepositoryImpl.querySaProductItemNewCustomerList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        saProductItemNewCustomerResBean = this.pageDataBySql(pagerQuery, pager, SaProductItemNewCustomerResBean.class);
        return saProductItemNewCustomerResBean;
    }

    /**
     * SA品项新客报表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaProductItemNewCustomerList(HttpServletResponse response, SaProductItemNewCustomerReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        //默认历史
        String sql= SqlUtil.get("SaProductItemNewCustomerRepositoryImpl.querySaProductItemNewCustomerList.list", freeMakerContext);

        List<SaProductItemNewCustomerResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaProductItemNewCustomerResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "SA品项新客报表";

        th = new String[]{"大区","办事处","SA编码","SA属性","会员ID","宝宝生日/预产期",
                "手机号码", "首次绑定微信时间","积分来源","BCC工号","育婴顾问工号","育婴顾问归属","关联育儿指导师","产品名称","积分时间",
                "购买门店","品牌","品项","奶粉阶段","是否SAYN新客","是否FTF新客","是否线上礼包新客"};

        colName = new String[] {"areaName","officeName","saCode","saAttr","customerId","babyBirthday","mobile", "bindWxTime","pointSource","bccCode","yygwCode","yygwByCompany",
                "yezdsCode","productName","pointTime","buyTerminal","brandName","productItemName","milkStage","isSaYnNewCustomer","isFtfNewCustomer","isNewCustGiftPkg"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
}
