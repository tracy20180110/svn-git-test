package com.biostime.samanage.repository.impl.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.actreport.ActRePortWebReqBean;
import com.biostime.samanage.bean.actreport.ActRePortWebResBean;
import com.biostime.samanage.bean.actreport.ActRePortZhiBoReqBean;
import com.biostime.samanage.bean.actroadshow.*;
import com.biostime.samanage.domain.SaConfig;
import com.biostime.samanage.domain.act.*;
import com.biostime.samanage.repository.act.ActRoadShowRepository;
import com.biostime.samanage.util.DateUtils;
import com.biostime.samanage.util.ExcelUtil;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 活动现场配置
 * Created by dc on 2016/8/18.
 */
@Service("actRoadShowRepository")
public class ActRoadShowRepositoryImpl extends BaseOracleRepositoryImpl implements ActRoadShowRepository {

    private final String IMPORT_TERMINAL_TYPE = "无效";
    /**
     * 活动现场及活动明细的查询、导出条件组装
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(ActRoadShowListReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //事业部
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        //大区办事处
        String areaOfficeCode = req.getAreaofficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //大区
        String areaCode = req.getAreaCode();
        if (StringUtil.isNotNullNorEmpty(areaCode)) {
            paramsMap.put("areaCode", areaCode.trim());
        }

        //办事处
        String officeCode = req.getOfficeCode();
        if (StringUtil.isNotNullNorEmpty(officeCode)) {
            paramsMap.put("officeCode", officeCode.trim());
        }


        //品牌
        String brandCode = req.getBrandCode();
        if (StringUtil.isNotNullNorEmpty(brandCode)) {
            paramsMap.put("brandCode", brandCode.trim());
        }
        //渠道
        String channelCode = req.getChannelCode();
        if (StringUtil.isNotNullNorEmpty(channelCode)) {
            paramsMap.put("channelCode", channelCode.trim());
        }
        //类型
        String configActTypeCode = req.getConfigActTypeCode();
        if (StringUtil.isNotNullNorEmpty(configActTypeCode)) {
            paramsMap.put("configActTypeCode", configActTypeCode.trim());
        }


        //状态
        String status = req.getStatus();
        if (StringUtil.isNotNullNorEmpty(status)) {
            paramsMap.put("status", status);
        }


        //开始时间
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //名称
        String actName = req.getActName();
        if (StringUtil.isNotNullNorEmpty(actName)) {
            paramsMap.put("actName", actName.trim());
        }

        //活动ID
        String actId = req.getActId();
        if (StringUtil.isNotNullNorEmpty(actId)) {
            paramsMap.put("actId", actId.trim());
        }

        //门店编码
        String terminalCode = req.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode.trim());
        }


        //活动创建人工号
        String createdBy = req.getCreatedBy();
        if (StringUtil.isNotNullNorEmpty(createdBy)) {
            paramsMap.put("createdBy", createdBy.trim());
        }

        //活动创建人工号
        String reportBy = req.getReportBy();
        if (StringUtil.isNotNullNorEmpty(reportBy)) {
            paramsMap.put("reportBy", reportBy.trim());
        }

        String suffice = req.getSuffice();
        if (StringUtil.isNotNullNorEmpty(suffice)) {
            paramsMap.put("suffice", suffice.trim());
        }

        String reportStartDate = req.getReportStartDate();
        if (StringUtil.isNotNullNorEmpty(reportStartDate)) {
            paramsMap.put("reportStartDate", reportStartDate.trim());
        }

        String reportEndDate = req.getReportEndDate();
        if (StringUtil.isNotNullNorEmpty(reportEndDate)) {
            paramsMap.put("reportEndDate", reportEndDate.trim());
        }

        //连锁门店编码
        String relationTerminalCode = req.getRelationTerminalCode();
        if (StringUtil.isNotNullNorEmpty(relationTerminalCode)) {
            paramsMap.put("relationTerminalCode", relationTerminalCode.trim());
        }

        //连锁门店名称
        String relationTerminalName = req.getRelationTerminalName();
        if (StringUtil.isNotNullNorEmpty(relationTerminalName)) {
            paramsMap.put("relationTerminalName", relationTerminalName.trim());
        }


        String makeupMemo = req.getMakeupMemo();
        if (StringUtil.isNotNullNorEmpty(makeupMemo)) {
            paramsMap.put("makeupMemo", makeupMemo);
        }

        //是否补录（0：否，1：是")
        String actMakeup = req.getActMakeup();
        if (StringUtil.isNotNullNorEmpty(actMakeup)) {
            paramsMap.put("actMakeup", actMakeup.trim());
        }

        //图片类别
        String imageMarkType = req.getImageMarkType();
        if (StringUtil.isNotNullNorEmpty(imageMarkType)) {
            paramsMap.put("imageMarkType", imageMarkType.trim());
        }

        return paramsMap;
    }

    /**
     * 活动现场--查询
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<ActRoadShowListResBean> queryActRoadShowList(Pager<ActRoadShowListResBean> pager, ActRoadShowListReqBean reqBean) {

        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("ActRoadShowRepositoryImpl.queryActRoadShowList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, ActRoadShowListResBean.class);
    }
    /**
     * 活动现场--导出
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportActRoadShow(HttpServletResponse response, ActRoadShowListReqBean reqBean) throws Exception{
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("ActRoadShowRepositoryImpl.queryActRoadShowList.list", freeMakerContext);
        List<ActRoadShowListResBean> bscarList = this.findAllForListBySql(sql,paramsMap,ActRoadShowListResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "活动现场列表";

        th = new String[]{"活动ID","活动大区","活动办事处","活动名称","考核类型","活动类型","活动品牌","官微二维码申请","签到小程序码","活动渠道","活动门店类型",
                "活动开始时间","活动结束时间", "状态","创建人工号","创建人姓名","创建时间"};

        colName = new String[] {"id","areaName","officeName","actName","khTypeName","configActTypeName",
                "brandName","qrCodeName","signMiniCode","channelName","configTerminalTypeName",
                "startDate","endDate", "status","createdBy","createdName","createdTime"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"),"UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
    /**
     * 活动现场--保存
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveActRoadShow(ActRoadShowSaveReqBean reqBean){
        ServiceBean serviceBean = new ServiceBean();
        try {
            Date sDate = DateUtils.stringToDate(reqBean.getStartDate(), "yyyy-MM-dd");
            Date eDate = DateUtils.stringToDate(reqBean.getEndDate(), "yyyy-MM-dd");

            ActRoadShow arsBean = new ActRoadShow();

            if (StringUtil.isNotNullNorEmpty(reqBean.getId())) {
                arsBean = super.getHibernateTemplate().get(ActRoadShow.class, Long.valueOf(reqBean.getId()));
                arsBean.setUpdatedTime(new Date());
                arsBean.setUpdatedBy(reqBean.getUserId());
            } else {
                arsBean.setCreatedTime(new Date());
                arsBean.setCreatedBy(reqBean.getUserId());
                arsBean.setCreatedName(reqBean.getUserName());
                //状态 默认启用
                arsBean.setStatus(1);
            }
            arsBean.setKhType(Long.valueOf(reqBean.getKhType()));
            arsBean.setAreaOfficeCode(reqBean.getAreaOfficeCode());
            arsBean.setActName(reqBean.getActName());
            arsBean.setStartTime(sDate);
            arsBean.setEndTime(eDate);
            arsBean.setActConfigId(Long.valueOf(reqBean.getActConfigId()));

            arsBean.setChannelName(reqBean.getChannelName());
            arsBean.setChannelCode(reqBean.getChannelCode());
            arsBean.setTerminalTypeCode(reqBean.getTerminalTypeCode());
            arsBean.setTerminalTypeName(reqBean.getTerminalTypeName());

            arsBean.setSignMiniCode(Long.valueOf(reqBean.getSignMiniCode()));

            if("2".equals(reqBean.getKhType())){ //考核类型1:活动现场，2：物料陈列
                arsBean.setActReportType(Long.valueOf(reqBean.getActReportType())); //上报选相册（1：是，0否/空也是否）
            }
            if (StringUtil.isNotNullNorEmpty(reqBean.getId())) {
                super.getHibernateTemplate().update(arsBean);
            } else {
                super.save(arsBean, ActRoadShow.KEY, "MAMA100_OWNER");
            }
            String[] swisse = reqBean.getSwisseId();
            if(StringUtil.isNotNullNorEmpty(swisse)) {
                if (swisse.length != 0) {
                    //先删除  在更新
                    String hql = " delete from ActRoadShowSw arss where 1=1 and arss.actShowId = :actShowId ";
                    Query query = super.createQuery(hql);
                    query.setParameter("actShowId", arsBean.getId());
                    query.executeUpdate();

                    List<ActRoadShowSw> actRoadShowSwList = new ArrayList<ActRoadShowSw>();
                    List<String> swIdList = Arrays.asList(swisse);
                    for (String swId : swIdList) {
                        ActRoadShowSw actRoadShowSw = new ActRoadShowSw();
                        actRoadShowSw.setActShowId(arsBean.getId());
                        actRoadShowSw.setSwId(Long.valueOf(swId));
                        actRoadShowSwList.add(actRoadShowSw);
                    }
                    super.batchAdd(actRoadShowSwList, ActRoadShowSw.KEY, "MAMA100_OWNER");
                }
            }
            String[] brandCodes = reqBean.getBrandCode();
            if(StringUtil.isNotNullNorEmpty(brandCodes)) {
                if (brandCodes.length != 0) {
                    //先删除  在更新
                    String hql = " delete from ActRoadShowBrand arss where 1=1 and arss.actRoadId = :actShowId ";
                    Query query = super.createQuery(hql);
                    query.setParameter("actShowId", arsBean.getId());
                    query.executeUpdate();

                    List<ActRoadShowBrand> actRoadShowBrandList = new ArrayList<ActRoadShowBrand>();
                    List<String> brandCodeList = Arrays.asList(brandCodes);
                    for (String brandCode : brandCodeList) {
                        ActRoadShowBrand actRoadShowBrand = new ActRoadShowBrand();
                        actRoadShowBrand.setActRoadId(arsBean.getId());
                        actRoadShowBrand.setCode(Long.valueOf(brandCode));
                        actRoadShowBrand.setType(1L);
                        actRoadShowBrandList.add(actRoadShowBrand);
                    }
                    super.batchAdd(actRoadShowBrandList, ActRoadShowBrand.KEY, "MAMA100_OWNER");
                }
            }

            //是否申请二维码
            List<BaseKeyValueBean>  qrCodeList = reqBean.getQrCodeList();
            if(CollectionUtils.isNotEmpty(qrCodeList)){
                //先删除  在更新
                String hql = " delete from SaConfig sc where 1=1 and sc.tabId = :tabId ";
                Query query = super.createQuery(hql);
                query.setParameter("tabId", arsBean.getId());
                query.executeUpdate();

                List<SaConfig> saConfigList = new ArrayList<SaConfig>();
                for (BaseKeyValueBean bean : qrCodeList) {
                    SaConfig saConfig = new SaConfig();
                    saConfig.setTabId(arsBean.getId());
                    saConfig.setName(String.valueOf(bean.getValue()));
                    saConfig.setCode(Long.valueOf(bean.getKey()));
                    saConfig.setType(1L);//
                    saConfigList.add(saConfig);
                }
                super.batchAdd(saConfigList, SaConfig.KEY, "MAMA100_OWNER");
            }
        }catch (Exception e){
            e.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }

        return serviceBean;
    }

    /**
     * 活动现场--状态更改
     * @param reqBean
     * @return
     */
    @Override
    public void changeStatusActRoadShow(ActRoadShowChangeStatusReqBean reqBean) {
        ActRoadShow arsBean = super.get(ActRoadShow.class, Long.valueOf(reqBean.getId()));
        arsBean.setStatus(Integer.valueOf(reqBean.getStatus()));
        arsBean.setUpdatedTime(new Date());
        arsBean.setUpdatedBy(reqBean.getUserId());
        super.getHibernateTemplate().update(arsBean);
    }

    /**
     * 活动现场-获取swisse列表、活动类型
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<ActRoadShowResBean> querySwActTypeList(ActRoadShowReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        if("2".equals(reqBean.getType())) {
            if(StringUtil.isNotNullNorEmpty(reqBean.getSwName())) {
                paramsMap.put("swName", reqBean.getSwName());
            }
        }else {
            if (StringUtil.isNotNullNorEmpty(reqBean.getBrandCode())) {
                String brandCode[] = reqBean.getBrandCode().split(",");
                paramsMap.put("brandCode", Arrays.asList(brandCode));
            }
            if(StringUtil.isNotNullNorEmpty(reqBean.getChannelCode())) {
                String channelCode[] = reqBean.getChannelCode().split(",");
                paramsMap.put("channelCode", Arrays.asList(channelCode));
            }
            if(StringUtil.isNotNullNorEmpty(reqBean.getTerminalTypeCode())) {
                String terminalTypeCode[] = reqBean.getTerminalTypeCode().split(",");
                paramsMap.put("terminalTypeCode", Arrays.asList(terminalTypeCode));
            }
            if(StringUtil.isNotNullNorEmpty(reqBean.getReportTypeCode())) {
                String reportTypeCode[] = reqBean.getReportTypeCode().split(",");
                paramsMap.put("reportTypeCode", Arrays.asList(reportTypeCode));
            }
        }
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        String sql = SqlUtil.get("ActRePortRepositoryImpl.querySwActTypeList.actType", freeMakerContext);
        if("2".equals(reqBean.getType())) {
            sql = SqlUtil.get("ActRePortRepositoryImpl.querySwActTypeList.sw", freeMakerContext);
        }

        System.out.println("----ActRePortRepositoryImpl.querySwActTypeList SQL----" + sql.toString());
        List<ActRoadShowResBean> actRoadShowResBeanList = this.findAllForListBySql(sql, paramsMap,ActRoadShowResBean.class);
        return actRoadShowResBeanList;
    }


    /**
     * 营销上报明细--查询
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<ActRoadShowDetailResBean> queryActRoadShowDetailList(Pager<ActRoadShowDetailResBean> pager, ActRoadShowListReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);

        String sql = SqlUtil.get("ActRoadShowRepositoryImpl.queryActRoadShowDetailList.list", new FreeMakerContext(paramsMap));
        //按促销员维度查询1:查询，空就是不查
        String salesQuery = reqBean.getSalesQuery();
        if (StringUtil.isNotNullNorEmpty(salesQuery)) {
            sql = SqlUtil.get("ActRoadShowRepositoryImpl.queryActRoadShowDetailList.sales.list", new FreeMakerContext(paramsMap));
        }
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, ActRoadShowDetailResBean.class);
    }

    /**
     * 营销上报明细-审核
     * @param reqBean
     * @return
     */
    @Override
    public void auditActRoadShowDetail(ActRoadShowAuditReqBean reqBean) throws Exception{
        String actId = reqBean.getActDetailId();
        if(StringUtil.isNotNullNorEmpty(actId)){
            for(String id:actId.split(",")){
                ActRoadShowAudit arsAudit = new ActRoadShowAudit();
                arsAudit.setActDetailId(Long.valueOf(id));
                arsAudit.setAuditMemo(reqBean.getAuditMemo());
                arsAudit.setCreatedBy(reqBean.getUserId());
                arsAudit.setCreatedName(reqBean.getCreatedName());
                arsAudit.setCreatedTime(new Date());
                arsAudit.setIsSuffice(reqBean.getIsSuffice());
                super.save(arsAudit, ActRoadShowAudit.KEY, "MAMA100_OWNER");
            }
        }

    }

    /**
     * 营销上报明细--导出
     * @param response
     * @param reqBean
     * @throws Exception
     */
    @Override
    public void exportActRoadShowDetail(HttpServletResponse response, ActRoadShowListReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);

        String sql = SqlUtil.get("ActRoadShowRepositoryImpl.queryActRoadShowDetailList.list", new FreeMakerContext(paramsMap));
        //按促销员维度查询1:查询，空就是不查
        String salesQuery = reqBean.getSalesQuery();
        if (StringUtil.isNotNullNorEmpty(salesQuery)) {
            sql = SqlUtil.get("ActRoadShowRepositoryImpl.queryActRoadShowDetailList.sales.list", new FreeMakerContext(paramsMap));
        }
        String countSql= "select count(*) from ("+sql+")";

        List<ActRoadShowDetailResBean> bscarList = this.findAllForListBySql(sql,paramsMap,ActRoadShowDetailResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "营销上报明细";

        th = new String[]{"上报品牌","活动ID","活动名称","活动类型","考核类型","创建人工号","创建人名称","活动开始日期","活动结束日期",
                "上报门店编码","上报门店名称","连锁门店编码","连锁门店名称","门店大区","门店办事处","门店办渠道","省","市","区","街道","上报人工号","上报人名称","上报人岗位","上报人所属公司","上报时间","活动执行开始时间","活动执行结束时间",
                "上报备注","照片1","图片类别1","照片2","图片类别2","照片3","图片类别3","照片4","图片类别4","照片5","图片类别5","照片6","图片类别6","是否合格","审批人","审批时间","审批原因","是否补录","补录原因"
                };

        colName = new String[] {"brand","actId","actName","actType","khTypeName","actCreatedBy","actCreatedName","startDate","endDate",
                "terminalCode","terminalName","relationTerminalCode","relationTerminalName","areaTerminal","officeTerminal","channelName","province","city","district","town",
                "createdBy","createdName","postName","reportByCompany","createdTime","bbStartDate","bbEndDate","actMemo",
                "img1","mark1","img2","mark2","img3","mark3","img4","mark4","img5","mark5","img6","mark6",
                "suffice","auditAccount","auditTime","auditMemo","actMakeup","makeupMemo"
               };

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"),"UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * 路演报表查询、导出条件组装
     * @param reqBean
     * @return
     */
    public HashMap<String, Object> roadShowParamsMap(ActRoadShowReportReqBean reqBean){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        String areaOfficeCode = reqBean.getAreaofficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }
        //开始时间
        String startDate = reqBean.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        String endDate = reqBean.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        String channelCode = reqBean.getChannelCode();
        if (StringUtil.isNotNullNorEmpty(channelCode)) {
            paramsMap.put("channelCode", channelCode.trim());
        }
        String terminalCode = reqBean.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode.trim());
        }

        //名称
        String actName = reqBean.getActName();
        if (StringUtil.isNotNullNorEmpty(actName)) {
            paramsMap.put("actName", actName.trim());
        }

        //活动ID
        String actId = reqBean.getActId();
        if (StringUtil.isNotNullNorEmpty(actId)) {
            paramsMap.put("actId", actId.trim());
        }

        //状态
        String status = reqBean.getStatus();
        if (StringUtil.isNotNullNorEmpty(status)) {
            paramsMap.put("status", status);
        }
        
        // 上报开始日期
        String reportingStartTime = reqBean.getReportingStartTime();
        if (StringUtil.isNotNullNorEmpty(reportingStartTime)) {
            paramsMap.put("reportingStartTime", reportingStartTime);
        }
        
        // 上报结束日期
        String reportingEndTime = reqBean.getReportingEndTime();
        if (StringUtil.isNotNullNorEmpty(reportingEndTime)) {
            paramsMap.put("reportingEndTime", reportingEndTime);
        }
        
        //活动类型
        String type = reqBean.getType();
        if (StringUtil.isNotNullNorEmpty(type)) {
            paramsMap.put("type", type);
        }

        //品牌
        String brandCode = reqBean.getBrandCode();
        if (StringUtil.isNotNullNorEmpty(brandCode)) {
            paramsMap.put("brandCode", brandCode.trim());
        }

        //连锁门店编码
        String relationTerminalCode = reqBean.getRelationTerminalCode();
        if (StringUtil.isNotNullNorEmpty(relationTerminalCode)) {
            paramsMap.put("relationTerminalCode", relationTerminalCode.trim());
        }

        //连锁门店名称
        String relationTerminalName = reqBean.getRelationTerminalName();
        if (StringUtil.isNotNullNorEmpty(relationTerminalName)) {
            paramsMap.put("relationTerminalName", relationTerminalName.trim());
        }

        String buCode = reqBean.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }

        return paramsMap;
    }
    /**
     * 活动现场报表-查询
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<ActRoadShowReportResBean> queryReportActRoadShowList(Pager<ActRoadShowReportResBean> pager, ActRoadShowReportReqBean reqBean){

        //手动切换成只读数据源
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());

        HashMap<String, Object> paramsMap = roadShowParamsMap(reqBean);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("ActRoadShowRepositoryImpl.queryReportActRoadShowList.list.history", freeMakerContext);



        String countSql= "select count(*) from ("+sql+")";
        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, ActRoadShowReportResBean.class);
    }

    /**
     * 路演报表--导出
     * @param response
     * @param reqBean
     */
    @Override
    public void exportReportActRoadShow(HttpServletResponse response, ActRoadShowReportReqBean reqBean) throws Exception{

        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = roadShowParamsMap(reqBean);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        String sql= SqlUtil.get("ActRoadShowRepositoryImpl.queryReportActRoadShowList.list.history", freeMakerContext);
        /*String sql= SqlUtil.get("ActRoadShowRepositoryImpl.queryReportActRoadShowList.list", freeMakerContext);

        Integer endDate = 20180425;
        if(StringUtil.isNotNullNorEmpty(reqBean.getStartDate())) {
            Integer sbStartDate = Integer.valueOf(reqBean.getStartDate().replace("-", ""));
            if (endDate - sbStartDate >= 0) {   //查询历史数据
                sql= SqlUtil.get("ActRoadShowRepositoryImpl.queryReportActRoadShowList.list.old", freeMakerContext);
            }
        }*/
        List<ActRoadShowReportResBean> bscarList = this.findAllForListBySql(sql,paramsMap,ActRoadShowReportResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "活动考核报表";

        th = new String[]{"活动ID","活动名称","活动类型","关联SA活动","上报品牌","上报人工号","上报人名称","上报人岗位","上报人所属公司","门店编码","门店名称", "门店大区", "门店办事处", "连锁门店编码","连锁门店名称","门店渠道","省","市","区","街道","执行开始日期", "执行结束日期",
               "上报时间","上报备注","上报审核情况","合生元微信新会员","HT微信新会员","Dodie微信新会员","活动签到人数","全品项积分","Dodie积分","奶粉新客", "合生元奶粉新客","HT新客","可贝思新客", "益生菌新客", "Dodie新客", "营养补充剂新客","Swisse活动SKU的POS数（元）", "HT罐数(400g)", "HT罐数(800g)", "Dodie纸尿裤包数",
            "全品项积分(签到转化)","Dodie积分(签到转化)","奶粉新客(签到转化)","合生元奶粉新客(签到转化)","HT新客(签到转化)","可贝思新客(签到转化)","益生菌新客(签到转化)","Dodie新客(签到转化)","营养补充剂新客(签到转化)" };

        colName = new String[] {"actId","actName","actTypeName","saFtfActName","brandName","reportBy","reportName","reportPostName","reportByCompany","terminalCode","terminalName","areaName","officeName","relationTerminalCode","relationTerminalName","channelName","province","city","district","town","startDate","endDate",
                "reportingTime","memo","isQualified","wxNewCustomer","htWxNewCustomer","dodieWxNewcustomer","signNum","allPoint","pointDodie","milkNewCust", "milkNewCustomer","htNewCust","kbsNewCustomer","ysjNewCustomer","dodieNewCustomerNum","addiNutrition","swPosNum","ht4g","ht8g","dodieNum",
                "signAllPoint","signPointDodie","milkSignNewCust","bstMilkSignNewCustomer","htSignNewCust","kbsSignNewCustomer","ysjSignNewCustomer","dodieSignNewCustomerNum" ,"addiNutritionSignNewCustNum"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"),"UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * 活动现场报表--导出
     *
     * @param response
     * @param actId
     * @throws Exception
     */
    @Override
    public void exportActRoadShowTerminal(HttpServletResponse response, String actId) throws Exception {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        if (StringUtil.isNotNullNorEmpty(actId)) {
            paramsMap.put("actId", actId.trim());
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("ActRoadShowRepositoryImpl.exportActRoadShowTerminal.export", freeMakerContext);

        List<ActRoadShowTerminalResBean> bscarList = this.findAllForListBySql(sql,paramsMap,ActRoadShowTerminalResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "活动现场门店";

        th = new String[]{"门店编码", "活动开始日期", "活动结束日期","状态"};

        colName = new String[] {"terminalCode","startDate","endDate","status"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"),"UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * 活动现场门店--导入
     * @param reqBean
     * @throws Exception
     */
    @Override
    public String importActRoadShowTerminal(MultipartFile multipartFile, ActRoadShowTerminalReqBean reqBean) throws Exception {
        String areaOfficeCode = reqBean.getAreaOfficeCode();
        String channelCode = reqBean.getChannelCode();

        //时间验证
        String dateMatches = "\\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[0-1])";
        //存放不符合要求的信息提示。
        StringBuffer sb = new StringBuffer();
        String fileName = multipartFile.getOriginalFilename();
        boolean flag = false;
        if(fileName.endsWith(".xls")){
            flag = true;
        }
        InputStream inputXLS = multipartFile.getInputStream();
        ExcelUtil excelReader = new ExcelUtil();
        Map<Integer, String[]> map = excelReader.readExcelContent(inputXLS,flag);


        String actId = reqBean.getActId();
        ActRoadShow arsBean = super.get(ActRoadShow.class,Long.valueOf(reqBean.getActId()));

        for (int i = 1; i <= map.size(); i++) {
            List<ActRoadShowTerminal> arstList = new ArrayList<ActRoadShowTerminal>();
            String[] str = map.get(i);
            int j = 0;
            ActRoadShowTerminal arst = new ActRoadShowTerminal();
            //门店
            String terminalCode = str[j++].trim();
            if(terminalCode.endsWith(".0")){
                terminalCode = terminalCode.substring(0,terminalCode.indexOf("."));
            }

            arst.setTerminalCode(terminalCode);
            //获取门店对应的大区办事处编码、渠道
            Map codeMap = getTerminalAreaOfficeCode(terminalCode);

            if(null == codeMap){
                sb.append("第" + i + "行，门店编码不存在。");
                continue;
            }
            String q_departmentcode = (String) codeMap.get("DEPARTMENTCODE");
            String q_channelCode = (String) codeMap.get("CHANNELCODE");
            System.out.println("门店所属办事处编码:"+q_departmentcode);
            //登录人所属大区办事处是否与门店所属大区办事一致
            if(StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
                if (!q_departmentcode.startsWith(areaOfficeCode)) {
                    sb.append("第" + i + "行，您与门店所属大区办事不一致。");
                    continue;
                }
            }
            String actAreaOfficeCode = arsBean.getAreaOfficeCode();
            //登录人所属大区办事处是否与门店所属大区办事一致
            if(StringUtil.isNotNullNorEmpty(actAreaOfficeCode)) {
                 if(!q_departmentcode.startsWith(actAreaOfficeCode)) {
                    sb.append("第" + i + "行，门店与活动所属大区办事不一致。");
                    continue;
                }

            }
            if(StringUtil.isNotNullNorEmpty(channelCode)) {
                String channels[] = channelCode.split(",");
                if (! Arrays.asList(channels).contains(q_channelCode)) {
                    sb.append("第" + i + "行，您与门店所属渠道不一致。");
                    continue;
                }
            }
            //2018-03-15
            /*String actChannelCode = arsBean.getChannelCode();
            System.out.println("门店渠道："+q_channelCode+",活动渠道"+actChannelCode);
            if(StringUtil.isNotNullNorEmpty(actChannelCode)) {
                String channels[] = actChannelCode.split(",");
                if (! Arrays.asList(channels).contains(q_channelCode)) {
                    sb.append("第" + i + "行，门店与活动所属渠道不一致。");
                    continue;
                }
               *//*if (!q_channelCode.equals(actChannelCode)) {

                }*//*
            }*/
            //活动备案开始日期
            String startDate = str[j++].trim();
            if (StringUtil.isNullOrEmpty(startDate)) {
                sb.append("第" + i + "行，活动开始日期不能为空。");
                continue;
            } else if (!startDate.matches(dateMatches)) {
                sb.append("第" + i + "行，不是有效的活动开始日期。");
                continue;
            }
            Date sDate = DateUtils.stringToDate(startDate, "yyyy-MM-dd");
            if(sDate.before(new Date())){
                sb.append("第" + i + "行，活动开始日期不能小于今天。");
                continue;
            }
            //活动备案开始日期
            String endDate = str[j++].trim();
            if (StringUtil.isNullOrEmpty(endDate)) {
                sb.append("第" + i + "行，活动结束日期不能为空。");
                continue;
            } else if (!endDate.matches(dateMatches)) {
                sb.append("第" + i + "行，不是有效的活动结束日期。");
                continue;
            }

            Date eDate = DateUtils.stringToDate(endDate, "yyyy-MM-dd");
            //校验开始时间与结束时间，开始时间需小于结束时间
            if (null != sDate && eDate.before(sDate)) {
                sb.append("第" + i + "行，活动开始日期不能大于活动结束日期，请重新导入。");
                continue;
            }

            //如果导入状态为失效，则根据门店、开始日期、结束日期去查询是否有数据，如果有直接更新状态
            String status = str[j++].trim();
            boolean temp = false;
            if(StringUtil.isNotNullNorEmpty(status)){
                if(status.equals(IMPORT_TERMINAL_TYPE)){
                    List<ActRoadShowTerminalResBean> bscarList = queryByActIdTerminal(actId,terminalCode,startDate,endDate);
                    if(bscarList.size()>0){
                        for(ActRoadShowTerminalResBean arstResBean:bscarList){
                            ActRoadShowTerminal arstBean = super.getHibernateTemplate().get(ActRoadShowTerminal.class,Long.valueOf(arstResBean.getId()));
                            arstBean.setUpdateBy(reqBean.getUserId());
                            arstBean.setUpdateTime(new Date());
                            arstBean.setStatus(0);  //无效
                            super.getHibernateTemplate().update(arstBean);
                            temp = true;
                            break;
                        }
                        if(temp){
                            continue;
                        }
                    }else{
                        continue;
                    }
                }
            }else{
                sb.append("第" + i + "行，活动状态不能为空，请重新导入。");
                continue;
            }

            Date actStartDate = arsBean.getStartTime();  //活动开始日期
            Date actEndDate = arsBean.getEndTime();      //活动结束日期
            System.out.println("活动日期"+arsBean.getStartTime()+"--"+arsBean.getEndTime()+"导入日期"+startDate+"---"+endDate);
            //验证导入的开始日期跟结束日期是否满足活动的日期
            if(!((sDate.after(actStartDate) || sDate.equals(actStartDate)) && (eDate.before(actEndDate) || eDate.equals(actEndDate)))) {
                sb.append("第" + i + "行，门店日期不在活动日期范围内，请重新导入。");
                continue;
            }else{
                //满足活动日期
                //第一：根据活动ID、门店编码，验证是否已经存在
                List<ActRoadShowTerminalResBean> bscarList = queryByActIdTerminal(actId,terminalCode,"","");

                if(bscarList.size()>0) {
                    for (ActRoadShowTerminalResBean arstBean : bscarList) {
                        //当前活动已经存在相同的门店，验证时间是否交叉
                        System.out.println("门店活动日期"+arstBean.getStartDate()+"--"+arstBean.getEndDate()+"导入日期"+startDate+"---"+endDate);
                        if(!(eDate.before(DateUtils.stringToDate(arstBean.getStartDate(), "yyyy-MM-dd")) ||
                                sDate.after(DateUtils.stringToDate(arstBean.getEndDate(), "yyyy-MM-dd")))){

                            /*
                            //有交叉，把数据库中对应活动、门店的这条数据状态更改为失效。
                            ActRoadShowTerminal astBean = super.get(ActRoadShowTerminal.class,Long.valueOf(arstBean.getId()));
                            astBean.setStatus(0);  //无效
                            astBean.setUpdateBy(reqBean.getUserId());
                            astBean.setUpdateTime(new Date());
                            super.getHibernateTemplate().update(astBean);*/

                            sb.append("第" + i + "行，活动时间已交叉，请重新导入。");
                            temp = true;
                            break;
                        }
                    }
                    if(temp){
                        continue;
                    }

                }

            }

            arst.setStartTime(sDate);
            arst.setEndTime(eDate);
            arst.setStatus(1);
            arst.setCreatedBy(reqBean.getUserId());
            arst.setCreatedName(reqBean.getCreatedName());
            arst.setCreatedTime(new Date());
            arst.setActRsId(Long.valueOf(actId));

            arstList.add(arst);
            super.batchAdd(arstList, ActRoadShowTerminal.KEY, "MAMA100_OWNER");
        }

        return sb.toString();

    }



    /**
     * 根据活动ID、门店获取报备门店
     * @param actId
     * @param terminalCode
     * @return
     */
    public List<ActRoadShowTerminalResBean> queryByActIdTerminal(String actId,String terminalCode,String startDate,String endDate) {

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        paramsMap.put("status", "1");
        if (StringUtil.isNotNullNorEmpty(actId)) {
            paramsMap.put("actId", actId.trim());
        }
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode.trim());
        }
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("ActRoadShowRepositoryImpl.exportActRoadShowTerminal.export", freeMakerContext);
        List<ActRoadShowTerminalResBean> bscarList = this.findAllForListBySql(sql,paramsMap,ActRoadShowTerminalResBean.class);
        return bscarList;
    }

    /**
     * 获取门店大区办事编码
     *
     * @param terminalCode
     * @return
     */
    public Map getTerminalAreaOfficeCode(String terminalCode) {

        String sql = "select t.department_code departmentCode,t.channel_code channelCode from mama100_owner.common_department_terminal t " +
                "where 1=1" +
                "and t.terminal_code = :terminalCode";
        Query query = super.createSQLQuery(sql);
        query.setParameter("terminalCode", terminalCode.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return (Map)query.uniqueResult();

    }

    /**
     * 营销上报明细-补录
     *
     * @param reqBean
     * @return
     */
    @Override
    public void saveActRoadShowMakeup(ActRePortWebReqBean reqBean) throws Exception {
        ActRePortDetail actRePortDetail = new ActRePortDetail();
        String userId = reqBean.getCreatedBy();
        String terminalCode = reqBean.getTerminalCode();

        actRePortDetail.setActId(reqBean.getCode());
        actRePortDetail.setTerminalCode(terminalCode);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        actRePortDetail.setCreatedTime(sdf.parse(reqBean.getRePortDate()));
        actRePortDetail.setType("1");    //1：活动现场、2：SA上报
        actRePortDetail.setCreatedBy(userId);

        actRePortDetail.setCreatedName(reqBean.getCreatedName());

        actRePortDetail.setImagePath1(reqBean.getImgOne());
        actRePortDetail.setImagePath2(reqBean.getImgTwo());
        actRePortDetail.setImagePath3(reqBean.getImgThree());

        actRePortDetail.setActMakeup(1);
        super.save(actRePortDetail, ActRePortDetail.KEY, "MAMA100_OWNER");
    }

    /**
     * 营销上报明细-补录活动门店验证
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean checkActTerminal(ActRePortWebReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        Map map = queryTerminalInfo(reqBean);
        if(null == map){
            serviceBean.setCode(120);
            serviceBean.setDesc("所填写门店编码不存在或状态无效，请重新输入");
            return serviceBean;
        }
        String  statusCode= (String)map.get("STATUSCODE");
        if(!"01".equals(statusCode)){
            serviceBean.setCode(120);
            serviceBean.setDesc("所填写门店编码不存在或状态无效，请重新输入");
            return serviceBean;
        }

        String q_actAreaOfficeCode = (String)map.get("DEPARTMENTCODE");
        String actAreaOfficeCode = reqBean.getAreaOfficeCode();
        //登录人所属大区办事处是否与门店所属大区办事一致
        if(StringUtil.isNotNullNorEmpty(actAreaOfficeCode)) {
            if(!q_actAreaOfficeCode.startsWith(actAreaOfficeCode)) {
                serviceBean.setCode(121);
                serviceBean.setDesc("门店与活动所属大区办事不一致");
                return serviceBean;
            }

        }
        String actChannelCode = reqBean.getChannelCode();
        String q_channelCode = (String)map.get("CHANNELCODE");
        System.out.println("门店渠道："+q_channelCode+",活动渠道"+actChannelCode);
         if(StringUtil.isNotNullNorEmpty(actChannelCode) && null != q_channelCode) {
            String channels[] = actChannelCode.split(",");
            if (! Arrays.asList(channels).contains(q_channelCode)) {
                serviceBean.setCode(122);
                serviceBean.setDesc("门店与活动所属渠道不一致");
                return serviceBean;
            }
        }
        String terminalName = (String)map.get("TERMINALNAME");
        serviceBean.setResponse(terminalName);
        return serviceBean;
    }

    /**
     * 营销上报明细-补录活动验证
     * @param reqBean
     * @return
     */
    @Override
    public ActRePortWebResBean checkActShowInfo(ActRePortWebReqBean reqBean) throws Exception {
        ActRePortWebResBean arwResBean = new ActRePortWebResBean();

        String sql  =   "    select  " +
                        " to_char(a.start_time, 'yyyy-MM-dd') actStartDate,\n" +
                        "       to_char(a.end_time, 'yyyy-MM-dd') actEndDate,\n" +
                        "       a.area_offic_code areaOfficeCode,\n" +
                        "       a.act_name actName,\n" +
                        "       a.channel_code channelCode\n" +
                        "  from mama100_owner.mkt_sa_act_road_show a\n" +
                        " where 1 = 1\n" +
                        "   and a.end_time <= trunc(sysdate)\n" +
                        "   and a.status = 1    " +
                        "   and a.id = :code ";
        Query query = super.createSQLQuery(sql).addScalar("actStartDate", StringType.INSTANCE)
                .addScalar("actEndDate", StringType.INSTANCE).addScalar("areaOfficeCode", StringType.INSTANCE)
                .addScalar("channelCode", StringType.INSTANCE).addScalar("actName", StringType.INSTANCE);
        query.setParameter("code", reqBean.getCode().trim());
        arwResBean =   (ActRePortWebResBean)query.setResultTransformer(Transformers.aliasToBean(ActRePortWebResBean.class)).uniqueResult();
        return arwResBean;
    }



    /**
     * 查询门店信息
     *
     * @param reqBean
     * @return
     */
    public Map queryTerminalInfo(ActRePortWebReqBean reqBean) {

        String sql = "select dt.department_code  departmentCode,\n" +
                "       t.terminal_channel_code  channelCode,\n" +
                "       to_char(t.short_name)  terminalName,         \n" +
                "       t.status_code  statusCode          \n" +
                "  from mama100_owner.crm_terminal               t,\n" +
                "       mama100_owner.common_department_terminal dt\n" +
                " where 1 = 1                       \n" +
                "   and t.code = dt.terminal_code   \n" +
                "   and t.status_code = '01'   \n" +
                "   and t.code = :terminalCode  ";
        Query query = super.createSQLQuery(sql);
        query.setParameter("terminalCode", reqBean.getTerminalCode().trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map =  (Map)query.uniqueResult();
        return map;
    }

    /**
     * 营销上报明细-上报人验证
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean checkAccountTerminal(ActRePortWebReqBean reqBean) throws Exception {
        ServiceBean serviceBean = new ServiceBean();

        Map map = queryAccountTerminalInfo(reqBean);
        if(null == map){
            serviceBean.setCode(130);
            serviceBean.setDesc("人员与门店无关联。");
            return serviceBean;
        }

        String accountName = (String)map.get("ACCOUNTNAME");
        if(!StringUtil.isNotNullNorEmpty(accountName)){
            serviceBean.setCode(131);
            serviceBean.setDesc("该人员暂无关联门店。");
            return serviceBean;
        }

        serviceBean.setResponse(accountName);
        return serviceBean;
    }

    /**
     * 活动精英上报明细--查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<ActRoadShowDetailResBean> queryActElitesReportDetailList(Pager<ActRoadShowDetailResBean> pager, ActRoadShowListReqBean reqBean) {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);

            String makeupMemo = reqBean.getMakeupMemo();
            if (StringUtil.isNotNullNorEmpty(makeupMemo)) {
                paramsMap.put("makeupMemo", makeupMemo);
            }

            //是否补录（0：否，1：是")
            String actMakeup = reqBean.getActMakeup();
            if (StringUtil.isNotNullNorEmpty(actMakeup)) {
                paramsMap.put("actMakeup", actMakeup.trim());
            }

            /*//图片类别
            String imageMarkType = reqBean.getImageMarkType();
            if (StringUtil.isNotNullNorEmpty(imageMarkType)) {
                paramsMap.put("imageMarkType", imageMarkType.trim());
            }*/
        String sql = SqlUtil.get("ActRoadShowRepositoryImpl.queryActElitesReportDetail.list", new FreeMakerContext(paramsMap));
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, ActRoadShowDetailResBean.class);
    }

    /**
     * 活动精英上报明细--导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportActElitesReportDetailList(HttpServletResponse response, ActRoadShowListReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);

        String makeupMemo = reqBean.getMakeupMemo();
        if (StringUtil.isNotNullNorEmpty(makeupMemo)) {
            paramsMap.put("makeupMemo", makeupMemo);
        }

        //是否补录（0：否，1：是")
        String actMakeup = reqBean.getActMakeup();
        if (StringUtil.isNotNullNorEmpty(actMakeup)) {
            paramsMap.put("actMakeup", actMakeup.trim());
        }

       /* //图片类别
        String imageMarkType = reqBean.getImageMarkType();
        if (StringUtil.isNotNullNorEmpty(imageMarkType)) {
            paramsMap.put("imageMarkType", imageMarkType.trim());
        }*/
        String sql = SqlUtil.get("ActRoadShowRepositoryImpl.queryActElitesReportDetail.list", new FreeMakerContext(paramsMap));

        List<ActRoadShowDetailResBean> bscarList = this.findAllForListBySql(sql,paramsMap,ActRoadShowDetailResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "活动精英上报明细";

        th = new String[]{"活动ID","活动名称","活动类型","考核类型","创建人工号","创建人名称","活动开始日期","活动结束日期","上报品牌",
                "上报门店编码","上报门店名称","门店大区","门店办事处","门店办渠道","省","市","区","街道","上报人工号","上报人名称","上报人岗位","上报人品牌属性","上报人所属公司","上报时间",
                "全品项积分","合生元积分","HT积分","可贝思积分","Dodie积分","奶粉新客","合生元奶粉新客","HT奶粉新客","可贝思奶粉新客","400g奶粉礼包新客","益生菌新客","Dodie新客","营养补充剂新客",
                "全品项积分(均分)","HT积分(均分)","可贝思积分(均分)","Dodie积分(均分)","奶粉新客(均分)","HT奶粉新客(均分)","可贝思奶粉新客(均分)","营养补充剂新客(均分)",
                "SwissePOS销量", "照片1","图片类别1","照片2","图片类别2","照片3","图片类别3","照片4","图片类别4","照片5","图片类别5","照片6","图片类别6",
                "上报备注","是否补录","补录原因"
        };

        colName = new String[] {"actId","actName","actType","khTypeName","actCreatedBy","actCreatedName","startDate","endDate","brand",
                "terminalCode","terminalName","areaTerminal","officeTerminal","channelName","province","city","district","town","createdBy","createdName","postName","reportByBrand","reportByCompany","createdTime",
                "pointTot","pointBiostime","pointMilkHtAsse","pointKbs","pointDodie","newCustMilk","newCustMilkBiostime","newCustMilkHt","newCustKbs8","newCustMilkGift4","newCustProbiotics","dodieNewCustomerNum","addiNutrition",
               "pointTotAvg","pointHtAvg","pointKbsAvg","pointDodieAvg","newCustMilkAvg","newCustMilkHtAvg","newCustKbs8Avg","addiNutritionAvg",
                "swPosNums","img1","mark1","img2","mark2","img3","mark3","img4","mark4","img5","mark5","img6","mark6",
                "actMemo","actMakeup","makeupMemo"
        };

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"),"UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());
    }

    public HashMap<String, Object> queryZhiBoParamsMap(ActRePortZhiBoReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //事业部
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        // 直播主题
        String titleName = req.getTitleName();
        if (StringUtil.isNotNullNorEmpty(titleName)) {
            paramsMap.put("titleName", titleName.trim());
        }
        // 上报人工号
        String createdBy = req.getCreatedBy();
        if (StringUtil.isNotNullNorEmpty(createdBy)) {
            paramsMap.put("createdBy", createdBy.trim());
        }

        // SA编号
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }

        //开始时间
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }



        return paramsMap;
    }
    /**
     * 直播上报报表查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<ActRePortZhiBoReqBean> queryActReportZhiBoList(Pager<ActRePortZhiBoReqBean> pager, ActRePortZhiBoReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryZhiBoParamsMap(reqBean);

        String sql = SqlUtil.get("ActRePortRepositoryImpl.queryZhiBoInfo.list", new FreeMakerContext(paramsMap));
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, ActRePortZhiBoReqBean.class);
    }

    /**
     * 直播上报报表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportActReportZhiBoList(HttpServletResponse response, ActRePortZhiBoReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryZhiBoParamsMap(reqBean);


        String sql = SqlUtil.get("ActRePortRepositoryImpl.queryZhiBoInfo.list", new FreeMakerContext(paramsMap));

        List<ActRePortZhiBoReqBean> bscarList = this.findAllForListBySql(sql,paramsMap,ActRePortZhiBoReqBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "直播上报列表导出";

        th = new String[]{"大区","办事处","上报终端编码","直播主题","活动日期","开始时间","结束时间","直播平台","主播工号","主播名称","辅播工号","辅播名称",
                "直播专家","上报人工号","上报人名称","植入品牌","合作渠道","门店类型","合作门店数量","直播间观看人次","直播间销量","报名社群数","覆盖群人数",
                "活动销售总额（单位：元）","合生元妈妈奶粉新客","合生元婴幼儿奶粉新客","益生菌新客","Dodie新客","羊奶新客","HT新客","图片一","图片二","图片三"};

        colName = new String[] {"areaName","officeName","saCode","titleName","zbDate","startTime","endTime","zbTerraceName","zbCode","zbName","fbCode","fbName",
                "zbExpert","createdBy","createdName","brandName","channelName","terminalTypeName","terminalNums","visitorsNums","zbSales","regGroupNums","coverNums",
                "totalSalesAmount","bstMamaNewCust","bstBabyNewCust","probioticNewCust","dodieNewCust","kbsNewCust","htNewCust","imagePath1","imagePath2","imagePath3"  };

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"),"UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());
    }

    /**
     * 验证上报人员信息
     *
     * @param reqBean
     * @return
     */
    public Map queryAccountTerminalInfo(ActRePortWebReqBean reqBean) {

        String sql =
                "select max(ac.name) accountName \n" +
                        " from bst_mdm.common_account_terminal t,bst_mdm.common_account ac\n" +
                        "where 1=1\n" +
                        "and t.account_id = ac.id\n" +
                        "and ac.status = '01'\n" +
                        "and t.start_date <= trunc(sysdate)\n" +
                        "and (t.end_date is null or t.end_date >=  trunc(sysdate))"+
                        "and t.terminal_code = :terminalCode  "+
                        "and ac.code = :createdBy  ";
        Query query = super.createSQLQuery(sql);
        query.setParameter("terminalCode", reqBean.getTerminalCode().trim());
        query.setParameter("createdBy", reqBean.getCreatedBy().trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map =  (Map)query.uniqueResult();
        return map;
    }

}
