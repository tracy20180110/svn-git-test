package com.biostime.samanage.repository.impl.splitFlow;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.general.CollectionUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.splitFlow.SaSplitFlowBean;
import com.biostime.samanage.domain.splitflow.BstOtoCustSa;
import com.biostime.samanage.repository.splitFlow.SaSplitFlowRepository;
import com.biostime.samanage.util.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

/**
 * SAk客户明细
 */
@Repository("saSplitFlowRepository")
public class SaSplitFlowRepositoryImpl extends BaseOracleRepositoryImpl implements SaSplitFlowRepository {

    @Override
    public Pager<SaSplitFlowBean> searchSaSplitFlow(Pager<SaSplitFlowBean> pager, SaSplitFlowBean saSplitFlowBean) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String sqlStr = "SaSplitFlowRepositoryImpl.searchSaSplitFlow";
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        if (null != saSplitFlowBean) {
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getNursingConsultant())) {
                paramsMap.put("nursingConsultant", saSplitFlowBean.getNursingConsultant());
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getSaCode())) {
                paramsMap.put("saCode", saSplitFlowBean.getSaCode());
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getCustomerId())) {
                paramsMap.put("customerId", saSplitFlowBean.getCustomerId());
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getMobilePhone())) {
                paramsMap.put("mobile", saSplitFlowBean.getMobilePhone());
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getTerminalCode())) {
                paramsMap.put("terminalCode", saSplitFlowBean.getTerminalCode());
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getStartTime())) {
                paramsMap.put("startTime", DateUtils.getTimesmorning(sdf.parse(saSplitFlowBean.getStartTime())));
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getEndTime())) {
                paramsMap.put("endTime", DateUtils.getTimesnight(sdf.parse(saSplitFlowBean.getEndTime())));
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getAreaCode())) {
                paramsMap.put("areaCode", saSplitFlowBean.getAreaCode());
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getOfficeCode())) {
                paramsMap.put("officeCode", saSplitFlowBean.getOfficeCode());
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getTerminalChannelCode())) {
                paramsMap.put("terminalChannelCode", saSplitFlowBean.getTerminalChannelCode());
            }
            if (StringUtil.isNotNullNorEmpty(saSplitFlowBean.getChainChannelCode())) {
                paramsMap.put("chainChannelCode", saSplitFlowBean.getChainChannelCode());
            }        
            
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        PagerQuery pagerQuery = new PagerQuery();
        String SQL = SqlUtil.get(sqlStr, freeMakerContext);
        pagerQuery.setSearchSql(SQL);
        String countSql = "select count(1) from ( " + SQL + " )";
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaSplitFlowBean.class);

    }

    @Override
    public List<SaSplitFlowBean> getSaSplitFlow(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobilePhone, String terminalCode, String startTime, String endTime, String terminalChannelCode, String chainChannelCode) throws ParseException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String sqlStr = "SaSplitFlowRepositoryImpl.searchSaSplitFlow";

        if (StringUtil.isNotNullNorEmpty(nursingConsultant)) {
            paramsMap.put("nursingConsultant", nursingConsultant);
        }
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode);
        }
        if (StringUtil.isNotNullNorEmpty(customerId)) {
            paramsMap.put("customerId", customerId);
        }
        if (StringUtil.isNotNullNorEmpty(mobilePhone)) {
            paramsMap.put("mobile", mobilePhone);
        }
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode);
        }
        if (StringUtil.isNotNullNorEmpty(startTime)) {
            paramsMap.put("startTime", DateUtils.getTimesmorning(sdf.parse(startTime)));
        }
        if (StringUtil.isNotNullNorEmpty(endTime)) {
            paramsMap.put("endTime", DateUtils.getTimesnight(sdf.parse(endTime)));
        }
        if (StringUtil.isNotNullNorEmpty(areaCode)) {
            paramsMap.put("areaCode", areaCode);
        }
        if (StringUtil.isNotNullNorEmpty(officeCode)) {
            paramsMap.put("officeCode", officeCode);
        }
        if (StringUtil.isNotNullNorEmpty(terminalChannelCode)) {
            paramsMap.put("terminalChannelCode", terminalChannelCode);
        }
        if (StringUtil.isNotNullNorEmpty(chainChannelCode)) {
            paramsMap.put("chainChannelCode", chainChannelCode);
        }
        
        
        FreeMakerContext freeMakerContext1 = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get(sqlStr, freeMakerContext1);
        List<SaSplitFlowBean> saSplitFlowBeanList = this.findAllForListBySql(sqls, paramsMap, SaSplitFlowBean.class);
        return saSplitFlowBeanList;
    }

    @Override
    public String getNursingConsultantBySACode(String saCode) {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ca.code nursingConsultant");
        sql.append("  FROM bst_mdm.common_account_terminal  cat,");
        sql.append("       bst_mdm.common_account           ca,");
        sql.append("       bst_mdm.common_account_attr_type caat");
        sql.append(" WHERE cat.account_id = ca.id");
        sql.append("   AND cat.attr_id = caat.id");
        sql.append("   AND caat.id = 41");
        sql.append("   and cat.terminal_code =:saCode");
        sql.append("   and cat.start_date <= sysdate");
        sql.append("   and (cat.end_date >= sysdate or cat.end_date is null)");
        SQLQuery query = this.createSQLQuery(sql.toString());
        query.setParameter("saCode", saCode);
        List list = query.list();
        if (CollectionUtils.isNotEmpty(list)) {
            return list.get(0).toString();
        }
        return null;
    }

    public void addBstOtoCustSA(String saCode, String terminalCode, String customerId, String nursingConsultant) {
        StringBuffer sql = new StringBuffer();
        sql.append(" insert into mama100_owner.bst_oto_cust_sa	");
        sql.append(" (customer_id,relate_code,status,is_valid,is_newcust,relate_type,maintenance_type,terminal_code,created_by,updated_by,sa_code,send_coupon_terminal) ");
        sql.append(" values ");
        sql.append(" (:customerId,:nursingConsultant,'1','1','0','1','2',:terminalCode,:nursingConsultant,:nursingConsultant,:saCode,:saCode) ");
        SQLQuery query = this.createSQLQuery(sql.toString());
        query.setParameter("saCode", saCode);
        query.setParameter("terminalCode", terminalCode);
        query.setParameter("customerId", customerId);
        query.setParameter("nursingConsultant", nursingConsultant);
        query.executeUpdate();
    }

    @Override
    public void updateBstOtoCustSA(BstOtoCustSa bstOtoCustSa) {
        super.getHibernateTemplate().update(bstOtoCustSa);

    }

    @Override
    public void saveBstOtoCustSA(BstOtoCustSa bstOtoCustSa) throws IllegalAccessException {
        this.save(bstOtoCustSa, BstOtoCustSa.KEY);
    }

    @Override
    public String getRelationChainCodeBySACode(String sendCouponTerminal) {
        StringBuffer sql = new StringBuffer();
        sql.append("select mtm.relation_terminal_code");
        sql.append("  from mama100_owner.mkt_sa_terminal_manage mtm");
        sql.append(" where mtm.start_date <= trunc(sysdate)");
        sql.append("   and (mtm.end_date is null or mtm.end_date >= trunc(sysdate))");
        sql.append("   and mtm.status = 1");
        sql.append("   and mtm.sa_code =:sendCouponTerminal");
        SQLQuery query = this.createSQLQuery(sql.toString());
        query.setParameter("sendCouponTerminal", sendCouponTerminal);
        List list = query.list();
        if (CollectionUtils.isNotEmpty(list) && null != list.get(0)) {
            return list.get(0).toString();
        }
        return null;
    }

    @Override
    public BstOtoCustSa getBstOtoCustSAByCustomerId(String customerId) {
        List<BstOtoCustSa> bstOtoCustSaList = this.createQuery(" from BstOtoCustSa t where t.customerId = ? order by t.createdTime desc")
                .setParameter(0, Long.parseLong(customerId)).list();
        if (!CollectionUtil.isEmptyList(bstOtoCustSaList)) {
            return bstOtoCustSaList.get(0);
        }
        return null;
    }
}
