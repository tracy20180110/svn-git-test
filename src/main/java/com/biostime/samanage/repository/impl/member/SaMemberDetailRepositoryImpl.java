package com.biostime.samanage.repository.impl.member;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.member.CommonDepartmentBean;
import com.biostime.samanage.bean.member.MemberDetailBean;
import com.biostime.samanage.bean.member.MktImportInfoBean;
import com.biostime.samanage.domain.*;
import com.biostime.samanage.repository.member.SaMemberDetailRepository;
import com.biostime.samanage.util.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * SA会员明细
 */
@Repository("saMemberDetailRepository")
public class SaMemberDetailRepositoryImpl extends BaseOracleRepositoryImpl implements SaMemberDetailRepository {
    /**
     * 分页查询SA会员明细
     *
     * @param pager
     * @param memberDetailBean
     * @return
     */

    @Override
    public Pager<MemberDetailBean> searchSaMenberDetail(Pager<MemberDetailBean> pager, MemberDetailBean memberDetailBean) {

        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String sqlStr = "SaMemberDetailRepositoryImpl.searchSaMenberDetail.list";
        if (null != memberDetailBean) {
            if (StringUtil.isNotNullNorEmpty(memberDetailBean.getNursingConsultant())) {
                paramsMap.put("nursingConsultant", memberDetailBean.getNursingConsultant());
            }
            if (StringUtil.isNotNullNorEmpty(memberDetailBean.getAreaCode())) {
                paramsMap.put("areaCode", memberDetailBean.getAreaCode());
            }
            if (StringUtil.isNotNullNorEmpty(memberDetailBean.getOfficeCode())) {
                paramsMap.put("officeCode", memberDetailBean.getOfficeCode());
            }
            if (StringUtil.isNotNullNorEmpty(memberDetailBean.getSaCode())) {
                paramsMap.put("saCode", memberDetailBean.getSaCode());
            }
            if (null != memberDetailBean.getCustomerId()) {
                paramsMap.put("customerId", memberDetailBean.getCustomerId());
            }
            if (StringUtil.isNotNullNorEmpty(memberDetailBean.getMobile())) {
                paramsMap.put("mobile", memberDetailBean.getMobile());
            }
            if (StringUtil.isNotNullNorEmpty(memberDetailBean.getProBcc())) {
                paramsMap.put("proBcc", memberDetailBean.getProBcc());
            }
            if (StringUtil.isNotNullNorEmpty(memberDetailBean.getSrcLocName())) {
                paramsMap.put("srcLocName", memberDetailBean.getSrcLocName());
            }


            if (null != memberDetailBean.getStartTime()) {
                Calendar now = Calendar.getInstance();
                int day = now.get(Calendar.DAY_OF_MONTH);
                //当前日期大于25号
                int tempDay = DateUtils.getTempDay(sdf, now, day);

                int startDay = Integer.parseInt(sdf.format(memberDetailBean.getStartTime()));
                /**
                 * 如果开始时间小于目标时间，则查询历史明细表
                 */
                if (startDay < tempDay) {
                    sqlStr = "SaMemberDetailRepositoryImpl.searchSaMenberDetail.history.list";
                }
                paramsMap.put("startTime", DateUtils.getTimesmorning(memberDetailBean.getStartTime()));
            }
            if (null != memberDetailBean.getEndTime()) {
                paramsMap.put("endTime", DateUtils.getTimesnight(memberDetailBean.getEndTime()));

            }
        }

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        String sql = SqlUtil.get(sqlStr, freeMakerContext);

        String countSql = "select count(1) from ( " + sql + " )";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, MemberDetailBean.class);

    }

    /**
     * 导出查询SA会员明细
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param operateBcc
     * @param srcLocName
     * @param startTime
     * @param endTime
     * @return
     * @throws ParseException
     */
    @Override
    public List<MemberDetailBean> getSaMenberDetail(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobile, String proBcc, String srcLocName, String startTime, String endTime) throws ParseException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String sqlStr = "SaMemberDetailRepositoryImpl.searchSaMenberDetail.list";

        if (StringUtil.isNotNullNorEmpty(nursingConsultant)) {
            paramsMap.put("nursingConsultant", nursingConsultant);
        }
        if (StringUtil.isNotNullNorEmpty(areaCode)) {
            paramsMap.put("areaCode", areaCode);
        }
        if (StringUtil.isNotNullNorEmpty(officeCode)) {
            paramsMap.put("officeCode", officeCode);
        }
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode);
        }
        if (StringUtil.isNotNullNorEmpty(customerId)) {
            paramsMap.put("customerId", customerId);
        }
        if (StringUtil.isNotNullNorEmpty(mobile)) {
            paramsMap.put("mobile", mobile);
        }
        if (StringUtil.isNotNullNorEmpty(proBcc)) {
            paramsMap.put("proBcc", proBcc);
        }
        if (StringUtil.isNotNullNorEmpty(srcLocName)) {
            paramsMap.put("srcLocName", srcLocName);
        }
        if (StringUtil.isNotNullNorEmpty(startTime)) {
            Date s = sdf1.parse(startTime);
            Calendar now = Calendar.getInstance();
            int day = now.get(Calendar.DAY_OF_MONTH);
            //当前日期大于25号
            int tempDay = DateUtils.getTempDay(sdf, now, day);
            int startDay = Integer.parseInt(sdf.format(s));
            /**
             * 如果开始时间小于目标时间，则查询历史明细表
             */
            if (startDay < tempDay) {
                sqlStr = "SaMemberDetailRepositoryImpl.searchSaMenberDetail.history.list";
            }
            paramsMap.put("startTime", DateUtils.getTimesmorning(s));
        }
        if (StringUtil.isNotNullNorEmpty(endTime)) {
            Date e = sdf1.parse(endTime);
            paramsMap.put("endTime", DateUtils.getTimesnight(e));
        }

        FreeMakerContext freeMakerContext1 = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get(sqlStr, freeMakerContext1);
        List<MemberDetailBean> memberDetailBeanList = this.findAllForListBySql(sqls, paramsMap, MemberDetailBean.class);
        return memberDetailBeanList;
    }

    @Override
    public Pager<MktImportInfoBean> searchMktImportInfo(Pager<MktImportInfoBean> pager, MktImportInfoBean importInfoBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        paramsMap.put("type", "sa_new_member");
        if (null != importInfoBean.getStartTime()) {
            paramsMap.put("startTime", DateUtils.getTimesmorning(importInfoBean.getStartTime()));
        }
        if (null != importInfoBean.getEndTime()) {
            paramsMap.put("endTime", DateUtils.getTimesnight(importInfoBean.getEndTime()));
        }
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        String sql = SqlUtil.get("SaMemberDetailRepositoryImpl.searchMktImportInfo.list", freeMakerContext);

        String countSql = SqlUtil.get("SaMemberDetailRepositoryImpl.searchMktImportInfo.count", freeMakerContext);

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, MktImportInfoBean.class);
    }

    @Override
    public void saveMktImportInfoBean(MktImportInfo importInfo) throws IllegalAccessException {
        this.add(importInfo, MktImportInfo.KEY);
    }

    @Override
    public SaTerminal getSAByCode(String saCode) {
        Query query = this.createQuery("from SaTerminal po where po.code=:code and po.terminalChannelCode = '08'");
        query.setParameter("code", saCode);
        SaTerminal saTerminal = (SaTerminal) query.uniqueResult();
        return saTerminal;
    }

    @Override
    public CommonAccountList getCommonAccountListCode(String accountId, int type) {
        if (StringUtil.isNullOrEmpty(accountId)) {
            return null;
        }
        String sql = "";
        if (type == 0) {
            sql = "SaMemberDetailRepositoryImpl.getCommonAccountListCode.operateBcc";
        } else {
            sql = "SaMemberDetailRepositoryImpl.getCommonAccountListCode.nursingConsultant";
        }
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("accountId", accountId);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        sql = SqlUtil.get(sql, freeMakerContext);
        List<CommonAccountList> commonAccountLists = this.findAllForListBySql(sql, paramsMap, CommonAccountList.class);
        if (CollectionUtils.isNotEmpty(commonAccountLists)) {
            return commonAccountLists.get(0);
        }
        return null;
    }

    @Override
    public HpSrcLoc getHpSrcLoc(String srcLocName) {
        Query query = this.createQuery(" from HpSrcLoc where name = :srcLocName");
        query.setParameter("srcLocName", srcLocName);
        HpSrcLoc hpSrcLoc = (HpSrcLoc) query.uniqueResult();
        return hpSrcLoc;
    }

    @Override
    public SaTerminal getTerminalByCode(String saCode) {
        Query query = this.createQuery("from SaTerminal po where po.code=:code ");
        query.setParameter("code", saCode);
        SaTerminal saTerminal = (SaTerminal) query.uniqueResult();
        return saTerminal;
    }

    @Override
    public List<CommonDepartment> queryOfficeCode(String saOfficeCode, String areaCode) {

        Query query = this.createQuery(" from CommonDepartment where code = :officeCode or code =:areaCode order by length(code) asc");
        query.setParameter("officeCode", saOfficeCode);
        query.setParameter("areaCode", areaCode);
        List<CommonDepartment> commonDepartmentList = query.list();
        return commonDepartmentList;
    }

    @Override
    public List<CommonDepartmentBean> queryAreaAndOfficeCode(String userId) {


        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        if (StringUtil.isNullOrEmpty(userId)) {
            return null;
        }
        paramsMap.put("userId", userId);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sqls = SqlUtil.get("SaMemberDetailRepositoryImpl.queryAreaAndOfficeCode", freeMakerContext);
        List<CommonDepartmentBean> commonDepartmentBeanList = this.findAllForListBySql(sqls, paramsMap, CommonDepartmentBean.class);

        return commonDepartmentBeanList;
    }

    @Override
    public void saveCrmCustTerminalChannelJpa(CrmCustTerminalChannelJpa crmCustChannelJpa) {
        super.add(crmCustChannelJpa);
    }

    @Override
    public int getCrmCustomerCount(Long customerId, String channelCode) {
        String sql = "select count(id) from CRM_CUST_TERMINAL_CHANNEL where CUSTOMER_ID = :customerId  and TERMINAL_CHANNEL_CODE = :channelCode";

        SQLQuery query = this.createSQLQuery(sql);
        query.setParameter("customerId", customerId);
        query.setParameter("channelCode", channelCode);
        BigDecimal count = (BigDecimal) query.uniqueResult();

        return Integer.parseInt(count.toString());
    }

    @Override
    public CrmCustomer getCustomerById(Long customerId) {
        Query query = this.createQuery("from CrmCustomer po where po.id=:id ");
        query.setParameter("id", customerId);
        CrmCustomer crmCustomer = (CrmCustomer) query.uniqueResult();
        return crmCustomer;
    }

    @Override
    public void addMemberImportRec(HpMemberEventRec hpMemberEventRec) {
        super.add(hpMemberEventRec);
    }
}
