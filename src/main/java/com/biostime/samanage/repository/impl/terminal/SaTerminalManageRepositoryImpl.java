package com.biostime.samanage.repository.impl.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.terminal.*;
import com.biostime.samanage.domain.MktImportInfo;
import com.biostime.samanage.domain.terminal.SaTerminalManage;
import com.biostime.samanage.repository.terminal.SaTerminalManageRepository;
import com.biostime.samanage.util.DateUtils;
import com.biostime.samanage.util.ExcelUtil;
import com.biostime.samanage.util.UploadUtil;
import com.biostime.samanage.util.WsImageFileUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Describe:SA与门店管理RepositoryImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Repository("saTerminalManageRepository")
public class SaTerminalManageRepositoryImpl extends BaseOracleRepositoryImpl implements SaTerminalManageRepository {

    private static String IMPORT_TYPE = "mkt_sa_terminal_manage";

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryManageImportParamsMap(SaTerminalManageImportReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //活动开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //活动结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        return paramsMap;
    }
    /**
     * SA与门店管理导入列表
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaTerminalManageImportResBean> querySaTerminalManageImportList(Pager<SaTerminalManageImportResBean> pager, SaTerminalManageImportReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryManageImportParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalManageRepositoryImpl.querySaTerminalManageImportList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaTerminalManageImportResBean.class);
    }

    /**
     * SA与门店管理导入列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaTerminalManageImportList(HttpServletResponse response, SaTerminalManageImportReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryManageImportParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalManageRepositoryImpl.querySaTerminalManageImportList.list", freeMakerContext);
        List<SaTerminalManageImportResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaTerminalManageImportResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "SA与门店导入列表";

        th = new String[]{"文件路径","总数","创建时间","创建人工号","创建人名称"};

        colName = new String[] {"filePath","totalCount","createdTime","createdBy","createdName"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryManageParamsMap(SaTerminalManageReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //sa编码
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }

        //门店编码
        String terminalCode = req.getTerminalCode();
        if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode.trim());
        }

        //是否关联门店
        String relationTerminal = req.getRelationTerminal();
        if (StringUtil.isNotNullNorEmpty(relationTerminal)) {
            paramsMap.put("relationTerminal", relationTerminal.trim());
        }

        //有效关系
        String relation = req.getRelation();
        if (StringUtil.isNotNullNorEmpty(relation)) {
            paramsMap.put("relation", relation.trim());
        }

        //关联连锁门店编码
        String relationTerminalCode = req.getRelationTerminalCode();
        if (StringUtil.isNotNullNorEmpty(relationTerminalCode)) {
            paramsMap.put("relationTerminalCode", relationTerminalCode.trim());
        }


        //是否有效关系（旧）
        String type = req.getType();
        if (StringUtil.isNotNullNorEmpty(type)) {
            paramsMap.put("type", type.trim());
        }
        return paramsMap;
    }
    /**
     * SA与门店管理列表
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaTerminalManageResBean> querySaTerminalManageList(Pager<SaTerminalManageResBean> pager, SaTerminalManageReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryManageParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalManageRepositoryImpl.querySaTerminalManageList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaTerminalManageResBean.class);
    }


    /**
     * SA与门店管理--导入
     *
     * @param multipartFile
     * @param request
     * @throws Exception
     */
    @Override
    public List importSaTerminalManage(MultipartFile multipartFile, HttpServletRequest request) throws Exception {

        String createdBy = request.getParameter("createdBy"); //创建人
        String createdName = request.getParameter("createdName"); //创建名称
        String areaOfficeCode = request.getParameter("areaOfficeCode"); //大区办事处编码
//        String channelCode = request.getParameter("channelCode"); //渠道
        //时间验证
        String dateMatches = "\\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[0-1])";
        //存放不符合要求的信息提示。
        List list = new ArrayList();
        String fileName = multipartFile.getOriginalFilename();
        boolean flag = false;
        if(fileName.endsWith(".xls")){
            flag = true;
        }
        InputStream inputXLS = multipartFile.getInputStream();
        ExcelUtil excelReader = new ExcelUtil();
        Map<Integer, String[]> map = excelReader.readExcelContent(inputXLS,flag);
        List<SaTerminalManage> stmList = new ArrayList<SaTerminalManage>();

        for (int i = 1; i <= map.size(); i++) {
            String[] str = map.get(i);
            int j = 0;
            SaTerminalManage stmBean = new SaTerminalManage();
            //门店
            String terminalCode = str[j++].trim();
            if(terminalCode.endsWith(".0")){
                terminalCode = terminalCode.substring(0,terminalCode.indexOf("."));
            }

            stmBean.setTerminalCode(terminalCode);
            //获取门店对应的大区办事处编码、渠道
            Map terminalCodeMap = getTerminalAreaOfficeCode(terminalCode,"");

            if(null == terminalCodeMap){
                list.add("【行数】第" + i + "行，【失败原因】门店编码不存在或没有大区办事处。");
                continue;
            }
            String terminal_departmentCode = (String) terminalCodeMap.get("departmentCode");
//            String q_channelCode = (String) codeMap.get("CHANNELCODE");
            System.out.println(terminal_departmentCode);
            //登录人所属大区办事处是否与门店所属大区办事一致
            if(StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
                if (!terminal_departmentCode.startsWith(areaOfficeCode)) {
                    list.add("【行数】第" + i + "行，【失败原因】您与门店所属大区办事不一致。");
                    continue;
                }
            }


            //门店
            String saCode = str[j++].trim();
            if(saCode.endsWith(".0")){
                saCode = saCode.substring(0,saCode.indexOf("."));
            }

            stmBean.setSaCode(saCode);
            //获取门店对应的大区办事处编码、渠道
            Map saCodeMap = getTerminalAreaOfficeCode(saCode,"08");

            if(null == saCodeMap){
                list.add("【行数】第" + i + "行，【失败原因】SA编码不存在或不是SA渠道。");
                continue;
            }
            String sa_departmentCode = (String) saCodeMap.get("departmentCode");
            System.out.println(sa_departmentCode);
            //登录人所属大区办事处是否与门店所属大区办事一致
            if(StringUtil.isNotNullNorEmpty(sa_departmentCode)) {
                if (!terminal_departmentCode.equals(sa_departmentCode)) {
                    list.add("【行数】第" + i + "行，【失败原因】SA与门店所属大区办事不一致。");
                    continue;
                }
            }

            /*if(StringUtil.isNotNullNorEmpty(channelCode)) {
                String channels[] = channelCode.split(",");
                if (! Arrays.asList(channels).contains(q_channelCode)) {
                    sb.append("第" + i + "行，您与门店所属渠道不一致。");
                    continue;
                }
            }
            String actChannelCode = arsBean.getChannelCode();
            System.out.println("门店渠道："+q_channelCode+",活动渠道"+actChannelCode);
            if(StringUtil.isNotNullNorEmpty(actChannelCode)) {
                if (!q_channelCode.equals(actChannelCode)) {
                    sb.append("第" + i + "行，门店与活动所属渠道不一致。");
                    continue;
                }
            }*/
            //活动备案开始日期
            String startDate = str[j++].trim();
            if (StringUtil.isNullOrEmpty(startDate)) {
                list.add("【行数】第" + i + "行，【失败原因】开始日期不能为空。");
                continue;
            } else if (!startDate.matches(dateMatches)) {
                list.add("【行数】第" + i + "行，【失败原因】日期格式不对，格式举例2017-01-01。");
                continue;
            }
            Date sDate = DateUtils.stringToDate(startDate, "yyyy-MM-dd");
            if(!(sDate.after(new Date()) || startDate.equals(DateUtils.dateToString(new Date(),"yyyy-MM-dd")))){
                list.add("【行数】第" + i + "行，【失败原因】开始日期不能小于今天。");
                continue;
            }
           /* //活动备案开始日期
            String endDate = str[j++].trim();
            Date eDate = null;
            if (StringUtil.isNotNullNorEmpty(endDate)) {
                 if (!endDate.matches(dateMatches)) {
                     list.add("第" + i + "行，不是有效的结束日期。");
                     continue;
                 }
                eDate = DateUtils.stringToDate(endDate, "yyyy-MM-dd");
                //校验开始时间与结束时间，开始时间需小于结束时间
                if (null != sDate && eDate.before(sDate)) {
                    list.add("第" + i + "行，开始日期不能大于结束日期，请重新导入。");
                    continue;
                }
            }*/

            //如果SA与门店在这个时间段是否已经存在关系
            if(querySaTerminal(saCode,sDate,null,"")){
                //验证是否有交叉
                list.add("【行数】第" + i + "行，【失败原因】该时间段已存在关系，请重新导入。");
                continue;
            }

            stmBean.setStartDate(sDate);
//            stmBean.setEndDate(eDate);
            stmBean.setStatus(1);
            stmBean.setCreatedBy(createdBy);
            stmBean.setCreatedName(createdName);
            stmBean.setCreatedTime(new Date());

            boolean flags = true;
            if(stmList.size() >0 ){
                for(SaTerminalManage stmVo :stmList){
                    if(stmBean.getSaCode().equals(stmVo.getSaCode())
                            && stmBean.getTerminalCode().equals(stmVo.getTerminalCode())){
                        flags = false;
                        list.add("【行数】第" + i + "行，【失败原因】SA编码"+stmBean.getSaCode()+"在导入列表中存在重复记录。");
                        continue;
                    }
                }
            }
            if(flags){
                stmList.add(stmBean);
            }
        }
        //如果导入的整个Excel列表的内容都满足，则保持上传的文件，并且添加数据
        if(list.size() == 0){

            String pathDir = WsImageFileUtil.getFilePathDir(WsImageFileUtil.SA_TERMINAL_MANAGE);
            String show_filePath = "http://" + WsImageFileUtil.UPLOAD_IP + request.getContextPath() + pathDir;

            String  upload_filePath = WsImageFileUtil.getUploadDir(WsImageFileUtil.SA_TERMINAL_MANAGE);
            fileName = System.currentTimeMillis() + "_"+ fileName;
            UploadUtil.uploadFile(multipartFile, request, upload_filePath, fileName);

            MktImportInfo mktImportInfo = new MktImportInfo();
            mktImportInfo.setType(IMPORT_TYPE);
            mktImportInfo.setFileName(fileName);
            mktImportInfo.setFilePath(show_filePath+fileName);
            mktImportInfo.setTotalCount(stmList.size());
            mktImportInfo.setCreatedBy(createdBy);
            mktImportInfo.setCreatedName(createdName);
            mktImportInfo.setCreatedTime(new Date());

            System.out.println("----upload_filePath----"+upload_filePath);
            System.out.println("----filePath----"+mktImportInfo.getFilePath());
            if(stmList.size() > 0 ){
                super.batchAdd(stmList, SaTerminalManage.KEY, "MAMA100_OWNER");
            }
            super.add(mktImportInfo, MktImportInfo.KEY, "MAMA100_OWNER");

        }
        return list;
    }

    /**
     * SA与门店管理--删除
     * @param reqBean
     * @return
     */
    @Override
    public void deleteSaTerminalManage(SaTerminalManageEditReqBean reqBean) throws Exception {
        SaTerminalManage stmBean = super.get(SaTerminalManage.class, Long.valueOf(reqBean.getId()));
        stmBean.setStatus(0);
        stmBean.setUpdatedTime(new Date());
        stmBean.setUpdatedBy(reqBean.getCreatedBy());
        super.getHibernateTemplate().update(stmBean);
    }

    /**
     * SA与门店管理-编辑
     * @param reqBean
     * @return
     */
    @Override
    public String editSaTerminalManage(SaTerminalManageEditReqBean reqBean) {
        String msg = "";
        SaTerminalManage stmBean = super.get(SaTerminalManage.class, Long.valueOf(reqBean.getId()));
        if(null != stmBean) {
            if(querySaTerminal(stmBean.getSaCode(), DateUtils.stringToDate(reqBean.getStartDate(), "yyyy-MM-dd"),
                    DateUtils.stringToDate(reqBean.getEndDate(), "yyyy-MM-dd"), reqBean.getId())){
                //验证是否有交叉
                msg = "该时间段关系已存在。";
            }else{
                stmBean.setStartDate(DateUtils.stringToDate(reqBean.getStartDate(), "yyyy-MM-dd"));
                stmBean.setEndDate(DateUtils.stringToDate(reqBean.getEndDate(), "yyyy-MM-dd"));
                stmBean.setUpdatedTime(new Date());
                stmBean.setUpdatedBy(reqBean.getCreatedBy());
                super.getHibernateTemplate().update(stmBean);
            }

        }else{
            msg = "SA与门店关系不存在。";
        }
        return msg;
    }



    /**
     * 查询门店、SA是否存在
     * @param saCode
     * @param sDate
     * @param eDate
     * @param id
     * @return
     */
    public boolean querySaTerminal(String saCode,Date sDate,Date eDate,String id) {
        boolean flag = false;
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }
        /*if (StringUtil.isNotNullNorEmpty(terminalCode)) {
            paramsMap.put("terminalCode", terminalCode.trim());
        }*/

        if (StringUtil.isNotNullNorEmpty(id)) {
            paramsMap.put("id", id.trim());
        }
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalManageRepositoryImpl.querySaTerminal", freeMakerContext);

        List<SaTerminalManageQueryInfo> list = this.findAllForListBySql(sql, paramsMap,SaTerminalManageQueryInfo.class);

        if (list.size() > 0) {
            for(SaTerminalManageQueryInfo stmQInfo :list){
                Date startDate = DateUtils.stringToDate(stmQInfo.getStartDate(), "yyyy-MM-dd");
                Date endDate = null;
                if(null != stmQInfo.getEndDate()) {
                    endDate = DateUtils.stringToDate(stmQInfo.getEndDate(), "yyyy-MM-dd");
                }


                if(StringUtil.isNotNullNorEmpty(id)) {  //更新操作
                    if(null != endDate){
                        if(null != eDate){
                            if(eDate.equals(endDate) || eDate.equals(startDate)){  //开始日期不能等于结束日期
                                flag = true;
                            }
                            if(eDate.after(startDate) && eDate.before(endDate)){
                                flag = true;
                            }
                        }else{
                            if(sDate.equals(endDate) || sDate.equals(startDate) ){  //开始日期不能等于结束日期
                                flag = true;
                            }
                            if(sDate.before(endDate)){
                                flag = true;
                            }
                        }
                    }else{
                        if(null != eDate){
                            if(sDate.equals(startDate) || eDate.equals(startDate) ){  //开始日期不能等于结束日期
                                flag = true;
                            }
                            if(startDate.after(sDate) && startDate.before(eDate)){
                                flag = true;
                            }
                        }else{
                            flag = true;
                        }
                    }

                }else{ //保存

                    if(null != endDate){
                        if(sDate.equals(endDate) || sDate.equals(startDate)){  //开始日期不能等于结束日期
                            flag = true;
                        }
                        if(sDate.before(endDate)){
                            flag = true;
                        }
                    }else{
                        flag = true;
                    }
                }
            }
        }

        return flag;
    }

    /**
     * 获取门店大区办事编码
     *
     * @param terminalCode
     * @return
     */
    public Map getTerminalAreaOfficeCode(String terminalCode,String channelCode) {
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("select t.department_code departmentCode,t.channel_code channelCode " +
                    " from mama100_owner.common_department_terminal t " +
                    " where 1=1" +
                    " and t.terminal_code = :terminalCode");
            if (StringUtil.isNotNullNorEmpty(channelCode)) {
                sql.append(" and t.channel_code = :channelCode");
            }

            Query query = super.createSQLQuery(sql.toString());
            query.setParameter("terminalCode", terminalCode.trim());
            if (StringUtil.isNotNullNorEmpty(channelCode)) {
                query.setParameter("channelCode", channelCode.trim());
            }
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            return (Map) query.uniqueResult();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    /**
     * SA撤销结束与门店的关系
     *
     * @param reqBean
     * @return
     */
    @Override
    public void delSaTerminalRelation(SaTerminalManageReqBean reqBean) {
        //查询SA编码是否是注销
        String sql =    "update  mama100_owner.mkt_sa_terminal_manage t\n" +
                        "set t.end_date = trunc(sysdate),\n" +
                        "    t.updated_by = :createdBy,\n" +
                        "    t.updated_time = sysdate,\n" +
                        "    t.memo = 'SA撤销结束关系'\n" +
                        " where 1=1\n" +
                        " and (t.end_date >=sysdate or t.end_date is null)\n" +
                        " and t.sa_code = :saCode";
        Query query = this.createSQLQuery(sql);
        query.setParameter("createdBy",reqBean.getCreatedBy());
        query.setParameter("saCode",reqBean.getSaCode());
        query.executeUpdate();
    }

    /**
     * SA与门店管理列表（新）
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaTerminalManageResBean> querySaTerminalNewManageList(Pager<SaTerminalManageResBean> pager, SaTerminalManageReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryManageParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalManageRepositoryImpl.querySaTerminalManageList.new.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaTerminalManageResBean.class);
    }


    /**
     * SA与门店管理列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaTerminalManageList(HttpServletResponse response, SaTerminalManageReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryManageParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalManageRepositoryImpl.querySaTerminalManageList.new.list", freeMakerContext);
        List<SaTerminalManageResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaTerminalManageResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "SA与门店管理";

        th = new String[]{"大区","办事处","有效关系","SA编码","SA名称","1店编码","1店名称","连锁店所属体系","关联连锁门店名称","关联连锁门店编码","是否关联门店","开始时间","结束时间","操作人工号","操作人名称"};

        colName = new String[] {"areaName","officeName","relation","saCode","saName","terminalCode","terminalName","topTerminalName",
                "relationTerminalCode","relationTerminalName","relationTerminal","startDate","endDate","updatedBy","updatedName"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
    /**
     * SA与门店绑定（新）
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSaTerminalRelation(SaTerminalManageReqBean reqBean) throws Exception {
        ServiceBean serviceBean = new ServiceBean();

        Map map = queryTerminalInfo(reqBean.getTerminalCode());
        //验证主门店是否是连锁编码

        if(StringUtil.isNotNullNorEmpty(reqBean.getTerminalCode())){
            //SA门店大区办事处编码
            String saAreaOfficeCode = reqBean.getAreaOfficeCode();
            if(null == map){
                serviceBean.setCode(200);
                serviceBean.setDesc("1店编码不存在");
                return serviceBean;
            }
            String type = (String) map.get("terminalType");
            String[] t = {"1","2","3"};
            if(StringUtil.isNotNullNorEmpty(type)){
                if(!Arrays.asList(t).contains(type)){
                    serviceBean.setCode(200);
                    serviceBean.setDesc("1店类型只能为个体店、连锁店");
                    return serviceBean;
                }
            }else{
                serviceBean.setCode(200);
                serviceBean.setDesc("1店类型不能为空");
                return serviceBean;
            }

            String statusCode = (String) map.get("statusCode");
            if(StringUtil.isNotNullNorEmpty(statusCode)){
                if(!"01".equals(statusCode)){
                    serviceBean.setCode(200);
                    serviceBean.setDesc("1店编码状态非正常");
                    return serviceBean;
                }
            }else{
                serviceBean.setCode(200);
                serviceBean.setDesc("1店编码状态不能为空");
                return serviceBean;
            }

            String departmentCode = (String) map.get("departmentCode");
            if(!saAreaOfficeCode.equals(departmentCode)){
                serviceBean.setCode(200);
                serviceBean.setDesc("1店编码办事处与SA办事处不一致");
                return serviceBean;
            }
        }else{
            serviceBean.setCode(200);
            serviceBean.setDesc("1店编码不能为空");
            return serviceBean;
        }

        Map map2 = queryTerminalInfo(reqBean.getRelationTerminalCode());
        //关联连锁门店不为空
        if(StringUtil.isNotNullNorEmpty(reqBean.getRelationTerminalCode())){
            //关联连锁门店只要是连锁门店就可以帮到
            if(null != map2){
                String statusCode = (String) map2.get("statusCode");
                if(StringUtil.isNotNullNorEmpty(statusCode)){
                    if(!"01".equals(statusCode)){
                        serviceBean.setCode(200);
                        serviceBean.setDesc("关联连锁门店编码状态非正常");
                        return serviceBean;
                    }
                }else{
                    serviceBean.setCode(200);
                    serviceBean.setDesc("关联连锁门店编码状态不能为空");
                    return serviceBean;
                }

                //主店为单体店
                String type2 = (String) map2.get("terminalType");
                String type = (String) map.get("terminalType");

                if(StringUtil.isNotNullNorEmpty(type2)){
                    if("3".equals(type)){
                        String[] t = {"1","2"};
                        if(!Arrays.asList(t).contains(type2)){
                            serviceBean.setCode(200);
                            serviceBean.setDesc("关联连锁门店类型只能为连锁店");
                            return serviceBean;
                        }
                    }else{
                        //验证如果关联连锁门店编码为全国重点连锁

                        if(queryCount(reqBean.getTerminalCode(),reqBean.getTerminalCode(),true) > 0){
                            //全国重点连锁 验证是否同一连锁体系
                            if(queryCount(reqBean.getTerminalCode(),reqBean.getRelationTerminalCode(),false) != 1){
                                serviceBean.setCode(200);
                                serviceBean.setDesc("关联连锁门店编码与1店不在同一连锁体系内");
                                return serviceBean;
                            }
                        }else{
                            String[] t = {"1","2"};
                            if(!Arrays.asList(t).contains(type2)){
                                serviceBean.setCode(200);
                                serviceBean.setDesc("关联连锁门店类型只能为连锁店");
                                return serviceBean;
                            }
                        }
                    }
                }else{
                    serviceBean.setCode(200);
                    serviceBean.setDesc("关联连锁门店类型不能为空");
                    return serviceBean;
                }
            }else{
                serviceBean.setCode(200);
                serviceBean.setDesc("关联连锁门店编码不存在");
                return serviceBean;
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SaTerminalManage saTerminalManage = new SaTerminalManage();
        saTerminalManage.setSaCode(reqBean.getSaCode());
        saTerminalManage.setTerminalCode(reqBean.getTerminalCode());
        saTerminalManage.setStartDate(sdf.parse(reqBean.getStartDate()));
        if(StringUtils.isNotBlank(reqBean.getEndDate())){
            saTerminalManage.setEndDate(sdf.parse(reqBean.getEndDate()));
        }
        saTerminalManage.setRelationTerminalCode(reqBean.getRelationTerminalCode());
        saTerminalManage.setStatus(1);
        saTerminalManage.setCreatedTime(new Date());
        saTerminalManage.setCreatedBy(reqBean.getCreatedBy());
        saTerminalManage.setCreatedName(reqBean.getCreatedName());
        saTerminalManage.setUpdatedBy(reqBean.getCreatedBy());
        saTerminalManage.setUpdatedName(reqBean.getCreatedName());
        saTerminalManage.setUpdatedTime(new Date());

        //根据ID查询出对象，说明已经绑定过门店，然后更新end_date时间  status 无效
        if(StringUtil.isNotNullNorEmpty(reqBean.getId())){
            SaTerminalManage editBean = super.get(SaTerminalManage.class, Long.valueOf(reqBean.getId()));

            editBean.setUpdatedBy(reqBean.getCreatedBy());
            editBean.setUpdatedName(reqBean.getCreatedName());
            editBean.setUpdatedTime(new Date());

            //如果1店门店没有更改过，就不用新增，只修改。
            if(reqBean.getTerminalCode().equals(editBean.getTerminalCode())){
//                editBean.setTerminalCode(reqBean.getTerminalCode());
                editBean.setRelationTerminalCode(reqBean.getRelationTerminalCode());


                boolean flag = true;
                //验证时间是否有冲突 同一个SA、开始时间
                List<SaTerminalManage> stmList = getSaTerminalManageList(reqBean);

                for(SaTerminalManage stm:stmList) {
                    if (null == stm.getEndDate() && (!reqBean.getId().equals(String.valueOf(stm.getId())))) {
                        flag = false;
                        Date sDate = stm.getStartDate();
                        if(StringUtils.isNotBlank(reqBean.getEndDate())){
                            //结束时间不能大于SA记录中，结束时间为空的的开始时间
                            Date eDate = sdf.parse(reqBean.getEndDate());
                            if(sDate.after(eDate)) {
                                editBean.setEndDate(sdf.parse(reqBean.getEndDate()));
                            }else{
                                serviceBean.setCode(200);
                                serviceBean.setDesc("该时间段已存在有效关系，结束时间要小于"+sdf.format(sDate));
                                return serviceBean;
                            }
                        }else{
                            serviceBean.setCode(200);
                            serviceBean.setDesc("该时间段已存在有效关系，结束时间要小于"+sdf.format(sDate));
                            return serviceBean;
                        }
                    }
                }

                if(flag){
                    if(StringUtils.isNotBlank(reqBean.getEndDate())){
                        editBean.setEndDate(sdf.parse(reqBean.getEndDate()));
                    }else{
                        editBean.setEndDate(null);
                    }
                }
            }else{
                editBean.setEndDate(sdf.parse(sdf.format(new Date())));

                //1店更改过，结束当前关系，并新增新的关系
//                saTerminalManage.setStartDate(DateUtils.getAddDate(sdf.parse(sdf.format(new Date())),1));
//                saTerminalManage.setStartDate(sdf.parse(reqBean.getStartDate()));
                //结束时间为空的结束掉

                List<SaTerminalManage> stmList = getSaTerminalManageList(reqBean);
                for(SaTerminalManage stm:stmList){
                    if(null == stm.getEndDate()) {
                        stm.setEndDate(sdf.parse(sdf.format(new Date())));
                        stm.setUpdatedBy(reqBean.getCreatedBy());
                        stm.setUpdatedName(reqBean.getCreatedName());
                        stm.setUpdatedTime(new Date());
                        super.getHibernateTemplate().update(stm);
                    }
                }
                //1店更改过，结束当前关系，并新增新的关系
                super.save(saTerminalManage, SaTerminalManage.KEY, "MAMA100_OWNER");
            }

            super.getHibernateTemplate().update(editBean);

        }else{
            //当SA门店关联门店状态为撤销时。更改状态为无效
            Map terMap = queryTerminalStatus(reqBean.getSaCode());
            if(MapUtils.isNotEmpty(terMap)){
                SaTerminalManage saTerminalManageVo = super.get(SaTerminalManage.class, Long.valueOf((Integer)terMap.get("tmId")));
                saTerminalManageVo.setStatus(0);
                saTerminalManageVo.setUpdatedBy("system");
                saTerminalManageVo.setUpdatedName("system");
                saTerminalManageVo.setUpdatedTime(new Date());
                super.getHibernateTemplate().update(saTerminalManageVo);
            }
            //根据主门店类型，验证副门店
            super.save(saTerminalManage, SaTerminalManage.KEY, "MAMA100_OWNER");

        }

        return serviceBean;
    }

    public List<SaTerminalManage> getSaTerminalManageList(SaTerminalManageReqBean reqBean){
        SaTerminalManage saVo = new SaTerminalManage();
        saVo.setSaCode(reqBean.getSaCode());
        saVo.setStatus(1);
        List<SaTerminalManage> stmList = this.getHibernateTemplate().findByExample(saVo);
        return  stmList;
    }
    /**
     * 查询门店信息
     * @param terminalCode
     * @return
     */
    public Map queryTerminalInfo(String terminalCode) throws Exception {
        String sql = "select ct.terminalType terminalType, dt.department_code departmentCode,ct.status_code statusCode\n" +
                    "  from mama100_owner.crm_terminal               ct,\n" +
                    "       mama100_owner.common_department_terminal dt\n" +
                    " where 1 = 1\n" +
                    "   and ct.code = dt.terminal_code(+)\n" +
                    "   and ct.code = :terminalCode";
        Query query = super.createSQLQuery(sql).addScalar("terminalType", StringType.INSTANCE).addScalar("departmentCode", StringType.INSTANCE).addScalar("statusCode", StringType.INSTANCE);
        query.setParameter("terminalCode", terminalCode.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        return (Map)query.uniqueResult();
    }


    /**
     * 查询门店信息
     * @param terminalCode
     * @return
     */
    public int queryCount(String terminalCode,String relationTerminalCode,Boolean flag) throws Exception {
        String sql =
                        "select count(distinct nvl(nvl(cttt.code, ctt.code),1))  terminalNums\n" +
                        "  from mama100_owner.crm_terminal ct\n" +
                        "  left join mama100_owner.crm_terminal ctt\n" +
                        "    on ct.parent_code = ctt.code\n" +
                        "    and  ct.terminaltype in(1,2)\n" +
                        "  left join mama100_owner.crm_terminal cttt\n" +
                        "    on ctt.parent_code = cttt.code\n" +
                        "  left join mama100_owner.crm_terminal_chaintype chain\n" +
                        "    on chain.terminal_code = nvl(cttt.code, ctt.code)\n" +
                        "    and chain.end_time is null\n" +
                        "  left join mama100_owner.terminal_dictory td\n" +
                        "   on chain.chain_type = td.dic_code\n" +
                        "   and td.dic_type = 'C_T'\n" +
                        "  and ct.status_code = '01'\n" +
                        "  where ct.code in (:terminalCode, :relationTerminalCode) \n";
        StringBuffer sb = new StringBuffer();
        sb.append(sql);
        if(flag) {
            sb.append("  and td.dic_code = 1 ");
        }
        Query query = super.createSQLQuery(sb.toString()).addScalar("terminalNums", IntegerType.INSTANCE);
        query.setParameter("terminalCode", terminalCode.trim());
        query.setParameter("relationTerminalCode", relationTerminalCode.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map = (Map)query.uniqueResult();
        return (Integer)map.get("terminalNums");
    }


    /**
     * SA关联的门店状态为撤销
     * @param saCode
     * @return
     * @throws Exception
     */
    public Map queryTerminalStatus(String saCode) throws Exception{
        String sql =
                    "select t.id tmId\n" +
                        "  from mama100_owner.mkt_sa_terminal_manage t,\n" +
                        "       mama100_owner.crm_terminal           ct,\n" +
                        "       mama100_owner.crm_terminal           ct2\n" +
                        " where 1 = 1\n" +
                        "   and t.sa_code = ct.code\n" +
                        "   and t.terminal_code = ct2.code\n" +
                        "   and t.status = 1\n" +
                        "   and ct.status_code = '01'\n" +
                        "   and ct2.status_code != '01'\n" +
                        "   and (t.end_date is null or t.end_date >= trunc(sysdate))\n" +
                        "   and t.sa_code = :saCode ";


        Query query = super.createSQLQuery(sql).addScalar("tmId", IntegerType.INSTANCE);
        query.setParameter("saCode", saCode.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        return (Map)query.uniqueResult();
    }
}
