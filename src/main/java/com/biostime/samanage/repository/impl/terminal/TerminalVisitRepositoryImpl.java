package com.biostime.samanage.repository.impl.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.terminal.*;
import com.biostime.samanage.repository.terminal.TerminalVisitRepository;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Describe:SA与门店管理RepositoryImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Repository("terminalVisitRepository")
public class TerminalVisitRepositoryImpl extends BaseOracleRepositoryImpl implements TerminalVisitRepository {


    /**
     * 查询、导出条件(月度)
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(TerminalVisitReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //活动开始日期
        String startMonth = req.getStartMonth();
        if (StringUtil.isNotNullNorEmpty(startMonth)) {
            paramsMap.put("startDate", startMonth.trim());
        }

        //活动结束日期
        String endMonth = req.getEndMonth();
        if (StringUtil.isNotNullNorEmpty(endMonth)) {
            paramsMap.put("endDate", endMonth.trim());
        }

        //名称
        String name = req.getName();
        if (StringUtil.isNotNullNorEmpty(name)) {
            paramsMap.put("name", name.trim());
        }

        //工号
        String account = req.getAccount();
        if (StringUtil.isNotNullNorEmpty(account)) {
            paramsMap.put("account", account.trim());
        }

        //岗位编码
        String postCode = req.getPostCode();
        if (StringUtil.isNotNullNorEmpty(postCode)) {
            paramsMap.put("postCode", postCode.trim());
        }

        //季度
        String quarter = req.getQuarter();
        if (StringUtil.isNotNullNorEmpty(quarter)) {
            paramsMap.put("quarter", quarter.trim());
        }

        //大区办事处编码
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //大区办事处编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        return paramsMap;
    }



    /**
     * 门店拜访考核查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<TerminalVisitResBean> queryTerminalVisitList(Pager<TerminalVisitResBean> pager, TerminalVisitReqBean reqBean) {
        String startMonth = reqBean.getStartMonth();
        String endMonth = reqBean.getEndMonth();
        //根据开始月份、结束月份查询出具体的开始时间、结束时间。如：201906：开始时间2019-05-26，结束时间：2019-06-25
        Map map = queryKhMonthDate(startMonth,endMonth);
        //是否限制查询月份
        reqBean.setStartMonth((String) map.get("startDate"));
        reqBean.setEndMonth((String) map.get("endDate"));
        System.out.println("--reqBean.startMonth--"+(String) map.get("startDate"));
        System.out.println("--reqBean.endMonth--"+(String) map.get("endDate"));
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("TerminalVisitRepositoryImpl.queryTerminalVisitList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, TerminalVisitResBean.class);
    }

    /**
     * 门店拜访考核列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportTerminalVisitList(HttpServletResponse response, TerminalVisitReqBean reqBean) throws Exception {

        String startMonth = reqBean.getStartMonth();
        String endMonth = reqBean.getEndMonth();
        //根据开始月份、结束月份查询出具体的开始时间、结束时间。如：201906：开始时间2019-05-26，结束时间：2019-06-25
        Map map = queryKhMonthDate(startMonth,endMonth);
        //是否限制查询月份
        reqBean.setStartMonth((String) map.get("startDate"));
        reqBean.setEndMonth((String) map.get("endDate"));

        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("TerminalVisitRepositoryImpl.queryTerminalVisitList.list", freeMakerContext);
        List<TerminalVisitResBean> bscarList = this.findAllForListBySql(sql,paramsMap,TerminalVisitResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "门店拜访考核报表月度";

        th = new String[]{"考核月份","大区","办事处","姓名","工号","岗位","门店星级","本月门店关联数量","本月门店拜访数量","本月门店拜访达标数量","月度达标率"};

        colName = new String[] {"auditMonth","areaName","officeName","name","account","postName","starlevelName","relationTerminalNum","terminalVisitNum","terminalVisitAchievementNum","achievementRate"};
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * 门店拜访考核列表导出
     * @param reqBean
     * @return
     */
    @Override
    public List<BaseKeyValueBean> queryTerminalVisitPostList(TerminalVisitReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<BaseKeyValueBean> baseKeyValueBeanList = new ArrayList<>();
        try {
            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

            String sql = SqlUtil.get("TerminalVisitRepositoryImpl.queryTerminalVisitPostList", freeMakerContext);
            System.out.println("----TerminalVisitRepositoryImpl.queryTerminalVisitPostList SQL----"+sql.toString());

            if("2".equals(reqBean.getType())){
                sql = SqlUtil.get("TerminalVisitRepositoryImpl.queryQuarter.list", freeMakerContext);
                System.out.println("----TerminalVisitRepositoryImpl.queryQuarter SQL----"+sql.toString());
            }
            baseKeyValueBeanList = this.findAllForListBySql(sql, paramsMap,BaseKeyValueBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseKeyValueBeanList;
    }

    /**
     * 门店拜访考核季度查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<TerminalVisitQuarterResBean> queryTerminalVisitQuarterList(Pager<TerminalVisitQuarterResBean> pager, TerminalVisitReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);

        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("TerminalVisitRepositoryImpl.queryTerminalVisitQuarterList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, TerminalVisitQuarterResBean.class);
    }

    /**
     * 门店拜访考核列表季度导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportTerminalVisitQuarterList(HttpServletResponse response, TerminalVisitReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("TerminalVisitRepositoryImpl.queryTerminalVisitQuarterList.list", freeMakerContext);
        List<TerminalVisitQuarterResBean> bscarList = this.findAllForListBySql(sql,paramsMap,TerminalVisitQuarterResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "门店拜访考核报表季度";

        th = new String[]{"季度","大区","办事处","姓名","工号","岗位","1/无星级季度达标率","2星级季度达标率","3星级季度达标率","4星级季度达标率","5星级季度达标率","季度达标率"};

        colName = new String[] {"quarter","areaName","officeName","name","account","postName","oneStarlevelRate","twoStarlevelRate","threeStarlevelRate","fourStarlevelRate","fiveStarlevelRate","quarterAchievementRate"};
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * 查询门店拜访列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public List<TerminalVisitMobResBean> queryTerminalVisitMobList(TerminalVisitMobReqBean reqBean) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        //名称
        String month = reqBean.getMonth();
        if (StringUtil.isNotNullNorEmpty(month)) {
            paramsMap.put("month", month.trim());
        }

        //工号
        String account = reqBean.getAccount();
        if (StringUtil.isNotNullNorEmpty(account)) {
            paramsMap.put("code", account.trim());
        }

        List<TerminalVisitMobResBean> terminalVisitMobList = new ArrayList<>();
        try {
            FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

            String sql = SqlUtil.get("TerminalVisitRepositoryImpl.queryTerminalVisitMobList", freeMakerContext);
            System.out.println("----TerminalVisitRepositoryImpl.queryTerminalVisitMobList SQL----"+sql.toString());


            terminalVisitMobList = this.findAllForListBySql(sql, paramsMap,TerminalVisitMobResBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return terminalVisitMobList;
    }

    /**
     * 查询月份开始、结束时间
     * @param startMonth
     * @param endMonth
     * @return
     */
    public Map queryKhMonthDate(String startMonth,String endMonth) {
        String sql =
                "select     to_char(qdate.startDate,'yyyy-MM-dd') startDate,\n" +
                        "    to_char(qdate.endDate,'yyyy-MM-dd') endDate,\n" +
                        "       count(distinct dcd.month_audit) monthNum\n" +
                        "  from mama100_owner.dmsn_cal_date dcd,\n" +
                        "       (select min(sdcd.cal_date) startDate, max(edcd.cal_date) endDate\n" +
                        "          from mama100_owner.dmsn_cal_date sdcd,\n" +
                        "               mama100_owner.dmsn_cal_date edcd\n" +
                        "         where 1 = 1\n" +
                        "           and sdcd.month_audit = :startMonth \n" +
                        "           and edcd.month_audit = :endMonth) qdate\n" +
                        " where 1 = 1\n" +
                        "   and dcd.cal_date >= qdate.startDate\n" +
                        "   and dcd.cal_date <= qdate.endDate\n" +
                        "   group by qdate.startDate,qdate.endDate";
        Query query = super.createSQLQuery(sql).addScalar("startDate", StringType.INSTANCE).addScalar("endDate", StringType.INSTANCE).addScalar("monthNum", StringType.INSTANCE);
        query.setParameter("startMonth", startMonth.trim());
        query.setParameter("endMonth", endMonth.trim());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        return (Map) query.uniqueResult();
    }
}
