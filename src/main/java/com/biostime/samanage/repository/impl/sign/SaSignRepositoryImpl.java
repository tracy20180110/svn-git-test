package com.biostime.samanage.repository.impl.sign;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.CollectionUtil;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.sign.SaSignMerchantReqBean;
import com.biostime.samanage.bean.sign.SaSignReqBean;
import com.biostime.samanage.bean.sign.SaSignResBean;
import com.biostime.samanage.bean.sign.SaSignTerminalResBean;
import com.biostime.samanage.domain.sign.SaSign;
import com.biostime.samanage.repository.sign.SaSignRepository;
import com.biostime.samanage.util.DateUtils;
import com.biostime.samanage.util.LocationUtils;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Describe:促销员打卡RepositoryImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("saSignRepository")
@Slf4j
public class SaSignRepositoryImpl extends BaseOracleRepositoryImpl implements SaSignRepository {

    /**
     * 查询、导出条件
     *
     * @param req
     * @return
     */
    public HashMap<String, Object> queryInfoParamsMap(SaSignReqBean req) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //打卡开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", DateUtils.getTimesmorning(sdf.parse(startDate.trim())));
        }

        //打卡结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", DateUtils.getTimesnight(sdf.parse(endDate.trim())));
        }

        //渠道
        String channelCode = req.getChannelCode();
        if (StringUtil.isNotNullNorEmpty(channelCode)) {
            paramsMap.put("channelCode", channelCode.trim());
        }

        //打卡人
        String createdBy = req.getCreatedBy();
        if (StringUtil.isNotNullNorEmpty(createdBy)) {
            paramsMap.put("createdBy", createdBy.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }

        return paramsMap;
    }

    /**
     * 促销员打卡查询列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaSignResBean> querySaSignList(Pager<SaSignResBean> pager, SaSignReqBean reqBean) throws ParseException {
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("SaSignRepositoryImpl.querySaSignList", freeMakerContext);
        String countSql = "select count(*) from (" + sql + ")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaSignResBean.class);
    }

    /**
     * 促销员打卡询列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaSignList(HttpServletResponse response, SaSignReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("SaSignRepositoryImpl.querySaSignList", freeMakerContext);
        List<SaSignResBean> bscarList = this.findAllForListBySql(sql, paramsMap, SaSignResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;

        String report_name = "SA打卡明细报表";

        String th[] = new String[]{"大区",
                "办事处",
                "渠道",
                "打卡人工号",
                "打卡人名称",
                "上班打卡时间1",
                "上班门店编码1",
                "上班门店名称1",
                "上班位置一致性",
                "下班打卡时间1",
                "下班门店编码1",
                "下班门店名称1",
                "下班位置一致性",
                "上班打卡时间2",
                "上班门店编码2",
                "上班门店名称2",
                "上班位置一致性",
                "下班打卡时间2",
                "下班门店编码2",
                "下班门店名称2",
                "下班位置一致性",
                "上班时长"};
        String colName[] = new String[]{"areaName", "officeName", "channelName", "createdBy", "createdName", "startTime", "terminalCodeOn", "terminalNameOn",
                "statusOn", "endTime", "terminalCodeOff", "terminalNameOff", "statusOff", "startTime2", "terminalCodeOn2", "terminalNameOn2",
                "statusOn2", "endTime2", "terminalCodeOff2", "terminalNameOff2", "statusOff2", "workingTime"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }

    /**
     * 促销员打卡(手机端)
     *
     * @param reqBean
     * @return
     */
    @Override
    public void saveSaSign(SaSignMerchantReqBean reqBean) throws Exception {

        SaSign saSign = new SaSign();

        saSign.setSignTime(new Date());
        saSign.setTerminalCode(reqBean.getTerminalCode());
        saSign.setType(Integer.valueOf(reqBean.getType()));
        saSign.setLatitude(reqBean.getLatitude());
        saSign.setLongitude(reqBean.getLongitude());
        saSign.setStatus(reqBean.getStatus());
        saSign.setChannelCode(reqBean.getChannelCode());

        saSign.setDepartmentCode(reqBean.getDepartmentCode());
        saSign.setType(reqBean.getType());
        saSign.setTerminalNum(reqBean.getTerminalNum());

        saSign.setCreatedBy(reqBean.getCreatedBy());
        saSign.setCreatedTime(new Date());
        saSign.setCreatedName(reqBean.getCreatedName());
        saSign.setUpdatedBy(reqBean.getCreatedBy());
        saSign.setUpdatedTime(new Date());

        super.save(saSign, SaSign.KEY, "MAMA100_OWNER");
    }

    /**
     * 查询是否在指定的门店(手机端)
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<SaSignTerminalResBean>> queryIsInTerminal(SaSignMerchantReqBean reqBean) throws Exception {
        ServiceBean<List<SaSignTerminalResBean>> serviceBean = new ServiceBean();
        List<SaSignTerminalResBean> saSignTerminalResBeans = new ArrayList<>();

        if (StringUtil.isNullOrEmpty(reqBean.getCreatedBy())) {
            serviceBean.setCode(3004);
            serviceBean.setDesc(PropUtils.getProp(3004));
            return serviceBean;
        }
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("createdBy", reqBean.getCreatedBy().trim());
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql = SqlUtil.get("SaSignRepositoryImpl.queryIsInTerminal", freeMakerContext);
        saSignTerminalResBeans = this.findAllForListBySql(sql, paramsMap, SaSignTerminalResBean.class);
        serviceBean.setResponse(saSignTerminalResBeans);

        if (!CollectionUtil.isEmptyList(saSignTerminalResBeans) && saSignTerminalResBeans.size() == 2) {
            for (SaSignTerminalResBean saSignTerminalResBean : saSignTerminalResBeans) {
                if (saSignTerminalResBean.getTerminalCode().equals(reqBean.getTerminalCode())) {
                    serviceBean.setCode(3005);
                    serviceBean.setDesc(PropUtils.getProp(3005));
                    return serviceBean;
                }
            }

        }

        //验证经纬度与打卡门店是否一致
        String sql2 = "select t.longitude longitude,t.latitude latitude from mama100_owner.crm_terminal t " +
                " where 1 = 1 " +
                " and t.status_code = '01' " +
                " and t.code = :terminalCode";
        Query query2 = super.createSQLQuery(sql2);
        query2.setParameter("terminalCode", reqBean.getTerminalCode().trim());
        query2.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map = (Map) query2.uniqueResult();
        if (null == map || map.size() == 0) {
            serviceBean.setCode(202);
            serviceBean.setDesc(PropUtils.getProp(3002));
            return serviceBean;
        }
        if (null == map.get("LONGITUDE") || null == map.get("LATITUDE")) {
            serviceBean.setCode(202);
            serviceBean.setDesc(PropUtils.getProp(3002));
            return serviceBean;
        }


        Double longitude = Double.valueOf((String) map.get("LONGITUDE"));
        Double latitude = Double.valueOf((String) map.get("LATITUDE"));
        log.info("longitude:" + longitude + ",latitude:" + latitude);
        Double location = LocationUtils.getDistance(latitude, longitude, Double.valueOf(reqBean.getLatitude()), Double.valueOf(reqBean.getLongitude()));
        System.out.println("-------计算当前与门店的距离-------" + location + "米");
        log.info("-------计算当前与门店的距离-------" + location + "米");
        if (location > 1000) {  //大于1000米 则提示不在门店范围内
            serviceBean.setCode(202);
            serviceBean.setDesc(PropUtils.getProp(3003));
        }
        return serviceBean;

    }

    @Override
    public int getSaSingCount(String createdBy, String terminalCode, Integer terminalNum) {
        StringBuffer sql = new StringBuffer();
        sql.append(" select count(s.terminal_code) from mama100_owner.mkt_sa_sign s");
        sql.append(" where 1 = 1");
        sql.append(" and s.created_by = :createdBy              ");
        sql.append(" and s.terminal_num = :terminalNum          ");
        sql.append(" and s.terminal_code = :terminalCode        ");
        sql.append(" and trunc(s.created_time) = trunc(sysdate) ");
        Query query = super.createSQLQuery(sql.toString());
        query.setParameter("createdBy", createdBy);
        query.setParameter("terminalCode", terminalCode);
        query.setParameter("terminalNum", terminalNum);
        return ((Number) query.uniqueResult()).intValue();
    }

    @Override
    public String getSaSingTerminalCode(String createdBy) {
        StringBuffer sql = new StringBuffer();
        sql.append(" select s.terminal_code from mama100_owner.mkt_sa_sign s");
        sql.append(" where 1 = 1");
        sql.append(" and s.created_by = :createdBy  ");
        sql.append(" and s.terminal_num = 1         ");
        sql.append(" and s.type = 1                 ");
        sql.append(" and trunc(s.created_time) = trunc(sysdate) ");
        Query query = super.createSQLQuery(sql.toString());
        query.setParameter("createdBy", createdBy);
        List list = query.list();
        if (!CollectionUtil.isEmptyList(list)) {
            return list.get(0).toString();
        }
        return null;
    }

    @Override
    public int getSaSingCountOn(String createdBy) {
        StringBuffer sql = new StringBuffer();
        sql.append(" select count(s.terminal_code) from mama100_owner.mkt_sa_sign s");
        sql.append(" where 1 = 1");
        sql.append(" and s.created_by = :createdBy              ");
        sql.append(" and trunc(s.created_time) = trunc(sysdate) ");
        Query query = super.createSQLQuery(sql.toString());
        query.setParameter("createdBy", createdBy);
        return ((Number) query.uniqueResult()).intValue();
    }
}
