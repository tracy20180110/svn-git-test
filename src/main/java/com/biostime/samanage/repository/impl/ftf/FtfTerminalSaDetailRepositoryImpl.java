package com.biostime.samanage.repository.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.datasource.DataSourceEnum;
import com.biostime.common.datasource.DatabaseContextHolder;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.ftf.report.FtfTerminalSaDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfTerminalSaDetailResBean;
import com.biostime.samanage.repository.ftf.FtfTerminalSaDetailRepository;
import com.biostime.samanage.util.DateUtils;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动SA、门店信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("ftfTerminalSaDetailRepository")
public class FtfTerminalSaDetailRepositoryImpl extends BaseOracleRepositoryImpl implements FtfTerminalSaDetailRepository {



    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryParamsMap(FtfTerminalSaDetailReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //SA编码
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }

        //活动类型
        String type = req.getType();
        if (StringUtil.isNotNullNorEmpty(type)) {
            paramsMap.put("type", type.trim());
        }

        //讲师编号
        String lecturerCode = req.getLecturerCode();
        if (StringUtil.isNotNullNorEmpty(lecturerCode)) {
            paramsMap.put("lecturerCode", lecturerCode.trim());
        }

        //活动开始日期
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //活动结束日期
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //兼职讲师工号
        String jzLecturerAccount = req.getJzLecturerAccount();
        if (StringUtil.isNotNullNorEmpty(jzLecturerAccount)) {
            paramsMap.put("jzLecturerAccount", jzLecturerAccount.trim());
        }

        //活动有效性
        String actValid = req.getActValid();
        if (StringUtil.isNotNullNorEmpty(actValid)) {
            paramsMap.put("actValid", actValid.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }

        return paramsMap;
    }
    /**
     * FTF活动SA、门店信息
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<FtfTerminalSaDetailResBean> queryFtfTerminalSaDetailList(Pager<FtfTerminalSaDetailResBean> pager, FtfTerminalSaDetailReqBean reqBean) {
        //手动切换成只读数据源
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);

        String sql= SqlUtil.get("FtfTerminalSaDetailRepositoryImpl.newQueryFtfTerminalSaDetailList", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, FtfTerminalSaDetailResBean.class);
    }

    /**
     * FTF活动SA、门店信息导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportFtfTerminalSaDetailList(HttpServletResponse response, FtfTerminalSaDetailReqBean reqBean) throws Exception {
        DatabaseContextHolder.setCustomerType(DataSourceEnum.ORACLE_READ_ONLY.getDataSource());//手动切换成只读数据源
        HashMap<String, Object> paramsMap = queryParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("FtfTerminalSaDetailRepositoryImpl.queryFtfTerminalSaDetailList", freeMakerContext);
        List<FtfTerminalSaDetailResBean> bscarList = this.findAllForListBySql(sql,paramsMap,FtfTerminalSaDetailResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "FTF活动明细";

        th = new String[]{"大区","办事处","创建人工号","创建人姓名","课程名称","场地类型","活动品牌","活动日期","开始时间","结束时间","活动类型","SA编码",
                "FTF主题","内部讲师工号","内部讲师名称","兼职讲师工号","兼职讲师名称","兼职讲师归属","讲师岗位","外部专家名称","讲师认证","管理渠道","门店编码",
                "签到人数","活动规模","活动当天用券数","羊奶新客","合生元奶粉品项新客","HT品项新客","Dodie品项新客","益生菌品项新客",
                "羊奶销量","合生元奶粉销量","HT销量","Dodie销量","益生菌销量","FTF活动积分",
                "Feichat跨境购销量","Feichat有效跨境购销量","Dodie Feichat销售额","Dodie Feichat销售包数","一般贸易销量","活动有效性","照片1","照片2","照片3"};

        colName = new String[] {"areaName","officeName","createdBy","createdName","kcName","placeType","brandName","actDate","startTime","endTime","actType",
                "saCode","ftfTheme","lecturerCode","lecturerName","jzLecturerAccount","jzLecturerName","lecturerByCompany","postName","externalName","ftfLecturerCheck",
                "terminalChannel","terminalCode","singInNum","actScale","actDayCouponNums","kbsNewCustomer","biostimeNewCustomer","htNewCustomer","dodieNewCustomer","probioticsNewCustomer",
                "kbsSalesNums","biostimeSalesNums","htSalesNums","dodieSalesNums","probioticsSalesNums",
                "ftfActPoint","fcCBECSales","fcCBECEffectiveSales","feichatDodieNum","feichatDodieBNum","tradeVolume","actValid","image1","image1","image2","image3"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }
}
