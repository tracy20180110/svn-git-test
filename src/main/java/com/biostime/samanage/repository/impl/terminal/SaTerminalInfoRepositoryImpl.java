package com.biostime.samanage.repository.impl.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.PagerQuery;
import com.biostime.common.oraclerepo.impl.BaseOracleRepositoryImpl;
import com.biostime.common.sql.SqlUtil;
import com.biostime.common.sql.freemarker.FreeMakerContext;
import com.biostime.common.util.ExcelExportUtil;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.terminal.SaTerminalInfoReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalInfoResBean;
import com.biostime.samanage.repository.terminal.SaTerminalInfoRepository;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * Describe:SA与门店管理RepositoryImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Repository("saTerminalInfoRepository")
public class SaTerminalInfoRepositoryImpl extends BaseOracleRepositoryImpl implements SaTerminalInfoRepository {

    /**
     * 查询、导出条件
     * @param req
     * @return
     */
    public HashMap<String, Object> queryInfoParamsMap(SaTerminalInfoReqBean req){
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        //大区办事处
        String areaOfficeCode = req.getAreaOfficeCode();
        if (StringUtil.isNotNullNorEmpty(areaOfficeCode)) {
            paramsMap.put("areaOfficeCode", areaOfficeCode.trim());
        }

        //sa编号
        String saCode = req.getSaCode();
        if (StringUtil.isNotNullNorEmpty(saCode)) {
            paramsMap.put("saCode", saCode.trim());
        }

        //门店编码
        String status = req.getStatus();
        if (StringUtil.isNotNullNorEmpty(status)) {
            paramsMap.put("status", status.trim());
        }

        //门店编码
        String startDate = req.getStartDate();
        if (StringUtil.isNotNullNorEmpty(startDate)) {
            paramsMap.put("startDate", startDate.trim());
        }

        //门店编码
        String endDate = req.getEndDate();
        if (StringUtil.isNotNullNorEmpty(endDate)) {
            paramsMap.put("endDate", endDate.trim());
        }

        //事业部编码
        String buCode = req.getBuCode();
        if (StringUtil.isNotNullNorEmpty(buCode)) {
            paramsMap.put("buCode", buCode.trim());
        }
        return paramsMap;
    }

    /**
     * SA门店查询列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public Pager<SaTerminalInfoResBean> querySaTerminalInfoList(Pager<SaTerminalInfoResBean> pager, SaTerminalInfoReqBean reqBean) {
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalInfoRepositoryImpl.querySaTerminalInfoList.list", freeMakerContext);
        String countSql= "select count(*) from ("+sql+")";

        PagerQuery pagerQuery = new PagerQuery();
        pagerQuery.setSearchSql(sql);
        pagerQuery.setCountSql(countSql);
        pagerQuery.setParamsMap(paramsMap);

        return this.pageDataBySql(pagerQuery, pager, SaTerminalInfoResBean.class);
    }
    /**
     * SA门店查询列表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public void exportSaTerminalInfoList(HttpServletResponse response, SaTerminalInfoReqBean reqBean) throws Exception {
        HashMap<String, Object> paramsMap = queryInfoParamsMap(reqBean);
        FreeMakerContext freeMakerContext = new FreeMakerContext(paramsMap);
        String sql= SqlUtil.get("SaTerminalInfoRepositoryImpl.querySaTerminalInfoList.list", freeMakerContext);
        List<SaTerminalInfoResBean> bscarList = this.findAllForListBySql(sql,paramsMap,SaTerminalInfoResBean.class);

        String titleName[] = null;
        Integer titleRange[][] = null;
        String th[] = null;
        String colName[] = null;

        String report_name = "SA门店查询";

        th = new String[]{"大区","办事处","SA编码","SA名称","状态","门店二维码地址","创建时间","SA属性","SA地址",
                            "产科医护人员总数","产科门诊量（月）","VIP产科床位","VIP入住率","VIP有效床位",
                            "普通产科床位","普通入住率","普通有效床位","儿科医护人员总数","儿科门诊量（月）","新生儿床位","年分娩量"};

        colName = new String[] {"areaName","officeName","saCode","saName","status","saTwocodePath","createdTime","grade","saAddress",
                                "ckAllCount","ckMzCount","vipCkCw","vipRzl","vipYxCw","ptCkCw","ptRzl","ptYxCw","ekAllCount","ekMzCount","xseCw","nfmCount"};

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(new String(report_name.toString() + ".xlsx"), "UTF-8"));

        ExcelExportUtil.wirteDefaultExcel(titleName, titleRange, bscarList, th, colName, response.getOutputStream());

    }



}
