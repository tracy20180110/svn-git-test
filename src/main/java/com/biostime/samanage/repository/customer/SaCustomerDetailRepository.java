package com.biostime.samanage.repository.customer;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.customer.CustomerDetailBean;

import java.text.ParseException;
import java.util.List;

/**
 * SA客户明细
 * Created by zhuhaitao on 2017/1/3.
 */
public interface SaCustomerDetailRepository {
    /**
     * 分页查询SA客户明细
     *
     * @param pager
     * @param customerDetailBean
     * @return
     */
    Pager<CustomerDetailBean> searchSaCustomerDetail(Pager<CustomerDetailBean> pager, CustomerDetailBean customerDetailBean);

    /**
     * 导出SA客户明细
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param proBcc
     * @param srcLocName
     * @param recSrc
     * @param startTime
     * @param endTime
     * @return
     * @throws ParseException
     */
    List<CustomerDetailBean> getSaCustomerDetail(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobile, String proBcc, String srcLocName, String recSrc, String startTime, String endTime, String stage,String buCode, String parentingInstructor) throws ParseException;
}
