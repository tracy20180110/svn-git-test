package com.biostime.samanage.repository;

import com.biostime.common.bean.Pager;
import com.biostime.common.oraclerepo.BaseOracleRepository;
import com.biostime.samanage.bean.SaJobSchedulingBean;
import com.biostime.samanage.bean.SaJobSchedulingSalesAccountBean;
import com.biostime.samanage.domain.SaJobScheduling;
import com.biostime.samanage.domain.SaJobSchedulingStatus;
import com.biostime.samanage.domain.SaTerminal;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
/**
 * 类功能描述: SA工作排班
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年6月23日 上午11:21:59
 */
public interface JobSchedulingRepository extends BaseOracleRepository {

	/**
	 * 方法描述: 根据促销员编号和日期，查询当天排班
	 *
	 * @param customerId
	 * @param companyDate
	 * @return
	 * @author zhuhaitao
	 * @createDate 2016年6月23日 上午11:21:56
	 */
	List<SaJobScheduling> getJobScheduling(String salesAccountNo, Date startTime, Date endTime);

	/**
	 * 方法描述: 根据促销员编号和日期，删除当天排班数据
	 *
	 * @param customerId
	 * @param companyDate
	 * @author zhuhaitao
	 * @createDate 2016年6月23日 下午3:12:10
	 */
	void deleteByCustomerIdAndCompanyDate(String salesAccountNo, Date companyDate);

	/**
	 * 方法描述: 保存工作排班
	 *
	 * @param saJobScheduling
	 * @author zhuhaitao
	 * @throws IllegalAccessException
	 * @createDate 2016年6月23日 下午3:15:59
	 */
	void saveJobScheduling(SaJobScheduling saJobScheduling) throws IllegalAccessException;

	/**
	 * 方法描述: 根据办事处编号查询促销员排班列表
	 *
	 * @param officeCode
	 * @param month
	 * @return
	 * @author zhuhaitao
	 * @createDate 2016年6月23日 下午5:21:26
	 */
	List<SaJobSchedulingSalesAccountBean> queryJobScheduling(String officeCode, Date startTime, Date endTime,String buCode);

	/**
	 * 方法描述: 根据SA编号查询SA名称
	 *
	 * @param saCode
	 * @return
	 * @author zhuhaitao
	 * @createDate 2016年6月24日 下午5:05:18
	 */
	SaTerminal getSaNameBySaCode(String saCode);

	/**
	 * 方法描述: 根据促销员编号查询当月排班状态信息
	 *
	 * @param salesAccountNo
	 * @param startTime
	 * @param endTime
	 * @return
	 * @author zhuhaitao
	 * @createDate 2016年6月27日 下午2:23:11
	 */
	List<SaJobSchedulingStatus> getJobSchedulingStatus(String salesAccountNo, Date startTime, Date endTime);

	/**
	 * 方法描述: 保存促销员排班状态信息
	 *
	 * @param saJobSchedulingStatus
	 * @author zhuhaitao
	 * @throws IllegalAccessException
	 * @createDate 2016年6月27日 下午2:23:07
	 */
	void saveJobSchedulingStatus(SaJobSchedulingStatus saJobSchedulingStatus) throws IllegalAccessException;

	/**
	 * SA工作排班报表
	 * @param pager
	 * @param saJobSchedulingBean
	 * @return
	 */
	Pager<SaJobSchedulingBean> searchJobScheduling(Pager<SaJobSchedulingBean> pager, SaJobSchedulingBean saJobSchedulingBean) throws ParseException;

	/**
	 * 导出SA工作排班报表查询
	 * @param salesAccountNo
	 * @param areaCode
	 * @param officeCode
	 * @param startMonth
	 * @param endMonth
     * @return
     */
	List<SaJobSchedulingBean> getJobScheduling(String salesAccountNo, String areaCode, String officeCode, String startMonth, String endMonth,String buCode) throws ParseException;
}
