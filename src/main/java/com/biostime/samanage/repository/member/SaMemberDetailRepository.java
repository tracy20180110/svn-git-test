package com.biostime.samanage.repository.member;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.member.CommonDepartmentBean;
import com.biostime.samanage.bean.member.MemberDetailBean;
import com.biostime.samanage.bean.member.MktImportInfoBean;
import com.biostime.samanage.domain.*;

import java.text.ParseException;
import java.util.List;

/**
 * SA会员明细
 * Created by zhuhaitao on 2017/1/3.
 */
public interface SaMemberDetailRepository {
    /**
     * 分页查询SA会员明细
     *
     * @param pager
     * @param memberDetailBean
     * @return
     */
    Pager<MemberDetailBean> searchSaMenberDetail(Pager<MemberDetailBean> pager, MemberDetailBean memberDetailBean);

    /**
     * 导出查询SA会员明细
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param operateBcc
     * @param srcLocName
     * @param startTime
     * @param endTime
     * @return
     * @throws ParseException
     */
    List<MemberDetailBean> getSaMenberDetail(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobile, String proBcc, String srcLocName, String startTime, String endTime) throws ParseException;

    Pager<MktImportInfoBean> searchMktImportInfo(Pager<MktImportInfoBean> pager, MktImportInfoBean importInfoBean);

    void saveMktImportInfoBean(MktImportInfo importInfo) throws IllegalAccessException;
    SaTerminal getSAByCode(String saCode);

    CommonAccountList getCommonAccountListCode(String accountId, int type);

    HpSrcLoc getHpSrcLoc(String srcLocName);

    SaTerminal getTerminalByCode(String saCode);

    List<CommonDepartment> queryOfficeCode(String saOfficeCode, String areaCode);

    List<CommonDepartmentBean> queryAreaAndOfficeCode(String userId);

    void saveCrmCustTerminalChannelJpa(CrmCustTerminalChannelJpa crmCustChannelJpa);

    int getCrmCustomerCount(Long customerId, String channelCode);

    CrmCustomer getCustomerById(Long customerId);

    void addMemberImportRec(HpMemberEventRec hpMemberEventRec);
}
