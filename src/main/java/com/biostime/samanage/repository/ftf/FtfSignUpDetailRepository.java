package com.biostime.samanage.repository.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.ftf.sign.FtfSignUpDetailReqBean;
import com.biostime.samanage.bean.ftf.sign.FtfSignUpDetailResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动报名信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfSignUpDetailRepository {
    /**
     * FTF活动报名信息
     * @param reqBean
     * @return
     */
    public Pager<FtfSignUpDetailResBean> queryFtfSignUpDetailList(Pager<FtfSignUpDetailResBean> pager, FtfSignUpDetailReqBean reqBean);

    /**
     * FTF活动报名信息导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportFtfSignUpDetailList(HttpServletResponse response, FtfSignUpDetailReqBean reqBean) throws Exception;

}
