package com.biostime.samanage.repository.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.actreport.ActRePortListResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityReqBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityTemplateResBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoReqBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import com.biostime.samanage.domain.ftf.FtfActivity;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动Service
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfActivityRepository {
    /**
     * 获取FTF活动列表
     * @param reqBean
     * @return
     */
    public Pager<FtfActivityResBean>  queryFtfActivityList(Pager<FtfActivityResBean> pager, FtfActivityReqBean reqBean);

    /**
     * FTF活动编辑
     * @param reqBean
     * @return
     */
    public FtfActivity saveFtfActivity(FtfActivityReqBean reqBean) throws Exception;

    /**
     * FTF活动停用、启用
     * @param reqBean
     * @return
     */
    public void changeStatusFtfActivity(FtfActivityReqBean reqBean);


    /**
     * FTF活动列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportFtfActivityList(HttpServletResponse response, FtfActivityReqBean reqBean) throws Exception;

    /**
     * 获取SA对应的门店
     * @param saCode
     * @return
     */
    public ActRePortListResBean querySaTerminal(String saCode) throws Exception;

    /**
     * 查询门店是否存在
     * @param terminalCode
     * @return
     */
    public ActRePortListResBean queryTerminal(String terminalCode) throws Exception;

    /**
     * 查询导购ID是否存在
     * @param shoppingGuideId
     * @return
     */
    public ServiceBean queryShoppingGuideId(String shoppingGuideId) throws Exception;



    /**
     * FTF海报分享链接更改
     * @param reqBean
     * @return
     */
    public void changeImageFtfActivity(FtfActivityReqBean reqBean);


    /**
     * 获取FTF活动模板列表
     * @return
     */
    public List<FtfActivityTemplateResBean> queryFtfActivityTemplateList();


    /**
     * 查询SA线下课堂模板详情
     * @param templateId
     * @param actId
     * @return
     */
    public Map querySaFtfActTemplateInfo(String templateId, String actId) ;

    /**
     * 获取讲师列表
     * @return
     */
    public List<FtfLecturerResBean> queryLecturerList(FtfActivityReqBean reqBean);

    /**
     * 更新小程序签到码
     * @param actId
     * @return
     */
    public String changeActMama100QrCode(String actId);

    /**
     * 获取讲师列表APP
     * @return
     */
    public List<FtfLecturerResBean> queryAppLecturerList(FtfActivityReqBean reqBean);

    /**
     * 获取讲师列表APP
     * @return
     */
    public List<AccountInfoResBean> queryAppReportAccountInfoList(AccountInfoReqBean reqBean);

}
