package com.biostime.samanage.repository.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.ftf.report.FtfNewCustomerDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfNewCustomerDetailResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF新客报名信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfNewCustomerDetailRepository {
    /**
     * FTF新客报名信息
     * @param reqBean
     * @return
     */
    public Pager<FtfNewCustomerDetailResBean> queryFtfNewCustomerDetailList(Pager<FtfNewCustomerDetailResBean> pager, FtfNewCustomerDetailReqBean reqBean);

    /**
     * FTF新客报名信息导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportFtfNewCustomerDetailList(HttpServletResponse response, FtfNewCustomerDetailReqBean reqBean) throws Exception;

}
