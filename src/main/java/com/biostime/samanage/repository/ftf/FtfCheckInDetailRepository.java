package com.biostime.samanage.repository.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInAppGroupDetailResBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInAppIndividualDetailResBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInDetailReqBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInDetailResBean;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created with IntelliJ IDEA. Describe:FTF签到报名信息 Date: 2016-11-23 Time: 11:37
 * User: 12804 Version:1.0
 */
public interface FtfCheckInDetailRepository {
	/**
	 * FTF签到报名信息
	 * 
	 * @param reqBean
	 * @return
	 */
	public Pager<FtfCheckInDetailResBean> queryFtfCheckInDetailList(Pager<FtfCheckInDetailResBean> pager,
			FtfCheckInDetailReqBean reqBean);

	/**
	 * FTF签到报名信息导出
	 * 
	 * @param response
	 * @param reqBean
	 * @return
	 */
	public void exportFtfCheckInDetailList(HttpServletResponse response, FtfCheckInDetailReqBean reqBean)
			throws Exception;

	/**
	 * FTF 个人签到信息查询
	 * 
	 * @param reqBean
	 * @return
	 */
	public List<FtfCheckInAppIndividualDetailResBean> queryFtfCheckInAppIndividualDetail(
			FtfCheckInDetailReqBean reqBean);

	/**
	 * FTF 组团签到信息查询
	 * 
	 * @param reqBean
	 * @return
	 */
	public List<FtfCheckInAppGroupDetailResBean> queryFtfCheckInAppGroupDetail(FtfCheckInDetailReqBean reqBean);

	/**
	 * FTF活动签到领取奖品
	 * 
	 * @param reqBean
	 */
	public void receiveFtfCheckInAppPrizes(FtfCheckInDetailReqBean reqBean);

	/**
	 * 获取FTF活动签到领取状态
	 * 
	 * @param ftfActId
	 * @param groupId
	 */
	public String getReceiveStatus(String ftfActId, String groupId);

}
