package com.biostime.samanage.repository.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.ftf.Bnc6Product;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumReqBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumResBean;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:ftf课程
 * Date: 2016-6-23
 * Time: 16:53
 * User: 12804
 * Version:1.0
 */
public interface FtfCurriculumRepository {
    /**
     * FTF课程列表-查询
     * @param ftfCurriculumReqBean
     * @return
     */
    public Pager<FtfCurriculumResBean> queryFtfCurriculumList(Pager<FtfCurriculumResBean> pager,FtfCurriculumReqBean ftfCurriculumReqBean) ;

    /**
     * FTF课程列表-导出
     * @param response
     * @param reqBean
     * @throws Exception
     */
    public void exportFtfCurriculumList(HttpServletResponse response, FtfCurriculumReqBean reqBean)  throws Exception;

    /**
     * 路演活动--状态修改
     * @param reqBean
     * @return
     */
    public void changeStatusFtfCurriculum(FtfCurriculumReqBean reqBean);

    /**
     * 路演活动--保存
     * @param reqBean
     * @return
     */
    public void saveFtfCurriculum(FtfCurriculumReqBean reqBean) throws Exception;

    /**
     * 查询BNC6品商品
     * @return
     */
    public List<Bnc6Product> queryBnc6Product() throws Exception;

}


