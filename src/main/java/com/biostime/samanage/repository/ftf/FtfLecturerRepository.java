package com.biostime.samanage.repository.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.ftf.lecture.FtfLectureCodeResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerReqBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF讲师Service
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfLecturerRepository {
    /**
     * 获取FTF讲师列表
     * @param reqBean
     * @return
     */
    public Pager<FtfLecturerResBean>  queryFtfLecturerList(Pager<FtfLecturerResBean> pager, FtfLecturerReqBean reqBean);
    /**
     * FTF课程列表-导出
     * @param response
     * @param reqBean
     * @throws Exception
     */
    public void exportFtfLecturerList(HttpServletResponse response, FtfLecturerReqBean reqBean)  throws Exception;

    /**
     * 路演活动--状态修改
     * @param reqBean
     * @return
     */
    public void changeStatusFtfLecturer(FtfLecturerReqBean reqBean);

    /**
     * 路演活动--保存
     * @param reqBean
     * @return
     */
    public String saveFtfLecturer(FtfLecturerReqBean reqBean) throws Exception;

    /**
     * 获取讲师名称工号
     * @param keyword
     * @return
     */
    public List<FtfLectureCodeResBean> queryLecturerCode(String keyword,String type) throws Exception;


    /**
     * 路演活动--门店导入
     * @param reqBean
     * @return
     */
    public String importLecturerCheck(MultipartFile multipartFile,FtfLecturerReqBean reqBean)  throws Exception;


}
