package com.biostime.samanage.repository.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.ftf.report.FtfTerminalSaDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfTerminalSaDetailResBean;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动SA、门店信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfTerminalSaDetailRepository {
    /**
     * FTF活动SA、门店信息
     * @param reqBean
     * @return
     */
    public Pager<FtfTerminalSaDetailResBean> queryFtfTerminalSaDetailList(Pager<FtfTerminalSaDetailResBean> pager, FtfTerminalSaDetailReqBean reqBean);

    /**
     * FTF活动SA、门店信息导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportFtfTerminalSaDetailList(HttpServletResponse response, FtfTerminalSaDetailReqBean reqBean) throws Exception;

}
