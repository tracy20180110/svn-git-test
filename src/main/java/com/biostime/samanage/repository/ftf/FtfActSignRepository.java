package com.biostime.samanage.repository.ftf;

import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.ftf.sign.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动列表
 * Date: 2016-12-3
 * Time: 16:53
 * User: 12804
 * Version:1.0
 */
public interface FtfActSignRepository {
    /**
     * 活动列表
     * @param ftfActSignReqBean
     * @return
     */
    public List<FtfActSignResBean> queryMobFtfActList(FtfActSignReqBean ftfActSignReqBean);

    /**
     * 提交活动上报
     * @param reqBean
     * @return
     */
    public ServiceBean saveMobSign(FtfActSignInfoReqBean reqBean);

    /**
     * 签到详情
     * @param actId
     * @return
     */
    public FtfActSignListBean querySignInfoList(String actId,String createdBy);

    /**
     * 查询FTF签到活动信息
     * @param reqBean
     * @return
     */
    public FtfSignMiniAppResBean querySignFtfActInfo(FtfSignMiniAppReqBean reqBean);

    /**
     * 活动开始前、结束后30分钟可以签到
     * @param actId
     * @return
     */
    public boolean checkIsSign(String actId);

    /**
     * 上传销量
     * @param reqBean
     * @return
     */
    public void saveMobSignReportSales(FtfActSignInfoReqBean reqBean)  throws Exception;

    /**
     * 小程序扫码签到
     * @param reqBean
     * @return
     */
    public ServiceBean saveSignMiniApp(FtfSignMiniAppReqBean reqBean) throws Exception;

    /**
     * feichat贸易销量
     * @return
     */
    public ServiceBean saveActFeiChatSale() throws Exception;



    /**
     * 查询登陆人所属的SA门店
     * @param reqBean
     * @return
     */
    public List<BaseKeyValueBean> querySaTerminal(BaseKeyValueBean reqBean) ;
}


