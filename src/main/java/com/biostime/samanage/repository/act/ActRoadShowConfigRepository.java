package com.biostime.samanage.repository.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigReqBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * 路演活动
 * Created by dc on 2016/8/18.
 */
public interface ActRoadShowConfigRepository {


    /**
     * 路演活动关系--查询
     * @param pager
     * @param reqBean
     * @return
     */
    public Pager<ActRoadShowConfigResBean> queryActRoadShowConfigList(Pager<ActRoadShowConfigResBean> pager, ActRoadShowConfigReqBean reqBean);

    /**
     * 路演活动关系--导出
     * @param response
     * @param reqBean
     * @throws Exception
     */
    public void exportActRoadShowConfigList(HttpServletResponse response, ActRoadShowConfigReqBean reqBean) throws Exception;


    /**
     * 路演活动关系--状态修改
     * @param reqBean
     * @return
     */
    public void editStatusActRoadShowConfig(ActRoadShowConfigReqBean reqBean);

    /**
     * 路演活动关系-新增
     * @param reqBean
     * @return
     */
    public ServiceBean saveActRoadShowConfig(ActRoadShowConfigReqBean reqBean);
}
