package com.biostime.samanage.repository.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.actreport.*;
import com.biostime.samanage.bean.actroadshow.ActReportDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowListReqBean;

import java.util.List;

/**
 * Describe:活动上报
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface ActRePortRepository {

    /**
     * 获取登陆人可上报的活动
     * @param activityReqBean
     * @return
     */
    public List<ActRePortListResBean> queryActRePortList(ActRePortListReqBean activityReqBean) ;


    /**
     * 提交活动上报
     * @param reqBean
     * @return
     */
    public ServiceBean submitActRePort(ActRePortReqBean reqBean) ;

    /**
     * 获取门店对应的渠道
     * @param terminalCode
     * @return
     */
    public ActRePortTypeResBean queryTerminalChannel(String terminalCode,String reportTypeCode,String buCode) throws Exception ;

    /**
     * 获取图片类型列表
     * @param activityReqBean
     * @return
     */
    public List<ActRePortImgTypeResBean> queryImgTypeList(ActRePortImgTypeReqBean activityReqBean) ;


    /**
     * 路活动上报图片类型查询
     * @param pager
     * @param reqBean
     * @return
     */
    public Pager<ActRePortImgTypeResBean> queryActRePortImgTypeList(Pager<ActRePortImgTypeResBean> pager, ActRePortImgTypeReqBean reqBean);

    /**
     * 图片类型保存、编辑
     * @param reqBean
     * @return
     */
    public ServiceBean saveActRePortImgType(ActRePortImgTypeReqBean reqBean) throws Exception;

    /**
     * 获取上报关联的SA活动
     * @param activityReqBean
     * @return
     */
    public List<BaseKeyValueBean> querySaFtfActList(ActRePortReqBean activityReqBean) ;

    /**
     * 手机上报活动明细列表
     * @param activityReqBean
     * @return
     */
    public ActReportDetailResBean queryActReportDetail(ActRoadShowListReqBean activityReqBean) ;

    /**
     * 手机上报活动明细详情列表
     * @param activityReqBean
     * @return
     */
    public List<ActRoadShowDetailResBean> queryActReportDetailInfo(ActRoadShowListReqBean activityReqBean) ;

    /**
     * 查询是否是活动精英
     * @param reqBean
     * @return
     */
    public ServiceBean queryIsActElites(ActRoadShowListReqBean reqBean) ;
    /**
     * 查询活动精英所属办事处的门店
     * @param reqBean
     * @return
     */
    public List<BaseKeyValueBean> queryActElitesByOfficeTerminal(ActRoadShowListReqBean reqBean) ;

    /**
     * 查询品牌官微二维码
     * @param reqBean
     * @return
     */
    public ServiceBean queryReportBrandQrCodeList(ActRePortReqBean reqBean) ;

    /**
     * 查询活动上报签到小程序码
     * @param reqBean
     * @return
     */
    public ActRePortQrcodeResBean queryReportActSignMiniCode(ActRePortReqBean reqBean) ;

    /**
     * 提交活动上报
     * @param reqBean
     * @return
     */
    public ServiceBean saveActRePortZb(ActRePortZhiBoReqBean reqBean) ;

    /**
     * 查询直播人员
     * @param reqBean
     * @return
     */
    public List<BaseKeyValueBean> queryZhiBoAccount(BaseKeyValueBean reqBean) ;

    /**
     * 查询活动精英所属办事处的门店
     * @param reqBean
     * @return
     */
    public List<BaseKeyValueBean> queryBncActElitesTerminal(ActRoadShowListReqBean reqBean) ;
}
