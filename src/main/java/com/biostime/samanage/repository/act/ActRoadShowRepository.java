package com.biostime.samanage.repository.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.actreport.ActRePortWebReqBean;
import com.biostime.samanage.bean.actreport.ActRePortWebResBean;
import com.biostime.samanage.bean.actreport.ActRePortZhiBoReqBean;
import com.biostime.samanage.bean.actroadshow.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 活动现场
 * Created by dc on 2016/8/18.
 */
public interface ActRoadShowRepository {



    /**
     * 活动现场--查询
     * @param pager
     * @param reqBean
     * @return
     */
    public Pager<ActRoadShowListResBean> queryActRoadShowList(Pager<ActRoadShowListResBean> pager, ActRoadShowListReqBean reqBean);

    /**
     * 活动现场--导出
     * @param response
     * @param reqBean
     * @throws Exception
     */
    public void exportActRoadShow(HttpServletResponse response, ActRoadShowListReqBean reqBean) throws Exception;

    /**
     * 活动现场--保存
     * @param reqBean
     * @return
     */
    public ServiceBean saveActRoadShow(ActRoadShowSaveReqBean reqBean);


    /**
     * 活动现场--状态修改
     * @param reqBean
     * @return
     */
    public void changeStatusActRoadShow(ActRoadShowChangeStatusReqBean reqBean);

    /**
     * 活动现场-获取swisse列表、活动类型
     * @param reqBean
     * @return
     */
    public List<ActRoadShowResBean> querySwActTypeList(ActRoadShowReqBean reqBean);

    /**
     * 营销上报明细--查询
     * @param pager
     * @param reqBean
     * @return
     */
    public Pager<ActRoadShowDetailResBean> queryActRoadShowDetailList(Pager<ActRoadShowDetailResBean> pager, ActRoadShowListReqBean reqBean);

    /**
     * 营销上报明细-审核
     * @param reqBean
     * @return
     */
    public void auditActRoadShowDetail(ActRoadShowAuditReqBean reqBean) throws Exception;

    /**
     * 营销上报明细--导出
     * @param reqBean
     * @return
     */
    public void exportActRoadShowDetail(HttpServletResponse response, ActRoadShowListReqBean reqBean) throws Exception;

    /**
     * 活动现场报表--查询
     * @param pager
     * @param reqBean
     * @return
     */
    public Pager<ActRoadShowReportResBean> queryReportActRoadShowList(Pager<ActRoadShowReportResBean> pager, ActRoadShowReportReqBean reqBean);

    /**
     * 活动现场报表--导出
     * @param response
     * @param reqBean
     * @throws Exception
     */
    public void exportReportActRoadShow(HttpServletResponse response, ActRoadShowReportReqBean reqBean)  throws Exception;

    /**
     * 活动现场门店--导出
     * @param response
     * @param actId
     * @throws Exception
     */
    public void exportActRoadShowTerminal(HttpServletResponse response, String actId)  throws Exception;

    /**
     * 活动现场门店--导入
     * @param multipartFile
     * @param reqBean
     * @throws Exception
     */
    public String importActRoadShowTerminal(MultipartFile multipartFile,ActRoadShowTerminalReqBean reqBean)  throws Exception;


    /**
     * 营销上报明细-补录
     * @param reqBean
     * @return
     */
    public void saveActRoadShowMakeup(ActRePortWebReqBean reqBean) throws Exception;

    /**
     * 营销上报明细-补录活动门店验证
     * @param reqBean
     * @return
     */
    public ServiceBean checkActTerminal(ActRePortWebReqBean reqBean);


    /**
     * 营销上报明细-补录活动验证
     * @param reqBean
     * @return
     */
    public ActRePortWebResBean checkActShowInfo(ActRePortWebReqBean reqBean) throws Exception;


    /**
     * 营销上报明细-上报人验证
     * @param reqBean
     * @return
     */
    public ServiceBean checkAccountTerminal(ActRePortWebReqBean reqBean) throws Exception;

    /**
     * 活动精英上报明细--查询
     * @param pager
     * @param reqBean
     * @return
     */
    public Pager<ActRoadShowDetailResBean> queryActElitesReportDetailList(Pager<ActRoadShowDetailResBean> pager, ActRoadShowListReqBean reqBean);



    /**
     * 活动精英上报明细--导出
     * @param reqBean
     * @return
     */
    public void exportActElitesReportDetailList(HttpServletResponse response, ActRoadShowListReqBean reqBean) throws Exception;

    /**
     * 直播上报报表查询
     * @param pager
     * @param reqBean
     * @return
     */
    public Pager<ActRePortZhiBoReqBean> queryActReportZhiBoList(Pager<ActRePortZhiBoReqBean> pager, ActRePortZhiBoReqBean reqBean);



    /**
     * 直播上报报表导出
     * @param reqBean
     * @return
     */
    public void exportActReportZhiBoList(HttpServletResponse response, ActRePortZhiBoReqBean reqBean) throws Exception;




}
