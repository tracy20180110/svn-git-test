package com.biostime.samanage.repository.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.terminal.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:门店拜访考核
 * Date: 2017-02-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface TerminalVisitRepository {

    /**
     * 门店拜访考核查询
     * @param reqBean
     * @return
     */
    public Pager<TerminalVisitResBean> queryTerminalVisitList(Pager<TerminalVisitResBean> pager, TerminalVisitReqBean reqBean);

    /**
     * 门店拜访考核列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportTerminalVisitList(HttpServletResponse response, TerminalVisitReqBean reqBean) throws Exception;

    /**
     * 门店拜访考核列表导出
     * @param reqBean
     * @return
     */
    public List<BaseKeyValueBean> queryTerminalVisitPostList(TerminalVisitReqBean reqBean) ;

    /**
     * 门店拜访考核季度查询
     * @param reqBean
     * @return
     */
    public Pager<TerminalVisitQuarterResBean> queryTerminalVisitQuarterList(Pager<TerminalVisitQuarterResBean> pager, TerminalVisitReqBean reqBean);

    /**
     * 门店拜访考核列表季度导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportTerminalVisitQuarterList(HttpServletResponse response, TerminalVisitReqBean reqBean) throws Exception;

    /**
     * 查询门店拜访列表
     * @param reqBean
     * @return
     */
    public List<TerminalVisitMobResBean> queryTerminalVisitMobList(TerminalVisitMobReqBean reqBean) ;

}
