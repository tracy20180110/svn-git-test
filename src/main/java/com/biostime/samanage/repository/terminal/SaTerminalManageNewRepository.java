package com.biostime.samanage.repository.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageEditReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA与门店管理Repository
 * Date: 2017-02-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaTerminalManageNewRepository {

    /**
     * SA与门店管理列表（新）
     * @param reqBean
     * @return
     */
    public Pager<SaTerminalManageNewResBean> querySaTerminalManageNewList(Pager<SaTerminalManageNewResBean> pager, SaTerminalManageNewReqBean reqBean);
    /**
     * 查询门店渠道、状态
     * @param reqBean
     * @return
     */
    public ServiceBean<SaTerminalManageNewResBean> queryTerminalInfo(SaTerminalManageNewReqBean reqBean);

    /**
     * SA与门店管理列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaTerminalManageNewList(HttpServletResponse response, SaTerminalManageNewReqBean reqBean) throws Exception;


    /**
     * SA与门店管理-编辑
     * @param reqBean
     * @return
     */
    public String editSaTerminalManage(SaTerminalManageEditReqBean reqBean);

    /**
     * SA与门店绑定（新）
     * @param reqBean
     * @return
     */
    public ServiceBean saveSaTerminalRelationNew(SaTerminalManageNewReqBean reqBean) throws Exception;

}
