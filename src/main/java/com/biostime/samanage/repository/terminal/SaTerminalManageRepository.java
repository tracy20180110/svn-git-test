package com.biostime.samanage.repository.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.terminal.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA与门店管理Repository
 * Date: 2017-02-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaTerminalManageRepository {

    /**
     * SA与门店管理列表
     * @param reqBean
     * @return
     */
    public Pager<SaTerminalManageImportResBean> querySaTerminalManageImportList(Pager<SaTerminalManageImportResBean> pager, SaTerminalManageImportReqBean reqBean);

    /**
     * SA与门店管理列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaTerminalManageImportList(HttpServletResponse response, SaTerminalManageImportReqBean reqBean) throws Exception;


    /**
     * SA与门店管理列表
     * @param reqBean
     * @return
     */
    public Pager<SaTerminalManageResBean> querySaTerminalManageList(Pager<SaTerminalManageResBean> pager, SaTerminalManageReqBean reqBean);

    /**
     * SA与门店管理列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaTerminalManageList(HttpServletResponse response, SaTerminalManageReqBean reqBean) throws Exception;


    /**
     * SA与门店管理--导入
     * @param multipartFile
     * @param request
     * @throws Exception
     */
    public List importSaTerminalManage(MultipartFile multipartFile,HttpServletRequest request)  throws Exception;

    /**
     * SA与门店管理--删除
     * @param reqBean
     * @return
     */
    public void deleteSaTerminalManage(SaTerminalManageEditReqBean reqBean) throws Exception;

    /**
     * SA与门店管理-编辑
     * @param reqBean
     * @return
     */
    public String editSaTerminalManage(SaTerminalManageEditReqBean reqBean);


    /**
     * SA撤销结束与门店的关系
     * @param reqBean
     * @return
     */
    public void delSaTerminalRelation(SaTerminalManageReqBean reqBean);

    /**
     * SA与门店管理列表（新）
     * @param reqBean
     * @return
     */
    public Pager<SaTerminalManageResBean> querySaTerminalNewManageList(Pager<SaTerminalManageResBean> pager, SaTerminalManageReqBean reqBean);

    /**
     * SA与门店绑定（新）
     * @param reqBean
     * @return
     */
    public ServiceBean saveSaTerminalRelation(SaTerminalManageReqBean reqBean) throws Exception;

}
