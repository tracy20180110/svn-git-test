package com.biostime.samanage.repository.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.samanage.bean.terminal.SaTerminalInfoReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalInfoResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA查询管理Repository
 * Date: 2017-02-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaTerminalInfoRepository {

    /**
     * SA门店查询列表
     * @param reqBean
     * @return
     */
    public Pager<SaTerminalInfoResBean> querySaTerminalInfoList(Pager<SaTerminalInfoResBean> pager, SaTerminalInfoReqBean reqBean);

    /**
     * SA门店查询列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSaTerminalInfoList(HttpServletResponse response, SaTerminalInfoReqBean reqBean) throws Exception;



}
