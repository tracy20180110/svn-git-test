package com.biostime.samanage.repository.common;

import com.biostime.common.oraclerepo.BaseOracleRepository;
import com.biostime.samanage.bean.common.AreaOfficeResBean;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取用户所属的大区办事处
 * Date: 2016-6-22
 * Time: 9:58
 * User: 12804
 * Version:1.0
 */
public interface QueryAreaOfficeRepository extends BaseOracleRepository {

    /**
     * 获取用户所属的大区办事处
     * @param areaOfficeCode
     * @return
     */
    public List<AreaOfficeResBean> getUserAreaOffice(String areaOfficeCode);

}
