package com.biostime.samanage.repository.quota;

import com.biostime.samanage.bean.quota.DoubleSaQuotaBean;
import com.biostime.samanage.bean.quota.SaQuotaTerminalBean;

import java.text.ParseException;

public interface SaQuotaRepository {
    SaQuotaTerminalBean getTerminalBySaCode(String saCode);

    DoubleSaQuotaBean getDoubleSaQuota(String saCode, String terminalCode, String startTime, String endTime,String chainCode) throws ParseException;
}
