package com.biostime.samanage.repository.sales;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.sales.SalesSignMerchantReqBean;
import com.biostime.samanage.bean.sales.SalesSignReqBean;
import com.biostime.samanage.bean.sales.SalesSignResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Describe:促销员打卡Repository
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface SalesSignRepository {

    /**
     * 促销员打卡查询列表
     * @param reqBean
     * @return
     */
    public Pager<SalesSignResBean> querySalesSignList(Pager<SalesSignResBean> pager, SalesSignReqBean reqBean);

    /**
     * 促销员打卡查询列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSalesSignList(HttpServletResponse response, SalesSignReqBean reqBean) throws Exception;

    /**
     * 促销员打卡(手机端)
     * @param reqBean
     * @return
     */
    public void saveSalesSign(SalesSignMerchantReqBean reqBean) throws Exception;



    /**
     * 查询是否在指定的门店(手机端)
     * @param reqBean
     * @return
     */
    public ServiceBean queryIsInTerminal(SalesSignMerchantReqBean reqBean) throws Exception;


}
