package com.biostime.samanage.repository.sales;

import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.sales.SchedulingManageReqBean;
import com.biostime.samanage.bean.sales.SchedulingManageResBean;
import com.biostime.samanage.bean.sales.SchedulingReqBean;
import com.biostime.samanage.bean.sales.SchedulingResBean;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:工作排班
 * Date: 2019-03-15
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface SchedulingRepository {

    /**
     * 查询排班详情
     * @param reqBean
     * @return
     */
    public List<SchedulingResBean> querySchedulingInfo(SchedulingReqBean reqBean) ;

    /**
     * 查询排班管理详情
     * @param reqBean
     * @return
     */
    public List<SchedulingManageResBean> querySchedulingManageInfo(SchedulingManageReqBean reqBean) ;

   /**
     * 查询人员与终端
     * @param reqBean
     * @return
     */
   public List<BaseKeyValueBean> queryAccountTerminal(SchedulingReqBean reqBean) ;

    /**
     * 保存排班
     * @param reqBean
     * @return
     */
    public ServiceBean saveScheduling(SchedulingReqBean reqBean) ;

    /**
     * 排班查询列表导出
     * @param response
     * @param reqBean
     * @return
     */
    public void exportSchedulingList(HttpServletResponse response, SchedulingManageReqBean reqBean) throws Exception;


}
