package com.biostime.samanage.util;


/**
 * Function:初始化sql环境
 * <P> 版权所有 ©2013 Biostime Inc.. All Rights Reserved
 * <p> 未经本公司许可，不得以任何方式复制或使用本程序任何部分 <p>
 * User: 12360
 * Date: 2016/2/24
 * Time: 18:32
 */
public class InitializationPath {
    public void init() {
        String defalutRoot = this.getClass().getProtectionDomain().getCodeSource().getLocation().getFile();
        if(defalutRoot.indexOf("classes/")>-1){
            defalutRoot = defalutRoot.substring(0,defalutRoot.indexOf("classes/")+8);
            System.setProperty("classes.root", defalutRoot);
        }
//        TracingHelper.addTraceInfo("localIp", TracingExtraHelper.getLocalAddr());
    }
}
