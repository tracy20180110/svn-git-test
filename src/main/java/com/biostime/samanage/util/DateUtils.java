package com.biostime.samanage.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @description：时间工具类(用到java7及以上版本新特性)
 * @author: zwx
 * @createtime: 2016/2/22
 */
public class DateUtils {



    /**
     * @description 获取当前考核月  格式：yyyymm
     * @return
     * @version 1.0
     * @author  Tracy
     * @update  2015-2-1 上午10:33:06
     */
    public static String getNowMonthDate(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = new Date();
        return sdf.format(date);
    }


    /**
     * @description 获取当前考核月  格式：yyyymm
     * @return
     * @version 1.0
     * @update  2015-2-1 上午10:33:06
     */
    public static String getCurKaoHeMonth(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int d=calendar.get(Calendar.DATE);
        if(d>25){
            calendar.add(Calendar.MONTH, +1);
        }
        Date date = calendar.getTime();
        return sdf.format(date);
    }

    /**
     *
     * @description 根据传入的时间获取相应考核月的上一个考核月时间
     * @return
     * @version 1.0
     * @author  zwx
     */
    public static String getUpKaoHeMonth(Date datetime){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datetime);
        int d=calendar.get(Calendar.DATE);
        if(d>25){
            calendar.add(Calendar.MONTH,0);
        }else{
            calendar.add(Calendar.MONTH,-1);
        }
        Date date = calendar.getTime();
        return sdf.format(date);
    }

    /**
     *@description 获取当前是第几号
     * @return
     */
    public static int getCurrentDayNo(){
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DATE);
    }
    public static void main(String[] arg){
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println(sdf.format(getAddDate(sdf.parse(sdf.format(new Date())),1)));

            System.out.println(getAddReduceDate(new Date(),-3));
            System.out.println(getTimesnight(new Date()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @param date   ：时间
     * @param format ：字符串格式 (类似： yyyy-MM-dd HH:mm:ss )
     * @return  SSS
     * @description :时间转成相应格式字符串
     */
    public static String dateToString(Date date, String format
    ) {
        String dateStr = null;
        /*switch (format.toLowerCase()){
            case "yyyy-mm-dd":
                SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd");
                dateStr = sdf.format(date);
                break;
            case "yyyymmdd":
                SimpleDateFormat sdf1  = new SimpleDateFormat("yyyyMMdd");
                dateStr = sdf1.format(date);
                break;
            case "yyyymmddhhmmss":
                SimpleDateFormat sdf2  = new SimpleDateFormat("yyyyMMddHHmmss");
                dateStr = sdf2.format(date);
                break;
            case "yyyymmddhhmmsssss":
                SimpleDateFormat sdf3  = new SimpleDateFormat("yyyyMMddHHmmssSSS");
                dateStr = sdf3.format(date);
                break;
            case "yyyy-MM":
                SimpleDateFormat sdf3  = new SimpleDateFormat("yyyyMMddHHmmssSSS");
                dateStr = sdf3.format(date);
                break;
            default:break;
        }*/

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        dateStr = sdf.format(date);
        return dateStr;
    }

    /**
     * @param dateStr   ：时间
     * @param format ：字符串格式 (类似： yyyy-MM-dd HH:mm:ss )
     * @return  SSS
     * @description :时间转成相应格式字符串
     */
    public static Date stringToDate(String dateStr, String format) {
        Date date = null;
        try {
            /*switch (format.toLowerCase()) {
                case "yyyy-mm-dd":
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    date = sdf.parse(dateStr);
                    break;
                case "yyyymmdd":
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
                    date = sdf1.parse(dateStr);
                    break;
                case "yyyymmddhhmmss":
                    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
                    date = sdf2.parse(dateStr);
                    break;
                case "yyyymmddhhmmsssss":
                    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
                    date = sdf3.parse(dateStr);
                    break;
                default:
                    break;

            }*/
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(dateStr);
        }catch (Exception e){
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 方法描述: 根据传入月份和指定第几天返回当天日期
     *
     * @param specifiedDay 当前月份
     * @param month 当前月份+或-多少月
     * @param day 第几天
     * @return
     * @author zhuhaitao
     * @createDate 2016年4月5日 下午4:48:53
     */
    public static Date getSpecifiedDay(Date specifiedDay, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.setTime(specifiedDay);
        c.add(Calendar.MONTH, month);
        c.set(Calendar.DATE, day);
        return c.getTime();
    }
    /**
     * 方法描述: 根据传入月份和指定第几天返回当天日期（字符串）
     *
     * @param specifiedDay 当前月份
     * @param day 第几天
     * @return
     * @author zhuhaitao
     * @createDate 2016年4月5日 下午4:49:53
     */
    public static String getSpecifiedDayStr(Date specifiedDay, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.setTime(specifiedDay);
        c.add(Calendar.MONTH, month);
        c.set(Calendar.DATE, day);

        String dayAfter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(c.getTime());
        return dayAfter;
    }
    /**
     * 方法描述: 获得当天0点时间
     *
     * @return
     * @author zhuhaitao
     * @createDate 2016年4月8日 下午5:03:58
     */
    public static Date getTimesmorning(Date specifiedDay) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(specifiedDay);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    /**
     * 方法描述: 获得当天24点时间
     *
     * @return
     * @author zhuhaitao
     * @createDate 2016年4月8日 下午5:04:01
     */
    public static Date getTimesnight(Date specifiedDay) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(specifiedDay);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 方法描述: 获取指定某天24点
     *
     * @param specifiedDay
     * @param day
     * @return
     * @author zhuhaitao
     * @createDate 2016年5月12日 下午4:49:17
     */
    public static Date getSpecifiedTimesnight(int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, month);
        cal.set(Calendar.DATE, day);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 方法描述: 根据月份获取第一天0点时间
     *
     * @param month
     * @return
     * @throws ParseException
     * @author zhuhaitao
     * @createDate 2016年6月27日 下午4:17:07
     */
    public static Date getFirstDay(String month) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Calendar cal = Calendar.getInstance();
        cal.setTime(format.parse(month));
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    /**
     * 方法描述: 根据月份获取最后一天24点时间
     *
     * @return
     * @author zhuhaitao
     * @throws ParseException
     * @createDate 2016年4月8日 下午5:04:01
     */
    public static Date getLastDay(String month) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Calendar cal = Calendar.getInstance();
        cal.setTime(format.parse(month));
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 获取考核月（SA销售积分、SA会员明细、SA新客专用）
     * @param sdf
     * @param now
     * @param day
     * @return
     */
    public static int getTempDay(SimpleDateFormat sdf, Calendar now, int day) {
        int tempDay;
        if (day > 25) {
            now.set(Calendar.DAY_OF_MONTH , 26);

            tempDay = Integer.parseInt(sdf.format(now.getTime()));
        }else{
            now.add(Calendar.MONTH, -1);
            now.set(Calendar.DAY_OF_MONTH , 26);

            tempDay = Integer.parseInt(sdf.format(now.getTime()));
        }
        return tempDay;
    }

    public static Date getAddDate(Date date,int i){
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        System.out.println("当前日期:"+sf.format(c.getTime()));
        c.add(Calendar.DAY_OF_MONTH, i);
        System.out.println("增加一天后日期:"+sf.format(c.getTime()));
        return  c.getTime();
    }


    /**
     * @description 获取考核月  格式：yyyymm
     * @return
     * @version 1.0
     * @update  2015-2-1 上午10:33:06
     */
    public static Date getAssessmentMonth(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int d=calendar.get(Calendar.DATE);
        if(d>25){
            calendar.add(Calendar.MONTH, +1);
        }
        return calendar.getTime();
    }


    /**
     * 根据时间计算前后几小时
     * @param date
     * @param hour
     * @return
     */
    public static Date getAddReduceDate(Date date,int hour){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY,hour);
        return calendar.getTime();
    }

}
