package com.biostime.samanage.util;

/**
 * Created by rayleung on 2017/1/3.
 */
public enum OaResult {
    SUCCESS("0", "流程提交成功"),
    CREATE_PROCESS_FAIL("-1", "创建流程失败"),
    USER_NO_PRIVILAGE("-2", "用户没有流程创建权限"),
    CREATE_BASEINFO_FAIL("-3", "创建流程基本信息失败"),
    SAVE_MAINTABILE_FAIL("-4", "保存表单主表信息失败"),
    UPDATE_LEVEL_FAIL("-5", "更新紧急程度失败"),
    PROCESS_CREATOR_FAIL("-6", "流程操作者失败"),
    JUMP_TO_NEXT_NODE_FAIL("-7", "流转至下一节点失败"),
    NODE_ATTACH_OPERATION_FAIL("-8", "节点附加操作失败"),
    OTHER_ERROR("-100", "其他错误");


    private String code;

    private String originCode; //OA返回的原始码

    private String desc;

    OaResult(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    OaResult(String code, String originCode, String desc) {
        this.code = code;
        this.originCode = originCode;
        this.desc = desc;
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
