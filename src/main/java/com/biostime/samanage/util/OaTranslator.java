package com.biostime.samanage.util;

/**
 * 类功能描述: TODO
 *
 * @author 12360
 * @version 1.0
 * @createDate Nov 19, 2015 11:16:44 AM
 */
public class OaTranslator {

    public static OaResult translate(String flowId) {

        if (Integer.valueOf(flowId) > 0) {
            return OaResult.SUCCESS;
        }else {

            switch (flowId) {
                case "0":
                    return OaResult.SUCCESS;
                case "-1":
                    return OaResult.CREATE_PROCESS_FAIL;
                case "-2":
                    return OaResult.USER_NO_PRIVILAGE;
                case "-3":
                    return OaResult.CREATE_BASEINFO_FAIL;
                case "-4":
                    return OaResult.SAVE_MAINTABILE_FAIL;
                case "-5":
                    return OaResult.UPDATE_LEVEL_FAIL;
                case "-6":
                    return OaResult.PROCESS_CREATOR_FAIL;
                case "-7":
                    return OaResult.JUMP_TO_NEXT_NODE_FAIL;
                case "-8":
                    return OaResult.NODE_ATTACH_OPERATION_FAIL;
                default:
                    OaResult oaResult = OaResult.OTHER_ERROR;
                    oaResult.setOriginCode(flowId);
                    return oaResult;
            }
        }
    }

}
