
package com.biostime.samanage.util;


import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * @Description: 对于现场管理照片路径的处理类
 * 
 * @author Mico
 * 
 *         原则：按模块分类
 * 
 *         1、陈列模块
 * 
 *         2、曝光模块
 * 
 *         3、活动模块
 * 
 *         ......
 * 
 *         在各个模块下按考核月来来分类文件夹
 * 
 *         1、比如2012-5是指2012-4-26 00:00:00至2012-5-25 23:59:59 所以文件名叫201205
 * 
 *         2、照片文件名组成：登陆人编号+日期（年月日时分秒）
 * 
 *         综上所述，照片按模块、按考核月来分文件夹，主要目的是减少一个文件夹过多的照片，并不是在界面呈现的分类
 * 
 * 
 * 
 * @date 2012-5-4 上午09:51:25
 */
public class WsImageFileUtil {

	protected static Logger logger = Logger.getLogger(WsImageFileUtil.class.getName());


	//生产
	public static String UPLOAD_IP = "img2.mama100.com";

	public static final String UPLOAD_DIR = "upload";

	public static final String WORKSITE_DIR = "worksite";


	// 活动现场
	public static final String WS_ACT_WS_DIR = "ws_act";
	
	//医务活动
	public static final String SA_FTF_DIR = "sa_ftf";

	//开班
	public static final String SA_FTF_KB_DIR = "sa_kb_ftf";

	//FTF课程图标
	public static final String SA_FTF_KC = "sa_ftf_kc";

	//FTF课程礼品
	public static final String SA_FTF_LP = "sa_ftf_lp";

	//FTF活动礼品
	public static final String SA_FTF_ACT_LP = "sa_ftf_act_lp";

	//SA与门店关系
	public static final String SA_TERMINAL_MANAGE = "sa_terminal_manage";

	/**
	 * 模块文件夹
	 * 
	 * 如果有新的模块文件夹，直接在这里添加
	 * 
	 */
	public static List<String> MODULE_DIRS = new ArrayList<String>();

	/**
	 * 考核月文件夹,日期：如201205
	 */
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");

	private static SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");

	/**
	 * 图片服务器存储路径
	 */
	// private static String rootPath = "/opt/image/mkt_mobile/";
	private static String rootPath = "/opt/resources/img/samanage/";


	static {
		System.out.println("--最初--WsImageFileUtil.UPLOAD_IP----"+WsImageFileUtil.UPLOAD_IP);
		String runtimeEnv = System.getProperty("runtime.env");
		if(!runtimeEnv.equals("prd")){
			WsImageFileUtil.UPLOAD_IP = "img.mama100.cn";
		}
		System.out.println("----runtimeEnv----"+ runtimeEnv);
		System.out.println("----WsImageFileUtil.UPLOAD_IP----"+WsImageFileUtil.UPLOAD_IP);
		// T 加图片目录
		MODULE_DIRS.add(SA_FTF_DIR);
		MODULE_DIRS.add(WS_ACT_WS_DIR);
		MODULE_DIRS.add(SA_FTF_KC);
		MODULE_DIRS.add(SA_TERMINAL_MANAGE);
		// String rootPath =
		// ServletActionContext.getServletContext().getRealPath("/");

		/**
		 * 
		 * 初始化所有文件夹
		 * 
		 * 包括所有模块、当前考核月的文件夹
		 */

		Date date = convertToAuditDate(new Date());

		// 保存图片的目录: /upload
		File rootFile = new File(rootPath);
		if (!rootFile.exists()) {
			rootFile.mkdir();
		}

		// 保存图片的目录: /upload
		File uploadFile = new File(rootPath + UPLOAD_DIR);
		if (!uploadFile.exists()) {
			uploadFile.mkdir();
		}

		// 保存现场的目录：/upload/worksite
		File worksiteFile = new File(rootPath + UPLOAD_DIR + File.separator + WORKSITE_DIR);
		if (!worksiteFile.exists()) {
			worksiteFile.mkdir();
		}

		// 现场下的各个目录
		File f = null;
		for (int i = 0; i < MODULE_DIRS.size(); i++) {

			// 各个模块的目录
			f = new File(rootPath + UPLOAD_DIR + File.separator + WORKSITE_DIR + File.separator
					+ MODULE_DIRS.get(i));
			if (!f.exists()) {
				f.mkdir();
			}

			// 月份目录
			f = new File(rootPath + UPLOAD_DIR + File.separator + WORKSITE_DIR + File.separator
					+ MODULE_DIRS.get(i) + File.separator + sdf.format(date));
			if (!f.exists()) {
				f.mkdir();
			}
		}

	}

	public WsImageFileUtil() {

	}

	/**
	 * 对外提供的接口
	 * 
	 * 根据模块得到保存照片的文件夹路径
	 * 
	 * @param module
	 *            模块文件夹名称
	 * @return
	 */
	private static String buildFilePathByModule(String module) {

		/**
		 * 模块目录
		 */
		File f = new File(rootPath + UPLOAD_DIR + File.separator + WORKSITE_DIR + File.separator
				+ module);
		if (!f.exists()) {
			f.mkdir();
		}

		Date date = convertToAuditDate(new Date());
		f = new File(rootPath + UPLOAD_DIR + File.separator + WORKSITE_DIR + File.separator
				+ module + File.separator + sdf.format(date));
		if (!f.exists()) {
			f.mkdir();
		}

		return f.getPath();
	}

	public static void main(String[] args) {
		System.out.println(WsImageFileUtil.UPLOAD_IP);
//		System.out.println(buildFilePathByModule(WsImageFileUtil.SA_TERMINAL_MANAGE));
//		System.out.println(WsImageFileUtil.getFilePathDir(WsImageFileUtil.SA_TERMINAL_MANAGE));
	}
	/**
	 * 构建照片名称
	 * 
	 * 名称 ：大区-办事处-门店编号-日期-时间-上报人工号
	 * @return  
	 */
	public static File buildCustomerFileName(String module, String opno,String fileName,String suffix) {
		String dir = buildFilePathByModule(module);
		Date date = new Date();
		return new File(dir + File.separator + fileName + "_" + sdf1.format(date) + "_"+ opno + suffix);
	}

	/**
	 * 构建照片名称
	 *
	 * 名称 ：大区-办事处-门店编号-日期-时间-上报人工号
	 * @return
	 */
	public static String buildUploadFileName(String module, String opno,String fileName,String suffix) {
		String dir = buildFilePathByModule(module);
		Date date = new Date();
		fileName = dir + File.separator + fileName + "_" + sdf1.format(date) + "_"+ opno + suffix;
		//return new File(dir + File.separator + opno + "_" + sdf1.format(date) + "_"+ new Random().nextInt(1000) + ".jpg");
		return fileName;
	}


	public static String getFilePathDir(String module) {
		String dir = "/" + UPLOAD_DIR + "/" + WORKSITE_DIR + "/" + module + "/"
				+ sdf.format(convertToAuditDate(new Date())) + "/";
		return dir;
	}
	public static String getUploadDir(String module) {
		return buildFilePathByModule(module)+ "/";
	}

	/**
	 * 把当前日期转会员成考核月的日期
	 *
	 * @return
	 */
	private static Date convertToAuditDate(Date nowDate) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(nowDate);

		int year = cal.get(Calendar.YEAR);// 年
		int month = cal.get(Calendar.MONTH);// 月
		int date = cal.get(Calendar.DATE); // 日——代表第几日，比如26

		int newYear = year, newMonth = month;

		/**
		 * 2012.2.1-2012.2.25
		 */
		if (year == 2012 && month == 1) {
			if (date <= 23) {
				newMonth = month;
			} else {
				newMonth = month + 1;
			}
		} else if (year >= 2013 || (year == 2012 && month != 0 && month != 1)) {

			/**
			 * 2012.2.25开始
			 *
			 * 考核月是：本月26日 00:00:00~下月26日00:00:00 即本月26到下月25为一个考核月
			 *
			 */
			if (month == 11) {// 代表12月 比如：2013-12-26至2014-1-26
				if (date <= 25) {
					newMonth = month;
				} else {
					newYear = year + 1;
					newMonth = 0;  //2016-12-27 修改为0
				}
			} else {
				if (date <= 25) {
					newMonth = month;
				} else {
					newMonth = month + 1;
				}
			}

		} else if (year < 2012 || (year == 2012 && month == 0)) {
			// 按默认自然月
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

		Date customerDate = null;
		try {
			customerDate = sdf.parse(newYear + "-" + (newMonth + 1));
		} catch (ParseException e) {
			logger.severe(LogUtils.getStackTrace(e));
		}

		return customerDate;

	}


}
