package com.biostime.samanage.util;

/**
 * Created by 12804 on 2016-6-16.
 */
public class ArrayUtils {

    public static String arrayTostring(String[] strs){
        StringBuffer sbchanne = new StringBuffer();
        String string = "";
        for(String str:strs){
            sbchanne.append("'").append(str).append("'").append(",");
        }
        string = sbchanne.deleteCharAt(sbchanne.length()-1).toString();
        return string.toString();
    }
}
