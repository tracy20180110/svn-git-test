package com.biostime.samanage.util.rabbitMQ;

import com.biostime.mc.config.MCQueueEnum;
import com.biostime.samanage.service.single.SaEventDetailService;
import com.biostime.samanage.util.JsonUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class WxRegisterListener {

    @Autowired
    private SaEventDetailService saEventDetailService;

    private static final Logger logger = LogManager.getLogger(WxRegisterListener.class);

    @RabbitListener(queues = "#{routingKey}")
    public void saveSaWxBindNewCustomer(Object object) {
        try {
            System.out.println("--------WxRegisterListener--------");
            byte[] bytes = ((Message) object).getBody();
            Map map = (HashMap) SerializationUtils.deserialize(bytes);
            logger.info("监听注册绑定=============" + JsonUtils.toJson(map));
            saEventDetailService.saveSaWxBindNewCustomer(map);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Bean
    public String[] routingKey() {
        String[] names = new String[1];
        // 要监听处理的队列名称
        names[0] = MCQueueEnum.MAMA100_WECHAT_REGISTER_SA.getRoutingKey();
        return names;
    }

}
