package com.biostime.samanage.util.rabbitMQ;

import com.biostime.mc.config.MCQueueEnum;
import com.biostime.mc.producer.Producer;
import com.biostime.mc.util.MCUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Description:
 * <p>
 * 封装了RabbitMQ的发送端，其它业务模块统一使用这个公共类调用发送消息
 * <p>
 * 1、syncSend方式是通过已知的队列来发送
 * <p>
 * 2、发送短信、发送语音、发送微信、发送邮件、发送
 *
 * @author:3742
 * @date:2017-12-14 15:36
 */
@Component
public class RabbitMQManager {

    private static Logger logger = LoggerFactory.getLogger(RabbitMQManager.class.getName());


    @Autowired
    private Producer producer;

//    private MCProducer mcProducer;

    /**
     * 方法的功能描述: 根据已知队列来发送消息
     *
     * @param
     * @return
     * @author 3742
     * @createDate 2017/12/14 16:49
     */
    public void syncSend(MCQueueEnum mcQueueEnum, HashMap<String, String> message) {
        String routingKey = mcQueueEnum.getRoutingKey();
        String exchangeName = MCUtil.QUEUE_EXCHANGE.get(routingKey);
        syncSend(exchangeName, routingKey, message);
    }

    /**
     * 方法的功能描述: 自定义队列来发送消息，此队列没有在RabbitMQ备案
     *
     * @param
     * @return
     * @author 3742
     * @createDate 2017/12/14 16:53
     */
    public void syncSend(String exchangeName, String routingKey, HashMap<String, String> message) {
        Map<String, String> result = producer.send(exchangeName, routingKey, message);
        printSendResult(Integer.parseInt(result.get("code")), message);
    }

    /**
     * 方法的功能描述: 打印发送应答信息
     *
     * @param
     * @return
     * @author 3742
     * @createDate 2017/12/14 16:39
     */
    private void printSendResult(int resultCode, HashMap<String, String> message) {
        StringBuilder sb = new StringBuilder(150);
        for (HashMap.Entry<String, String> entry : message.entrySet()) {
            sb.append(entry.getKey() + ":").append(entry.getValue()).append("\n");
        }
        /*String templateId = message.get("templateId");
        if (StringUtils.isNotBlank(templateId)) {
            String msmContent = SMSTemplate.queryDescByCode(templateId);
            sb.append("msmContent:").append(msmContent);
        }*/
        if (resultCode == 0) {
            logger.info("推送成功，消息内容：\n" + sb.toString());
        } else {
            logger.info("发送失败，消息内容：\n" + sb.toString());
        }
    }

}
