package com.biostime.samanage.util;

import java.util.Random;

public class NumberUtil {
	
	/**
	 * 获取指定位数的随机数
	 * @param size int 位数
	 * @return 指定位数的随机数 String
	 */
	public static String getRandom(int size) {
		StringBuffer sb = new StringBuffer();
		Random r = new Random();
		for(int i=1;i<=size;i++){
			sb.append(r.nextInt(10));
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		System.out.println(getRandom(6));
	}
	
	public static boolean checkNumber(String  str){  
        String regex ="[\\d]+";
        return str.matches(regex);  
    }
	
}
