package com.biostime.samanage.util;


import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * <p>功能：http接口处理类 （包括get和post请求）
 * @author zwx
 *
 */
public class HttpUtils {

	private static Logger logger = Logger.getLogger(HttpUtils.class);

	public static String httpPostWithStringObject(String url, String json) throws Exception {
		String responseMsg = "";
    	try{
    		PostMethod post = null;
    		HttpClient httpclient = new HttpClient();
    		RequestEntity requestEntity = new StringRequestEntity(json,"text/json","UTF-8");
    		post = new PostMethod(url);
    		post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
    		post.addRequestHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
    		post.setRequestEntity(requestEntity);
    		int result = httpclient.executeMethod(post);
    		if (result == HttpStatus.SC_OK) {
    			responseMsg = new String(post.getResponseBodyAsString());
    		}else{
    			new ServiceException("ERROR:code="+result+";msg="+responseMsg);
    		}
    	}catch(Exception e){
    		throw new ServiceException("HTTP请求出错！"+e.getMessage());
    	}
    	return responseMsg;
	}
	public static String sendHttpByPost(String url, Map<String,String> paramMap) throws Exception{
		//建立HttpPost对象
		HttpPost httppost=new HttpPost(url);
		//建立一个NameValuePair数组，用于存储欲传送的参数
		List<BasicNameValuePair> params=new ArrayList<BasicNameValuePair>();
		//添加参数
		for(String key : paramMap.keySet()){
			params.add(new BasicNameValuePair(key,paramMap.get(key)));
		}
		//设置编码
		httppost.setEntity(new UrlEncodedFormEntity(params,HTTP.UTF_8));
		/*if(httpClient == null){
			httpClient = HttpClientUtil.getPoolingHttpClient();
		}
		HttpResponse response = httpClient.execute(httppost);
		*/
		//不使用线程池
		CloseableHttpClient httpClient2 = HttpClients.createDefault();
		//发送Post,并返回一个HttpResponse对象
		HttpResponse response = httpClient2.execute(httppost);
		String result = null;
		if(response.getStatusLine().getStatusCode()==200){//如果状态码为200,就是正常返回
			//得到返回的字符串
			result=EntityUtils.toString(response.getEntity());
		}
		return result;
	}
	/**
	 *
	 * 功能描述：带JSON对象的POST请求
	 *
	 * @param url
	 * @param json
	 * @return
	 * @throws Exception
	 * @author 12624HL
	 * @version DEALER 5.0
	 * @since 2017年8月23日
	 */
	public static String httpPostWithJSON(String url, String json) throws Exception {
		String responseMsg = "";
    	try{
    		PostMethod post = null;
    		HttpClient httpclient = new HttpClient();
    		RequestEntity requestEntity = new StringRequestEntity(json,"text/json","UTF-8");
    		post = new PostMethod(url);
    		post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
    		post.addRequestHeader(HTTP.CONTENT_TYPE, "application/json");
    		post.setRequestEntity(requestEntity);
    		int result = httpclient.executeMethod(post);
    		if (result == HttpStatus.SC_OK) {
    			responseMsg = new String(post.getResponseBodyAsString());
    		}else{
    			new ServiceException("ERROR:code="+result+";msg="+new String(post.getResponseBodyAsString()));
    		}
    	}catch(Exception e){
    		throw new ServiceException("HTTP请求出错！"+e.getMessage());
    	}
    	return responseMsg;
	}

	/**
	 *
	 * 功能描述：功能描述：带JSON对象、自定义HEADER的POST请求
	 *
	 * @param url
	 * @param json
	 * @return
	 * @throws Exception
	 * @author 12624HL
	 * @version DEALER 5.0
	 * @since 2017年8月23日
	 */
	public static String httpPostWithJSON(String url, String json,List<Header> list) throws Exception {
		String responseMsg = "";
    	try{
    		PostMethod post = null;
    		HttpClient httpclient = new HttpClient();
    		RequestEntity requestEntity = new StringRequestEntity(json,"text/json","UTF-8");
    		post = new PostMethod(url);
    		post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
    		post.addRequestHeader(HTTP.CONTENT_TYPE, "application/json");
    		if(ParamUtil.isList(list)){
    			for(Header header:list){
    				post.addRequestHeader(header);
    			}
    		}
    		post.setRequestEntity(requestEntity);
    		int result = httpclient.executeMethod(post);
    		if (result == HttpStatus.SC_OK) {
    			responseMsg = new String(post.getResponseBodyAsString());
    		}else{
    			new ServiceException("ERROR:code="+result+";msg="+new String(post.getResponseBodyAsString()));
    		}
    	}catch(Exception e){
    		throw new ServiceException("HTTP请求出错！"+e.getMessage());
    	}
    	return responseMsg;
	}

	public static String httpPostWithXML(String url, String xml) throws Exception {
		String responseMsg = "";
    	try{
    		PostMethod post = null;
    		HttpClient httpclient = new HttpClient();
    		RequestEntity requestEntity = new StringRequestEntity(xml,"text/xml","UTF-8");
    		post = new PostMethod(url);
    		post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
    		post.addRequestHeader(HTTP.CONTENT_TYPE, "application/xml");
    		post.setRequestEntity(requestEntity);
    		int result = httpclient.executeMethod(post);
    		if (result == HttpStatus.SC_OK) {
    			responseMsg = new String(post.getResponseBodyAsString());
    		}else{
    			new ServiceException("ERROR:code="+result+";msg="+responseMsg);
    		}
    	}catch(Exception e){
    		throw new ServiceException("HTTP请求出错！"+e.getMessage());
    	}
    	return responseMsg;
	}

	/**
	 * <p>功能：post请求
	 * @param paramName 参数名称
	 * @param paramValue 参数值
	 * @return
	 * @throws ServiceException
	 * @throws Exception
	 */
    public static String postHttp(String url,String [] paramName,String [] paramValue) throws ServiceException{
    	String responseMsg = "";
    	try{
    		PostMethod post = null;
    		HttpClient httpclient = new HttpClient();
    		post = new PostMethod(url);
    		post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
	    	// 设置参数
    		if(paramName != null && paramName.length > 0
			    	   && paramValue != null && paramValue.length > 0
			    	   && paramName.length == paramValue.length){
    		   for(int i=0;i<paramName.length;++i){ // 添加参数
    				post.setParameter(paramName[i], paramValue[i]);
    			}
    		   int result = httpclient.executeMethod(post);
    		   if (result == HttpStatus.SC_OK) {
    			   responseMsg = new String(post.getResponseBodyAsString());
    		   }
	    	}
    	}catch(Exception e){
    		throw new ServiceException("HTTP请求出错！"+e.getMessage());
    	}
       return responseMsg;
    }

    public static String getHttp(String url,String [] paramName,String [] paramValue){
    	String responseMsg = "";
    	try{
    		String params = "";
	    	// 设置参数
    		if(paramName != null && paramName.length > 0
			    	   && paramValue != null && paramValue.length > 0
			    	   && paramName.length == paramValue.length){
    		   for(int i=0;i<paramName.length;++i){ // 添加参数
    				if(i==0){
    					params += paramName[i]+"="+paramValue[i];
    				}else{
    					params += "&"+paramName[i]+"="+paramValue[i];
    				}
    			}
    		   GetMethod get = null;
    		   HttpClient httpclient = new HttpClient();
    		   get = new GetMethod(url+"?"+params);
    		   get.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
    		   int result = httpclient.executeMethod(get);
    		   if (result == HttpStatus.SC_OK) {
    			   responseMsg = new String(get.getResponseBodyAsString());
    		   }
	    	}

    	}catch(Exception e){
    		e.printStackTrace();
    	}
       return responseMsg;
    }

    public static String postHttpNew(String url,String [] paramName,String [] paramValue) throws ServiceException{
    	String responseMsg = "";
    	try{
    		String params = "";
	    	// 设置参数
    		if(paramName != null && paramName.length > 0
			    	   && paramValue != null && paramValue.length > 0
			    	   && paramName.length == paramValue.length){
    		   for(int i=0;i<paramName.length;++i){ // 添加参数
    				if(i==0){
    					params += paramName[i]+"="+paramValue[i];
    				}else{
    					params += "&"+paramName[i]+"="+paramValue[i];
    				}
    			}
    		   PostMethod post = null;
    		   HttpClient httpclient = new HttpClient();
    		   System.out.println(url+"?"+params);
    		   post = new PostMethod(url+"?"+params);
    		   post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
    		   int result = httpclient.executeMethod(post);
    		   if (result == HttpStatus.SC_OK) {
    			   responseMsg = new String(post.getResponseBodyAsString());
    		   }
	    	}

    	}catch(Exception e){
    		e.printStackTrace();
    	}
       return responseMsg;
    }

    /*
    public static String pojoHttp(String url,Object object,String keyword) throws Exception{
    	String responseMsg = "";
    	try{

    		PostMethod post = null;
    		HttpClient httpclient = new HttpClient();
    		post = new PostMethod(url);
    		post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
	    	// 设置参数
	    	XStream xstream = new XStream();//使用xstream转换pojo和xml字符串
	    	xstream.autodetectAnnotations(true);
    		// 设置参数
	    	post.setParameter("trade",xstream.toXML(object));
	    	post.setParameter("keyword", keyword);
    		   int result = httpclient.executeMethod(post);
    		   if (result == HttpStatus.SC_OK) {
    			   responseMsg = new String(post.getResponseBodyAsString());
    		   }

    	}catch(Exception e){
    		e.printStackTrace();
    	}
       return responseMsg;
    }
    */

    public static String postHttp(String url,String param,String paramType,String HeadType) throws Exception{
    	String responseMsg = "";
    	try{
    		PostMethod post = null;
	   		HttpClient httpclient = new HttpClient();
	   		RequestEntity requestEntity = new StringRequestEntity(param,paramType,"UTF-8");
	   		post = new PostMethod(url);
	   		post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
	   		post.addRequestHeader(HTTP.CONTENT_TYPE, HeadType);
	   		post.setRequestEntity(requestEntity);
	   		int result = httpclient.executeMethod(post);
	   		if (result == HttpStatus.SC_OK) {
	   			responseMsg = new String(post.getResponseBodyAsString());
	   		}else{
	   			new Exception("code="+HttpStatus.SC_OK+";msg="+responseMsg);
	   		}
	   	}catch(Exception e){
	   		throw new Exception("HTTP请求出错！入参:"+param+"异常："+e.getMessage());
	   	}
    	return responseMsg;
    }

    /**
     *
    * 方法描述: 可传入Content-Type
    * 传入为v=1.0&sign=&message={}拼接方式的方法
    * @param url
    * @param paramName
    * @param paramValue
    * @param paramType
    * @param HeadType
    * @return
    * @throws Exception
    * @author DELL
    * @createDate 2016-10-8 上午10:25:03
     */
    public static String postHttp(String url,String [] paramName,String [] paramValue,String paramType,String HeadType) throws Exception{
    	String responseMsg = "";
    	String params = "";
    	try{
    		PostMethod post = null;
	   		HttpClient httpclient = new HttpClient();
	   		// 设置参数
    		if(paramName != null && paramName.length > 0
			    	   && paramValue != null && paramValue.length > 0
			    	   && paramName.length == paramValue.length){

    		for(int i=0;i<paramName.length;++i){ // 添加参数
				if(i==0){
					params += paramName[i]+"="+paramValue[i];
				}else{
					params += "&"+paramName[i]+"="+paramValue[i];
				}
			}
	   		RequestEntity requestEntity = new StringRequestEntity(params,paramType,"UTF-8");
	   		post = new PostMethod(url);
	   		post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");  //传的时候需要转成utf-8,不然会乱码
	   		post.addRequestHeader(HTTP.CONTENT_TYPE, HeadType);
	   		post.setRequestEntity(requestEntity);
	   		int result = httpclient.executeMethod(post);
	   		responseMsg = new String(post.getResponseBodyAsString());
	   		if (result != HttpStatus.SC_OK) {
	   			throw new Exception("code="+HttpStatus.SC_OK+";msg="+responseMsg);
	   		}
    	  }
	   	}catch(Exception e){
	   		throw new Exception("HTTP请求出错！入参:"+params+"异常："+e.getMessage());
	   	}
    	return responseMsg;
    }

    /**
     *
    * 方法描述: 获取request请求中的body内容
    * 改方法能获取到内容的前提是
    * 根据Servlet规范，如果同时满足下列条件，则请求体(Entity)中的表单数据，将被填充到request的parameter集合中（request.getParameter系列方法可以读取相关数据）：
	*	1 这是一个HTTP/HTTPS请求
	*	2 请求方法是POST（querystring无论是否POST都将被设置到parameter中）
	*	3 请求的类型（Content-Type头）是application/x-www-form-urlencoded
	*	4 Servlet调用了getParameter系列方法
    *
    * @param br
    * @return
    * @throws Exception
    * @author w1025-test10
    * @createDate 2017-7-27 下午3:16:56
     */
    public static String getBodyString(BufferedReader br) throws Exception {
    	String inputLine;
	    String str = "";
	    try {
	       while ((inputLine = br.readLine()) != null) {
	        str += inputLine;
	       }
	    } catch (IOException e) {
			logger.error("getBodyString IOException:"+e.getMessage());
	       throw new ServiceException("获取请求体异常");
	    }finally{
	       br.close();
	    }
	    return str;
	 }



	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(new OutputStreamWriter(conn.getOutputStream(),"utf-8"));
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(
					new InputStreamReader(conn.getInputStream(),"utf-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！"+e);
			e.printStackTrace();
		}
		//使用finally块来关闭输出流、输入流
		finally{
			try{
				if(out!=null){
					out.close();
				}
				if(in!=null){
					in.close();
				}
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
		}
		return result;
	}

	public static String post(String url, String param) {
		// 创建默认的httpClient实例
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse httpResponse = null;
		// 发送Post请求
		try {
			// 用Post方法发送http请求
			HttpPost httpPost = new HttpPost(url);
			//logger.info("执行Post请求, uri: " + httpPost.getURI());

			StringEntity reqEntity = new StringEntity(param,"UTF-8");//解决中文乱码问题
			reqEntity.setContentEncoding("UTF-8");
			reqEntity.setContentType("application/json");
			httpPost.setEntity(reqEntity);
			logger.info("post url={},param={}"+url+param);
			httpResponse = httpClient.execute(httpPost);
			// response实体
			HttpEntity entity = httpResponse.getEntity();
			if (null != entity) {
				String response = EntityUtils.toString(entity,"UTF-8");
				logger.info("post url={},response={}"+url);
				int statusCode = httpResponse.getStatusLine().getStatusCode();
				if (statusCode == HttpStatus.SC_OK) {
					// 成功
					return response;
				} else {
					return null;
				}
			}
			return null;
		} catch (IOException e) {
			logger.error("post url={}请求失败,e={}"+url, e);
			return null;
		} finally {
			if (httpResponse != null) {
				try {
					EntityUtils.consume(httpResponse.getEntity());
					httpResponse.close();
				} catch (IOException e) {
					//logger.error("关闭 Post response失败", e);
				}
			}
		}
	}
}
