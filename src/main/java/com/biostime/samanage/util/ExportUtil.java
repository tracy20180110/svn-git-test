package com.biostime.samanage.util;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类功能描述: 导出门店工具类
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年3月16日 上午10:58:54
 */
public class ExportUtil implements java.io.Serializable {
	private static Logger logger = LoggerFactory.getLogger(ExportUtil.class);

	private static final long serialVersionUID = 4444953492658042108L;

	/**
	 * 方法描述: 导出Excel
	 * 
	 * @param titleName 复合表头(无复合表头时,titleName置为null即可)
	 * @param titleRange 复合表头起止列,2维数组(无复合表头时,titleRange置为null即可)
	 * @param dataList 数据集
	 * @param ths 数据行 th列头
	 * @param colNames 数据集字段
	 * @param outputStream 输出流(文件名)
	 * @author 7920
	 * @createDate 2016年1月12日 下午2:43:57
	 */
	public static void wirteDefaultExcel(String titleName[], Integer titleRange[][], List<?> dataList, String ths[],
			String colNames[], HttpServletResponse response) {

		Map<String, Object> defaultExcelFormat = getDefaultExcelFormat();
		SXSSFWorkbook defaultWorkbook = (SXSSFWorkbook) defaultExcelFormat.get("defaultWorkbook");
		Sheet defaultSheet = (Sheet) defaultExcelFormat.get("defaultSheet");
		CellStyle defaultStyle = (CellStyle) defaultExcelFormat.get("defaultStyle");
		CellStyle defaultDateCellStyle = (CellStyle) defaultExcelFormat.get("defaultDateCellStyle");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		int rowIndex = 0; // 当前操作的行数

		// 写数据行 th列头
		if (ths != null && ths.length > 0) {
			Row row = defaultSheet.createRow(rowIndex);

			for (short i = 0; i < ths.length; i++) {
				Cell cell = row.createCell(i);
				cell.setCellStyle(defaultStyle);
				XSSFRichTextString text = new XSSFRichTextString(ths[i]);
				cell.setCellValue(text);
			}
			rowIndex++;
		}

		// 写数据行
		for (int i = 0; i < dataList.size(); i++) {
			Object data = dataList.get(i);
			Row row = defaultSheet.createRow(rowIndex);
			for (short j = 0; j < colNames.length; j++) {
				Cell cell = row.createCell(j);
				// cell.setCellStyle(style2);

				Object value = null;
				try {
					value = PropertyUtils.getProperty(data, colNames[j]);
				} catch (Exception ex) {
					logger.error(ex.getMessage(), ex);
				}

				if (value instanceof Date) {
					cell.setCellValue(sdf.format(value));
					cell.setCellStyle(defaultDateCellStyle);
				} else if (value instanceof Double) {
					cell.setCellValue((Double) value);
				} else if (value instanceof Long) {
					cell.setCellValue((Long) value);
				} else if (value instanceof BigDecimal) {
					cell.setCellValue(((BigDecimal) value).doubleValue());
				} else {
					String valueStr = (value == null || "null".equals(value)) ? "" : String.valueOf(value);
					XSSFRichTextString richString = new XSSFRichTextString(valueStr);
					cell.setCellValue(richString);
				}

			}
			rowIndex++;
		}

		try {
			defaultWorkbook.write(response.getOutputStream());
			response.getOutputStream().close();
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}

	}

	/**
	 * 方法描述: 导出文件到指定目录
	 *
	 * @param titleName 复合表头(无复合表头时,titleName置为null即可)
	 * @param titleRange 复合表头起止列,2维数组(无复合表头时,titleRange置为null即可)
	 * @param dataList 数据集
	 * @param ths 数据行 th列头
	 * @param colNames 数据集字段
	 * @param fileName 文件名
	 * @throws FileNotFoundException
	 * @author zhuhaitao
	 * @createDate 2016年3月29日 下午5:18:54
	 */
	public static void wirteExcel(String titleName[], Integer titleRange[][], List<?> dataList, String ths[],
			String colNames[], String path) throws FileNotFoundException {

		File file = new File(path);
		FileOutputStream outStream = new FileOutputStream(file);

		Map<String, Object> defaultExcelFormat = getDefaultExcelFormat();
		SXSSFWorkbook defaultWorkbook = (SXSSFWorkbook) defaultExcelFormat.get("defaultWorkbook");
		Sheet defaultSheet = (Sheet) defaultExcelFormat.get("defaultSheet");
		CellStyle defaultStyle = (CellStyle) defaultExcelFormat.get("defaultStyle");
		CellStyle defaultDateCellStyle = (CellStyle) defaultExcelFormat.get("defaultDateCellStyle");

		int rowIndex = 0; // 当前操作的行数

		// 写数据行 th列头
		if (ths != null && ths.length > 0) {
			Row row = defaultSheet.createRow(rowIndex);

			for (short i = 0; i < ths.length; i++) {
				Cell cell = row.createCell(i);
				cell.setCellStyle(defaultStyle);
				XSSFRichTextString text = new XSSFRichTextString(ths[i]);
				cell.setCellValue(text);
			}
			rowIndex++;
		}

		// 写数据行
		for (int i = 0; i < dataList.size(); i++) {
			Object data = dataList.get(i);
			Row row = defaultSheet.createRow(rowIndex);
			for (short j = 0; j < colNames.length; j++) {
				Cell cell = row.createCell(j);
				// cell.setCellStyle(style2);

				Object value = null;
				try {
					value = PropertyUtils.getProperty(data, colNames[j]);
				} catch (Exception ex) {
					logger.error(ex.getMessage(), ex);
				}

				if (value instanceof Date) {
					cell.setCellValue((Date) value);
					cell.setCellStyle(defaultDateCellStyle);
				} else if (value instanceof Double) {
					cell.setCellValue((Double) value);
				} else if (value instanceof Long) {
					cell.setCellValue((Long) value);
				} else if (value instanceof BigDecimal) {
					cell.setCellValue(((BigDecimal) value).doubleValue());
				} else {
					String valueStr = (value == null || "null".equals(value)) ? "" : String.valueOf(value);
					XSSFRichTextString richString = new XSSFRichTextString(valueStr);
					cell.setCellValue(richString);
				}

			}
			rowIndex++;
		}

		try {
			defaultWorkbook.write(outStream);
			outStream.close();
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}

	}

	/**
	 * 方法描述: 获取默认的Excel表格及样式
	 * 
	 * @returnmap
	 * @author 7920
	 * @createDate 2016年1月12日 下午2:43:38
	 */
	public static Map<String, Object> getDefaultExcelFormat() {

		Map<String, Object> defaultExcelFormat = new HashMap<String, Object>();

		// 声明一个工作薄 SXSSFWorkbook
		SXSSFWorkbook defaultWorkbook = new SXSSFWorkbook();
		defaultExcelFormat.put("defaultWorkbook", defaultWorkbook);

		// 生成工作薄中的一个表格
		Sheet defaultSheet = defaultWorkbook.createSheet();
		// 设置表格默认列宽度为10个字节
		defaultSheet.setDefaultColumnWidth((short) 13);
		defaultExcelFormat.put("defaultSheet", defaultSheet);

		// 生成一个样式
		CellStyle defaultStyle = defaultWorkbook.createCellStyle();
		defaultStyle.setBorderBottom(CellStyle.BORDER_THIN);
		defaultStyle.setBorderTop(CellStyle.BORDER_THIN);
		defaultStyle.setBorderLeft(CellStyle.BORDER_THIN);
		defaultStyle.setBorderRight(CellStyle.BORDER_THIN);
		defaultStyle.setAlignment(CellStyle.ALIGN_CENTER);
		defaultStyle.setFillBackgroundColor(IndexedColors.SKY_BLUE.getIndex());
		defaultStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
		defaultStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		// 生成一个字体
		Font defaultFont = defaultWorkbook.createFont();
		defaultFont.setFontHeightInPoints((short) 12);
		defaultFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		// 把字体应用到当前的样式
		defaultStyle.setFont(defaultFont);
		defaultExcelFormat.put("defaultStyle", defaultStyle);

		/* 特殊样式 */
		// 日期样式
		CellStyle defaultDateCellStyle = defaultWorkbook.createCellStyle();
		DataFormat dateFormat = defaultWorkbook.createDataFormat();
		defaultDateCellStyle.setDataFormat(dateFormat.getFormat("yyyy-MM-dd"));
		defaultExcelFormat.put("defaultDateCellStyle", defaultDateCellStyle);

		return defaultExcelFormat;
	}
}
