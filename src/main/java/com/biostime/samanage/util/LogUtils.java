package com.biostime.samanage.util;

/**
 * @title 日志工具类
 * @description
 * @author 尹泉
 * @date 2009-4-28
 */
public class LogUtils {

	/**
	 * 输出完整的错误堆栈
	 * @param e
	 * @return
	 */
	public static String getStackTrace(Throwable e) {
		
		StringBuffer stack = new StringBuffer();
		stack.append(e);
		stack.append("\r\n");
		stack.append(e.getMessage());
		stack.append("\r\n");

		Throwable rootCause = e.getCause();

		while (rootCause != null) {
			stack.append("Root Cause:\r\n");
			stack.append(rootCause);
			stack.append("\r\n");
			stack.append(rootCause.getMessage());
			stack.append("\r\n");
			stack.append("StackTrace:\r\n");
			stack.append(rootCause);
			stack.append("\r\n");
			rootCause = rootCause.getCause();
		}

		for (int i = 0; i < e.getStackTrace().length; i++) {
			stack.append(e.getStackTrace()[i].toString());
			stack.append("\r\n");
		}
		
		return stack.toString();
	}

}
