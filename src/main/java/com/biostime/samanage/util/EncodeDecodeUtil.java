package com.biostime.samanage.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class EncodeDecodeUtil {
	//
	public static String KEY = "hejrwfgxaiwdfnoq";

	// DES编码格式
	public static String FORMAT = "DES/ECB/PKCS5Padding";

	public static String encodeStr(String encodeStr, String algorithm) {
		String encode_str = "";
		if (StringUtils.isNotEmpty(algorithm) && algorithm.equals("des")) {
			encode_str = desEncode(encodeStr);
		}
		return encode_str;
	}

	/**
	 * 加密
	 * 
	 * @param data
	 *            需加密的原始数据
	 * @return
	 * @throws Exception
	 */
	public static String desEncode(String data) {
		byte[] encodedBytes = null;
		try {
			// 从原始密匙数据创建DESKeySpec对象
			DESKeySpec dks = new DESKeySpec(KEY.getBytes("ASCII"));
			// 创建一个密匙工厂，然后用它把DESKeySpec转换成 一个SecretKey对象
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey securekey = keyFactory.generateSecret(dks);
			// Cipher对象实际完成加密操作
			Cipher cipher = Cipher.getInstance(FORMAT);
			// 用密匙初始化Cipher对象
			cipher.init(Cipher.ENCRYPT_MODE, securekey);
			// 现在，获取数据并加密
			// 正式执行加密操作
			byte[] result = cipher.doFinal(data.getBytes("UTF-8"));
			Base64 base64 = new Base64();
			encodedBytes = base64.encode(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return new String(encodedBytes);
	}

	/**
	 * 解密
	 * 
	 * @param encryptedStr
	 *            已加密的数据
	 * @return
	 * @throws Exception
	 */
	public static String desDecode(String encryptedStr) {
		String decryptedStr = "";
		try {
			// 先將Base64字串轉碼為byte[]
			Base64 base64 = new Base64();
			byte[] decodedBytes = base64.decode(encryptedStr.getBytes());
			// 建立解密所需的Key. 因為加密時的key是用ASCII轉換, 所以這邊也用ASCII做
			DESKeySpec objDesKeySpec = new DESKeySpec(KEY.getBytes("ASCII"));
			SecretKeyFactory objKeyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey objSecretKey = objKeyFactory.generateSecret(objDesKeySpec);
			// 設定一個DES/ECB/PKCS5Padding的Cipher
			// ECB對應到.Net的CipherMode.ECB
			// 用PKCS5Padding對應到.Net的PaddingMode.PKCS7
			Cipher objCipher = Cipher.getInstance(FORMAT);
			// 設定為解密模式, 並設定解密的key
			objCipher.init(Cipher.DECRYPT_MODE, objSecretKey);
			// 輸出解密後的字串. 因為加密時指定PaddingMode.PKCS7, 所以可以不用處理空字元
			// 不過若想保險點, 也是可以用trim()去處理過一遍
			decryptedStr = new String(objCipher.doFinal(decodedBytes), "UTF-8").trim();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return decryptedStr;
	}

	/**
	 * 加密
	 * 
	 * @param data
	 *            需加密的原始数据
	 * @return
	 * @throws Exception
	 */
	public static String desEncode(String data, String keyCustom) {
		byte[] encodedBytes = null;
		try {
			// 从原始密匙数据创建DESKeySpec对象
			DESKeySpec dks = new DESKeySpec(keyCustom.getBytes("ASCII"));
			// 创建一个密匙工厂，然后用它把DESKeySpec转换成 一个SecretKey对象
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey securekey = keyFactory.generateSecret(dks);
			// Cipher对象实际完成加密操作
			Cipher cipher = Cipher.getInstance(FORMAT);
			// 用密匙初始化Cipher对象
			cipher.init(Cipher.ENCRYPT_MODE, securekey);
			// 现在，获取数据并加密
			// 正式执行加密操作
			byte[] result = cipher.doFinal(data.getBytes("UTF-8"));
			Base64 base64 = new Base64();
			encodedBytes = base64.encode(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return new String(encodedBytes);
	}

	/**
	 * 解密
	 * 
	 * @param encryptedStr
	 *            已加密的数据
	 * @return
	 * @throws Exception
	 */
	public static String desDecode(String encryptedStr, String keyCustom) {
		String decryptedStr = "";
		try {
			// 先將Base64字串轉碼為byte[]
			Base64 base64 = new Base64();
			byte[] decodedBytes = base64.decode(encryptedStr.getBytes());
			// 建立解密所需的Key. 因為加密時的key是用ASCII轉換, 所以這邊也用ASCII做
			DESKeySpec objDesKeySpec = new DESKeySpec(keyCustom.getBytes("ASCII"));
			SecretKeyFactory objKeyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey objSecretKey = objKeyFactory.generateSecret(objDesKeySpec);
			// 設定一個DES/ECB/PKCS5Padding的Cipher
			// ECB對應到.Net的CipherMode.ECB
			// 用PKCS5Padding對應到.Net的PaddingMode.PKCS7
			Cipher objCipher = Cipher.getInstance(FORMAT);
			// 設定為解密模式, 並設定解密的key
			objCipher.init(Cipher.DECRYPT_MODE, objSecretKey);
			// 輸出解密後的字串. 因為加密時指定PaddingMode.PKCS7, 所以可以不用處理空字元
			// 不過若想保險點, 也是可以用trim()去處理過一遍
			decryptedStr = new String(objCipher.doFinal(decodedBytes), "UTF-8").trim();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return decryptedStr;
	}
	
	public static void main(String[] args) {
	/*	StringBuffer sb =  new StringBuffer();
		sb.append("<?xml version='1.0' encoding='utf-8'?><SumbitbarcodeStruct >");
        sb.append("<in_delivery_id>9999</in_delivery_id>");
        sb.append("<in_delivery_type>1</in_delivery_type >");
		sb.append("<in_receiver_id>100814</in_receiver_id>");
		sb.append("<in_receiver_type>100814</in_receiver_type>");
		sb.append("<in_barcodes>313304A00001,313204B00001,101119112100</in_barcodes>");
		sb.append("<in_operater>何华军 </in_operater>");  
		sb.append("<in_commit_time>2014-1-8</in_commit_time>");  
		sb.append("<in_remark>出库测试</in_remark>");    
		sb.append("<in_outstock_type>1</in_outstock_type>");       
		sb.append("<in_order_id>123456</in_order_id>");      
		sb.append("<in_flow_code>dealer_1</in_flow_code>");       
		sb.append("</SumbitbarcodeStruct>");
		
		EncodeDecodeUtil.desEncode(sb.toString());
		System.out.println(EncodeDecodeUtil.desDecode(EncodeDecodeUtil.desEncode(sb.toString())));*/
		
		
		System.out.println(EncodeDecodeUtil.desEncode("a888888", "dE@lEr09@8105BioS"));
	}
}
