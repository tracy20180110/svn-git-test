package com.biostime.samanage.util;

import cn.com.weaver.services.webservices.WorkflowService;
import cn.com.weaver.services.webservices.WorkflowServicePortType;
import com.biostime.samanage.bean.caravan.CaravanOaDetailReqInfo;
import com.biostime.samanage.bean.caravan.CaravanOaReqInfo;
import org.apache.commons.lang.StringUtils;
import weaver.workflow.webservices.*;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dc on 2017-12-20.
 */

public class Test {
    public static void main(String[] args) throws Exception{
        /*       // 1.获取工号对应的OA用户ID
        String workCode = "12580";
//        OaUserInfoDto oaUserInfoDto = oaMapper.getOaUserInfo(workCode);
        // 2.设置OA主要审批流程信息
        CaravanOaReqInfo caravanOaReqInfo = new CaravanOaReqInfo();
        caravanOaReqInfo.setApplyDate("2018-01-02");
        caravanOaReqInfo.setApplyId("2018010200001");
        caravanOaReqInfo.setApplyUser("12580");
        caravanOaReqInfo.setSubcompany("1");
        caravanOaReqInfo.setDepartment("6265");
        caravanOaReqInfo.setRequestLevel("0");
        caravanOaReqInfo.setRequestName("王锈文");
        caravanOaReqInfo.setCreatorId("23985");
        caravanOaReqInfo.setCreateType("0");
        caravanOaReqInfo.setWorkflowid("13988");
        caravanOaReqInfo.setCaravanType("1");
        //设置OA审批流程
        WorkflowBaseInfo wbi = new WorkflowBaseInfo();
        wbi.setWorkflowId("13988");
        // 设置要提交申请的信息
        WorkflowMainTableInfo wmti = returnSaMainTable(caravanOaReqInfo);
        // 设置流程请求信息
        WorkflowRequestInfo wri = new WorkflowRequestInfo();
        wri.setCreatetype(caravanOaReqInfo.getCreateType());
        wri.setCreatorId(caravanOaReqInfo.getCreatorId());
        wri.setRequestLevel(caravanOaReqInfo.getRequestLevel());
        wri.setRequestName("Y03.大小篷车申请流程");
        wri.setWorkflowMainTableInfo(wmti);
        wri.setWorkflowBaseInfo(wbi);

   *//*     List<WorkflowDetailTableInfo> workflowDetailTableInfo =
        ArrayOfWorkflowDetailTableInfo adInfos =
        wri.setWorkflowDetailTableInfos();*//*
        WorkflowServicePortType workflowServicePortType = new WorkflowService(new URL("http://10.50.115.128/services/WorkflowService?wsdl")).getWorkflowServiceHttpPort();
        String code = workflowServicePortType.doCreateWorkflowRequest(wri, Integer.valueOf(caravanOaReqInfo.getCreatorId()));
        OaResult ss =  OaTranslator.translate(code);
        System.out.print(ss.getCode());



*/





        String url = "http://10.50.115.128/services/WorkflowService?wsdl";   //测试
        //调用OA
        // 1.获取工号对应的OA用户ID
        String workCode = "12580";
//        Map map = queryOaAccountInfo(workCode);

        String Date = DateUtils.getNowMonthDate("yyyyMMdd");
        // 2.设置OA主要审批流程信息
        CaravanOaReqInfo caravanOaReqInfo = new CaravanOaReqInfo();
        caravanOaReqInfo.setApplyDate(Date);
        //设置appId
        String applyId = "Y03-" + Date + StringUtils.leftPad("22", 5, "0");
//        reqBean.setOaCode(applyId);
        caravanOaReqInfo.setRequestLevel("0");
//        caravanOaReqInfo.setCreateType(String.valueOf(reqBean.getCostType()));
        caravanOaReqInfo.setWorkflowid("13988");   //OA定义的
//        if (null != map) {
        caravanOaReqInfo.setCreatorId("23985");
        caravanOaReqInfo.setCreateType("0");
        caravanOaReqInfo.setWorkflowid("13988");
        caravanOaReqInfo.setApplyDate("2018-01-02");

        /*caravanOaReqInfo.setApplyId("2018010200001");
        caravanOaReqInfo.setApplyUser("12580");
        caravanOaReqInfo.setSubcompany("1");
        caravanOaReqInfo.setDepartment("6265");*/

//        }
        caravanOaReqInfo.setCaravanType(String.valueOf(1));
        caravanOaReqInfo.setStatus(String.valueOf(1));
        caravanOaReqInfo.setTerminalCode("80807" + "广医五院");
        caravanOaReqInfo.setDeliveryMode(String.valueOf(2));
        caravanOaReqInfo.setPutAddress("广州市天河区");
        caravanOaReqInfo.setPutSize("100*100");
        caravanOaReqInfo.setProjectCooperation("阿斯顿发圣诞");
        caravanOaReqInfo.setCostType(String.valueOf(1));
        caravanOaReqInfo.setConsignee("广东省");
        caravanOaReqInfo.setContactPhone("广州市");
        caravanOaReqInfo.setAddress("广东省广州市天河区光宝路");
        caravanOaReqInfo.setMemo("暂无备注");

        //设置OA审批流程
        WorkflowBaseInfo wbi = new WorkflowBaseInfo();
        wbi.setWorkflowId("13988");
        // 设置要提交申请的信息
        WorkflowMainTableInfo wmti = returnSaMainTable(caravanOaReqInfo);
        // 设置流程请求信息
        weaver.workflow.webservices.WorkflowRequestInfo wri = new weaver.workflow.webservices.WorkflowRequestInfo();
        wri.setCreatetype(caravanOaReqInfo.getCreateType());
        wri.setCreatorId(caravanOaReqInfo.getCreatorId());
        wri.setRequestLevel(caravanOaReqInfo.getRequestLevel());
        wri.setRequestName("Y03.大小篷车申请流程");
        wri.setWorkflowMainTableInfo(wmti);
        wri.setWorkflowBaseInfo(wbi);

              //明细字段
        List<CaravanOaDetailReqInfo> listDetailInfo = new ArrayList<CaravanOaDetailReqInfo>();
        CaravanOaDetailReqInfo detailInfo = new CaravanOaDetailReqInfo();
        detailInfo.setCmaterialName("合生元益生菌");
        detailInfo.setPrice("22.00");
        detailInfo.setCmaterialCode("1801010002");
        detailInfo.setNum("3");

        CaravanOaDetailReqInfo detailInfo2 = new CaravanOaDetailReqInfo();
        detailInfo2.setCmaterialName("葆艾纸尿裤");
        detailInfo2.setPrice("24.00");
        detailInfo2.setCmaterialCode("1801022202");
        detailInfo2.setNum("5");

        listDetailInfo.add(detailInfo);
        listDetailInfo.add(detailInfo2);



        /*List<WorkflowRequestTableRecord> tableRecordList = new ArrayList<WorkflowRequestTableRecord>();
        for(CaravanOaDetailReqInfo caravanOaDetailReqInfo:listDetailInfo){
            List<WorkflowRequestTableField> tableFieldList  = new ArrayList<WorkflowRequestTableField>();
            WorkflowRequestTableRecord wrtri = new WorkflowRequestTableRecord();
            Field[] fields = caravanOaDetailReqInfo.getClass().getFields();
            for (Field field : fields) {
                WorkflowRequestTableField tableField = new WorkflowRequestTableField();;
                field.setAccessible(true);
                String value = (String) field.get(caravanOaDetailReqInfo);
                String name = field.getName();
                tableField.setFieldName(name);//申请单号
                tableField.setFieldValue(value);
                tableField.setView(true);//字段是否可见
                tableField.setEdit(true);//字段是否可编辑

                tableFieldList.add(tableField);
            }
            ArrayOfWorkflowRequestTableField arrayTableField = new ArrayOfWorkflowRequestTableField();
            arrayTableField.setWorkflowRequestTableField(tableFieldList);
            wrtri.setWorkflowRequestTableFields(arrayTableField);
            tableRecordList.add(wrtri);

        }
        ArrayOfWorkflowRequestTableRecord tableRecord = new ArrayOfWorkflowRequestTableRecord();
        tableRecord.setWorkflowRequestTableRecord(tableRecordList);
        WorkflowDetailTableInfo wdti = new WorkflowDetailTableInfo();  //一个明细表
        wdti.setWorkflowRequestTableRecords(tableRecord);
        ArrayOfWorkflowDetailTableInfo detailTableInfo = new ArrayOfWorkflowDetailTableInfo();
        List<WorkflowDetailTableInfo> tableInfoList = detailTableInfo.getWorkflowDetailTableInfo();
        tableInfoList.add(wdti);
*/
        wri.setWorkflowDetailTableInfos(returnSaMainDetailTable(listDetailInfo));


        WorkflowServicePortType workflowServicePortType = new WorkflowService(new URL(url)).getWorkflowServiceHttpPort();
//        String code = workflowServicePortType.doCreateWorkflowRequest(wri, Integer.valueOf(caravanOaReqInfo.getCreatorId()));
        //TODO 获取不到对应的工号 暂时先写死
        String code = workflowServicePortType.doCreateWorkflowRequest(wri, 127);   //获取不到对应的工号 暂时先写死
        System.out.println("----调用OA审批返回-------"+code);
        OaResult oaResult = OaTranslator.translate(code);
        System.out.print(oaResult.getCode()+oaResult.getDesc());
    }


    public static  ArrayOfWorkflowDetailTableInfo returnSaMainDetailTable(List<CaravanOaDetailReqInfo> listDetailInfo) throws IllegalAccessException {
        List<WorkflowRequestTableRecord> tableRecordList = new ArrayList<WorkflowRequestTableRecord>();
        for(CaravanOaDetailReqInfo caravanOaDetailReqInfo:listDetailInfo){
            List<WorkflowRequestTableField> tableFieldList  = new ArrayList<WorkflowRequestTableField>();
            WorkflowRequestTableRecord wrtri = new WorkflowRequestTableRecord();
            Field[] fields = caravanOaDetailReqInfo.getClass().getFields();
            for (Field field : fields) {
                WorkflowRequestTableField tableField = new WorkflowRequestTableField();;
                field.setAccessible(true);
                String value = (String) field.get(caravanOaDetailReqInfo);
                String name = field.getName();
                tableField.setFieldName(name);//申请单号
                tableField.setFieldValue(value);
                tableField.setView(true);//字段是否可见
                tableField.setEdit(true);//字段是否可编辑

                tableFieldList.add(tableField);
            }
            ArrayOfWorkflowRequestTableField arrayTableField = new ArrayOfWorkflowRequestTableField();
            arrayTableField.setWorkflowRequestTableField(tableFieldList);
            wrtri.setWorkflowRequestTableFields(arrayTableField);
            tableRecordList.add(wrtri);

        }
        ArrayOfWorkflowRequestTableRecord tableRecord = new ArrayOfWorkflowRequestTableRecord();
        tableRecord.setWorkflowRequestTableRecord(tableRecordList);
        WorkflowDetailTableInfo wdti = new WorkflowDetailTableInfo();  //一个明细表
        wdti.setWorkflowRequestTableRecords(tableRecord);
        ArrayOfWorkflowDetailTableInfo detailTableInfo = new ArrayOfWorkflowDetailTableInfo();
        List<WorkflowDetailTableInfo> tableInfoList = detailTableInfo.getWorkflowDetailTableInfo();
        tableInfoList.add(wdti);
        return detailTableInfo;
    }

    public static  WorkflowMainTableInfo returnSaMainTable(CaravanOaReqInfo caravanOaReqInfo) throws IllegalAccessException {
        Field[] fields = caravanOaReqInfo.getClass().getFields();
        List<WorkflowRequestTableField> wdList = new ArrayList<WorkflowRequestTableField>();

        for (Field field : fields) {
            WorkflowRequestTableField wd = null;
            field.setAccessible(true);
            String value = (String) field.get(caravanOaReqInfo);
            String name = field.getName();
            wd = new WorkflowRequestTableField();
            wd.setFieldName(name);//申请单号
            wd.setFieldValue(value);
            wd.setView(true);//字段是否可见
            wd.setEdit(true);//字段是否可编辑
            wdList.add(wd);
        }
        ArrayOfWorkflowRequestTableField ad = new ArrayOfWorkflowRequestTableField();
        ad.setWorkflowRequestTableField(wdList);

        WorkflowRequestTableRecord wrtri = new WorkflowRequestTableRecord();  // 把fieldList封装到TableRecord对象去
        wrtri.setWorkflowRequestTableFields(ad);
        List<WorkflowRequestTableRecord> wdRecordList = new ArrayList<WorkflowRequestTableRecord>();
        wdRecordList.add(wrtri);
        ArrayOfWorkflowRequestTableRecord record = new ArrayOfWorkflowRequestTableRecord();
        record.setWorkflowRequestTableRecord(wdRecordList);   // 把tableRecordList封装到Array里去
        WorkflowMainTableInfo wmi = new WorkflowMainTableInfo();
        wmi.setRequestRecords(record);    // 把tableRecordList封装到主对象表里去
        return wmi;
    }


}
