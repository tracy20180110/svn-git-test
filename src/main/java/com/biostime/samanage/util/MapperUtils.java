package com.biostime.samanage.util;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 对象映射Mapper工具类
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author 12804
 * @createDate 2017-02-11
 */
public class MapperUtils {

	private static Mapper mapper = new DozerBeanMapper();

	/**
	 * 通过来源对象完成属性值映射生成新的目标对象
	 * 
	 * @param source
	 * @param destinationClass
	 * @return
	 * @author 12804
	 * @createDate 2017-01-01
	 */
	public static <T> T map(Object source, Class<T> destinationClass) {
		if (source == null) {
			return null;
		}
		return mapper.map(source, destinationClass);
	}

	/**
	 * 批量通过来源对象完成属性值映射生成新的目标对象
	 * 
	 * @param source
	 * @param destinationClass
	 * @return
	 * @author 12804
	 * @createDate 2017-01-01
	 */
	public static <T> List<T> map(List<?> source, Class<T> destinationClass) {
		if (CollectionUtils.isEmpty(source)) {
			return null;
		} else {
			List<T> target = new LinkedList<T>();
			for (Object o : source) {
				T to = MapperUtils.map(o, destinationClass);
				target.add(to);
			}
			return target;
		}
	}

	/**
	 * 批量通过来源对象完成属性值映射生成新的目标对象
	 * 
	 * @param map
	 * @param keyClz
	 * @param valueClz
	 * @return
	 * @author 12804
	 * @createDate 2017-01-01
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	public static <T, V> Map<T, V> map(Map map, Class<T> keyClz, Class<V> valueClz) {
		if (MapUtils.isEmpty(map)) {
			return null;
		} else {
			Map<T, V> target = new HashMap<T, V>();
			Set<Map.Entry> entrySet = map.entrySet();
			for (Map.Entry entry : entrySet) {
				T key = (T) entry.getKey();
				V value = MapperUtils.map(entry.getValue(), valueClz);
				target.put(key, value);
			}
			return target;
		}
	}
	/**
	 * 将request请求参数映射对应的实体
	 *
	 * @param request
	 * @param beanClass
	 * @return
	 * @author 12804
	 * @createDate 2017-01-01
	 */
	public static <T> T reqParameToBean(HttpServletRequest request, Class<T> beanClass) {
		try {
			/**创建封装数据的bean**/
			T bean = beanClass.newInstance();
			Map map = request.getParameterMap();
			BeanUtils.populate(bean, map);
			return bean;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
