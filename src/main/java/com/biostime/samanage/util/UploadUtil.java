package com.biostime.samanage.util;

import com.biostime.common.util.general.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.StringTokenizer;

/**
 * 上传工具类.
 */
public class UploadUtil {

	private static Logger logger = Logger.getLogger(UploadUtil.class);

	public static String uploadFile(MultipartFile file, HttpServletRequest request,String filepath,String fileName)
			throws IOException {
		StringTokenizer st = new StringTokenizer(file.getOriginalFilename(), ".");
		String[] st_array = new String[st.countTokens()];
		int k = 0;
		while (st.hasMoreElements()) {
			st_array[k++] = st.nextToken();
		}
		try {
			InputStream stream = file.getInputStream();
			String path = filepath + fileName;

			File uploadsFile = new File(filepath);
			if (!uploadsFile.exists())
				uploadsFile.mkdirs();

			OutputStream bos = new FileOutputStream(path);
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}
			bos.close();
			stream.close();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}

		return fileName;
	}

	/**
	 * 
	 * @param file 文件路径+名字
	 * @param data 文件内容
	 * @return
	 */
	public static boolean uploadFile(File file, String data){
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(data);
			bw.close();
			fw.close();
		} catch (IOException e) {
			logger.error("上传失败");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 64编码图片上传
	 * @param imgBase64
	 * @param upFileName
	 * @param moduleName
	 * @param projectName
	 * @param userId
	 * @return
	 */
	public static String uploadImage(String imgBase64,String upFileName,String moduleName,String projectName,String userId){
		String imgPath = "";
		try {
			if (StringUtil.isNotNullNorEmpty(imgBase64)) {
				//上传服务器的url
				String imgUrlPrefix = "http://" + WsImageFileUtil.UPLOAD_IP + projectName + WsImageFileUtil.getFilePathDir(moduleName);
				//png
				String base64Code = "";
				if (upFileName.endsWith(".png")) {
					base64Code = imgBase64.substring(3);
				} else {
					base64Code = imgBase64.substring(imgBase64.indexOf(File.separator));
				}

				String fileName = String.valueOf(System.currentTimeMillis());
				File imageFile = WsImageFileUtil.buildCustomerFileName(moduleName, userId, fileName, upFileName.substring(upFileName.lastIndexOf(".")));

				//将图片解码
				BASE64Decoder decoder = new BASE64Decoder();
				FileOutputStream write = new FileOutputStream(imageFile);
				byte[] decoderBytes = decoder.decodeBuffer(base64Code);
				write.write(decoderBytes);
				write.flush();
				write.close();
				imgPath = imgUrlPrefix + imageFile.getName();
			}

		}catch (Exception e){
			e.printStackTrace();
			logger.info("图片上传失败"+e.getMessage());
		}
		return imgPath;
	}
}
