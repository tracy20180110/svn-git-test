package com.biostime.samanage.util;


import java.io.Serializable;

/**
 * @author 12804
 */
public class Constant implements Serializable {

    private static final long serialVersionUID = -4911488338207234500L;

    //活动现场-关系配置 门店类型 其他
    public static final String TERMINAL_TYPE_OTHER = "2";


}
