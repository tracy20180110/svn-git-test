package com.biostime.samanage.calibratorchain;

import com.biostime.common.calibrator.CalibratorChain;
import com.biostime.samanage.calibrator.JobSchedulingAddCalibrator;

/**
 * 类功能描述: 保存促销员排班校验器
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2015年11月25日 下午12:01:46
 */
public class JobSchedulingAddCalibratorChain extends CalibratorChain {

	private static JobSchedulingAddCalibratorChain instance = new JobSchedulingAddCalibratorChain();

	public static JobSchedulingAddCalibratorChain instance() {
		if (instance == null) {
			instance = new JobSchedulingAddCalibratorChain();
		}
		return instance;
	}

	private JobSchedulingAddCalibratorChain() {
		// 1、保存促销员排班校验器
		addCalibrator(JobSchedulingAddCalibrator.instance());
	}
}
