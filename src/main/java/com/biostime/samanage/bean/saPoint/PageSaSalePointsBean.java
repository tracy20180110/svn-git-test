package com.biostime.samanage.bean.saPoint;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * sa消费积分分页bean
 */
@Data
public class PageSaSalePointsBean extends BaseBean {
    private static final long serialVersionUID = 4974044828670735542L;
    @AutoDocField(note = "分页参数", nullable = true)
    private int pageNo = 1;
    @AutoDocField(note = "默认每页20条", nullable = true)
    private int pageSize = 10;
    @AutoDocField(note = "sa销售积分", nullable = true)
    private SaSalePointsBean saSalePointsBean;
}