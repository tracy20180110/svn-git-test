package com.biostime.samanage.bean.saPoint;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.Date;

/**
 * SA销售积分
 */
@Data
public class SaSalePointsBean extends BaseBean {

	@AutoDocField(note = "分页参数", nullable = true)
	private Long id;
	private String areaName;
	private String officeCode;
	private String officeName;
	private String proCode;
	private String proBcc;
	private Long rank;
	private Long customerId;
	private String customerMobilePhone;
	private String productCustomizedType;
	private String productName;
	private Date registerDate;
	private String registerDateStr;
	private Long productPoints;
	private String newCustomerProductName;
	private Long newCustomerProductId;
	private Long productId;
	private String saCode;
	private String terminalCode;
	private String brand;
	private String babyConsultant;
	private String babyConsultantName;
	private String lastPointTime;
	private String lastPointOper;
	private String lastPointType;
	private String lastPointSrc;
	private String recSrc;
	private String isO2OOrder;
	private String isAppUser;
	private String isWeixinBind;
	private String isAssessSA;

	private String areaCode;
	private String bccCode;
	private String startDate;
	private String endDate;
	private String yearMonth;

	private String buCode;

}