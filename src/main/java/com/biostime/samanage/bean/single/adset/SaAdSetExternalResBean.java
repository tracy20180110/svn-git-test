package com.biostime.samanage.bean.single.adset;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA广告图设置
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaAdSetExternalResBean extends BaseBean {

    private static final long serialVersionUID = 347604056842620563L;

    @AutoDocField(note="配置广告图")
    private String image;

    @AutoDocField(note="广告详情")
    private String adInfo;

    @AutoDocField(note="广告详情类型（1：链接，2：图片）")
    private String adInfoType;

}
