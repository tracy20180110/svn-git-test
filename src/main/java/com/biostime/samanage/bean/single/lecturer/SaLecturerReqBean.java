package com.biostime.samanage.bean.single.lecturer;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

@Data
public class SaLecturerReqBean  extends BaseBean {
    private static final long serialVersionUID = 5009616789587749648L;

    private String lectureCode;   //讲师编码
    private String month;   //考核月（201810）
}
