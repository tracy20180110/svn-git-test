package com.biostime.samanage.bean.single.adset;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA广告图设置
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaAdSetResBean extends BaseBean {

    private static final long serialVersionUID = -1085012009490876103L;

    @AutoDocField(note="广告图id")
    private String id;

    @AutoDocField(note="连锁编码")
    private String lsTerminalCode;

    @AutoDocField(note="连锁名称")
    private String lsTerminalName;

    @AutoDocField(note="终端编码")
    private String terminalCode;

    @AutoDocField(note="终端名称")
    private String terminalName;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="广告类型")
    private String type;

    @AutoDocField(note="门店类型")
    private String terminalType;

    @AutoDocField(note="广告图片")
    private String image;

    @AutoDocField(note="状态")
    private String status;

    @AutoDocField(note="渠道")
    private String channelName;

    @AutoDocField(note="开始时间")
    private String startDate;

    @AutoDocField(note="结束时间")
    private String endTime;

    @AutoDocField(note="创建人")
    private String createdBy;

    @AutoDocField(note="创建人名称")
    private String createdName;

    @AutoDocField(note="创建时间")
    private String createdTime;

    @AutoDocField(note="链接")
    private String adInfo;

    @AutoDocField(note="链接类型")
    private String adInfoType;

    @AutoDocField(note="类型编码")
    private String typeCode;

    @AutoDocField(note="点击次数")
    private String clickCount;

    @AutoDocField(note="展示次数")
    private String showCount;

    @AutoDocField(note="状态编码") //3：过期    2：未开启    1：使用中    0：失效
    private String statusCode;



}
