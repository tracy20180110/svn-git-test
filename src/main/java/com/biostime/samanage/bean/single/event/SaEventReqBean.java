package com.biostime.samanage.bean.single.event;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:SA接触明细
 * Date: 2017-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class SaEventReqBean extends BaseBean {

    private static final long serialVersionUID = -8926913927607459497L;
    /*** 会员ID*/
    private Long customerId;

    /*** SA编码*/
    private String saCode;

    /*** 系统来源*/
    private Long systemSource;

    /**来源的具体业务类型*/
    private Long	srcType;

    /** 创建者 */
    private String createdBy;

    /** 创建者名称 */
    private String createdName;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条

}
