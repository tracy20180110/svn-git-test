package com.biostime.samanage.bean.single.lecturer;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

@Data
public class SaLecturerResBean extends BaseBean {

    private static final long serialVersionUID = -2536356370997009246L;

    private String actType;   //活动类型

    private String effectiveNo;   //有效场次（一场活动签到人数大于15）

    private String signNum; //签到人数

    private String saPxNewCust; //SA品项新客

    private String actTypeCode;   //活动类型编码
}
