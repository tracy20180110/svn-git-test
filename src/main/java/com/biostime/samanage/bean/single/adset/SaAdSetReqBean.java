package com.biostime.samanage.bean.single.adset;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA广告图设置
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaAdSetReqBean extends BaseBean {
    private static final long serialVersionUID = -8465172036018716879L;

    @AutoDocField(note="大区办事处")
    private String areaOfficeCode;

    @AutoDocField(note="渠道")
    private String channelCode;

    //广告类型（0：无门店信息，1：全部门店：2：指定门店）
    @AutoDocField(note="广告类型")
    private Integer type;

    @AutoDocField(note="门店编码")
    private String terminalCode;

    @AutoDocField(note="配置广告图")
    private String image;

    @AutoDocField(note="广告详情")
    private String adInfo;

    @AutoDocField(note="广告详情类型")
    private Integer adInfoType;

    @AutoDocField(note="开始时间")
    private String startDate;

    @AutoDocField(note="结束时间")
    private String endDate;

    @AutoDocField(note="创建人")
    private String createdBy;

    @AutoDocField(note="创建人名称")
    private String createName;

    @AutoDocField(note="广告图ID")
    private String id;

    @AutoDocField(note="状态编码0：失效，1：使用中")
    private String statusCode;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
