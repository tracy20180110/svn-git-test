package com.biostime.samanage.bean.single.lecturer;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * @author 12804
 */
@Data
public class SaLecturerActResBean extends BaseBean {

    @AutoDocField(note="讲师ID")
    private String lecturerId;

    @AutoDocField(note="讲师名称")
    private String name;

    @AutoDocField(note="讲师手机")
    private String mobile;

    @AutoDocField(note="讲师类型")
    private String type;

    @AutoDocField(note="单位")
    private String company;

    @AutoDocField(note="讲师头衔")
    private String rank;

    @AutoDocField(note="讲师工号")
    private String code;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="活动开始日期")
    private String startDate;

    @AutoDocField(note="活动结束日期")
    private String endDate;

    @AutoDocField(note="讲师状态")
    private String status;

    @AutoDocField(note="场次汇总")
    private String allActTimes;

    @AutoDocField(note="SA-FTF场次")
    private String saFtfTimes;

    @AutoDocField(note="门店-FTF场次")
    private String terminalFtfTimes;

    @AutoDocField(note="学术推广场次")
    private String learningTimes;

    @AutoDocField(note="内训场次")
    private String nternalTimes;

    @AutoDocField(note="SA活动ID")
    private String ftfActId;

    @AutoDocField(note="活动类型")
    private String actType;

    @AutoDocField(note="活动名称")
    private String actName;

    @AutoDocField(note="活动时间")
    private String actTime;

    @AutoDocField(note="活动地点")
    private String actAdress;

    @AutoDocField(note="支持类型")
    private String actExpertType;

    @AutoDocField(note="签到人数")
    private String signNums;

    @AutoDocField(note="FTF品项新客")
    private String ftfBrandNums;

    @AutoDocField(note="社群活动场次")
    private String communityActNums;



 }
