package com.biostime.samanage.bean.single.event;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:SA接触明细
 * Date: 2017-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class SaEventDetailReqBean extends BaseBean {

    private static final long serialVersionUID = 5639352016130822702L;
    private String areaOfficeCode;  //大区办事处编码
    private String saCode;          //SA编码
    private String customerId;      //会员ID
    private String customerMobile;  //手机号码
    private String systemSourse;    //系统来源;
    private String eventSourse;     //接触来源;
    private String startDate;       //接触开始日期;
    private String endDate;        //接触结束日期;

    private String buCode;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条

}
