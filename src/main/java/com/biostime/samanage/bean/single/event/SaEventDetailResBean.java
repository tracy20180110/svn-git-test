package com.biostime.samanage.bean.single.event;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:SA接触明细
 * Date: 2017-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class SaEventDetailResBean extends BaseBean {

    private static final long serialVersionUID = 5336427785464085489L;
    private String areaName;        //大区名称
    private String officeName;      //办事处名称
    private String channelName;     //渠道名称
    private String saCode;          //SA编号
    private String saName;          //SA名称
    private String systemSourse;    //系统来源
    private String eventSourse;     //接触来源
    private String customerId;      //会员ID
    private String customerMobile;  //会员手机号码
    private String bccName;         //BCC名称
    private String bccCode;         //BCC编码
    private String yygwCode;        //育婴顾问编码
    private String yygwName;        //育婴顾问名称
    private String createdTime;     //创建时间

}
