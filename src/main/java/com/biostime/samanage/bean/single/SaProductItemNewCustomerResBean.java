package com.biostime.samanage.bean.single;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:SA品项新客
 * Date: 2017-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class SaProductItemNewCustomerResBean extends BaseBean {
    private static final long serialVersionUID = 6734470134322625208L;

    private String areaName;
    private String officeName;
    private String saCode;          //SA编号
    private String saAttr;          //SA属性
    private String customerId ;   //会员ID
    private String babyBirthday ; //宝宝生日
    private String mobile ; //手机
    private String bindWxTime ; //绑定微信时间
    private String pointSource ; //积分来源
    private String bccCode ;    //bcc工号
    private String yygwCode ; //育婴顾问
    private String yezdsCode ; //育儿指导师
    private String productName ; //产品名称
    private String pointTime ; //积分时间
    private String buyTerminal ; //购买门店
    private String brandName ; //品牌
    private String productItemName ; //品项
    private String milkStage; //奶粉阶段
    private String isSaYnNewCustomer;  //是否SAYN新客
    private String isFtfNewCustomer;  //是否FTF新客
    private String isNewCustGiftPkg; //是否来自新客礼包

    private String yygwByCompany; //育婴顾问归属

}
