package com.biostime.samanage.bean.single;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:SA品项新客
 * Date: 2019-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class SaProductItemNewCustomerReqBean extends BaseBean {


    private static final long serialVersionUID = -3598399574968893693L;

    //大区办事处
    private String areaOfficeCode ;

    //会员ID
    private String customerId ;

    //sa编码
    private String saCode ;

    //bcc工号
    private String bccCode ;

    //育婴顾问
    private String yygwCode ;

    //育儿指导师
    private String yezdsCode ;
    //手机
    private String mobile ;
    //积分来源
    private String pointSource ;

    //奶粉阶段
    private String milkStage;

    //积分开始时间
    private String startDate;

    //积分结束时间
    private String endDate;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条

}
