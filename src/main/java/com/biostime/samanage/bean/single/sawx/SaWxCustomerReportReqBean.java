package com.biostime.samanage.bean.single.sawx;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA微信会员报表ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaWxCustomerReportReqBean extends BaseBean {

    private static final long serialVersionUID = 6815596463401470535L;

    @AutoDocField(note="大区办事处")
    private String areaOfficeCode;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="BCC工号")
    private String bccCode;

    @AutoDocField(note="育婴顾问工号")
    private String yygwCode;

    @AutoDocField(note="会员手机号码")
    private String customerId;

    @AutoDocField(note="手机号码")
    private String customerMobile;

    @AutoDocField(note="活动开始时间")
    private String startDate;

    @AutoDocField(note="活动结束时间")
    private String endDate;

    @AutoDocField(note="事业部编码")
    private String buCode;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
