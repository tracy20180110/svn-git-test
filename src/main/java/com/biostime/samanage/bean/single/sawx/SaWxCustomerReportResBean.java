package com.biostime.samanage.bean.single.sawx;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA微信会员报表ResBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaWxCustomerReportResBean extends BaseBean {
    private String areaName;
    private String officeName;
    private String saCode;          //SA编号
    private String saName;          //SA名称
    private String khSaCode;        //是否考核门店
    private String customerId;      //会员ID
    private String customerMobile;  //会员手机号码
    private String customerName;    //会员名称
    private String customerRole;    //用户角色
    private String bccName;         //BCC名称
    private String bccCode;         //BCC编码
    private String yygwCode;        //育婴顾问编码
    private String yygwName;        //育婴顾问名称
    private String customerNum;        //当月成为SA新客次数
    private String createdTime;
    private String birthDate;

    private String yygwByCompany; //育婴顾问归属

}
