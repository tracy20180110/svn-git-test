package com.biostime.samanage.bean.single.lecturer;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * @author 12804
 */
@Data
public class SaLecturerActReqBean extends BaseBean {
    private static final long serialVersionUID = 7147893510983538058L;

    @AutoDocField(note="大区办事处")
    private String areaOfficeCode;

    @AutoDocField(note="讲师类型")
    private String type;

    @AutoDocField(note="讲师名称")
    private String name;

    @AutoDocField(note="活动开始日期")
    private String startDate;

    @AutoDocField(note="活动结束日期")
    private String endDate;

    @AutoDocField(note="讲师ID")
    private String lecturerId;

    @AutoDocField(note="事业部")
    private String buCode;


    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
