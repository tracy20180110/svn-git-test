package com.biostime.samanage.bean.common;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * 分页通用bean
 * Created by zhuhaitao on 2017/1/3.
 */
@Data
public class PageBaseBean extends BaseBean {
    private static final long serialVersionUID = -3083532522233597386L;
    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
