package com.biostime.samanage.bean.common;

import java.io.Serializable;

/**
 * 类功能描述: 获取当然系统登录账号
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年3月22日 下午4:55:36
 */
public class SessionUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5431845113033134520L;

	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public static final String SESSTION_USER = "userInfo";
	public static final String SESSION_USERID = "userid";

}
