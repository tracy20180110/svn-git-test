package com.biostime.samanage.bean.common;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取人员所属大区办事处Res
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class AreaOfficeResBean  extends BaseBean {

    private static final long serialVersionUID = 1961712004325247729L;
    @AutoDocField(note="大区办事处编码",nullable = true)
    private String code; //

    @AutoDocField(note="大区办事处名称",nullable = true)
    private String name;

    @AutoDocField(note="大区对应的办事处",nullable = true)
    private List<AreaOfficeResBean> officeBeanList;
}
