package com.biostime.samanage.bean.common;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取人员所属大区办事处Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class AreaOfficeReqBean  extends BaseBean {

    private static final long serialVersionUID = -2550384793399468855L;

    @AutoDocField(note="登录人大区办事处",nullable = true)
    private String areaOfficeCode;
}
