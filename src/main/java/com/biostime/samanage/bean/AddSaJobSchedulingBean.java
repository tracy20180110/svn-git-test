package com.biostime.samanage.bean;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述: SA工作排班
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年6月23日 上午10:59:18
 */
@Data
public class AddSaJobSchedulingBean extends BaseBean {
	/** 
	* @Fields serialVersionUID : TODO
	*/
	private static final long serialVersionUID = -6604891535948672926L;
	@AutoDocField(note = "促销员id", nullable = true)
	private String salesAccountNo;
	@AutoDocField(note = "工作日期", nullable = true)
	private String companyDate;
	@AutoDocField(note = "SA工作排班列表", nullable = true)
	private List<SaJobSchedulingBean> saJobSchedulingBeans = new ArrayList<SaJobSchedulingBean>();
}