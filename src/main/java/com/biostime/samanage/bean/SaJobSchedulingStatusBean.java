package com.biostime.samanage.bean;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.Date;

/**
 * 类功能描述: SA促销员工作排班状态表
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年6月23日 上午10:59:18
 */
@Data
public class SaJobSchedulingStatusBean extends BaseBean {
	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1981013643318899862L;
	@AutoDocField(note = "id", nullable = true)
	private Long id;
	@AutoDocField(note = "促销员id", nullable = true)
	private Long salesAccountNo;
	@AutoDocField(note = "工作日期", nullable = true)
	private Date companyDate;
	@AutoDocField(note = "状态（0休息，1排班）", nullable = true)
	private Integer status;
}