package com.biostime.samanage.bean;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * 类功能描述: SA工作排班
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年6月23日 上午10:59:18
 */
@Data
public class SaJobSchedulingSalesAccountBean extends BaseBean {
	private static final long serialVersionUID = -489655629817627553L;
	@AutoDocField(note = "促销员id", nullable = true)
	private String salesAccountNo;
	@AutoDocField(note = "促销员名称", nullable = true)
	private String name;
	@AutoDocField(note = "排班状态", nullable = true)
	private String status;
}