package com.biostime.samanage.bean.actroadshow;

import lombok.Data;

import java.io.Serializable;

/**
 * 路演活动门店
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRoadShowTerminalReqBean implements Serializable {

    private static final long serialVersionUID = -3238468880445111438L;
    private String actId;//活动ID
    private String userId;//登陆人ID
    private String createdName;//登陆人名称
    private String areaOfficeCode; //大区办事处编码
    private String channelCode; //渠道

}


