package com.biostime.samanage.bean.actroadshow;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:路演活动配置
 * Date: 2018-03-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRoadShowReqBean extends BaseBean {

    private static final long serialVersionUID = 3237950733520568342L;

    @AutoDocField(note="品牌编码")   // 如：1,2,3
    private String brandCode;

    @AutoDocField(note="渠道编码")   //如：01,02,08
    private String channelCode;

    @AutoDocField(note="活动类型编码")  //  如：1,2
    private String terminalTypeCode;

    @AutoDocField(note="活动上报编码")  //  如：1,2
    private String reportTypeCode;

    @AutoDocField(note="swisse名称")
    private String swName;

    @AutoDocField(note="1:门店类型,2:swisse列表")
    private String type;


}
