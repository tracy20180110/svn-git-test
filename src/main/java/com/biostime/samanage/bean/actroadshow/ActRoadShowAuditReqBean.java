package com.biostime.samanage.bean.actroadshow;

import lombok.Data;

import java.io.Serializable;


/**
 * 路演活动审核
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2016-08-16
 */
@Data
public class ActRoadShowAuditReqBean implements Serializable {

    private static final long serialVersionUID = 8344237613717299447L;
    private String actDetailId;    //ID
    private String isSuffice;     //是否合格
    private String auditMemo;     //审批备注
    private String userId;  //登录人ID
    private String createdName; //登陆人名称
}


