package com.biostime.samanage.bean.actroadshow;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:路演活动配置--列表返回
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRoadShowConfigResBean extends BaseBean {

    private static final long serialVersionUID = 1810962551801926601L;

    @AutoDocField(note="id")
    private String id;

    @AutoDocField(note="活动类型")
    private String actTypeName;

    @AutoDocField(note="品牌1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie")
    private String brandName;

    @AutoDocField(note="渠道 01：婴线,02:商超,03:药线,13:美妆,08:SA")
    private String channelName;

    @AutoDocField(note="门店类型 1：区域重点连锁,2：其他,3:婴KA")
    private String terminalType;

    @AutoDocField(note="上报类型 1:活动上报，2：陈列上报")
    private String reportTypeName;

    @AutoDocField(note="活动状态1：启用，2：停用")
    private String statusName;

    @AutoDocField(note="活动状态1：启用，2：停用")
    private String statusCode;

    @AutoDocField(note="最近操作时间")
    private String updatedTime;

    @AutoDocField(note="最近操作人")
    private String updatedBy;

    @AutoDocField(note="创建时间")
    private String createdTime;


}
