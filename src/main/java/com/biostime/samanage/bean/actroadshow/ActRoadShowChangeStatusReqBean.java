package com.biostime.samanage.bean.actroadshow;

import lombok.Data;

import java.io.Serializable;


/**
 * 大小篷车活动备案
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2016-08-16
 */
@Data
public class ActRoadShowChangeStatusReqBean implements Serializable {

    private static final long serialVersionUID = 2516694789188798653L;
    private String id;    //ID
    private String status;
    private String userId;  //登录人ID
    private String userName; //登陆人名称
}


