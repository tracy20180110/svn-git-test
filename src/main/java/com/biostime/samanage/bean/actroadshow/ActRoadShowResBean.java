package com.biostime.samanage.bean.actroadshow;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:路演活动配置
 * Date: 2018-03-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRoadShowResBean extends BaseBean {

    private static final long serialVersionUID = 867244123160957078L;

    @AutoDocField(note="编码")
    private String code;

    @AutoDocField(note="名称")
    private String name;

}
