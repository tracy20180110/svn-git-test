package com.biostime.samanage.bean.actroadshow;

import lombok.Data;

import java.io.Serializable;

/**
 * 路演活动门店
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRoadShowTerminalResBean implements Serializable {

    private static final long serialVersionUID = -2204334566616276894L;
    private String id;
    private String terminalCode;//活动门店
    private String startDate;//开始时间
    private String endDate;   //结束时间
    private String status;

}


