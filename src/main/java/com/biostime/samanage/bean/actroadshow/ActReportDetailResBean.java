package com.biostime.samanage.bean.actroadshow;

import lombok.Data;

import java.util.List;

/**
 * Describe:手机上报明细
 * Date: 2018-03-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActReportDetailResBean {
    //上报人数
    private String reportCount;
    //总数
    private String allCount;

    private List<ActRoadShowDetailResBean> arsdList;
}
