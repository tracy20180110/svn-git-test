package com.biostime.samanage.bean.actroadshow;

import lombok.Data;

import java.io.Serializable;

/**
 * 路演考核报表
 * Created by dc on 2016/8/16.
 */
@Data
public class ActRoadShowReportResBean implements Serializable {

    private static final long serialVersionUID = -3044470274359763893L;
    private String areaName;//大区名称
    private String officeName;//办事处名称
    private String channelName;//渠道
    private String terminalCode;//门店编号
    private String terminalName;//门店名称
    private String startDate;//开始时间
    private String endDate;//结束时间

    private String lsTerminalCode; //连锁门店编码
    private String lsTerminalName;//连锁门店名称
    private String type;        //类型  去掉

    private String actTypeName;        //类型
    private String khTypeName;        //活动考核类型

    private String allPoint; //全品项积分
    private String milkNewCustomer; //活动期奶粉新客（合生元）
    private String ysjNewCustomer; //活动期益生菌新客

    private String wxNewCustomer; //活动期微信新会员
    private String compliance; //达标情况

    private String actId;   //活动ID
    private String actName; //活动名称
    private String status;  //活动状态

    private String isUploadPhoto; // 是否上传照片
    private String isQualified; // 照片是否合格
    private String reportingTime; // 上报日期

    private String swPosNum;  //"Swisse活动SKU的POS数（元）
    private String ht4g; // HT罐数(400g)
    private String ht8g; // HT罐数(800g)
    private String dodieNum; // Dodie纸尿裤包数

    //2018-09 版本
    private String brandCode;  //品牌编码
    private String brandName;  //品牌名称
    private String milkNewCust; //奶粉新客
    private String htNewCust; //ht新客
    private String reportBy;   //上报人工号
    private String reportName; //上报人名称
    //201907
    private String dodieNewCustomerNum; //dodie新客
    //2018-12
    //SA关联活动名称
    private String saFtfActName;

    //上报备注
    private String memo;

    //连锁门店名称
    private String relationTerminalName;
    //连锁门店编码
    private String relationTerminalCode;

    //HT微信新会员
    private String htWxNewCustomer;
    //Dodie微信新会员
    private String dodieWxNewcustomer;
    //活动签到人数
    private String signNum;

    private String province; //省
    private String city; //市
    private String district;//区
    private String town; //街道

    //2019-11-08 add

    private String reportPostName;  //上报人岗位名称
    private String kbsNewCustomer; //可贝思新客

    private String signAllPoint; //全品项积分(签到内)
    private String milkSignNewCust; //奶粉新客(签到内)
    private String bstMilkSignNewCustomer; //活动期奶粉新客（合生元）(签到内)
    private String ysjSignNewCustomer; //活动期益生菌新客(签到内)
    private String kbsSignNewCustomer; //可贝思新客(签到内)
    private String htSignNewCust; //ht新客(签到内)
    private String dodieSignNewCustomerNum; //dodie新客(签到内)

    //2020-03-18 add
    private String pointDodie; //Dodie总积分
    private String signPointDodie; //签到内Dodie总积分

    //2020-06-18 add
    private String addiNutrition; //营养补充剂新客
    private String addiNutritionSignNewCustNum; //签到内营养补充剂新客

    private String reportByCompany; //上报人所属公司
  }


