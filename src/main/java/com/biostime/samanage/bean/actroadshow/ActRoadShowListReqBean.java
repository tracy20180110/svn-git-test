package com.biostime.samanage.bean.actroadshow;

import com.biostime.autodoc.annotations.AutoDocField;
import lombok.Data;

import java.io.Serializable;


/**
 * 路演活动备案
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2016-08-16
 */
@Data
public class ActRoadShowListReqBean implements Serializable {

    private static final long serialVersionUID = -2538252250811645014L;

    @AutoDocField(note="事业部编码")
    private String buCode  ;

    @AutoDocField(note="大区办事处")
    private String areaofficeCode;

    @AutoDocField(note="大区（ID）")
    private String areaCode;

    @AutoDocField(note="办事处（ID）")
    private String officeCode;

    @AutoDocField(note="门店渠道")
    private String channelCode;

    @AutoDocField(note="活动类型")
    private String configActTypeCode;

    @AutoDocField(note="活动状态")
    private String status;

    @AutoDocField(note="品牌")
    private String brandCode;

    @AutoDocField(note="活动名称")
    private String actName;

    @AutoDocField(note="开始日期")
    private String startDate;

    @AutoDocField(note="结束日期")
    private String endDate;

    @AutoDocField(note="登陆人名称")
    private String craetedName;

    @AutoDocField(note="活动创建人工号")
    private String createdBy;

    @AutoDocField(note="上报人工号")
    private String reportBy;

    @AutoDocField(note="活动ID")
    private String actId;

    @AutoDocField(note="门店编码")
    private String terminalCode;

    @AutoDocField(note="活动上报开始日期")
    private String reportStartDate;

    @AutoDocField(note="活动上报结束日期")
    private String reportEndDate;


    @AutoDocField(note="0：未审核，1：已审核")
    private String suffice;

    /**
     * 补录原因(1：网络原因未能及时上报、2：营销通系统原因未能及时上报、3：其他)
     */
    @AutoDocField(note = "MAKEUP_MEMO")
    private String	makeupMemo;

    @AutoDocField(note = "是否补录（0：否，1：是)")
    private String	actMakeup;

    @AutoDocField(note = "图片类别")
    private String imageMarkType;

    //连锁门店名称
    private String relationTerminalName;
    //连锁门店编码
    private String relationTerminalCode;

    //按促销员维度查询1:查询，空就是不查
    private String salesQuery;

    private int pageNo = 1;
    private int pageSize = 20;
}


