package com.biostime.samanage.bean.actroadshow;

import lombok.Data;

import java.io.Serializable;

/**
 * 路演活动列表
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRoadShowListResBean implements Serializable {

    private static final long serialVersionUID = -1429818671029565019L;
    private String id;//活动ID
    private String areaName;//大区名称
    private String officeName;//办事处名称
    private String channelCode; //渠道编码
    private String channelName; //渠道名称
    private String brandCode; //品牌名称
    private String brandName; //品牌编码
    private String actName;     //活动名称
    private String startDate;//开始时间
    private String endDate;   //结束时间
    private String createdBy;//创建人
    private String createdName;//创建人名称
    private String createdTime;//创建时间
    private String status;//活动状态（0：停用，1：启用）
    private String khTypeCode; //考核类型1:路演活动，2：物料陈列
    private String khTypeName; //考核类型1:路演活动，2：物料陈列
    private String configActTypeName; //活动配置类型名称
    private String configActTypeCode; //活动配置类型编码
    private String configTerminalTypeName; //门店类型名称
    private String configTerminalTypeCode; //门店类型编码
    private String swId; //参与SKU 如：1,2,3

    private String actReportType;//上报选相册（1：是，0否/空也是否）
    private String qrCodeName;//是否申请二维码品牌名称
    private String qrCode;//是否申请二维码品牌编码
    private String signMiniCode;//签到小程序
}


