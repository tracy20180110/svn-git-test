package com.biostime.samanage.bean.actroadshow;

import com.biostime.autodoc.annotations.AutoDocField;
import lombok.Data;

import java.io.Serializable;


/**
 * 路演考核报表
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2016-08-16
 */
@Data
public class ActRoadShowReportReqBean implements Serializable {


    private static final long serialVersionUID = -6459531621087386922L;

    @AutoDocField(note="事业部")
    private String buCode;

    @AutoDocField(note="大区办事处")
    private String areaofficeCode;

    @AutoDocField(note="活动渠道")
    private String channelCode;

    @AutoDocField(note="门店编号")
    private String terminalCode;

    @AutoDocField(note="执行开始日期")
    private String startDate;

    @AutoDocField(note="执行结束日期")
    private String endDate;

    @AutoDocField(note="活动ID")
    private String actId;

    @AutoDocField(note="活动名称")
    private String actName;

    @AutoDocField(note="活动状态")
    private String status;
    
    @AutoDocField(note="上报开始日期")
    private String reportingStartTime;
    
    @AutoDocField(note="上报结束日期")
    private String reportingEndTime;
    
    @AutoDocField(note="活动类型")
    private String type;

    @AutoDocField(note="品牌")
    private String brandCode;

    //连锁门店名称
    private String relationTerminalName;
    //连锁门店编码
    private String relationTerminalCode;
    
    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}


