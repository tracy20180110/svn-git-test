package com.biostime.samanage.bean.actroadshow;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Describe:路演活动配置--查询
 * Date: 2018-03-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRoadShowConfigReqBean extends BaseBean {

    private static final long serialVersionUID = 9064449302074853800L;


    @AutoDocField(note="活动状态1：启用，2：停用")
    private String status;

    @AutoDocField(note="活动类型")
    private String actTypeName;

    /*@AutoDocField(note="品牌")
    private String[] brand;

    @AutoDocField(note="活动类型")
    private String[] channel;

    @AutoDocField(note="活动类型")
    private String[] terminal;*/

    private Map<String,List<String>> actConfigMap;  //key:1 品牌, 2 渠道, 3 门店, 4 上报类型

    @AutoDocField(note="创建人工号")
    private String  createdBy;

    @AutoDocField(note="创建人名称")
    private String  createdByName;

    @AutoDocField(note="id")
    private String  id;

    @AutoDocField(note="更新人工号/名称")
    private String  updatedBy;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
