package com.biostime.samanage.bean.actroadshow;

import lombok.Data;

import java.io.Serializable;

/**
 * 路演活动明细
 * Created by dc on 2016/8/16.
 */
@Data
public class ActRoadShowDetailResBean implements Serializable {

    private static final long serialVersionUID = -6533106903555323975L;
    private String id;//ID
    private String actId;//活动ID
    private String areaName;//大区名称
    private String officeName;//办事处名称
    private String channelName; //渠道
    private String actName;     //活动名称
    private String actType;     //活动类型
    private String actStatus;//活动状态（0：停用，1：启用）
    private String actCreatedBy;//创建人
    private String actCreatedName;//创建人名称
    private String brand;       //品牌
    private String brandName;       //品牌名称
    private String startDate;//开始时间
    private String endDate;   //结束时间

    private String terminalCode;//门店编号
    private String terminalName;//门店名称

    private String createdBy;//上报人
    private String createdTime;//上报时间
    private String createdName; //上报人名称
    private String actMemo; //上报备注
    private String img1;//图片一
    private String img2;//图片二
    private String img3;//图片三
    private String img4;//图片四
    private String img5;//图片五
    private String img6;//图片六
    private String khTypeName; //考核类型1:路演活动，2：物料陈列

    private String suffice; //是否合格
    private String auditAccount; //审核人
    private String auditMemo;  //原因
    private String auditTime;  //审核时间

    private String isBbTerminal;  //是否报备门店
    private String bbStartDate;  //报备开始日期
    private String bbEndDate;  //报备结束日期

    private String areaTerminal;   //上报门店大区
    private String officeTerminal; //上报门店办事处

    private String	makeupMemo; //补录原因
    private String	actMakeup;  //是否补录（0：否，1：是）

    private String mark1;//图片标签一
    private String mark2;//图片标签二
    private String mark3;//图片标签三
    private String mark4;//图片标签四
    private String mark5;//图片标签五
    private String mark6;//图片标签六

    //2018-10-16 add
    private String postName; //上报人岗位

    //2019-1-23
    //总积分
    private String pointTot;
    //合生元总积分
    private String pointBiostime;
    //HT总积分
    private String pointMilkHtAsse;
    //Dodie总积分
    private String pointDodie;
    //奶粉
    private String newCustMilk;
    //奶粉-合生元
    private String newCustMilkBiostime;
    //HT奶粉
    private String newCustMilkHt;
    //益生菌
    private String newCustProbiotics;
    //swisse 门店POS销量
    private String swPosNums;

    //连锁门店名称
    private String relationTerminalName;
    //连锁门店编码
    private String relationTerminalCode;

    //Dodie新客
    private String dodieNewCustomerNum;

    private String province; //省
    private String city; //市
    private String district;//区
    private String town; //街道

    //2019-10-22
    //新客_奶粉400G礼包数
    private String newCustMilkGift4;


    //2020-02-12

    //上报人员品牌属性
    private String reportByBrand;
    //可贝思积分
    private String pointKbs;
    //可贝思新客
    private String newCustKbs8;

    //全品项积分
    private String pointTotAvg;
    //奶粉新客
    private String newCustMilkAvg;

    //HT积分
    private String pointHtAvg;
    //可贝思积分
    private String pointKbsAvg;
    //HT奶粉新客
    private String newCustMilkHtAvg;
    //可贝思奶粉新客
    private String newCustKbs8Avg;
    //Dodie积分
    private String pointDodieAvg;

    //2020-06-18 add
    private String addiNutrition; //营养补充剂新客
    private String addiNutritionAvg; //营养补充剂新客均分

    private String reportByCompany; //上报人所属公司

/**
 * 全品项积分\奶粉新客=合生元/奶创/dodie
 * HT积分\可贝思积分\HT奶粉新客\可贝思奶粉新客=奶创品牌
 * Dodie积分=Dodie品牌
 * 营养补充剂新客 = 合生元
 **/
}


