package com.biostime.samanage.bean.actroadshow;

import com.biostime.samanage.bean.BaseKeyValueBean;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 路演活动
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2016-08-16
 */
@Data
public class ActRoadShowSaveReqBean implements Serializable {

    private static final long serialVersionUID = -2152675583883185110L;
    private String id;
    private String areaOfficeCode; //大区办事处
    private String actName;  //名称
    private String startDate;   //活动开始时间
    private String endDate;     //活动结束时间
    private String userId;  //登录人ID
    private String userName; //登陆人名称
    private String khType;  //考核类型1:路演活动，2：物料陈列
    private String[] swisseId; //参与SKU
    private String actConfigId; //活动配置id

    private String[] brandCode; //品牌1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie
    private String channelCode; //渠道 01：婴线,02:商超,03:药线,13:美妆,08:SA
    private String channelName;
    private String terminalTypeCode; //门店类型编码
    private String terminalTypeName; //门店类型 1：区域重点连锁,2：其他,3:婴KA

    private String actReportType;   //上报选相册（1：是，0否/空也是否）

    private String signMiniCode;   //签到小程序(1:需要，0：不需要)

    private List<BaseKeyValueBean>  qrCodeList; //是否申请二维码
}


