package com.biostime.samanage.bean.customer;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.samanage.bean.common.PageBaseBean;
import lombok.Data;

/**
 * SA品项新客报表
 * Created by zhuhaitao on 2017/1/3.
 */
@Data
public class CustomerDetailBean extends PageBaseBean {
    @AutoDocField(note = "大区", nullable = true)
    private String areaCode;
    @AutoDocField(note = "大区", nullable = true)
    private String areaName;
    @AutoDocField(note = "办事处", nullable = true)
    private String officeCode;
    @AutoDocField(note = "办事处", nullable = true)
    private String officeName;
    @AutoDocField(note = "SA编号", nullable = true)
    private String saCode;
    @AutoDocField(note = "SA姓名", nullable = true)
    private String saName;
    @AutoDocField(note = "会员ID", nullable = true)
    private Long customerId;
    @AutoDocField(note = "会员姓名", nullable = true)
    private String name;
    @AutoDocField(note = "用户角色", nullable = true)
    private String customerRole;
    @AutoDocField(note = "手机号码", nullable = true)
    private String mobile;
    @AutoDocField(note = "系统来源", nullable = true)
    private String system;
    @AutoDocField(note = "所属BCC", nullable = true)
    private String proBcc;
    @AutoDocField(note = "实操BCC", nullable = true)
    private String operateBcc;
    @AutoDocField(note = "所属育婴顾问", nullable = true)
    private String nursingConsultant;
    @AutoDocField(note = "礼包/礼品", nullable = true)
    private String productName;
    @AutoDocField(note = "得悉途径 ", nullable = true)
    private String srcLocName;
    @AutoDocField(note = "名单类别 ", nullable = true)
    private String memberType;
    @AutoDocField(note = "开始时间 ", nullable = true)
    private String startTime;
    @AutoDocField(note = "结束时间 ", nullable = true)
    private String endTime;
    @AutoDocField(note = "积分时间 ", nullable = true)
    private String registerDate;
    @AutoDocField(note = "来源", nullable = true)
    private String recSrc;
    @AutoDocField(note = "是否订单积分", nullable = true)
    private String isO2OOrder;
    @AutoDocField(note = "是否妈妈100APP用户", nullable = true)
    private String isAppUser;
    @AutoDocField(note = "是否绑定微信 ", nullable = true)
    private String isWeixinBind;
    @AutoDocField(note = "品牌 ", nullable = true)
    private String brand;
    @AutoDocField(note = "品项 ", nullable = true)
    private String typeName;
    @AutoDocField(note = "奶粉阶段 ", nullable = true)
    private String stage;
    @AutoDocField(note = "购买门店 ", nullable = true)
    private String buyTerminal;

    @AutoDocField(note = "宝宝生日/预产期 ", nullable = true)
    private String birthdate;
    @AutoDocField(note = "宝宝生日/预产期(字符串) ", nullable = true)
    private String birthdateStr;
    @AutoDocField(note = "育儿指导师", nullable = true)
    private String parentingInstructor;
    @AutoDocField(note = "是否SAYN新客", nullable = true)
    private String isCoupon;

    @AutoDocField(note = "是否来自新客礼包", nullable = true)
    private String isNewCustGiftPkg;

    @AutoDocField(note = "事业部编码", nullable = true)
    private String buCode;

}
