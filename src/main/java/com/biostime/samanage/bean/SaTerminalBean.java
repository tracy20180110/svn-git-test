package com.biostime.samanage.bean;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * 类功能描述: SA终端信息表
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年6月23日 上午10:59:18
 */
@Data
public class SaTerminalBean extends BaseBean {
	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = -4630275472466949149L;
	@AutoDocField(note = "SA编号", nullable = true)
	private String code;
	@AutoDocField(note = "sa名称", nullable = true)
	private String name;
	@AutoDocField(note = "管理渠道编码", nullable = true)
	private String terminalChannelCode;
}