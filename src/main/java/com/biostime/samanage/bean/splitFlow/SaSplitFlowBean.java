package com.biostime.samanage.bean.splitFlow;

import com.biostime.samanage.bean.common.PageBaseBean;
import lombok.Data;

/**
 * Created by zhuhaitao on 2017/3/2.
 */
@Data
public class SaSplitFlowBean extends PageBaseBean {


    private String areaCode;//大区编码
    private String officeCode;//办事处编码
    private String terminalCode;//门店编号
    private String customerId;//会员ID
    private String mobilePhone;//会员手机
    private String saCode;//SA编号
    private String nursingConsultant;//SA育婴助理工号
    private String startTime;//发券日期查询起始条件
    private String endTime;// 发券日期查询截止条件


    private String areaName;//大区
    private String officeName;//办事处
    private String createTime;//发券日期
    private String relateCode;//促销员工号
    private String status;//是否到店
    private String useTerminalCode;//实际到店
    private String useTime;//到店时间(即使用券的时间)
    private String motherMilkClient;//妈妈奶粉新客
    private String babyMilkClient;//婴儿奶粉新客（含HT）
    private String probioticsClient;//益生菌新客
    private String dodieClient;//Dodie新客
    private String htMilkClient;//HT奶粉新客
    private String productPoints;//总积分(妈妈奶粉,婴儿奶粉,HT奶粉,益生菌)
    private String isMatch;//校对用券门店和发券终端是否符合“SA与门店”的关系

    /**
     * 连锁-妈妈奶粉新客
     */
    private String chainMothermilkclient;
    /**
     * 连锁-婴儿奶粉新客
     */
    private String chainBabymilkclient;
    /**
     * 连锁-益生菌新客
     */
    private String chainProbioticsclient;
    /**
     * 连锁-Dodie新客
     */
    private String chainDodieclient;
    /**
     * 连锁编码
     */
    private String chainCode;
    /**
     * 领取礼品门店
     */
    private String presentTerminal;
    /**
     * 会员进店
     */
    private String useTerminal;
    
    // 1店所属渠道编码
    private String terminalChannelCode;
    
    // 1店所属渠道名称
    private String terminalChannelName;
    
    // 1店所属体系
    private String terminalTopChainName;
    
    // 连锁店所属渠道编码
    private String chainChannelCode;

    // 连锁店所属渠道名称
    private String chainChannelName;    
    
    // 连锁店所属体系
    private String chainTopChainName;
}
