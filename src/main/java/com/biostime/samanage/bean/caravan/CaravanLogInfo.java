package com.biostime.samanage.bean.caravan;

import lombok.Data;

import java.io.Serializable;

/**
 * Describe:大小篷车物料信息
 * Date: 2018-04-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanLogInfo implements Serializable{

    private static final long serialVersionUID = 1317168089706894750L;
    /***操作记录（1：新增，2：更换，3：暂停，4：恢复，5：更改申请人） */
    private String typeName;

    /***创建时间 */
    private String createdTime;

    /***创建人名称 */
    private String createdName;

    /***创建人工号 */
    private String createdBy;


}