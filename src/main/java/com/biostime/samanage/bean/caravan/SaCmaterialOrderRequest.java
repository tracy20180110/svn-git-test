package com.biostime.samanage.bean.caravan;/**
 * Created by W1016 on 2017/12/11.
 */


import java.io.Serializable;

/**
 * 方法描述: 大小篷车物料下单接口
 *
 * @param
 * @author w1016
 * @throws
 * @createDate 2017-12-11-9:52
 */
public class SaCmaterialOrderRequest implements Serializable {

    private static final long serialVersionUID = 4637552187729648004L;
    private String sign ;
    private String platform ;
    private CmaterialOrderInfo orderInfo;
    private CmaterialOrderAddr orderAddr;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public CmaterialOrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(CmaterialOrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    public CmaterialOrderAddr getOrderAddr() {
        return orderAddr;
    }

    public void setOrderAddr(CmaterialOrderAddr orderAddr) {
        this.orderAddr = orderAddr;
    }

    @Override
    public String toString() {
        return "SaCmaterialOrderRequest{" +
                "sign='" + sign + '\'' +
                ", platform='" + platform + '\'' +
                ", orderInfo=" + orderInfo +
                ", orderAddr=" + orderAddr +
                '}';
    }
}
