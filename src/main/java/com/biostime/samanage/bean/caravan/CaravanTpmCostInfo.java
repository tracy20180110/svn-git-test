package com.biostime.samanage.bean.caravan;

import lombok.Data;

import java.io.Serializable;

/**
 * Describe:大小篷车物料信息
 * Date: 2018-04-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanTpmCostInfo implements Serializable{

    private static final long serialVersionUID = 1317168089706894750L;

    /***数量 */
    private String nums;

    /***物料单价 */
    private String price;

    /***总金额 */
    private String amount;

    /***登陆人工号 */
    private String userAccount;

    /***描述 */
    private String activityBrief;

    /***活动编码 */
    private String activityCode;

    /***费用开始时间 */
    private String actStartDate;

    /***费用结束时间 */
    private String actEndDate;

    /***费用池ID***/
    private String id;

    /***可用金额***/
    private String amountAvailable;


}