package com.biostime.samanage.bean.caravan;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.List;

/**
 * Describe:大小篷车申请信息
 * Date: 2016-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanReqBean extends BaseBean {
    private static final long serialVersionUID = -5304792829346665019L;

    @AutoDocField(note="大小篷车ID")
    private String id;

    /*** 申请类型（1：小篷车：2：大篷车）*/
    @AutoDocField(note="申请类型")
    private Long caravanType;

    @AutoDocField(note="门店编码")
    private String terminalCode;

    /**申请状态（1：新增，2：更换，3：暂停，4：恢复）*/
    @AutoDocField(note="申请状态")
    private Long status;

    /** 发货方式（1：当地制作：2：总部发货） */
    @AutoDocField(note="发货方式")
    private Long deliveryMode ;

    @AutoDocField(note="摆放地点")
    private String putAddress;

    @AutoDocField(note="摆放尺寸")
    private String putSize = "";

    /** 物料类型（1：儿科：2：产科） */
    @AutoDocField(note="物料类型")
    private Long cmaterialType;

    /** 费用类型（1：总部承担：2：办事处/大区承担） */
    @AutoDocField(note="费用类型")
    private Long costType;

    @AutoDocField(note="微信会员量")
    private Long wxCustomer;

    @AutoDocField(note="新客")
    private Long newCustomer;

    @AutoDocField(note="有效新客")
    private Long effectiveNewCustomer;

    @AutoDocField(note="项目合作")
    private String projectCooperation;

    @AutoDocField(note="省")
    private String province = "";

    @AutoDocField(note="市")
    private String city = "";

    @AutoDocField(note="区/县")
    private String district = "";

    /** 详细地址,一般不包括省市区信息 */
    @AutoDocField(note="详细地址")
    private String address = "";

    @AutoDocField(note="收货人")
    private String consignee = "";

    @AutoDocField(note="联系电话")
    private String contactPhone = "";

    @AutoDocField(note="备注")
    private String memo  = "";

    /**申请物料的流程状态（0:审批不通过，1:已提交，2：审批通过，3：提交OA流程失败，4：预算不足，5:提交订单失败，6：待发货，7：已发货，8：已完结，9：验收不通过,10：已撤销）*/
    @AutoDocField(note="申请物料的流程状态")
    private Long procedureStatus;

    @AutoDocField(note="创建者")
    private String createdBy;

    @AutoDocField(note="门店名称")
    private String terminalName = "";

    @AutoDocField(note="创建者名称")
    private String createdName  = "";

    @AutoDocField(note="更新人")
    private String updatedBy = "";

    @AutoDocField(note="OA流程编号")
    private String oaCode  = "";

    @AutoDocField(note="物料信息")
    private List<CaravanCmaterialInfo> caravanCmaterialInfoList;

    @AutoDocField(note="物料总价格")
    private String payPrice  = "";

    @AutoDocField(note="上传协议")
    private String fileUpload;

    @AutoDocField(note="文件名称")
    private String fileName;

    private String areaOfficeCode = "";
}
