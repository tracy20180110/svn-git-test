package com.biostime.samanage.bean.caravan;

import lombok.Data;

import java.io.Serializable;

/**
 * Describe:大小篷车物料信息
 * Date: 2016-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanCmaterialInfo implements Serializable{
    private static final long serialVersionUID = -7699024215744920489L;

    /***分销物料id */
    private String cmaterialId;

    /*** 物料名称 */
    private String cmaterialName;

    /*** 物料编码 */
    private String cmaterialCode;

    /*** 单价 */
    private String price;

    /*** 单位 */
    private String unit;

    /*** NC转换比例 */
    private String ncChange;

    /*** 图片（缩略） */
    private String imgMin;

    /*** 图片（原图） */
    private String imgBig;

    /*** 到货时间 */
    private String arrivalGoodsSime;

    /** 创建者 */
    private String createdBy;

    /** 创建时间 */
    private String createdTime;

    /** 物料类型1：儿科,2:产科 */
    private String cmaterialType;

    /** 大小篷车ID */
    private String caravanId;

    /** 数量 */
    private String num;

    /** nc转换后的数量 */
    private String ncchangeNum;

    /** 下单时的装箱规格 */
    private String boxNorms;

    /**下单方式*/
    private String orderType;

    /*** 物料费用单 */
    private String costId;

    /*** 物料费用单名称 */
    private String costName;

}