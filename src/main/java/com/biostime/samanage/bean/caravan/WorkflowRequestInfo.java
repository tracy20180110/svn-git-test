package com.biostime.samanage.bean.caravan;

import lombok.Data;

import java.io.Serializable;

/**
 * Describe:大小篷车物料信息
 * Date: 2016-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class WorkflowRequestInfo implements Serializable {

    /***申请单号 */
    public String applyId;

    /*** 申请日期 */
    public String applyDate;

    /*** 申请人 */
    public String applyUser;

    /*** 所属大区 */
    public String subcompany;

    /*** 所属办事处 */
    public String department;

    /*** 申请类型 */
    public String caravanType;

    public String workflowid;

    //OA提交独有的属性
    public String requestLevel;
    public String requestName;
    public String creatorId;
    public String createType;

}