package com.biostime.samanage.bean.caravan;

import lombok.Data;

import java.io.Serializable;

/**
 * Describe:大小篷车物料信息
 * Date: 2018-04-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanTpmQueryBean implements Serializable{


    private static final long serialVersionUID = -6778103248871355397L;

    private String activityCode;
    private String amount;
    private String userAccount;
    private String activityBrief;
}