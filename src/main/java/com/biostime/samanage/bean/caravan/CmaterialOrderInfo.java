package com.biostime.samanage.bean.caravan;

import com.biostime.common.bean.BaseBean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 
 * 类功能描述：经销商用户实体类
 *
 * <p> 版权所有：BIOSTIME.com
 * <p> 未经本公司许可，不得以任何方式复制或使用本程序任何部分 <p>
 * 
 * @author <a href="mailto:Administrator@biostime.com">Administrator</a>
 * @version 1.0
 * @since 2013-9-3 
 *
 */
public class CmaterialOrderInfo  extends BaseBean {

	private static final long serialVersionUID = 5850795384399444173L;
	private long id;
	private String orderNo;
	private String type;
	private String status;
	private String receiveMan;
	private String receiveType;
	private BigDecimal payPrice;
	private String receivePhone;
	private String receiveAddress;
	private String budgetUnit;
	private String budgetSubject;
	private String budgetSubjectId;
	private String remark;
	private Date createTime;
	private String createBy;
	private Date updateTime;
	private String updateBy;
	private String createById;
	private String departmentCode;
	private String channel;
	private String projectpk;
	private Long addrId;
	private String projectPurpose;
	private List<CmaterialOrderItemInfo> cmaterialOrderItemInfoList;

	//费用类型：1-NC预算；2-TPM费用
	private Integer costType;

	//预算单位
	private String orgPk;
	//预算部门
	private String deptPk;
	//预算科目
	private String subjectPk;

	public Integer getCostType() {
		return costType;
	}

	public void setCostType(Integer costType) {
		this.costType = costType;
	}
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReceiveMan() {
		return receiveMan;
	}

	public void setReceiveMan(String receiveMan) {
		this.receiveMan = receiveMan;
	}

	public String getReceiveType() {
		return receiveType;
	}

	public void setReceiveType(String receiveType) {
		this.receiveType = receiveType;
	}

	public String getReceivePhone() {
		return receivePhone;
	}

	public void setReceivePhone(String receivePhone) {
		this.receivePhone = receivePhone;
	}

	public String getReceiveAddress() {
		return receiveAddress;
	}

	public void setReceiveAddress(String receiveAddress) {
		this.receiveAddress = receiveAddress;
	}

	public String getBudgetUnit() {
		return budgetUnit;
	}

	public void setBudgetUnit(String budgetUnit) {
		this.budgetUnit = budgetUnit;
	}

	public String getBudgetSubject() {
		return budgetSubject;
	}

	public void setBudgetSubject(String budgetSubject) {
		this.budgetSubject = budgetSubject;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public BigDecimal getPayPrice() {
		return payPrice;
	}

	public void setPayPrice(BigDecimal payPrice) {
		this.payPrice = payPrice;
	}

	public List<CmaterialOrderItemInfo> getCmaterialOrderItemInfoList() {
		return cmaterialOrderItemInfoList;
	}

	public void setCmaterialOrderItemInfoList(
			List<CmaterialOrderItemInfo> cmaterialOrderItemInfoList) {
		this.cmaterialOrderItemInfoList = cmaterialOrderItemInfoList;
	}

	public Long getAddrId() {
		return addrId;
	}

	public void setAddrId(Long addrId) {
		this.addrId = addrId;
	}

	public String getCreateById() {
		return createById;
	}

	public void setCreateById(String createById) {
		this.createById = createById;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	

	public String getBudgetSubjectId() {
		return budgetSubjectId;
	}

	public void setBudgetSubjectId(String budgetSubjectId) {
		this.budgetSubjectId = budgetSubjectId;
	}
	

	public String getProjectpk() {
		return projectpk;
	}

	public void setProjectpk(String projectpk) {
		this.projectpk = projectpk;
	}

	public String getProjectPurpose() {
		return projectPurpose;
	}

	public void setProjectPurpose(String projectPurpose) {
		this.projectPurpose = projectPurpose;
	}

	public String getOrgPk() {
		return orgPk;
	}

	public void setOrgPk(String orgPk) {
		this.orgPk = orgPk;
	}

	public String getDeptPk() {
		return deptPk;
	}

	public void setDeptPk(String deptPk) {
		this.deptPk = deptPk;
	}

	public String getSubjectPk() {
		return subjectPk;
	}

	public void setSubjectPk(String subjectPk) {
		this.subjectPk = subjectPk;
	}

	@Override
	public String toString() {
		return "CmaterialOrderInfo [id=" + id + ", orderNo=" + orderNo
				+ ", type=" + type + ", status=" + status + ", receiveMan="
				+ receiveMan + ", receiveType=" + receiveType + ", payPrice="
				+ payPrice + ", receivePhone=" + receivePhone
				+ ", receiveAddress=" + receiveAddress + ", budgetUnit="
				+ budgetUnit + ", budgetSubject=" + budgetSubject
				+ ", budgetSubjectId=" + budgetSubjectId + ", remark=" + remark
				+ ", createTime=" + createTime + ", createBy=" + createBy
				+ ", updateTime=" + updateTime + ", updateBy=" + updateBy
				+ ", createById=" + createById + ", departmentCode="
				+ departmentCode + ", channel=" + channel + ", projectpk="
				+ projectpk + ", addrId=" + addrId + ", projectPurpose=" + projectPurpose
				+ ", orgPk=" + orgPk + ", deptPk=" + deptPk + ", subjectPk=" + subjectPk
				+ ", cmaterialOrderItemInfoList=" + cmaterialOrderItemInfoList
				+ ", costType=" + costType
				+ "]";
	}

}

