package com.biostime.samanage.bean.caravan;


import com.biostime.common.bean.BaseBean;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * 物料仓库实体类
 * @author 7913
 *
 */
@Data
public class CmaterialOrderAddr  extends BaseBean {


	private Long id;
	
	@Column(name = "ORDER_ID")
	private String orderId;
	
	
	@Column(name = "MOBILE")
	private String mobile;
	
	@Column(name = "PROVINCE")
	private String province;
	
	@Column(name = "CITY")
	private String city;
	
	@Column(name = "TOWN")
	private String town;
	
	@Column(name = "DISTRICT")
	private String district;
	
	@Column(name = "POSTAL_CODE")
	private String postalCode;
	
	@Column(name = "ADDR")
	private String addr;
	
	@Column(name = "STATUS")
	private Integer status;
	
	@Column(name = "CREATE_BY")
	private String createBy;
	
	@Column(name = "CREATE_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}


	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

		

	
}

