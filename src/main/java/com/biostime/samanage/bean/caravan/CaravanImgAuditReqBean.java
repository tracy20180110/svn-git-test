package com.biostime.samanage.bean.caravan;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:大小篷车竣工图片及审核
 * Date: 2016-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanImgAuditReqBean extends BaseBean {
    private static final long serialVersionUID = -5304792829346665019L;

    @AutoDocField(note="ID")
    private Long imgAuditId;

    @AutoDocField(note="大小篷车ID")
    private Long caravanId;

    @AutoDocField(note="审核状态（1：通过，2：不通过）")
    private Long auditStatus;

    @AutoDocField(note="图片一")
    private String imgUrlOne;

    @AutoDocField(note="图片一")
    private String imgUrlTwo;

    @AutoDocField(note="图片三")
    private String imgUrlThree;

    @AutoDocField(note="审批备注")
    private String memo;

    @AutoDocField(note="创建者")
    private String createdBy;

    @AutoDocField(note="创建者名称")
    private String createdName;

}
