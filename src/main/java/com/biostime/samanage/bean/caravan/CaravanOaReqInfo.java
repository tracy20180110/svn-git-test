package com.biostime.samanage.bean.caravan;

import lombok.Data;

/**
 * Describe:大小篷车OA流程
 * Date: 2016-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanOaReqInfo {
    /***申请单号 */
    public String applyId;

    /*** 申请日期 */
    public String applyDate;

    /*** 申请人 */
    public String applyUser;

    /*** 所属大区 */
    public String subcompany;

    /*** 所属办事处 */
    public String department;

    /*** 申请类型 */
    public String caravanType;

    public String workflowid;

    //编号+名称
    public String terminalCode;

    //状态
    public String status;

    //发货方式
    public String deliveryMode;

    //摆放地点
    public String putAddress;

    //摆放尺寸
    public String putSize;

    //物料类型
    public String cmaterialType;

    //微信会员量
    public String wxCustomer;

    //新客
    public String newCustomer;

    //有效新客
    public String effectiveNewCustomer;

    //项目合作
    public String projectCooperation;

    //费用类型
    public String costType;

    //收货人
    public String consignee;

    //联系电话
    public String contactPhone;

    //收货地址 省+市+详细地址
    public String address;

    //备注
    public String memo;

    //上传协议
    public String fileUpload;

    //OA提交独有的属性
    public String requestLevel;
    public String requestName;
    public String creatorId;
    public String createType;
}
