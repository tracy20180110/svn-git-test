package com.biostime.samanage.bean.caravan;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:大小篷车申请信息
 * Date: 2016-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanListResBean extends BaseBean {


    private static final long serialVersionUID = -4939657452771221849L;

    @AutoDocField(note="大小篷车ID")
    private String id = "";

    @AutoDocField(note="图片审核ID")
    private String imgAuditId = "";

    @AutoDocField(note="大区名称")
    private String areaName = "";

    @AutoDocField(note="办事处名称")
    private String officeName = "";

    @AutoDocField(note="SA属性")
    private String saGrad = "";

    @AutoDocField(note="SA编码")
    private String saCode = "";

    @AutoDocField(note="SA名称")
    private String saName = "";

    /*** 申请类型（1：小篷车：2：大篷车）*/
    @AutoDocField(note="申请类型编码")
    private String caravanType = "";

    /*** 申请类型（1：小篷车：2：大篷车）*/
    @AutoDocField(note="申请类型编码名称")
    private String caravanTypeName  = "";

    @AutoDocField(note="创建人名称")
    private String createdName  = "";

    @AutoDocField(note="创建时间")
    private String createdTime = "";

    @AutoDocField(note="创建人ID")
    private String createdBy  = "";

    @AutoDocField(note="OA流程编号")
    private String oaCode = "";

    /**申请状态（1：新增，2：更换，3：暂停，4：恢复）*/
    @AutoDocField(note="状态")
    private String status = "";

    @AutoDocField(note="图片一")
    private String imgUrlOne = "";

    @AutoDocField(note="图片一")
    private String imgUrlTwo = "";

    @AutoDocField(note="图片三")
    private String imgUrlThree = "";

    @AutoDocField(note="图片申请状态")
    private String auditStatus = "";

    /**申请物料的流程状态（0:审批不通过，1:已提交，2：审批通过，3：提交OA流程失败，4：预算不足，5:提交订单失败，6：待发货，7：已发货，8：已完结，9：验收不通过,10：已撤销）*/
    @AutoDocField(note="申请物料的流程状态")
    private String procedureStatusName = "";

/**申请物料的流程状态（0:审批不通过，1:已提交，2：审批通过，3：提交OA流程失败，4：预算不足，5:提交订单失败，6：待发货，7：已发货，8：已完结，9：验收不通过,10：已撤销，13 暂停,14 撤销）*/
    @AutoDocField(note="申请物料的流程状态")
    private String procedureStatus = "";

    @AutoDocField(note="物料类型（1：儿科：2：产科）编码")
    private String cmaterialType;

    @AutoDocField(note="物料类型（1：儿科：2：产科）名称")
    private String cmaterialTypeName;

    @AutoDocField(note="发货方式（1：当地制作：2：总部发货）编码")
    private String deliveryMode;

    @AutoDocField(note="发货方式（1：当地制作：2：总部发货）名称")
    private String deliveryModeName;

    @AutoDocField(note="微信会员数量")
    private String wxCustomer;

    @AutoDocField(note="新客")
    private String newCustomer;

    @AutoDocField(note="有效新客")
    private String effectiveNewCustomer;
}
