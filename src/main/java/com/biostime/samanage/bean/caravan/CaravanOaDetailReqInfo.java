package com.biostime.samanage.bean.caravan;

import lombok.Data;

/**
 * Describe:大小篷车OA流程
 * Date: 2016-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanOaDetailReqInfo {

    /*** 物料名称 */
    public String cmaterialName;

    /*** 物料编码 */
    public String cmaterialCode;

    /*** 单价 */
    public String price;

    /** 数量 */
    public String num;

}
