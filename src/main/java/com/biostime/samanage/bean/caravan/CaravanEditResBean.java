package com.biostime.samanage.bean.caravan;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import com.biostime.samanage.domain.caravan.Caravan;
import lombok.Data;

import java.util.List;

/**
 * Describe:大小篷车申请信息
 * Date: 2016-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanEditResBean extends BaseBean {
    @AutoDocField(note="大小篷车信息")
    private Caravan caravan;


    @AutoDocField(note="物料信息")
    private List<CaravanCmaterialInfo> caravanCmaterialInfoList;
}
