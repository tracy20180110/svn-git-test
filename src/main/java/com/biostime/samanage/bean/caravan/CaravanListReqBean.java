package com.biostime.samanage.bean.caravan;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:大小篷车申请信息
 * Date: 2016-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class CaravanListReqBean extends BaseBean {


    private static final long serialVersionUID = -5798902110066459445L;


    @AutoDocField(note="大区、办事处")
    private String areaOfficeCode;

    @AutoDocField(note="SA编码")
    private String saCode;

    @AutoDocField(note="申请开始日期")
    private String startDate;

    @AutoDocField(note="申请结束日期")
    private String endDate;

    @AutoDocField(note="类型")
    private String caravanType;

    @AutoDocField(note="事业部编码")
    private String buCode;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
