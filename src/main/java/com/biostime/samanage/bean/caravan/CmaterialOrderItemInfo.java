package com.biostime.samanage.bean.caravan;

import com.biostime.common.bean.BaseBean;

import java.util.Date;

/**
 * 
 * 类功能描述：订单详细表实体类
 *
 * <p> 版权所有：BIOSTIME.com
 * <p> 未经本公司许可，不得以任何方式复制或使用本程序任何部分 <p>
 * 
 * @author <a href="mailto:Administrator@biostime.com">Administrator</a>
 * @version 1.0
 *
 *
 */
public class CmaterialOrderItemInfo  extends BaseBean {


	private static final long serialVersionUID = 3234685645886906294L;
	private long id;
	private String orderNo;
	private String cmaterialId;
	private String cmaterialName;
	private String cmaterialPicture;
	private String price;
	private String gNum;
	private String senStatus;
	private String sendNo;
	private String sendNum;
	private String sendMsg;
	private Date updateTime;
	private String updateBy;
	private String cmaterialUnit;
	private String cmaterialNcNum;
	private String feeDeductNo;

	private Long CartId;

	private String ncChange;
	private String arrivalGoodsTime;
	private String boxNorms;
	private String orderType;

	//物料明细
	private String costId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getCmaterialId() {
		return cmaterialId;
	}

	public void setCmaterialId(String cmaterialId) {
		this.cmaterialId = cmaterialId;
	}

	public String getCmaterialName() {
		return cmaterialName;
	}

	public void setCmaterialName(String cmaterialName) {
		this.cmaterialName = cmaterialName;
	}

	public String getCmaterialPicture() {
		return cmaterialPicture;
	}

	public void setCmaterialPicture(String cmaterialPicture) {
		this.cmaterialPicture = cmaterialPicture;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getgNum() {
		return gNum;
	}

	public void setgNum(String gNum) {
		this.gNum = gNum;
	}

	public String getSenStatus() {
		return senStatus;
	}

	public void setSenStatus(String senStatus) {
		this.senStatus = senStatus;
	}

	public String getSendNo() {
		return sendNo;
	}

	public void setSendNo(String sendNo) {
		this.sendNo = sendNo;
	}

	public String getSendNum() {
		return sendNum;
	}

	public void setSendNum(String sendNum) {
		this.sendNum = sendNum;
	}

	public String getSendMsg() {
		return sendMsg;
	}

	public void setSendMsg(String sendMsg) {
		this.sendMsg = sendMsg;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getCartId() {
		return CartId;
	}

	public void setCartId(Long cartId) {
		CartId = cartId;
	}

	public String getCmaterialUnit() {
		return cmaterialUnit;
	}

	public void setCmaterialUnit(String cmaterialUnit) {
		this.cmaterialUnit = cmaterialUnit;
	}

	public String getCmaterialNcNum() {
		return cmaterialNcNum;
	}

	public void setCmaterialNcNum(String cmaterialNcNum) {
		this.cmaterialNcNum = cmaterialNcNum;
	}

	public String getFeeDeductNo() {
		return feeDeductNo;
	}

	public void setFeeDeductNo(String feeDeductNo) {
		this.feeDeductNo = feeDeductNo;
	}

	public String getNcChange() {
		return ncChange;
	}

	public void setNcChange(String ncChange) {
		this.ncChange = ncChange;
	}

	public String getArrivalGoodsTime() {
		return arrivalGoodsTime;
	}

	public void setArrivalGoodsTime(String arrivalGoodsTime) {
		this.arrivalGoodsTime = arrivalGoodsTime;
	}

	public String getBoxNorms() {
		return boxNorms;
	}

	public void setBoxNorms(String boxNorms) {
		this.boxNorms = boxNorms;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getCostId() {
		return costId;
	}

	public void setCostId(String costId) {
		this.costId = costId;
	}
	@Override
	public String toString() {
		return "CmaterialOrderItemInfo{" +
				"id=" + id +
				", orderNo='" + orderNo + '\'' +
				", cmaterialId='" + cmaterialId + '\'' +
				", cmaterialName='" + cmaterialName + '\'' +
				", cmaterialPicture='" + cmaterialPicture + '\'' +
				", price='" + price + '\'' +
				", gNum='" + gNum + '\'' +
				", senStatus='" + senStatus + '\'' +
				", sendNo='" + sendNo + '\'' +
				", sendNum='" + sendNum + '\'' +
				", sendMsg='" + sendMsg + '\'' +
				", updateTime=" + updateTime +
				", updateBy='" + updateBy + '\'' +
				", cmaterialUnit='" + cmaterialUnit + '\'' +
				", cmaterialNcNum='" + cmaterialNcNum + '\'' +
				", feeDeductNo='" + feeDeductNo + '\'' +
				", cartId=" + CartId +
				", ncChange=" + ncChange +
				", arrivalGoodsTime=" + arrivalGoodsTime +
				", boxNorms='" + boxNorms + '\'' +
				", orderType='" + orderType + '\'' +
				", costId='" + costId + '\'' +
				'}';
	}
}

