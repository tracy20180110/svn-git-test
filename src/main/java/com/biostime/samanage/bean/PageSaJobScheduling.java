package com.biostime.samanage.bean;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * 类功能描述: sa工作排班分页
 * 
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016-8-16 下午1:41:32
 */
@Data
public class PageSaJobScheduling extends BaseBean {
	private static final long serialVersionUID = 4974044828670735542L;
	@AutoDocField(note = "分页参数", nullable = true)
	private int pageNo = 1;
	@AutoDocField(note = "默认每页20条", nullable = true)
	private int pageSize = 10;
	@AutoDocField(note = "sa工作排班", nullable = true)
	private SaJobSchedulingBean saJobSchedulingBean;
}