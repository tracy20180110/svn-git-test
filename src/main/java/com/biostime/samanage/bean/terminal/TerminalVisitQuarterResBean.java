package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:门店拜访
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class TerminalVisitQuarterResBean extends BaseBean {


    private static final long serialVersionUID = 3120153244764343856L;
    @AutoDocField(note="考核季度")
    private String quarter;

    @AutoDocField(note="姓名")
    private String name;

    @AutoDocField(note="工号")
    private String account;

    @AutoDocField(note="岗位名称")
    private String postName;

    @AutoDocField(note="1/无星级季度达标率")
    private String oneStarlevelRate;

    @AutoDocField(note="2星级季度达标率")
    private String twoStarlevelRate;

    @AutoDocField(note="3星级季度达标率")
    private String threeStarlevelRate;

    @AutoDocField(note="4星级季度达标率")
    private String fourStarlevelRate;

    @AutoDocField(note="5星级季度达标率")
    private String fiveStarlevelRate;

    @AutoDocField(note="季度达标率")
    private String quarterAchievementRate;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
