package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:门店拜访
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class TerminalVisitResBean extends BaseBean {


    private static final long serialVersionUID = 1760655684911599879L;

    @AutoDocField(note="考核月")
    private String auditMonth;

    @AutoDocField(note="姓名")
    private String name;

    @AutoDocField(note="工号")
    private String account;

    @AutoDocField(note="岗位名称")
    private String postName;

    @AutoDocField(note="星级")
    private String starlevelName;

    @AutoDocField(note="门店拜访量")
    private String terminalVisitNum;

    @AutoDocField(note="门店拜访达标量")
    private String terminalVisitAchievementNum;

    @AutoDocField(note="达标率")
    private String achievementRate;

    @AutoDocField(note="星级Id")
    private String starlevelId;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="本月门店关联数量")
    private String relationTerminalNum;


    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
