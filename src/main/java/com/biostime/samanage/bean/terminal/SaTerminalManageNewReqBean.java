package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA与门店管理ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaTerminalManageNewReqBean extends BaseBean {

    private static final long serialVersionUID = -8241941277439862800L;

    @AutoDocField(note="id")
    private String id;

    @AutoDocField(note="大区办事处")
    private String areaOfficeCode;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="门店编码")
    private String terminalCode;

    @AutoDocField(note="有效关系1未生效、2:有效、3失效")
    private String statusCode;

    @AutoDocField(note="登陆人账号")
    private String createdBy;

    @AutoDocField(note="登陆人账号")
    private String createdName;

    @AutoDocField(note="开始时间")
    private String startDate;

    @AutoDocField(note="结束时间")
    private String endDate;

    @AutoDocField(note="事业部编码")
    private String buCode;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
