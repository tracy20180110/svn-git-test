package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:门店拜访查询 手机
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class TerminalVisitMobReqBean extends BaseBean {


    private static final long serialVersionUID = 9211399831347720992L;

    @AutoDocField(note="工号")
    private String account;

    @AutoDocField(note="考核月份yyyymm")
    private String month;

}
