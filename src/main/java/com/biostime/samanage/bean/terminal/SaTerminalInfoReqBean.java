package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA门店信息查询ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaTerminalInfoReqBean extends BaseBean {

    private static final long serialVersionUID = 7769287686339831779L;

    @AutoDocField(note="大区办事处")
    private String areaOfficeCode;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="SA状态")
    private String status;

    @AutoDocField(note="创建开始日期")
    private String startDate;

    @AutoDocField(note="创建开始日期")
    private String endDate;

    @AutoDocField(note="事业部")
    private String buCode;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
