package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA与门店关系查询
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaTerminalManageQueryInfo extends BaseBean {

    private static final long serialVersionUID = -7849416541560286748L;

    @AutoDocField(note="开始时间")
    private String startDate;

    @AutoDocField(note="结束时间")
    private String endDate;

}
