package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:门店拜访
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class TerminalVisitMobResBean extends BaseBean {

    private static final long serialVersionUID = -2595115784441850047L;

    @AutoDocField(note = "门店编码")
    private String terminalCode;

    @AutoDocField(note = "星级名称")
    private String starlevelname;

    @AutoDocField(note = "上报数量")
    private String reportNum;

}