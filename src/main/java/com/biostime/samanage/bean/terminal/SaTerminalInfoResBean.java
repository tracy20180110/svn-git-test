package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA门店信息查询ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaTerminalInfoResBean extends BaseBean {

    private static final long serialVersionUID = 7769287686339831779L;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="SA名称")
    private String saName;

    @AutoDocField(note="状态")
    private String status;

    @AutoDocField(note="门店二维码地址")
    private String saTwocodePath;

    @AutoDocField(note="门店编号")
    private String terminalCode;

    @AutoDocField(note="创建时间")
    private String createdTime;

    @AutoDocField(note="等级")
    private String grade;

    @AutoDocField(note="SA地址")
    private String saAddress;

    @AutoDocField(note="产科医护人员总数")
    private String ckAllCount;

    @AutoDocField(note="产科门诊量（月）")
    private String ckMzCount;

    @AutoDocField(note="VIP产科床位")
    private String vipCkCw;

    @AutoDocField(note="VIP入住率")
    private String vipRzl;

    @AutoDocField(note="VIP有效床位")
    private String vipYxCw;

    @AutoDocField(note="普通产科床位")
    private String ptCkCw;

    @AutoDocField(note="普通入住率")
    private String ptRzl;

    @AutoDocField(note="普通有效床位")
    private String ptYxCw;

    @AutoDocField(note="儿科医护人员总数")
    private String ekAllCount;

    @AutoDocField(note="儿科门诊量（月）")
    private String ekMzCount;

    @AutoDocField(note="新生儿床位")
    private String xseCw;

    @AutoDocField(note="年分娩量")
    private String nfmCount;

}
