package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA与门店管理ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaTerminalManageImportResBean extends BaseBean {


    private static final long serialVersionUID = -1705162333250288062L;

    @AutoDocField(note="上传文件路径")
    private String filePath;

    @AutoDocField(note="上传文件名称")
    private String fileName;

    @AutoDocField(note="总记录数")
    private String totalCount;

    @AutoDocField(note="创建人工号")
    private String createdBy;

    @AutoDocField(note="创建人工号")
    private String createdName;

    @AutoDocField(note="创建时间")
    private String createdTime;
}
