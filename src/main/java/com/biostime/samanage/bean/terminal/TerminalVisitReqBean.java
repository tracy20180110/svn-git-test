package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:门店拜访
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class TerminalVisitReqBean extends BaseBean {


    private static final long serialVersionUID = 1760655684911599879L;
    @AutoDocField(note="姓名")
    private String name;

    @AutoDocField(note="工号")
    private String account;

    @AutoDocField(note="岗位编码")
    private String postCode;

    @AutoDocField(note="季度")
    private String quarter;

    @AutoDocField(note="开始月份")
    private String startMonth;

    @AutoDocField(note="结束月份")
    private String endMonth;

    @AutoDocField(note="大区办事处")
    private String areaOfficeCode;

    private String buCode;

    private String type; //1、查岗位列表、2：查季度列表

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
