package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA与门店管理ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaTerminalManageNewResBean extends BaseBean {

    private static final long serialVersionUID = -4325770370848031782L;
    @AutoDocField(note="id")
    private String id;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="SA名称")
    private String saName;

    @AutoDocField(note="门店编号")
    private String terminalCode;

    @AutoDocField(note="门店名称")
    private String terminalName;

    @AutoDocField(note="开始时间")
    private String startDate;

    @AutoDocField(note="结束时间")
    private String endDate;

    @AutoDocField(note="有效关系")
    private String statusName;

    @AutoDocField(note="创建人")
    private String createdBy;

    @AutoDocField(note="创建人名称")
    private String createdName;


}
