package com.biostime.samanage.bean.terminal;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA与门店管理ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaTerminalManageEditReqBean extends BaseBean {

    private static final long serialVersionUID = 7000617978715764790L;
    @AutoDocField(note="id")
    private String id;

    @AutoDocField(note="开始时间")
    private String startDate;

    @AutoDocField(note="结束时间")
    private String endDate;

    @AutoDocField(note="登陆人账号")
    private String createdBy;

    @AutoDocField(note="登陆人名称")
    private String createdName;
}
