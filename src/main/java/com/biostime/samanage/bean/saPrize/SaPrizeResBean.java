package com.biostime.samanage.bean.saPrize;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取人员列表Req
 * Date: 2016-6-23
 * Time: 10:17
 * User: 12804
 * Version:1.0
 */
@Data
public class SaPrizeResBean extends BaseBean {
    private String account;  			//工号
    private String name; 				//名称
    private String prize;               //奖金
}
