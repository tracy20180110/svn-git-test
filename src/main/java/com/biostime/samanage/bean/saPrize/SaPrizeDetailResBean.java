package com.biostime.samanage.bean.saPrize;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA育婴顾问奖金明细Res
 * Date: 2016-6-23
 * Time: 10:17
 * User: 12804
 * Version:1.0
 */
@Data
public class SaPrizeDetailResBean extends BaseBean {

    private String wxCustomerPrize = "0";      //微信会员奖金
    private String newCustomerPrize = "0";     //新客奖金
    private String yxNewCustomerPrize = "0";	  //有效新客奖金
    private String rankSalary = "0";	//星级工资
    private String countPrize = "0";           //总计（实时奖金）
    private String wxCustomer = "0";           //微信会员数
    private String saCustomer = "0";  	  //SA会员

    private String babyMilkOne = "0";	//宝宝奶粉一阶
    private String babyMilkTwo = "0";	//宝宝奶粉二阶
    private String babyMilkThree = "0";	//宝宝奶粉三阶
    private String mamaMilk = "0";	//妈妈奶粉
    private String probiotics = "0";	//益生菌
    private String dodieDiaper = "0";	//纸尿裤
    private String countNewCustomer = "0";	//总计（新客数据）
    private String saMilkNewCustomer = "0";	//sa奶粉有效新客（新客总数）
    private String saPoint = "0";	//sa销售积分（积分总计）

    private String saWxNum = "0"; //微信会员数
    private String saNewCustomerNum = "0"; //新客奖金数
    private String saMilkNewCustomerNum = "0"; // 有效新客数
    private String salesstar = "无";   //星级
}
