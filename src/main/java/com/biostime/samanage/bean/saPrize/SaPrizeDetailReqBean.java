package com.biostime.samanage.bean.saPrize;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA育婴顾问奖金明细Req
 * Date: 2016-6-23
 * Time: 10:33
 * User: 12804
 * Version:1.0
 */
@Data
public class SaPrizeDetailReqBean extends BaseBean {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String month; // 考核月
    private String officeCode; // 办事处编码
    private String account; // 工号
    
    private String terminalCode; // 门店编码
    private String accountId; // 用户Id
}
