package com.biostime.samanage.bean.saPrize;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:TODO
 * Date: 2016-6-23
 * Time: 10:33
 * User: 12804
 * Version:1.0
 */
@Data
public class SaPrizeReqBean extends BaseBean {
    private String month;
    private String officeCode;
    private String buCode;
}
