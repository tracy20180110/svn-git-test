package com.biostime.samanage.bean.saPrize;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * SA考核指标(外部调用)
 * 
 * @title:SaAssessIndexResBean.java
 * @description:
 * @author 7919
 * @date 2018年1月26日 上午10:22:06
 */
@Data
public class SaAssessIndexResBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	
	private String wxCustomer = "0"; // 微信会员数
	private String saNewCustomerNum = "0"; // 新客数
	private String saMilkNewCustomerNum = "0"; // 奶粉有效新客数
}
