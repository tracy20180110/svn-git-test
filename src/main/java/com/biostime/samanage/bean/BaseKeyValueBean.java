package com.biostime.samanage.bean;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

@Data
public class BaseKeyValueBean extends BaseBean {

    @AutoDocField(note = "编码")
    private String key;

    @AutoDocField(note = "值")
    private String value;
}
