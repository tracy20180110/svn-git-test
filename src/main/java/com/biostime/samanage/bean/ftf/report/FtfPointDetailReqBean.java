package com.biostime.samanage.bean.ftf.report;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动报名信息Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfPointDetailReqBean extends BaseBean {
    private static final long serialVersionUID = 5804156224749199272L;
    @AutoDocField(note="大区办事处编码")
    private String areaOfficeCode;

    @AutoDocField(note="课程名称")
    private String name;

    @AutoDocField(note="活动日期开始")
    private String startDate;

    @AutoDocField(note="活动日期结束")
    private String endDate;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="会员ID")
    private String customerId;

    @AutoDocField(note="手机号码")
    private String mobile;

    @AutoDocField(note="事业部编码")
    private String buCode;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
