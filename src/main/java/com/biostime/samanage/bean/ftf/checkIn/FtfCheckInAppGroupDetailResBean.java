package com.biostime.samanage.bean.ftf.checkIn;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Ftf 组团签到信息
 * 
 * @title:FtfCheckInAppGroupDetailResBean.java
 * @description:
 * @author 7919
 * @date 2018年1月18日 下午5:13:18
 */
@Data
public class FtfCheckInAppGroupDetailResBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	@AutoDocField(note = "手机号码")
	private String mobile;
	@AutoDocField(note = "签到代码")
	private String signInCode;
	@AutoDocField(note = "签到名称")
	private String signInName;
	@AutoDocField(note = "组团ID")
	private String groupId;
	@AutoDocField(note = "成团人数")
	private int groupNums;

	public FtfCheckInAppGroupDetailResBean() {

	}

	public FtfCheckInAppGroupDetailResBean(String mobile, String signInCode, String groupId, String signInName, int groupNums) {
		this.mobile = mobile;
		this.signInCode = signInCode;
		this.groupId = groupId;
		this.signInName = signInName;
		this.groupNums = groupNums;
	}
}
