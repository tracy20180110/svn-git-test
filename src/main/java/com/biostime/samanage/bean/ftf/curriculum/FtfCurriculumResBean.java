package com.biostime.samanage.bean.ftf.curriculum;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:返回课程列表的Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfCurriculumResBean extends BaseBean {


    private static final long serialVersionUID = -483588397726795828L;

    @AutoDocField(note="课程ID")
    private String id;

    @AutoDocField(note="课程名称")
    private String name;

    @AutoDocField(note="课程状态")
    private String status;

    @AutoDocField(note="推荐产品")
    private String productName;

    @AutoDocField(note="推荐产品Id")
    private String productId;

    @AutoDocField(note="推荐人群")
    private String recomGroups;

    @AutoDocField(note="课程简介")
    private String synops;

    @AutoDocField(note="课程笔记")
    private String note;

    @AutoDocField(note="图片路径")
    private String imgPath;

    @AutoDocField(note="创建人工号")
    private String createdBy;

    @AutoDocField(note="创建人姓名")
    private String createdName;

    @AutoDocField(note="创建时间")
    private String createdTime;

    @AutoDocField(note="呈现应用")
    private String appType;

    @AutoDocField(note="呈现应用名称")
    private String appTypeName;

    @AutoDocField(note="所属类型")
    private String actType;

    @AutoDocField(note="所属类型")
    private String actTypeName;
}
