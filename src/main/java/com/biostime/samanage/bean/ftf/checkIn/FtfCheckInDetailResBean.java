package com.biostime.samanage.bean.ftf.checkIn;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动签到信息Res
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfCheckInDetailResBean extends BaseBean {

    private static final long serialVersionUID = 7038937137982915209L;
    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="课程名称")
    private String kcName;

    @AutoDocField(note="手机号码")
    private String mobile;

    @AutoDocField(note="会员ID")
    private String customerId;

    @AutoDocField(note="签到时间")
    private String singInTime;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="SA名称")
    private String saName;

    @AutoDocField(note="最近接触门店编号")
    private String terminalCode;

    @AutoDocField(note="签到途径")
    private String formSysSourse;

    @AutoDocField(note="签到途径")
    private String signInSourse;

    @AutoDocField(note="活动类型")
    private String type;


}
