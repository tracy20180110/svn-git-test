package com.biostime.samanage.bean.ftf.lecture;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:讲师列表的Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class FtfLecturerInfoReqBean extends BaseBean {


    private static final long serialVersionUID = 5107381880267780695L;
    @AutoDocField(note="查询列名")
    private String queryColumn;

    @AutoDocField(note="值")
    private String queryValue;

    @AutoDocField(note="数据类型")
    private String dataType;

    @AutoDocField(note="查询类型")
    private String queryType;
}
