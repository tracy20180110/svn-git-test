package com.biostime.samanage.bean.ftf.sign;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * @Description:FTF小程序签到
 * @Auther: 12804
 * @Date: 2019/6/12 16:05
 * @Version:1.0.0
 */
@Data
public class FtfSignMiniAppReqBean extends BaseBean {

    private static final long serialVersionUID = 7822328181182952602L;
    @AutoDocField(note="活动id")
    private String actId;

    @AutoDocField(note="会员ID")
    private String customerId;

    @AutoDocField(note="系统来源")
    private String sourceSystem;

    @AutoDocField(note="签到位置经度")
    private String longitude;

    @AutoDocField(note="签到位置纬度")
    private String latitude;

    @AutoDocField(note="SA编码")
    private String saCode;

}
