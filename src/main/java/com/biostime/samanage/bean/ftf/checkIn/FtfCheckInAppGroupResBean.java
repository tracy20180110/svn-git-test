package com.biostime.samanage.bean.ftf.checkIn;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.List;

@Data
public class FtfCheckInAppGroupResBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	List<FtfCheckInAppGroupDetailResBean> groupDetailResBeans;

	private String receiveStatus; // 领取状态
}
