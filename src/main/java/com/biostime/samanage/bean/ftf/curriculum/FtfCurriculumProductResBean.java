package com.biostime.samanage.bean.ftf.curriculum;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取BNC6品
 * Date: 2016-6-23
 * Time: 10:17
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfCurriculumProductResBean extends BaseBean {
    private static final long serialVersionUID = -8858959714902403524L;
    private String productId;
    private String productName;
    private String series;
}
