package com.biostime.samanage.bean.ftf.sign;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * @Description:
 * @Auther: 12804
 * @Date: 2019/8/6 17:04
 * @Version:1.0.0
 */
/**
 * @Description:FTF小程序签到
 * @Auther: 12804
 * @Date: 2019/6/12 16:05
 * @Version:1.0.0
 */
@Data
public class FtfSignMiniAppResBean extends BaseBean {
    private static final long serialVersionUID = -1632760721877885851L;

    @AutoDocField(note="活动id")
    private String actId;

    @AutoDocField(note="活动名称")
    private String actName;

    @AutoDocField(note="活动类型")
    private String actType;

    @AutoDocField(note="签到时间状态：1-签到时间，0-非签到时间")
    private String signTimeStatus;

    @AutoDocField(note="活动开始时间")
    private String startTime;

    @AutoDocField(note="活动结束时间")
    private String endTime;

    @AutoDocField(note="活动日期")
    private String actDate;

    @AutoDocField(note="活动状态（进行中，未开始，已结束）")
    private String actStatus;

    @AutoDocField(note="签到状态：1-已签到，0-未签到)")
    private String signStatus;

    @AutoDocField(note="SA编码")
    private String saCode;
}
