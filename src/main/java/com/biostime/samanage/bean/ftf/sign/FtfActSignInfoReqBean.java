package com.biostime.samanage.bean.ftf.sign;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:签到
 * Date: 2016-12-08
 * Time: 10:17
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfActSignInfoReqBean extends BaseBean {
    //活动ID
    private String id;
    //会员手机号码 用来创建会员
    private String mobile;
    //登陆人账号
    private String userId;
    //活动对应的SA编码
    private String saCode;
    //活动对应的门店编码
    private String terminalCode;
    private String customerId;
    private String seqNo;
    //系统来源
    private String sourceSystem;
    //签到位置经度
    private String longitude;
    //签到位置纬度
    private String latitude;

    //2019-01-16
    //图片文件（最多6张）
    private String[] files;

    //销量
    private String salesVolume;

}
