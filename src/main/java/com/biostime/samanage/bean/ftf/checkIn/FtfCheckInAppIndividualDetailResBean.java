package com.biostime.samanage.bean.ftf.checkIn;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Ftf 个人签到信息
 * 
 * @title:FtfCheckInAppIndividualDetailResBean.java
 * @description:
 * @author 7919
 * @date 2018年1月18日 下午5:13:02
 */
@Data
public class FtfCheckInAppIndividualDetailResBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	@AutoDocField(note = "手机号码")
	private String mobile;
	@AutoDocField(note = "签到代码")
	private String signInCode;
	@AutoDocField(note = "签到名称")
	private String signInName;
}
