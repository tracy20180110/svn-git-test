package com.biostime.samanage.bean.ftf.sign;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * @author 12804
 */
@Data
public class FtfActSignInfoBean extends BaseBean {
    private static final long serialVersionUID = 9206972376942451606L;

    //会有手机号码
    private String mobile;

    //签到时间
    private String signTime;

    //购买金额(feichat)
    private String fcSalesAmount;

    //有效购买金额(feichat)
    private String fcEffectiveSalesAmount;

    private Long customid;


}
