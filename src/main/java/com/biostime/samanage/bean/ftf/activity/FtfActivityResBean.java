package com.biostime.samanage.bean.ftf.activity;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:活动列表的Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfActivityResBean extends BaseBean {


    private static final long serialVersionUID = 2416417214149312983L;

    @AutoDocField(note="活动ID")
    private String id;

    @AutoDocField(note="讲师ID")
    private String lecturerId;

    @AutoDocField(note="课程ID")
    private String curriculumId;

    @AutoDocField(note="品牌")
    private String brand;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="课程名称")
    private String curriculumName;

    @AutoDocField(note="内部讲师ID")
    private String lecturerCode;

    @AutoDocField(note="讲师名称")
    private String lecturerName;

    @AutoDocField(note="活动日期")
    private String actDate;

    @AutoDocField(note="活动开始时间")
    private String startTime;

    @AutoDocField(note="活动结束时间")
    private String endTime;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="SA编号")
    private String saName;

    @AutoDocField(note="门店编号")
    private String terminalCode;

    @AutoDocField(note="活动类型")
    private String type;

    @AutoDocField(note="活动状态")
    private String status;

    @AutoDocField(note="省")
    private String province = "";

    @AutoDocField(note="市")
    private String city = "";

    @AutoDocField(note="区/县")
    private String district = "";

    @AutoDocField(note="活动地址")
    private String address;

    @AutoDocField(note="创建人ID")
    private String createdBy;

    @AutoDocField(note="创建人名称")
    private String createdName;

    @AutoDocField(note="创建时间")
    private String createdTime;

    @AutoDocField(note="视频链接")
    private String videoLink;

    @AutoDocField(note="品牌名称")
    private String brandName;

    @AutoDocField(note="门店名称")
    private String terminalName;

    /*@AutoDocField(note="活动流程")
    private String activityFlow;

    @AutoDocField(note="参与方式")
    private String participationMode;

    @AutoDocField(note="礼品图片一")
    private String giftPicturesOne;

    @AutoDocField(note="礼品图片二")
    private String giftPicturesTwo;

    @AutoDocField(note="组团类型1:不参团，2：参团")
    private Long groupType;

    @AutoDocField(note="参与人数")
    private String groupNums;

    @AutoDocField(note="导购ID")
    private String shoppingGuideId;

    @AutoDocField(note="活动上限团数")
    private String actGroupNums;*/

    @AutoDocField(note="分享海报链接")
    private String posterImageUrl;


    private String appTypeName;

    @AutoDocField(note = "外部讲师名称")
    private String externalName;

    @AutoDocField(note = "外部讲师编码")
    private String externalCode;

    @AutoDocField(note="讲师头衔")
    private String lecturerRank;

    @AutoDocField(note = "外部讲师头衔")
    private String externalRank;

    @AutoDocField(note = "外部讲师场地")
    private String externalExpertType;

    @AutoDocField(note = "内部讲师场地")
    private String lecturerExpertType;

    @AutoDocField(note = "FTF小程序签到二维码")
    private String ftfQrCodeUrl;

    @AutoDocField(note = "消费者类型 1：孕妈班，2：宝妈班 ")
    private Long consumerType;

    @AutoDocField(note="1:院内班,2:酒店班 (当活动类型是SA-FTF才有)")
    private String placeType;

    @AutoDocField(note="讲师工号")
    private String lecturerAccount;

    @AutoDocField(note="专家支持类型：1：专家类型，2：大会主席，3：会议讲着(当活动类型是学术推广才有2和3)")
    private String expertType;

    /**FTF主题：1：全品场FTF，2：Dodie专场FTF，3：羊奶专场FTF **/
    private String ftfTheme;

    //兼职讲师工号
    private String jzLecturerAccount;

    //兼职讲师名称
    private String jzLecturerName;

    //兼职讲师编码
    private String jzLecturerCode;

    //新客计算方式
    private String newCustomerCalculation;

    private String source;
}
