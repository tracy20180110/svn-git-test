package com.biostime.samanage.bean.ftf.sign;

import com.biostime.common.bean.BaseBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF签到销量明细
 * Date: 2016-6-23
 * Time: 10:17
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class FtfActSignListBean extends BaseBean {

    private static final long serialVersionUID = -8530232250998710408L;

    //签到人数
    private String signCount;

    //总有效销量（feichat总量+一般贸易总量）
    private String salesCount;

    //是否可以签到
    private String isSignBtn;

    private List<FtfActSignInfoBean> signInfoBeanList;

    //一般贸易
    private List<BaseKeyValueBean> tradeVolumeList;
}
