package com.biostime.samanage.bean.ftf.lecture;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:讲师列表的Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class FtfLecturerInfoResBean extends BaseBean {
    private static final long serialVersionUID = -670143439026469216L;


    @AutoDocField(note="讲师名称")
    private String expertName;

    @AutoDocField(note="讲师ID")
    private String expertId;

    @AutoDocField(note="讲师类型 0:内部，1：外部")
    private String expertType;

    @AutoDocField(note="讲师头衔")
    private String level;

    @AutoDocField(note="讲师头像图片")
    private String imageUrl;

    @AutoDocField(note="讲师状态0：禁用，1：启用")
    private String state;

    @AutoDocField(note="讲师手机号码")
    private String mobile;

    @AutoDocField(note="讲师认证0：否，1：是")
    private String certificate;

    @AutoDocField(note="讲师简介")
    private String introduction;

    @AutoDocField(note="讲师编码")
    private String innerAccount;

}
