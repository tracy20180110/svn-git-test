package com.biostime.samanage.bean.ftf.activity;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:活动模板列表的Req
 * Date: 2018-7-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfActivityTemplateInfoResBean extends BaseBean {



    @AutoDocField(note="活动时间")
    private String actTime;

    @AutoDocField(note="活动地点")
    private String actAddress;

    @AutoDocField(note="课程简介")
    private String curriculumSynops;

    @AutoDocField(note="讲师简介")
    private String lecturerSynops;

    @AutoDocField(note="活动主题")
    private String actName;

    @AutoDocField(note="讲师头衔")
    private String lecturerRank;

    @AutoDocField(note="讲师名称")
    private String lecturerName;

    @AutoDocField(note="课程图片")
    private String curriculumImagePath;

    @AutoDocField(note="分享文案")
    private String sharingCopywriting;

}
