package com.biostime.samanage.bean.ftf.app;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:录入工作人员
 * Date: 2020-7-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class AccountInfoResBean extends BaseBean {


    private static final long serialVersionUID = 3311016422350318399L;

    @AutoDocField(note="id")
    private String accountId;

    @AutoDocField(note="工号")
    private String accountCode;

    @AutoDocField(note="名称")
    private String accountName;

    @AutoDocField(note="岗位名称")
    private String postName;

    @AutoDocField(note="来源ID")
    private String sourceId;

}
