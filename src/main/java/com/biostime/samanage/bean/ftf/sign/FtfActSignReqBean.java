package com.biostime.samanage.bean.ftf.sign;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取人员列表Req
 * Date: 2016-6-23
 * Time: 10:17
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfActSignReqBean extends BaseBean {
    private String type;
    private String saCode;
    private String officeCode;
}
