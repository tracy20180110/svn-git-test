package com.biostime.samanage.bean.ftf.app;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:录入工作人员
 * Date: 2020-7-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class AccountInfoReqBean extends BaseBean {


    private static final long serialVersionUID = 1479701782556997097L;

    @AutoDocField(note="查询的工号或名称")
    private String keyword;

}
