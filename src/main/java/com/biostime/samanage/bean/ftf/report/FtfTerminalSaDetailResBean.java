package com.biostime.samanage.bean.ftf.report;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动SA、FTF信息Res
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfTerminalSaDetailResBean extends BaseBean {

    private static final long serialVersionUID = 8580607467049894577L;


    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="创建人工号")
    private String createdBy;

    @AutoDocField(note="创建人名称")
    private String createdName;

    @AutoDocField(note="课程名称")
    private String kcName;

    @AutoDocField(note="活动日期")
    private String actDate;

    @AutoDocField(note="开始时间")
    private String startTime;

    @AutoDocField(note="结束时间")
    private String endTime;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="讲师名称")
    private String lecturerName;

    @AutoDocField(note="讲师编号")
    private String lecturerCode;

    @AutoDocField(note="推荐产品")
    private String productId;

    @AutoDocField(note="门店编号")
    private String terminalCode;

    @AutoDocField(note="签到人数")
    private String singInNum;

    @AutoDocField(note="FTF活动积分")
    private String ftfActPoint;

    @AutoDocField(note="照片1")
    private String image1;

    @AutoDocField(note="照片2")
    private String image2;

    @AutoDocField(note="照片3")
    private String image3;


    @AutoDocField(note="活动类型")
    private String actType;

    @AutoDocField(note="Feichat跨境购销量")
    private String fcCBECSales;

    @AutoDocField(note="Feichat有效跨境购销量")
    private String fcCBECEffectiveSales;

    @AutoDocField(note="一般贸易销量")
    private String tradeVolume;

    @AutoDocField(note="活动有效性")
    private String actValid;

    @AutoDocField(note="活动品牌")
    private String brandName;

    @AutoDocField(note="认证讲师")
    private String ftfLecturerCheck;
    
    @AutoDocField(note="管理渠道")
    private String terminalChannel;

    @AutoDocField(note="活动当天用券数")
    private String actDayCouponNums;

    @AutoDocField(note = "活动规模")
    private String actScale;

    @AutoDocField(note = "场地类型")
    private String placeType;

    @AutoDocField(note = "外部讲师名称")
    private String externalName;

    @AutoDocField(note = "外部讲师编码")
    private String externalCode;

    @AutoDocField(note="讲师头衔")
    private String lecturerRank;

    @AutoDocField(note = "外部讲师头衔")
    private String externalRank;

    @AutoDocField(note = "外部讲师场地")
    private String externalExpertType;

    @AutoDocField(note = "内部讲师场地")
    private String lecturerExpertType;

    @AutoDocField(note="Dodie品项新客")
    private String dodieNewCustomer;

    @AutoDocField(note="合生元品项新客")
    private String biostimeNewCustomer;

    @AutoDocField(note="益生菌品项新客")
    private String probioticsNewCustomer;

    @AutoDocField(note="HT品项新客")
    private String htNewCustomer;

    @AutoDocField(note="Dodie Feichat销售额")
    private String feichatDodieNum;

    @AutoDocField(note="Dodie Feichat销售包数")
    private String feichatDodieBNum;

    /**FTF主题：1：全品场FTF，2：Dodie专场FTF，3：羊奶专场FTF **/
    private String ftfTheme;

    //兼职讲师工号
    private String jzLecturerAccount;

    //兼职讲师名称
    private String jzLecturerName;

    //讲师岗位名称
    private String postName;

    //羊奶新客
    private String kbsNewCustomer;

    @AutoDocField(note="Dodie销量")
    private String dodieSalesNums;

    @AutoDocField(note="合生元销量")
    private String biostimeSalesNums;

    @AutoDocField(note="益生菌销量")
    private String probioticsSalesNums;

    @AutoDocField(note="HT销量")
    private String htSalesNums;

    @AutoDocField(note="羊奶销量")
    private String kbsSalesNums;

    //兼职讲师归属
    private String lecturerByCompany;
}
