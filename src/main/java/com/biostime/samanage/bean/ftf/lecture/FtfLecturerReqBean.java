package com.biostime.samanage.bean.ftf.lecture;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:讲师列表的Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfLecturerReqBean  extends BaseBean {

    private static final long serialVersionUID = -4896200342742121834L;

    @AutoDocField(note="事业部编码")
    private String buCode;

    @AutoDocField(note="讲师状态")
    private String status;

    @AutoDocField(note="讲师类型")
    private String type;

    @AutoDocField(note="讲师名称")
    private String name;

    @AutoDocField(note="讲师编号")
    private String code;

    @AutoDocField(note="id")
    private String id;

    @AutoDocField(note="讲师简介")
    private String synops;

    @AutoDocField(note="登录人ID")
    private String userId;

    @AutoDocField(note="登陆人名称")
    private String userName;

    @AutoDocField(note="新增活动讲师搜索")
    private String keyword;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
