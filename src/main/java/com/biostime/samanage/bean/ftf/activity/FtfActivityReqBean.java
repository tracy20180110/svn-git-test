package com.biostime.samanage.bean.ftf.activity;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.domain.ftf.FtfActLecturer;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:活动列表的Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfActivityReqBean extends BaseBean {

    private static final long serialVersionUID = -1476879374333844710L;

    @AutoDocField(note="id")
    private String id;

    @AutoDocField(note="品牌")
    private String brand;

    @AutoDocField(note="品牌名称")
    private String brandName;

    @AutoDocField(note="大区办事处")
    private String areaOfficeCode;

    @AutoDocField(note="活动类型") //0:SA-FTF,1:门店-FTF,2:学术推广,3：内训
    private String type;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="门店编号")
    private String terminalCode;

    @AutoDocField(note="课程ID")
    private String curriculumId;

    @AutoDocField(note="讲师Id")
    private String lecturerId;

    @AutoDocField(note="讲师头衔")
    private String lecturerRank;

    @AutoDocField(note="活动日期")
    private String actDate;

    @AutoDocField(note="活动开始时间")
    private String startTime;

    @AutoDocField(note="活动结束时间")
    private String endTime;

    @AutoDocField(note="省")
    private String province = "";

    @AutoDocField(note="市")
    private String city = "";

    @AutoDocField(note="区/县")
    private String district = "";

    /** 详细地址,一般不包括省市区信息 */
    @AutoDocField(note="详细地址")
    private String address = "";

    @AutoDocField(note="分享文案")
    private String sharingCopywriting;

    @AutoDocField(note="登录人ID")
    private String userId;

    @AutoDocField(note="登陆人名称")
    private String userName;

    @AutoDocField(note="状态")
    private String status;

    @AutoDocField(note="课程名称")
    private String curriculumName;

    @AutoDocField(note="讲师编号")
    private String lecturerCode;

    @AutoDocField(note="视频链接")
    private String videoLink;

    @AutoDocField(note="1:状态，2：添加视频")
    private String methodType;

    @AutoDocField(note="分享海报链接")
    private String posterImageUrl;

    @AutoDocField(note="工号ID")
    private String accountId;

    @AutoDocField(note="模板内容")
    private List<BaseKeyValueBean> listKeyValue;

    @AutoDocField(note="模板ID")
    private String actTemplateId;

    @AutoDocField(note="是否生成页面 1：是，0：否")
    private String createPage;

    @AutoDocField(note="讲师名称")
    private String lecturerName;

    @AutoDocField(note="1:院内班,2:酒店班 (当活动类型是SA-FTF才有)")
    private String placeType;

    @AutoDocField(note="专家支持类型：1：专家类型，2：大会主席，3：会议讲着(当活动类型是学术推广才有2和3)")
    private String expertType;

    @AutoDocField(note = "讲师信息")
    private List<FtfActLecturer> ftfActLecturer;

    private String token;

    /**讲师类型 1:外部，0：内部**/
    private String lecturerType;

    //2019.6 版本 FTF小程序签到二维码
    private String ftfQrCodeUrl;

    @AutoDocField(note = "消费者类型 1：孕妈班，2：宝妈班 ")
    private Long consumerType;

    /**FTF主题：1：全品场FTF，2：Dodie专场FTF，3：羊奶专场FTF **/
    private Long ftfTheme;

    //兼职讲师
    private String jzLecturerAccount;

    //事业部编码
    private String buCode;


    /**新客计算方式 **/
    private Long newCustomerCalculation;

    /**数据来源： 1：网页版，2：营销通APP**/
    private Long source;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
