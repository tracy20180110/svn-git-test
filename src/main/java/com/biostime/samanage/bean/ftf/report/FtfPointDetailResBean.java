package com.biostime.samanage.bean.ftf.report;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动积分信息Res
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfPointDetailResBean extends BaseBean {

    private static final long serialVersionUID = 8485895426726256834L;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="课程名称")
    private String kcName;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="会员ID")
    private String customerId;

    @AutoDocField(note="会员名称")
    private String customerName;

    @AutoDocField(note="手机号码")
    private String mobile;

    @AutoDocField(note="固定电话")
    private String phone;

    @AutoDocField(note="购买时间")
    private String createdTime;

    @AutoDocField(note="产品ID")
    private String productId;

    @AutoDocField(note="产品名称")
    private String productName;

    @AutoDocField(note="产品积分")
    private String productPoint;

    @AutoDocField(note="门店编号")
    private String terminalCode;
}
