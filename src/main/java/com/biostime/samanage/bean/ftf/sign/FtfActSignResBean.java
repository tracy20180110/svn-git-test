package com.biostime.samanage.bean.ftf.sign;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取人员列表Res
 * Date: 2016-6-23
 * Time: 10:17
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfActSignResBean extends BaseBean {

    private static final long serialVersionUID = 6923961849652934781L;

    /**
     * 活动ID
     */
    private String id;
    /**
     * 活动名称
     */
    private String name;

    /**
     * 活动日期
     */
    private String actDate;
    /**
     * 开始时间
     */
    private String startActTime;

    /**
     * 结束时间
     */
    private String endActTime;

    /**
     * 签到中人数
     */
    private String allCount;

    /**
     * 微信签到中人数
     */
    private String wxCount;

    /**
     * 开班培训签到中人数
     */
    private String kbCount;

    /**
     * 门店编码
     */
    private String terminalCode;

    /**
     * sa编码
     */
    private String saCode;

    /**
     * 活动开始状态(0:未开始,1:进行中,2:已结束)
     */
    private String actStatusCode;

    /**
     * 活动开始状态(0:未开始,1:进行中,2:已结束)
     */
    private String actStatusName;

    /**
     * 是否有进去按钮（0:无,1:有）
     */
    private String isStartBtn;

    @AutoDocField(note = "FTF小程序签到二维码")
    private String ftfQrCodeUrl;
}
