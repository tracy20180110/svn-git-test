package com.biostime.samanage.bean.ftf.report;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动新客信息Res
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfNewCustomerDetailResBean extends BaseBean {

    private static final long serialVersionUID = 7038937137982915209L;
    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="课程名称")
    private String kcName;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="手机号码")
    private String mobile;

    @AutoDocField(note="会员ID")
    private String customerId;

    @AutoDocField(note="会员名称")
    private String customerName;

    @AutoDocField(note="购买时间")
    private String createdTime;

    @AutoDocField(note="产品ID")
    private String productId;

    @AutoDocField(note="产品名称")
    private String productName;

    @AutoDocField(note="是否首次购买")
    private String isSc;

    @AutoDocField(note="是否交叉品项")
    private String isJc;

    @AutoDocField(note="最近接触门店编号")
    private String terminalCode;

    @AutoDocField(note="兼职讲师工号")
    private String jzLecturerAccount;

    //兼职讲师名称
    @AutoDocField(note="兼职讲师名称")
    private String jzLecturerName;

    @AutoDocField(note="内部讲师工号")
    private String lecturerAccount;

    @AutoDocField(note="内部讲师名称")
    private String lecturerName;

    @AutoDocField(note="品项名称")
    private String categoryName;

    //兼职讲师归属
    private String lecturerByCompany;
}
