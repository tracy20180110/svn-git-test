package com.biostime.samanage.bean.ftf;

import com.biostime.common.bean.BaseBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumProductResBean;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF-课程--BNC6品产品
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class Bnc6Product  extends BaseBean {

    private static final long serialVersionUID = 3529331014456255711L;

    private String series;       //系列
    private List<FtfCurriculumProductResBean> list;

}
