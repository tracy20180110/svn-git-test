package com.biostime.samanage.bean.ftf.lecture;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:讲师列表列表的Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class FtfLecturerResBean  extends BaseBean {

    private static final long serialVersionUID = -4321362616611462971L;

    @AutoDocField(note="id")
    private String id;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="讲师状态")
    private String status;

    @AutoDocField(note="讲师类型")
    private String type;

    @AutoDocField(note="讲师名称")
    private String name;

    @AutoDocField(note="讲师编号对应C端的讲师ID")
    private String code;

    @AutoDocField(note="讲师简介")
    private String synops;

    @AutoDocField(note="创建人")
    private String createdBy;

    @AutoDocField(note="创建人名称")
    private String createdName;

    @AutoDocField(note="创建时间")
    private String createdTime;

    @AutoDocField(note="讲师验证")
    private String ftfLecturerCheck;

    @AutoDocField(note="讲师头衔")
    private String rank;

    @AutoDocField(note="内部讲师工号")
    private String lecturerAccount;

}
