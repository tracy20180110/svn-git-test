package com.biostime.samanage.bean.ftf.lecture;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取讲师工号
 * Date: 2016-6-23
 * Time: 10:17
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfLectureCodeResBean extends BaseBean {
    private static final long serialVersionUID = -8277761783473403892L;
    private String code;
    private String name;
    private String keyword;
}
