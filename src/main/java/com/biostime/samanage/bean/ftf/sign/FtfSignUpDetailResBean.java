package com.biostime.samanage.bean.ftf.sign;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动报名信息Res
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfSignUpDetailResBean  extends BaseBean {
    private static final long serialVersionUID = 4364800943469940385L;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="课程名称")
    private String kcName;

    @AutoDocField(note="讲师名称")
    private String jsName;

    @AutoDocField(note="活动日期")
    private String actDate;

    @AutoDocField(note="开始时间")
    private String startTime;

    @AutoDocField(note="结束时间")
    private String endTime;

    @AutoDocField(note="报名手机号码")
    private String mobile;

    @AutoDocField(note="报名时间")
    private String singUpTime;

    @AutoDocField(note="报名途径")
    private String formSysSourse;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="SA名称")
    private String saName;

    @AutoDocField(note="门店编号")
    private String terminalCode;

    @AutoDocField(note="活动类型")
    private String type;

    @AutoDocField(note="活动状态")
    private String status;

    @AutoDocField(note="创建人工号")
    private String createdBy;

    @AutoDocField(note="创建人工号")
    private String createdName;

}
