package com.biostime.samanage.bean.ftf.activity;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:活动模板列表的Req
 * Date: 2018-7-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfActivityTemplateResBean extends BaseBean {



    @AutoDocField(note="模板ID")
    private String id;

    @AutoDocField(note="模板名称")
    private String name;

    @AutoDocField(note="模板图片")
    private String image;

    @AutoDocField(note="模板背景音乐")
    private String bgMusic;

    @AutoDocField(note="模板背景图片")
    private String bgImage;

    @AutoDocField(note="0:失效，1：有效")
    private String status;


}
