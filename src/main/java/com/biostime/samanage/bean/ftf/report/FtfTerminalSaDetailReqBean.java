package com.biostime.samanage.bean.ftf.report;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动SA、FTF信息Req
 * Date: 2016-6-20
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class FtfTerminalSaDetailReqBean extends BaseBean {

    private static final long serialVersionUID = -1025560052215100341L;

    @AutoDocField(note="大区办事处编码")
    private String areaOfficeCode;

    @AutoDocField(note="活动日期开始")
    private String startDate;

    @AutoDocField(note="活动日期结束")
    private String endDate;

    @AutoDocField(note="SA编号")
    private String saCode;

    @AutoDocField(note="活动类型")
    private String type;

    @AutoDocField(note="讲师编号")
    private String lecturerCode;

    @AutoDocField(note="兼职讲师")
    private String jzLecturerAccount;

    @AutoDocField(note="活动有效性0:无效，1：有效")
    private String actValid;

    @AutoDocField(note="事业部编码")
    private String buCode;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
