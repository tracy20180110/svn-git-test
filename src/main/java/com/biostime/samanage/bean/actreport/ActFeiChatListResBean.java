package com.biostime.samanage.bean.actreport;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.List;

/**
 * Describe:feichat贸易销量
 * Date: 2019-07-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActFeiChatListResBean extends BaseBean {
        private static final long serialVersionUID = 5100134207438218559L;
        private String actId;  //活动ID
        private String createdBy; //创建人
        private String actDate;  //活动日期
        private String startTime;  // 活动开始时间
        private List<Long> customerId; //会员ID
}
