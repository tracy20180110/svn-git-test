package com.biostime.samanage.bean.actreport;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:获取登陆人可上报的活动
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRePortListReqBean extends BaseBean {
    private static final long serialVersionUID = 527551745971541479L;
    private String officeCode;
    private String channelCode;
    private String type;
    private String actConfigId;
    private String terminalCode;
    private String backUpType; //补录类型  1：上报  2：补录

    private String startDate;
    private String endDate;

    private String buCode;
}
