package com.biostime.samanage.bean.actreport;

import com.biostime.common.bean.BaseBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import lombok.Data;

import java.util.List;

/**
 * Describe:获取登陆人可上报的活动
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRePortListResBean extends BaseBean {
        private static final long serialVersionUID = 5100134207438218559L;
        private String code;  //活动编码
        private String name;  //活动名称  //获取SA对应的门店 功能中的SA名称
        private String ctName;  // 获取SA对应的门店 功能 中的连锁门店名称
        private String status; //9月版本中  未开始的活动 0:不可择、1：可选
        private String khType; //2018-03月版本中  考核类型1:路演活动，2：物料陈列
        private String actReportType;//上报选相册（1：是，0否/空也是否）
        private List<BaseKeyValueBean> listBrand; //品牌
        //门店编码名称
        private String shortName;
}
