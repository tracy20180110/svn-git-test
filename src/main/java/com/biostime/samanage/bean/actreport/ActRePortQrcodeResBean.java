package com.biostime.samanage.bean.actreport;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:活动上报
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class ActRePortQrcodeResBean extends BaseBean {

    private static final long serialVersionUID = -3201564663642831587L;

    //表ID
    private String id;

    //二维码编码
    private String qrCode;

    //二维码URL
    private String qrUrl;

    //二维码短URL
    private String qrShortUrl;

    //二维码类型
    private String qrCodeType;

    //创建时间
    private String createdDate;

    //验证二维码时间是否超过30天
    private String status;


}
