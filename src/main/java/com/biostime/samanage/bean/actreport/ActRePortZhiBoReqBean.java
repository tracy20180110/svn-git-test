package com.biostime.samanage.bean.actreport;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.samanage.bean.common.PageBaseBean;
import lombok.Data;

import java.util.List;

/**
 * Describe:活动上报
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class ActRePortZhiBoReqBean extends PageBaseBean {

    private static final long serialVersionUID = -4190112906100964122L;

    /**
     * SA编码
     */
    private String saCode;

    /**
     * 操作员
     */
    private String createdBy;

    /** 创建者名称 */
    private String createdName;


    /**
     * 直播主题
     */
    private String titleName;

    /**
     * 直播日期
     */
    private String zbDate;


    /**
     * 直播开始时间
     */
    private String startTime;

    /**
     * 直播开始时间
     */
    private String endTime;


    /**
     * 主播工号
     */
    private String zbCode;

    /**
     * 主播名称
     */
    private String zbName;

    /**
     * 辅播工号
     */
    private String fbCode;

    /**
     * 辅播名称
     */
    private String fbName;

    /**
     * 直播专家(格式姓名+单位+科室职称)
     */
    private String zbExpert;

    /**
     * 合作门店数量
     */
    private Integer terminalNums;

    /**
     * 观看人次
     */
    private Integer visitorsNums;


    /**
     * 直播销量
     */
    private String zbSales;


    /**
     * 报名社群数
     */
    private Integer	regGroupNums;

    /**
     * 覆盖群人数
     */
    private Integer	coverNums;

    /**
     * 总销售金额
     */
    private String totalSalesAmount;

    /**
     * 合生元婴幼儿奶粉新客
     */
    private Integer	bstBabyNewCust;

    /**
     * 合生元妈妈奶粉新客
     */
    private Integer	bstMamaNewCust;

    /**
     * 益生菌新客数
     */
    private Integer	probioticNewCust;

    /**
     * 羊奶新客数
     */
    private Integer	kbsNewCust;

    /**
     * HT新客数
     */
    private Integer	htNewCust;

    /**
     * Dodie新客
     */
    private Integer	dodieNewCust;

    /**
     * 图片路径1
     */
    private String	imagePath1;

    /**
     * 图片路径2
     */
    private String	imagePath2;

    /**
     * 图片路径3
     */
    private String	imagePath3;

    /**
     * 直播平台名称
     */
    private String	zbTerraceName;


    /**
     * 合作渠道名称
     */
    private String	channelName;


    /**
     * 门店类型名称
     */
    private String	terminalTypeName;


    /**
     * 品牌名称
     */
    private String	brandName;

    /**
     * 直播平台编码
     */
    private String	zbTerraceCode;


    /**
     * 合作渠道编码
     */
    private String	channelCode;


    /**
     * 门店类型编码
     */
    private String	terminalTypeCode;


    /**
     * 品牌编码
     */
    private String	brandCode;

    private String areaName; //大区名称
    private String officeName; //办事处名称

    @AutoDocField(note="事业部编码")
    private String buCode;
    @AutoDocField(note="大区办事处编码")
    private String areaOfficeCode;

    /*** 活动开始日期 */
    private String startDate;

    /** * 活动开始日期 */
    private String endDate;

    /**
     * 渠道类型（1：线上妈妈班，2：线下妈妈班，3：内训）
     */
    private Long	channelType;


    /**
     * 活动模式（1：直播、2：语音）
     */
    private Long	actModel;


    /**
     * 授课讲师工号
     */
    private String	lecturerCode;


    /**
     * 授课讲师名称
     */
    private String	lecturerName;

    /**
     * 签到人数
     */
    private String	signNum;

    /**
     * 合作门店编码
     */
    private String	cooperationTerminalCode;

    //录入人员列表
    private List<Long> accountList;
}
