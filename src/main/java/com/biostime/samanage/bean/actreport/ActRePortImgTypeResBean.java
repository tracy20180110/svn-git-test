package com.biostime.samanage.bean.actreport;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:根据门店返回对应的活动类型
 * Date: 2018-08-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRePortImgTypeResBean extends BaseBean {

    @AutoDocField(note="id")
    private String id;

    @AutoDocField(note="名称")
    private String name;

    @AutoDocField(note="状态名称")
    private String statusName;

    @AutoDocField(note="状编码")
    private String statusCode;

    @AutoDocField(note="创建时间")
    private String createdTime;

    @AutoDocField(note="创建人名称")
    private String createdName;

    @AutoDocField(note="创建人工号")
    private String createdBy;
}
