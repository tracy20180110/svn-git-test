package com.biostime.samanage.bean.actreport;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:活动上报网页版
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRePortWebReqBean extends BaseBean {

    private static final long serialVersionUID = -3518433345983534565L;
    private String code;   //活动编码
    private String terminalCode; //门店编码
    private String imgOne;
    private String imgTwo;
    private String imgThree;
    private String createdName;
    private String createdBy;
    private String rePortDate;  //上报日期
    private String makeupMemo;  //补录原因

    private String channelCode;  //活动渠道
    private String areaOfficeCode; //活动大区办事处

}
