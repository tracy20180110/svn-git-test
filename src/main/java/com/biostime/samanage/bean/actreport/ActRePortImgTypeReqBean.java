package com.biostime.samanage.bean.actreport;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:根据门店返回对应的活动类型
 * Date: 2018-08-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRePortImgTypeReqBean extends BaseBean {
    private String id;
    private String name;    //图片类型名称
    private String status;  //状态  1：启用  2：停用
    private String userId;  //登录人工号
    private String userName; //登录人名称
    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
