package com.biostime.samanage.bean.actreport;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:活动上报网页版
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRePortWebResBean extends BaseBean {

    private static final long serialVersionUID = 5381660358268780970L;
    private String actStartDate;   //活动开始时间
    private String actEndDate;  //活动结束时间
    private String channelCode;  //活动渠道
    private String areaOfficeCode; //活动大区办事处
    private String actName; //活动名称
}
