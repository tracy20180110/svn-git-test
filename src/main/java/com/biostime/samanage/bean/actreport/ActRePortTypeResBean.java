package com.biostime.samanage.bean.actreport;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.List;

/**
 * Describe:根据门店返回对应的活动类型
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Data
public class ActRePortTypeResBean extends BaseBean {
    private static final long serialVersionUID = 344258621364282166L;
    private String officeCode;
    private String channelCode;
    private List<ActRePortListResBean> types;
}
