package com.biostime.samanage.bean.actreport;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Describe:活动上报
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class ActRePortReqBean extends BaseBean {

    private static final long serialVersionUID = -3201564663642831587L;

    //活动编码
    private String code;

    //备注（开班培训就写开班培训）
    private String memo;

    //图片文件（最多6张）
    private String[] files;

    //登陆人账号
    private String userId;

    //登陆人名称
    private String createdName;
    //设备ID
    private String deviceId;
    //门店编号
    private String terminalCode;

    //方法操作类型 1：路演活动现场、2：SA上报 3、开班培训
    private String methodType;

    //1:营销通FTF上报、2：SA 3：B端开班培训  （2017-10-13  可删除 暂无用）
//    private String type;

    private String officeCode;

    //报备开始日期
    private String startDate;

    //报备结束日期
    private String endDate;

    // 考核类型1:路演活动，2：物料陈列
    private String khType;

    //1:确定上报，2：取消不做任何操作
    private String flag;

    //补录类型  1：上报  2：补录
    private String backUpType;

    //补录原因(1：网络原因未能及时上报、2：营销通系统原因未能及时上报、3：其他)
    private String makeupMemo;

    //补录原因 其他
    private String makeupMemoOther;
    //上报选择的品牌
    private String[] brandCode;

    //SA-FTF活动ID
    private String saFtfActId;

    //2019-01-28 (只用活动上报的活动列表接口)
    private String queryType;

    @AutoDocField(note="大区（ID）")
    private String areaCode;

//    @AutoDocField(note="办事处（ID）") 有一样的
//    private String officeCode;

    @AutoDocField(note="门店渠道")
    private String channelCode;

    @AutoDocField(note="事业部编码")
    private String buCode;

    //0：非指定岗位  1、BNC活动精英  2：活动精英
    private String bncElites;
}
