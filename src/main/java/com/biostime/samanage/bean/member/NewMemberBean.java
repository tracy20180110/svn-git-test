package com.biostime.samanage.bean.member;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.Date;

/**
 * Created by zhuhaitao on 2017/2/22.
 */
@Data
public class NewMemberBean extends BaseBean {

   private String name;//*姓名
   private Date createdTime;//*注册时间
   private String phone;//电话
   private String mobilePhone;//*手机
   private String srcLocName;//*得悉途径
   private String saCode;//*SA编号
   private String operateBCC;//BCC工号
   private String nursingConsultring;//育婴顾问工号
   private Date kidBirthDay;//*宝宝生日/预产期
   private String kidName;//宝宝姓名
   private String kidSex;//宝宝性别
   private String province;//省份
   private String city;//市或县
   private String memo;//备注
   private String operator;
   private int rowNun;

   private com.biostime.samanage.domain.HpMemberEventRec hpMemberEventRec;
}
