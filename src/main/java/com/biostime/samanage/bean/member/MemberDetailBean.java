package com.biostime.samanage.bean.member;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.samanage.bean.common.PageBaseBean;
import lombok.Data;

import java.util.Date;

/**
 * Created by zhuhaitao on 2017/1/3.
 */
@Data
public class MemberDetailBean extends PageBaseBean {
    @AutoDocField(note = "大区", nullable = true)
    private String areaCode;
    @AutoDocField(note = "大区", nullable = true)
    private String areaName;
    @AutoDocField(note = "办事处", nullable = true)
    private String officeCode;
    @AutoDocField(note = "办事处", nullable = true)
    private String officeName;
    @AutoDocField(note = "SA编号", nullable = true)
    private String saCode;
    @AutoDocField(note = "SA姓名", nullable = true)
    private String saName;
    @AutoDocField(note = "会员ID", nullable = true)
    private Long customerId;
    @AutoDocField(note = "会员姓名", nullable = true)
    private String name;
    @AutoDocField(note = "手机号码", nullable = true)
    private String mobile;
    @AutoDocField(note = "系统来源", nullable = true)
    private String system;
    @AutoDocField(note = "所属BCC", nullable = true)
    private String proBcc;
    @AutoDocField(note = "实操BCC", nullable = true)
    private String operateBcc;
    @AutoDocField(note = "所属育婴顾问", nullable = true)
    private String nursingConsultant;
    @AutoDocField(note = "礼包/礼品", nullable = true)
    private String giftName;
    @AutoDocField(note = "得悉途径 ", nullable = true)
    private String srcLocName;
    @AutoDocField(note = "名单类别 ", nullable = true)
    private String memberType;
    @AutoDocField(note = "开始时间 ", nullable = true)
    private Date startTime;
    @AutoDocField(note = "结束时间 ", nullable = true)
    private Date endTime;
    @AutoDocField(note = "记录时间 ", nullable = true)
    private Date recTime;
    @AutoDocField(note = "来源 ", nullable = true)
    private String recSrc;

    @AutoDocField(note = "宝宝生日/预产期 ", nullable = true)
    private Date birthdate;
    @AutoDocField(note = "宝宝生日/预产期(字符串) ", nullable = true)
    private String birthdateStr;

}
