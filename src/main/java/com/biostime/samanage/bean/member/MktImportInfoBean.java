package com.biostime.samanage.bean.member;

import com.biostime.samanage.bean.common.PageBaseBean;
import lombok.Data;

import java.util.Date;

/**
 * 营销通批量导入
 */

@Data
public class MktImportInfoBean extends PageBaseBean {
    /**
     * @Fields serialVersionUID : TODO
     */
    private static final long serialVersionUID = 6078874925848836402L;
    private String id;
    private String type;//功能模块名称
    private String filePath;//上传文件路径
    private String fileName;//上传文件名
    private String totalCount;//总记录数
    private Date createdTime;//创建时间
    private String createdBy;//创建人
    private Date updatedTime;//更新时间
    private String updatedBy;//更新人
    private String createdName;//创建人名称

    private Date startTime;//开始时间（搜索条件）
    private Date endTime;//结束时间（搜索条件）
}