package com.biostime.samanage.bean.member;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * 导入文件校验错误信息描述
 */
@Data
public class ExgPlmOutItem extends BaseBean {
    /**
     * 失败原因描述
     */
    private String description;

}
