package com.biostime.samanage.bean.member;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.List;

/**
 * 导入文件结果
 */
@Data
public class ExgPlmOutImportResult extends BaseBean {

    private int totalCount;

    private int succeedCount;

    private int failedCount;

    private String importDate;

    private String operator;

    private List<ExgPlmOutItem> items;

    private List<String> descriptions;
}
