package com.biostime.samanage.bean.member;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.Date;

@Data
public class CommonDepartmentBean extends BaseBean {
    private Long id;
    private String areaCode;
    private String areaName;
    private String officeCode;
    private String officeName;
    private Long parentId;
    private String name;
    private Integer tier;
    private Integer isEnd;
    private String logisticsCode;
    private String remark;
    private String createdBy;
    private Date createdTime;
    private String updatedBy;
    private Date updatedTime;
    private String shortName;
}
