package com.biostime.samanage.bean;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

import java.util.Date;

/**
 * 类功能描述: SA工作排班
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年6月23日 上午10:59:18
 */
@Data
public class SaJobSchedulingBean extends BaseBean {
	private static final long serialVersionUID = -489655629817627553L;
	@AutoDocField(note = "id", nullable = true)
	private Long id;
	@AutoDocField(note = "育婴顾问工号", nullable = true)
	private Long salesAccountNo;
	@AutoDocField(note = "育婴顾问姓名", nullable = true)
	private String salesName;
	@AutoDocField(note = "工作日期", nullable = true)
	private Date companyDate;
	@AutoDocField(note = "sa编号", nullable = true)
	private String saCode;
	@AutoDocField(note = "sa名称", nullable = true)
	private String saName;
	@AutoDocField(note = "微信会员", nullable = true)
	private Long weixinCustomerCount;
	@AutoDocField(note = "到店数", nullable = true)
	private Long arriveShopCount;
	@AutoDocField(note = "sa新客数", nullable = true)
	private Long saNewCustomerCount;
	@AutoDocField(note = "备注", nullable = true)
	private String remarks;
	@AutoDocField(note = "时段（0:上午、1:下午）", nullable = true)
	private Integer timeBucket;

	@AutoDocField(note = "时段（0:上午、1:下午）", nullable = true)
	private String timeBucketStr;
	@AutoDocField(note = "大区", nullable = true)
	private String areaname;
	@AutoDocField(note = "办事处", nullable = true)
	private String officename;
	@AutoDocField(note = "考核月", nullable = true)
	private String month;
	@AutoDocField(note = "渠道", nullable = true)
	private String channel;
	@AutoDocField(note = "排班状态", nullable = true)
	private String status;
	@AutoDocField(note = "序号", nullable = true)
	private Long serialNumber;

	@AutoDocField(note = "搜索条件：大区编号", nullable = true)
	private String areaCode;
	@AutoDocField(note = "搜索条件：办事处编号", nullable = true)
	private String officeCode;
	@AutoDocField(note = "搜索条件：开始月份", nullable = true)
	private String startMonth;
	@AutoDocField(note = "搜索条件：结束月份", nullable = true)
	private String endMonth;

	@AutoDocField(note = "事业部", nullable = true)
	private String buCode;



}