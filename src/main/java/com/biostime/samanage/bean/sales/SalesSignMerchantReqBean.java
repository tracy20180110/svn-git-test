package com.biostime.samanage.bean.sales;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA门店信息查询ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SalesSignMerchantReqBean extends BaseBean {

    private static final long serialVersionUID = 5776871305606941480L;

    @AutoDocField(note="大区办事处")
    private String departmentCode;

    @AutoDocField(note="渠道")
    private String channelCode;

    @AutoDocField(note="打卡人")
    private String createdBy;

    @AutoDocField(note="打卡人名称")
    private String createdName;

    @AutoDocField(note="类型（1：上班，2：下班）")
    private Integer type;

    @AutoDocField(note="经度")
    private String longitude;

    @AutoDocField(note="纬度")
    private String latitude;

    @AutoDocField(note="打卡终端")
    private String terminalCode;

    @AutoDocField(note="是否在一致")
    private Integer status;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
