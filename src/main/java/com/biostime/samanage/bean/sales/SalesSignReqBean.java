package com.biostime.samanage.bean.sales;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA门店信息查询ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SalesSignReqBean extends BaseBean {

    private static final long serialVersionUID = 1421319531148726264L;
    @AutoDocField(note="大区办事处")
    private String areaOfficeCode;

    @AutoDocField(note="渠道")
    private String channelCode;

    @AutoDocField(note="打卡人")
    private String createdBy;

    @AutoDocField(note="打卡开始日期")
    private String startDate;

    @AutoDocField(note="打卡结束日期")
    private String endDate;

    @AutoDocField(note="事业部编码")
    private String buCode;

    private int pageNo = 1;// 分页参数
    private int pageSize = 20;// 默认每页20条
}
