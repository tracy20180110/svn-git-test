package com.biostime.samanage.bean.sales;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:工作排班
 * Date: 2019-03-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class SchedulingManageResBean extends BaseBean {


    private static final long serialVersionUID = -4207436329299040797L;
    @AutoDocField(note="渠道")
    private String channelName;

    @AutoDocField(note="排班人工号")
    private String createdBy;

    @AutoDocField(note="排班人名称")
    private String createdName;

    @AutoDocField(note="手机号码")
    private String mobile;
}