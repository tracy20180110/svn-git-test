package com.biostime.samanage.bean.sales;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:工作排班
 * Date: 2019-03-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class SchedulingReqBean extends BaseBean {

    private static final long serialVersionUID = -426219014610233493L;

    private String id;

    /**状态 1：上班、2：本休、3：调休、4请假 */
    private Integer	status;

    /** 排班日期 */
    private String schedulingDate;

    /** 备注 */
    private String memo;

    /** 创建者 */
    private String createdBy;

    /** 创建者名称 */
    private String createdName;

    /** 创建时间 */
    private String createdTime;

    //排班月份
    private String month;

    //排班具体时间
    private String schedulingTime;

    //门店编码
    private String terminalCode;
}
