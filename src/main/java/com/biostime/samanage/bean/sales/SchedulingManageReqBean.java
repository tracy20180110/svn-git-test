package com.biostime.samanage.bean.sales;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:工作排班
 * Date: 2019-03-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Data
public class SchedulingManageReqBean extends BaseBean {


    private static final long serialVersionUID = -1607213766648562821L;
    @AutoDocField(note="大区办事处")
    private String areaOfficeCode;

    @AutoDocField(note="渠道")
    private String channelCode;

    @AutoDocField(note="排班人工号")
    private String createdBy;

    @AutoDocField(note="登陆人工号")
    private String account;

    @AutoDocField(note="门店编码")
    private String terminalCode;

    @AutoDocField(note="开始日期")
    private String startDate;

    @AutoDocField(note="结束日期")
    private String endDate;

    @AutoDocField(note="事业部编码")
    private String buCode;

}