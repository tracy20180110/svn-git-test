package com.biostime.samanage.bean.sales;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:工作排班
 * Date: 2019-03-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SchedulingResBean extends BaseBean {

    private static final long serialVersionUID = 1421319531148726264L;

    @AutoDocField(note="排班ID")
    private String id;

    @AutoDocField(note="排班日期")
    private String schedulingDate;

    @AutoDocField(note="备注")
    private String memo;

    @AutoDocField(note="状态编码")
    private String statusCode;

    @AutoDocField(note="状态名称")
    private String statusName;

    private List<BaseKeyValueBean> terminalInfo;

}
