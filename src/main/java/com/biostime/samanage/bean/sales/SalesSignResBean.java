package com.biostime.samanage.bean.sales;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA门店信息查询ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SalesSignResBean extends BaseBean {

    private static final long serialVersionUID = -5149260116528854154L;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="渠道")
    private String channelName;

    @AutoDocField(note="创建人")
    private String createdBy;

    @AutoDocField(note="创建人名称")
    private String createdName;

    //第一门店
    @AutoDocField(note="上班打卡时间")
    private String signTimeUp;

    @AutoDocField(note="上班打卡门店编号")
    private String terminalCodeUp;

    @AutoDocField(note="上班打卡门店名称")
    private String terminalNameUp;

    @AutoDocField(note="上班位置一致性")
    private String statusUp;

    @AutoDocField(note="下班打卡时间")
    private String signTimeDown;

    @AutoDocField(note="下班位置一致性")
    private String statusDown;

    //第二门店
    @AutoDocField(note="上班打卡时间")
    private String signTimeUpTwo;

    @AutoDocField(note="上班打卡门店编号")
    private String terminalCodeUpTwo;

    @AutoDocField(note="上班打卡门店名称")
    private String terminalNameUpTwo;

    @AutoDocField(note="上班位置一致性")
    private String statusUpTwo;

    @AutoDocField(note="下班打卡时间")
    private String signTimeDownTwo;

    @AutoDocField(note="下班位置一致性")
    private String statusDownTwo;

    //第三门店
    @AutoDocField(note="上班打卡时间")
    private String signTimeUpThree;

    @AutoDocField(note="上班打卡门店编号")
    private String terminalCodeUpThree;

    @AutoDocField(note="上班打卡门店名称")
    private String terminalNameUpThree;

    @AutoDocField(note="上班位置一致性")
    private String statusUpThree;

    @AutoDocField(note="下班打卡时间")
    private String signTimeDownThree;

    @AutoDocField(note="下班位置一致性")
    private String statusDownThree;




}
