package com.biostime.samanage.bean.sign;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

@Data
public class SaSignTerminalResBean extends BaseBean {

    @AutoDocField(note = "打卡门店编号")
    private String terminalCode;

    @AutoDocField(note = "打卡门店名称")
    private String terminalName;

    @AutoDocField(note = "打卡时间")
    private String signTime;

    @AutoDocField(note = "经度")
    private String longitude;

    @AutoDocField(note = "纬度")
    private String latitude;

    @AutoDocField(note = "打卡人")
    private String createdBy;

    @AutoDocField(note = "打卡人名称")
    private String createdName;

    @AutoDocField(note = "上班/下班")
    private String type;

    @AutoDocField(note = "门店1/门店2")
    private String terminalNum;

    @AutoDocField(note="位置一致性")
    private String status;
}
