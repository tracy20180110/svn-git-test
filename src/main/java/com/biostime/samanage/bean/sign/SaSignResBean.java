package com.biostime.samanage.bean.sign;

import com.biostime.autodoc.annotations.AutoDocField;
import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA门店信息查询ReqBean
 * Date: 2017-02-08
 * Time: 16:55
 * User: 12804
 * Version:1.0
 */
@Data
public class SaSignResBean extends BaseBean {

    private static final long serialVersionUID = -5149260116528854154L;

    @AutoDocField(note="大区名称")
    private String areaName;

    @AutoDocField(note="办事处名称")
    private String officeName;

    @AutoDocField(note="渠道")
    private String channelName;

    @AutoDocField(note="创建人")
    private String createdBy;

    @AutoDocField(note="创建人名称")
    private String createdName;

    @AutoDocField(note="上班打卡时间1")
    private String startTime;

    @AutoDocField(note="上班打卡门店编号1")
    private String terminalCodeOn;

    @AutoDocField(note="上班打卡门店名称1")
    private String terminalNameOn;

    @AutoDocField(note="上班位置一致性1")
    private String statusOn;

    @AutoDocField(note="下班打卡时间1")
    private String endTime;

    @AutoDocField(note="上班打卡门店编号1")
    private String terminalCodeOff;

    @AutoDocField(note="上班打卡门店名称1")
    private String terminalNameOff;

    @AutoDocField(note="上班位置一致性1")
    private String statusOff;

    @AutoDocField(note="上班打卡时间2")
    private String startTime2;

    @AutoDocField(note="上班打卡门店编号2")
    private String terminalCodeOn2;

    @AutoDocField(note="上班打卡门店名称2")
    private String terminalNameOn2;

    @AutoDocField(note="上班位置一致性2")
    private String statusOn2;

    @AutoDocField(note="下班打卡时间2")
    private String endTime2;

    @AutoDocField(note="上班打卡门店编号2")
    private String terminalCodeOff2;

    @AutoDocField(note="上班打卡门店名称2")
    private String terminalNameOff2;

    @AutoDocField(note="上班位置一致性2")
    private String statusOff2;

    @AutoDocField(note="上班时长")
    private String workingTime;

}
