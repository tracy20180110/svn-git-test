package com.biostime.samanage.bean.quota;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * SA门店
 */
@Data
public class SaQuotaTerminalBean extends BaseBean {
    /**
     * 门店编号
     */
    private String terminalCode;
    /**
     * 门店名称
     */
    private String terminalName;
    /**
     * 连锁编号
     */
    private String chainCode;
    /**
     * 连锁名称
     */
    private String chainName;
}
