package com.biostime.samanage.bean.quota;

import com.biostime.common.bean.BaseBean;
import lombok.Data;

/**
 * 双SA指标
 */
@Data
public class DoubleSaQuotaBean extends BaseBean {
    /**
     * 妈妈奶粉新客
     */
    private Long motherMilkNewCust;
    /**
     * 婴儿奶粉新客（含HT）
     */
    private Long babyMilkNewCust;
    /**
     * Dodie纸尿裤
     */
    private Long DodieDiaperNewCust;
    /**
     * 益生菌新客
     */
    private Long probioticsNewCust;
    /**
     * 进店数
     */
    private Long totalCust;
    /**
     * 当前与SA绑定门店总积分
     */
    private Long totalPoint;

    /**
     * 连锁门店妈妈奶粉新客
     */
    private Long chainMotherMilkNewCust;
    /**
     * 连锁门店婴儿奶粉新客（含HT）
     */
    private Long chainBabyMilkNewCust;
    /**
     * 连锁门店Dodie纸尿裤
     */
    private Long chainDodieDiaperNewCust;
    /**
     * 连锁门店益生菌新客
     */
    private Long chainProbioticsNewCust;
    /**
     * 连锁门店进店数
     */
    private Long chainTotalCust;
    /**
     * 当前与SA绑定连锁门店总积分
     */
    private Long chainTotalPoint;
}
