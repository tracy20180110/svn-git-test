package com.biostime.samanage.domain.splitflow;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 育婴顾问与会员一对一关系
 */
@Entity
@Table(name = "bst_oto_cust_sa", schema = "mama100_owner")
@Data
public class BstOtoCustSa extends BaseDomain {

    public static final String KEY = "BST_OTO_CUST_SA";
    private static final long serialVersionUID = -4498714010828839760L;

    /**
     * ID
     */
    @Id
    @Column(name = "id")
    private Long Id;
    /**
     * 会员ID
     */
    @Column(name = "customer_id")
    private Long customerId;
    /**
     * 育婴顾问/促销员/品牌大使/推广员/
     */
    @Column(name = "relate_code")
    private String relateCode;
    /**
     * 促销员状态  0-未启用 1-在职 2-离职 3-产假 7-事假 5-休假 门店状态：01 正常 02 撤销 03 测试 04 冻结 05 非会员店
     */
    @Column(name = "status")
    private String status;
    /**
     * 促销员状态是否有效  0-无效 1-有效
     */
    @Column(name = "is_valid")
    private String isValid;
    /**
     * 是否新客  0-非新客 1-新客
     */
    @Column(name = "is_newcust")
    private String isNewcust;
    /**
     * 关系类型1.与育婴顾问间关系  2.与促销员关系  3.与品牌大使关系  4.与推广员关系
     */
    @Column(name = "relate_type")
    private String relateType;
    /**
     * 新客时间
     */
    @Column(name = "newcust_time")
    private Date newcustTime;
    /**
     * 关系创建时间
     */
    @Column(name = "created_time", insertable = false)
    private Date createdTime;
    /**
     * 关系更新时间
     */
    @Column(name = "updated_time", insertable = false)
    private Date updatedTime;
    /**
     * 维护类型1.自行维护  2.推送门店
     */
    @Column(name = "maintenance_type")
    private String maintenanceType;
    /**
     * 门店编号
     */
    @Column(name = "terminal_code")
    private String terminalCode;
    /**
     * 创建人
     */
    @Column(name = "created_by")
    private String createdBy;
    /**
     * sa编号
     */
    @Column(name = "sa_code")
    private String saCode;
    /**
     * 更新人
     */
    @Column(name = "updated_by")
    private String updatedBy;
    /**
     * 推送至门店券使用时间所在考核月的起始时间
     */
    @Column(name = "assessmonth_start")
    private String assessmonthStart;
    /**
     * 推送至门店券使用时间所在考核月的截止时间
     */
    @Column(name = "assessmonth_end")
    private String assessmonthEnd;
    /**
     * 发券终端sa_code
     */
    @Column(name = "send_coupon_terminal")
    private String sendCouponTerminal;
    /**
     * SA关联连锁门店
     */
    @Column(name = "RELATION_CHAIN_CODE")
    private String relationChainCode;

}