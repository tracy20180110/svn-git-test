package com.biostime.samanage.domain;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by zhuhaitao on 2017/2/23.
 */
@Entity
@Table(name = "COMMON_DEPARTMENT")
@Data
public class CommonDepartment extends BaseDomain {
    @Id
    @Column(name = "ID", length = 20)
    private Long id;
    @Column(name = "CODE", length = 10)
    private String code;
    @Column(name = "PARENT_ID", length = 20)
    private Long parentId;
    @Column(name = "NAME", length = 40)
    private String name;
    @Column(name = "TIER", length = 1)
    private Integer tier;
    @Column(name = "IS_END", length = 1)
    private Integer isEnd;
    @Column(name = "LOGISTICS_CODE", length = 20)
    private String logisticsCode;
    @Column(name = "REMARK", length = 600)
    private String remark;
    @Column(name = "CREATED_BY", length = 10)
    private String createdBy;
    @Column(name = "CREATED_TIME", length = 7)
    private Date createdTime;
    @Column(name = "UPDATED_BY", length = 10)
    private String updatedBy;
    @Column(name = "UPDATED_TIME", length = 7)
    private Date updatedTime;
    @Column(name = "SHORT_NAME", length = 7)
    private String shortName;
}
