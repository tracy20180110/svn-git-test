package com.biostime.samanage.domain;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 类功能描述: SA字典设置
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_CONFIG", schema = "mama100_owner")
@Data
public class SaConfig extends BaseDomain {

	public static final String KEY = "mkt_sa_config";

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**
	 * 编码
	 */
	@Column(name = "CODE")
	private Long code;

	/**
	 * 名称
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * 关联表ID
	 */
	@Column(name = "TAB_ID")
	private Long tabId;

	/**
	 * 功能模块类型
	 */
	@Column(name = "TYPE")
	private Long type;
}