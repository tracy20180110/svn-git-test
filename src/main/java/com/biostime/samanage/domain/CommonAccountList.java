package com.biostime.samanage.domain;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zhuhaitao on 2017/2/22.
 */
@Entity
@Table(name = "COMMON_ACCOUNT_LIST", schema = "bst_mdm")
@Data
public class CommonAccountList extends BaseDomain {
    @Id
    @Column(name = "ID", length = 20)
    private Long id;
    @Column(name = "account_id", length = 100)
    private String accountId;
    @Column(name = "department_code", length = 100)
    private String departmentCode;

}
