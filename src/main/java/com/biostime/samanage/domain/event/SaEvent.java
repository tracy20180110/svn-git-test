package com.biostime.samanage.domain.event;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
/**
 * Describe:FTF课程
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_EVENT", schema = "MAMA100_OWNER")
@Data
public class SaEvent {
    public static final String KEY = "mkt_sa_event";
    private static final long serialVersionUID = -8692552592028591964L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** 会员ID*/
    @Column(name = "CUSTOMER_ID")
    private Long customerId;

    /*** SA编码*/
    @Column(name = "SA_CODE")
    private String saCode;

    /*** 系统来源*/
    @Column(name = "SYSTEM_SOURCE")
    private Long systemSource;

    /**来源的具体业务类型*/
    @Column(name = "SRC_TYPE")
    private Long	srcType;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;
}
