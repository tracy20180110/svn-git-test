package com.biostime.samanage.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * 会员事件明细记录
 */
@Entity
@Table(name = "HP_MEMBER_EVENT_REC")
@Data
public class HpMemberEventRec implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HP_MEMBER_EVENT_REC_SEQ")
    @SequenceGenerator(name = "HP_MEMBER_EVENT_REC_SEQ", sequenceName = "HP_MEMBER_EVENT_REC_SEQ", allocationSize = 1)
    private Integer id;


    @Column(name = "AREA_CODE")
    private String areaCode;


    @Column(name = "AREA_NAME")
    private String areaName;

    @Column(name = "OFFICE_CODE")
    private String officeCode;

    @Column(name = "OFFICE_NAME")
    private String officeName;

    /**
     * 明细类型 1-新会员 2-新客户
     */
    @Column(name = "rec_type")
    private int recType;

    /**
     * 记录操作来源 1-SA-POS机 2-会员通 3-普通POS机
     */
    @Column(name = "REC_SRC_TYPE")
    private int recSrcType;


    /**
     * 医生编号
     */
    @Column(name = "doctor_code")
    private String doctorCode;

    /**
     * 医生姓名
     */
    @Column(name = "doctor_name")
    private String doctorName;

    /**
     * PRO所属的BCC编号
     */
    @Column(name = "doctor_bcc")
    private String doctorBcc;


    /**
     * PRO所属的BCC姓名
     */
    @Column(name = "doctor_bcc_name")
    private String doctorBccName;

    /**
     * 实际操作的BCC编号
     */
    @Column(name = "OPERATE_BCC")
    private String operateBcc;

    /**
     * 实际操作的BCC姓名
     */
    @Column(name = "OPERATE_BCC_NAME")
    private String operateBccName;

    /**
     * 是否是新创建的会员
     */
    @Column(name = "is_new_customer")
    private Boolean newCustomer;

    /**
     * 会员ID  对应CRM_CUSTOMER表的ID
     */
    @Column(name = "customer_id")
    private Integer customerId;

    /**
     * 会员卡号
     */
    @Column(name = "card_no")
    private String cardNo;

    /**
     * 卡盘给医生的操作员编号
     */
    @Column(name = "SEND_DOCTOR_BY")
    private String sendDoctorBy;

    /**
     * 卡发送医生时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SEND_DOCTOR_TIME")
    private Date sendDoctorTime;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 电话
     */
    private String phone;

    /**
     * 省份
     */
    private String province;

    /**
     * 市或县
     */
    private String city;

    /**
     * 邮编
     */
    private String postcode;

    /**
     * 地址
     */
    private String address;

    /**
     * 预产期
     */
    @Column(name = "estimated_date")
    private String estimatedDate;

    /**
     * 孩子姓名
     */
    @Column(name = "kid_name")
    private String kidName;

    /**
     * 孩子生日
     */
    @Column(name = "kid_birthday")
    private Date kidBirthday;

    /**
     * 得悉途径编号
     * CF_ZY - 产科住院部
     * EK_ZY - 儿科住院部
     * CK_MZ - 产科门诊
     * EK_MZ - 儿科门诊
     * EBK_MZ - 儿保科门诊
     * YH_B - 孕妇班
     * MM_B - 妈妈班
     * YY_G - 游泳馆
     */
    @Column(name = "src_loc_code")
    private String srcLocCode;

    /**
     * 得悉途径名称
     */
    @Column(name = "SRC_LOC_NAME")
    private String srcLocName;

    @Column(name = "TERMINAL_CODE")
    private String terminalCode;

    @Column(name = "TERMINAL_NAME")
    private String terminalName;


    // ================== 新会员 ===================

    /**
     * 名单类别编号
     */
    @Column(name = "member_type_code")
    private String memberTypeCode;

    /**
     * 名单类别
     */
    @Column(name = "member_type")
    private String memberType;

    /**
     * 妈妈班活动主题
     */
    @Column(name = "mclass_act_theme")
    private String mclassActTheme;

    /**
     * 妈妈班活动地点
     */
    @Column(name = "mclass_act_loc")
    private String mclassActLoc;

    /**
     * 礼包类别编号
     */
    @Column(name = "gift_type_code")
    private String giftTypeCode;

    /**
     * 礼包类别名称
     */
    @Column(name = "GIFT_TYPE_NAME")
    private String giftTypeName;

    /**
     * 礼包分类编号
     */
    @Column(name = "gift_cat_code")
    private String giftCatCode;


    // ================== 新会员 END ===================

    // ================== 新客户 ===================

    /**
     * 序列号
     */
    @Column(name = "serial_no")
    private String serialNo;

    /**
     * 防伪码
     */
    @Column(name = "security_no")
    private String securityNo;

    /**
     * 购买地点
     */
    @Column(name = "buy_loc")
    private String buyLoc;

    /**
     * 购买的终端编号
     */
    @Column(name = "buy_terminal_code")
    private String buyTerminalCode;

    /**
     * 购买的终端名称
     */
    @Column(name = "buy_terminal_name")
    private String buyTerminalName;

    /**
     * 产品名称
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * 新客户审核结果1级编码
     */
    @Column(name = "RESULT_CODE")
    private String resultCode;

    /**
     * 新客户审核结果2级编码
     */
    @Column(name = "RESULT_CODE_2")
    private String resultCode2;

    /**
     * 新客户审核结果 0-未审核 1-有效名单 2-无效名单 3-退回修改
     * (新客户导入)
     */
    @Column(name = "C_APPROVE_RESULT")
    private Integer approveResult;

    /**
     * 无效新客户名单原因  1-非首次购买 2-网店购买 3-故意造假 4-其它   (新客户导入)
     * (新客户导入)
     */
    @Column(name = "C_INVALID_REASON")
    private Integer invalidReson;

    /**
     * 审核结果备注
     */
    @Column(name = "APPROVE_MEMO")
    private String approveMemo;

    /**
     * 审核时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "APPROVED_TIME")
    private Date approvedTime;

    /**
     * 退回修改时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RETURN_EDIT_TIME")
    private Date returnEditTime;

    /**
     * 最后一次审核时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_APPROVED_TIME")
    private Date lastApprovedTime;

    /**
     * 系统自动更改状态时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SYSTEM_UPDATED_TIME")
    private Date systemUpdatedTime;

    /**
     * 审核人员账号
     */
    private String approver;


    // ================== 新客户 END ===================


    /**
     * 备注
     */
    private String memo;

    /**
     * 创建时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /**
     * 会员事件时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "EVENT_TIME")
    private Date eventTime;

    /**
     * 记录修改时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    /**
     * 对应的POS机流水号 (仅POS机的记录使用)
     */
    @Column(name = "POS_SEQ_NO")
    private String posSeqNo;

    /**
     * 对应的POS机序列号 (仅POS机的记录使用)
     */
    @Column(name = "POS_SERIAL_NO")
    private String posSerialNo;

    /**
     * 操作员账号 记录导入时填入
     */
    private String operator;

    /**
     * 批处理ID<br>
     * 关联 HYT_COMMON_BATCH_FILE_REC 的 ID
     */
    @Column(name = "batch_id")
    private Integer batchId;

}
