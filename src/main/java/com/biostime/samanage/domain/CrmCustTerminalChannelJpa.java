package com.biostime.samanage.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * CRM客户加入的终端渠道
 * @author Jimmy
 */
@Entity
@Table(name = "CRM_CUST_TERMINAL_CHANNEL")
public class CrmCustTerminalChannelJpa implements Serializable {

	@Id
	@TableGenerator(
			name = "cust_channel_gen", 
			table = "crm_id_generator", 
            pkColumnName = "type",
            valueColumnName = "sequence",
            pkColumnValue = "customer_terminal_channel",
			allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "cust_channel_gen")
	private Integer id;	
	
	/**
	 * 会员<br>
	 * 关联 CRM_CUSTOMER 表的ID 
	 */
	@Column(name = "CUSTOMER_ID")
	private Integer customerId;
	
	/**
	 * 终端渠道编码
	 */
	@Column(name = "TERMINAL_CHANNEL_CODE")
	private String channelCode;
	
	/**
	 * 终端编号
	 */
	@Column(name = "terminal_code")
	private String terminalCode;
	
	/**
	 * 首次交易日期
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FIRST_DEAL_DATE")
	private Date firstDealDate;

	/**
	 * 录入人
	 */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/**
	 * 录入时间
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIMESTAMP")
	private Date createdTime;

	/**
	 * 修改人
	 */
	@Column(name = "UPDATED_BY")
	private String updatedBy;

	/**
	 * 修改时间
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_TIMESTAMP")
	private Date updatedTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	
	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public Date getFirstDealDate() {
		return firstDealDate;
	}

	public void setFirstDealDate(Date firstDealDate) {
		this.firstDealDate = firstDealDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	
}
