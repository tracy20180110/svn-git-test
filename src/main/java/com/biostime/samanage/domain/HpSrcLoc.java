package com.biostime.samanage.domain;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zhuhaitao on 2017/2/23.
 */
@Entity
@Table(name = "HP_SRC_LOC")
@Data
public class HpSrcLoc extends BaseDomain {
    @Id
    @Column(name = "ID", length = 20)
    private Long id;
    @Column(name = "CODE", length = 20)
    private String code;
    @Column(name = "NAME", length = 20)
    private String name;
    @Column(name = "CRM_CODE", length = 20)
    private String crmCode;
}
