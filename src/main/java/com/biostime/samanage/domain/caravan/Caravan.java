package com.biostime.samanage.domain.caravan;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:大小篷车
 * Date: 2017-12-13
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_CARAVAN", schema = "MAMA100_OWNER")
@Data
public class Caravan extends BaseDomain {

    public static final String KEY = "mkt_sa_caravan";
    private static final long serialVersionUID = -509568914199552978L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** 申请类型（1：小篷车：2：大篷车）*/
    @Column(name = "CARAVAN_TYPE")
    private Long caravanType;

    /** 门店编码 */
    @Column(name = "TERMINAL_CODE")
    private String terminalCode;

    /**申请状态（1：新增，2：更换，3：暂停，4：恢复,5:修改）*/
    @Column(name = "STATUS")
    private Long status;

    /** 发货方式（1：当地制作：2：总部发货） */
    @Column(name = "DELIVERY_MODE")
    private Long deliveryMode;

    /** 摆放地点 */
    @Column(name = "PUT_ADDRESS")
    private String putAddress;

    /** 摆放尺寸 */
    @Column(name = "PUT_SIZE")
    private String putSize;

    /** 物料类型（1：儿科：2：产科） */
    @Column(name = "CMATERIaL_TYPE")
    private Long cmaterialType;

    /** 费用类型（1：总部承担：2：办事处/大区承担） */
    @Column(name = "COST_TYPE")
    private Long costType;

    /** 微信会员量 */
    @Column(name = "WX_CUSTOMER")
    private Long wxCustomer;

    /** 新客 */
    @Column(name = "NEW_CUSTOMER")
    private Long newCustomer;

    /** 有效新客 */
    @Column(name = "EFFECTIVE_NEW_CUSTOMER")
    private Long effectiveNewCustomer;

    /** 项目合作 */
    @Column(name = "PROJECT_COOPERATION")
    private String projectCooperation;

    /** 省 */
    @Column(name = "PROVINCE")
    private String province;

    /** 市 */
    @Column(name = "CITY")
    private String city;

    /** 区/县 */
    @Column(name = "DISTRICT")
    private String district;

    /** 详细地址,不包括省市区信息 */
    @Column(name = "ADDRESS")
    private String address;

    /** 收货人 */
    @Column(name = "CONSIGNEE")
    private String consignee;

    /** 联系电话 */
    @Column(name = "CONTACT_PHONE")
    private String contactPhone;

    /** 备注 */
    @Column(name = "MEMO")
    private String memo;

    @Column(name = "PROCEDURE_STATUS")
    private Long procedureStatus;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    /** OA流程编号 */
    @Column(name = "OA_CODE")
    private String oaCode;

    /** 分销订单编号 */
    @Column(name = "ORDER_ID")
    private String orderId;

    /** 上传协议 */
    @Column(name = "FILE_UPLOAD")
    private String fileUpload;

    /** 文件名称 */
    @Column(name = "FILE_NAME")
    private String fileName;
}
