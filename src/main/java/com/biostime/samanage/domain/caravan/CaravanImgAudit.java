package com.biostime.samanage.domain.caravan;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:大小篷车图片审核
 * Date: 2017-12-13
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_CARAVAN_IMG_AUDIT", schema = "MAMA100_OWNER")
@Data
public class CaravanImgAudit extends BaseDomain {

    public static final String KEY = "mkt_sa_caravan_img_audit";
    private static final long serialVersionUID = -509568914199552978L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** 大小篷车ID*/
    @Column(name = "CARAVAN_ID")
    private Long caravanId;

    /**审核状态（1：通过，2：不通过）*/
    @Column(name = "AUDIT_STATUS")
    private Long auditStatus;

    /** 审批备注 */
    @Column(name = "MEMO")
    private String memo;

    /** 图片一 */
    @Column(name = "IMG_URL_ONE")
    private String imgUrlOne;

    /** 图片二 */
    @Column(name = "IMG_URL_TWO")
    private String imgUrlTwo;

    /** 图片三 */
    @Column(name = "IMG_URL_THREE")
    private String imgUrlThree;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;
}
