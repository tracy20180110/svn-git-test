package com.biostime.samanage.domain.caravan;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:大小篷车
 * Date: 2018-04-13
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_CARAVAN_LOG", schema = "MAMA100_OWNER")
@Data
public class CaravanLog extends BaseDomain {

    public static final String KEY = "mkt_sa_caravan_log";
    private static final long serialVersionUID = 6151435454137355097L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /** 路演活动ID */
    @Column(name = "CARAVAN_ID")
    private Long caravanId;

    /** 操作记录（1：新增，2：更换，3：暂停，4：恢复，5：更改申请人） */
    @Column(name = "TYPE")
    private Long type;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;


}
