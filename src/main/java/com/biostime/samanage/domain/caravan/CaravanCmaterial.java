package com.biostime.samanage.domain.caravan;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:大小篷车物料
 * Date: 2017-12-18
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_CARAVAN_CMATERIAL", schema = "MAMA100_OWNER")
@Data
public class CaravanCmaterial extends BaseDomain {

    public static final String KEY = "mkt_sa_caravan_cmaterial";
    private static final long serialVersionUID = -7294248163832765561L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** 物料ID */
    @Column(name = "CMATERIAL_ID", nullable = false)
    private Long cmaterialId;

    /*** 物料名称 */
    @Column(name = "CMATERIAL_NAME")
    private String cmaterialName;

    /*** 物料编码 */
    @Column(name = "CMATERIAL_CODE")
    private String cmaterialCode;

    /*** 单价 */
    @Column(name = "PRICE")
    private Double price;

    /*** 单位 */
    @Column(name = "UNIT")
    private String unit;

    /*** 图片（缩略） */
    @Column(name = "IMG_MIN")
    private String imgMin;

    /*** 图片（原图） */
    @Column(name = "IMG_BIG")
    private String imgBig;

    /*** 到货时间 */
    @Column(name = "ARRIVAL_GOODS_TIME")
    private Date arrivalGoodsSime;

    /*** NC转换比例 */
    @Column(name = "NC_CHANGE")
    private Long ncChange;

    /*** 数量 */
    @Column(name = "NUM")
    private Long num;

    /*** nc转换后的数量 */
    @Column(name = "NCCHANGE_NUM")
    private Long ncchangeNum;

    /*** 下单时的装箱规格 */
    @Column(name = "BOX_NORMS")
    private String boxNorms;

    /*** 大小篷车的ID */
    @Column(name = "CARAVAN_ID")
    private Long caravanId;

    /*** 1：儿科, 2：产科 */
    @Column(name = "TYPE")
    private Long type;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /*** 下单方式 */
    @Column(name = "ORDER_TYPE")
    private Long orderType;

    /*** 物料费用单 */
    @Column(name = "COST_ID")
    private String costId;

    /*** 物料费用单名称 */
    @Column(name = "COST_NAME")
    private String costName;
}
