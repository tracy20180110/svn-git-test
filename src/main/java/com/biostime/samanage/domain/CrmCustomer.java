package com.biostime.samanage.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * CrmCustomer entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CRM_CUSTOMER", schema = "MAMA100_OWNER")
public class CrmCustomer implements java.io.Serializable {

	// Fields

	private Long id;
	private Long version;
	private String accountId;
	private String password;
	private String mama100LoginName;
	private String name;
	private String customerCardCode;
	private String statusCode;
	private String gradeCode;
	private String mobilePhone;
	private String phone;
	private String alternatePhone;
	private String regionCode;
	private String departmentCode;
	private String address;
	private String zip;
	private String addressTypeCode;
	private String addressStatusCode;
	private String email;
	private String onLineName;
	private String learnFromChannelCode;
	private Long recommenderId;
	private String recommenderAlternatePhone;
	private String recommenderName;
	private String doctorCode;
	private String hospitalCode;
	private Long terminalId;
	private String terminalCode;
	private String jobTypeCode;
	private String callbackStatusCode;
	private String callbackResultCode;
	private String remark;
	private String kidName;
	private String kidGenderCode;
	private Date kidBirthDate;
	private Date kidExpectedConfinementDate;
	private String kidHealthCode;
	private String relativeGenderCode;
	private Date relativeBirthDate;
	private String relativeHealthCode;
	private String relativeCharacter;
	private String createdBy;
	private Date createdTimestamp;
	private String updatedBy;
	private Date updatedTimestamp;
	private Long totalPoint;
	private Long productPoint;
	private Long gamePoint;
	private Long activityPoint;
	private Long doublePoint;
	private Long exchangedPoint;
	private Long frozenPoint;
	private Long remainingPoint;
	private String applicationSourceCode;
	private String kidNameListStr;
	private String kidBirthDateListStr;
	private String addressType;
	private String gender;
	private String kidGender;
	private String mobileMemoType;
	private Date callbackResultTypeinTime;
	private String callbackResult2Code;
	private Date callbackResult2TypeinTime;
	private String callbackStatus2Code;
	private String provinceCode;
	private String cityCode;
	private String districtCode;
	private String phoneMemoType;
	private String specificNeed;
	private String familyRole;



	// Property accessors
	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 15, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "VERSION", precision = 10, scale = 0)
	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Column(name = "ACCOUNT_ID", length = 20)
	public String getAccountId() {
		return this.accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@Column(name = "PASSWORD", length = 100)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "MAMA100_LOGIN_NAME", length = 50)
	public String getMama100LoginName() {
		return this.mama100LoginName;
	}

	public void setMama100LoginName(String mama100LoginName) {
		this.mama100LoginName = mama100LoginName;
	}

	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "CUSTOMER_CARD_CODE", length = 60)
	public String getCustomerCardCode() {
		return this.customerCardCode;
	}

	public void setCustomerCardCode(String customerCardCode) {
		this.customerCardCode = customerCardCode;
	}

	@Column(name = "STATUS_CODE", length = 20)
	public String getStatusCode() {
		return this.statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	@Column(name = "GRADE_CODE", length = 20)
	public String getGradeCode() {
		return this.gradeCode;
	}

	public void setGradeCode(String gradeCode) {
		this.gradeCode = gradeCode;
	}

	@Column(name = "MOBILE_PHONE", length = 50)
	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	@Column(name = "PHONE", length = 50)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "ALTERNATE_PHONE", length = 120)
	public String getAlternatePhone() {
		return this.alternatePhone;
	}

	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}

	@Column(name = "REGION_CODE", length = 20)
	public String getRegionCode() {
		return this.regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	@Column(name = "DEPARTMENT_CODE", length = 20)
	public String getDepartmentCode() {
		return this.departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	@Column(name = "ADDRESS", length = 300)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "ZIP", length = 20)
	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Column(name = "ADDRESS_TYPE_CODE", length = 20)
	public String getAddressTypeCode() {
		return this.addressTypeCode;
	}

	public void setAddressTypeCode(String addressTypeCode) {
		this.addressTypeCode = addressTypeCode;
	}

	@Column(name = "ADDRESS_STATUS_CODE", length = 20)
	public String getAddressStatusCode() {
		return this.addressStatusCode;
	}

	public void setAddressStatusCode(String addressStatusCode) {
		this.addressStatusCode = addressStatusCode;
	}

	@Column(name = "EMAIL", length = 120)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "ON_LINE_NAME", length = 100)
	public String getOnLineName() {
		return this.onLineName;
	}

	public void setOnLineName(String onLineName) {
		this.onLineName = onLineName;
	}

	@Column(name = "LEARN_FROM_CHANNEL_CODE", length = 20)
	public String getLearnFromChannelCode() {
		return this.learnFromChannelCode;
	}

	public void setLearnFromChannelCode(String learnFromChannelCode) {
		this.learnFromChannelCode = learnFromChannelCode;
	}

	@Column(name = "RECOMMENDER_ID", precision = 15, scale = 0)
	public Long getRecommenderId() {
		return this.recommenderId;
	}

	public void setRecommenderId(Long recommenderId) {
		this.recommenderId = recommenderId;
	}

	@Column(name = "RECOMMENDER_ALTERNATE_PHONE", length = 120)
	public String getRecommenderAlternatePhone() {
		return this.recommenderAlternatePhone;
	}

	public void setRecommenderAlternatePhone(String recommenderAlternatePhone) {
		this.recommenderAlternatePhone = recommenderAlternatePhone;
	}

	@Column(name = "RECOMMENDER_NAME", length = 100)
	public String getRecommenderName() {
		return this.recommenderName;
	}

	public void setRecommenderName(String recommenderName) {
		this.recommenderName = recommenderName;
	}

	@Column(name = "DOCTOR_CODE", length = 20)
	public String getDoctorCode() {
		return this.doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	@Column(name = "HOSPITAL_CODE", length = 20)
	public String getHospitalCode() {
		return this.hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	@Column(name = "TERMINAL_ID", precision = 15, scale = 0)
	public Long getTerminalId() {
		return this.terminalId;
	}

	public void setTerminalId(Long terminalId) {
		this.terminalId = terminalId;
	}

	@Column(name = "TERMINAL_CODE", length = 20)
	public String getTerminalCode() {
		return this.terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	@Column(name = "JOB_TYPE_CODE", length = 20)
	public String getJobTypeCode() {
		return this.jobTypeCode;
	}

	public void setJobTypeCode(String jobTypeCode) {
		this.jobTypeCode = jobTypeCode;
	}

	@Column(name = "CALLBACK_STATUS_CODE", length = 20)
	public String getCallbackStatusCode() {
		return this.callbackStatusCode;
	}

	public void setCallbackStatusCode(String callbackStatusCode) {
		this.callbackStatusCode = callbackStatusCode;
	}

	@Column(name = "CALLBACK_RESULT_CODE", length = 20)
	public String getCallbackResultCode() {
		return this.callbackResultCode;
	}

	public void setCallbackResultCode(String callbackResultCode) {
		this.callbackResultCode = callbackResultCode;
	}

	@Column(name = "REMARK", length = 2000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "KID_NAME", length = 100)
	public String getKidName() {
		return this.kidName;
	}

	public void setKidName(String kidName) {
		this.kidName = kidName;
	}

	@Column(name = "KID_GENDER_CODE", length = 20)
	public String getKidGenderCode() {
		return this.kidGenderCode;
	}

	public void setKidGenderCode(String kidGenderCode) {
		this.kidGenderCode = kidGenderCode;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "KID_BIRTH_DATE", length = 7)
	public Date getKidBirthDate() {
		return this.kidBirthDate;
	}

	public void setKidBirthDate(Date kidBirthDate) {
		this.kidBirthDate = kidBirthDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "KID_EXPECTED_CONFINEMENT_DATE", length = 7)
	public Date getKidExpectedConfinementDate() {
		return this.kidExpectedConfinementDate;
	}

	public void setKidExpectedConfinementDate(Date kidExpectedConfinementDate) {
		this.kidExpectedConfinementDate = kidExpectedConfinementDate;
	}

	@Column(name = "KID_HEALTH_CODE", length = 50)
	public String getKidHealthCode() {
		return this.kidHealthCode;
	}

	public void setKidHealthCode(String kidHealthCode) {
		this.kidHealthCode = kidHealthCode;
	}

	@Column(name = "RELATIVE_GENDER_CODE", length = 20)
	public String getRelativeGenderCode() {
		return this.relativeGenderCode;
	}

	public void setRelativeGenderCode(String relativeGenderCode) {
		this.relativeGenderCode = relativeGenderCode;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "RELATIVE_BIRTH_DATE", length = 7)
	public Date getRelativeBirthDate() {
		return this.relativeBirthDate;
	}

	public void setRelativeBirthDate(Date relativeBirthDate) {
		this.relativeBirthDate = relativeBirthDate;
	}

	@Column(name = "RELATIVE_HEALTH_CODE", length = 20)
	public String getRelativeHealthCode() {
		return this.relativeHealthCode;
	}

	public void setRelativeHealthCode(String relativeHealthCode) {
		this.relativeHealthCode = relativeHealthCode;
	}

	@Column(name = "RELATIVE_CHARACTER", length = 120)
	public String getRelativeCharacter() {
		return this.relativeCharacter;
	}

	public void setRelativeCharacter(String relativeCharacter) {
		this.relativeCharacter = relativeCharacter;
	}

	@Column(name = "CREATED_BY", length = 40)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_TIMESTAMP", length = 7)
	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	@Column(name = "UPDATED_BY", length = 40)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "UPDATED_TIMESTAMP", length = 7)
	public Date getUpdatedTimestamp() {
		return this.updatedTimestamp;
	}

	public void setUpdatedTimestamp(Date updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

	@Column(name = "TOTAL_POINT", nullable = false, precision = 10, scale = 0)
	public Long getTotalPoint() {
		return this.totalPoint;
	}

	public void setTotalPoint(Long totalPoint) {
		this.totalPoint = totalPoint;
	}

	@Column(name = "PRODUCT_POINT", nullable = false, precision = 10, scale = 0)
	public Long getProductPoint() {
		return this.productPoint;
	}

	public void setProductPoint(Long productPoint) {
		this.productPoint = productPoint;
	}

	@Column(name = "GAME_POINT", nullable = false, precision = 10, scale = 0)
	public Long getGamePoint() {
		return this.gamePoint;
	}

	public void setGamePoint(Long gamePoint) {
		this.gamePoint = gamePoint;
	}

	@Column(name = "ACTIVITY_POINT", nullable = false, precision = 10, scale = 0)
	public Long getActivityPoint() {
		return this.activityPoint;
	}

	public void setActivityPoint(Long activityPoint) {
		this.activityPoint = activityPoint;
	}

	@Column(name = "DOUBLE_POINT", nullable = false, precision = 10, scale = 0)
	public Long getDoublePoint() {
		return this.doublePoint;
	}

	public void setDoublePoint(Long doublePoint) {
		this.doublePoint = doublePoint;
	}

	@Column(name = "EXCHANGED_POINT", nullable = false, precision = 10, scale = 0)
	public Long getExchangedPoint() {
		return this.exchangedPoint;
	}

	public void setExchangedPoint(Long exchangedPoint) {
		this.exchangedPoint = exchangedPoint;
	}

	@Column(name = "FROZEN_POINT", nullable = false, precision = 10, scale = 0)
	public Long getFrozenPoint() {
		return this.frozenPoint;
	}

	public void setFrozenPoint(Long frozenPoint) {
		this.frozenPoint = frozenPoint;
	}

	@Column(name = "REMAINING_POINT", nullable = false, precision = 10, scale = 0)
	public Long getRemainingPoint() {
		return this.remainingPoint;
	}

	public void setRemainingPoint(Long remainingPoint) {
		this.remainingPoint = remainingPoint;
	}

	@Column(name = "APPLICATION_SOURCE_CODE", length = 20)
	public String getApplicationSourceCode() {
		return this.applicationSourceCode;
	}

	public void setApplicationSourceCode(String applicationSourceCode) {
		this.applicationSourceCode = applicationSourceCode;
	}

	@Column(name = "KID_NAME_LIST_STR", length = 1000)
	public String getKidNameListStr() {
		return this.kidNameListStr;
	}

	public void setKidNameListStr(String kidNameListStr) {
		this.kidNameListStr = kidNameListStr;
	}

	@Column(name = "KID_BIRTH_DATE_LIST_STR", length = 1000)
	public String getKidBirthDateListStr() {
		return this.kidBirthDateListStr;
	}

	public void setKidBirthDateListStr(String kidBirthDateListStr) {
		this.kidBirthDateListStr = kidBirthDateListStr;
	}

	@Column(name = "ADDRESS_TYPE", length = 50)
	public String getAddressType() {
		return this.addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	@Column(name = "GENDER", length = 50)
	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "KID_GENDER", length = 50)
	public String getKidGender() {
		return this.kidGender;
	}

	public void setKidGender(String kidGender) {
		this.kidGender = kidGender;
	}

	@Column(name = "MOBILE_MEMO_TYPE", length = 20)
	public String getMobileMemoType() {
		return this.mobileMemoType;
	}

	public void setMobileMemoType(String mobileMemoType) {
		this.mobileMemoType = mobileMemoType;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CALLBACK_RESULT_TYPEIN_TIME", length = 7)
	public Date getCallbackResultTypeinTime() {
		return this.callbackResultTypeinTime;
	}

	public void setCallbackResultTypeinTime(Date callbackResultTypeinTime) {
		this.callbackResultTypeinTime = callbackResultTypeinTime;
	}

	@Column(name = "CALLBACK_RESULT2_CODE", length = 20)
	public String getCallbackResult2Code() {
		return this.callbackResult2Code;
	}

	public void setCallbackResult2Code(String callbackResult2Code) {
		this.callbackResult2Code = callbackResult2Code;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CALLBACK_RESULT2_TYPEIN_TIME", length = 7)
	public Date getCallbackResult2TypeinTime() {
		return this.callbackResult2TypeinTime;
	}

	public void setCallbackResult2TypeinTime(Date callbackResult2TypeinTime) {
		this.callbackResult2TypeinTime = callbackResult2TypeinTime;
	}

	@Column(name = "CALLBACK_STATUS2_CODE", length = 20)
	public String getCallbackStatus2Code() {
		return this.callbackStatus2Code;
	}

	public void setCallbackStatus2Code(String callbackStatus2Code) {
		this.callbackStatus2Code = callbackStatus2Code;
	}

	@Column(name = "PROVINCE_CODE", length = 20)
	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	@Column(name = "CITY_CODE", length = 20)
	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	@Column(name = "DISTRICT_CODE", length = 20)
	public String getDistrictCode() {
		return this.districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	@Column(name = "PHONE_MEMO_TYPE", length = 20)
	public String getPhoneMemoType() {
		return this.phoneMemoType;
	}

	public void setPhoneMemoType(String phoneMemoType) {
		this.phoneMemoType = phoneMemoType;
	}

	@Column(name = "SPECIFIC_NEED", length = 20)
	public String getSpecificNeed() {
		return this.specificNeed;
	}

	public void setSpecificNeed(String specificNeed) {
		this.specificNeed = specificNeed;
	}

	@Column(name = "FAMILY_ROLE", length = 20)
	public String getFamilyRole() {
		return this.familyRole;
	}

	public void setFamilyRole(String familyRole) {
		this.familyRole = familyRole;
	}

}