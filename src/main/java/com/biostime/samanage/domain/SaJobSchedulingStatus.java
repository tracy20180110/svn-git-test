package com.biostime.samanage.domain;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/** 
* 类功能描述: SA促销员工作排班状态表
*
* @version 1.0
* @author zhuhaitao
* @createDate 2016年6月23日 上午10:59:18
*/
@Entity
@Table(name = "SA_JOB_SCHEDULING_STATUS", schema = "BST_MKT")
@Data
public class SaJobSchedulingStatus extends BaseDomain {
	private static final long serialVersionUID = -489655629817627553L;

	public static final String KEY = "SA_JOB_SCHEDULING_STATUS";

	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 15, scale = 0)
	private Long id;
	@Column(name = "SALES_ACCOUNT_NO", precision = 15, scale = 0)
	private Long salesAccountNo;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "COMPANY_DATE", length = 7)
	private Date companyDate;
	@Column(name = "STATUS", precision = 1, scale = 0)
	private Integer status;
}