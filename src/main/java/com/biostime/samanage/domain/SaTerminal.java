package com.biostime.samanage.domain;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 类功能描述: SA终端信息表
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "crm_terminal")
@Data
public class SaTerminal extends BaseDomain {
	/** 
	* @Fields serialVersionUID : TODO
	*/
	private static final long serialVersionUID = 6078874925848836402L;
	@Id
	@Column(name = "CODE", length = 20)
	private String code;
	@Column(name = "NAME", length = 100)
	private String name;
	@Column(name = "terminal_channel_code", length = 20)
	private String terminalChannelCode;
	@Column(name = "department_code", length = 20)
	private String departmentCode;
}