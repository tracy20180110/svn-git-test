package com.biostime.samanage.domain;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:SA与门店管理
 * Date: 2017-01-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_IMPORT_INFO", schema = "MAMA100_OWNER")
@Data
public class MktImportInfo extends BaseDomain {
    public static final String KEY = "mkt_import_info";
    private static final long serialVersionUID = 170688158957657376L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /**功能模块名称 */
    @Column(name = "TYPE")
    private String	type;

    /** 上传文件路径 */
    @Column(name = "FILE_PATH")
    private String filePath;

    /** 文件名 */
    @Column(name = "FILE_NAME")
    private String fileName;

    /** 总记录数 */
    @Column(name = "TOTAL_COUNT")
    private Integer totalCount;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;
}
