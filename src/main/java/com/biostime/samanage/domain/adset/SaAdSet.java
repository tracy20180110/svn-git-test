package com.biostime.samanage.domain.adset;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:促销员广告图设置
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_AD_SET", schema = "MAMA100_OWNER")
@Data
public class SaAdSet extends BaseDomain {
    public static final String KEY = "mkt_sa_ad_set";
    private static final long serialVersionUID = 871040870695630831L;


    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /**广告类型（0：无门店信息，1：全部门店：2：指定门店） */
    @Column(name = "TYPE")
    private Integer	type;

    /**门店编码*/
    @Column(name = "TERMINAL_CODE")
    private String terminalCode;

    /**0：失效，1：使用中 */
    @Column(name = "STATUS")
    private Integer	status;

    /**广告图片链接*/
    @Column(name = "IMAGE")
    private String image;

    /**广告详情*/
    @Column(name = "AD_INFO")
    private String adInfo;

    /**广告详情类型（1：链接，2：图片） */
    @Column(name = "AD_INFO_TYPE")
    private Integer	adInfoType;

    /**有效期开始时间 */
    @Column(name = "START_DATE")
    private Date startDate;

    /**有效期结束时间 */
    @Column(name = "END_DATE")
    private Date endDate;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;


}
