package com.biostime.samanage.domain;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * 类功能描述: SA工作排班
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "SA_JOB_SCHEDULING", schema = "BST_MKT")
@Data
public class SaJobScheduling extends BaseDomain {
	private static final long serialVersionUID = -489655629817627553L;

	public static final String KEY = "SA_IOB_SCHEDULING";

	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 15, scale = 0)
	private Long id;
	@Column(name = "SALES_ACCOUNT_NO", precision = 15, scale = 0)
	private Long salesAccountNo;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "COMPANY_DATE", length = 7)
	private Date companyDate;
	@Column(name = "SA_CODE", length = 20)
	private String saCode;
	@Column(name = "SA_NAME", length = 100)
	private String saName;
	@Column(name = "WEIXIN_CUSTOMER_COUNT", precision = 10, scale = 0)
	private Long weixinCustomerCount;
	@Column(name = "ARRIVE_SHOP_COUNT", precision = 10, scale = 0)
	private Long arriveShopCount;
	@Column(name = "SA_NEW_CUSTOMER_COUNT", precision = 10, scale = 0)
	private Long saNewCustomerCount;
	@Column(name = "REMARKS", length = 50)
	private String remarks;
	@Column(name = "TIME_BUCKET", precision = 1, scale = 0)
	private Integer timeBucket;
}