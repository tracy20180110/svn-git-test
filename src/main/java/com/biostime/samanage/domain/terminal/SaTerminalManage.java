package com.biostime.samanage.domain.terminal;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:SA与门店管理
 * Date: 2017-01-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_TERMINAL_MANAGE", schema = "MAMA100_OWNER")
@Data
public class SaTerminalManage extends BaseDomain {
    public static final String KEY = "mkt_sa_terminal_manage";
    private static final long serialVersionUID = -1288932571420768139L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /**状态 0-删除 */
    @Column(name = "STATUS")
    private Integer	status;

    /** 门店编码 */
    @Column(name = "TERMINAL_CODE")
    private String terminalCode;

    /** SA编码 */
    @Column(name = "SA_CODE")
    private String saCode;

    /** 开始时间 */
    @Column(name = "START_DATE")
    private Date startDate;

    /*** 结束时间**/
    @Column(name = "END_DATE")
    private Date endDate;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    /** 关联门店编码 */
    @Column(name = "RELATION_TERMINAL_CODE")
    private String relationTerminalCode;

    /** 更新人名称 */
    @Column(name = "UPDATED_NAME")
    private String updatedName;
}
