package com.biostime.samanage.domain.ftf;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:FTF课程
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_FTF_LECTURER", schema = "MAMA100_OWNER")
@Data
public class FtfLecturer extends BaseDomain {
    public static final String KEY = "mkt_sa_ftf_lecturer";
    private static final long serialVersionUID = -8692552592028591964L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** 讲师名称*/
    @Column(name = "NAME")
    private String name;

    /*** 讲师编号*/
    @Column(name = "CODE")
    private String code;

    /*** 讲师类型*/
    @Column(name = "TYPE")
    private String type;

    /**状态 0-停用 1-启用*/
    @Column(name = "STATUS")
    private Integer	status;

    /** 课程简介 */
    @Column(name = "SYNOPS")
    private String synops;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;
}
