package com.biostime.samanage.domain.ftf;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:FTF课程
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_FTF_ACTIVITY", schema = "MAMA100_OWNER")
@Data
public class FtfActivity extends BaseDomain {

    public static final String KEY = "mkt_sa_ftf_activity";

    private static final long serialVersionUID = 6616212671724684666L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** 讲师ID*/
    @Column(name = "LECTURER_ID")
    private Long lecturerId;

    /*** 课程ID*/
    @Column(name = "CURRICULUM_ID")
    private Long curriculumId;

    /*** 品牌**/
    @Column(name = "BRAND")
    private String brand;

    /**活动类型  0:SA-FTF 1-门店-FTF*/
    @Column(name = "TYPE")
    private String type;

    /**状态 0-停用 1-启用*/
    @Column(name = "STATUS")
    private Integer	status;

    /** 大区办事处 */
    @Column(name = "AREA_OFFICE_CODE")
    private String areaOfficeCode;

    /** 门店编码 */
    @Column(name = "TERMINAL_CODE")
    private String terminalCode;

    /** SA编码 */
    @Column(name = "SA_CODE")
    private String saCode;

    /** 省 */
    @Column(name = "PROVINCE")
    private String province;

    /** 市 */
    @Column(name = "CITY")
    private String city;

    /** 区/县 */
    @Column(name = "DISTRICT")
    private String district;

    /** 活动地址 */
    @Column(name = "ADDRESS")
    private String address;

    /** 开始时间 */
    @Column(name = "START_TIME")
    private String startTime;

    /*** 结束时间**/
    @Column(name = "END_TIME")
    private String endTime;

    /**活动日期**/
    @Column(name = "ACT_DATE")
    private Date actDate;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    /**品牌名称 */
    @Column(name = "BRAND_NAME")
    private String brandName;

    /** 讲师头衔 **/
    @Column(name = "LECTURER_RANK")
    private String lecturerRank;

    /** 分享 海报链接 **/
    @Column(name = "POSTER_IMAGE_URL")
    private String posterImageUrl;

    /** 分享文案 **/
    @Column(name = "SHARING_COPYWRITING")
    private String sharingCopywriting;

    /** 是否生成页面 1：是，0：否 **/
    @Column(name = "CREATE_PAGE")
    private Long createPage;

    //2018-12-11
    /**1:院内班,2:酒店班 (当活动类型是SA-FTF才有) **/
    @Column(name = "PLACE_TYPE")
    private Long placeType;

    /**专家支持类型：1：专家类型，2：大会主席，3：会议讲着(当活动类型是学术推广才有2和3) **/
    @Column(name = "EXPERT_TYPE")
    private Long expertType;

    /**活动流程 **/
    @Column(name = "ACTIVITY_FLOW")
    private String activityFlow;

    /** 参与方式 **/
    @Column(name = "PARTICIPATION_MODE")
    private String participationMode;

    /** 礼品图片一 **/
    @Column(name = "GIFT_PICTURES_ONE")
    private String giftPicturesOne;

    /** 礼品图片二 **/
    @Column(name = "GIFT_PICTURES_TWO")
    private String giftPicturesTwo;

    /** 视频链接 */
    @Column(name = "VIDEO_LINK")
    private String videoLink;


    /** 组团类型1:单人，2：双人 **/
    @Column(name = "GROUP_TYPE")
    private Long groupType;

    /**组团人数 **/
    @Column(name = "GROUP_NUMS")
    private String groupNums;

    /** 导购ID **/
    @Column(name = "SHOPPING_GUIDE_ID")
    private String shoppingGuideId;

    /**活动上限团数 **/
    @Column(name = "ACT_GROUP_NUMS")
    private Long actGroupNums;


    //2019.6版本
    /** FTF小程序签到二维码 **/
    @Column(name = "FTF_QR_CODE_URL")
    private String ftfQrCodeUrl;


    /**消费者类型 1：孕妈班，2：宝妈班 **/
    @Column(name = "CONSUMER_TYPE")
    private Long consumerType;

    /**FTF主题：1：全品场FTF，2：Dodie专场FTF，3：羊奶专场FTF **/
    @Column(name = "FTF_THEME")
    private Long ftfTheme;

    /**新客计算方式 **/
    @Column(name = "NEW_CUSTOMER_CALCULATION")
    private Long newCustomerCalculation;

    /**数据来源： 1：网页版，2：营销通APP**/
    @Column(name = "SOURCE")
    private Long source;
}
