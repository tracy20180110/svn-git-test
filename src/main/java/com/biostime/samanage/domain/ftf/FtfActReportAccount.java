package com.biostime.samanage.domain.ftf;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:FTF课程
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Entity
@Table(name = "MKT_SA_ACT_REPORT_ACCOUNT", schema = "MAMA100_OWNER")
@Data
public class FtfActReportAccount extends BaseDomain {
    public static final String KEY = "mkt_sa_act_report_account";
    private static final long serialVersionUID = -3653511184128393782L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** FTF活动ID*/
    @Column(name = "REPORT_ID")
    private Long reportId;

    /*** 讲师名称*/
    @Column(name = "CODE")
    private Long code;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

}
