package com.biostime.samanage.domain.ftf;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "MKT_SA_FTF_ACT_TEMPLATE_INFO", schema = "MAMA100_OWNER")
@Data
public class FtfActivityTemplateInfo extends BaseDomain {

    public static final String KEY = "mkt_sa_ftf_act_template_info";

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** 活动ID*/
    @Column(name = "FTF_ACT_ID")
    private Long ftfActId;

    /*** 模板ID**/
    @Column(name = "ACT_TEMPLATE_ID")
    private Long actTemplateId;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;


    /** 模板内容名称 */
    @Column(name = "TEMPLATE_KEY")
    private String templateKey;


    /** 模板内容值 */
    @Column(name = "TEMPLATE_VALUE")
    private String templateValue;


    /*** log图片**//*
    @Column(name = "LOG_IMG")
    private String logImg;

    *//*** 背景音乐0：不播放、1：播放*//*
    @Column(name = "BG_MUSIC")
    private Long bgMusic;

    *//*** 讲师头像**//*
    @Column(name = "LECTURER_IMG")
    private String lecturerImg;

    *//*** 活动奖品图片1**//*
    @Column(name = "PRIZE_IMG_ONE")
    private String prizeImgOne;

    *//*** 活动奖品图片2**//*
    @Column(name = "PRIZE_IMG_TWO")
    private String prizeImgTwo;

    *//*** 活动奖品图片3**//*
    @Column(name = "PRIZE_IMG_THREE")
    private String prizeImgThree;

    *//*** 活动奖品图片4**//*
    @Column(name = "PRIZE_IMG_FOUR")
    private String prizeImgFour;

    *//*** 活动奖品图片5**//*
    @Column(name = "PRIZE_IMG_FIVE")
    private String prizeImgFive;

    *//*** 活动描述1**//*
    @Column(name = "ACT_DESC_ONE")
    private String actDescOne;

    *//*** 活动描述2**//*
    @Column(name = "ACT_DESC_TWO")
    private String actDescTwo;

    *//*** 奖品领取规则**//*
    @Column(name = "RECEIVE_RULES")
    private String receiveRules;

    *//*** 活动海报**//*
    @Column(name = "ACTIVE_POSTER_URL")
    private String activePosterUrl;*/


}
