package com.biostime.samanage.domain.ftf;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Describe:FTF课程
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Entity
@Table(name = "MKT_SA_FTF_ACT_LECTURER", schema = "MAMA100_OWNER")
@Data
public class FtfActLecturer extends BaseDomain {
    public static final String KEY = "mkt_sa_ftf_act_lecturer";
    private static final long serialVersionUID = -3426448593217335435L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** FTF活动ID*/
    @Column(name = "FTF_ACT_ID")
    private Long ftfActId;

    /*** 讲师名称*/
    @Column(name = "LECTURER_NAME")
    private String lecturerName;

    /*** 讲师编号*/
    @Column(name = "LECTURER_CODE")
    private String lecturerCode;

    /*** 讲师头衔*/
    @Column(name = "LECTURER_RANK")
    private String lecturerRank;

    /*** 2：大会主席，3：会议讲着(当活动类型是学术推广才有2和3)*/
    @Column(name = "EXPERT_TYPE")
    private Long expertType;

    /*** 1:内部讲师、2：外部讲师、3：兼职讲师*/
    @Column(name = "LECTURER_TYPE")
    private Long lecturerType;

    /** 内部讲师工号 */
    @Column(name = "LECTURER_ACCOUNT")
    private String lecturerAccount;

}
