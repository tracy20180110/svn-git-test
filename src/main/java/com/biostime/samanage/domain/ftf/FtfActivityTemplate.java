package com.biostime.samanage.domain.ftf;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MKT_SA_FTF_ACT_TEMPLATE", schema = "MAMA100_OWNER")
@Data
public class FtfActivityTemplate extends BaseDomain {

    public static final String KEY = "mkt_sa_ftf_act_template";

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /*** 模板名称*/
    @Column(name = "NAME")
    private String name;

    /*** 模板ID**/
    @Column(name = "IMAGE")
    private String image;

    /*** 模板背景图片**/
    @Column(name = "BG_IMAGE")
    private String bgImage;

    /*** 模板背景音乐**/
    @Column(name = "BG_MUSIC")
    private String bgMusic;

    /*** 0:失效，1：有效**/
    @Column(name = "STATUS")
    private String status;

}
