package com.biostime.samanage.domain.ftf;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:FTF课程
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_FTF_LECTURER_CHECK", schema = "MAMA100_OWNER")
@Data
public class FtfLecturerCheck extends BaseDomain {
    public static final String KEY = "mkt_sa_ftf_lecturer_check";
    private static final long serialVersionUID = 5884653474561178209L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /**讲师编号*/
    @Column(name = "LECTURER_CODE")
    private String	lecturerCode;

    /**状态 0-未验证 1-验证*/
    @Column(name = "STATUS")
    private Integer	status;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;
}
