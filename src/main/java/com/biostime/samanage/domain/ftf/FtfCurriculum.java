package com.biostime.samanage.domain.ftf;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:FTF课程
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_FTF_CURRICULUM", schema = "MAMA100_OWNER")
@Data
public class FtfCurriculum extends BaseDomain {

    public static final String KEY = "mkt_sa_ftf_curriculum";
    private static final long serialVersionUID = 5987765892752041787L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;


    /*** 课程名称*/
    @Column(name = "NAME")
    private String name;

    /**课程图片*/
    @Column(name = "IMAGE")
    private String image;

    /**状态 0-停用 1-启用*/
    @Column(name = "STATUS")
    private Integer	status;

    /** 课程简介 */
    @Column(name = "SYNOPS")
    private String synops;

    /** 课程笔记 */
    @Column(name = "NOTE")
    private String note;

    /**推荐人群*/
    @Column(name = "RECOM_GROUPS")
    private String recomGroups;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    /** 呈现应用 */
    @Column(name = "APP_TYPE")
    private String appType;

    /** 呈现应用名称 */
    @Column(name = "APP_TYPE_NAME")
    private String appTypeName;

    /**  "0:SA-FTF,1:门店-FTF , 2:学术推广，3：内训"  */
    @Column(name = "ACT_TYPE")
    private String actType;

    /** 0:SA-FTF,1:门店-FTF , 2:学术推广，3：内训" **/
    @Column(name = "ACT_TYPE_NAME")
    private String actTypeName;



}
