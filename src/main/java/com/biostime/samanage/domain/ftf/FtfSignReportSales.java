package com.biostime.samanage.domain.ftf;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 类功能描述: 手机营销通 - FTF签到上传销量
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_FTF_SIGN_REPORT_SALES", schema = "mama100_owner")
@Data
public class FtfSignReportSales  extends BaseDomain {


    public static final String KEY = "mkt_sa_ftf_sign_report_sales";
    private static final long serialVersionUID = 8933497448547496594L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;


    /**
     * 活动id
     */
    @Column(name = "ACT_ID")
    private String actId;

    /**
     * 销量
     */
    @Column(name = "SALES_VOLUME")
    private String salesVolume;


    /**
     * 图片一
     */
    @Column(name = "IMAGE_PATH1")
    private String imagePath1;

    /**
     * 图片二
     */
    @Column(name = "IMAGE_PATH2")
    private String imagePath2;

    /**
     * 图片三
     */
    @Column(name = "IMAGE_PATH3")
    private String imagePath3;

    /**
     * 图片四
     */
    @Column(name = "IMAGE_PATH4")
    private String imagePath4;

    /**
     * 图片五
     */
    @Column(name = "IMAGE_PATH5")
    private String imagePath5;

    /**
     * 图片六
     */
    @Column(name = "IMAGE_PATH6")
    private String imagePath6;

    /**
     * 上报人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 上报时间
     */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;
}
