package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 类功能描述: 路演活动
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_ROAD_SHOW_SW", schema = "mama100_owner")
@Data
public class ActRoadShowSw extends BaseDomain {

	public static final String KEY = "mkt_sa_act_road_show_sw";
	private static final long serialVersionUID = 8939867392193065550L;

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**swisse ID**/
	@Column(name = "SW_ID")
	private Long swId;

	/**
	 * 活动基础配置ID
	 */
	@Column(name = "ACT_SHOW_ID")
	private Long actShowId;

}