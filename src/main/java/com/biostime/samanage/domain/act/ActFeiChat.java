package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 类功能描述: feichat贸易销量
 *
 * @version 1.0
 * @author 12804
 * @createDate 2019年7月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_FEICHAT", schema = "mama100_owner")
@Data
public class ActFeiChat extends BaseDomain {

	public static final String KEY = "mkt_sa_act_feichat";
	private static final long serialVersionUID = 6563961437501894613L;

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**活动ID**/
	@Column(name = "ACT_ID")
	private Long actId;

	/**
	 * feichat 销量
	 */
	@Column(name = "TOTAL_SALE")
	private Double totalSale;

	/**
	 * feichat 有效销量
	 */
	@Column(name = "SALE")
	private Double sale;

	/**
	 * 数据创建时间
	 */
	@Column(name = "CREATED_TIME")
	private Date createdTime;

}