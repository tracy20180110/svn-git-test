package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 类功能描述: 路演活动
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_ROAD_SHOW_CONFIG", schema = "mama100_owner")
@Data
public class ActRoadShowConfig extends BaseDomain {

	public static final String KEY = "mkt_sa_act_road_show_config";
	private static final long serialVersionUID = -2599738591888156900L;

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**
	 * 活动类型名称
	 */
	@Column(name = "ACT_TYPE_NAME")
	private String actTypeName;

	/**
	 * 活动状态  活动状态 1-启用 2-停用  过期  根据END_TIME来判断
	 */
	@Column(name = "STATUS")
	private Integer status;


	/** 创建者 */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** 创建者名称 */
	@Column(name = "CREATED_NAME")
	private String createdName;

	/** 创建时间 */
	@Column(name = "CREATED_TIME")
	private Date createdTime;

	/** 更新人 */
	@Column(name = "UPDATED_BY")
	private String updatedBy;

	/** 更新时间 */
	@Column(name = "UPDATED_TIME")
	private Date updatedTime;


}