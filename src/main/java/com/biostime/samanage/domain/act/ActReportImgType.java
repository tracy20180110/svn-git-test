package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
/**
 * 类功能描述: 路演活动上报图片类型
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_REPORT_IMG_TYPE", schema = "mama100_owner")
@Data
public class ActReportImgType extends BaseDomain {

    public static final String KEY = "mkt_sa_act_report_img_type";

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /**
     * 活动名称
     */
    @Column(name = "NAME")
    private String name;
    /**
     * 活动状态  活动状态 1-启用 2-停用
     */
    @Column(name = "STATUS")
    private Integer status;


    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    /** 更新人名称 */
    @Column(name = "UPDATED_NAME")
    private String updatedName;
}
