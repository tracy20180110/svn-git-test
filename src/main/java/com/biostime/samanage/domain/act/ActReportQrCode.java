package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 类功能描述: 活动上报报备日期
 *
 * @version 1.0
 * @author 12804
 * @createDate 2018年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_REPORT_QRCODE", schema = "mama100_owner")
@Data
public class ActReportQrCode extends BaseDomain {

	public static final String KEY = "mkt_sa_report_qrcode";
	private static final long serialVersionUID = -8318626594334100794L;

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**swisse ID**/
	@Column(name = "ACT_ID")
	private Long actId;

	/**
	 * 门店编码
	 */
	@Column(name = "TERMINAL_CODE")
	private Long terminalCode;

	/**
	 * 创建人
	 */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/**
	 * 创建人名称
	 */
	@Column(name = "CREATED_NAME")
	private String createdName;

	/**
	 * 创建人时间
	 */
	@Column(name = "CREATED_TIME")
	private Date createdTime;

	/**
	 * 二维码编码
	 */
	@Column(name = "QR_CODE")
	private String qrCode;

	/**
	 * 二维码链接
	 */
	@Column(name = "QR_URL")
	private String qrUrl;

	/**
	 * 二维码原始链接
	 */
	@Column(name = "QR_SHORT_URL")
	private String qrShortUrl;

	/**
	 * 二维码类型（1：HT，2：Dodie）
	 */
	@Column(name = "QR_TYPE")
	private Long qrType;

	/**
	 * 更新人
	 */
	@Column(name = "UPDATE_BY")
	private String updateBy;

	/**
	 * 更新人名称
	 */
	@Column(name = "UPDATE_NAME")
	private String updateName;

	/**
	 * 更新人时间
	 */
	@Column(name = "UPDATE_TIME")
	private Date updateTime;
}