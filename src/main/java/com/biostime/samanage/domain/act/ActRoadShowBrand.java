package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 类功能描述: 活动现场品牌
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_ROAD_SHOW_BRAND", schema = "mama100_owner")
@Data
public class ActRoadShowBrand extends BaseDomain {

	public static final String KEY = "mkt_sa_act_road_show_brand";

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**
	 * 类型名称1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie
	 *
	 */
	@Column(name = "CODE")
	private Long code;

	/**
	 * 活动ID
	 */
	@Column(name = "ACT_ROAD_ID")
	private Long actRoadId;

	/**
	 * 1:活动现场配置，2：活动现场上报
	 */
	@Column(name = "TYPE")
	private Long type;
}