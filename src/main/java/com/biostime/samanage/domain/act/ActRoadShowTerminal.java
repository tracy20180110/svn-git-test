package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 类功能描述: 路演活动门店
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_ROAD_SHOW_TERMINAL", schema = "mama100_owner")
@Data
public class ActRoadShowTerminal extends BaseDomain {


	private static final long serialVersionUID = 533361431827820819L;

	public static final String KEY = "mkt_sa_act_road_show_terminal";

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**
	 * 路演活动ID
	 */
	@Column(name = "ACT_RS_ID")
	private Long actRsId;

	/**
	 * 活动开始时间
	 */
	@Column(name = "START_TIME")
	private Date	startTime;

	/**
	 * 活动开始时间
	 */
	@Column(name = "END_TIME")
	private Date	endTime;

	/**
	 * 活动状态  活动状态 0-无效 1-有效
	 */
	@Column(name = "STATUS")
	private Integer status;

	/** 门店编码 */
	@Column(name = "TERMINAL_CODE")
	private String terminalCode;


	/** 创建者 */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** 创建者名称 */
	@Column(name = "CREATED_NAME")
	private String createdName;

	/** 创建时间 */
	@Column(name = "CREATED_TIME")
	private Date createdTime;

	/** 更新人 */
	@Column(name = "UPDATED_BY")
	private String updateBy;

	/** 更新时间 */
	@Column(name = "UPDATED_TIME")
	private Date updateTime;


}