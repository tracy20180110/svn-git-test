package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 类功能描述: 直播上报
 *
 * @version 1.0
 * @author 12804
 * @createDate 2020年4月13日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_REPORT_ZB", schema = "mama100_owner")
@Data
public class ActRePortZhiBo extends BaseDomain {


	public static final String KEY = "mkt_sa_act_report_zb";
	private static final long serialVersionUID = -3493744619617348769L;

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**
	 * 上报时间
	 */
	@Column(name = "CREATED_TIME")
	private Date createdTime;

	/**
	 * 门店编码
	 */
	@Column(name = "SA_CODE")
	private String saCode;

	/**
	 * 操作员
	 */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** 创建者名称 */
	@Column(name = "CREATED_NAME")
	private String createdName;


	/**
	 * 直播主题
	 */
	@Column(name = "TITLE_NAME")
	private String titleName;

	/**
	 * 直播日期
	 */
	@Column(name = "ZB_DATE")
	private Date zbDate;

	/**
	 * 直播开始时间
	 */
	@Column(name = "START_TIME")
	private String startTime;

	/**
	 * 直播开始时间
	 */
	@Column(name = "END_TIME")
	private String endTime;


	/**
	 * 主播工号
	 */
	@Column(name = "ZB_CODE")
	private String zbCode;

	/**
	 * 主播名称
	 */
	@Column(name = "ZB_NAME")
	private String zbName;

	/**
	 * 辅播工号
	 */
	@Column(name = "FB_CODE")
	private String fbCode;

	/**
	 * 辅播名称
	 */
	@Column(name = "FB_NAME")
	private String fbName;

	/**
	 * 直播专家(格式姓名+单位+科室职称)
	 */
	@Column(name = "ZB_EXPERT")
	private String zbExpert;

	/**
	 * 合作门店数量
	 */
	@Column(name = "TERMINAL_NUMS")
	private Integer terminalNums;

	/**
	 * 观看人次
	 */
	@Column(name = "VISITORS_NUMS")
	private Integer visitorsNums;


	/**
	 * 直播销量
	 */
	@Column(name = "ZB_SALES")
	private String zbSales;

	/**
	 * 报名社群数
	 */
	@Column(name = "REG_GROUP_NUMS")
	private Integer	regGroupNums;

	/**
	 * 覆盖群人数
	 */
	@Column(name = "COVER_NUMS")
	private Integer	coverNums;

	/**
	 * 总销售金额
	 */
	@Column(name = "TOTAL_SALES_AMOUNT")
	private String totalSalesAmount;

	/**
	 * 合生元婴幼儿奶粉新客
	 */
	@Column(name = "BST_BABY_NEW_CUST")
	private Integer	bstBabyNewCust;

	/**
	 * 合生元妈妈奶粉新客
	 */
	@Column(name = "BST_MAMA_NEW_CUST")
	private Integer	bstMamaNewCust;

	/**
	 * 益生菌新客数
	 */
	@Column(name = "PROBIOTIC_NEW_CUST")
	private Integer	probioticNewCust;

	/**
	 * 羊奶新客数
	 */
	@Column(name = "KBS_NEW_CUST")
	private Integer	kbsNewCust;

	/**
	 * HT新客数
	 */
	@Column(name = "HT_NEW_CUST")
	private Integer	htNewCust;

	/**
	 * Dodie新客
	 */
	@Column(name = "DODIE_NEW_CUST")
	private Integer	dodieNewCust;

	/**
	 * 图片路径1
	 */
	@Column(name = "IMAGE_PATH1")
	private String	imagePath1;

	/**
	 * 图片路径2
	 */
	@Column(name = "IMAGE_PATH2")
	private String	imagePath2;

	/**
	 * 图片路径3
	 */
	@Column(name = "IMAGE_PATH3")
	private String	imagePath3;

	/**
	 * 直播平台名称
	 */
	@Column(name = "ZB_TERRACE_NAME")
	private String	zbTerraceName;


	/**
	 * 合作渠道名称
	 */
	@Column(name = "CHANNEL_NAME")
	private String	channelName;


	/**
	 * 门店类型名称
	 */
	@Column(name = "TERMINAL_TYPE_NAME")
	private String	terminalTypeName;


	/**
	 * 品牌名称
	 */
	@Column(name = "BRAND_NAME")
	private String	brandName;

	/**
	 * 直播平台编码
	 */
	@Column(name = "ZB_TERRACE_CODE")
	private String	zbTerraceCode;


	/**
	 * 合作渠道编码
	 */
	@Column(name = "CHANNEL_CODE")
	private String	channelCode;


	/**
	 * 门店类型编码
	 */
	@Column(name = "TERMINAL_TYPE_CODE")
	private String	terminalTypeCode;


	/**
	 * 品牌编码
	 */
	@Column(name = "BRAND_CODE")
	private String	brandCode;

	/**
	 * 渠道类型
	 */
	@Column(name = "CHANNEL_TYPE")
	private Long	channelType;


	/**
	 * 活动模式（1：直播、2：语音）
	 */
	@Column(name = "ACT_MODEL")
	private Long	actModel;


	/**
	 * 授课讲师工号
	 */
	@Column(name = "LECTURER_CODE")
	private String	lecturerCode;


	/**
	 * 授课讲师名称
	 */
	@Column(name = "LECTURER_NAME")
	private String	lecturerName;

	/**
	 * 签到人数
	 */
	@Column(name = "SIGN_NUM")
	private Long	signNum;

	/**
	 * 合作门店编码
	 */
	@Column(name = "COOPERATION_TERMINAL_CODE")
	private Long	cooperationTerminalCode;

}