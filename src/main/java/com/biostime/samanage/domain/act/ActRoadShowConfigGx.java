package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 类功能描述: 路演活动
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_ROAD_SHOW_CONFIG_GX", schema = "mama100_owner")
@Data
public class ActRoadShowConfigGx extends BaseDomain {

	public static final String KEY = "mkt_sa_act_road_show_config_gx";
	private static final long serialVersionUID = 8606418969207408587L;

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**
	 * 类型名称1：合生元，2：素加，3：葆艾，21706：HT，26509：Swisse，33206：Dodie
	 *
	 */
	@Column(name = "CODE")
	private String code;

	/**
	 * 活动基础配置ID
	 */
	@Column(name = "ACT_CONFIG_ID")
	private Long actConfigId;

	/**
	 * 1：品牌，2：渠道，3：门店
	 */
	@Column(name = "TYPE")
	private Long type;

}