package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * 类功能描述: 明细上报审批
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_REPORT_DETAIL_AUDIT", schema = "mama100_owner")
@Data
public class ActRoadShowAudit extends BaseDomain {

    public static final String KEY = "mkt_sa_act_report_detail_audit";
    private static final long serialVersionUID = 6711566928718079802L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /**
     * 活动上报明细id
     */
    @Column(name = "ACT_REPORT_DETAIL_ID")
    private Long actDetailId;

    /**
     * 是否合格
     */
    @Column(name = "IS_SUFFICE")
    private String isSuffice;     //是否合格

    /**
     * 审批备注
     */
    @Column(name = "AUDIT_MEMO")
    private String auditMemo;     //审批备注

    /**
     * 操作员
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /**
     * 上报时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_TIME")
    private Date createdTime;
}
