package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 类功能描述: 现场上报
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_REPORT_DETAIL", schema = "mama100_owner")
@Data
public class ActRePortDetail extends BaseDomain {

	private static final long serialVersionUID = -7844732206294506681L;

	public static final String KEY = "mkt_sa_act_report_detail";

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**
	 * 设备ID
	 */
	@Column(name = "DEVICE_ID")
	private String deviceId;

	/**
	 * 门店编码
	 */
	@Column(name = "TERMINAL_CODE")
	private String terminalCode;

	/**
	 * 活动id
	 */
	@Column(name = "ACT_ID")
	private String actId;

	/**
	 * 活动类型（1：路演活动现场上报、2：SA上报）
	 */
	@Column(name = "type")
	private String type;


	/**
	 * 活动备注
	 */
	@Column(name = "ACT_MEMO")
	private String actMemo;


	/**
	 * 图片一
	 */
	@Column(name = "IMAGE_PATH1")
	private String imagePath1;

	/**
	 * 图片二
	 */
	@Column(name = "IMAGE_PATH2")
	private String imagePath2;

	/**
	 * 图片三
	 */
	@Column(name = "IMAGE_PATH3")
	private String imagePath3;

	/**
	 * 图片四
	 */
	@Column(name = "IMAGE_PATH4")
	private String imagePath4;

	/**
	 * 图片五
	 */
	@Column(name = "IMAGE_PATH5")
	private String imagePath5;

	/**
	 * 图片六
	 */
	@Column(name = "IMAGE_PATH6")
	private String imagePath6;

	/**
	 * 图片一标签
	 */
	@Column(name = "IMAGE_MARK1")
	private String imageMark1;

	/**
	 * 图片二标签
	 */
	@Column(name = "IMAGE_MARK2")
	private String imageMark2;

	/**
	 * 图片三标签
	 */
	@Column(name = "IMAGE_MARK3")
	private String imageMark3;

	/**
	 * 图片四标签
	 */
	@Column(name = "IMAGE_MARK4")
	private String imageMark4;

	/**
	 * 图片五标签
	 */
	@Column(name = "IMAGE_MARK5")
	private String imageMark5;

	/**
	 * 图片六标签
	 */
	@Column(name = "IMAGE_MARK6")
	private String imageMark6;

	/**
	 * 操作员
	 */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** 创建者名称 */
	@Column(name = "CREATED_NAME")
	private String createdName;

	/**
	 * 上报时间
	 */
	@Column(name = "CREATED_TIME")
	private Date createdTime;

	/**
	 * 补录原因(1：网络原因未能及时上报、2：营销通系统原因未能及时上报、3：其他)
	 */
	@Column(name = "MAKEUP_MEMO")
	private Integer	makeupMemo;

	/**
	 * 补录原因 其他
	 */
	@Column(name = "MAKEUP_MEMO_OTHER")
	private String	makeupMemoOther;

	/**
	 * 是否补录（0：否，1：是）
	 */
	@Column(name = "ACT_MAKEUP")
	private Integer	actMakeup;

	//2018-12-28 版本
	/**
	 * SA-FTF活动ID
	 */
	@Column(name = "SA_FTF_ACT_ID")
	private Long saFtfActId	;
}