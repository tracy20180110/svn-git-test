package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 类功能描述: 路演活动
 *
 * @version 1.0
 * @author 12804
 * @createDate 2016年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_ROAD_SHOW", schema = "mama100_owner")
@Data
public class ActRoadShow extends BaseDomain {

	private static final long serialVersionUID = -7844732206294506681L;
	public static final String KEY = "mkt_sa_act_road_show";

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;


	/**
	 * 活动名称
	 */
	@Column(name = "ACT_NAME")
	private String actName;

	/**
	 * 大区办事处编码
	 */
	@Column(name = "AREA_OFFIC_CODE")
	private String areaOfficeCode;

	/**
	 * 1:大篷车，2：迷你版，3：30平  ---不适用（null ：常规活动，ws_act_baby_brand_shop：特殊活动）
	 */
	@Column(name = "TYPE")
	private String	type;

	/**
	 * 活动品牌
	 */@Column(name = "BRAND")
	private String brand;

	/** 渠道名称 */
	@Column(name = "CHANNEL_NAME")
	private String channelName;

	/** 渠道编码 */
	@Column(name = "CHANNEL_CODE")
	private String channelCode;

	/**
	 * 活动开始时间
	 */
	@Column(name = "START_TIME")
	private Date	startTime;

	/**
	 * 活动开始时间
	 */
	@Column(name = "END_TIME")
	private Date	endTime;

	/**
	 * 活动状态  活动状态 1-启用 2-停用  过期  根据END_TIME来判断
	 */
	@Column(name = "STATUS")
	private Integer status;


	/** 创建者 */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** 创建者名称 */
	@Column(name = "CREATED_NAME")
	private String createdName;

	/** 创建时间 */
	@Column(name = "CREATED_TIME")
	private Date createdTime;

	/** 更新人 */
	@Column(name = "UPDATED_BY")
	private String updatedBy;

	/** 更新时间 */
	@Column(name = "UPDATED_TIME")
	private Date updatedTime;

	/** 活动关系配置 */
	@Column(name = "ACT_CONFIG_ID")
	private Long actConfigId;

	/** 考核类型1:路演活动，2：物料陈列 */
	@Column(name = "KH_TYPE")
	private Long khType;

	/**门店类型 1：区域重点连锁,2：其他,3:婴KA */
	@Column(name = "Terminal_TYPE_CODE")
	private String terminalTypeCode;

	/**门店类型 1：区域重点连锁,2：其他,3:婴KA */
	@Column(name = "Terminal_TYPE_NAME")
	private String terminalTypeName;

	/**上报选相册（1：是，0否/空也是否） */
	@Column(name = "ACT_REPORT_TYPE")
	private Long actReportType;

	/**签到小程序 */
	@Column(name = "SIGN_MINI_CODE")
	private Long signMiniCode;
}