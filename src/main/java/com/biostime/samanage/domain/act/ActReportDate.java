package com.biostime.samanage.domain.act;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 类功能描述: 活动上报报备日期
 *
 * @version 1.0
 * @author 12804
 * @createDate 2018年6月23日 上午10:59:18
 */
@Entity
@Table(name = "MKT_SA_ACT_REPORT_DATE", schema = "mama100_owner")
@Data
public class ActReportDate extends BaseDomain {

	public static final String KEY = "mkt_sa_act_report_date";
	private static final long serialVersionUID = -4498714010828839760L;

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	/**swisse ID**/
	@Column(name = "ACT_DETAIL_ID")
	private Long actDetailId;

	/**
	 * 活动开始日期
	 */
	@Column(name = "START_DATE")
	private Date startDate;

	/**
	 * 活动结束日期
	 */
	@Column(name = "END_DATE")
	private Date endDate;

}