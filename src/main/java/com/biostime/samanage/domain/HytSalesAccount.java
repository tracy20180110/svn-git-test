package com.biostime.samanage.domain;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zhuhaitao on 2017/2/22.
 */
@Entity
@Table(name = "HYT_SALES_ACCOUNT")
@Data
public class HytSalesAccount extends BaseDomain {
    @Id
    @Column(name = "ID", length = 20)
    private Long id;
    @Column(name = "ACCOUNT_NO", length = 100)
    private String accountNo;
    @Column(name = "officeCode", length = 100)
    private String officeCode;

}
