package com.biostime.samanage.domain.sales;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:促销员打卡
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_SALES_SIGN", schema = "MAMA100_OWNER")
@Data
public class SalesSign extends BaseDomain {
    public static final String KEY = "mkt_sa_sales_sign";

    private static final long serialVersionUID = 3876415407989718948L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /**是否一致 0：不一致，1：一致 */
    @Column(name = "STATUS")
    private Integer	status;

    /** 签到位置经度 */
    @Column(name = "LONGITUDE")
    private String longitude;

    /** 签到位置纬度 */
    @Column(name = "LATITUDE")
    private String latitude;

    /**打卡签到门店编码*/
    @Column(name = "TERMINAL_CODE")
    private String terminalCode;

    /**1：上班，2：下班 */
    @Column(name = "TYPE")
    private Integer	type;

    /** 打卡时间 */
    @Column(name = "SIGN_TIME")
    private Date signTime;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    @Column(name = "CHANNEL_CODE")
    private String channelCode;

    @Column(name = "DEPARTMENT_CODE")
    private String departmentCode;

}
