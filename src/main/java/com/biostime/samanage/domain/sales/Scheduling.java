package com.biostime.samanage.domain.sales;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Describe:工作排班
 * Date: 2019-03-15
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Entity
@Table(name = "MKT_SA_SCHEDULING", schema = "MAMA100_OWNER")
@Data
public class Scheduling extends BaseDomain {
    public static final String KEY = "mkt_sa_scheduling";
    private static final long serialVersionUID = -8467125891035261346L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /**状态 1：上班、2：本休、3：调休、4请假 */
    @Column(name = "STATUS")
    private Integer	status;

    /** 排班日期 */
    @Column(name = "SCHEDULING_DATE")
    private Date schedulingDate;

    /** 备注 */
    @Column(name = "MEMO")
    private String memo;

    /** 创建者 */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** 创建者名称 */
    @Column(name = "CREATED_NAME")
    private String createdName;

    /** 创建时间 */
    @Column(name = "CREATED_TIME")
    private Date createdTime;

    /** 更新人 */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** 更新时间 */
    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    /** 更新人名称 */
    @Column(name = "UPDATED_NAME")
    private String updatedName;
}
