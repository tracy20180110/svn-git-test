package com.biostime.samanage.domain.sales;

import com.biostime.common.domain.BaseDomain;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Describe:工作排班
 * Date: 2019-03-15
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Entity
@Table(name = "MKT_SA_SCHEDULING_TERMINAL", schema = "MAMA100_OWNER")
@Data
public class SchedulingTerminal extends BaseDomain {
    public static final String KEY = "mkt_sa_scheduling_terminal";
    private static final long serialVersionUID = -6185739492973521826L;

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /** 门店编码 */
    @Column(name = "TERMINAL_CODE")
    private String terminalCode;

    /** 排班ID */
    @Column(name = "SCHEDULING_ID")
    private Long schedulingId;


}
