package com.biostime.samanage.task;

import com.biostime.samanage.service.ftf.FtfActSignService;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 查询feiChat贸易销量
 * Created by 12804 on 2018/10/24.
 */
@Slf4j
@Component
public class ActFeiChatSaleJob implements SimpleJob {

    @Autowired
    private FtfActSignService ftfActSignService;
    /**
     * feiChat贸易销量
     */
    @Override
    public void execute(ShardingContext context) {
        switch (context.getShardingItem()) {
            case 0:
                System.out.println("-----0-----:"+context.getShardingParameter());
                ftfActSignService.saveActFeiChatSale();
                log.info("定时任务==========elasticJob================");
                break;
            case 1:
                System.out.println("-----1-----:"+context.getShardingParameter());
                break;
            case 2:
                System.out.println("-----2-----:"+context.getShardingParameter());
                break;
            default:
                System.out.println(context.getJobParameter());
        }
    }
}
