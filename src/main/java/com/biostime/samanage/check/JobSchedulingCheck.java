package com.biostime.samanage.check;

import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.calibrator.CalibratorResponse;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.AddSaJobSchedulingBean;
import com.biostime.samanage.calibratorchain.JobSchedulingAddCalibratorChain;
import org.springframework.beans.BeanUtils;

import javax.servlet.http.HttpServletRequest;
/**
 * 类功能描述: 保存促销员排班接口校验
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年3月11日 下午5:31:11
 */
public class JobSchedulingCheck {
	/**
	 * 方法描述: 新增校验
	 *
	 * @param req
	 * @param request
	 * @return
	 * @author zhuhaitao
	 * @createDate 2016年3月14日 下午4:51:41
	 */
	public static BaseResponse<String> saveJobScheduling(BaseRequest<AddSaJobSchedulingBean> req,
			HttpServletRequest request) {
		BaseResponse<String> response = new BaseResponse<String>(DateUtils.makeDateSeqNo());
		// 使用校验器进行校验（实现校验功能的通用）
		AddSaJobSchedulingBean addSaJobSchedulingBean = req.getRequest();
		CalibratorResponse calibratorResponse = JobSchedulingAddCalibratorChain.instance()
				.check(addSaJobSchedulingBean);
		if (calibratorResponse.getCode() != 100) {
			// 基础校验（只是做用例，正式开发时基础校验放service层，controller层校验多数用来检测文本的格式（email，手机号等））
			BeanUtils.copyProperties(calibratorResponse, response);
		}
		ExceptionUtils.setError(response);
		return response;
	}

	/**
	 * 方法描述: 促销员编号不能为空校验
	 *
	 * @param salesAccountNo
	 * @param request
	 * @return
	 * @author zhuhaitao
	 * @createDate 2016年7月1日 下午1:19:57
	 */
	public static BaseResponse<String> salesAccountNoCheck(String salesAccountNo, HttpServletRequest request) {
		BaseResponse<String> response = new BaseResponse<String>(DateUtils.makeDateSeqNo());
		if (StringUtil.isNullOrEmpty(salesAccountNo)) {
			response.setCode(1000);
			response.setDesc(PropUtils.getProp(response.getCode()));
		}
		ExceptionUtils.setError(response);
		return response;
	}
}
