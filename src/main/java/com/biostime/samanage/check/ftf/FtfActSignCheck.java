package com.biostime.samanage.check.ftf;

import com.biostime.common.bean.BaseResponse;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.ftf.sign.FtfActSignInfoReqBean;

/**
 * 类功能描述: FTF活动
 *
 * @version 1.0
 * @author Tracy
 * @createDate 2016年10月11日 下午5:31:11
 */
public class FtfActSignCheck {

    /**
     * 方法描述: 活动ID
     * @param ftfActSignInfoReqBean
     * @return
     */
    public static BaseResponse saveMobSign(FtfActSignInfoReqBean ftfActSignInfoReqBean) {
        BaseResponse response = new BaseResponse(DateUtils.makeDateSeqNo());
        if (StringUtil.isNullOrEmpty(ftfActSignInfoReqBean.getId())) {
            response.setCode(1002);
            response.setDesc(PropUtils.getProp(response.getCode()));
        }
        ExceptionUtils.setError(response);
        return response;
    }
}
