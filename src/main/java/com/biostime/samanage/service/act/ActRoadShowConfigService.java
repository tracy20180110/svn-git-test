package com.biostime.samanage.service.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigReqBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigResBean;

import javax.servlet.http.HttpServletResponse;

/**
 *
 * 路演活动
 * Created by dc on 2018/03/13
 */
public interface ActRoadShowConfigService {



    /**
     * 路演活动关系配置-查询
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<ActRoadShowConfigResBean>> queryActRoadShowConfigList(Pager<ActRoadShowConfigResBean> pager, ActRoadShowConfigReqBean reqBean);

    /**
     * 路演活动关系配置-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportActRoadShowConfigList(HttpServletResponse response, ActRoadShowConfigReqBean reqBean);

    /**
     * 路演活动关系-状态修改
     * @param reqBean
     * @return
     */
    public ServiceBean editStatusActRoadShowConfig(ActRoadShowConfigReqBean reqBean);

    /**
     * 路演活动关系-新增
     * @param reqBean
     * @return
     */
    public ServiceBean saveActRoadShowConfig(ActRoadShowConfigReqBean reqBean);


}
