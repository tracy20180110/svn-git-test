package com.biostime.samanage.service.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.actreport.ActRePortWebReqBean;
import com.biostime.samanage.bean.actreport.ActRePortWebResBean;
import com.biostime.samanage.bean.actreport.ActRePortZhiBoReqBean;
import com.biostime.samanage.bean.actroadshow.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * 活动现场
 * Created by dc on 2016/8/17.
 */
public interface ActRoadShowService {



    /**
     * 活动现场查询
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<ActRoadShowListResBean>> queryActRoadShowList(Pager<ActRoadShowListResBean> pager, ActRoadShowListReqBean reqBean);

    /**
     * 活动现场导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportActRoadShow(HttpServletResponse response, ActRoadShowListReqBean reqBean);
    /**
     * 活动现场编辑-新增
     * @param reqBean
     * @return
     */
    public ServiceBean saveActRoadShow(ActRoadShowSaveReqBean reqBean);


    /**
     * 活动现场状态修改
     * @param reqBean
     * @return
     */
    public ServiceBean changeStatusActRoadShow(ActRoadShowChangeStatusReqBean reqBean);


    /**
     * 活动现场-获取swisse列表、活动类型
     * @param reqBean
     * @return
     */
    public ServiceBean<List<ActRoadShowResBean>> querySwActTypeList(ActRoadShowReqBean reqBean);


    /**
     * 营销上报明细
     * @param pager
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<ActRoadShowDetailResBean>> queryActRoadShowDetailList(Pager<ActRoadShowDetailResBean> pager, ActRoadShowListReqBean reqBean);

    /**
     * 营销上报明细-审核
     * @param reqBean
     * @return
     */
    public ServiceBean auditActRoadShowDetail(ActRoadShowAuditReqBean reqBean);


    /**
     * 营销上报明细导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportActRoadShowDetail(HttpServletResponse response, ActRoadShowListReqBean reqBean);


    /**
     * 路演报表查询
     * @param pager
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<ActRoadShowReportResBean>> queryReportActRoadShowList(Pager<ActRoadShowReportResBean> pager, ActRoadShowReportReqBean reqBean);

    /**
     * 路演报表导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportReportActRoadShow(HttpServletResponse response, ActRoadShowReportReqBean reqBean);

    /**
     * 活动现场--门店导出
     * @param response
     * @param actId 活动ID
     * @return
     */
    public ServiceBean exportActRoadShowTerminal(HttpServletResponse response, String actId);

    /**
     * 活动现场--门店导入
     * @param reqBean
     * @return
     */
    public ServiceBean<String> importActRoadShowTerminal(MultipartFile multipartFile, ActRoadShowTerminalReqBean reqBean);


    /**
     * 营销上报明细-补录
     * @param reqBean
     * @return
     */
    public ServiceBean saveActRoadShowMakeup(ActRePortWebReqBean reqBean);

    /**
     * 营销上报明细-补录活动验证
     * @param reqBean
     * @return
     */
    public ServiceBean<ActRePortWebResBean> checkActShowInfo(ActRePortWebReqBean reqBean);

    /**
     * 营销上报明细-补录活动验证
     * @param reqBean
     * @return
     */
    public ServiceBean checkActTerminal(ActRePortWebReqBean reqBean);

    /**
     * 营销上报明细-上报人验证
     * @param reqBean
     * @return
     */
    public ServiceBean checkAccountTerminal(ActRePortWebReqBean reqBean);

    /**
     * 活动精英上报明细查询
     * @param pager
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<ActRoadShowDetailResBean>> queryActElitesReportDetailList(Pager<ActRoadShowDetailResBean> pager, ActRoadShowListReqBean reqBean);

    /**
     * 活动精英上报明细导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportActElitesReportDetailList(HttpServletResponse response, ActRoadShowListReqBean reqBean);

    /**
     * 直播上报报表查询
     * @param pager
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<ActRePortZhiBoReqBean>> queryActReportZhiBoList(Pager<ActRePortZhiBoReqBean> pager, ActRePortZhiBoReqBean reqBean);

    /**
     * 直播上报报表导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportActReportZhiBoList(HttpServletResponse response, ActRePortZhiBoReqBean reqBean);

}
