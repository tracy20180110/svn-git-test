package com.biostime.samanage.service.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.actreport.*;
import com.biostime.samanage.bean.actroadshow.ActReportDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowListReqBean;

import java.util.List;

/**
 * Describe:活动上报
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface ActRePortService {

    /**
     * 获取登陆人可上报的活动
     * @param reqBean
     * @return
     */
    public ServiceBean<List<ActRePortListResBean>> queryActRePortList(ActRePortListReqBean reqBean) ;

    /**
     * 提交活动上报
     * @param reqBean
     * @return
     */
    public ServiceBean submitActRePort(ActRePortReqBean reqBean) ;

    /**
     * 获取门店对应的渠道
     * @param terminalCode
     * @return
     */
    public ServiceBean<ActRePortTypeResBean> queryTerminalChannel(String terminalCode,String reportTypeCode,String buCode) ;

    /**
     * 路活动上报图片类型查询
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<ActRePortImgTypeResBean>> queryActRePortImgTypeList(Pager<ActRePortImgTypeResBean> pager, ActRePortImgTypeReqBean reqBean);

    /**
     * 获取图片类型列表
     * @param reqBean
     * @return
     */
    public ServiceBean<List<ActRePortImgTypeResBean>> queryImgTypeList(ActRePortImgTypeReqBean reqBean) ;


    /**
     * 图片类型保存、编辑
     * @param reqBean
     * @return
     */
    public ServiceBean saveActRePortImgType(ActRePortImgTypeReqBean reqBean);

    /**
     * 获取上报关联的SA活动
     * @param reqBean
     * @return
     */
    public ServiceBean<List<BaseKeyValueBean>> querySaFtfActList(ActRePortReqBean reqBean) ;

    /**
     * 手机上报活动明细列表
     * @param reqBean
     * @return
     */
    public ServiceBean<ActReportDetailResBean> queryActReportDetail(ActRoadShowListReqBean reqBean) ;

    /**
     * 手机上报活动明细详情列表
     * @param reqBean
     * @return
     */
    public ServiceBean<List<ActRoadShowDetailResBean>> queryActReportDetailInfo(ActRoadShowListReqBean reqBean) ;

    /**
     * 查询是否是活动精英
     * @param reqBean
     * @return
     */
    public ServiceBean queryIsActElites(ActRoadShowListReqBean reqBean) ;
    /**
     * 查询活动精英所属办事处的门店
     * @param reqBean
     * @return
     */
    public ServiceBean<List<BaseKeyValueBean>> queryActElitesByOfficeTerminal(ActRoadShowListReqBean reqBean) ;

    /**
     * 查询品牌官微二维码
     * @param reqBean
     * @return
     */
    public ServiceBean<List<ActRePortQrcodeResBean>> queryReportBrandQrCodeList(ActRePortReqBean reqBean) ;


    /**
     * 活动上报直播
     * @param reqBean
     * @return
     */
    public ServiceBean saveActRePortZb(ActRePortZhiBoReqBean reqBean) ;

    /**
     * 查询直播人员
     * @param reqBean
     * @return
     */
    public ServiceBean<List<BaseKeyValueBean>> queryZhiBoAccount(BaseKeyValueBean reqBean) ;

    /**
     * BNC活动精英指派门店
     * @param reqBean
     * @return
     */
    public ServiceBean<List<BaseKeyValueBean>> queryBncActElitesTerminal(ActRoadShowListReqBean reqBean) ;


}
