package com.biostime.samanage.service.customer;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.customer.CustomerDetailBean;

import javax.servlet.http.HttpServletResponse;

/**
 * SA客户明细
 * Created by zhuhaitao on 2017/1/3.
 */
public interface SaCustomerDetailService {
    /**
     * 分页查询SA客户明细
     *
     * @param pager
     * @param customerDetailBean
     * @return
     */
    ServiceBean<Pager<CustomerDetailBean>> searchSaCustomerDetail(Pager<CustomerDetailBean> pager, CustomerDetailBean customerDetailBean);

    /**
     * 导出SA客户明细
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param proBcc
     * @param srcLocName
     * @param recSrc
     * @param startTime
     * @param endTime
     * @param response
     * @return
     */
    ServiceBean exportSaCustomerDetail(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobile, String proBcc, String srcLocName, String recSrc, String startTime, String endTime, String stage, String buCode,String parentingInstructor, HttpServletResponse response);
}
