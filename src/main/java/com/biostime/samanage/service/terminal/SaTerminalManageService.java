package com.biostime.samanage.service.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.terminal.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA与门店管理Service
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaTerminalManageService {

    /**
     * SA与门店管理导入列表
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaTerminalManageImportResBean>> querySaTerminalManageImportList(Pager<SaTerminalManageImportResBean> pager, SaTerminalManageImportReqBean reqBean);

    /**
     * SA与门店管理导入列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaTerminalManageImportList(HttpServletResponse response, SaTerminalManageImportReqBean reqBean);

    /**
     * SA与门店管理
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaTerminalManageResBean>> querySaTerminalManageList(Pager<SaTerminalManageResBean> pager, SaTerminalManageReqBean reqBean);

    /**
     * SA与门店管理-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaTerminalManageList(HttpServletResponse response, SaTerminalManageReqBean reqBean);


    /**
     * SA与门店管理-导入
     * @param multipartFile
     * @param request
     * @return
     */
    public ServiceBean importSaTerminalManage(MultipartFile multipartFile,HttpServletRequest request);

    /**
     * SA与门店管理--删除
     * @param reqBean
     * @return
     */
    public ServiceBean deleteSaTerminalManage(SaTerminalManageEditReqBean reqBean);

    /**
     * SA与门店管理-编辑
     * @param reqBean
     * @return
     */
    public ServiceBean editSaTerminalManage(SaTerminalManageEditReqBean reqBean);

    /**
     * SA撤销结束与门店的关系
     *  @param reqBean
     * @return
     */
    public ServiceBean delSaTerminalRelation(SaTerminalManageReqBean reqBean);

    /**
     * SA与门店管理(新)
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaTerminalManageResBean>> querySaTerminalNewManageList(Pager<SaTerminalManageResBean> pager, SaTerminalManageReqBean reqBean);

    /**
     * SA与门店绑定关系
     * @param reqBean
     * @return
     */
    public ServiceBean saveSaTerminalRelation(SaTerminalManageReqBean reqBean);
}
