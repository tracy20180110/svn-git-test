package com.biostime.samanage.service.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.terminal.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description:门店拜访考核报表
 * @Auther: 12804
 * @Date: 2019/6/21 10:19
 * @Version:1.0.0
 */
public interface TerminalVisitService {
    /**
     * 门店拜访考核查询
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<TerminalVisitResBean>> queryTerminalVisitList(Pager<TerminalVisitResBean> pager, TerminalVisitReqBean reqBean);

    /**
     * 门店拜访考核-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportTerminalVisitList(HttpServletResponse response, TerminalVisitReqBean reqBean);

    /**
     * 查询门店拜访岗位列表
     * @param reqBean
     * @return
     */
    public ServiceBean<List<BaseKeyValueBean>> queryTerminalVisitPostList(TerminalVisitReqBean reqBean);

    /**
     * 门店拜访考核季度查询
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<TerminalVisitQuarterResBean>> queryTerminalVisitQuarterList(Pager<TerminalVisitQuarterResBean> pager, TerminalVisitReqBean reqBean);

    /**
     * 门店拜访考核季度查询-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportTerminalVisitQuarterList(HttpServletResponse response, TerminalVisitReqBean reqBean);

    /**
     * 门店拜访考核查询
     * @param reqBean
     * @return
     */
    public ServiceBean<List<TerminalVisitMobResBean>> queryTerminalVisitMobList(TerminalVisitMobReqBean reqBean);

}
