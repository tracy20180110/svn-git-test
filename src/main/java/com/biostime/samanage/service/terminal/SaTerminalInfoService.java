package com.biostime.samanage.service.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.terminal.SaTerminalInfoReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalInfoResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA门店查询Service
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaTerminalInfoService {


    /**
     * SA门店查询列表
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaTerminalInfoResBean>> querySaTerminalInfoList(Pager<SaTerminalInfoResBean> pager, SaTerminalInfoReqBean reqBean);

    /**
     * SA门店查询列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaTerminalInfoList(HttpServletResponse response, SaTerminalInfoReqBean reqBean);


}
