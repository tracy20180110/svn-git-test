package com.biostime.samanage.service.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageEditReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA与门店管理Service
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaTerminalManageNewService {

    /**
     * SA与门店管理
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaTerminalManageNewResBean>> querySaTerminalManageNewList(Pager<SaTerminalManageNewResBean> pager, SaTerminalManageNewReqBean reqBean);

    /**
     * SA与门店管理-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaTerminalManageNewList(HttpServletResponse response, SaTerminalManageNewReqBean reqBean);


    /**
     * SA与门店管理-编辑
     * @param reqBean
     * @return
     */
    public ServiceBean editSaTerminalManage(SaTerminalManageEditReqBean reqBean);



    /**
     * SA与门店绑定关系
     * @param reqBean
     * @return
     */
    public ServiceBean saveSaTerminalRelationNew(SaTerminalManageNewReqBean reqBean);

    /**
     * 查询门店渠道、状态
     * @param reqBean
     * @return
     */
    public ServiceBean<SaTerminalManageNewResBean> queryTerminalInfo(SaTerminalManageNewReqBean reqBean);

}
