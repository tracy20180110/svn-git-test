package com.biostime.samanage.service.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActResBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerResBean;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface SaLecturerService {

    /**
     * SA讲师看板
     * @param reqBean
     * @return
     */
    public ServiceBean<List<SaLecturerResBean>> querySaLecturerInfo(SaLecturerReqBean reqBean) ;

    /**
     * SA指标
     * @param reqBean
     * @return
     */
    public ServiceBean<List<BaseKeyValueBean>> querySaIndex(SaLecturerReqBean reqBean) ;

    /**
     * SA讲师活动报表查詢
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaLecturerActResBean>> querySaLecturerActList(Pager<SaLecturerActResBean> pager, SaLecturerActReqBean reqBean);

    /**
     * SA讲师活动报表查詢-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaLecturerActList(HttpServletResponse response, SaLecturerActReqBean reqBean);


    /**
     * SA讲师活动报表查詢-线下导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaLecturerActDownList(HttpServletResponse response, SaLecturerActReqBean reqBean);

}
