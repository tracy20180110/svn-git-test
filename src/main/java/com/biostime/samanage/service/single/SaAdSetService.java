package com.biostime.samanage.service.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.single.adset.SaAdSetExternalResBean;
import com.biostime.samanage.bean.single.adset.SaAdSetReqBean;
import com.biostime.samanage.bean.single.adset.SaAdSetResBean;

import javax.servlet.http.HttpServletResponse;


/**
 * Describe:SA广告图Service
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface SaAdSetService {


    /**
     * SA广告图查询列表
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaAdSetResBean>> querySaAdSetList(Pager<SaAdSetResBean> pager, SaAdSetReqBean reqBean);

    /**
     * SA广告图查询列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaAdSetList(HttpServletResponse response, SaAdSetReqBean reqBean);

    /**
     * SA广告图(手机端)
     * @param reqBean
     * @return
     */
    public ServiceBean saveSaAdSet(SaAdSetReqBean reqBean);

    /**
     * 查询SA广告图片
     * @param saCode
     * @return
     */
    public ServiceBean<SaAdSetExternalResBean> querySaAdSetImage(String saCode) ;


    /**
     * SA广告图片状态修改
     * @param reqBean
     * @return
     */
    public ServiceBean changeStatusSaAdSet(SaAdSetReqBean reqBean);

    /**
     * SA广告图片验证门店信息
     * @param reqBean
     * @return
     */
    public ServiceBean checkTerminalInfo(SaAdSetReqBean reqBean);


}
