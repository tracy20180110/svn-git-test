package com.biostime.samanage.service.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportReqBean;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA微信会员报表Service
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaWxCustomerReportService {
    /**
     * SA微信会员报表
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaWxCustomerReportResBean>> querySaWxCustomerReportList(Pager<SaWxCustomerReportResBean> pager, SaWxCustomerReportReqBean reqBean);

    /**
     * SA微信会员报表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaWxCustomerReportList(HttpServletResponse response, SaWxCustomerReportReqBean reqBean);

}

