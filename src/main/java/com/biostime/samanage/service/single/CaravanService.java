package com.biostime.samanage.service.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.caravan.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:大小篷车申请
 * Date: 2017-12-15
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface CaravanService {

    /**
     * 查询分销物料信息
     * @return
     */
    public ServiceBean<List<CaravanCmaterialInfo>> queryCaravanMaterielList(CaravanReqBean reqBean);

    /**
     * 大小篷车申请
     * @return
     */
    public ServiceBean saveCaravan(CaravanReqBean reqBean,HttpServletRequest request);

    /**
     * 大小篷车申请OA审批
     * @return
     */
    public ServiceBean caravanOaAudit(CaravanReqBean reqBean,HttpServletRequest request);

    /**
     * 重新推送获取订单
     * @return
     */
    public ServiceBean caravanOrder(CaravanReqBean reqBean,HttpServletRequest request);

    /**
     * 大小篷车状态更新
     * @return
     */
    public ServiceBean editCaravanStatus(CaravanReqBean reqBean,HttpServletRequest request);

    /**
     * 大小篷车上传竣工图片及审核
     * @return
     */
    public ServiceBean saveCaravanImgAudit(CaravanImgAuditReqBean reqBean,HttpServletRequest request);

    /**
     * 更新大小篷车OA流程审批状态
     * @return
     */
    public ServiceBean editCaravanOaAuditStatus(CaravanReqBean reqBean);

    /**
     * 大小篷车列表查询
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<CaravanListResBean>> querySaCaravanList(Pager<CaravanListResBean> pager, CaravanListReqBean reqBean);

    /**
     * 大小篷车列表--导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaCaravanList(HttpServletResponse response, CaravanListReqBean reqBean);

    /**
     * 查询SA门店
     * @param reqBean
     * @return
     */
    public ServiceBean querySaTerminalInfo(CaravanReqBean reqBean);

    /**
     * 查询大小篷车信息
     * @param reqBean
     * @return
     */
    public ServiceBean<CaravanReqBean> queryCaravanInfo(CaravanReqBean reqBean);


    /**
     * 查询可更新的人员列表
     * @param reqBean
     * @return
     */
    public ServiceBean<List<String>> queryUpdateByList(CaravanListReqBean reqBean);

    /**
     * 更新申请人
     * @param reqBean
     * @return
     */
    public ServiceBean editUpdateBy(CaravanReqBean reqBean);

    /**
     * 查询大小篷车操作日志
     * @return
     */
    public ServiceBean<List<CaravanLogInfo>> queryCaravanLogList(CaravanReqBean reqBean);

    /**
     * 查询TPM费用列表
     * @return
     */
    public ServiceBean<List<CaravanTpmCostInfo>> queryTpmCostList(CaravanTpmCostInfo reqBean);
}
