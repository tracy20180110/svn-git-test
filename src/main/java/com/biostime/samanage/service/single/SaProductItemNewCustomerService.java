package com.biostime.samanage.service.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerReqBean;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA品项新客报表(新)Service
 * Date: 2019-12-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface SaProductItemNewCustomerService {
    /**
     * SA品项新客报表-查询
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaProductItemNewCustomerResBean>> querySaProductItemNewCustomerList(Pager<SaProductItemNewCustomerResBean> pager, SaProductItemNewCustomerReqBean reqBean);

    /**
     * SA品项新客报表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaProductItemNewCustomerList(HttpServletResponse response, SaProductItemNewCustomerReqBean reqBean);


}
