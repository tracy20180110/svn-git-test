package com.biostime.samanage.service.saPrize;

import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.saPrize.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA. Describe:获取SA育婴顾问奖金Service Date: 2016-6-23 Time:
 * 11:37 User: 12804 Version:1.0
 */
public interface SaPrizeService {

	/**
	 * 获取SA育婴顾问奖金
	 * 
	 * @param saPrizeReqBean
	 * @return
	 */
	public ServiceBean<List<SaPrizeResBean>> querySaPrizeList(SaPrizeReqBean saPrizeReqBean);

	/**
	 * 获取SA育婴顾问奖金明细
	 * 
	 * @param saPrizeDetailReqBean
	 * @return
	 */
	public ServiceBean<List<SaPrizeDetailResBean>> querySaPrizeDetailInfo(SaPrizeDetailReqBean saPrizeDetailReqBean);

	/**
	 * 获取SA考核指标信息，APP调用
	 * 
	 * @param saPrizeReqBean
	 * @return
	 */
	public ServiceBean<SaAssessIndexResBean> querySaAssessIndexInfo(SaPrizeDetailReqBean saPrizeReqBean);

}
