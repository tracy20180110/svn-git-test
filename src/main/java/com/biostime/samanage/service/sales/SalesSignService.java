package com.biostime.samanage.service.sales;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.sales.SalesSignMerchantReqBean;
import com.biostime.samanage.bean.sales.SalesSignReqBean;
import com.biostime.samanage.bean.sales.SalesSignResBean;

import javax.servlet.http.HttpServletResponse;


/**
 * Describe:促销员打卡Service
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface SalesSignService {


    /**
     * 促销员打卡查询列表
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SalesSignResBean>> querySalesSignList(Pager<SalesSignResBean> pager, SalesSignReqBean reqBean);

    /**
     * 促销员打卡查询列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSalesSignList(HttpServletResponse response, SalesSignReqBean reqBean);

    /**
     * 促销员打卡(手机端)
     * @param reqBean
     * @return
     */
    public ServiceBean saveSalesSign(SalesSignMerchantReqBean reqBean);

    /**
     * 查询是否在指定的门店(手机端)
     * @param reqBean
     * @return
     */
    public ServiceBean queryIsInTerminal(SalesSignMerchantReqBean reqBean);


}
