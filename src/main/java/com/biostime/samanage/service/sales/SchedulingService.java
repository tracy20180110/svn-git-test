package com.biostime.samanage.service.sales;

import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.sales.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * Describe:促销员打卡Service
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface SchedulingService {


    /**
     * 查询排班详情
     * @param reqBean
     * @return
     */
    public ServiceBean<List<SchedulingResBean>> querySchedulingInfo(SchedulingReqBean reqBean) ;


    /**
     * 查询排班管理详情
     * @param reqBean
     * @return
     */
    public ServiceBean<List<SchedulingManageResBean>> querySchedulingManageInfo(SchedulingManageReqBean reqBean) ;


    /**
     * 查询人员与终端
     * @param reqBean
     * @return
     */
    public ServiceBean<List<BaseKeyValueBean>> queryAccountTerminal(SchedulingReqBean reqBean) ;



    /**
     * 保存排班
     * @param reqBean
     * @return
     */
    public ServiceBean saveScheduling(SchedulingReqBean reqBean);

    /**
     * 排班查询列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSchedulingList(HttpServletResponse response, SchedulingManageReqBean reqBean);


}
