package com.biostime.samanage.service;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 类功能描述: SA促销员工作排班
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年6月23日 上午11:18:32
 */
public interface JobSchedulingService {

	/**
	 * 方法描述: 根据促销员编号和日期，查询当天排班
	 *
	 * @param customerId
	 * @param companyDate
	 * @return
	 * @author zhuhaitao
	 * @createDate 2016年6月23日 上午11:18:46
	 */
	ServiceBean<List<SaJobSchedulingBean>> getJobScheduling(String salesAccountNo, String companyDate);

	/** 
	* 方法描述: 保存促销员排班
	*
	* @param saJobSchedulingBeans
	* @author zhuhaitao
	 * @return 
	* @createDate 2016年6月23日 下午2:46:18
	*/
	ServiceBean<String> saveJobScheduling(AddSaJobSchedulingBean addSaJobSchedulingBean);

	/** 
	* 方法描述: 查询促销员工作排班列表
	*
	* @param officeCode
	* @param month
	* @return
	* @author zhuhaitao
	* @createDate 2016年6月23日 下午5:18:14
	*/
	ServiceBean<List<SaJobSchedulingSalesAccountBean>> queryJobScheduling(String officeCode, String month, String buCode);

	/** 
	* 方法描述: 根据SA编号查询SA名称
	*
	* @param saCode
	* @return
	* @author zhuhaitao
	* @createDate 2016年6月24日 下午5:03:15
	*/
	ServiceBean<SaTerminalBean> getSaNameBySaCode(String saCode);

	/** 
	* 方法描述: 根据促销员编号查询当月排班状态信息
	*
	* @param salesAccountNo
	* @param month
	* @return
	* @author zhuhaitao
	* @createDate 2016年6月27日 下午2:21:13
	*/
	ServiceBean<List<SaJobSchedulingStatusBean>> getJobSchedulingStatus(String salesAccountNo, String month);

	/**
	* 方法描述: 保存促销员排班状态信息
	*
	* @param saJobSchedulingStatusBean
	* @return
	* @author zhuhaitao
	* @createDate 2016年6月27日 下午2:20:58
	*/
	ServiceBean<String> saveJobSchedulingStatus(SaJobSchedulingStatusBean saJobSchedulingStatusBean);


    /**
     * 方法描述: SA工作排班报表
     *
     * @param pager
     * @param saJobScheduling
     * @return
     */
    ServiceBean<Pager<SaJobSchedulingBean>> searchJobScheduling(Pager<SaJobSchedulingBean> pager, SaJobSchedulingBean saJobSchedulingBean);

    /**
     * 方法描述: 导出SA育婴顾问工作排班报表
     * @param salesAccountNo
     * @param areaCode
     * @param officeCode
     * @param startMonth
     * @param endMonth
     * @param response
     * @return
     */
    ServiceBean exportJobScheduling(String salesAccountNo, String areaCode, String officeCode, String startMonth, String endMonth,String buCode, HttpServletResponse response);
}
