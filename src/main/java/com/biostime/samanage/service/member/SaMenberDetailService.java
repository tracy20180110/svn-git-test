package com.biostime.samanage.service.member;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.member.ExgPlmOutImportResult;
import com.biostime.samanage.bean.member.MemberDetailBean;
import com.biostime.samanage.bean.member.MktImportInfoBean;
import com.biostime.samanage.bean.member.NewMemberBean;
import com.biostime.samanage.domain.MktImportInfo;
import org.apache.poi.xssf.usermodel.XSSFRow;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * SA会员明细
 * Created by zhuhaitao on 2017/1/3.
 */
public interface SaMenberDetailService {
    /**
     * 分页查询SA会员明细
     *
     * @param pager
     * @param memberDetailBean
     * @return
     */
    ServiceBean<Pager<MemberDetailBean>> searchSaMenberDetail(Pager<MemberDetailBean> pager, MemberDetailBean memberDetailBean);

    /**
     * 导出SA会员明细
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param proBcc
     * @param srcLocName
     * @param startTime
     * @param endTime
     * @param response
     * @return
     */
    ServiceBean exportSaMenberDetail(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobile, String proBcc, String srcLocName, String startTime, String endTime, HttpServletResponse response);

    ServiceBean<Pager<MktImportInfoBean>> searchMktImportInfo(Pager<MktImportInfoBean> pager, MktImportInfoBean importInfoBean);

    ServiceBean<MktImportInfo> saveMktImportInfoBean(String filePath, String fileNameNow, String userId, Date date, int total, String type, String userName);

    ServiceBean<Map<String,Object>> importNewMember(XSSFRow row, int failed, List<String> descriptions, ExgPlmOutImportResult result, String userId);

    ServiceBean<String> addNewMember(List<NewMemberBean> newMemberBeanList) throws Exception;
}
