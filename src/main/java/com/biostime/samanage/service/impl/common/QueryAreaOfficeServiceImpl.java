package com.biostime.samanage.service.impl.common;

import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.common.AreaOfficeReqBean;
import com.biostime.samanage.bean.common.AreaOfficeResBean;
import com.biostime.samanage.repository.common.QueryAreaOfficeRepository;
import com.biostime.samanage.service.common.QueryAreaOfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取用户所属的大区办事处impl
 * Date: 2016-6-22
 * Time: 9:54
 * User: 12804
 * Version:1.0
 */
@Service("queryAreaOfficeService")
public class QueryAreaOfficeServiceImpl implements QueryAreaOfficeService{

    @Autowired
    private QueryAreaOfficeRepository queryAreaOfficeRepository;

    /**
     * 获取用户所属的大区办事处
     * @param areaOfficeReqBean
     * @return
     */
    @Override
    public ServiceBean<List<AreaOfficeResBean>> queryUserAreaOffice(AreaOfficeReqBean areaOfficeReqBean) {

        ServiceBean<List<AreaOfficeResBean>> serviceBean = new ServiceBean<List<AreaOfficeResBean>>();
        try {
            String areaOfficeCode = areaOfficeReqBean.getAreaOfficeCode(); //登录人所属大区办事处
            List<AreaOfficeResBean> areaOfficeResBean = queryAreaOfficeRepository.getUserAreaOffice(areaOfficeCode);
            serviceBean.setResponse(areaOfficeResBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }
}
