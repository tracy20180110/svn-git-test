package com.biostime.samanage.service.impl.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.terminal.SaTerminalManageEditReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewResBean;
import com.biostime.samanage.repository.terminal.SaTerminalManageNewRepository;
import com.biostime.samanage.service.terminal.SaTerminalManageNewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA与门店管理ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Service("saTerminalManageNewService")
@Slf4j
public class SaTerminalManageNewServiceImpl implements SaTerminalManageNewService {


    @Autowired
    private SaTerminalManageNewRepository saTerminalManageNewRepository;

    /**
     * SA与门店管理
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaTerminalManageNewResBean>> querySaTerminalManageNewList(Pager<SaTerminalManageNewResBean> pager, SaTerminalManageNewReqBean reqBean) {
        ServiceBean<Pager<SaTerminalManageNewResBean>> serviceBean = new ServiceBean<Pager<SaTerminalManageNewResBean>>();
        try {
            serviceBean.setResponse(saTerminalManageNewRepository.querySaTerminalManageNewList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店管理-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaTerminalManageNewList(HttpServletResponse response, SaTerminalManageNewReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saTerminalManageNewRepository.exportSaTerminalManageNewList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店绑定关系
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSaTerminalRelationNew(SaTerminalManageNewReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = saTerminalManageNewRepository.saveSaTerminalRelationNew(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询门店渠道、状态
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<SaTerminalManageNewResBean> queryTerminalInfo(SaTerminalManageNewReqBean reqBean) {
        ServiceBean<SaTerminalManageNewResBean> serviceBean = new ServiceBean<SaTerminalManageNewResBean>();
        try {
            serviceBean = saTerminalManageNewRepository.queryTerminalInfo(reqBean);
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店管理-编辑
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean editSaTerminalManage(SaTerminalManageEditReqBean reqBean) {
        return null;
    }


}
