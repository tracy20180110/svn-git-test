package com.biostime.samanage.service.impl.sign;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.sign.SaSignMerchantReqBean;
import com.biostime.samanage.bean.sign.SaSignReqBean;
import com.biostime.samanage.bean.sign.SaSignResBean;
import com.biostime.samanage.bean.sign.SaSignTerminalResBean;
import com.biostime.samanage.repository.sign.SaSignRepository;
import com.biostime.samanage.service.sign.SaSignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:促销员打卡ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("saSignService")
@Slf4j
public class SaSignServiceImpl implements SaSignService {
    @Autowired
    private SaSignRepository saSignRepository;

    /**
     * 促销员打卡查询列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaSignResBean>> querySaSignList(Pager<SaSignResBean> pager, SaSignReqBean reqBean) {
        ServiceBean<Pager<SaSignResBean>> serviceBean = new ServiceBean<Pager<SaSignResBean>>();
        try {
            serviceBean.setResponse(saSignRepository.querySaSignList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}", ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 促销员打卡查询列表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaSignList(HttpServletResponse response, SaSignReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saSignRepository.exportSaSignList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 促销员打卡
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSaSign(SaSignMerchantReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            Integer terminalNum = reqBean.getTerminalNum();
            Integer type = reqBean.getType();
            if (type == 2) {
                int count = saSignRepository.getSaSingCount(reqBean.getCreatedBy(), reqBean.getTerminalCode(), terminalNum);
                if (count != 1) {
                    String code = saSignRepository.getSaSingTerminalCode(reqBean.getCreatedBy());
                    serviceBean.setCode(3001);
                    serviceBean.setDesc(code + PropUtils.getProp(3001));
                    return serviceBean;
                }
            }
            /**
             * 如果是第二家门店上班打卡，则验证当天打卡门店数是否=2，不等于2则第一家未完成下班打卡，不允许第二家打卡
             */
            if (type == 1 && terminalNum == 2) {
                int count = saSignRepository.getSaSingCountOn(reqBean.getCreatedBy());
                if (count != 2) {
                    String code = saSignRepository.getSaSingTerminalCode(reqBean.getCreatedBy());
                    serviceBean.setCode(3001);
                    serviceBean.setDesc(code + PropUtils.getProp(3001));
                    return serviceBean;
                }
            }
            if (type == 1) {
                int count = saSignRepository.getSaSingCountOn(reqBean.getCreatedBy());
                if (count == 1 || count == 3) {
                    serviceBean.setCode(3001);
                    serviceBean.setDesc("已完成上班打卡，请勿重复操作！");
                    return serviceBean;
                }
            }
            saSignRepository.saveSaSign(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 促销员打卡(手机端)
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<SaSignTerminalResBean>> queryIsInTerminal(SaSignMerchantReqBean reqBean) {
        ServiceBean<List<SaSignTerminalResBean>> serviceBean = new ServiceBean();
        try {
            serviceBean = saSignRepository.queryIsInTerminal(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
