package com.biostime.samanage.service.impl;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.*;
import com.biostime.samanage.domain.SaJobScheduling;
import com.biostime.samanage.domain.SaJobSchedulingStatus;
import com.biostime.samanage.domain.SaTerminal;
import com.biostime.samanage.repository.JobSchedulingRepository;
import com.biostime.samanage.service.JobSchedulingService;
import com.biostime.samanage.util.DateUtils;
import com.biostime.samanage.util.ExportUtil;
import com.biostime.samanage.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 类功能描述: SA工作排班
 *
 * @author zhuhaitao
 * @version 1.0
 * @createDate 2016年6月23日 上午11:22:26
 */
@Service("jobSchedulingService")
@Slf4j
public class JobSchedulingServiceImpl implements JobSchedulingService {
    @Autowired
    private JobSchedulingRepository jobSchedulingRepository;

    /**
     * 根据促销员编号和日期，查询当天排班
     */
    @Override
    public ServiceBean<List<SaJobSchedulingBean>> getJobScheduling(String salesAccountNo, String companyDate) {

        ServiceBean<List<SaJobSchedulingBean>> serviceBean = new ServiceBean<List<SaJobSchedulingBean>>();
        try {

            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(companyDate);
            Date startTime = DateUtils.getTimesmorning(date);
            Date endTime = DateUtils.getTimesnight(date);

            List<SaJobScheduling> saIobSchedulings = jobSchedulingRepository.getJobScheduling(salesAccountNo,
                    startTime, endTime);

            serviceBean.setResponse(MapperUtils.map(saIobSchedulings, SaJobSchedulingBean.class));

        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<List<SaJobSchedulingBean>>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }

    /**
     * 保存工作排班
     */
    @Override
    public ServiceBean<String> saveJobScheduling(AddSaJobSchedulingBean addSaJobSchedulingBean) {
        ServiceBean<String> serviceBean = new ServiceBean<String>();
        try {
            /**
             * 先删除旧数据
             */
            String salesAccountNo = addSaJobSchedulingBean.getSalesAccountNo();
            String companyDate = addSaJobSchedulingBean.getCompanyDate();

            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(companyDate);

            jobSchedulingRepository.deleteByCustomerIdAndCompanyDate(salesAccountNo, date);

            if (CollectionUtils.isNotEmpty(addSaJobSchedulingBean.getSaJobSchedulingBeans())) {

                List<SaJobScheduling> saJobSchedulings = MapperUtils.map(
                        addSaJobSchedulingBean.getSaJobSchedulingBeans(), SaJobScheduling.class);

                for (SaJobScheduling saJobScheduling : saJobSchedulings) {
                    saJobScheduling.setSalesAccountNo(Long.parseLong(salesAccountNo));
                    saJobScheduling.setCompanyDate(date);
                    jobSchedulingRepository.saveJobScheduling(saJobScheduling);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<String>(500, ex.getMessage().toString());//系统异常
        }
        return serviceBean;
    }

    /**
     * 根据办事处编号查询促销员排班列表
     */
    @Override
    public ServiceBean<List<SaJobSchedulingSalesAccountBean>> queryJobScheduling(String officeCode, String month, String buCode) {

        ServiceBean<List<SaJobSchedulingSalesAccountBean>> serviceBean = new ServiceBean<List<SaJobSchedulingSalesAccountBean>>();
        try {
            String sysMonth = new SimpleDateFormat("yyyy-MM").format(new Date());
            Date startTime = null;
            Date endTime = null;
            /**
             * 当前月，则查询当天是否有排班
             */
            if (null != month && sysMonth.equals(month)) {
                startTime = DateUtils.getTimesmorning(new Date());
                endTime = DateUtils.getTimesnight(new Date());
            }
            /**
             * 不是当前月，则查询该月是否有排班
             */
            else {
                startTime = DateUtils.getFirstDay(month);
                endTime = DateUtils.getLastDay(month);
            }

            List<SaJobSchedulingSalesAccountBean> saIobSchedulings = jobSchedulingRepository.queryJobScheduling(
                    officeCode, startTime, endTime,buCode);

            serviceBean.setResponse(MapperUtils.map(saIobSchedulings, SaJobSchedulingSalesAccountBean.class));

        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<List<SaJobSchedulingSalesAccountBean>>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }

    /**
     * 根据SA编号查询SA名称
     */
    @Override
    public ServiceBean<SaTerminalBean> getSaNameBySaCode(String saCode) {
        ServiceBean<SaTerminalBean> serviceBean = new ServiceBean<SaTerminalBean>();
        try {

            SaTerminal sa = jobSchedulingRepository.getSaNameBySaCode(saCode);
            if (null != sa) {
                serviceBean.setResponse(MapperUtils.map(sa, SaTerminalBean.class));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<SaTerminalBean>(500, ex.getMessage().toString());//系统异常
        }
        return serviceBean;
    }

    /**
     * 根据促销员编号查询当月排班状态信息
     */
    @Override
    public ServiceBean<List<SaJobSchedulingStatusBean>> getJobSchedulingStatus(String salesAccountNo, String month) {

        ServiceBean<List<SaJobSchedulingStatusBean>> serviceBean = new ServiceBean<List<SaJobSchedulingStatusBean>>();
        try {

            Date startTime = DateUtils.getFirstDay(month);
            Date endTime = DateUtils.getLastDay(month);
            List<SaJobSchedulingStatus> saIobSchedulings = jobSchedulingRepository.getJobSchedulingStatus(
                    salesAccountNo, startTime, endTime);

            serviceBean.setResponse(MapperUtils.map(saIobSchedulings, SaJobSchedulingStatusBean.class));

        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<List<SaJobSchedulingStatusBean>>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }

    /**
     * 保存促销员排班状态信息
     */
    @Override
    public ServiceBean<String> saveJobSchedulingStatus(SaJobSchedulingStatusBean saJobSchedulingStatusBean) {
        ServiceBean<String> serviceBean = new ServiceBean<String>();
        try {
            if (null != saJobSchedulingStatusBean) {
                SaJobSchedulingStatus saJobSchedulingStatus = MapperUtils.map(saJobSchedulingStatusBean,
                        SaJobSchedulingStatus.class);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date companyDate = sdf.parse(sdf.format(saJobSchedulingStatus.getCompanyDate()));
                saJobSchedulingStatus.setCompanyDate(companyDate);
                jobSchedulingRepository.saveJobSchedulingStatus(saJobSchedulingStatus);
                //如果休息，则删除排班明细
                if (saJobSchedulingStatus.getStatus() == 0){
                    jobSchedulingRepository.deleteByCustomerIdAndCompanyDate(saJobSchedulingStatus.getSalesAccountNo().toString(), companyDate);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<String>(500, ex.getMessage().toString());//系统异常
        }
        return serviceBean;
    }

    /**
     * SA工作排班报表
     *
     * @param pager
     * @param saJobSchedulingBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaJobSchedulingBean>> searchJobScheduling(Pager<SaJobSchedulingBean> pager, SaJobSchedulingBean saJobSchedulingBean) {

        ServiceBean<Pager<SaJobSchedulingBean>> serviceBean = new ServiceBean<Pager<SaJobSchedulingBean>>();
        try {
            pager = jobSchedulingRepository.searchJobScheduling(pager, saJobSchedulingBean);
            serviceBean.setResponse(pager);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;

    }

    /**
     * 导出SA育婴顾问工作排班报表
     *
     * @param salesAccountNo
     * @param areaCode
     * @param officeCode
     * @param startMonth
     * @param endMonth
     * @param response
     * @return
     */
    @Override
    public ServiceBean exportJobScheduling(String salesAccountNo, String areaCode, String officeCode, String startMonth, String endMonth,String buCode, HttpServletResponse response) {

        ServiceBean<List<SaJobSchedulingBean>> serviceBean = new ServiceBean<List<SaJobSchedulingBean>>();
        try {
            List<SaJobSchedulingBean> lists = new ArrayList<SaJobSchedulingBean>();

            lists = jobSchedulingRepository.getJobScheduling(salesAccountNo,areaCode,officeCode,startMonth,endMonth,buCode);

            if (CollectionUtils.isNotEmpty(lists)) {
                writeReportExcel(response, "育婴顾问工作排班信息", lists);
            } else {
                serviceBean.setCode(5000);
                serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<List<SaJobSchedulingBean>>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;

    }

    /**
     * 方法描述: 根据传进去的参数导出.xlsx文档
     *
     * @param response
     * @param fileName
     * @param list
     * @author zhuhaitao
     * @createDate 2016年1月12日 下午6:54:34
     */
    private String writeReportExcel(HttpServletResponse response, String fileName, List<SaJobSchedulingBean> list) {
        String url = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HHmmss");
            fileName = fileName + sdf.format(new Date());
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition",
                    "attachment; filename=" + URLEncoder.encode(new String(fileName + ".xlsx"), "UTF-8"));

            url = this.writeAllBrandsExcel(fileName, list, response);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            try {
                response.reset();
                response.setContentType("text/html;charset=utf-8");
                response.getWriter().print("导出失败，数据量过大");
            } catch (Exception e1) {
                log.error(e1.getMessage(), e1);
            }
        }
        return url;
    }

    /**
     * 方法描述: 导出数据所需要的参数
     *
     * @param fileName
     * @param dataList
     * @param response
     * @return
     * @throws FileNotFoundException
     */
    private String writeAllBrandsExcel(String fileName, List<SaJobSchedulingBean> dataList, HttpServletResponse response) throws FileNotFoundException {

        String path = "";
        // 复合表头
        String titleName[] = new String[]{"基本资料"};
        // 复合表头起止列
        Integer titleRange[][] = new Integer[][]{{0, 2}};
        // 数据列表头
        String ths[] = new String[]{
                // 基本资料
                "序号", "日期", "大区", "办事处", "渠道", "育婴顾问工号", "育婴顾问姓名", "排班状态", "时间", "SA编码", "SA名称", "微信会员", "到店数", "SA新客", "备注"};
        // 数据列
        String colNames[] = new String[]{
                // 基本资料
                "serialNumber", "month", "areaname", "officename", "channel", "salesAccountNo", "salesName", "status",  "timeBucketStr",
                "saCode", "saName", "weixinCustomerCount", "arriveShopCount", "saNewCustomerCount", "remarks"};

        ExportUtil.wirteDefaultExcel(titleName, titleRange, dataList, ths, colNames, response);

        return path;
    }
}
