package com.biostime.samanage.service.impl.sales;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.sales.SalesSignMerchantReqBean;
import com.biostime.samanage.bean.sales.SalesSignReqBean;
import com.biostime.samanage.bean.sales.SalesSignResBean;
import com.biostime.samanage.repository.sales.SalesSignRepository;
import com.biostime.samanage.service.sales.SalesSignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Describe:促销员打卡ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("salesSignService")
@Slf4j
public class SalesSignServiceImpl implements SalesSignService {
    @Autowired
    private SalesSignRepository salesSignRepository;

    /**
     * 促销员打卡查询列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SalesSignResBean>> querySalesSignList(Pager<SalesSignResBean> pager, SalesSignReqBean reqBean) {
        ServiceBean<Pager<SalesSignResBean>> serviceBean = new ServiceBean<Pager<SalesSignResBean>>();
        try {
            serviceBean.setResponse(salesSignRepository.querySalesSignList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 促销员打卡查询列表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSalesSignList(HttpServletResponse response, SalesSignReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            salesSignRepository.exportSalesSignList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 促销员打卡
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSalesSign(SalesSignMerchantReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            salesSignRepository.saveSalesSign(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 促销员打卡(手机端)
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean queryIsInTerminal(SalesSignMerchantReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = salesSignRepository.queryIsInTerminal(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
