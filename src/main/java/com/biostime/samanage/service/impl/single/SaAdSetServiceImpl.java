package com.biostime.samanage.service.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.single.adset.SaAdSetExternalResBean;
import com.biostime.samanage.bean.single.adset.SaAdSetReqBean;
import com.biostime.samanage.bean.single.adset.SaAdSetResBean;
import com.biostime.samanage.repository.single.SaAdSetRepository;
import com.biostime.samanage.service.single.SaAdSetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA广告图设置ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("saAdSetService")
@Slf4j
public class SaAdSetServiceImpl implements SaAdSetService {
    @Autowired
    private SaAdSetRepository saAdSetRepository;

    /**
     * SA广告图设置查询列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaAdSetResBean>> querySaAdSetList(Pager<SaAdSetResBean> pager, SaAdSetReqBean reqBean) {
        ServiceBean<Pager<SaAdSetResBean>> serviceBean = new ServiceBean<Pager<SaAdSetResBean>>();
        try {
            serviceBean.setResponse(saAdSetRepository.querySaAdSetList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA广告图设置查询列表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaAdSetList(HttpServletResponse response, SaAdSetReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saAdSetRepository.exportSaAdSetList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA广告图设置
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSaAdSet(SaAdSetReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = saAdSetRepository.saveSaAdSet(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询SA广告图片
     *
     * @param saCode
     * @return
     */
    @Override
    public ServiceBean<SaAdSetExternalResBean> querySaAdSetImage(String saCode) {
        ServiceBean<SaAdSetExternalResBean> serviceBean = new ServiceBean<SaAdSetExternalResBean>();
        try {
            serviceBean.setResponse(saAdSetRepository.querySaAdSetImage(saCode));
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA广告图片状态修改
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean changeStatusSaAdSet(SaAdSetReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saAdSetRepository.changeStatusSaAdSet(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA广告图片验证门店信息
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean checkTerminalInfo(SaAdSetReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            serviceBean = saAdSetRepository.checkTerminalInfo(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }


}
