package com.biostime.samanage.service.impl.quota;

import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.quota.DoubleSaQuotaBean;
import com.biostime.samanage.bean.quota.SaQuotaTerminalBean;
import com.biostime.samanage.repository.quota.SaQuotaRepository;
import com.biostime.samanage.service.quota.SaQuotaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("saQuotaService")
@Slf4j
public class SaQuotaServiceImpl implements SaQuotaService {
    @Autowired
    private SaQuotaRepository saQuotaRepository;

    @Override
    public ServiceBean<SaQuotaTerminalBean> getTerminalBySaCode(String saCode) {
        ServiceBean<SaQuotaTerminalBean> serviceBean = new ServiceBean<SaQuotaTerminalBean>();
        try {

            SaQuotaTerminalBean saQuotaTerminalBean = saQuotaRepository.getTerminalBySaCode(saCode);
            serviceBean.setResponse(saQuotaTerminalBean);

        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<SaQuotaTerminalBean>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }

    @Override
    public ServiceBean<DoubleSaQuotaBean> getDoubleSaQuota(String saCode, String terminalCode, String startTime, String endTime, String chainCode) {
        ServiceBean<DoubleSaQuotaBean> serviceBean = new ServiceBean<DoubleSaQuotaBean>();
        try {

            DoubleSaQuotaBean doubleSaQuotaBean = saQuotaRepository.getDoubleSaQuota(saCode, terminalCode, startTime, endTime, chainCode);
            serviceBean.setResponse(doubleSaQuotaBean);

        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<DoubleSaQuotaBean>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }
}
