package com.biostime.samanage.service.impl.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigReqBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigResBean;
import com.biostime.samanage.repository.act.ActRoadShowConfigRepository;
import com.biostime.samanage.service.act.ActRoadShowConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * 路演活动ServiceImpl
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2018-03-13
 */
@Service("actRoadShowConfigService")
@Slf4j
public class ActRoadShowConfigServiceImpl implements ActRoadShowConfigService {


    @Autowired
    private ActRoadShowConfigRepository actRoadShowConfigRepository;


    /**
     * 路演活动配置查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<ActRoadShowConfigResBean>> queryActRoadShowConfigList(Pager<ActRoadShowConfigResBean> pager, ActRoadShowConfigReqBean reqBean) {
        ServiceBean<Pager<ActRoadShowConfigResBean>> serviceBean = new ServiceBean<Pager<ActRoadShowConfigResBean>>();
        try {
            serviceBean.setResponse(actRoadShowConfigRepository.queryActRoadShowConfigList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 路演活动配置导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportActRoadShowConfigList(HttpServletResponse response, ActRoadShowConfigReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            actRoadShowConfigRepository.exportActRoadShowConfigList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 路演活动关系状态修改
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean editStatusActRoadShowConfig(ActRoadShowConfigReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            actRoadShowConfigRepository.editStatusActRoadShowConfig(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 路演活动关系-新增
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveActRoadShowConfig(ActRoadShowConfigReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = actRoadShowConfigRepository.saveActRoadShowConfig(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }


}
