package com.biostime.samanage.service.impl.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.actreport.*;
import com.biostime.samanage.bean.actroadshow.ActReportDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowListReqBean;
import com.biostime.samanage.repository.act.ActRePortRepository;
import com.biostime.samanage.service.act.ActRePortService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Describe:活动上报
 * Date: 2016-10-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Service("actRePortService")
public class ActRePortServiceImpl implements ActRePortService {

    @Autowired
    private ActRePortRepository actRePortRepository;

    /**
     * 获取登陆人可上报的活动
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<ActRePortListResBean>> queryActRePortList(ActRePortListReqBean reqBean) {
        ServiceBean<List<ActRePortListResBean>> serviceBean = new ServiceBean<List<ActRePortListResBean>>();
        try {
            List<ActRePortListResBean> activityResBean = actRePortRepository.queryActRePortList(reqBean);
            serviceBean.setResponse(activityResBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 提交活动上报
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean submitActRePort(ActRePortReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = actRePortRepository.submitActRePort(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 获取登陆人可上报的活动
     *
     * @param terminalCode
     * @return
     */
    @Override
    public ServiceBean<ActRePortTypeResBean> queryTerminalChannel(String terminalCode,String reportTypeCode,String buCode) {
        ServiceBean<ActRePortTypeResBean> serviceBean = new ServiceBean<ActRePortTypeResBean>();
        try {
            ActRePortTypeResBean actRePortTypeResBean = actRePortRepository.queryTerminalChannel(terminalCode,reportTypeCode,buCode);
            if(null != actRePortTypeResBean){
                serviceBean.setResponse(actRePortTypeResBean);
            }else{
                serviceBean.setCode(501);
                serviceBean.setDesc("门店渠道不在所属范围内");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 路活动上报图片类型查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<ActRePortImgTypeResBean>> queryActRePortImgTypeList(Pager<ActRePortImgTypeResBean> pager, ActRePortImgTypeReqBean reqBean) {
        ServiceBean<Pager<ActRePortImgTypeResBean>> serviceBean = new ServiceBean<Pager<ActRePortImgTypeResBean>>();
        try {
            serviceBean.setResponse(actRePortRepository.queryActRePortImgTypeList(pager, reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 获取图片类型列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<ActRePortImgTypeResBean>> queryImgTypeList(ActRePortImgTypeReqBean reqBean) {
        ServiceBean<List<ActRePortImgTypeResBean>> serviceBean = new ServiceBean<List<ActRePortImgTypeResBean>>();
        try {
            List<ActRePortImgTypeResBean> actRePortImgTypeResBean = actRePortRepository.queryImgTypeList(reqBean);
            serviceBean.setResponse(actRePortImgTypeResBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 图片类型保存、编辑
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveActRePortImgType(ActRePortImgTypeReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = actRePortRepository.saveActRePortImgType(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 获取上报关联的SA活动
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<BaseKeyValueBean>> querySaFtfActList(ActRePortReqBean reqBean) {
        ServiceBean<List<BaseKeyValueBean>> serviceBean = new ServiceBean<List<BaseKeyValueBean>>();
        try {
            List<BaseKeyValueBean> activityResBean = actRePortRepository.querySaFtfActList(reqBean);
            serviceBean.setResponse(activityResBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 手机上报活动明细列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<ActReportDetailResBean> queryActReportDetail(ActRoadShowListReqBean reqBean) {
        ServiceBean<ActReportDetailResBean> serviceBean = new ServiceBean<ActReportDetailResBean>();
        try {
            serviceBean.setResponse(actRePortRepository.queryActReportDetail(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 手机上报活动明细详情列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<ActRoadShowDetailResBean>> queryActReportDetailInfo(ActRoadShowListReqBean reqBean) {
        ServiceBean<List<ActRoadShowDetailResBean>> serviceBean = new ServiceBean<List<ActRoadShowDetailResBean>>();
        try {
            serviceBean.setResponse(actRePortRepository.queryActReportDetailInfo(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询是否是活动精英
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean queryIsActElites(ActRoadShowListReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = actRePortRepository.queryIsActElites(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询活动精英所属办事处的门店
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<BaseKeyValueBean>> queryActElitesByOfficeTerminal(ActRoadShowListReqBean reqBean) {
        ServiceBean<List<BaseKeyValueBean>> serviceBean = new ServiceBean<>();
        try {
            List<BaseKeyValueBean> baseKeyValueBean = actRePortRepository.queryActElitesByOfficeTerminal(reqBean);
            serviceBean.setResponse(baseKeyValueBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询品牌官微二维码
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<ActRePortQrcodeResBean>> queryReportBrandQrCodeList(ActRePortReqBean reqBean) {

        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = actRePortRepository.queryReportBrandQrCodeList(reqBean);
            List<ActRePortQrcodeResBean> list = new ArrayList<ActRePortQrcodeResBean>();
            List<ActRePortQrcodeResBean> resultList = (List<ActRePortQrcodeResBean>)serviceBean.getResponse();
            if(CollectionUtils.isNotEmpty(resultList)) {
                list = resultList;
            }
            ActRePortQrcodeResBean resBean = actRePortRepository.queryReportActSignMiniCode(reqBean);
            if(null != resBean){
                list.add(resBean);
            }
            serviceBean.setResponse(list);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 活动上报直播
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveActRePortZb(ActRePortZhiBoReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = actRePortRepository.saveActRePortZb(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询直播人员
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<BaseKeyValueBean>> queryZhiBoAccount(BaseKeyValueBean reqBean) {
        ServiceBean<List<BaseKeyValueBean>> serviceBean = new ServiceBean<>();
        try {
            List<BaseKeyValueBean> baseKeyValueBean = actRePortRepository.queryZhiBoAccount(reqBean);
            serviceBean.setResponse(baseKeyValueBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * BNC活动精英指派门店
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<BaseKeyValueBean>> queryBncActElitesTerminal(ActRoadShowListReqBean reqBean) {
        ServiceBean<List<BaseKeyValueBean>> serviceBean = new ServiceBean<>();
        try {
            List<BaseKeyValueBean> baseKeyValueBean = actRePortRepository.queryBncActElitesTerminal(reqBean);
            serviceBean.setResponse(baseKeyValueBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
