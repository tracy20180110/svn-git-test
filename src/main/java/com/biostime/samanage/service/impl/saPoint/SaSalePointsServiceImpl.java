package com.biostime.samanage.service.impl.saPoint;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.saPoint.SaSalePointsBean;
import com.biostime.samanage.repository.saPoint.SaSalePointsRepository;
import com.biostime.samanage.service.saPoint.SaSalePointsService;
import com.biostime.samanage.util.ExportUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * SA销售积分
 */
@Service("saSalePointsService")
@Slf4j
public class SaSalePointsServiceImpl implements SaSalePointsService {
    @Autowired
    private SaSalePointsRepository saSalePointsRepository;

    /**
     * SA销售积分报表
     *
     * @param pager
     * @param saSalePointsBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaSalePointsBean>> searchSaSalePoints(Pager<SaSalePointsBean> pager, SaSalePointsBean saSalePointsBean) {

        ServiceBean<Pager<SaSalePointsBean>> serviceBean = new ServiceBean<Pager<SaSalePointsBean>>();
        try {
            pager = saSalePointsRepository.searchSaSalePoints(pager, saSalePointsBean);
            if (CollectionUtils.isNotEmpty(pager.getDatas())){
                for (SaSalePointsBean saSale: pager.getDatas()) {
                    if(StringUtil.isNotNullNorEmpty(saSale.getCustomerMobilePhone()) && saSale.getCustomerMobilePhone().length() == 11){
                        saSale.setCustomerMobilePhone(saSale.getCustomerMobilePhone().substring(0,3) + "****" + saSale.getCustomerMobilePhone().substring(7, saSale.getCustomerMobilePhone().length()));
                    }
                }
            }

            serviceBean.setResponse(pager);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;

    }

    /**
     * 导出SA销售积分
     *
     * @param babyConsultant
     * @param areaCode
     * @param officeCode
     * @param startDate
     * @param endDate
     * @param bccCode
     * @param response
     * @return
     */
    @Override
    public ServiceBean exportSaSalePoints(String babyConsultant, String areaCode, String officeCode,String startDate, String endDate, String bccCode,String buCode, HttpServletResponse response) {

        ServiceBean<List<SaSalePointsBean>> serviceBean = new ServiceBean<List<SaSalePointsBean>>();
        try {
            List<SaSalePointsBean> lists = new ArrayList<SaSalePointsBean>();

            lists = saSalePointsRepository.getSaSalePoints(babyConsultant, areaCode, officeCode, startDate, endDate, bccCode,buCode);

            writeReportExcel(response, "SA销售积分", lists);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<List<SaSalePointsBean>>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;

    }

    /**
     * 方法描述: 根据传进去的参数导出.xlsx文档
     *
     * @param response
     * @param fileName
     * @param list
     * @author zhuhaitao
     * @createDate 2016年1月12日 下午6:54:34
     */
    private String writeReportExcel(HttpServletResponse response, String fileName, List<SaSalePointsBean> list) {
        String url = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HHmmss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            fileName = fileName + sdf.format(new Date());
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition",
                    "attachment; filename=" + URLEncoder.encode(new String(fileName + ".xlsx"), "UTF-8"));
            if (CollectionUtils.isNotEmpty(list)){
                for (SaSalePointsBean saSale: list) {
                    if(StringUtil.isNotNullNorEmpty(saSale.getCustomerMobilePhone()) && saSale.getCustomerMobilePhone().length() == 11){
                        saSale.setCustomerMobilePhone(saSale.getCustomerMobilePhone().substring(0,3) + "****" + saSale.getCustomerMobilePhone().substring(7, saSale.getCustomerMobilePhone().length()));
                    }if(null != saSale.getRegisterDate()){
                        saSale.setRegisterDateStr(sdf1.format(saSale.getRegisterDate()));
                    }
                }
            }


            url = this.writeAllBrandsExcel(fileName, list, response);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            try {
                response.reset();
                response.setContentType("text/html;charset=utf-8");
                response.getWriter().print("导出失败，数据量过大");
            } catch (Exception e1) {
                log.error(e1.getMessage(), e1);
            }
        }
        return url;
    }

    /**
     * 方法描述: 导出数据所需要的参数
     *
     * @param fileName
     * @param dataList
     * @param response
     * @return
     * @throws FileNotFoundException
     */
    private String writeAllBrandsExcel(String fileName, List<SaSalePointsBean> dataList, HttpServletResponse response) throws FileNotFoundException {

        String path = "";
        // 复合表头
        String titleName[] = new String[]{"基本资料"};
        // 复合表头起止列
        Integer titleRange[][] = new Integer[][]{{0, 2}};
        // 数据列表头
        String ths[] = new String[]{
                // 基本资料
                "大区",
                "办事处",
                "SA编码",
                "门店编码",
                "BCC工号",
                "BCC名称",
                "育婴顾问工号",
                "育婴顾问名称",
                "客户ID",
                "手机号码",
                "FP时间",
                "FP人员",
                "FP类型",
                "FP得悉途径",
                "品牌",
                "品规",
                "成为品规新客的首次购买产品ID",
                "成为品规新客的首次购买产品名称",
                "当次购买的产品ID",
                "当次购买的产品名称",
                "积分日期",
                "积分值",
                "次数",
                "积分途径",
                "是否订单积分",
                "是否绑定微信",
                "是否妈妈100APP用户"};
        // 数据列
        String colNames[] = new String[]{
                // 基本资料
                "areaName", "officeName", "saCode", "terminalCode", "proBcc", "proBcc", "babyConsultant", "babyConsultantName", "customerId",
                "customerMobilePhone", "lastPointTime", "lastPointOper", "lastPointType", "lastPointSrc", "brand", "productCustomizedType",
                "newCustomerProductId", "newCustomerProductName", "productId", "productName", "registerDateStr", "productPoints", "rank",
                "recSrc", "isO2OOrder", "isWeixinBind", "isAppUser"};

        ExportUtil.wirteDefaultExcel(null, titleRange, dataList, ths, colNames, response);

        return path;
    }
}
