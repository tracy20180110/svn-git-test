package com.biostime.samanage.service.impl.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.terminal.SaTerminalInfoReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalInfoResBean;
import com.biostime.samanage.repository.terminal.SaTerminalInfoRepository;
import com.biostime.samanage.service.terminal.SaTerminalInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA与门店管理ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("saTerminalInfoService")
@Slf4j
public class SaTerminalInfoServiceImpl implements SaTerminalInfoService {
    @Autowired
    private SaTerminalInfoRepository saTerminalInfoRepository;

    /**
     * SA门店查询列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaTerminalInfoResBean>> querySaTerminalInfoList(Pager<SaTerminalInfoResBean> pager, SaTerminalInfoReqBean reqBean) {
        ServiceBean<Pager<SaTerminalInfoResBean>> serviceBean = new ServiceBean<Pager<SaTerminalInfoResBean>>();
        try {
            serviceBean.setResponse(saTerminalInfoRepository.querySaTerminalInfoList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA门店查询列表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaTerminalInfoList(HttpServletResponse response, SaTerminalInfoReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saTerminalInfoRepository.exportSaTerminalInfoList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
