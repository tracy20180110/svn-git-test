package com.biostime.samanage.service.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerReqBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import com.biostime.samanage.repository.ftf.FtfLecturerRepository;
import com.biostime.samanage.service.ftf.FtfLecturerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF讲师ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("ftfLecturerService")
@Slf4j
public class FtfLecturerServiceImpl implements FtfLecturerService {

    @Autowired
    private FtfLecturerRepository ftfLecturerRepository;

    /**
     * 获取FTF讲师列表
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<FtfLecturerResBean>> queryFtfLecturerList(Pager<FtfLecturerResBean> pager, FtfLecturerReqBean reqBean) {
        ServiceBean<Pager<FtfLecturerResBean>> serviceBean = new ServiceBean<Pager<FtfLecturerResBean>>();
        try {
            serviceBean.setResponse(ftfLecturerRepository.queryFtfLecturerList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(2000);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF讲师列表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportFtfLecturerList(HttpServletResponse response, FtfLecturerReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            ftfLecturerRepository.exportFtfLecturerList(response,reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }



    /**
     * 讲师状态修改
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean changeStatusFtfLecturer(FtfLecturerReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            ftfLecturerRepository.changeStatusFtfLecturer(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF讲师保存
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveFtfLecturer(FtfLecturerReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            serviceBean.setResponse(ftfLecturerRepository.saveFtfLecturer(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 获取讲师名称工号
     * @param keyword
     * @return
     */
    @Override
    public ServiceBean queryLecturerCode(String keyword,String type) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean.setResponse(ftfLecturerRepository.queryLecturerCode(keyword,type));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 讲师认证-导入
     *
     * @param multipartFile
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<String> importLecturerCheck(MultipartFile multipartFile, FtfLecturerReqBean reqBean) {
        ServiceBean<String> serviceBean = new ServiceBean<String>();
        try {
            String msg = ftfLecturerRepository.importLecturerCheck(multipartFile, reqBean);
            if(StringUtil.isNotNullNorEmpty(msg)) {
                serviceBean.setCode(500);
                serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
                serviceBean.setResponse(msg);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
