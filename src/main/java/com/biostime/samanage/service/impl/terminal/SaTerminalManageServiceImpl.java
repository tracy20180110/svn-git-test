package com.biostime.samanage.service.impl.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.terminal.*;
import com.biostime.samanage.repository.terminal.SaTerminalManageRepository;
import com.biostime.samanage.service.terminal.SaTerminalManageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:SA与门店管理ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Service("saTerminalManageService")
@Slf4j
public class SaTerminalManageServiceImpl implements SaTerminalManageService {
    @Autowired
    private SaTerminalManageRepository saTerminalManageRepository;
    /**
     * SA与门店管理导入列表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaTerminalManageImportResBean>> querySaTerminalManageImportList(Pager<SaTerminalManageImportResBean> pager, SaTerminalManageImportReqBean reqBean) {
        ServiceBean<Pager<SaTerminalManageImportResBean>> serviceBean = new ServiceBean<Pager<SaTerminalManageImportResBean>>();
        try {
            serviceBean.setResponse(saTerminalManageRepository.querySaTerminalManageImportList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店管理导入列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaTerminalManageImportList(HttpServletResponse response, SaTerminalManageImportReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saTerminalManageRepository.exportSaTerminalManageImportList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店管理
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaTerminalManageResBean>> querySaTerminalManageList(Pager<SaTerminalManageResBean> pager, SaTerminalManageReqBean reqBean) {
        ServiceBean<Pager<SaTerminalManageResBean>> serviceBean = new ServiceBean<Pager<SaTerminalManageResBean>>();
        try {
            serviceBean.setResponse(saTerminalManageRepository.querySaTerminalManageList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店管理-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaTerminalManageList(HttpServletResponse response, SaTerminalManageReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saTerminalManageRepository.exportSaTerminalManageList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店管理-导入
     *
     * @param multipartFile
     * @param request
     * @return
     */
    @Override
    public ServiceBean importSaTerminalManage(MultipartFile multipartFile, HttpServletRequest request) {

        ServiceBean<List> serviceBean = new ServiceBean<List>();
        try {
            List list = saTerminalManageRepository.importSaTerminalManage(multipartFile, request);
            if(list.size() > 0) {
                serviceBean.setCode(500);
                serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
                serviceBean.setResponse(list);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店管理--删除
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean deleteSaTerminalManage(SaTerminalManageEditReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            saTerminalManageRepository.deleteSaTerminalManage(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店管理-编辑
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean editSaTerminalManage(SaTerminalManageEditReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            String msg = saTerminalManageRepository.editSaTerminalManage(reqBean);
            if(StringUtil.isNotNullNorEmpty(msg)){
                serviceBean.setCode(201);
                serviceBean.setDesc(msg);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA撤销结束与门店的关系
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean delSaTerminalRelation(SaTerminalManageReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            saTerminalManageRepository.delSaTerminalRelation(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA与门店管理(新)
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaTerminalManageResBean>> querySaTerminalNewManageList(Pager<SaTerminalManageResBean> pager, SaTerminalManageReqBean reqBean) {
        ServiceBean<Pager<SaTerminalManageResBean>> serviceBean = new ServiceBean<Pager<SaTerminalManageResBean>>();
        try {
            serviceBean.setResponse(saTerminalManageRepository.querySaTerminalNewManageList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }


    /**
     * SA与门店绑定关系
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSaTerminalRelation(SaTerminalManageReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = saTerminalManageRepository.saveSaTerminalRelation(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
