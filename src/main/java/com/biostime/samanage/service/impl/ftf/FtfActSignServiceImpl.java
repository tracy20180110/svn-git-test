package com.biostime.samanage.service.impl.ftf;

import com.alibaba.fastjson.JSON;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.ftf.sign.*;
import com.biostime.samanage.repository.ftf.FtfActSignRepository;
import com.biostime.samanage.service.ftf.FtfActSignService;
import com.mama100.rpc.cust.CustRpcFactory;
import com.mama100.rpc.cust.thrift.CustomerService;
import com.mama100.rpc.cust.thrift.PhoneService;
import com.mama100.rpc.cust.thrift.inout.*;
import com.mama100.rpc.cust.thrift.inout.common.BaseRequest;
import com.mama100.rpc.cust.thrift.inout.common.BoolResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF课程Service Impl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Service("ftfActSignService")
@Slf4j
public class FtfActSignServiceImpl implements FtfActSignService {

    @Autowired
    private FtfActSignRepository ftfActSignRepository;

    @Autowired
    private CustRpcFactory custRpcFactory;

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * FTF活动列表
     * @param ftfActSignReqBean
     * @return
     */
    @Override
    public ServiceBean<List<FtfActSignResBean>> queryMobFtfActList(FtfActSignReqBean ftfActSignReqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            List<FtfActSignResBean> ftfActSignResList = ftfActSignRepository.queryMobFtfActList(ftfActSignReqBean);
            serviceBean.setResponse(ftfActSignResList);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(1001);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 签到详情
     *
     * @param actId
     * @return
     */
    @Override
    public ServiceBean<FtfActSignListBean> querySignInfoList(String actId,String createdBy) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            FtfActSignListBean ftfActSignListBean = ftfActSignRepository.querySignInfoList(actId,createdBy);
            serviceBean.setResponse(ftfActSignListBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询FTF签到活动信息
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<FtfSignMiniAppResBean> querySignFtfActInfo(FtfSignMiniAppReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            if(StringUtil.isNullOrEmpty(reqBean.getActId())){
                serviceBean.setCode(1108);
                serviceBean.setDesc("活动ID为空");
            }else{
                FtfSignMiniAppResBean ftfSignMiniAppResBean = ftfActSignRepository.querySignFtfActInfo(reqBean);
                serviceBean.setResponse(ftfSignMiniAppResBean);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 小程序扫码签到
     *
     * @param ftfSignMiniAppReqBean
     * @return
     */
    @Override
    public ServiceBean saveSignMiniApp(FtfSignMiniAppReqBean ftfSignMiniAppReqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = ftfActSignRepository.saveSignMiniApp(ftfSignMiniAppReqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * feichat贸易销量
     *
     * @return
     */
    @Override
    public ServiceBean saveActFeiChatSale() {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = ftfActSignRepository.saveActFeiChatSale();
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询登陆人所属的SA门店
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<BaseKeyValueBean>> querySaTerminal(BaseKeyValueBean reqBean) {
        ServiceBean<List<BaseKeyValueBean>> serviceBean = new ServiceBean<>();
        try {
            List<BaseKeyValueBean> baseKeyValueBean = ftfActSignRepository.querySaTerminal(reqBean);
            serviceBean.setResponse(baseKeyValueBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }


    /**
     * FTF活动签到
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<String> saveMobSign(FtfActSignInfoReqBean reqBean) {

        ServiceBean serviceBean = new ServiceBean();
        try {
            boolean flag = true;
            if("HYT".equals(reqBean.getSourceSystem())){
                flag = ftfActSignRepository.checkIsSign(reqBean.getId());
            }
            if(flag) {
                String mobile = reqBean.getMobile();
                String userId = reqBean.getUserId();
                String customerId = "";
                PhoneService.ServiceIface phoneService = custRpcFactory.getPhoneService();
                //验证手机号码是否已经注册过会员
                BaseRequest baseRequest = new BaseRequest();
                baseRequest.setSeqNo(reqBean.getSeqNo());
                baseRequest.setFromSystem(reqBean.getSourceSystem());
                BoolResponse boolResponse = phoneService.isCustMobileExist(baseRequest, mobile).get();
                CustomerService.ServiceIface custSerivce = custRpcFactory.getCustomerService();
                if (boolResponse.isValue()) {  //如果手机号码存在

                /*1.手机 *2.固话*3.其他电话*4.qq_id*5.sina_id*6.weixin_open_id
                7.weixin_union_id *8.weixin_app_id *9.支付宝账号
                10.QQ号*11.微信号*12.天猫虚拟昵称（天猫ID） *13.天猫真实昵称
                14.bnc_weixin_open_id*15.swisse_weixin_open_id*16.ht_weixin_open_id*/

                    //根据手机号码 获取会员信息
                    Customer4AppResponse customer4AppResponse = custSerivce.getCustomerByContact(baseRequest, 1, mobile).get();
                    customerId = String.valueOf(customer4AppResponse.getValue().getId());

                } else { //如果手机号码不存在，创建会员
                    TCustCustomer customer = addCrmCustomer(mobile, userId, reqBean.getSourceSystem());
                    customerId = String.valueOf(customer.getId());
                    //绑定与门店的关系
                    AddCustTerminalRequest terminalRequest = new AddCustTerminalRequest();

                    terminalRequest.setBaseReq(baseRequest);

                    terminalRequest.setTerminalCode(reqBean.getSaCode());
                    terminalRequest.setTerminalType(3);
                    terminalRequest.setCustomerId(Long.valueOf(customerId));

                    custRpcFactory.getCustTerminalService().addCustTerminal(terminalRequest);
                }
                reqBean.setCustomerId(customerId);
                logger.info("saveMobSign-Req参数:" + reqBean);
                serviceBean = ftfActSignRepository.saveMobSign(reqBean);
            }else{
                serviceBean.setCode(1108);
                serviceBean.setDesc("活动已结束停止签到");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(1006);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 上传销量
     *
     * @param ftfActSignInfoReqBean
     * @return
     */
    @Override
    public ServiceBean<String> saveMobSignReportSales(FtfActSignInfoReqBean ftfActSignInfoReqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {

            ftfActSignRepository.saveMobSignReportSales(ftfActSignInfoReqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    public TCustCustomer addCrmCustomer(String mobile,String userId,String sourceSystem) {
        String logTitle = "营销通app-创建会员rpc服务";
        TCustCustomer custCustomer = new TCustCustomer();
        CustomerService.ServiceIface custSerivce = custRpcFactory.getCustomerService();

        AddCustomer4CrmRequest req = new AddCustomer4CrmRequest();
        BaseRequest baseReq = new BaseRequest();
        baseReq.setSeqNo((new Date()).getTime() + "");
        req.setBaseReq(baseReq);

        custCustomer.setCreatedBy(userId);
        custCustomer.setCreatedTime(System.currentTimeMillis());
        custCustomer.setFromSystem(sourceSystem);
        custCustomer.setUpdatedBy(userId);
        custCustomer.setUpdatedTime(System.currentTimeMillis());
        custCustomer.setStatusCode(2);   //2.激活
        req.setCust(custCustomer);

        List<TCustPhone> custPhoneList=new ArrayList<TCustPhone>();
        TCustPhone custPhone=new TCustPhone();
        custPhone.setContactNumber(mobile);
        custPhone.setContactType(1); //1.代表手机
        custPhone.setIsDefault(1); //0.否    1.是
        custPhoneList.add(custPhone);
        req.setCustPhones(custPhoneList);

        String json = JSON.toJSONString(req);
        logger.info(logTitle+"-Req参数:" + json);
        long start = System.currentTimeMillis();

        CustCustomerResponse res = custSerivce.addCustomer4Crm(req).get();
        custCustomer = res.getValue();
        long end = System.currentTimeMillis();
        logger.info(logTitle+"-addCustomer4Crm()耗时:"+(end - start) / 1000.00 + "秒");

        logger.info(logTitle+"Res参数 "+ JSON.toJSONString(res.getValue()));
        logger.info(logTitle+"custCustomer "+ JSON.toJSONString(custCustomer));

        return custCustomer;
    }
}
