package com.biostime.samanage.service.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportReqBean;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportResBean;
import com.biostime.samanage.repository.single.SaWxCustomerReportRepository;
import com.biostime.samanage.service.single.SaWxCustomerReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA微信会员报表ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("saWxCustomerReportService")
@Slf4j
public class SaWxCustomerReportServiceImpl implements SaWxCustomerReportService {
    @Autowired
    private SaWxCustomerReportRepository saWxCustomerReportRepository;
    /**
     * SA微信会员报表
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaWxCustomerReportResBean>> querySaWxCustomerReportList(Pager<SaWxCustomerReportResBean> pager, SaWxCustomerReportReqBean reqBean) {
        ServiceBean<Pager<SaWxCustomerReportResBean>> serviceBean = new ServiceBean<Pager<SaWxCustomerReportResBean>>();
        try {
            serviceBean.setResponse(saWxCustomerReportRepository.querySaWxCustomerReportList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA微信会员报表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaWxCustomerReportList(HttpServletResponse response, SaWxCustomerReportReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saWxCustomerReportRepository.exportSaWxCustomerReportList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
