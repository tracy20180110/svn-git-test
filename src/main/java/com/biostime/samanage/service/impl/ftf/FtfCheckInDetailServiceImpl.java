package com.biostime.samanage.service.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.ftf.checkIn.*;
import com.biostime.samanage.repository.ftf.FtfCheckInDetailRepository;
import com.biostime.samanage.service.ftf.FtfCheckInDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA. Describe:FTF活动签到信息 Date: 2016-11-23 Time: 11:37
 * User: 12804 Version:1.0
 */
@Service("ftfCheckInDetailService")
@Slf4j
public class FtfCheckInDetailServiceImpl implements FtfCheckInDetailService {
	@Autowired
	private FtfCheckInDetailRepository ftfCheckInDetailRepository;

	/**
	 * FTF活动签到信息
	 *
	 * @param pager
	 * @param reqBean
	 * @return
	 */
	@Override
	public ServiceBean<Pager<FtfCheckInDetailResBean>> queryFtfCheckInDetailList(Pager<FtfCheckInDetailResBean> pager,
			FtfCheckInDetailReqBean reqBean) {
		ServiceBean<Pager<FtfCheckInDetailResBean>> serviceBean = new ServiceBean<Pager<FtfCheckInDetailResBean>>();
		try {
			serviceBean.setResponse(ftfCheckInDetailRepository.queryFtfCheckInDetailList(pager, reqBean));
		} catch (Exception ex) {
			log.info("msg{}", ex);
			ex.printStackTrace();
			serviceBean.setCode(500);
			serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
		}
		return serviceBean;
	}

	/**
	 * FTF活动签到信息-导出
	 *
	 * @param response
	 * @param reqBean
	 * @return
	 */
	@Override
	public ServiceBean exportFtfCheckInDetailList(HttpServletResponse response, FtfCheckInDetailReqBean reqBean) {
		ServiceBean serviceBean = new ServiceBean<String>();
		try {
			ftfCheckInDetailRepository.exportFtfCheckInDetailList(response, reqBean);
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("msg{}", ex);
			serviceBean.setCode(500);
			serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
		}
		return serviceBean;
	}

	@Override
	public ServiceBean<FtfCheckInAppDetailResBean> queryFtfCheckInAppDetailList(FtfCheckInDetailReqBean reqBean) {
		ServiceBean<FtfCheckInAppDetailResBean> serviceBean = new ServiceBean<FtfCheckInAppDetailResBean>();
		try {
			// 获取组团、个人签到信息
			List<FtfCheckInAppIndividualDetailResBean> individualDetailResBeans = ftfCheckInDetailRepository
					.queryFtfCheckInAppIndividualDetail(reqBean);
			List<FtfCheckInAppGroupDetailResBean> groupDetailResBeans = ftfCheckInDetailRepository
					.queryFtfCheckInAppGroupDetail(reqBean);
			
			// 对组团签到人数进行分组
			List<List<FtfCheckInAppGroupDetailResBean>> groupList = getListByGroup(groupDetailResBeans);
			// 组团有人数限制，必须多少人才能成团，SQL不好处理，故在Java层处理
			int groupNums = 0;
			for(int i = 0; i < groupList.size(); i++){
				if(groupNums == 0){
					groupNums = groupList.get(i).get(i).getGroupNums();
				}
				if(groupList.get(i).size() < groupNums){
					groupList.remove(i);
				}
			}
			
			FtfCheckInAppDetailResBean ftfCheckInAppDetailResBean = new FtfCheckInAppDetailResBean();
			// 个人签到信息
			ftfCheckInAppDetailResBean.setIndividualDetailResBeans(individualDetailResBeans);
			// 组团签到信息
			List<FtfCheckInAppGroupResBean> groupResBean = new ArrayList<FtfCheckInAppGroupResBean>();
			
			for (List<FtfCheckInAppGroupDetailResBean> group : groupList) {
				FtfCheckInAppGroupResBean ftfCheckInAppGroupResBean = new FtfCheckInAppGroupResBean();
				ftfCheckInAppGroupResBean.setGroupDetailResBeans(group);
				// 获取领取状态
				ftfCheckInAppGroupResBean.setReceiveStatus(
						ftfCheckInDetailRepository.getReceiveStatus(reqBean.getFtfActId(), group.get(0).getGroupId()));
				groupResBean.add(ftfCheckInAppGroupResBean);
			}
			
			ftfCheckInAppDetailResBean.setGroupResBean(groupResBean);
			serviceBean.setResponse(ftfCheckInAppDetailResBean);
		} catch (Exception ex) {
			log.info("msg{}", ex);
			ex.printStackTrace();
			serviceBean.setCode(500);
			serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
		}
		return serviceBean;
	}

	/**
	 * 对组团签到信息按组团Id分割
	 * 
	 * @param groupDetailResBeans
	 * @return
	 */
	private List<List<FtfCheckInAppGroupDetailResBean>> getListByGroup(
			List<FtfCheckInAppGroupDetailResBean> groupDetailResBeans) {

		List<List<FtfCheckInAppGroupDetailResBean>> result = new ArrayList<List<FtfCheckInAppGroupDetailResBean>>();
		Map<String, List<FtfCheckInAppGroupDetailResBean>> map = new TreeMap<String, List<FtfCheckInAppGroupDetailResBean>>();

		for (FtfCheckInAppGroupDetailResBean bean : groupDetailResBeans) {
			if (map.containsKey(bean.getGroupId())) {
				List<FtfCheckInAppGroupDetailResBean> t = map.get(bean.getGroupId());
				t.add(new FtfCheckInAppGroupDetailResBean(bean.getMobile(), bean.getSignInCode(), bean.getGroupId(),
						bean.getSignInName(), bean.getGroupNums()));
				map.put(bean.getGroupId(), t);
			} else {
				List<FtfCheckInAppGroupDetailResBean> t = new ArrayList<FtfCheckInAppGroupDetailResBean>();
				t.add(new FtfCheckInAppGroupDetailResBean(bean.getMobile(), bean.getSignInCode(), bean.getGroupId(),
						bean.getSignInName(), bean.getGroupNums()));
				map.put(bean.getGroupId(), t);
			}
		}
		for (Entry<String, List<FtfCheckInAppGroupDetailResBean>> entry : map.entrySet()) {
			result.add(entry.getValue());
		}
		return result;
	}

	/**
	 * FTF活动签到领取奖品
	 */
	@Override
	public ServiceBean<String> receiveFtfCheckInAppPrizes(FtfCheckInDetailReqBean reqBean) {
		ServiceBean<String> serviceBean = new ServiceBean<String>();
		try {
			ftfCheckInDetailRepository.receiveFtfCheckInAppPrizes(reqBean);
		} catch (Exception ex) {
			log.info("msg{}", ex);
			ex.printStackTrace();
			serviceBean.setCode(500);
			serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
		}
		return serviceBean;
	}
}
