package com.biostime.samanage.service.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActResBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerResBean;
import com.biostime.samanage.repository.single.SaLecturerRepository;
import com.biostime.samanage.service.single.SaLecturerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 12804
 */
@Service("saLecturerService")
@Slf4j
public class SaLecturerServiceImpl implements SaLecturerService {


    @Autowired
    private SaLecturerRepository saLecturerRepository;

    /**
     * SA讲师看板
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<SaLecturerResBean>> querySaLecturerInfo(SaLecturerReqBean reqBean) {
        ServiceBean<List<SaLecturerResBean>> serviceBean = new ServiceBean<List<SaLecturerResBean>>();
        try {
            List<SaLecturerResBean> actRePortImgTypeResBean = saLecturerRepository.querySaLecturerInfo(reqBean);
            serviceBean.setResponse(actRePortImgTypeResBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA指标
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<BaseKeyValueBean>> querySaIndex(SaLecturerReqBean reqBean) {
        ServiceBean<List<BaseKeyValueBean>> serviceBean = new ServiceBean<List<BaseKeyValueBean>>();
        try {
            List<BaseKeyValueBean> resBeanList = saLecturerRepository.querySaIndex(reqBean);
            serviceBean.setResponse(resBeanList);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA讲师活动报表查詢
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaLecturerActResBean>> querySaLecturerActList(Pager<SaLecturerActResBean> pager, SaLecturerActReqBean reqBean) {
        ServiceBean<Pager<SaLecturerActResBean>> serviceBean = new ServiceBean<Pager<SaLecturerActResBean>>();
        try {
            serviceBean.setResponse(saLecturerRepository.querySaLecturerActList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA讲师活动报表查詢-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaLecturerActList(HttpServletResponse response, SaLecturerActReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saLecturerRepository.exportSaLecturerActList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA讲师活动报表查詢-线下导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaLecturerActDownList(HttpServletResponse response, SaLecturerActReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saLecturerRepository.exportSaLecturerActDownList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
