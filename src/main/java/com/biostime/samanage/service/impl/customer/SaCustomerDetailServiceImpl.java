package com.biostime.samanage.service.impl.customer;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.customer.CustomerDetailBean;
import com.biostime.samanage.repository.customer.SaCustomerDetailRepository;
import com.biostime.samanage.service.customer.SaCustomerDetailService;
import com.biostime.samanage.util.ExportUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * SA客户明细
 */
@Service("saCustomerDetailService")
@Slf4j
public class SaCustomerDetailServiceImpl implements SaCustomerDetailService {
    @Autowired
    private SaCustomerDetailRepository saCustomerDetailRepository;

    /**
     * 方法描述: 根据传进去的参数导出.xlsx文档
     *
     * @param response
     * @param fileName
     * @param mlist
     * @author zhuhaitao
     * @createDate 2016年1月12日 下午6:54:34
     */
    private String writeReportExcel(HttpServletResponse response, String fileName, List<CustomerDetailBean> mlist) {
        String url = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HHmmss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            fileName = fileName + sdf.format(new Date());
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(new String(fileName + ".xlsx"), "UTF-8"));

            url = this.writeAllBrandsExcel(fileName, mlist, response);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            try {
                response.reset();
                response.setContentType("text/html;charset=utf-8");
                response.getWriter().print("导出失败！");
            } catch (Exception e1) {
                log.error(e1.getMessage(), e1);
            }
        }
        return url;
    }

    /**
     * 获取来源
     *
     * @param recSrc
     * @return
     */
    public static String getRecSrc(String recSrc) {
        String rec = "";
        switch (recSrc) {
            case "1":
                rec = "POS机";
                break;
            case "2":
                rec = "营销通";
                break;
            case "3":
                rec = "CRM";
                break;
            case "4":
                rec = "商家中心";
                break;
            case "5":
                rec = "微信";
                break;
            case "6":
                rec = "妈妈100APP";
                break;
            case "7":
                rec = "母婴之家";
                break;
            default:
                rec = "无";
                break;
        }
        return rec;

    }

    /**
     * 方法描述: 导出数据所需要的参数
     *
     * @param fileName
     * @param dataList
     * @param response
     * @return
     * @throws FileNotFoundException
     */
    private String writeAllBrandsExcel(String fileName, List<CustomerDetailBean> dataList, HttpServletResponse response) throws FileNotFoundException {

        String path = "";
        // 复合表头
        String titleName[] = new String[]{"基本资料"};
        // 复合表头起止列
        Integer titleRange[][] = new Integer[][]{{0, 2}};
        // 数据列表头
        String ths[] = new String[]{
                // 基本资料
                "大区"
                , "办事处"
                , "SA编码"
                , "会员ID"
                , "会员姓名"
                , "用户角色"
                , "宝宝生日/预产期"
                , "手机号码"
                , "积分来源"
                , "BCC工号"
                , "实操BCC工号"
                , "育婴顾问工号"
                , "关联育儿指导师"
                , "产品名称"
                , "积分时间"
                , "购买门店"
                , "品牌"
                , "品项"
                , "奶粉阶段"
                , "是否订单积分"
                , "是否绑定微信"
                , "是否妈妈100APP用户"
                , "是否SAYN新客"
                , "是否来自新客礼包"};
        // 数据列
        String colNames[] = new String[]{
                // 基本资料
                "areaName", "officeName", "saCode", "customerId", "name", "customerRole", "birthdate", "mobile", "recSrc", "proBcc", "operateBcc",
                "nursingConsultant", "parentingInstructor", "productName", "registerDate", "buyTerminal", "brand", "typeName", "stage", "isO2OOrder", "isWeixinBind", "isAppUser", "isCoupon", "isNewCustGiftPkg"};

        ExportUtil.wirteDefaultExcel(null, titleRange, dataList, ths, colNames, response);

        return path;
    }

    /**
     * 分页查询SA客户明细
     *
     * @param pager
     * @param customerDetailBean
     * @return
     */
    @Override
    public ServiceBean<Pager<CustomerDetailBean>> searchSaCustomerDetail(Pager<CustomerDetailBean> pager, CustomerDetailBean customerDetailBean) {

        ServiceBean<Pager<CustomerDetailBean>> serviceBean = new ServiceBean<Pager<CustomerDetailBean>>();
        try {
            pager = saCustomerDetailRepository.searchSaCustomerDetail(pager, customerDetailBean);
           /* if (CollectionUtils.isNotEmpty(pager.getDatas())) {
                for (CustomerDetailBean customerDetail : pager.getDatas()) {
                    if (StringUtil.isNotNullNorEmpty(customerDetail.getMobile()) && customerDetail.getMobile().length() == 11) {
                        customerDetail.setMobile(customerDetail.getMobile().substring(0, 3) + "****" + customerDetail.getMobile().substring(7, customerDetail.getMobile().length()));
                    }
                }
            }*/
            serviceBean.setResponse(pager);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;

    }

    /**
     * 导出SA客户明细
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param proBcc
     * @param srcLocName
     * @param recSrc
     * @param startTime
     * @param endTime
     * @param response
     * @return
     */
    @Override
    public ServiceBean exportSaCustomerDetail(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobile, String proBcc, String srcLocName, String recSrc, String startTime, String endTime, String stage,String buCode, String parentingInstructor, HttpServletResponse response) {
        ServiceBean<List<CustomerDetailBean>> serviceBean = new ServiceBean<List<CustomerDetailBean>>();
        try {
            List<CustomerDetailBean> lists = new ArrayList<CustomerDetailBean>();

            lists = saCustomerDetailRepository.getSaCustomerDetail(nursingConsultant, areaCode, officeCode, saCode, customerId, mobile, proBcc, srcLocName, recSrc, startTime, endTime, stage,buCode, parentingInstructor);

            writeReportExcel(response, "SA客户明细", lists);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<List<CustomerDetailBean>>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }
}
