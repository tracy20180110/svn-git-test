package com.biostime.samanage.service.impl.terminal;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.ftf.sign.FtfActSignResBean;
import com.biostime.samanage.bean.terminal.*;
import com.biostime.samanage.repository.terminal.TerminalVisitRepository;
import com.biostime.samanage.service.terminal.TerminalVisitService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description:门店拜访考核报表
 * @Auther: 12804
 * @Date: 2019/6/21 10:30
 * @Version:1.0.0
 */
@Service("terminalVisitService")
@Slf4j
public class TerminalVisitServiceImpl  implements TerminalVisitService {
    @Autowired
    private TerminalVisitRepository terminalVisitRepository;

    /**
     * 门店拜访考核查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<TerminalVisitResBean>> queryTerminalVisitList(Pager<TerminalVisitResBean> pager, TerminalVisitReqBean reqBean) {
        ServiceBean<Pager<TerminalVisitResBean>> serviceBean = new ServiceBean<Pager<TerminalVisitResBean>>();
        try {

            if(StringUtil.isNotNullNorEmpty(reqBean.getStartMonth()) && StringUtil.isNotNullNorEmpty(reqBean.getEndMonth())){
                serviceBean.setResponse(terminalVisitRepository.queryTerminalVisitList(pager, reqBean));
            }else{
                serviceBean.setCode(502);
                serviceBean.setDesc("开始月份和结束月份不能为空");
            }
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 门店拜访考核-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportTerminalVisitList(HttpServletResponse response, TerminalVisitReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            terminalVisitRepository.exportTerminalVisitList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询门店拜访岗位列表
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<BaseKeyValueBean>> queryTerminalVisitPostList(TerminalVisitReqBean reqBean) {
        ServiceBean<List<BaseKeyValueBean>> serviceBean = new ServiceBean<>();
        try {
            List<BaseKeyValueBean> baseKeyValueBean = terminalVisitRepository.queryTerminalVisitPostList(reqBean);
            serviceBean.setResponse(baseKeyValueBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 门店拜访考核季度查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<TerminalVisitQuarterResBean>> queryTerminalVisitQuarterList(Pager<TerminalVisitQuarterResBean> pager, TerminalVisitReqBean reqBean) {
        ServiceBean<Pager<TerminalVisitQuarterResBean>> serviceBean = new ServiceBean<Pager<TerminalVisitQuarterResBean>>();
        try {
            if(StringUtil.isNotNullNorEmpty(reqBean.getQuarter())){
                serviceBean.setResponse(terminalVisitRepository.queryTerminalVisitQuarterList(pager, reqBean));
            }else{
                serviceBean.setCode(502);
                serviceBean.setDesc("季度不能为空");
            }
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 门店拜访考核季度查询-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportTerminalVisitQuarterList(HttpServletResponse response, TerminalVisitReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            terminalVisitRepository.exportTerminalVisitQuarterList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 门店拜访考核查询
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<TerminalVisitMobResBean>> queryTerminalVisitMobList(TerminalVisitMobReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            List<TerminalVisitMobResBean> terminalVisitMobList = terminalVisitRepository.queryTerminalVisitMobList(reqBean);
            serviceBean.setResponse(terminalVisitMobList);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(1001);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
