package com.biostime.samanage.service.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.single.event.SaEventDetailReqBean;
import com.biostime.samanage.bean.single.event.SaEventDetailResBean;
import com.biostime.samanage.bean.single.event.SaEventReqBean;
import com.biostime.samanage.repository.single.SaEventDetailRepository;
import com.biostime.samanage.service.single.SaEventDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Describe:SA与门店管理ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("saEventDetailService")
@Slf4j
public class SaEventDetailServiceImpl implements SaEventDetailService {
    
    @Autowired
    private SaEventDetailRepository saEventDetailRepository;


    /**
     * SA接触列表-查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaEventDetailResBean>> querySaEventDetailList(Pager<SaEventDetailResBean > pager, SaEventDetailReqBean reqBean) {
        ServiceBean<Pager<SaEventDetailResBean>> serviceBean = new ServiceBean<Pager<SaEventDetailResBean>>();
        try {
            serviceBean.setResponse(saEventDetailRepository.querySaEventDetailList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA接触列表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaEventDetailList(HttpServletResponse response, SaEventDetailReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saEventDetailRepository.exportSaEventDetailList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 扫码注册绑定会员
     *
     * @param map
     * @return
     */
    @Override
    public void saveSaWxBindNewCustomer(Map map) {
        try {
            saEventDetailRepository.saveSaWxBindNewCustomer(map);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
        }
    }

    /**
     * 妈妈100微信、营销通app接触签到
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveSaEvent(SaEventReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            if(StringUtil.isNullOrEmpty(String.valueOf(reqBean.getCustomerId()))){
                serviceBean.setCode(506);
                serviceBean.setDesc("会员ID不能为空");
                return serviceBean;
            }
            if(StringUtil.isNullOrEmpty(reqBean.getSaCode())){
                serviceBean.setCode(507);
                serviceBean.setDesc("SA编码不能为空");
                return serviceBean;
            }
            serviceBean = saEventDetailRepository.saveSaEvent(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
