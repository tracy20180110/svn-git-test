package com.biostime.samanage.service.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.caravan.*;
import com.biostime.samanage.repository.single.CaravanRepository;
import com.biostime.samanage.service.single.CaravanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:大小篷车申请
 * Date: 2017-12-15
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("caravanService")
@Slf4j
public class CaravanServiceImpl implements CaravanService {

    @Autowired
    private CaravanRepository caravanRepository;
    
    /**
     * 查询分销物料信息
     * @return
     */
    @Override
    public ServiceBean<List<CaravanCmaterialInfo>> queryCaravanMaterielList(CaravanReqBean reqBean) {
        ServiceBean<List<CaravanCmaterialInfo>> serviceBean = new ServiceBean<List<CaravanCmaterialInfo>>();
        try {
            serviceBean.setResponse(caravanRepository.queryCaravanMaterielList(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 大小篷车申请
     *
     * @param request
     * @return
     */
    @Override
    public ServiceBean saveCaravan(CaravanReqBean reqBean,HttpServletRequest request) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = caravanRepository.saveCaravan(reqBean,request);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 大小篷车申请OA审批
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean caravanOaAudit(CaravanReqBean reqBean,HttpServletRequest request) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = caravanRepository.caravanOaAudit(reqBean, request);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 重新推送获取订单
     *
     * @param reqBean
     * @param request
     * @return
     */
    @Override
    public ServiceBean caravanOrder(CaravanReqBean reqBean, HttpServletRequest request) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            String msg = caravanRepository.caravanOrder(reqBean, request);
            if(StringUtil.isNotNullNorEmpty(msg)){
                serviceBean.setCode(202);
                serviceBean.setDesc(msg);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 大小篷车状态更新
     *
     * @param reqBean
     * @param request
     * @return
     */
    @Override
    public ServiceBean editCaravanStatus(CaravanReqBean reqBean, HttpServletRequest request) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            caravanRepository.editCaravanStatus(reqBean, request);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 大小篷车上传竣工图片及审核
     *
     * @param reqBean
     * @param request
     * @return
     */
    @Override
    public ServiceBean saveCaravanImgAudit(CaravanImgAuditReqBean reqBean, HttpServletRequest request) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            caravanRepository.saveCaravanImgAudit(reqBean,request);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 更新大小篷车OA流程审批状态
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean editCaravanOaAuditStatus(CaravanReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean.setResponse(caravanRepository.editCaravanOaAuditStatus(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 大小篷车列表查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<CaravanListResBean>> querySaCaravanList(Pager<CaravanListResBean> pager, CaravanListReqBean reqBean) {
        ServiceBean<Pager<CaravanListResBean>> serviceBean = new ServiceBean<Pager<CaravanListResBean>>();
        try {
            serviceBean.setResponse(caravanRepository.querySaCaravanList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 大小篷车列表--导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaCaravanList(HttpServletResponse response, CaravanListReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            caravanRepository.exportSaCaravanList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询SA门店
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean querySaTerminalInfo(CaravanReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            serviceBean = caravanRepository.querySaTerminalInfo(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询大小篷车信息
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<CaravanReqBean> queryCaravanInfo(CaravanReqBean reqBean) {
        ServiceBean<CaravanReqBean> serviceBean = new ServiceBean<CaravanReqBean>();
        try {
            serviceBean.setResponse(caravanRepository.queryCaravanInfo(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询可更新的人员列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<String>> queryUpdateByList(CaravanListReqBean reqBean) {
        ServiceBean<List<String>> serviceBean = new ServiceBean<List<String>>();
        try {
            serviceBean.setResponse(caravanRepository.queryUpdateByList(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 更新申请人
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean editUpdateBy(CaravanReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            serviceBean = caravanRepository.editUpdateBy(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询大小篷车操作日志
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<CaravanLogInfo>> queryCaravanLogList(CaravanReqBean reqBean) {
        ServiceBean<List<CaravanLogInfo>> serviceBean = new ServiceBean<List<CaravanLogInfo>>();
        try {
            serviceBean.setResponse(caravanRepository.queryCaravanLogList(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询TPM费用列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<CaravanTpmCostInfo>> queryTpmCostList(CaravanTpmCostInfo reqBean) {
        ServiceBean<List<CaravanTpmCostInfo>> serviceBean = new ServiceBean<List<CaravanTpmCostInfo>>();
        try {
            serviceBean.setResponse(caravanRepository.queryTpmCostList(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}", ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
