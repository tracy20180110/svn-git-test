package com.biostime.samanage.service.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.ftf.report.FtfNewCustomerDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfNewCustomerDetailResBean;
import com.biostime.samanage.repository.ftf.FtfNewCustomerDetailRepository;
import com.biostime.samanage.service.ftf.FtfNewCustomerDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动新客信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("ftfNewCustomerDetailService")
@Slf4j
public class FtfNewCustomerDetailServiceImpl implements FtfNewCustomerDetailService {
    @Autowired
    private FtfNewCustomerDetailRepository ftfNewCustomerDetailRepository;

    /**
     * FTF活动新客信息
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<FtfNewCustomerDetailResBean>> queryFtfNewCustomerDetailList(Pager<FtfNewCustomerDetailResBean> pager, FtfNewCustomerDetailReqBean reqBean) {
        ServiceBean<Pager<FtfNewCustomerDetailResBean>> serviceBean = new ServiceBean<Pager<FtfNewCustomerDetailResBean>>();
        try {
            serviceBean.setResponse(ftfNewCustomerDetailRepository.queryFtfNewCustomerDetailList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF活动新客信息-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportFtfNewCustomerDetailList(HttpServletResponse response, FtfNewCustomerDetailReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            ftfNewCustomerDetailRepository.exportFtfNewCustomerDetailList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
