package com.biostime.samanage.service.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.ftf.activity.FtfActivityReqBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityTemplateResBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoReqBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import com.biostime.samanage.domain.ftf.FtfActivity;
import com.biostime.samanage.repository.ftf.FtfActivityRepository;
import com.biostime.samanage.service.ftf.FtfActivityService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF讲师ServiceImpl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("ftfActivityService")
@Slf4j
public class FtfActivityServiceImpl implements FtfActivityService {

    @Autowired
    private FtfActivityRepository ftfActivityRepository;

    /**
     * 获取FTF活动列表
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<FtfActivityResBean>> queryFtfActivityList(Pager<FtfActivityResBean> pager, FtfActivityReqBean reqBean) {
        ServiceBean<Pager<FtfActivityResBean>> serviceBean = new ServiceBean<Pager<FtfActivityResBean>>();
        try {
            serviceBean.setResponse(ftfActivityRepository.queryFtfActivityList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(1001);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF活动列表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportFtfActivityList(HttpServletResponse response, FtfActivityReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            ftfActivityRepository.exportFtfActivityList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF活动状态修改
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean changeStatusFtfActivity(FtfActivityReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            ftfActivityRepository.changeStatusFtfActivity(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF活动保存
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<FtfActivity> saveFtfActivity(FtfActivityReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            if(StringUtil.isNullOrEmpty(reqBean.getCurriculumId())){
                serviceBean.setCode(102);
                serviceBean.setDesc("请选择课程");
                return serviceBean;
            }
            if(CollectionUtils.isEmpty(reqBean.getFtfActLecturer())){
                serviceBean.setCode(103);
                serviceBean.setDesc("请选择讲师");
                return serviceBean;
            }
            if(StringUtil.isNullOrEmpty(reqBean.getActDate())){
                serviceBean.setCode(104);
                serviceBean.setDesc("请选择活动时间");
                return serviceBean;
            }
            if(StringUtil.isNullOrEmpty(String.valueOf(reqBean.getNewCustomerCalculation()))){
                serviceBean.setCode(105);
                serviceBean.setDesc("请选择新客计算方式");
                return serviceBean;
            }

            /*String actId = ftfActivityRepository.saveFtfActivity(reqBean);
            System.out.println("-----actId-------"+actId);
            if(StringUtil.isNotNullNorEmpty(actId)){  //新增返回活动ID 更新不返回
                System.out.println("-----新增返回活动actId-------"+actId);
                serviceBean.setResponse(actId);
            }*/
            serviceBean.setResponse(ftfActivityRepository.saveFtfActivity(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 获取SA对应的门店
     * @param saCode
     * @return
     */
    @Override
    public ServiceBean querySaTerminal(String saCode) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean.setResponse(ftfActivityRepository.querySaTerminal(saCode));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }


    /**
     * 查询门店是否存在
     * @param terminalCode
     * @return
     */
    @Override
    public ServiceBean queryTerminal(String terminalCode) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean.setResponse(ftfActivityRepository.queryTerminal(terminalCode));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询导购ID是否存在
     *
     * @param shoppingGuideId
     * @return
     */
    @Override
    public ServiceBean queryShoppingGuideId(String shoppingGuideId) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = ftfActivityRepository.queryShoppingGuideId(shoppingGuideId);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF海报分享链接更改
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean changeImageFtfActivity(FtfActivityReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            ftfActivityRepository.changeImageFtfActivity(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF活动模板列表
     * @return
     */

    @Override
    public ServiceBean<List<FtfActivityTemplateResBean>> queryFtfActivityTemplateList() {
        ServiceBean<List<FtfActivityTemplateResBean>> serviceBean = new ServiceBean<List<FtfActivityTemplateResBean>>();
        try {
            serviceBean.setResponse(ftfActivityRepository.queryFtfActivityTemplateList());
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询SA线下课堂模板详情
     *
     * @param templateId
     * @param actId
     * @return
     */
    @Override
    public ServiceBean<Map> querySaFtfActTemplateInfo(String templateId, String actId) {
        ServiceBean<Map> serviceBean = new ServiceBean<Map>();
        try {
            serviceBean.setResponse(ftfActivityRepository.querySaFtfActTemplateInfo(templateId,actId));
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF活动模板列表
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<FtfLecturerResBean>> queryLecturerList(FtfActivityReqBean reqBean) {
        ServiceBean<List<FtfLecturerResBean>> serviceBean = new ServiceBean<List<FtfLecturerResBean>>();
        try {
            serviceBean.setResponse(ftfActivityRepository.queryLecturerList(reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 更新小程序签到码
     *
     * @param actId
     * @return
     */
    @Override
    public String changeActMama100QrCode(String actId) {
        return ftfActivityRepository.changeActMama100QrCode(actId);
    }

    /**
     * 查询讲师列表app
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<FtfLecturerResBean>> queryAppLecturerList(FtfActivityReqBean reqBean) {
        ServiceBean<List<FtfLecturerResBean>> serviceBean = new ServiceBean<List<FtfLecturerResBean>>();
        try {
            serviceBean.setResponse(ftfActivityRepository.queryAppLecturerList(reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 上报中的人员列表app
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<AccountInfoResBean>> queryAppReportAccountInfoList(AccountInfoReqBean reqBean) {
        ServiceBean<List<AccountInfoResBean>> serviceBean = new ServiceBean<List<AccountInfoResBean>>();
        try {
            serviceBean.setResponse(ftfActivityRepository.queryAppReportAccountInfoList(reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
