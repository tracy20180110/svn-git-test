package com.biostime.samanage.service.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.ftf.sign.FtfSignUpDetailReqBean;
import com.biostime.samanage.bean.ftf.sign.FtfSignUpDetailResBean;
import com.biostime.samanage.repository.ftf.FtfSignUpDetailRepository;
import com.biostime.samanage.service.ftf.FtfSignUpDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动报名信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("ftfSignUpDetailService")
@Slf4j
public class FtfSignUpDetailServiceImpl implements FtfSignUpDetailService {
    @Autowired
    private FtfSignUpDetailRepository ftfSignUpDetailRepository;

    /**
     * FTF活动报名信息
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<FtfSignUpDetailResBean>> queryFtfSignUpDetailList(Pager<FtfSignUpDetailResBean> pager, FtfSignUpDetailReqBean reqBean) {
        ServiceBean<Pager<FtfSignUpDetailResBean>> serviceBean = new ServiceBean<Pager<FtfSignUpDetailResBean>>();
        try {
            serviceBean.setResponse(ftfSignUpDetailRepository.queryFtfSignUpDetailList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF活动报名信息-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportFtfSignUpDetailList(HttpServletResponse response, FtfSignUpDetailReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            ftfSignUpDetailRepository.exportFtfSignUpDetailList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
