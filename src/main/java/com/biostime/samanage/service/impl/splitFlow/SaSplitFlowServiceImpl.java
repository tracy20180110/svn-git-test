package com.biostime.samanage.service.impl.splitFlow;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.customer.CustomerDetailBean;
import com.biostime.samanage.bean.splitFlow.SaSplitFlowBean;
import com.biostime.samanage.domain.splitflow.BstOtoCustSa;
import com.biostime.samanage.repository.splitFlow.SaSplitFlowRepository;
import com.biostime.samanage.service.splitFlow.SaSplitFlowService;
import com.biostime.samanage.util.ExportUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * SA分流报表
 */
@Service("saSplitFlowService")
@Slf4j
public class SaSplitFlowServiceImpl implements SaSplitFlowService {
    @Autowired
    private SaSplitFlowRepository saSplitFlowRepository;

    /**
     * 方法描述: 根据传进去的参数导出.xlsx文档
     *
     * @param response
     * @param fileName
     * @param mlist
     * @author zhuhaitao
     * @createDate 2016年1月12日 下午6:54:34
     */
    private String writeReportExcel(HttpServletResponse response, String fileName, List<SaSplitFlowBean> mlist) {
        String url = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HHmmss");
            fileName = fileName + sdf.format(new Date());
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(new String(fileName + ".xlsx"), "UTF-8"));

            if (CollectionUtils.isNotEmpty(mlist)) {
                for (SaSplitFlowBean splitFlowBean : mlist) {
                    if (StringUtil.isNotNullNorEmpty(splitFlowBean.getMobilePhone()) && splitFlowBean.getMobilePhone().length() == 11) {
                        splitFlowBean.setMobilePhone(splitFlowBean.getMobilePhone().substring(0, 3) + "****" + splitFlowBean.getMobilePhone().substring(7, splitFlowBean.getMobilePhone().length()));
                    }

                }
            }
            url = this.writeAllBrandsExcel(fileName, mlist, response);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            try {
                response.reset();
                response.setContentType("text/html;charset=utf-8");
                response.getWriter().print("导出失败！");
            } catch (Exception e1) {
                log.error(e1.getMessage(), e1);
            }
        }
        return url;
    }


    /**
     * 方法描述: 导出数据所需要的参数
     *
     * @param fileName
     * @param dataList
     * @param response
     * @return
     * @throws FileNotFoundException
     */
    private String writeAllBrandsExcel(String fileName, List<SaSplitFlowBean> dataList, HttpServletResponse response) throws FileNotFoundException {

        String path = "";
        // 复合表头
        String titleName[] = new String[]{"基本资料"};
        // 复合表头起止列
        Integer titleRange[][] = new Integer[][]{{0, 2}};
        // 数据列表头
        String ths[] = new String[]{
/*      <th>大区</th>
        <th>办事处</th>
        <th>发券时间</th>
        <th>SA编号</th>
        <th>门店编号</th>
        <th>会员ID</th>
        <th>手机号码</th>
        <th>育婴顾问工号</th>
        <th>促销员工号</th>
        <th>是否到店</th>
        <th>到店时间</th>
        <th>妈妈奶粉新客</th>
        <th>婴儿奶粉新客（含HT）</th>
        <th>HT奶粉新客</th>
        <th>益生菌新客</th>
        <th>总积分(妈妈奶粉,婴儿奶粉,HT奶粉,益生菌)</th>*/
                // 基本资料
                "大区", "办事处", "发券日期", "SA编码", "1店", "1店所属体系", "1店所属渠道", "关联连锁门店", "连锁店所属体系", "连锁店所属渠道",
                "会员ID", "手机号码", "育婴顾问工号", "是否到店", "是否匹配", "实际到店", "到店时间", "领取礼品门店", "会员进店", 
                "1店-妈妈奶粉新客", "1店-婴儿奶粉新客", "1店-Dodie新客", "1店-益生菌新客", "连锁-妈妈奶粉新客", "连锁-婴儿奶粉新客", "连锁-Dodie新客", "连锁-益生菌新客"};
        // 数据列
        String colNames[] = new String[]{
                // 基本资料
                "areaName", "officeName", "createTime", "saCode", "terminalCode", "terminalTopChainName", "terminalChannelName", "chainCode", "chainTopChainName", "chainChannelName",
                "customerId", "mobilePhone", "nursingConsultant", "status", "isMatch", "useTerminalCode", "useTime", "presentTerminal", "useTerminal", 
                "motherMilkClient", "babyMilkClient", "dodieClient", "probioticsClient", "chainMothermilkclient", "chainBabymilkclient", "chainDodieclient", "chainProbioticsclient"};

        ExportUtil.wirteDefaultExcel(null, titleRange, dataList, ths, colNames, response);

        return path;
    }


    @Override
    public ServiceBean<Pager<SaSplitFlowBean>> searchSaSplitFlow(Pager<SaSplitFlowBean> pager, SaSplitFlowBean saSplitFlowBean) {
        ServiceBean<Pager<SaSplitFlowBean>> serviceBean = new ServiceBean<Pager<SaSplitFlowBean>>();
        try {
            pager = saSplitFlowRepository.searchSaSplitFlow(pager, saSplitFlowBean);
            if (CollectionUtils.isNotEmpty(pager.getDatas())) {
                for (SaSplitFlowBean splitFlowBean : pager.getDatas()) {
                    if (StringUtil.isNotNullNorEmpty(splitFlowBean.getMobilePhone()) && splitFlowBean.getMobilePhone().length() == 11) {
                        splitFlowBean.setMobilePhone(splitFlowBean.getMobilePhone().substring(0, 3) + "****" + splitFlowBean.getMobilePhone().substring(7, splitFlowBean.getMobilePhone().length()));
                    }
                }
            }
            serviceBean.setResponse(pager);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }

    @Override
    public ServiceBean exportSaSplitFlow(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobilePhone, String terminalCode, String startTime, String endTime, String terminalChannelCode, String chainChannelCode, HttpServletResponse response) {
        ServiceBean<List<CustomerDetailBean>> serviceBean = new ServiceBean<List<CustomerDetailBean>>();
        try {
            List<SaSplitFlowBean> lists = new ArrayList<SaSplitFlowBean>();

            lists = saSplitFlowRepository.getSaSplitFlow(nursingConsultant, areaCode, officeCode, saCode, customerId, mobilePhone, terminalCode, startTime, endTime, terminalChannelCode, chainChannelCode);

            writeReportExcel(response, "SA分流报表", lists);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<List<CustomerDetailBean>>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }

    @Override
    public ServiceBean addBstOtoCustSA(String saCode, String terminalCode, String customerId, String relateCode, String isNewcust, String maintenanceType, String sendCouponTerminal, String operatorUser) {
        ServiceBean<List<CustomerDetailBean>> serviceBean = new ServiceBean<List<CustomerDetailBean>>();
        try {
            String nursingConsultant = saSplitFlowRepository.getNursingConsultantBySACode(saCode);
            if (StringUtil.isNotNullNorEmpty(relateCode)) {
                relateCode = nursingConsultant;
            }
            /**
             * 根据会员id查询是否已经存在关系表（如果存在就修改，不存在就新增）
             */
            BstOtoCustSa bstOtoCustSa = saSplitFlowRepository.getBstOtoCustSAByCustomerId(customerId);

            if (null != bstOtoCustSa) {
                bstOtoCustSa.setUpdatedTime(new Date());
                if (StringUtil.isNotNullNorEmpty(maintenanceType)) {
                    bstOtoCustSa.setMaintenanceType(maintenanceType);
                }
                if (StringUtil.isNotNullNorEmpty(relateCode)) {
                    bstOtoCustSa.setRelateCode(relateCode);
                }
                if (StringUtil.isNotNullNorEmpty(saCode)) {
                    bstOtoCustSa.setSaCode(saCode);
                }
                if (StringUtil.isNotNullNorEmpty(operatorUser)) {
                    operatorUser = customerId;
                }
                bstOtoCustSa.setUpdatedBy(operatorUser);
                bstOtoCustSa.setUpdatedTime(new Date());
                saSplitFlowRepository.updateBstOtoCustSA(bstOtoCustSa);
            } else {
                if (StringUtil.isNullOrEmpty(maintenanceType)) {
                    maintenanceType = "2";
                }
                if (StringUtil.isNullOrEmpty(operatorUser)) {
                    operatorUser = customerId;
                }
                if (StringUtil.isNullOrEmpty(isNewcust)) {
                    isNewcust = "0";
                }
                if (StringUtil.isNullOrEmpty(sendCouponTerminal) || sendCouponTerminal.equals("null")) {
                    sendCouponTerminal = saCode;
                }

                String relationChainCode = saSplitFlowRepository.getRelationChainCodeBySACode(sendCouponTerminal);
                bstOtoCustSa = new BstOtoCustSa();
                bstOtoCustSa.setMaintenanceType(maintenanceType);
                bstOtoCustSa.setCustomerId(Long.parseLong(customerId));
                bstOtoCustSa.setTerminalCode(terminalCode);
                bstOtoCustSa.setSaCode(saCode);
                bstOtoCustSa.setSendCouponTerminal(sendCouponTerminal);
                bstOtoCustSa.setRelateCode(relateCode);
                bstOtoCustSa.setCreatedBy(operatorUser);
                bstOtoCustSa.setIsNewcust(isNewcust);
                bstOtoCustSa.setIsValid("1");
                bstOtoCustSa.setUpdatedBy(operatorUser);
                bstOtoCustSa.setStatus("1");
                bstOtoCustSa.setRelateType("1");
                bstOtoCustSa.setRelationChainCode(relationChainCode);

                saSplitFlowRepository.saveBstOtoCustSA(bstOtoCustSa);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<List<CustomerDetailBean>>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }


}
