package com.biostime.samanage.service.impl.member;

import com.alibaba.fastjson.JSON;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.member.*;
import com.biostime.samanage.domain.*;
import com.biostime.samanage.repository.member.SaMemberDetailRepository;
import com.biostime.samanage.service.impl.customer.SaCustomerDetailServiceImpl;
import com.biostime.samanage.service.member.SaMenberDetailService;
import com.biostime.samanage.util.ExportUtil;
import com.mama100.common.finagle.util.RpcUtil;
import com.mama100.rpc.cust.CustRpcFactory;
import com.mama100.rpc.cust.thrift.CustomerService;
import com.mama100.rpc.cust.thrift.inout.*;
import com.mama100.rpc.cust.thrift.inout.common.BaseRequest;
import com.twitter.util.Await;
import com.twitter.util.Future;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * SA销售积分
 */
@Service("saMenberDetailService")
@Slf4j
public class SaMemberDetailServiceImpl implements SaMenberDetailService {
    @Autowired
    private SaMemberDetailRepository saMemberDetailREpository;

    @Autowired
    private CustRpcFactory custRpcFactory;

    public class ImportHandleResult {

        ExgPlmOutItem item;
        int result;

    }

    /**
     * 方法描述: 根据传进去的参数导出.xlsx文档
     *
     * @param response
     * @param fileName
     * @param mlist
     * @author zhuhaitao
     * @createDate 2016年1月12日 下午6:54:34
     */
    private String writeReportExcel(HttpServletResponse response, String fileName, List<MemberDetailBean> mlist) {
        String url = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HHmmss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            fileName = fileName + sdf.format(new Date());
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(new String(fileName + ".xlsx"), "UTF-8"));

            if (CollectionUtils.isNotEmpty(mlist)) {
                for (MemberDetailBean member : mlist) {
                    if (StringUtil.isNotNullNorEmpty(member.getMobile()) && member.getMobile().length() > 8) {
                        member.setMobile(member.getMobile().substring(0, 3) + "****" + member.getMobile().substring(7, member.getMobile().length()));
                    }
                    if (StringUtil.isNotNullNorEmpty(member.getRecSrc())) {
                        member.setRecSrc(SaCustomerDetailServiceImpl.getRecSrc(member.getRecSrc()));
                    }
                    if (null != member.getBirthdate()) {
                        member.setBirthdateStr(sdf1.format(member.getBirthdate()));
                    }
                }
            }
            url = this.writeAllBrandsExcel(fileName, mlist, response);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            try {
                response.reset();
                response.setContentType("text/html;charset=utf-8");
                response.getWriter().print("导出失败！");
            } catch (Exception e1) {
                log.error(e1.getMessage(), e1);
            }
        }
        return url;
    }

    /**
     * 方法描述: 导出数据所需要的参数
     *
     * @param fileName
     * @param dataList
     * @param response
     * @return
     * @throws FileNotFoundException
     */
    private String writeAllBrandsExcel(String fileName, List<MemberDetailBean> dataList, HttpServletResponse response) throws FileNotFoundException {

        String path = "";
        // 复合表头
        String titleName[] = new String[]{"基本资料"};
        // 复合表头起止列
        Integer titleRange[][] = new Integer[][]{{0, 2}};
        // 数据列表头
        String ths[] = new String[]{
                // 基本资料
                "大区"
                , "办事处"
                , "SA编码"
                , "会员ID"
                , "会员姓名"
                , "宝宝生日/预产期"
                , "手机号码"
                , "BCC工号"
                , "实操BCC工号"
                , "育婴顾问工号"
                , "礼包/礼品"
                , "得悉途径"
                , "名单类别"
                , "创建时间"};
        // 数据列
        String colNames[] = new String[]{
                // 基本资料
                "areaName", "officeName", "saCode", "customerId", "name", "birthdateStr", "mobile", "proBcc", "operateBcc",
                "nursingConsultant", "giftName", "srcLocName", "memberType", "recTime"};

        ExportUtil.wirteDefaultExcel(null, titleRange, dataList, ths, colNames, response);

        return path;
    }

    /**
     * 分页查询SA会员明细
     *
     * @param pager
     * @param memberDetailBean
     * @return
     */
    @Override
    public ServiceBean<Pager<MemberDetailBean>> searchSaMenberDetail(Pager<MemberDetailBean> pager, MemberDetailBean memberDetailBean) {

        ServiceBean<Pager<MemberDetailBean>> serviceBean = new ServiceBean<Pager<MemberDetailBean>>();
        try {
            pager = saMemberDetailREpository.searchSaMenberDetail(pager, memberDetailBean);
            if (CollectionUtils.isNotEmpty(pager.getDatas())) {
                for (MemberDetailBean memberDetail : pager.getDatas()) {
                    if (StringUtil.isNotNullNorEmpty(memberDetail.getMobile()) && memberDetail.getMobile().length() == 11) {
                        memberDetail.setMobile(memberDetail.getMobile().substring(0, 3) + "****" + memberDetail.getMobile().substring(7, memberDetail.getMobile().length()));
                    }
                }
            }
            serviceBean.setResponse(pager);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;

    }

    /**
     * 导出SA会员明细
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param operateBcc
     * @param srcLocName
     * @param startTime
     * @param endTime
     * @param response
     * @return
     */
    @Override
    public ServiceBean exportSaMenberDetail(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobile, String proBcc, String srcLocName, String startTime, String endTime, HttpServletResponse response) {
        ServiceBean<List<MemberDetailBean>> serviceBean = new ServiceBean<List<MemberDetailBean>>();
        try {
            List<MemberDetailBean> lists = new ArrayList<MemberDetailBean>();

            lists = saMemberDetailREpository.getSaMenberDetail(nursingConsultant, areaCode, officeCode, saCode, customerId, mobile, proBcc, srcLocName, startTime, endTime);

            writeReportExcel(response, "SA会员明细", lists);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean<List<MemberDetailBean>>(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }

    @Override
    public ServiceBean<Pager<MktImportInfoBean>> searchMktImportInfo(Pager<MktImportInfoBean> pager, MktImportInfoBean importInfoBean) {
        {
            ServiceBean<Pager<MktImportInfoBean>> serviceBean = new ServiceBean<Pager<MktImportInfoBean>>();
            try {
                pager = saMemberDetailREpository.searchMktImportInfo(pager, importInfoBean);
                serviceBean.setResponse(pager);
            } catch (Exception ex) {
                ex.printStackTrace();
                serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
            }
            return serviceBean;
        }
    }

    @Override
    public ServiceBean<MktImportInfo> saveMktImportInfoBean(String filePath, String fileNameNow, String userId, Date date, int total, String type, String userName) {
        {

            ServiceBean<MktImportInfo> serviceBean = new ServiceBean<MktImportInfo>();
            try {

                MktImportInfo importInfo = new MktImportInfo();

                importInfo.setType(type);
                importInfo.setFilePath(filePath);
                importInfo.setFileName(fileNameNow);

                importInfo.setCreatedBy(userId);
                importInfo.setCreatedTime(date);
                importInfo.setTotalCount(total);
                importInfo.setUpdatedBy(userId);
                importInfo.setUpdatedTime(date);
                importInfo.setCreatedName(userName);

                saMemberDetailREpository.saveMktImportInfoBean(importInfo);
                serviceBean.setResponse(importInfo);
            } catch (Exception ex) {
                ex.printStackTrace();
                serviceBean = new ServiceBean<MktImportInfo>(500, ex.getMessage().toString());//系统异常
            }
            return serviceBean;
        }
    }

    @Override
    public ServiceBean<Map<String, Object>> importNewMember(XSSFRow row, int failed, List<String> descriptions, ExgPlmOutImportResult result, String userId) {
        {

            Map<String, Object> map = new HashMap<String, Object>();
            ServiceBean<Map<String, Object>> serviceBean = new ServiceBean<Map<String, Object>>();
            try {
                int rowNum = row.getRowNum();
                String res = "【行数】第" + rowNum + "行；【失败原因】";
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                ImportHandleResult handleResult = new ImportHandleResult();

                ExgPlmOutItem item = new ExgPlmOutItem();

                handleResult.item = item;
                handleResult.result = 0;

                String name = null;
                String createdTime = null;
                String phone = null;
                String mobilePhone = null;
                String srcLocName = null;
                String saCode = null;
                String operateBCC = null;
                String nursingConsultant = null;
                String kidBirthDayStr = null;
                String kidName = null;
                String kidSex = null;
                String province = null;
                String city = null;
                String memo = null;

                if (null != row.getCell(0)) {
                    name = row.getCell(0).toString();//*姓名
                }
                if (null != row.getCell(1)) {
                    createdTime = row.getCell(1).toString();//*注册时间
                }
                if (null != row.getCell(2)) {
                    row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
                    phone = row.getCell(2).getStringCellValue();//电话
                }
                if (null != row.getCell(3)) {
                    row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
                    mobilePhone = row.getCell(3).getStringCellValue();//*手机
                }
                if (null != row.getCell(4)) {
                    srcLocName = row.getCell(4).toString();//*得悉途径
                }
                if (null != row.getCell(5)) {
                    row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
                    saCode = row.getCell(5).getStringCellValue();//*SA编号
                }
                if (null != row.getCell(6)) {
                    row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
                    operateBCC = row.getCell(6).getStringCellValue();//BCC工号
                }
                if (null != row.getCell(7)) {
                    row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);
                    nursingConsultant = row.getCell(7).getStringCellValue();//育婴顾问工号
                }
                if (null != row.getCell(8)) {
                    kidBirthDayStr = row.getCell(8).toString();//*宝宝生日/预产期
                }
                if (null != row.getCell(9)) {
                    kidName = row.getCell(9).toString();//宝宝姓名
                }
                if (null != row.getCell(10)) {
                    kidSex = row.getCell(10).toString();//宝宝性别
                }
                if (null != row.getCell(11)) {
                    province = row.getCell(11).toString();//省份
                }
                if (null != row.getCell(12)) {
                    city = row.getCell(12).toString();//市或县
                }
                if (null != row.getCell(13)) {
                    memo = row.getCell(13).toString();//备注
                }


                Date kidBirthDay = null;
                Date createdDate = null;
                String srcLocCode = null;
                String operateBccOfficeCode = "";
                String nursingConsultantOfficeCode = "";

                if (StringUtils.isBlank(name)) {
                    descriptions.add(res + "会员姓名为空");
                    handleResult.result = -1;
                }

                if (StringUtils.isNotBlank(createdTime)) {
                    try {
                        createdDate = format.parse(createdTime);
                    } catch (Exception e) {
                        descriptions.add(res + "注册时间" + createdTime + "格式不正确, 【正确格式】示例:  2017-01-01");
                        handleResult.result = -1;
                    }
                } else {
                    descriptions.add(res + "注册时间为空");
                    handleResult.result = -1;
                }
                CommonAccountList commonAccountList = null;
                if (StringUtils.isNotBlank(operateBCC)) {

                    commonAccountList = saMemberDetailREpository.getCommonAccountListCode(operateBCC, 0);

                    if (null == commonAccountList) {
                        descriptions.add(res + "BCC工号:" + operateBCC + "不正确");
                        handleResult.result = -1;
                    } else {
                        operateBccOfficeCode = commonAccountList.getDepartmentCode();
                    }
                }
                CommonAccountList ncommonAccountList = null;
                if (StringUtils.isNotBlank(nursingConsultant)) {

                    ncommonAccountList = saMemberDetailREpository.getCommonAccountListCode(nursingConsultant, 1);

                    if (null == ncommonAccountList) {
                        descriptions.add(res + "育婴顾问:" + nursingConsultant + "不正确");
                        handleResult.result = -1;
                    } else {
                        nursingConsultantOfficeCode = ncommonAccountList.getDepartmentCode();
                    }
                }


                if (StringUtils.isBlank(srcLocName)) {
                    descriptions.add(res + "得悉途径为空");
                    handleResult.result = -1;
                } else {
                    HpSrcLoc hpSrcLoc = saMemberDetailREpository.getHpSrcLoc(srcLocName);
                    if (null == hpSrcLoc) {
                        descriptions.add(res + "得悉途径 " + srcLocName + " 不正确");
                        handleResult.result = -1;
                    } else {
                        srcLocCode = hpSrcLoc.getCode();
                    }
                }


                if (StringUtils.isNotBlank(phone)) {
                    if (phone.indexOf("-") != -1 && (phone.length() < 11 || phone.length() > 13)) {
                        descriptions.add(res + "电话 " + phone + " 格式不正确或位数不对（位数需11-13位 例:020-12345678）");
                        handleResult.result = -1;
                    }
                }
                // 手机为必填字段
                if (StringUtils.isBlank(mobilePhone)) {
                    descriptions.add(res + "手机号码为空");
                    handleResult.result = -1;
                } else {
                    if (!StringUtils.isNumeric(mobilePhone) || mobilePhone.length() != 11) {
                        descriptions.add(res + "手机 " + mobilePhone + " 格式不正确或位数不对（位数需11位）");
                        handleResult.result = -1;
                    }
                }
                /*TCustomer4App mobileCust = null;
                TCustomer4App phoneCust = null;
                //获取会员RPC接口
                CustomerService.ServiceIface custRpcService = custRpcFactory.getCustomerService();
                BaseRequest baseRequest = new BaseRequest();
                if (StringUtils.isNotBlank(mobilePhone)) {
                    baseRequest.setSeqNo(RpcUtil.genSeqNo());
                    Future<Customer4AppResponse> mobileCustFuture = custRpcService.getCustomerByContact(baseRequest, 1, mobilePhone);
                    Customer4AppResponse mobileCustResponse = Await.result(mobileCustFuture);
                    mobileCust = mobileCustResponse.getValue();

                    baseRequest.setSeqNo(RpcUtil.genSeqNo());
                    Future<Customer4AppResponse> phoneCustFuture = custRpcService.getCustomerByContact(baseRequest, 2, mobilePhone);
                    Customer4AppResponse phoneCustResponse = Await.result(phoneCustFuture);
                    phoneCust = phoneCustResponse.getValue();
                }
                if (mobileCust != null || phoneCust != null) {
                    descriptions.add(res + "会员已存在");
                    handleResult.result = -1;
                }*/


                String serialNum = "";
                if (StringUtils.isNotBlank(kidBirthDayStr)) {
                    try {
                        kidBirthDay = format.parse(kidBirthDayStr);
                    } catch (Exception e) {
                        descriptions.add(res + "宝宝生日" + kidBirthDayStr + "格式不正确, 【正确格式】示例:  2017-01-01");
                        handleResult.result = -1;
                    }
                } else {
                    descriptions.add(res + "宝宝生日为空！");
                    handleResult.result = -1;
                }
                if (null != kidBirthDay) {
                    Date date = DateUtils.addYears(new Date(), 1);
                    if (kidBirthDay.getTime() > date.getTime()) {
                        descriptions.add(res + "宝宝生日/预产期大于当前时间超过一年！");
                        handleResult.result = -1;
                    }
                }
                if (StringUtil.isNotNullNorEmpty(kidSex)) {
                    if (!"男".equals(kidSex) && !"女".equals(kidSex) && !"未知".equals(kidSex)) {
                        descriptions.add(res + "宝宝性别不正确, 【正确格式】示例:  男、女、未知");
                        handleResult.result = -1;
                    }
                }
                HpMemberEventRec memberImportRec = new HpMemberEventRec();
                if (StringUtils.isBlank(saCode)) {
                    descriptions.add(res + "SA编码为空");
                    handleResult.result = -1;
                } else {
                    SaTerminal saTerminal = saMemberDetailREpository.getSAByCode(saCode);
                    String saOfficeCode = "";
                    String terminalName = "";
                    if (null == saTerminal) {
                        descriptions.add(res + "SA编码：" + saCode + "，不存在");
                        handleResult.result = -1;
                    } else {
                        saOfficeCode = saTerminal.getDepartmentCode();
                        terminalName = saTerminal.getName();
                    }

                    /*
                    SaTerminal terminal = saMemberDetailREpository.getTerminalByCode(saCode);

                    if (null == terminal) {
                        descriptions.add(res + "SA编号: " + saCode + " 在CRM不存在");
                        handleResult.result = -1;
                    } else {

                        terminalName = terminal.getName();
                    }*/

                    /*if (StringUtils.isBlank(saOfficeCode)) {
                        descriptions.add(res + "SA编号对应的办事处" + saOfficeCode + "为空");
                        handleResult.result = -1;
                    }*/
                    /**
                     * SA、BCC、育婴顾问，暂只校验三者处于同一办事处
                     */
                    if (null != commonAccountList) {
                        if (StringUtils.isBlank(operateBccOfficeCode)) {
                            descriptions.add(res + "BCC工号对应的办事处" + operateBccOfficeCode + "为空");
                            handleResult.result = -1;
                        }
                        if (StringUtil.isNotNullNorEmpty(saOfficeCode) && StringUtil.isNotNullNorEmpty(operateBccOfficeCode)) {
                            if (!operateBccOfficeCode.equals(saOfficeCode)) {
                                descriptions.add(res + "BCC工号" + operateBCC + "办事处与SA" + saCode + "办事处不对应");
                                handleResult.result = -1;
                            }
                        }
                    }
                    if (null != ncommonAccountList) {
                        if (StringUtils.isBlank(nursingConsultantOfficeCode)) {
                            descriptions.add(res + "育婴顾问对应的办事处" + nursingConsultantOfficeCode + "为空");
                            handleResult.result = -1;
                        }
                        if (StringUtil.isNotNullNorEmpty(saOfficeCode) && StringUtil.isNotNullNorEmpty(nursingConsultantOfficeCode)) {
                            if (!nursingConsultantOfficeCode.equals(saOfficeCode)) {
                                descriptions.add(res + "育婴顾问" + nursingConsultant + "办事处与SA" + saCode + "办事处不对应");
                                handleResult.result = -1;
                            }
                        }
                    }

                    if (StringUtil.isNotNullNorEmpty(saOfficeCode)) {
                        List<CommonDepartment> officeCodes = saMemberDetailREpository.queryOfficeCode(saOfficeCode, saOfficeCode.substring(0, 4));
                        String officeCode = officeCodes.get(1).getCode();
                        String officeName = officeCodes.get(1).getName();
                        String areaCode = officeCodes.get(0).getCode();
                        String areaName = officeCodes.get(0).getName();
                        memberImportRec.setOfficeCode(officeCode);
                        memberImportRec.setOfficeName(officeName);
                        memberImportRec.setAreaCode(areaCode);
                        memberImportRec.setAreaName(areaName);
                    }

                    memberImportRec.setRecType(1);
                    memberImportRec.setRecSrcType(7);
                    memberImportRec.setTerminalCode(saCode);
                    memberImportRec.setTerminalName(terminalName);
                    memberImportRec.setName(name);
                    //memberImportRec.setEventTime(eventTimeDate);
                    //memberImportRec.setCustomerId(customer.getId());

                    memberImportRec.setMobile(mobilePhone);
                    memberImportRec.setPhone(phone);
                    memberImportRec.setProvince(province);
                    memberImportRec.setCity(city);
                    //memberImportRec.setPostcode(postCode);
                    //memberImportRec.setAddress(address);
                    //memberImportRec.setEstimatedDate(estimatedDateStr);
                    memberImportRec.setKidName(kidName);
                    memberImportRec.setKidBirthday(kidBirthDay);
                    //memberImportRec.setMclassActTheme(mclassActTheme);
                    //memberImportRec.setMclassActLoc(mclassActLoc);
                    memberImportRec.setOperator(userId);
                    //memberImportRec.setBatchId(batchId);
                    memberImportRec.setCreatedTime(new Date());

            /*if (srcLocName.equals("妈妈班") || srcLocName.equals("孕妇班")) {
                memberImportRec.setMemberType("妈妈班会员");
            } else {
                memberImportRec.setMemberType(memberType);
            }*/

                    memberImportRec.setMemo(memo);
                    memberImportRec.setResultCode("05"); // 电访待审核

                    memberImportRec.setSrcLocCode(srcLocCode);
                    memberImportRec.setSrcLocName(srcLocName);
           /* if (StringUtils.isNotBlank(giftTypeCode)) {

                memberImportRec.setGiftTypeCode(giftTypeCode);
            } else {

                memberImportRec.setGiftTypeCode(giftCatCode);
            }*/

                    //memberImportRec.setGiftTypeName(giftTypeName);

                    //memberImportRec.setGiftCatCode(giftCatCode);
                    if (StringUtil.isNotNullNorEmpty(operateBCC)) {
                        memberImportRec.setOperateBcc(operateBCC);
                    }
                    /**
                     * 如果育婴顾问为空，则将SA编号写入（以防数据库出错）
                     */
                    if (StringUtil.isNotNullNorEmpty(nursingConsultant)) {
                        memberImportRec.setDoctorCode(nursingConsultant);
                    } else {
                        memberImportRec.setDoctorCode(saCode);
                    }
                    //memberImportRec.setOperateBCCName(operateBCCName);
                    //memberImportRec.setDoctorBCC(parentNo);
                    //memberImportRec.setDoctorBCCName(doctorBCCName);

                    memberImportRec.setSerialNo(serialNum);
                    //memberImportRec.setSecurityNo(securityNum);


                }



            /*if (StringUtils.isNotBlank(estimatedDateStr)) {
                try {
                    estimatedDate = format.parse(estimatedDateStr);
                } catch (Exception e) {
                    item.setDescription("预产期 " + estimatedDateStr
                            + " 格式不正确, 正确的格式示例: 2010-02-06");
                    handleResult.result = -1;
                }
            }*/





            /*String parentNo = bccMap.get(saCode);

            String doctorBCCNameHql = " from HytSalesAccount where accountNo = :parentNo ";

            Query queryDoctorBCCName = session.createQuery(doctorBCCNameHql);

            queryDoctorBCCName.setString("parentNo", parentNo);
            List<HytSalesAccount> listDoctorBCCName = queryDoctorBCCName.list();

            if (listDoctorBCCName.isEmpty()) {
                item.setDescription("所属BCC编号" + parentNo + "已不存在,请联系医务部.");
                handleResult.result = -1;
            }

            String doctorBCCName = listDoctorBCCName.get(0).getName();

            String operateBCCNameHql = " from HytSalesAccount where accountNo = :operateBCC ";

            Query queryOperateBCCName = session.createQuery(operateBCCNameHql);

            System.out.println("operateBCC" + operateBCC);
            queryOperateBCCName.setString("operateBCC", operateBCC);
            List<HytSalesAccount> listOperateBCCName = queryOperateBCCName
                    .list();

            String operateBCCName = "";
            if (!listOperateBCCName.isEmpty()) {
                operateBCCName = listOperateBCCName.get(0).getName();
            }

            if (StringUtils.isBlank(officeCode)) {
                item.setDescription("SA所有对应的办事处编号不存在,请联系相关负责人.");
                handleResult.result = -1;
            }

            if (StringUtils.isBlank(areaCode)) {
                item.setDescription("SA所有对应的大区编号不存在,请联系相关负责人.");
                handleResult.result = -1;
            }*/


                CrmCustomer customer = null;

                /*try {

                    CreateOrUpdateCustomerResult custResult = createOrUpdateCustomer(name, mobilePhone, phone, kidName, kidBirthDay, createdDate,
                            saCode, saCode, srcLocName, saMemberDetailREpository);

                    if (!custResult.getRespCode().equals("100")) {
                        item.setDescription(custResult.getRespDesc() + " - " + custResult.getRespCode());
                        handleResult.result = -1;
                    }

                    customer = custResult.getCustomer();

                } catch (Exception e) {
                    e.printStackTrace();
                    serviceBean = new ServiceBean<Map<String, Object>>(4020, PropUtils.getProp(4020));// 系统异常
                    return serviceBean;
                }*/


                if (handleResult.result < 0) {
                    failed++;
                }
                map.put("failed", failed);
                if (handleResult.result == 0) {
                    NewMemberBean bean = new NewMemberBean();
                    bean.setCity(city);
                    bean.setCreatedTime(createdDate);
                    bean.setKidBirthDay(kidBirthDay);
                    bean.setKidName(kidName);
                    bean.setMemo(memo);
                    bean.setMobilePhone(mobilePhone);
                    bean.setNursingConsultring(nursingConsultant);
                    bean.setOperateBCC(operateBCC);
                    bean.setPhone(phone);
                    bean.setProvince(province);
                    bean.setSaCode(saCode);
                    bean.setSrcLocName(srcLocName);
                    bean.setKidSex(kidSex);
                    bean.setName(name);
                    bean.setOperator(userId);
                    bean.setRowNun(rowNum);
                    if (null != memberImportRec) {
                        bean.setHpMemberEventRec(memberImportRec);
                    }

                    map.put("bean", bean);
                } else {
                    map.put("bean", null);
                }
                serviceBean.setResponse(map);

            } catch (Exception ex) {
                ex.printStackTrace();
                serviceBean = new ServiceBean<Map<String, Object>>(4020, PropUtils.getProp(4020));// 系统异常
                return serviceBean;
            }
            return serviceBean;
        }
    }

    @Override
    public ServiceBean<String> addNewMember(List<NewMemberBean> newMemberBeanList) throws Exception {
        ServiceBean<String> serviceBean = new ServiceBean<String>();
        try {
            for (NewMemberBean bean : newMemberBeanList) {
                if (null != bean) {
                    String channelCode = "08";

                    Long customerId = null;

                    //获取会员RPC接口
                    CustomerService.ServiceIface custRpcService = custRpcFactory.getCustomerService();

                    TCustomer4App mobileCust = null;
                    TCustomer4App phoneCust = null;
                    BaseRequest baseRequest = new BaseRequest();
                    if (StringUtils.isNotBlank(bean.getMobilePhone())) {
                        baseRequest.setSeqNo(RpcUtil.genSeqNo());
                        Future<Customer4AppResponse> mobileCustFuture = custRpcService.getCustomerByContact(baseRequest, 1, bean.getMobilePhone());
                        Customer4AppResponse mobileCustResponse = Await.result(mobileCustFuture);
                        mobileCust = mobileCustResponse.getValue();
                    }
                    if (StringUtils.isNotBlank(bean.getMobilePhone())) {
                        baseRequest.setSeqNo(RpcUtil.genSeqNo());
                        Future<Customer4AppResponse> phoneCustFuture = custRpcService.getCustomerByContact(baseRequest, 2, bean.getMobilePhone());
                        Customer4AppResponse phoneCustResponse = Await.result(phoneCustFuture);
                        phoneCust = phoneCustResponse.getValue();
                    }

                    if (mobileCust != null || phoneCust != null) {
                        customerId = mobileCust != null ? mobileCust.getId() : phoneCust.getId();
                        int count = saMemberDetailREpository.getCrmCustomerCount(customerId, channelCode);

                        if (count == 0) {
                            // 新增会员渠道
                            CrmCustTerminalChannelJpa crmCustChannelJpa = new CrmCustTerminalChannelJpa();
                            crmCustChannelJpa.setCustomerId(Integer.parseInt(customerId.toString()));
                            crmCustChannelJpa.setChannelCode(channelCode);
                            crmCustChannelJpa.setCreatedTime(bean.getCreatedTime());
                            crmCustChannelJpa.setFirstDealDate(bean.getCreatedTime());
                            crmCustChannelJpa.setUpdatedTime(bean.getCreatedTime());
                            crmCustChannelJpa.setCreatedBy(bean.getOperator());
                            crmCustChannelJpa.setUpdatedBy(bean.getOperator());
                            crmCustChannelJpa.setTerminalCode(bean.getSaCode());

                            saMemberDetailREpository.saveCrmCustTerminalChannelJpa(crmCustChannelJpa);
                        }

                    } else {

                        // 创建一个新会员

                        CustCustomerResponse resp = null;
                        AddCustomer4PosRequest addCustReq = new AddCustomer4PosRequest();
                        BaseRequest baseReq = new BaseRequest();
                        baseReq.setSeqNo(RpcUtil.genSeqNo());
                        addCustReq.setBaseReq(baseReq);
                        addCustReq.setMobile(bean.getMobilePhone());
                        addCustReq.setChannelCode("08");
                        addCustReq.setTerminalCode(bean.getSaCode());
                        addCustReq.setCreatedBy(bean.getOperator());
                        addCustReq.setFromSystem("MEDICAL_HYT");
                        addCustReq.setPhone(bean.getPhone());
                        addCustReq.setName(bean.getName());

                        log.info("AddCustomer4PosRequest parameters{}:{}", JSON.toJSON(addCustReq));
                        Future<CustCustomerResponse> addCustFuture = custRpcService.addCustomer4Pos(addCustReq);
                        CustCustomerResponse addCustRes = Await.result(addCustFuture);
                        if (StringUtil.isNotNullNorEmpty(addCustRes.getBaseResp().getRespCode()) && !"100".equals(addCustRes.getBaseResp().getRespCode())) {
                            serviceBean.setCode(Integer.parseInt(addCustRes.getBaseResp().getRespCode()));
                            serviceBean.setDesc(addCustRes.getBaseResp().getRespDesc());
                            return serviceBean;
                        }
                        customerId = addCustRes.getValue().getId();
                        //绑定与门店的关系
                        AddCustTerminalRequest terminalRequest = new AddCustTerminalRequest();

                        terminalRequest.setBaseReq(baseRequest);

                        terminalRequest.setTerminalCode(bean.getSaCode());
                        terminalRequest.setTerminalType(3);
                        terminalRequest.setCustomerId(customerId);

                        custRpcFactory.getCustTerminalService().addCustTerminal(terminalRequest);
                    }
                    if (null != customerId) {
                        BaseRequest baseRequest1 = new BaseRequest();
                        baseRequest1.setSeqNo(RpcUtil.genSeqNo());
                        TCustKid kid = new TCustKid();
                        kid.setCustomerId(customerId);
                        kid.setBirthdate(bean.getKidBirthDay().getTime());
                        kid.setName(bean.getKidName());
                        if (StringUtil.isNotNullNorEmpty(bean.getKidSex())) {
                            if (bean.getKidSex().equals("男")) {
                                kid.setGender(0);
                            } else if (bean.getKidSex().equals("女")) {
                                kid.setGender(1);
                            } else if (bean.getKidSex().equals("未知")) {
                                kid.setGender(2);
                            }
                        } else {
                            kid.setGender(2);
                        }
                        kid.setCreatedBy(bean.getOperator());
                        bean.setCreatedTime(new Date());
                        log.info("addCustKid parameters{}:{}", JSON.toJSON(kid));
                        log.info("BaseRequest parameters{}:{}", JSON.toJSON(baseRequest1));

                        custRpcFactory.getKidService().addCustKid(baseRequest1, kid);

                        /*TCustAddress address = new TCustAddress();
                        custRpcFactory.getAddressService().addCustomerAddress(baseRequest1,address);*/

                    }
                    serviceBean.setCode(100);
                    serviceBean.setDesc("导入成功！");

                    /*if (null != bean.getHpMemberEventRec()) {
                        bean.getHpMemberEventRec().setCustomerId(Integer.parseInt(customerId.toString()));
                        saMemberDetailREpository.addMemberImportRec(bean.getHpMemberEventRec());
                    }*/
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
        }
        return serviceBean;
    }


    /*public static CreateOrUpdateCustomerResult createOrUpdateCustomer(
            String name, String mobile,
            String phone, String kidName,
            Date kidBirthDay, Date now,
            String terminalCode, String operator,
            String srcLocName, SaMemberDetailRepository saMemberDetailREpository) throws Exception {

        CreateOrUpdateCustomerResult result = new CreateOrUpdateCustomerResult();

        String channelCode = "08";

        Long customerId = null;

        if (StringUtils.isBlank(mobile) && StringUtils.isBlank(phone)) {
            result.setRespCode("102");
            result.setRespDesc("手机或固话全为空");
            return result;
        }

        //获取会员RPC接口
        CustRpcFactory custRpcFactory = (CustRpcFactory) ContextLoader.getCurrentWebApplicationContext().getBean("custRpcFactory");
        CustomerService.ServiceIface custRpcService = custRpcFactory.getCustomerService();

        TCustomer4App mobileCust = null;
        TCustomer4App phoneCust = null;
        BaseRequest baseRequest = new BaseRequest();
        if (StringUtils.isNotBlank(mobile)) {
            baseRequest.setSeqNo(RpcUtil.genSeqNo());
            Future<Customer4AppResponse> mobileCustFuture = custRpcService.getCustomerByContact(baseRequest, 1, mobile);
            Customer4AppResponse mobileCustResponse = Await.result(mobileCustFuture);
            mobileCust = mobileCustResponse.getValue();
        }
        if (StringUtils.isNotBlank(phone)) {
            baseRequest.setSeqNo(RpcUtil.genSeqNo());
            Future<Customer4AppResponse> phoneCustFuture = custRpcService.getCustomerByContact(baseRequest, 2, phone);
            Customer4AppResponse phoneCustResponse = Await.result(phoneCustFuture);
            phoneCust = phoneCustResponse.getValue();
        }

        CrmCustomer customer = null;

        if (mobileCust != null || phoneCust != null) {

            System.out.println("customer exist.");
            customerId = mobileCust != null ? mobileCust.getId() : phoneCust.getId();
            //获取会员信息
            customer = saMemberDetailREpository.getCustomerById(customerId);
            int count = saMemberDetailREpository.getCrmCustomerCount(customerId, channelCode);

            if (count == 0) {

                // 新增会员渠道

                CrmCustTerminalChannelJpa crmCustChannelJpa = new CrmCustTerminalChannelJpa();
                crmCustChannelJpa.setCustomerId(Integer.parseInt(customerId.toString()));
                crmCustChannelJpa.setChannelCode(channelCode);
                crmCustChannelJpa.setCreatedTime(now);
                crmCustChannelJpa.setFirstDealDate(now);
                crmCustChannelJpa.setUpdatedTime(now);
                crmCustChannelJpa.setCreatedBy(operator);
                crmCustChannelJpa.setUpdatedBy(operator);
                crmCustChannelJpa.setTerminalCode(terminalCode);

                saMemberDetailREpository.saveCrmCustTerminalChannelJpa(crmCustChannelJpa);
            }

        } else {

            // 创建一个新会员

            System.out.println("customer not exist.");

            CustCustomerResponse resp = null;
            resp = createCustomer(name, mobile, phone, kidName, kidBirthDay, now, terminalCode, operator, srcLocName);
            if (!com.mama100.rpc.cust.constant.ResponseCode.SUCCESS.getCode().equals(resp.getBaseResp().getRespCode())) {
                result.setRespCode(resp.getBaseResp().getRespCode());
                result.setRespDesc(resp.getBaseResp().getRespDesc());
                return result;
            }

            customerId = resp.getValue().getId();

            customer = saMemberDetailREpository.getCustomerById(customerId);
        }

        result.setCustomer(customer);

        result.setRespCode("100");
        result.setRespDesc("创建或更新会员成功");

        return result;
    }

    public static CustCustomerResponse createCustomer(
            String name,
            String mobile,
            String phone,
            String kidName,
            Date kidBirthDay,
            Date now,
            String terminalCode,
            String operator,
            String srcLocName) throws Exception {

        String channelCode = "08";

        //获取会员RPC接口
        CustRpcFactory custRpcFactory = (CustRpcFactory) ContextLoader.getCurrentWebApplicationContext().getBean("custRpcFactory");
        CustomerService.ServiceIface custRpcService = custRpcFactory.getCustomerService();
        AddCustomer4PosRequest addCustReq = new AddCustomer4PosRequest();
        BaseRequest baseReq = new BaseRequest();
        baseReq.setSeqNo(RpcUtil.genSeqNo());
        addCustReq.setBaseReq(baseReq);
        addCustReq.setMobile(mobile);
        addCustReq.setChannelCode(channelCode);
        addCustReq.setTerminalCode(terminalCode);
        addCustReq.setCreatedBy(operator);
        addCustReq.setFromSystem("MEDICAL_HYT");

        long startTime = System.currentTimeMillis();

        Future<CustCustomerResponse> addCustFuture = custRpcService.addCustomer4Pos(addCustReq);
        CustCustomerResponse addCustRes = Await.result(addCustFuture);
        System.out.println("create customer res code: " + addCustRes.getBaseResp().getRespCode());
        System.out.println("create customer res code desc: " + addCustRes.getBaseResp().getRespDesc());

        long timeTake = System.currentTimeMillis() - startTime;
        double timeTaken = ((double) timeTake) / 1000;

        log.info("rpc create customer called. time taken: " + timeTaken + " s.");

        return addCustRes;
    }*/
}
