package com.biostime.samanage.service.impl.act;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.actreport.ActRePortWebReqBean;
import com.biostime.samanage.bean.actreport.ActRePortWebResBean;
import com.biostime.samanage.bean.actreport.ActRePortZhiBoReqBean;
import com.biostime.samanage.bean.actroadshow.*;
import com.biostime.samanage.repository.act.ActRoadShowRepository;
import com.biostime.samanage.service.act.ActRoadShowService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 活动现场ServiceImpl
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2016-08-16
 */
@Service("actRoadShowService")
@Slf4j
public class ActRoadShowServiceImpl implements ActRoadShowService {


    @Autowired
    private ActRoadShowRepository actRoadShowRepository;



    /**
     * 活动现场查询
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<ActRoadShowListResBean>> queryActRoadShowList(Pager<ActRoadShowListResBean> pager, ActRoadShowListReqBean reqBean) {
        ServiceBean<Pager<ActRoadShowListResBean>> serviceBean = new ServiceBean<Pager<ActRoadShowListResBean>>();
        try {
            serviceBean.setResponse(actRoadShowRepository.queryActRoadShowList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 活动现场保存
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveActRoadShow(ActRoadShowSaveReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            serviceBean = actRoadShowRepository.saveActRoadShow(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 活动现场状态更改
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean changeStatusActRoadShow(ActRoadShowChangeStatusReqBean reqBean) {

        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            actRoadShowRepository.changeStatusActRoadShow(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 活动现场-获取swisse列表、活动类型
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<ActRoadShowResBean>> querySwActTypeList(ActRoadShowReqBean reqBean) {
        ServiceBean<List<ActRoadShowResBean>> serviceBean = new ServiceBean<List<ActRoadShowResBean>>();
        try {
            serviceBean.setResponse(actRoadShowRepository.querySwActTypeList(reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 活动现场导出
     * @param bigSmallCaravanActivityReqBean
     * @return
     */
    @Override
    public ServiceBean exportActRoadShow(HttpServletResponse response,ActRoadShowListReqBean bigSmallCaravanActivityReqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            actRoadShowRepository.exportActRoadShow(response, bigSmallCaravanActivityReqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 营销上报明细查询
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<ActRoadShowDetailResBean>> queryActRoadShowDetailList(Pager<ActRoadShowDetailResBean> pager, ActRoadShowListReqBean reqBean) {
        ServiceBean<Pager<ActRoadShowDetailResBean>> serviceBean = new ServiceBean<Pager<ActRoadShowDetailResBean>>();
        try {
            serviceBean.setResponse(actRoadShowRepository.queryActRoadShowDetailList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 营销上报明细-审核
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean  auditActRoadShowDetail(ActRoadShowAuditReqBean reqBean){
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            actRoadShowRepository.auditActRoadShowDetail(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 营销上报明细导出
     * @param response
     * @param bigSmallCaravanActivityReqBean
     * @return
     */
    @Override
    public ServiceBean exportActRoadShowDetail(HttpServletResponse response, ActRoadShowListReqBean bigSmallCaravanActivityReqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            actRoadShowRepository.exportActRoadShowDetail(response, bigSmallCaravanActivityReqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 路演报表查询
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<ActRoadShowReportResBean>> queryReportActRoadShowList(Pager<ActRoadShowReportResBean> pager, ActRoadShowReportReqBean reqBean){
        ServiceBean<Pager<ActRoadShowReportResBean>> serviceBean = new ServiceBean<Pager<ActRoadShowReportResBean>>();
        try {
            serviceBean.setResponse(actRoadShowRepository.queryReportActRoadShowList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 路演报表查询导出
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportReportActRoadShow(HttpServletResponse response, ActRoadShowReportReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            actRoadShowRepository.exportReportActRoadShow(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 活动现场--门店导出
     * @param response
     * @param actId    活动ID
     * @return
     */
    @Override
    public ServiceBean exportActRoadShowTerminal(HttpServletResponse response, String actId) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            actRoadShowRepository.exportActRoadShowTerminal(response, actId);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 活动现场--门店导入
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<String> importActRoadShowTerminal(MultipartFile multipartFile,ActRoadShowTerminalReqBean reqBean) {
        ServiceBean<String> serviceBean = new ServiceBean<String>();
        try {
            String msg = actRoadShowRepository.importActRoadShowTerminal(multipartFile, reqBean);
            if(StringUtil.isNotNullNorEmpty(msg)) {
                serviceBean.setCode(500);
                serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
                serviceBean.setResponse(msg);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 营销上报明细-补录
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveActRoadShowMakeup(ActRePortWebReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            actRoadShowRepository.saveActRoadShowMakeup(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 营销上报明细-补录活动验证
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<ActRePortWebResBean> checkActShowInfo(ActRePortWebReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean.setResponse(actRoadShowRepository.checkActShowInfo(reqBean));
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 营销上报明细-补录活动门店验证
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean checkActTerminal(ActRePortWebReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            serviceBean = actRoadShowRepository.checkActTerminal(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 营销上报明细-上报人验证
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean checkAccountTerminal(ActRePortWebReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            serviceBean = actRoadShowRepository.checkAccountTerminal(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 活动精英上报明细查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<ActRoadShowDetailResBean>> queryActElitesReportDetailList(Pager<ActRoadShowDetailResBean> pager, ActRoadShowListReqBean reqBean) {
        ServiceBean<Pager<ActRoadShowDetailResBean>> serviceBean = new ServiceBean<Pager<ActRoadShowDetailResBean>>();
        try {
            serviceBean.setResponse(actRoadShowRepository.queryActElitesReportDetailList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 活动精英上报明细导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportActElitesReportDetailList(HttpServletResponse response, ActRoadShowListReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            actRoadShowRepository.exportActElitesReportDetailList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 直播上报报表查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<ActRePortZhiBoReqBean>> queryActReportZhiBoList(Pager<ActRePortZhiBoReqBean> pager, ActRePortZhiBoReqBean reqBean) {
        ServiceBean<Pager<ActRePortZhiBoReqBean>> serviceBean = new ServiceBean<Pager<ActRePortZhiBoReqBean>>();
        try {
            serviceBean.setResponse(actRoadShowRepository.queryActReportZhiBoList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 直播上报报表导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportActReportZhiBoList(HttpServletResponse response, ActRePortZhiBoReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            actRoadShowRepository.exportActReportZhiBoList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
