package com.biostime.samanage.service.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.ftf.report.FtfPointDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfPointDetailResBean;
import com.biostime.samanage.repository.ftf.FtfPointDetailRepository;
import com.biostime.samanage.service.ftf.FtfPointDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动积分信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("ftfPointDetailService")
@Slf4j
public class FtfPointDetailServiceImpl implements FtfPointDetailService {
    @Autowired
    private FtfPointDetailRepository ftfPointDetailRepository;

    /**
     * FTF活动积分信息
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<FtfPointDetailResBean>> queryFtfPointDetailList(Pager<FtfPointDetailResBean> pager, FtfPointDetailReqBean reqBean) {
        ServiceBean<Pager<FtfPointDetailResBean>> serviceBean = new ServiceBean<Pager<FtfPointDetailResBean>>();
        try {
            serviceBean.setResponse(ftfPointDetailRepository.queryFtfPointDetailList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF活动积分信息-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportFtfPointDetailList(HttpServletResponse response, FtfPointDetailReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            ftfPointDetailRepository.exportFtfPointDetailList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
