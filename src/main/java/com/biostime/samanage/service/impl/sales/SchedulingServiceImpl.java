package com.biostime.samanage.service.impl.sales;

import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.sales.SchedulingManageReqBean;
import com.biostime.samanage.bean.sales.SchedulingManageResBean;
import com.biostime.samanage.bean.sales.SchedulingReqBean;
import com.biostime.samanage.bean.sales.SchedulingResBean;
import com.biostime.samanage.repository.sales.SchedulingRepository;
import com.biostime.samanage.service.sales.SchedulingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:工作排版
 * Date: 2019-03-15
 * Time: 11:37
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Service("schedulingService")
@Slf4j
public class SchedulingServiceImpl implements SchedulingService {
    @Autowired
    private SchedulingRepository schedulingRepository;


    /**
     * 查询排班详情
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<SchedulingResBean>> querySchedulingInfo(SchedulingReqBean reqBean) {
        ServiceBean<List<SchedulingResBean>> serviceBean = new ServiceBean<List<SchedulingResBean>>();
        try {
            List<SchedulingResBean> activityResBean = schedulingRepository.querySchedulingInfo(reqBean);
            serviceBean.setResponse(activityResBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询排班管理详情
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<SchedulingManageResBean>> querySchedulingManageInfo(SchedulingManageReqBean reqBean) {

        ServiceBean<List<SchedulingManageResBean>> serviceBean = new ServiceBean<>();
        try {
            List<SchedulingManageResBean> schedulingManageResBean = schedulingRepository.querySchedulingManageInfo(reqBean);
            serviceBean.setResponse(schedulingManageResBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询人员与终端
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<List<BaseKeyValueBean>> queryAccountTerminal(SchedulingReqBean reqBean) {
        ServiceBean<List<BaseKeyValueBean>> serviceBean = new ServiceBean<>();
        try {
            List<BaseKeyValueBean> baseKeyValueBean = schedulingRepository.queryAccountTerminal(reqBean);
            serviceBean.setResponse(baseKeyValueBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 保存排班
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveScheduling(SchedulingReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean = schedulingRepository.saveScheduling(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 排班查询列表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSchedulingList(HttpServletResponse response, SchedulingManageReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            schedulingRepository.exportSchedulingList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }


}
