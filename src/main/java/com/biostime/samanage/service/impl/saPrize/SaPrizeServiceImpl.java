package com.biostime.samanage.service.impl.saPrize;

import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.saPrize.*;
import com.biostime.samanage.repository.saPrize.SaPrizeRepository;
import com.biostime.samanage.service.saPrize.SaPrizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA. Describe:获取SA育婴顾问奖金impl Date: 2016-6-23 Time:
 * 14:46 User: 12804 Version:1.0
 */
@Service("saPrizeService")
public class SaPrizeServiceImpl implements SaPrizeService {

	@Autowired
	private SaPrizeRepository saPrizeRepository;

	@Override
	public ServiceBean<List<SaPrizeResBean>> querySaPrizeList(SaPrizeReqBean saPrizeReqBean) {
		ServiceBean<List<SaPrizeResBean>> serviceBean = new ServiceBean<List<SaPrizeResBean>>();
		try {
			List<SaPrizeResBean> saPrizeResBean = saPrizeRepository.querySaPrizeList(saPrizeReqBean);
			serviceBean.setResponse(saPrizeResBean);
		} catch (Exception ex) {
			ex.printStackTrace();
			serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
		}
		return serviceBean;
	}

	@Override
	public ServiceBean<List<SaPrizeDetailResBean>> querySaPrizeDetailInfo(SaPrizeDetailReqBean saPrizeDetailReqBean) {
		ServiceBean<List<SaPrizeDetailResBean>> serviceBean = new ServiceBean<List<SaPrizeDetailResBean>>();
		try {
			List<SaPrizeDetailResBean> saPrizeDetailResBean = saPrizeRepository
					.querySaPrizeDetailInfo(saPrizeDetailReqBean);
			serviceBean.setResponse(saPrizeDetailResBean);
		} catch (Exception ex) {
			ex.printStackTrace();
			serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
		}
		return serviceBean;
	}

	@Override
	public ServiceBean<SaAssessIndexResBean> querySaAssessIndexInfo(SaPrizeDetailReqBean saPrizeDetailReqBean) {
		ServiceBean<SaAssessIndexResBean> serviceBean = new ServiceBean<SaAssessIndexResBean>();

		try {
			if(StringUtil.isNullOrEmpty(saPrizeDetailReqBean.getAccount())){
				serviceBean.setCode(102);
				serviceBean.setDesc("工号不能为空");
				return serviceBean;
			}
			if(StringUtil.isNullOrEmpty(saPrizeDetailReqBean.getMonth())){
				serviceBean.setCode(102);
				serviceBean.setDesc("月份不能为空");
				return serviceBean;
			}
			SaAssessIndexResBean saAssessIndexResBean = saPrizeRepository.querySaAssessIndexInfo(saPrizeDetailReqBean);
			serviceBean.setResponse(saAssessIndexResBean);
		} catch (Exception ex) {
			ex.printStackTrace();
			serviceBean = new ServiceBean(500, ex.getMessage().toString());// 系统异常
		}
		return serviceBean;
	}
}
