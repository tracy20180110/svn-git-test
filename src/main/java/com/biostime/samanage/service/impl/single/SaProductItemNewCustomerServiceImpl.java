package com.biostime.samanage.service.impl.single;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerReqBean;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerResBean;
import com.biostime.samanage.repository.single.SaProductItemNewCustomerRepository;
import com.biostime.samanage.service.single.SaProductItemNewCustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA品项新客报表
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("saProductItemNewCustomerService")
@Slf4j
public class SaProductItemNewCustomerServiceImpl implements SaProductItemNewCustomerService {
    
    @Autowired
    private SaProductItemNewCustomerRepository saProductItemNewCustomerRepository;


    /**
     * SA品项新客报表-查询
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<SaProductItemNewCustomerResBean>> querySaProductItemNewCustomerList(Pager<SaProductItemNewCustomerResBean> pager, SaProductItemNewCustomerReqBean reqBean) {
        ServiceBean<Pager<SaProductItemNewCustomerResBean>> serviceBean = new ServiceBean<Pager<SaProductItemNewCustomerResBean>>();
        try {
            serviceBean.setResponse(saProductItemNewCustomerRepository.querySaProductItemNewCustomerList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * SA品项新客报表-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportSaProductItemNewCustomerList(HttpServletResponse response, SaProductItemNewCustomerReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            saProductItemNewCustomerRepository.exportSaProductItemNewCustomerList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
