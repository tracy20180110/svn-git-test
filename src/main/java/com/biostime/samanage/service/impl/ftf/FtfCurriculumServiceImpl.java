package com.biostime.samanage.service.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumReqBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumResBean;
import com.biostime.samanage.repository.ftf.FtfCurriculumRepository;
import com.biostime.samanage.service.ftf.FtfCurriculumService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF课程Service Impl
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("ftfCurriculumService")
@Slf4j
public class FtfCurriculumServiceImpl implements FtfCurriculumService {

    @Autowired
    private FtfCurriculumRepository ftfCurriculumRepository;


    /**
     * FTF课程列表-查询
     * @param ftfCurriculumReqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<FtfCurriculumResBean>> queryFtfCurriculumList(Pager<FtfCurriculumResBean> pager, FtfCurriculumReqBean ftfCurriculumReqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            Pager<FtfCurriculumResBean> ftfCurriculumResBean = ftfCurriculumRepository.queryFtfCurriculumList(pager,ftfCurriculumReqBean);
            serviceBean.setResponse(ftfCurriculumResBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            serviceBean.setCode(1000);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }


    /**
     * FTF课程列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportFtfCurriculumList(HttpServletResponse response, FtfCurriculumReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            ftfCurriculumRepository.exportFtfCurriculumList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }


    /**
     * FTF课程状态修改
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean changeStatusFtfCurriculum(FtfCurriculumReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            ftfCurriculumRepository.changeStatusFtfCurriculum(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF课程状保存
     *
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean saveFtfCurriculum(FtfCurriculumReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean();
        try {
            ftfCurriculumRepository.saveFtfCurriculum(reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * 查询BNC6品商品
     * @return
     */
    @Override
    public ServiceBean queryBnc6Product() {
        ServiceBean serviceBean = new ServiceBean();
        try {
            serviceBean.setResponse(ftfCurriculumRepository.queryBnc6Product());
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
