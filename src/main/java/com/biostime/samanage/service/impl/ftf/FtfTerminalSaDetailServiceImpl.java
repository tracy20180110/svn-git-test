package com.biostime.samanage.service.impl.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.general.PropUtils;
import com.biostime.samanage.bean.ftf.report.FtfTerminalSaDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfTerminalSaDetailResBean;
import com.biostime.samanage.repository.ftf.FtfTerminalSaDetailRepository;
import com.biostime.samanage.service.ftf.FtfTerminalSaDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动SA、门店信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
@Service("ftfTerminalSaDetailService")
@Slf4j
public class FtfTerminalSaDetailServiceImpl implements FtfTerminalSaDetailService {
    @Autowired
    private FtfTerminalSaDetailRepository ftfTerminalSaDetailRepository;

    /**
     * FTF活动SA、门店信息
     *
     * @param pager
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean<Pager<FtfTerminalSaDetailResBean>> queryFtfTerminalSaDetailList(Pager<FtfTerminalSaDetailResBean> pager, FtfTerminalSaDetailReqBean reqBean) {
        ServiceBean<Pager<FtfTerminalSaDetailResBean>> serviceBean = new ServiceBean<Pager<FtfTerminalSaDetailResBean>>();
        try {
            serviceBean.setResponse(ftfTerminalSaDetailRepository.queryFtfTerminalSaDetailList(pager, reqBean));
        } catch (Exception ex) {
            log.info("msg{}",ex);
            ex.printStackTrace();
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }

    /**
     * FTF活动SA、门店信息-导出
     *
     * @param response
     * @param reqBean
     * @return
     */
    @Override
    public ServiceBean exportFtfTerminalSaDetailList(HttpServletResponse response, FtfTerminalSaDetailReqBean reqBean) {
        ServiceBean serviceBean = new ServiceBean<String>();
        try {
            ftfTerminalSaDetailRepository.exportFtfTerminalSaDetailList(response, reqBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("msg{}",ex);
            serviceBean.setCode(500);
            serviceBean.setDesc(PropUtils.getProp(serviceBean.getCode()));
        }
        return serviceBean;
    }
}
