package com.biostime.samanage.service.saPoint;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.saPoint.SaSalePointsBean;

import javax.servlet.http.HttpServletResponse;

/**
 * SA销售积分
 * Created by zhuhaitao on 2016/12/22.
 */
public interface SaSalePointsService {
    /**
     * SA销售积分报表
     * @param babyConsultant
     * @param areaCode
     * @param officeCode
     * @param startDate
     * @param endDate
     * @param bccCode
     * @param response
     * @return
     */
    ServiceBean exportSaSalePoints(String babyConsultant, String areaCode, String officeCode,String startDate, String endDate, String bccCode,String buCode,HttpServletResponse response);

    /**
     * 导出SA销售积分
     * @param pager
     * @param saSalePointsBean
     * @return
     */
    ServiceBean<Pager<SaSalePointsBean>> searchSaSalePoints(Pager<SaSalePointsBean> pager, SaSalePointsBean saSalePointsBean);
}
