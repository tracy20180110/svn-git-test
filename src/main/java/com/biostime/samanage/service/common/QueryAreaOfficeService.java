package com.biostime.samanage.service.common;

import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.common.AreaOfficeReqBean;
import com.biostime.samanage.bean.common.AreaOfficeResBean;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取用户所属的大区办事处
 * Date: 2016-6-22
 * Time: 9:39
 * User: 12804
 * Version:1.0
 */
public interface QueryAreaOfficeService {

    /**
     * 获取用户所属的大区办事处
     * @param areaOfficeReqBean
     * @return
     */
    public ServiceBean<List<AreaOfficeResBean>> queryUserAreaOffice(AreaOfficeReqBean areaOfficeReqBean) ;

}
