package com.biostime.samanage.service.sign;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.sign.SaSignMerchantReqBean;
import com.biostime.samanage.bean.sign.SaSignReqBean;
import com.biostime.samanage.bean.sign.SaSignResBean;
import com.biostime.samanage.bean.sign.SaSignTerminalResBean;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * Describe:促销员打卡Service
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
public interface SaSignService {


    /**
     * 促销员打卡查询列表
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<SaSignResBean>> querySaSignList(Pager<SaSignResBean> pager, SaSignReqBean reqBean);

    /**
     * 促销员打卡查询列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportSaSignList(HttpServletResponse response, SaSignReqBean reqBean);

    /**
     * 促销员打卡(手机端)
     * @param reqBean
     * @return
     */
    public ServiceBean saveSaSign(SaSignMerchantReqBean reqBean);

    /**
     * 查询是否在指定的门店(手机端)
     * @param reqBean
     * @return
     */
    public ServiceBean<List<SaSignTerminalResBean>> queryIsInTerminal(SaSignMerchantReqBean reqBean);


}
