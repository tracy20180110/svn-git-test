package com.biostime.samanage.service.quota;

import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.quota.DoubleSaQuotaBean;
import com.biostime.samanage.bean.quota.SaQuotaTerminalBean;

public interface SaQuotaService {
    ServiceBean<SaQuotaTerminalBean> getTerminalBySaCode(String saCode);

    ServiceBean<DoubleSaQuotaBean> getDoubleSaQuota(String saCode, String terminalCode, String startTime, String endTime,String chainCode);
}
