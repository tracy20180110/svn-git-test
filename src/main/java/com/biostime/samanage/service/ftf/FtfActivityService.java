package com.biostime.samanage.service.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityReqBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityTemplateResBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoReqBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动Service
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfActivityService {
    /**
     * FTF活动列表
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<FtfActivityResBean>>  queryFtfActivityList(Pager<FtfActivityResBean> pager, FtfActivityReqBean reqBean);

    /**
     * FTF活动列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportFtfActivityList(HttpServletResponse response, FtfActivityReqBean reqBean);

    /**
     * FTF活动状态修改
     * @param reqBean
     * @return
     */
    public ServiceBean changeStatusFtfActivity(FtfActivityReqBean reqBean);

    /**
     * FTF活动保存
     * @param reqBean
     * @return
     */
    public ServiceBean saveFtfActivity(FtfActivityReqBean reqBean);


    /**
     * 获取SA对应的门店
     * @param saCode
     * @return
     */
    public ServiceBean querySaTerminal(String saCode);

    /**
     * 查询门店是否存在
     * @param terminalCode
     * @return
     */
    public ServiceBean queryTerminal(String terminalCode);

    /**
     * 查询导购ID是否存在
     * @param shoppingGuideId
     * @return
     */
    public ServiceBean queryShoppingGuideId(String shoppingGuideId);


    /**
     * FTF海报分享链接更改
     * @param reqBean
     * @return
     */
    public ServiceBean changeImageFtfActivity(FtfActivityReqBean reqBean);

    /**
     * FTF活动模板列表
     * @return
     */
    public ServiceBean<List<FtfActivityTemplateResBean>>  queryFtfActivityTemplateList();


    /**
     * 查询SA线下课堂模板详情
     * @param templateId
     * @param actId
     * @return
     */
    public ServiceBean<Map> querySaFtfActTemplateInfo(String templateId, String actId) ;


    /**
     * 查询讲师列表
     * @return
     */
    public ServiceBean<List<FtfLecturerResBean>>  queryLecturerList(FtfActivityReqBean reqBean);

    /**
     * 更新小程序签到码
     * @param actId
     * @return
     */
    public String changeActMama100QrCode(String actId);

    /**
     * 查询讲师列表app
     * @return
     */
    public ServiceBean<List<FtfLecturerResBean>>  queryAppLecturerList(FtfActivityReqBean reqBean);

    /**
     * 上报中的人员列表app
     * @return
     */
    public ServiceBean<List<AccountInfoResBean>>  queryAppReportAccountInfoList(AccountInfoReqBean reqBean);

}
