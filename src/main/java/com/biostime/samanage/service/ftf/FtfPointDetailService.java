package com.biostime.samanage.service.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.ftf.report.FtfPointDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfPointDetailResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动积分信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfPointDetailService {
    /**
     * FTF活动积分信息
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<FtfPointDetailResBean>> queryFtfPointDetailList(Pager<FtfPointDetailResBean> pager, FtfPointDetailReqBean reqBean);

    /**
     * FTF活动积分信息-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportFtfPointDetailList(HttpServletResponse response, FtfPointDetailReqBean reqBean);

}

