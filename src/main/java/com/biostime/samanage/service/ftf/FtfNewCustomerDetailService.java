package com.biostime.samanage.service.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.ftf.report.FtfNewCustomerDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfNewCustomerDetailResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF新客报名信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfNewCustomerDetailService {
    /**
     * FTF活动新客信息
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<FtfNewCustomerDetailResBean>> queryFtfNewCustomerDetailList(Pager<FtfNewCustomerDetailResBean> pager, FtfNewCustomerDetailReqBean reqBean);

    /**
     * FTF活动新客信息-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportFtfNewCustomerDetailList(HttpServletResponse response, FtfNewCustomerDetailReqBean reqBean);

}

