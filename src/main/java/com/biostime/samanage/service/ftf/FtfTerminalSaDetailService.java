package com.biostime.samanage.service.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.ftf.report.FtfTerminalSaDetailReqBean;
import com.biostime.samanage.bean.ftf.report.FtfTerminalSaDetailResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动SA、门店信息
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfTerminalSaDetailService {
    /**
     * FTF活动SA、门店信息
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<FtfTerminalSaDetailResBean>> queryFtfTerminalSaDetailList(Pager<FtfTerminalSaDetailResBean> pager, FtfTerminalSaDetailReqBean reqBean);

    /**
     * FTF活动SA、门店信息-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportFtfTerminalSaDetailList(HttpServletResponse response, FtfTerminalSaDetailReqBean reqBean);

}

