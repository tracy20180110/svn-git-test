package com.biostime.samanage.service.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumReqBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF课程Service
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfCurriculumService {
    /**
     * FTF课程列表-查询
     * @param ftfCurriculumReqBean
     * @return
     */
    public ServiceBean<Pager<FtfCurriculumResBean>> queryFtfCurriculumList(Pager<FtfCurriculumResBean> pager,FtfCurriculumReqBean ftfCurriculumReqBean) ;

    /**
     * FTF课程列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportFtfCurriculumList(HttpServletResponse response, FtfCurriculumReqBean reqBean);

    /**
     * FTF课程状态修改
     * @param reqBean
     * @return
     */
    public ServiceBean changeStatusFtfCurriculum(FtfCurriculumReqBean reqBean);

    /**
     * FTF课程保存
     * @param reqBean
     * @return
     */
    public ServiceBean saveFtfCurriculum(FtfCurriculumReqBean reqBean);

    /**
     * 查询BNC6品商品
     * @return
     */
    public ServiceBean queryBnc6Product();

}
