package com.biostime.samanage.service.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInAppDetailResBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInDetailReqBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInDetailResBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA. Describe:FTF签到报名信息 Date: 2016-11-23 Time: 11:37
 * User: 12804 Version:1.0
 */
public interface FtfCheckInDetailService {
	/**
	 * FTF活动签到信息
	 * 
	 * @param reqBean
	 * @return
	 */
	public ServiceBean<Pager<FtfCheckInDetailResBean>> queryFtfCheckInDetailList(Pager<FtfCheckInDetailResBean> pager,
			FtfCheckInDetailReqBean reqBean);

	/**
	 * FTF活动签到信息-导出
	 * 
	 * @param response
	 * @param reqBean
	 * @return
	 */
	public ServiceBean exportFtfCheckInDetailList(HttpServletResponse response, FtfCheckInDetailReqBean reqBean);

	/**
	 * FTF活动签到信息App端查询显示
	 * 
	 * @param reqBean
	 * @return
	 */
	public ServiceBean<FtfCheckInAppDetailResBean> queryFtfCheckInAppDetailList(FtfCheckInDetailReqBean reqBean);

	/**
	 * FTF活动签到领取奖品
	 * 
	 * @param ftfCheckInDetailReqBean
	 * @return
	 */
	public ServiceBean<String> receiveFtfCheckInAppPrizes(FtfCheckInDetailReqBean ftfCheckInDetailReqBean);

}
