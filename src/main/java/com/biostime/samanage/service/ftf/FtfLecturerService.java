package com.biostime.samanage.service.ftf;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerReqBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF讲师Service
 * Date: 2016-11-23
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfLecturerService {
    /**
     * FTF讲师列表
     * @param reqBean
     * @return
     */
    public ServiceBean<Pager<FtfLecturerResBean>>  queryFtfLecturerList(Pager<FtfLecturerResBean> pager, FtfLecturerReqBean reqBean);

    /**
     * FTF讲师列表-导出
     * @param response
     * @param reqBean
     * @return
     */
    public ServiceBean exportFtfLecturerList(HttpServletResponse response, FtfLecturerReqBean reqBean);

    /**
     * FTF讲师状态修改
     * @param reqBean
     * @return
     */
    public ServiceBean changeStatusFtfLecturer(FtfLecturerReqBean reqBean);

    /**
     * FTF讲师保存
     * @param reqBean
     * @return
     */
    public ServiceBean saveFtfLecturer(FtfLecturerReqBean reqBean);


    /**
     * 获取讲师名称工号
     * @param keyword
     * @return
     */
    public ServiceBean queryLecturerCode(String keyword,String type);

    /**
     * 讲师认证-导入
     * @param reqBean
     * @return
     */
    public ServiceBean<String> importLecturerCheck(MultipartFile multipartFile, FtfLecturerReqBean reqBean);


}
