package com.biostime.samanage.service.ftf;

import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.ftf.sign.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动签到Service
 * Date: 2016-12-08
 * Time: 11:37
 * User: 12804
 * Version:1.0
 */
public interface FtfActSignService {

    /**
     * FTF活动列表
     * @param ftfActSignReqBean
     * @return
     */
    public ServiceBean<List<FtfActSignResBean>> queryMobFtfActList(FtfActSignReqBean ftfActSignReqBean) ;


    /**
     * FTF活动列表
     * @param actId
     * @return
     */
    public ServiceBean<FtfActSignListBean> querySignInfoList(String actId,String createdBy) ;


    /**
     * FTF活动列表
     * @param ftfActSignInfoReqBean
     * @return
     */
    public ServiceBean<String> saveMobSign(FtfActSignInfoReqBean ftfActSignInfoReqBean) ;


   /**
     * 上传销量
     * @param ftfActSignInfoReqBean
     * @return
     */
    public ServiceBean<String> saveMobSignReportSales(FtfActSignInfoReqBean ftfActSignInfoReqBean) ;

    /**
     * 查询FTF签到活动信息
     * @param reqBean
     * @return
     */
    public ServiceBean<FtfSignMiniAppResBean> querySignFtfActInfo(FtfSignMiniAppReqBean reqBean) ;

    /**
     * 小程序扫码签到
     * @param ftfSignMiniAppReqBean
     * @return
     */
    public ServiceBean saveSignMiniApp(FtfSignMiniAppReqBean ftfSignMiniAppReqBean) ;

    /**
     * feichat贸易销量
     * @return
     */
    public ServiceBean saveActFeiChatSale() ;


    /**
     * 查询登陆人所属的SA门店
     * @param reqBean
     * @return
     */
    public ServiceBean<List<BaseKeyValueBean>> querySaTerminal(BaseKeyValueBean reqBean) ;
}
