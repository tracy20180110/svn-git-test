package com.biostime.samanage.service.splitFlow;

import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.samanage.bean.splitFlow.SaSplitFlowBean;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by zhuhaitao on 2017/3/2.
 */
public interface SaSplitFlowService {
    ServiceBean<Pager<SaSplitFlowBean>> searchSaSplitFlow(Pager<SaSplitFlowBean> pager, SaSplitFlowBean saSplitFlowBean);

    ServiceBean exportSaSplitFlow(String nursingConsultant, String areaCode, String officeCode, String saCode, String customerId, String mobilePhone, String terminalCode, String startTime, String endTime, String terminalChannelCode, String chainChannelCode, HttpServletResponse response);

    ServiceBean addBstOtoCustSA(String saCode, String terminalCode, String customerId, String relateCode, String isNewcust, String maintenanceType, String sendCouponTerminal, String operatorUser);
}
