package com.biostime.samanage.constant;
/**
 * 常量类（主要是放置变动频率不大的信息，改动频率大的建议放属性文件或者数据库，如果访问量大建议不要放数据库）
 * Created by 12360 on 2015/10/14.
 */
public abstract class Constants {

    //系统名称
    public final static String SYSTEM_NAME = "mkt";

    //globalValue数据访问层实例名
    public final static String GLOBAL_VALUE_REPOSITORY = "globalValueRepository";

    //主机邮箱信息
    public final static String MKT_MAIL_HOST ="MKT_MAIL_HOST";

    //短信推送地址
    public final static String SMS_SERVER_MKT = "SMS_SERVER_MKT";

    //jms消息名称（多机负载时名称不能一样：之前版本，不知道消息中心那边是否已经修复了此bug）
    public final static String SMS_SERVER_MKT_JMS = "SMS_SERVER_MKT_JMS";

    //数据源切换枚举值
    public final static String ORACLE_MASTER = "dataSourceOracle";
    public final static String ORACLE_READ_ONLY = "dataSourceOracleReadOnly";
    public final static String MYSQL_MASTER = "dataSourceMySql";
    public final static String MYSQL_READ_ONLY = "dataSourceMySqlReadOnly";

}
