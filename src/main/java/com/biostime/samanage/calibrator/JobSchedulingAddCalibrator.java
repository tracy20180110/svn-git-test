package com.biostime.samanage.calibrator;

import com.biostime.common.calibrator.Calibrator;
import com.biostime.common.calibrator.CalibratorResponse;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.AddSaJobSchedulingBean;
import com.biostime.samanage.bean.SaJobSchedulingBean;
import org.apache.commons.collections.CollectionUtils;
/**
 * 类功能描述: 保存促销员排班校验器
 *
 * @version 1.0
 * @author zhuhaitao
 * @createDate 2016年3月14日 下午4:59:35
 */
public class JobSchedulingAddCalibrator extends Calibrator {

	private static JobSchedulingAddCalibrator instance = new JobSchedulingAddCalibrator();

	public static JobSchedulingAddCalibrator instance() {
		if (instance == null) {
			instance = new JobSchedulingAddCalibrator();
		}
		return instance;
	}

	private JobSchedulingAddCalibrator() {
	}

	@Override
	public CalibratorResponse execute(Object object) {
		AddSaJobSchedulingBean addSaJobSchedulingBean = (AddSaJobSchedulingBean) object;
		CalibratorResponse response = new CalibratorResponse(100, PropUtils.getProp(100));
		if (StringUtil.isNullOrEmpty(addSaJobSchedulingBean.getSalesAccountNo())) {
			return new CalibratorResponse(4000, PropUtils.getProp(4000));// 促销员id不能为空
		}
		if (StringUtil.isNullOrEmpty(addSaJobSchedulingBean.getCompanyDate())) {
			return new CalibratorResponse(4001, PropUtils.getProp(4001));// 工作日期不能为空
		}
		if (CollectionUtils.isNotEmpty(addSaJobSchedulingBean.getSaJobSchedulingBeans())) {
			for (SaJobSchedulingBean saJobSchedulingBean : addSaJobSchedulingBean.getSaJobSchedulingBeans()) {
				if (StringUtil.isNullOrEmpty(saJobSchedulingBean.getSaCode())) {
					return new CalibratorResponse(4002, PropUtils.getProp(4002));// SA编号不能为空
				}
			}
		}
		return response;
	}
}
