package com.biostime.samanage.rpc;

import com.biostime.samanage.util.JsonUtils;
import com.mama100.merchant.base.account.domain.oracle.HytMobDevice;
import com.mama100.order.rpc.OrderRpcJavaFactory;
import com.mama100.order.rpc.bean.*;
import com.mama100.order.rpc.constant.ResponseCode;
import com.mama100.order.rpc.service.OrderSearchClientJavaService;
import com.mama100.order.rpc.thrift.inout.common.SourceSystem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

@Slf4j
@Component
public class OrderRpcCall
{
    
    @Autowired
    private OrderRpcJavaFactory orderRpcJavaFactory;
    
    private static String getSeqNo()
    {
        SimpleDateFormat format = new SimpleDateFormat("yyMMddHHmmssSSS");
        return format.format(new Date());
    }
    
    /**
     * @param osCode
     * @return @author：hujie+ 12755
     * @createtime ： 2015年7月3日 上午11:04:35
     * @description 获取系统来源
     */
    private static SourceSystem getSourceSystem(Integer osCode)
    {
        SourceSystem sourceSystem = SourceSystem.HYT;
        if(osCode!=null&&HytMobDevice.OS_TYPE_CODE_IOS == osCode)
        {
            sourceSystem = SourceSystem.HYT_MOB_IPHONE;
        }
        else if(osCode!=null&&HytMobDevice.OS_TYPE_CODE_ANDROID == osCode)
        {
            sourceSystem = SourceSystem.HYT_MOB_ANDRIOD;
        }
        return sourceSystem;
    }
    
    /**
     * 查询导购员的订单金额
     *
     * @param accountId
     * @param customerIds
     * @param fromDate
     * @param toDate
     * @param assessmentEndDate
     * @return
     */
    public List<OrderGuideAmountBean> queryOrderGuideAmount(String accountId,List<Long> customerIds, Date fromDate, Date toDate, Date assessmentEndDate) {

        List<OrderGuideAmountBean> orgaBeanList = new ArrayList<>();
        try{
            OrderSearchClientJavaService orderSearchClientJavaService = orderRpcJavaFactory.getOrderSearchClientJavaService();
            
            // finagleService
            String seqNo = getSeqNo();
            QueryOrderGuideAmountRequest request = new QueryOrderGuideAmountRequest();
            RequestHeader requestHeader = new RequestHeader();
            requestHeader.setHandler(accountId);
            requestHeader.setSourceSystem(SourceSystem.HYT);
            requestHeader.setSeqNo(seqNo);
            request.setRequestHeader(requestHeader);
            
            //会员手机号码
            request.setCustomerIds(customerIds);
            //开始时间
            request.setFromDate(fromDate);
            //结束时间
            request.setToDate(toDate);
            //考核结束时间，默认toDate
            request.setAssessmentEndDate(assessmentEndDate);

            log.info("rpc queryOrderGuideAmount begin request=" + JsonUtils.toJson(request));
            Future<QueryOrderGuideAmountResponse> future = orderSearchClientJavaService.queryOrderGuideAmount(request);
            QueryOrderGuideAmountResponse response = future.get();
            if(response == null){
                log.info("rpc queryOrderGuideAmount 查询不到订单信息" + JsonUtils.toJson(response));
                return null;
            }
            BaseResponse baseResponse = response.getBaseResp();
            if(baseResponse != null && !ResponseCode.SUCCESS.getCode().equals(baseResponse.getRespCode())){
                log.error("查询订单失败信息"+baseResponse.getRespDesc());
                return null;
            }
            orgaBeanList = response.getOrderGuideAmountBeans();
            return orgaBeanList;
        }
        catch (Exception e){
            e.printStackTrace();
            log.error("查询订单金额异常" + e.getMessage());
        }
        return orgaBeanList;
    }

}
