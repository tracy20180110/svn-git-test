package com.biostime.samanage.controller.sign;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.sign.SaSignReqBean;
import com.biostime.samanage.bean.sign.SaSignResBean;
import com.biostime.samanage.service.sign.SaSignService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA打卡
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/saSign")
public class SaSignController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaSignService saSignService;


    /**
     * SA打卡列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaSignList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA打卡列表",description="促销员打卡列表",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse<Pager<SaSignResBean>> querySalesSignList(@RequestBody final BaseRequest<SaSignReqBean> req) {
        SaSignReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SaSignResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaSignController.querySaSignList.request{}", JSON.toJSONString(req));
            Pager<SaSignResBean> pager = new Pager<SaSignResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SaSignResBean>> result = saSignService.querySaSignList(pager, reqBean);
            logger.info("SaSignController.querySaSignList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA打卡列表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaSignList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA打卡列表导出",description="SA打卡列表导出",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse exportSalesSignList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaSignReqBean reqBean = MapperUtils.reqParameToBean(request, SaSignReqBean.class);

            // 查询
            logger.info("SaSignController.exportSaSignList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saSignService.exportSaSignList(response, reqBean);
            logger.info("SaSignController.exportSaSignList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


}
