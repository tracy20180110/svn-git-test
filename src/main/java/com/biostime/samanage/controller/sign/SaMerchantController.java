package com.biostime.samanage.controller.sign;


import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.sign.SaSignMerchantReqBean;
import com.biostime.samanage.bean.sign.SaSignTerminalResBean;
import com.biostime.samanage.service.sign.SaSignService;
import com.mama100.merchant.base.account.common.bean.mktmp.LoginChannelBean;
import com.mama100.merchant.base.system.common.bean.BgmUserOfficeBean;
import com.mama100.merchant.session.OperatorInfoContext;
import com.mama100.merchant.session.bean.OperatorInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Describe:SA打卡(手机端)
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Slf4j
@Controller
@RequestMapping("/merchant/saSign")
public class SaMerchantController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaSignService saSignService;

    /**
     * 查询是否在指定的门店
     *
     * @return
     */
    @RequestMapping(value = "/queryIsInTerminal.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "SA打卡", description = "SA打卡", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse<List<SaSignTerminalResBean>> queryIsInTerminal(@RequestBody final BaseRequest<SaSignMerchantReqBean> reqBean) {

        BaseResponse<List<SaSignTerminalResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            logger.info("SaMerchantController.queryIsInTerminal.request{}", JSON.toJSONString(reqBean));
            System.out.println("SaMerchantController.queryIsInTerminal.request{}" + JSON.toJSONString(reqBean));
            ServiceBean<List<SaSignTerminalResBean>> result = saSignService.queryIsInTerminal(reqBean.getRequest());
            System.out.println("SaMerchantController.queryIsInTerminal.response{}" + JSON.toJSONString(result));
            logger.info("SaMerchantController.queryIsInTerminal.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            resp.setResponse(result.getResponse());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * SA打卡(手机端)
     *
     * @return
     */
    @RequestMapping(value = "/saveSaSign.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "SA打卡", description = "促销员打卡", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse saveSaSign(@RequestBody final BaseRequest<SaSignMerchantReqBean> reqBean) {

        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaSignMerchantReqBean saSignMerchantReqBean = reqBean.getRequest();
            if (StringUtil.isNullOrEmpty(saSignMerchantReqBean.getDepartmentCode())) {
                OperatorInfo operatorInfo = OperatorInfoContext.get();
                BgmUserOfficeBean userOfficeBean = operatorInfo.getUserOffice();
                if (null != userOfficeBean) {
                    saSignMerchantReqBean.setDepartmentCode(userOfficeBean.getOfficeCode());
                }
                List<LoginChannelBean> lcBean = operatorInfo.getChannels();
                if (null != lcBean) {
                    if (lcBean.size() > 0) {
                        saSignMerchantReqBean.setChannelCode(lcBean.get(0).getChannelCode());
                    }
                }
            }
            if (null != saSignMerchantReqBean && saSignMerchantReqBean.getTerminalNum() == null) {
                resp.setCode(3000);
                resp.setDesc(PropUtils.getProp(3000));
                return resp;
            }
            logger.info("SaMerchantController.saveSalesSign.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saSignService.saveSaSign(reqBean.getRequest());
            logger.info("SaMerchantController.saveSalesSign.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
