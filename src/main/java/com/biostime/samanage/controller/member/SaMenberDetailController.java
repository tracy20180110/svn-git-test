package com.biostime.samanage.controller.member;

import com.alibaba.fastjson.JSON;
import com.biostime.cas.bean.UserInfoResult;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.common.util.general.PropUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.common.SessionUser;
import com.biostime.samanage.bean.member.ExgPlmOutImportResult;
import com.biostime.samanage.bean.member.MemberDetailBean;
import com.biostime.samanage.bean.member.MktImportInfoBean;
import com.biostime.samanage.bean.member.NewMemberBean;
import com.biostime.samanage.domain.MktImportInfo;
import com.biostime.samanage.service.member.SaMenberDetailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * SA会员明细
 */

@Controller
@RequestMapping("/menberDetail")
@Slf4j
public class SaMenberDetailController {

    @Autowired
    private SaMenberDetailService saMenberDetailService;

    /**
     * SA会员明细报表
     *
     * @param req
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/searchSaMenberDetail.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "searchSaMenberDetail", description = "SA会员明细报表", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<Pager<MemberDetailBean>> searchSaMenberDetail(
            @RequestBody final BaseRequest<MemberDetailBean> req, HttpServletRequest request,
            HttpServletResponse response) {
        BaseResponse<Pager<MemberDetailBean>> resp = new BaseResponse<Pager<MemberDetailBean>>(
                DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("searchSaMenberDetail:request parameters {}", JSON.toJSON(req));
            MemberDetailBean memberDetailBean = req.getRequest();
            Pager<MemberDetailBean> pager = new Pager<MemberDetailBean>(memberDetailBean.getPageSize(), memberDetailBean.getPageNo());
            ServiceBean<Pager<MemberDetailBean>> result = saMenberDetailService.searchSaMenberDetail(pager, memberDetailBean);
            log.info("searchSaMenberDetail:response parameters:{}", JSON.toJSON(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 导出SA会员明细
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param proBcc
     * @param srcLocName
     * @param startTime
     * @param endTime
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/exportSaMenberDetail.action", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "exportSaMenberDetail", description = "导出SA会员明细", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<String> exportSaMenberDetail(@RequestParam final String nursingConsultant, @RequestParam final String areaCode, @RequestParam final String officeCode, @RequestParam final String saCode, @RequestParam final String customerId, @RequestParam final String mobile, @RequestParam final String proBcc, @RequestParam final String srcLocName, @RequestParam final String startTime, @RequestParam final String endTime, HttpServletRequest request,
                                              HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse<String>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("exportSaMenberDetail:request parameters{}", JSON.toJSON("nursingConsultant:" + nursingConsultant));

            ServiceBean result = saMenberDetailService.exportSaMenberDetail(nursingConsultant, areaCode, officeCode, saCode, customerId, mobile, proBcc, srcLocName, startTime, endTime, response);

            log.info("exportSaMenberDetail:response parameters {}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 方法描述: 导入记录查询列表
     *
     * @param req
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author zhuhaitao
     * @createDate 2016年3月23日 上午9:21:44
     */
    @RequestMapping(value = "/searchMktImportInfo.action", method = {RequestMethod.POST})
    public
    @ResponseBody
    BaseResponse<Pager<MktImportInfoBean>> searchMktImportInfo(
            @RequestBody final BaseRequest<MktImportInfoBean> req, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        BaseResponse<Pager<MktImportInfoBean>> resp = new BaseResponse<Pager<MktImportInfoBean>>(
                DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("searchMktImportInfo:request parameters{}:{}", JSON.toJSON(req));
            MktImportInfoBean importInfoBean = req.getRequest();
            Pager<MktImportInfoBean> pager = new Pager<MktImportInfoBean>(importInfoBean.getPageSize(), importInfoBean.getPageNo());

            ServiceBean<Pager<MktImportInfoBean>> result = saMenberDetailService.searchMktImportInfo(pager, importInfoBean);

            log.info("searchMktImportInfo:response parameters:{}", JSON.toJSON(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 方法描述: 获取模板
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author zhuhaitao
     * @createDate 2016年3月31日 上午11:54:16
     */
    @RequestMapping(value = "/downloadTemplate.action", method = {RequestMethod.GET})
    public
    @ResponseBody
    BaseResponse<String> downloadTemplate(HttpServletRequest request,
                                          HttpServletResponse response) throws Exception {
        BaseResponse<String> resp = new BaseResponse<String>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            String url = request.getSession().getServletContext().getRealPath("/") + "/html5/uploads/template/sa_new_member_template.xlsx";
            File srcFile = new File(url);

            // 判断源文件是否存在
            if (!srcFile.exists()) {
                resp.setCode(2001);
                resp.setDesc(PropUtils.getProp(2001));
                return resp;
            } else {
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(new String("数据补录示例模板" + ".xlsx"), "UTF-8"));
                // 复制文件
                int byteread = 0; // 读取的字节数
                InputStream in = null;
                OutputStream outs = response.getOutputStream();
                in = new FileInputStream(srcFile);
                byte[] buffer = new byte[1024];

                while ((byteread = in.read(buffer)) != -1) {
                    outs.write(buffer, 0, byteread);
                }
                in.close();
                outs.close();
                resp.setCode(100);
                resp.setDesc("成功");
                ExceptionUtils.setError(resp);
            }
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 方法描述: 根据url从服务器下载文件
     *
     * @param filePath
     * @param fileName
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author zhuhaitao
     * @createDate 2016年3月30日 上午11:44:52
     */
    @RequestMapping(value = "/exportExcel.action", method = {RequestMethod.GET})
    public
    @ResponseBody
    BaseResponse<String> exportExcel(@RequestParam final String filePath, @RequestParam final String fileName, HttpServletRequest request,
                                     HttpServletResponse response) throws Exception {
        BaseResponse<String> resp = new BaseResponse<String>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("exportExcel:request parameters{}", JSON.toJSON("filePath:" + filePath + ";fileName:" + fileName));

            File srcFile = new File(filePath);
            // 判断源文件是否存在
            if (!srcFile.exists()) {
                resp.setCode(2002);
                resp.setDesc(PropUtils.getProp(2002));
                return resp;
            } else {
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(new String(fileName + ".xlsx"), "UTF-8"));
                // 复制文件
                int byteread = 0; // 读取的字节数
                InputStream in = null;
                OutputStream outs = response.getOutputStream();
                in = new FileInputStream(srcFile);
                byte[] buffer = new byte[1024];

                while ((byteread = in.read(buffer)) != -1) {
                    outs.write(buffer, 0, byteread);
                }
                in.close();
                outs.close();
                resp.setCode(100);
                resp.setDesc("成功");
                ExceptionUtils.setError(resp);
            }

        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 方法描述: 导入SA新会员
     *
     * @param multipartFile
     * @return
     * @throws Exception
     * @author zhuhaitao
     * @createDate 2017年2月21日14:04:10
     */
    @RequestMapping(value = "/importNewMember.action", method = {RequestMethod.POST})
    public
    @ResponseBody
    BaseResponse<ExgPlmOutImportResult> importNewMember(
            @RequestParam(value = "excel", required = false) MultipartFile multipartFile, HttpServletRequest request,
            HttpServletResponse resp) throws Exception {
        BaseResponse<ExgPlmOutImportResult> response = new BaseResponse<ExgPlmOutImportResult>(
                DateUtils.makeDateSeqNo());

        String userId = (String) request.getSession().getAttribute(SessionUser.SESSION_USERID);
        UserInfoResult userInfoResult = (UserInfoResult) request.getSession().getAttribute(SessionUser.SESSTION_USER);
        String userName = userInfoResult.getUserName();


        String filename = multipartFile.getOriginalFilename();
        if (StringUtil.isNullOrEmpty(filename)) {
            response.setCode(2006);
            response.setDesc(PropUtils.getProp(response.getCode()));
            return response;
        }
        String suffix = filename.substring(filename.lastIndexOf(".") + 1); // 后缀
        if (!suffix.equals("xlsx") && !suffix.equals("xls")) {
            response.setCode(2004);
            response.setDesc(PropUtils.getProp(response.getCode()));
            return response;
        }
        if (suffix.equals("xls")) {
            response.setCode(2005);
            response.setDesc(PropUtils.getProp(response.getCode()));
            return response;
        }
        try {

            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

            String dateFileName = format1.format(date);

            String fileNameNow = "SA会员数" + dateFileName + ".xlsx";

            InputStream input = multipartFile.getInputStream();
            XSSFWorkbook workBook = new XSSFWorkbook(input);
            XSSFSheet sheet = workBook.getSheetAt(0);

            if (null == sheet) {
                response.setCode(2003);
                response.setDesc(PropUtils.getProp(response.getCode()));
                return response;
            } else {

                List<NewMemberBean> newMemberBeanList = new ArrayList<NewMemberBean>();
                ExgPlmOutImportResult result = new ExgPlmOutImportResult();
                List<String> descriptions = new ArrayList<String>();
                result.setDescriptions(descriptions);

                int total = 0;
                int failed = 0;

                String type = "sa_new_member";

                for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                    XSSFRow row = sheet.getRow(i);
                    /**
                     * 校验数据+新增充值审核记录
                     */
                    ServiceBean<Map<String, Object>> failedServiceBean = saMenberDetailService.importNewMember(row, failed, descriptions, result, userId);
                    Map<String, Object> map = failedServiceBean.getResponse();
                    if (MapUtils.isNotEmpty(map)) {
                        failed = Integer.parseInt(map.get("failed").toString());
                        NewMemberBean bean = (NewMemberBean) map.get("bean");
                        if (null != bean) {
                            newMemberBeanList.add(bean);
                        }
                    }
                    if (failedServiceBean.getCode() == 4020) {
                        response.setCode(4020);
                        response.setDesc(PropUtils.getProp(response.getCode()));
                        return response;
                    }

                    total++;
                }
                boolean exists = false;
                if (CollectionUtils.isNotEmpty(newMemberBeanList)) {
                    for (int i = 0; i < newMemberBeanList.size() - 1; i++) {
                        String phone = newMemberBeanList.get(i).getMobilePhone();
                        for (int j = i + 1; j < newMemberBeanList.size(); j++) {
                            if (phone.equals(newMemberBeanList.get(j).getMobilePhone())) {
                                descriptions.add("【行数】第" + newMemberBeanList.get(i).getRowNun() + "行与第" + newMemberBeanList.get(j).getRowNun() + "行；【失败原因】" + phone + "手机号码重复");
                                exists = true;
                            }
                        }
                    }
                }
                if (exists) {
                    failed++;
                }
                int succeedCount = total - failed;
                int failedCount = failed;

                result.setFailedCount(failedCount);
                result.setTotalCount(total);
                result.setSucceedCount(succeedCount);

                //成功数不等于总数，不保存文件
                if (total != 0 && total == succeedCount) {

                    File file = new File("/opt/resources/img/samanage/upload/excel");
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    // 文件保存路径
                    String filePath = "/opt/resources/img/samanage/upload/excel/" + fileNameNow;
                    // 转存文件
                    multipartFile.transferTo(new File(filePath));

                    /**
                     * 添加导入文件记录
                     */
                    ServiceBean<MktImportInfo> serviceBean = saMenberDetailService.saveMktImportInfoBean(filePath, fileNameNow, userId, date, total, type, userName);
                    if (serviceBean.getCode() != 100) {
                        response.setCode(serviceBean.getCode());
                        response.setDesc(serviceBean.getDesc());
                        return response;
                    }
                    if (CollectionUtils.isNotEmpty(newMemberBeanList)) {
                        ServiceBean<String> addResult = saMenberDetailService.addNewMember(newMemberBeanList);
                        if (addResult.getCode() == 100) {
                            response.setCode(addResult.getCode());
                            response.setDesc("成功导入" + total + "个新会员");
                        } else {
                            response.setCode(addResult.getCode());
                            response.setDesc(addResult.getDesc());
                        }
                    }
                } else {
                    response.setResponse(result);
                    response.setCode(500);
                    response.setDesc("导入失败");
                }
            }
            log.info("importNewMember:response parameters {}", JSON.toJSON(response));
        } catch (Exception e) {
            ExceptionUtils.setSystemError(response, e.getMessage());
            log.error(e.getMessage(), e);
        }
        return response;
    }

}
