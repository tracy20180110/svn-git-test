package com.biostime.samanage.controller.quota;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.quota.DoubleSaQuotaBean;
import com.biostime.samanage.bean.quota.SaQuotaTerminalBean;
import com.biostime.samanage.service.quota.SaQuotaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SA指标
 */

@Controller
@RequestMapping("/saQuota")
@Slf4j
public class SaQuotaController {
    @Autowired
    private SaQuotaService saQuotaService;

    /**
     * 根据sa编号查询门店信息
     *
     * @param saCode
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/getTerminalBySaCode.do", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "getTerminalBySaCode", description = "根据sa编号查询门店信息", author = "w1014", model = ModelType.MKT, updateTime = "2018-3-23 09:43:56")
    public
    @ResponseBody
    BaseResponse<SaQuotaTerminalBean> getTerminalBySaCode(@RequestParam final String saCode, HttpServletRequest request,
                                                          HttpServletResponse response) {
        BaseResponse<SaQuotaTerminalBean> resp = new BaseResponse<SaQuotaTerminalBean>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("getTerminalBySaCode:request parameters{}", JSON.toJSON("saCode:" + saCode));

            ServiceBean<SaQuotaTerminalBean> result = saQuotaService.getTerminalBySaCode(saCode);

            log.info("getTerminalBySaCode:response parameters {}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            resp.setResponse(result.getResponse());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 根据sa对应的门店信息查询详细指标
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/getDoubleSaQuota.do", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "getDoubleSaQuota", description = "根据sa对应的门店信息查询详细指标", author = "w1014", model = ModelType.MKT, updateTime = "2018-3-23 09:43:49")
    public
    @ResponseBody
    BaseResponse<DoubleSaQuotaBean> getDoubleSaQuota(@RequestParam final String saCode, @RequestParam final String terminalCode, @RequestParam final String startTime, @RequestParam final String endTime, @RequestParam final String chainCode, HttpServletRequest request,
                                                     HttpServletResponse response) {
        BaseResponse<DoubleSaQuotaBean> resp = new BaseResponse<DoubleSaQuotaBean>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("getDoubleSaQuota:request parameters{}", JSON.toJSON("terminalCode:" + terminalCode + ",saCode:" + saCode + ",startTime:" + startTime + ",endTime:" + endTime+ ",chainCode:" + chainCode));

            ServiceBean<DoubleSaQuotaBean> result = saQuotaService.getDoubleSaQuota(saCode, terminalCode, startTime, endTime, chainCode);

            log.info("getDoubleSaQuota:response parameters {}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            resp.setResponse(result.getResponse());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

}
