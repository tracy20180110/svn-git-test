package com.biostime.samanage.controller;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.*;
import com.biostime.samanage.check.JobSchedulingCheck;
import com.biostime.samanage.service.JobSchedulingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 类功能描述: SASA育婴顾问工作排班
 *
 * @author zhuhaitao
 * @version 1.0
 * @createDate 2016年6月23日 上午10:24:32
 */

@Controller
@RequestMapping("/jobScheduling")
@Slf4j
public class JobSchedulingController {

    @Autowired
    private JobSchedulingService jobSchedulingService;

    /**
     * SA育婴顾问工作排班报表
     *
     * @param req
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/searchJobScheduling.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "searchJobScheduling", description = "SA育婴顾问工作排班报表", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<Pager<SaJobSchedulingBean>> searchJobScheduling(
            @RequestBody final BaseRequest<PageSaJobScheduling> req, HttpServletRequest request,
            HttpServletResponse response) {
        BaseResponse<Pager<SaJobSchedulingBean>> resp = new BaseResponse<Pager<SaJobSchedulingBean>>(
                DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("searchJobScheduling:request parameters {}", JSON.toJSON(req));
            PageSaJobScheduling pageSaJobScheduling = req.getRequest();
            Pager<SaJobSchedulingBean> pager = new Pager<SaJobSchedulingBean>(
                    pageSaJobScheduling.getPageSize(), pageSaJobScheduling.getPageNo());
            ServiceBean<Pager<SaJobSchedulingBean>> result = jobSchedulingService.searchJobScheduling(pager, pageSaJobScheduling.getSaJobSchedulingBean());
            log.info("searchJobScheduling:response parameters:{}", JSON.toJSON(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 导出SA育婴顾问工作排班报表
     * @param salesAccountNo
     * @param areaCode
     * @param officeCode
     * @param startMonth
     * @param endMonth
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/exportJobScheduling.action", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "exportJobScheduling", description = "SA育婴顾问工作排班报表", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<String> exportJobScheduling(@RequestParam final String salesAccountNo, @RequestParam final String areaCode, @RequestParam final String officeCode, @RequestParam final String startMonth, @RequestParam final String endMonth,@RequestParam final String buCode, HttpServletRequest request,
                                             HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse<String>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("exportJobScheduling:request parameters{}", JSON.toJSON("salesAccountNo:" + salesAccountNo));

            ServiceBean result = jobSchedulingService.exportJobScheduling(salesAccountNo, areaCode, officeCode, startMonth, endMonth,buCode, response);

            log.info("exportJobScheduling:response parameters {}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 方法描述: 根据办事处编号查询SA育婴顾问排班列表
     *
     * @param salesAccountId
     * @param companyDate
     * @param request
     * @param response
     * @return
     * @author zhuhaitao
     * @createDate 2016年6月23日 下午12:30:09
     */
    @RequestMapping(value = "/queryJobScheduling.do", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "queryJobScheduling", description = "根据SA育婴顾问编号和日期，查询当天排班", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<List<SaJobSchedulingSalesAccountBean>> queryJobScheduling(
            @RequestParam final String officeCode, @RequestParam final String month,@RequestParam final String buCode, HttpServletRequest request,
            HttpServletResponse response) {
        BaseResponse<List<SaJobSchedulingSalesAccountBean>> resp = new BaseResponse<List<SaJobSchedulingSalesAccountBean>>(
                DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            log.info("queryJobScheduling:request parameters {}", JSON.toJSON("officeCode:" + officeCode) + ",month:"
                    + month + ",buCode:"+ buCode);
            ServiceBean<List<SaJobSchedulingSalesAccountBean>> result = jobSchedulingService.queryJobScheduling(
                    officeCode, month,buCode);
            log.info("queryJobScheduling:response parameters:{}", JSON.toJSON(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 方法描述: 根据SA育婴顾问编号和日期，查询当天排班
     *
     * @param salesAccountId
     * @param companyDate
     * @param request
     * @param response
     * @return
     * @author zhuhaitao
     * @createDate 2016年6月27日 上午11:05:46
     */
    @RequestMapping(value = "/getJobScheduling.do", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "getJobScheduling", description = "根据SA育婴顾问编号和日期，查询当天排班", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<List<SaJobSchedulingBean>> getJobScheduling(
            @RequestParam final String salesAccountNo, @RequestParam final String companyDate,
            HttpServletRequest request, HttpServletResponse response) {
        BaseResponse<List<SaJobSchedulingBean>> resp = new BaseResponse<List<SaJobSchedulingBean>>(
                DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("getJobScheduling:request parameters {}", JSON.toJSON("salesAccountNo:" + salesAccountNo)
                    + ",companyDate:" + companyDate);
            ServiceBean<List<SaJobSchedulingBean>> result = jobSchedulingService.getJobScheduling(salesAccountNo,
                    companyDate);
            log.info("getJobScheduling:response parameters:{}", JSON.toJSON(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 方法描述: 根据SA育婴顾问编号查询当月排班状态信息
     *
     * @param salesAccountNo
     * @param companyDate
     * @param request
     * @param response
     * @return
     * @author zhuhaitao
     * @createDate 2016年6月27日 下午2:08:36
     */
    @RequestMapping(value = "/getJobSchedulingStatus.do", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "getJobSchedulingStatus", description = "根据SA育婴顾问编号查询当月排班状态信息", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<List<SaJobSchedulingStatusBean>> getJobSchedulingStatus(
            @RequestParam final String salesAccountNo, @RequestParam final String month, HttpServletRequest request,
            HttpServletResponse response) {
        BaseResponse<List<SaJobSchedulingStatusBean>> resp = new BaseResponse<List<SaJobSchedulingStatusBean>>(
                DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("getJobSchedulingStatus:request parameters {}", JSON.toJSON("salesAccountNo:" + salesAccountNo)
                    + ",month:" + month);
            ServiceBean<List<SaJobSchedulingStatusBean>> result = jobSchedulingService.getJobSchedulingStatus(
                    salesAccountNo, month);
            log.info("getJobSchedulingStatus:response parameters:{}", JSON.toJSON(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 方法描述: 保存SA育婴顾问排班
     *
     * @param req
     * @param request
     * @param response
     * @return
     * @author zhuhaitao
     * @createDate 2016年6月23日 下午1:46:11
     */
    @RequestMapping(value = "/saveJobScheduling.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "saveJobScheduling", description = "保存SA育婴顾问排班", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<String> saveJobScheduling(
            @RequestBody final BaseRequest<AddSaJobSchedulingBean> req, HttpServletRequest request,
            HttpServletResponse response) {
        BaseResponse<String> resp = JobSchedulingCheck.saveJobScheduling(req, request);
        if (!resp.isOk()) {
            return resp;
        }
        try {

            AddSaJobSchedulingBean addSaJobSchedulingBean = req.getRequest();
            log.info("saveJobScheduling:request parameters {}", JSON.toJSON(req));
            ServiceBean<String> result = jobSchedulingService.saveJobScheduling(addSaJobSchedulingBean);
            log.info("saveJobScheduling:response parameters:{}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 方法描述: 保存SA育婴顾问排班状态信息
     *
     * @param req
     * @param request
     * @param response
     * @return
     * @author zhuhaitao
     * @createDate 2016年6月27日 下午2:17:36
     */
    @RequestMapping(value = "/saveJobSchedulingStatus.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "saveJobSchedulingStatus", description = "保存SA育婴顾问排班状态信息", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<String> saveJobSchedulingStatus(
            @RequestBody final BaseRequest<SaJobSchedulingStatusBean> req, HttpServletRequest request,
            HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse<String>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            SaJobSchedulingStatusBean saJobSchedulingStatusBean = req.getRequest();
            log.info("saveJobSchedulingStatus:request parameters {}", JSON.toJSON(req));
            ServiceBean<String> result = jobSchedulingService.saveJobSchedulingStatus(saJobSchedulingStatusBean);
            log.info("saveJobSchedulingStatus:response parameters:{}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 方法描述: 根据SA编号获取SA名称
     *
     * @param saCode
     * @param request
     * @param response
     * @return
     * @author zhuhaitao
     * @createDate 2016年6月27日 上午11:02:28
     */
    @RequestMapping(value = "/getSaNameBySaCode.do", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "getSaNameBySaCode", description = "根据SA编号获取SA名称", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<SaTerminalBean> getSaNameBySaCode(@RequestParam final String saCode,
                                                   HttpServletRequest request, HttpServletResponse response) {
        BaseResponse<SaTerminalBean> resp = new BaseResponse<SaTerminalBean>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            log.info("getSaNameBySaCode:request parameters {}", JSON.toJSON(saCode));
            ServiceBean<SaTerminalBean> result = jobSchedulingService.getSaNameBySaCode(saCode);
            log.info("getSaNameBySaCode:response parameters:{}", JSON.toJSON(result));

            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
