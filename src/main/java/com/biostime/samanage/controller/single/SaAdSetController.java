package com.biostime.samanage.controller.single;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.single.adset.SaAdSetReqBean;
import com.biostime.samanage.bean.single.adset.SaAdSetResBean;
import com.biostime.samanage.service.single.SaAdSetService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA广告设置
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/sa/adset")
public class SaAdSetController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaAdSetService saAdSetService;


    /**
     * SA广告列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaAdSetList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA广告设置列表",description="SA广告设置列表",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse<Pager<SaAdSetResBean>> querySaAdSetList(@RequestBody final BaseRequest<SaAdSetReqBean> req) {
        SaAdSetReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SaAdSetResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaAdSetController.querySaAdSetList.request{}", JSON.toJSONString(req));
            Pager<SaAdSetResBean> pager = new Pager<SaAdSetResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SaAdSetResBean>> result = saAdSetService.querySaAdSetList(pager, reqBean);
            logger.info("SaAdSetController.querySaAdSetList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA广告列表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaAdSetList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA广告列表导出",description="SA广告列表导出",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse exportSaAdSetList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaAdSetReqBean reqBean = MapperUtils.reqParameToBean(request, SaAdSetReqBean.class);

            // 查询
            logger.info("SaAdSetController.exportSaAdSetList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saAdSetService.exportSaAdSetList(response, reqBean);
            logger.info("SaAdSetController.exportSaAdSetList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 保存SA广告图
     *
     * @return
     */
    @RequestMapping(value = "/saveSaAdset.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "保存SA广告图", description = "保存SA广告图", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse saveSaAdSet(@RequestBody final BaseRequest<SaAdSetReqBean> reqBean) {

        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 保存
            logger.info("SaAdSetMerchantController.saveSaAdSet.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saAdSetService.saveSaAdSet(reqBean.getRequest());
            logger.info("SaAdSetMerchantController.saveSaAdSet.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA广告设置状态更改
     * @param req
     * @return
     */
    @RequestMapping(value = "/changeStatusSaAdSet.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="SA广告设置状态更改(POST请求)",description="SA广告设置状态更改",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse changeStatusActRoadShow(@RequestBody final BaseRequest<SaAdSetReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("SaAdSetMerchantController.changeStatusSaAdSet.request{}", JSON.toJSONString(req));
            ServiceBean result = saAdSetService.changeStatusSaAdSet(req.getRequest());
            logger.info("SaAdSetMerchantController.changeStatusSaAdSet.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA广告设置验证指定门店
     * @param req
     * @return
     */
    @RequestMapping(value = "/checkTerminalInfo.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="SA广告设置验证指定门店(POST请求)",description="SA广告设置验证指定门店",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse checkTerminalInfo(@RequestBody final BaseRequest<SaAdSetReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("SaAdSetMerchantController.checkTerminalInfo.request{}", JSON.toJSONString(req));
            ServiceBean result = saAdSetService.checkTerminalInfo(req.getRequest());
            logger.info("SaAdSetMerchantController.checkTerminalInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
