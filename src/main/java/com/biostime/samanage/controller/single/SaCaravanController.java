package com.biostime.samanage.controller.single;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.caravan.*;
import com.biostime.samanage.service.single.CaravanService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:大小篷车申请
 * Date: 2017-12-15
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */

@Controller
@RequestMapping("/sa/caravan")
public class SaCaravanController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CaravanService caravanService;

    /**
     * 查询分销物料信息
     * @return
     */
    @RequestMapping(value = "/queryCaravanMaterielList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="查询分销物料信息",description="查询分销物料信息",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse<List<CaravanCmaterialInfo>> queryCaravanMaterielList(@RequestBody final BaseRequest<CaravanReqBean> reqBean) {
        BaseResponse<List<CaravanCmaterialInfo>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            ServiceBean<List<CaravanCmaterialInfo>> result = caravanService.queryCaravanMaterielList(reqBean.getRequest());
            logger.info("CaravanController.queryCaravanMaterielList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 大小篷车列表查询
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaCaravanList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="大小篷车列表查询",description="大小篷车列表查询",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse<Pager<CaravanListResBean>> querySaCaravanList(@RequestBody final BaseRequest<CaravanListReqBean> req) {
        CaravanListReqBean reqBean = req.getRequest();
        BaseResponse<Pager<CaravanListResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("CaravanController.querySaCaravanList.request{}", JSON.toJSONString(req));
            Pager<CaravanListResBean> pager = new Pager<CaravanListResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<CaravanListResBean>> result = caravanService.querySaCaravanList(pager, reqBean);
            logger.info("CaravanController.querySaCaravanList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 大小篷车列表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaCaravanList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="大小篷车列表导出",description="大小篷车列表导出",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse exportSaCaravanList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            CaravanListReqBean reqBean = MapperUtils.reqParameToBean(request, CaravanListReqBean.class);

            // 查询
            logger.info("CaravanController.exportSaCaravanList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = caravanService.exportSaCaravanList(response, reqBean);
            logger.info("CaravanController.exportSaCaravanList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 大小篷车申请
     * @return
     */
    @RequestMapping(value = "/saveCaravan.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="大小篷车申请",description="大小篷车申请",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse saveCaravan(@RequestBody final BaseRequest<CaravanReqBean> req,HttpServletRequest request) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("CaravanController.saveCaravan.request{}", JSON.toJSONString(req));
            ServiceBean result = caravanService.saveCaravan(req.getRequest(),request);
            logger.info("CaravanController.saveCaravan.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 大小篷车OA流程审批
     * @return
     */
    @RequestMapping(value = "/caravanOaAudit.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "大小篷车OA流程审批", description = "大小篷车OA流程审批", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse caravanOaAudit(@RequestBody final BaseRequest<CaravanReqBean> req,HttpServletRequest request) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaCaravanExternalController.editCaravanOaAudit.request{}", JSON.toJSONString(req));
            ServiceBean result = caravanService.caravanOaAudit(req.getRequest(),request);
            logger.info("SaCaravanExternalController.editCaravanOaAudit.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 审批通过，重新推送获取订单
     * @return
     */
    @RequestMapping(value = "/caravanOrder.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="重新推送获取订单",description="重新推送获取订单",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse caravanOrder(@RequestBody final BaseRequest<CaravanReqBean> req,HttpServletRequest request) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("CaravanController.caravanOrder.request{}", JSON.toJSONString(req));
            ServiceBean result = caravanService.caravanOrder(req.getRequest(), request);
            logger.info("CaravanController.caravanOrder.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 大小篷车上传竣工图片及审核
     * @return
     */
    @RequestMapping(value = "/saveCaravanImgAudit.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="大小篷车申请",description="大小篷车申请",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse saveCaravanImgAudit(@RequestBody final BaseRequest<CaravanImgAuditReqBean> req,HttpServletRequest request) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("CaravanController.saveCaravanImgAudit.request{}", JSON.toJSONString(req));
            ServiceBean result = caravanService.saveCaravanImgAudit(req.getRequest(), request);
            logger.info("CaravanController.saveCaravanImgAudit.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 大小篷车状态更新
     * @return
     */
    @RequestMapping(value = "/editCaravanStatus.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="大小篷车状态更新",description="大小篷车状态更新",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse editCaravanStatus(@RequestBody final BaseRequest<CaravanReqBean> req,HttpServletRequest request) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("CaravanController.editCaravanStatus.request{}", JSON.toJSONString(req));
            ServiceBean result = caravanService.editCaravanStatus(req.getRequest(), request);
            logger.info("CaravanController.editCaravanStatus.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询SA门店
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaTerminalInfo.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="查询SA门店",description="查询SA门店",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse querySaTerminalInfo(@RequestBody final BaseRequest<CaravanReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("CaravanController.querySaTerminalInfo.request{}", JSON.toJSONString(req));
            ServiceBean result = caravanService.querySaTerminalInfo(req.getRequest());
            logger.info("CaravanController.querySaTerminalInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询大小篷车信息
     * @return
     */
    @RequestMapping(value = "/queryCaravanInfo.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="查询大小篷车信息",description="查询大小篷车信息",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse<CaravanReqBean> queryCaravanInfo(@RequestBody final BaseRequest<CaravanReqBean> reqBean) {
        BaseResponse<CaravanReqBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            ServiceBean<CaravanReqBean> result = caravanService.queryCaravanInfo(reqBean.getRequest());
            logger.info("CaravanController.queryCaravanInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }



    /**
     * 查询可更新的人员列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryUpdateByList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="查询可更新的人员列表",description="查询可更新的人员列表",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse<List<String>> queryUpdateByList(@RequestBody final BaseRequest<CaravanListReqBean> req) {
        CaravanListReqBean reqBean = req.getRequest();
        BaseResponse<List<String>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("CaravanController.queryUpdateByList.request{}", JSON.toJSONString(req));
            ServiceBean<List<String>> result = caravanService.queryUpdateByList(reqBean);
            logger.info("CaravanController.queryUpdateByList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 更新申请人
     * @param req
     * @return
     */
    @RequestMapping(value = "/editUpdateBy.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="更新申请人",description="更新申请人",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse editUpdateBy(@RequestBody final BaseRequest<CaravanReqBean> req) {
        BaseResponse<List<String>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("CaravanController.editUpdateBy.request{}", JSON.toJSONString(req));
            ServiceBean<List<String>> result = caravanService.editUpdateBy(req.getRequest());
            logger.info("CaravanController.editUpdateBy.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询大小篷车操作日志
     * @return
     */
    @RequestMapping(value = "/queryCaravanLogList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="查询大小篷车操作日志",description="查询大小篷车操作日志",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse<List<CaravanLogInfo>> queryCaravanLogList(@RequestBody final BaseRequest<CaravanReqBean> reqBean) {
        BaseResponse<List<CaravanLogInfo>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            ServiceBean<List<CaravanLogInfo>> result = caravanService.queryCaravanLogList(reqBean.getRequest());
            logger.info("CaravanController.queryCaravanLogList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询TPM费用列表
     * @return
     */
    @RequestMapping(value = "/queryTpmCostList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="查询TPM费用列表",description="查询TPM费用列表",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse<List<CaravanTpmCostInfo>> queryTpmCostList(@RequestBody final BaseRequest<CaravanTpmCostInfo> reqBean) {
        BaseResponse<List<CaravanTpmCostInfo>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            ServiceBean<List<CaravanTpmCostInfo>> result = caravanService.queryTpmCostList(reqBean.getRequest());
            logger.info("CaravanController.queryTpmCostList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
