package com.biostime.samanage.controller.single;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerResBean;
import com.biostime.samanage.service.single.SaLecturerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Describe:SA讲师看板
 * Date: 2018-10-21
 * Time: 09:14
 * User: 12804
 * Version:1.0
 */
@Controller
@Slf4j
@RequestMapping("/merchant/salecturer")
public class SaLecturerMerchantController {


    @Autowired
    private SaLecturerService saLecturerService;

    /**
     * SA讲师看板
     * @param reqBean
     * @return
     */
    @RequestMapping(value = "/querySaLecturerInfo.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA讲师看板",description="SA讲师看板",author="12804",model= ModelType.MKT,updateTime="2017-07-25")
    public @ResponseBody
    BaseResponse<List<SaLecturerResBean>> querySaLecturerInfo(@RequestBody final BaseRequest<SaLecturerReqBean> reqBean) {
        BaseResponse<List<SaLecturerResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("SaLecturerMerchantController.querySaLecturerInfo.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<SaLecturerResBean>> result = saLecturerService.querySaLecturerInfo(reqBean.getRequest());
            log.info("SaLecturerMerchantController.querySaLecturerInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA指标
     * @param reqBean
     * @return
     */
    @RequestMapping(value = "/querySaIndex.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA指标",description="SA指标",author="12804",model= ModelType.MKT,updateTime="2017-07-25")
    public @ResponseBody
    BaseResponse<List<BaseKeyValueBean>> querySaIndex(@RequestBody final BaseRequest<SaLecturerReqBean> reqBean) {
        BaseResponse<List<BaseKeyValueBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("SaLecturerMerchantController.querySaIndex.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<BaseKeyValueBean>> result = saLecturerService.querySaIndex(reqBean.getRequest());
            log.info("SaLecturerMerchantController.querySaIndex.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

}
