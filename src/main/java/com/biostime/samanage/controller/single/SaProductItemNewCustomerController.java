package com.biostime.samanage.controller.single;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerReqBean;
import com.biostime.samanage.bean.single.SaProductItemNewCustomerResBean;
import com.biostime.samanage.service.single.SaProductItemNewCustomerService;
import com.biostime.samanage.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA品项新客报表(新)
 * Date: 2019-12-13
 * Time: 09:14
 * User: 12804
 * Version:1.0
 */
@Controller
@Slf4j
@RequestMapping("/sa/product/newCust")
public class SaProductItemNewCustomerController {


    @Autowired
    private SaProductItemNewCustomerService saProductItemNewCustomerService;

    /**
     * SA品项新客报表-查询
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaProductItemNewCustomerList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA品项新客报表",description="SA品项新客报表",author="12804",model= ModelType.MKT,updateTime="2017-07-25")
    public @ResponseBody
    BaseResponse<Pager<SaProductItemNewCustomerResBean>> querySaProductItemNewCustomerList(@RequestBody final BaseRequest<SaProductItemNewCustomerReqBean> req) {
        SaProductItemNewCustomerReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SaProductItemNewCustomerResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("SaProductItemNewCustomerController.querySaProductItemNewCustomerList.request{}", JSON.toJSONString(req));
            Pager<SaProductItemNewCustomerResBean> pager = new Pager<SaProductItemNewCustomerResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SaProductItemNewCustomerResBean>> result = saProductItemNewCustomerService.querySaProductItemNewCustomerList(pager, reqBean);
            log.info("SaProductItemNewCustomerController.querySaProductItemNewCustomerList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA品项新客报表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaProductItemNewCustomerList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA接触明细导出",description="SA接触明细导出",author="12804",model= ModelType.MKT,updateTime="2017-07-26")
    public @ResponseBody
    BaseResponse exportSaProductItemNewCustomerList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaProductItemNewCustomerReqBean reqBean = MapperUtils.reqParameToBean(request, SaProductItemNewCustomerReqBean.class);

            // 查询
            log.info("SaProductItemNewCustomerController.exportSaProductItemNewCustomerList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saProductItemNewCustomerService.exportSaProductItemNewCustomerList(response, reqBean);
            log.info("SaProductItemNewCustomerController.exportSaProductItemNewCustomerList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
