package com.biostime.samanage.controller.single;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActReqBean;
import com.biostime.samanage.bean.single.lecturer.SaLecturerActResBean;
import com.biostime.samanage.service.single.SaLecturerService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA讲师活动报表
 * Date: 2019-1-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Controller
@RequestMapping("/sa/lecturer")
public class SaLecturerActController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaLecturerService saLecturerService;

    /**
     * SA讲师活动报表查詢
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaLecturerActList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA讲师活动报表查詢",description="SA讲师活动报表查詢",author="12804",model= ModelType.MKT,updateTime="2017-01-25")
    public @ResponseBody
    BaseResponse<Pager<SaLecturerActResBean>> querySaLecturerActList(@RequestBody final BaseRequest<SaLecturerActReqBean> req) {
        SaLecturerActReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SaLecturerActResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaLecturerActController.querySaLecturerActList.request{}", JSON.toJSONString(req));
            Pager<SaLecturerActResBean> pager = new Pager<SaLecturerActResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SaLecturerActResBean>> result = saLecturerService.querySaLecturerActList(pager, reqBean);
            logger.info("SaLecturerActController.querySaLecturerActList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * SA讲师活动报表查詢--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaLecturerActList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA讲师活动报表查詢导出",description="SA讲师活动报表查詢导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportSaLecturerActList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaLecturerActReqBean reqBean = MapperUtils.reqParameToBean(request, SaLecturerActReqBean.class);

            // 查询
            logger.info("SaLecturerActController.exportSaLecturerActList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saLecturerService.exportSaLecturerActList(response, reqBean);
            logger.info("SaLecturerActController.exportSaLecturerActList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA讲师活动报表查詢--线下导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaLecturerActDownList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA讲师活动报表查詢--线下导出",description="SA讲师活动报表查詢--线下导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportSaLecturerActDownList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaLecturerActReqBean reqBean = MapperUtils.reqParameToBean(request, SaLecturerActReqBean.class);

            // 查询
            logger.info("SaLecturerActController.exportSaLecturerActDownList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saLecturerService.exportSaLecturerActDownList(response, reqBean);
            logger.info("SaLecturerActController.exportSaLecturerActDownList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
