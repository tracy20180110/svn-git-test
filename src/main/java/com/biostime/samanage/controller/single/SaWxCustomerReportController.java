package com.biostime.samanage.controller.single;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportReqBean;
import com.biostime.samanage.bean.single.sawx.SaWxCustomerReportResBean;
import com.biostime.samanage.service.single.SaWxCustomerReportService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA微信会员报表
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/sa/wx")
public class SaWxCustomerReportController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaWxCustomerReportService saWxCustomerReportService;

    /**
     * SA微信会员报表列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaWxCustomerReportList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA微信会员报表",description="SA微信会员报表",author="12804",model= ModelType.MKT,updateTime="2017-01-25")
    public @ResponseBody
    BaseResponse<Pager<SaWxCustomerReportResBean>> querySaWxCustomerReportList(@RequestBody final BaseRequest<SaWxCustomerReportReqBean> req) {
        SaWxCustomerReportReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SaWxCustomerReportResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaWxCustomerReportController.querySaWxCustomerReportList.request{}", JSON.toJSONString(req));
            Pager<SaWxCustomerReportResBean> pager = new Pager<SaWxCustomerReportResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SaWxCustomerReportResBean>> result = saWxCustomerReportService.querySaWxCustomerReportList(pager, reqBean);
            logger.info("SaWxCustomerReportController.querySaWxCustomerReportList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA微信会员报表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaWxCustomerReportList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA微信会员报表导出",description="SA微信会员报表导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportSaWxCustomerReportList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaWxCustomerReportReqBean reqBean = MapperUtils.reqParameToBean(request, SaWxCustomerReportReqBean.class);

            // 查询
            logger.info("SaWxCustomerReportController.exportSaWxCustomerReportList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saWxCustomerReportService.exportSaWxCustomerReportList(response, reqBean);
            logger.info("SaWxCustomerReportController.exportSaWxCustomerReportList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

}
