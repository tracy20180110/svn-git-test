package com.biostime.samanage.controller.act;


import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.actreport.*;
import com.biostime.samanage.bean.actroadshow.ActReportDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowListReqBean;
import com.biostime.samanage.service.act.ActRePortService;
import com.mama100.merchant.base.system.common.bean.BgmUserOfficeBean;
import com.mama100.merchant.session.OperatorInfoContext;
import com.mama100.merchant.session.bean.OperatorInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 活动上报
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2016-08-16
 */
@Slf4j
@Controller
@RequestMapping("/merchant/activityreport")
public class ActRePortMerchantController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ActRePortService actRePortService;


    /**
     * 获取门店对应的渠道
     * @return
     */
    @RequestMapping(value = "/queryTerminalChannel.do", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "获取门店对应的渠道", description = "获取门店对应的渠道", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<ActRePortTypeResBean> queryTerminalChannel(String terminalCode,String reportTypeCode,String buCode) {
        BaseResponse<ActRePortTypeResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("ActRePortMerchantController.queryTerminalChannel.request{}", JSON.toJSONString(terminalCode));
            ServiceBean<ActRePortTypeResBean> result = actRePortService.queryTerminalChannel(terminalCode,reportTypeCode,buCode);
            logger.info("ActRePortMerchantController.queryTerminalChannel.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 获取上报的活动列表
     *
     * @return
     */
    @RequestMapping(value = "/queryActivityList.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "获取上报的活动列表", description = "获取上报的活动列表", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<List<ActRePortListResBean>> queryActRePortList(@RequestBody final BaseRequest<ActRePortListReqBean> reqBean) {
        BaseResponse<List<ActRePortListResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("ActRePortMerchantController.queryActRePortList.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<ActRePortListResBean>> result = actRePortService.queryActRePortList(reqBean.getRequest());
            logger.info("ActRePortMerchantController.queryActRePortList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 提交活动上报
     *
     * @return
     */
        @RequestMapping(value = "/submitActReport.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "提交活动上报", description = "提交活动上报", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse submitActRePort(@RequestBody final BaseRequest<ActRePortReqBean> reqBean) {
        ActRePortReqBean arReqBeans = reqBean.getRequest();
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            OperatorInfo operatorInfo = OperatorInfoContext.get();
            arReqBeans.setUserId(operatorInfo.getUser().getUserId());
            arReqBeans.setCreatedName(operatorInfo.getUser().getUserName());
            String deviceId = operatorInfo.getMobDevice().getDeviceId();
            arReqBeans.setDeviceId(deviceId);

            //获取登陆所属大区办事处
            BgmUserOfficeBean userOfficeBean = operatorInfo.getUserOffice();
            if (null != userOfficeBean) {
                if (StringUtil.isNullOrEmpty(arReqBeans.getOfficeCode())) {
                    arReqBeans.setOfficeCode(userOfficeBean.getOfficeCode());
                }
            }

            // 查询
            logger.info("ActRePortMerchantController.submitActRePort.request{}", JSON.toJSONString(arReqBeans));
            ServiceBean result = actRePortService.submitActRePort(arReqBeans);
            logger.info("ActRePortMerchantController.submitActRePort.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
    /**
     * 获取图片类型列表
     *
     * @return
     */
    @RequestMapping(value = "/queryAppImgTypeList.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "获取图片类型列表", description = "获取图片类型列表", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<List<ActRePortImgTypeResBean>> queryAppImgTypeList(@RequestBody final BaseRequest<ActRePortImgTypeReqBean> reqBean) {
        BaseResponse<List<ActRePortImgTypeResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("ActRePortImgTypeController.queryAppImgTypeList.request{}", JSON.toJSONString(reqBean.getRequest()));
            reqBean.getRequest().setStatus("1");
            ServiceBean<List<ActRePortImgTypeResBean>> result = actRePortService.queryImgTypeList(reqBean.getRequest());
            logger.info("ActRePortImgTypeController.queryAppImgTypeList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 获取上报关联的SA活动
     *
     * @return
     */
    @RequestMapping(value = "/querySaFtfActList.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "获取上报关联的SA活动", description = "获取上报关联的SA活动", author = "12804", model = ModelType.MKT, updateTime = "2018-12-10")
    public
    @ResponseBody
    BaseResponse<List<BaseKeyValueBean>> querySaFtfActList(@RequestBody final BaseRequest<ActRePortReqBean> reqBean) {
        BaseResponse<List<BaseKeyValueBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("ActRePortMerchantController.querySaFtfActList.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<BaseKeyValueBean>> result = actRePortService.querySaFtfActList(reqBean.getRequest());
            logger.info("ActRePortMerchantController.querySaFtfActList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 手机上报活动明细列表
     *
     * @return
     */
    @RequestMapping(value = "/queryActReportDetail.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "手机上报活动明细列表", description = "手机上报活动明细列表", author = "12804", model = ModelType.MKT, updateTime = "2018-12-10")
    public
    @ResponseBody
    BaseResponse<ActReportDetailResBean> queryActReportDetail(@RequestBody final BaseRequest<ActRoadShowListReqBean> reqBean) {
        BaseResponse<ActReportDetailResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("ActRePortMerchantController.querySaFtfActList.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<ActReportDetailResBean> result = actRePortService.queryActReportDetail(reqBean.getRequest());
            logger.info("ActRePortMerchantController.querySaFtfActList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 手机上报活动明细详情列表
     *
     * @return
     */
    @RequestMapping(value = "/queryActReportDetailInfo.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "手机上报活动明细详情列表", description = "手机上报活动明细详情列表", author = "12804", model = ModelType.MKT, updateTime = "2018-12-10")
    public
    @ResponseBody
    BaseResponse<List<ActRoadShowDetailResBean>> queryActReportDetailInfo(@RequestBody final BaseRequest<ActRoadShowListReqBean> reqBean) {
        BaseResponse<List<ActRoadShowDetailResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("ActRePortMerchantController.queryActReportDetailInfo.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<ActRoadShowDetailResBean>> result = actRePortService.queryActReportDetailInfo(reqBean.getRequest());
            logger.info("ActRePortMerchantController.queryActReportDetailInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询是否是活动精英
     * @param reqBean
     * @return
     */
    @RequestMapping(value = "/queryIsActElites.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询是否是活动精英", description = "查询是否是活动精英", author = "12804", model = ModelType.MKT, updateTime = "2019-05-13")
    public
    @ResponseBody
    BaseResponse queryActElitesList(@RequestBody final BaseRequest<ActRoadShowListReqBean> reqBean) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            logger.info("ActRePortMerchantController.queryIsActElites.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean result = actRePortService.queryIsActElites(reqBean.getRequest());
            logger.info("ActRePortMerchantController.queryIsActElites.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询活动精英所属办事处的门店
     * @return
     */
    @RequestMapping(value = "/queryActElitesByOfficeTerminal.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询活动精英所属办事处的门店", description = "查询活动精英所属办事处的门店", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse<List<BaseKeyValueBean>> queryActElitesByOfficeTerminal(@RequestBody final BaseRequest<ActRoadShowListReqBean> reqBean) {
        BaseResponse<List<BaseKeyValueBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            logger.info("ActRePortMerchantController.queryActElitesByOfficeTerminal.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<BaseKeyValueBean>> result = actRePortService.queryActElitesByOfficeTerminal(reqBean.getRequest());
            logger.info("ActRePortMerchantController.queryActElitesByOfficeTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询品牌官微二维码
     * @return
     */
    @RequestMapping(value = "/queryReportBrandQrCodeList.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询品牌官微二维码", description = "查询品牌官微二维码", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse<List<ActRePortQrcodeResBean>> queryReportBrandQrCodeList(@RequestBody final BaseRequest<ActRePortReqBean> reqBean) {
        BaseResponse<List<ActRePortQrcodeResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            logger.info("ActRePortMerchantController.queryReportBrandQrCodeList.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<ActRePortQrcodeResBean>> result = actRePortService.queryReportBrandQrCodeList(reqBean.getRequest());
            logger.info("ActRePortMerchantController.queryReportBrandQrCodeList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 直播活动上报
     *
     * @return
     */
    @RequestMapping(value = "/saveActRePortZb.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "直播活动上报", description = "直播活动上报", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse saveActRePortZb(@RequestBody final BaseRequest<ActRePortZhiBoReqBean> reqBean) {
        ActRePortZhiBoReqBean arReqBeans = reqBean.getRequest();
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("ActRePortMerchantController.saveActRePortZb.request{}", JSON.toJSONString(arReqBeans));
            ServiceBean result = actRePortService.saveActRePortZb(arReqBeans);
            logger.info("ActRePortMerchantController.saveActRePortZb.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询直播人员
     *
     * @return
     */
    @RequestMapping(value = "/queryZhiBoAccount.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询直播人员", description = "查询直播人员", author = "12804", model = ModelType.MKT, updateTime = "2020-04-10")
    public
    @ResponseBody
    BaseResponse<List<BaseKeyValueBean>> queryZhiBoAccount(@RequestBody final BaseRequest<BaseKeyValueBean> reqBean) {
        BaseResponse<List<BaseKeyValueBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("ActRePortMerchantController.queryZhiBoAccount.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<BaseKeyValueBean>> result = actRePortService.queryZhiBoAccount(reqBean.getRequest());
            logger.info("ActRePortMerchantController.queryZhiBoAccount.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * BNC活动精英指派门店
     * @return
     */
    @RequestMapping(value = "/queryBncActElitesTerminal.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "BNC活动精英指派门店", description = "BNC活动精英指派门店", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse<List<BaseKeyValueBean>> queryBncActElitesTerminal(@RequestBody final BaseRequest<ActRoadShowListReqBean> reqBean) {
        BaseResponse<List<BaseKeyValueBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            logger.info("ActRePortMerchantController.queryBncActElitesTerminal.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<BaseKeyValueBean>> result = actRePortService.queryBncActElitesTerminal(reqBean.getRequest());
            logger.info("ActRePortMerchantController.queryBncActElitesTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
