package com.biostime.samanage.controller.act;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.actroadshow.ActRoadShowDetailResBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowListReqBean;
import com.biostime.samanage.service.act.ActRoadShowService;
import com.biostime.samanage.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
@RequestMapping("/acteLitesReport")
public class ActElitesReportController {


    @Autowired
    private ActRoadShowService actRoadShowService;

    /**
     * 活动精英上报明细-查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/queryActElitesReportDetailList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="活动精英上报明细",description="活动精英上报明细",author="12804",model= ModelType.MKT,updateTime="2018-08-18")
    public @ResponseBody
    BaseResponse<Pager<ActRoadShowDetailResBean>> queryActElitesReportDetailList(
            @RequestBody final BaseRequest<ActRoadShowListReqBean> req, HttpServletRequest request) {
        ActRoadShowListReqBean reqBean = req.getRequest();
        BaseResponse<Pager<ActRoadShowDetailResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("ActRoadShowController.queryActRoadShowDetailList.request{}", JSON.toJSONString(req));
            Pager<ActRoadShowDetailResBean> pager = new Pager<ActRoadShowDetailResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<ActRoadShowDetailResBean>> result = actRoadShowService.queryActElitesReportDetailList(pager, reqBean);
            log.info("ActRoadShowController.queryActRoadShowDetailList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 活动精英上报明细--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportActElitesReportDetailList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="活动精英上报明细导出",description="活动精英上报明细导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportActElitesReportDetailList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            ActRoadShowListReqBean reqBean = MapperUtils.reqParameToBean(request,ActRoadShowListReqBean.class);
            // 导出
            log.info("ActRoadShowController.exportActRoadShowDetail.request{}", JSON.toJSONString(reqBean));
            ServiceBean<String> result = actRoadShowService.exportActElitesReportDetailList(response, reqBean);
            log.info("ActRoadShowController.exportActRoadShowDetail.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

}
