package com.biostime.samanage.controller.act;


import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigReqBean;
import com.biostime.samanage.bean.actroadshow.ActRoadShowConfigResBean;
import com.biostime.samanage.service.act.ActRoadShowConfigService;
import com.biostime.samanage.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 路演活动
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2018-03-13
 */
@Slf4j
@Controller
@RequestMapping("/actRoadShow/config")
public class ActRoadShowConfigController {

    @Autowired
    private ActRoadShowConfigService actRoadShowConfigService;


    /**
     * 路演活动配置查询
     * @return
     */
    @RequestMapping(value = "/queryActRoadShowConfigList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="路演活动关系配置查询",description="路演活动关系配置查询",author="12804",model= ModelType.MKT,updateTime="2018-03-13")
    public @ResponseBody
    BaseResponse<Pager<ActRoadShowConfigResBean>> queryActRoadShowConfigList(
            @RequestBody final BaseRequest<ActRoadShowConfigReqBean> req) {
        ActRoadShowConfigReqBean reqBean = req.getRequest();
        BaseResponse<Pager<ActRoadShowConfigResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("ActRoadShowConfigController.queryActRoadShowConfigList.request{}", JSON.toJSONString(req));
            Pager<ActRoadShowConfigResBean> pager = new Pager<ActRoadShowConfigResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<ActRoadShowConfigResBean>> result = actRoadShowConfigService.queryActRoadShowConfigList(pager, reqBean);
            log.info("ActRoadShowConfigController.queryActRoadShowConfigList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 路演活动配置导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportActRoadShowConfigList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="路演活动关系导出",description="路演活动关系导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportActRoadShow(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            ActRoadShowConfigReqBean reqBean = MapperUtils.reqParameToBean(request, ActRoadShowConfigReqBean.class);
            // 导出
            log.info("ActRoadShowConfigController.exportActRoadShowConfigList.request{}", JSON.toJSONString(reqBean));
            ServiceBean<String> result = actRoadShowConfigService.exportActRoadShowConfigList(response, reqBean);
            log.info("ActRoadShowConfigController.exportActRoadShowConfigList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 路演活动关系状态修改
     * @param req
     * @return
     */
    @RequestMapping(value = "/editStatusActRoadShowConfig.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="路演活动关系状态修改",description="路演活动关系状态修改",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse editStatusActRoadShowConfig(@RequestBody final BaseRequest<ActRoadShowConfigReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRoadShowConfigController.editStatusActRoadShowConfig.request{}", JSON.toJSONString(req));
            ServiceBean result = actRoadShowConfigService.editStatusActRoadShowConfig(req.getRequest());
            log.info("ActRoadShowConfigController.editStatusActRoadShowConfig.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 路演活动关系配置-新增
     * @return
     */
    @RequestMapping(value = "/saveActRoadShowConfig.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="路演活动关系配置-新增",description="路演活动关系配置-新增",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse saveActRoadShowConfig(@RequestBody final BaseRequest<ActRoadShowConfigReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRoadShowConfigController.saveActRoadShowConfig.request{}", JSON.toJSONString(req));
            ServiceBean result = actRoadShowConfigService.saveActRoadShowConfig(req.getRequest());
            log.info("ActRoadShowConfigController.saveActRoadShowConfig.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }




}
