package com.biostime.samanage.controller.act;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.actreport.ActRePortImgTypeReqBean;
import com.biostime.samanage.bean.actreport.ActRePortImgTypeResBean;
import com.biostime.samanage.service.act.ActRePortService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 活动上报图片类型
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2018-08-16
 */
@Slf4j
@Controller
@RequestMapping("/act/report")
public class ActRePortImgTypeController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ActRePortService actRePortService;


    /**
     * 图片类型查询
     * @return
     */
    @RequestMapping(value = "/queryActRePortImgTypeList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="路活动上报图片类型查询",description="路活动上报图片类型查询",author="12804",model= ModelType.MKT,updateTime="2018-03-13")
    public @ResponseBody
    BaseResponse<Pager<ActRePortImgTypeResBean>> queryActRePortImgTypeList(
            @RequestBody final BaseRequest<ActRePortImgTypeReqBean> reqBean) {
        ActRePortImgTypeReqBean req = reqBean.getRequest();
        BaseResponse<Pager<ActRePortImgTypeResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("ActRePortImgTypeController.queryActRePortImgTypeList.request{}", JSON.toJSONString(req));
            Pager<ActRePortImgTypeResBean> pager = new Pager<ActRePortImgTypeResBean>(req.getPageSize(),req.getPageNo());
            ServiceBean<Pager<ActRePortImgTypeResBean>> result = actRePortService.queryActRePortImgTypeList(pager, req);
            log.info("ActRePortImgTypeController.queryActRePortImgTypeList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }





    /**
     * 获取图片类型列表
     *
     * @return
     */
    @RequestMapping(value = "/queryWebImgTypeList.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "获取图片类型列表", description = "获取图片类型列表", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<List<ActRePortImgTypeResBean>> queryAppImgTypeList(@RequestBody final BaseRequest<ActRePortImgTypeReqBean> reqBean) {
        BaseResponse<List<ActRePortImgTypeResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("ActRePortImgTypeController.queryImgTypeList.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<ActRePortImgTypeResBean>> result = actRePortService.queryImgTypeList(reqBean.getRequest());
            logger.info("ActRePortImgTypeController.queryImgTypeList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 图片类型保存、编辑
     * @return
     */
    @RequestMapping(value = "/saveActRePortImgType.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="图片类型保存、编辑",description="图片类型保存、编辑",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse saveActRePortImgType(@RequestBody final BaseRequest<ActRePortImgTypeReqBean> reqBean) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRePortImgTypeController.saveActRePortImgType.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = actRePortService.saveActRePortImgType(reqBean.getRequest());
            log.info("ActRePortImgTypeController.saveActRePortImgType.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
