package com.biostime.samanage.controller.act;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.actreport.ActRePortZhiBoReqBean;
import com.biostime.samanage.service.act.ActRoadShowService;
import com.biostime.samanage.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
@RequestMapping("/actReportZb")
public class ActReportZhiBoController {


    @Autowired
    private ActRoadShowService actRoadShowService;

    /**
     * 直播上报报表-查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/queryActReportZhiBoList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="直播上报报表",description="直播上报报表",author="12804",model= ModelType.MKT,updateTime="2018-08-18")
    public @ResponseBody
    BaseResponse<Pager<ActRePortZhiBoReqBean>> queryActReportZhiBoList(@RequestBody final BaseRequest<ActRePortZhiBoReqBean> request) {
        ActRePortZhiBoReqBean reqBean = request.getRequest();
        BaseResponse<Pager<ActRePortZhiBoReqBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("ActReportZhiBoController.queryActReportZhiBoList.request{}", JSON.toJSONString(request));
            Pager<ActRePortZhiBoReqBean> pager = new Pager<ActRePortZhiBoReqBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<ActRePortZhiBoReqBean>> result = actRoadShowService.queryActReportZhiBoList(pager, reqBean);
            log.info("ActReportZhiBoController.queryActReportZhiBoList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 直播上报报表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportActReportZhiBoList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="直播上报报表导出",description="直播上报报表导出",author="12804",model= ModelType.MKT,updateTime="2020-02-18")
    public @ResponseBody
    BaseResponse exportActReportZhiBoList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            ActRePortZhiBoReqBean reqBean = MapperUtils.reqParameToBean(request,ActRePortZhiBoReqBean.class);
            // 导出
            log.info("ActReportZhiBoController.exportActReportZhiBoList.request{}", JSON.toJSONString(reqBean));
            ServiceBean<String> result = actRoadShowService.exportActReportZhiBoList(response, reqBean);
            log.info("ActReportZhiBoController.exportActReportZhiBoList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

}
