package com.biostime.samanage.controller.act;


import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.actreport.ActRePortWebReqBean;
import com.biostime.samanage.bean.actreport.ActRePortWebResBean;
import com.biostime.samanage.bean.actroadshow.*;
import com.biostime.samanage.service.act.ActRoadShowService;
import com.biostime.samanage.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 活动现场
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author wangxiaowei
 * @createDate 2016-08-16
 */
@Slf4j
@Controller
@RequestMapping("/actRoadShow")
public class ActRoadShowController {

    @Autowired
    private ActRoadShowService actRoadShowService;


    /**
     * 活动现场配置查询
     * @return
     */
    @RequestMapping(value = "/queryActRoadShowList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="活动现场配置查询(POST请求)",description="活动现场配置查询",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse<Pager<ActRoadShowListResBean>> queryActRoadShowList(
            @RequestBody final BaseRequest<ActRoadShowListReqBean> req) {
        ActRoadShowListReqBean reqBean = req.getRequest();
        BaseResponse<Pager<ActRoadShowListResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("ActRoadShowController.queryActRoadShowList.request{}", JSON.toJSONString(req));
            Pager<ActRoadShowListResBean> pager = new Pager<ActRoadShowListResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<ActRoadShowListResBean>> result = actRoadShowService.queryActRoadShowList(pager,reqBean);
            log.info("ActRoadShowController.queryActRoadShowList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 活动现场配置导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportActRoadShow.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="活动现场配置导出",description="活动现场配置导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportActRoadShow(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            ActRoadShowListReqBean reqBean = MapperUtils.reqParameToBean(request, ActRoadShowListReqBean.class);
            // 导出
            log.info("ActRoadShowController.exportActRoadShow.request{}", JSON.toJSONString(reqBean));
            ServiceBean<String> result = actRoadShowService.exportActRoadShow(response, reqBean);
            log.info("ActRoadShowController.exportActRoadShow.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 活动现场配置编辑、新增
     * @return
     */
    @RequestMapping(value = "/saveActRoadShow.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="活动现场配置编辑、新增(POST请求)",description="活动现场配置编辑、新增",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse saveActRoadShow(@RequestBody final BaseRequest<ActRoadShowSaveReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRoadShowController.saveActRoadShow.request{}", JSON.toJSONString(req));
            ServiceBean result = actRoadShowService.saveActRoadShow(req.getRequest());
            log.info("ActRoadShowController.saveActRoadShow.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 活动现场配置停用
     * @param req
     * @return
     */
    @RequestMapping(value = "/changeStatusActRoadShow.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="活动现场配置停用(POST请求)",description="活动现场配置停用",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse changeStatusActRoadShow(@RequestBody final BaseRequest<ActRoadShowChangeStatusReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRoadShowController.changeStatusActRoadShow.request{}", JSON.toJSONString(req));
            ServiceBean result = actRoadShowService.changeStatusActRoadShow(req.getRequest());
            log.info("ActRoadShowController.changeStatusActRoadShow.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 活动现场-获取swisse列表、活动类型
     * @return
     */
    @RequestMapping(value = "/querySwActTypeList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="活动现场-获取swisse列表、活动类型",description="活动现场-获取swisse列表、活动类型",author="12804",model= ModelType.MKT,updateTime="2018-03-18")
    public @ResponseBody
    BaseResponse<List<ActRoadShowResBean>> querySwActTypeList(
            @RequestBody final BaseRequest<ActRoadShowReqBean> req) {
        ActRoadShowReqBean reqBean = req.getRequest();
        BaseResponse<List<ActRoadShowResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("ActRoadShowController.queryActRoadShowList.request{}", JSON.toJSONString(req));
            ServiceBean<List<ActRoadShowResBean>> result = actRoadShowService.querySwActTypeList(reqBean);
            log.info("ActRoadShowController.queryActRoadShowList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 营销上报明细-查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/queryActRoadShowDetailList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="营销上报明细(POST请求)",description="营销上报明细",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse<Pager<ActRoadShowDetailResBean>> queryActRoadShowDetailList(
            @RequestBody final BaseRequest<ActRoadShowListReqBean> req,HttpServletRequest request) {
        ActRoadShowListReqBean reqBean = req.getRequest();
        BaseResponse<Pager<ActRoadShowDetailResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("ActRoadShowController.queryActRoadShowDetailList.request{}", JSON.toJSONString(req));
            Pager<ActRoadShowDetailResBean> pager = new Pager<ActRoadShowDetailResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<ActRoadShowDetailResBean>> result = actRoadShowService.queryActRoadShowDetailList(pager, reqBean);
            log.info("ActRoadShowController.queryActRoadShowDetailList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 营销上报明细--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportActRoadShowDetail.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="活动现场配置导出",description="活动现场配置导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportActRoadShowDetail(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            ActRoadShowListReqBean reqBean = MapperUtils.reqParameToBean(request,ActRoadShowListReqBean.class);
            // 导出
            log.info("ActRoadShowController.exportActRoadShowDetail.request{}", JSON.toJSONString(reqBean));
            ServiceBean<String> result = actRoadShowService.exportActRoadShowDetail(response, reqBean);
            log.info("ActRoadShowController.exportActRoadShowDetail.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 营销上报明细-审核
     * @param req
     * @return
     */
    @RequestMapping(value = "/auditActRoadShowDetail.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="营销上报明细-审核(POST请求)",description="营销上报明细-审核",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse auditActRoadShowDetail(@RequestBody final BaseRequest<ActRoadShowAuditReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRoadShowController.auditActivity.request{}", JSON.toJSONString(req));
            ServiceBean result = actRoadShowService.auditActRoadShowDetail(req.getRequest());
            log.info("ActRoadShowController.auditActivity.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 路演报表查询
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryReportActRoadShowList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="路演报表查询(POST请求)",description="路演报表查询",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse<Pager<ActRoadShowReportResBean>> queryReportActRoadShowList(
            @RequestBody final BaseRequest<ActRoadShowReportReqBean> req) {
        ActRoadShowReportReqBean reqBean = req.getRequest();
        BaseResponse<Pager<ActRoadShowReportResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            log.info("ActRoadShowController.queryReportActRoadShow.request{}", JSON.toJSONString(req));
            Pager<ActRoadShowReportResBean> pager = new Pager<ActRoadShowReportResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<ActRoadShowReportResBean>> result = actRoadShowService.queryReportActRoadShowList(pager, reqBean);
            log.info("ActRoadShowController.queryReportActRoadShow.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 路演报表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportReportActRoadShow.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="路演报表导出",description="路演报表导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportReportActRoadShow(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            ActRoadShowReportReqBean reqBean = MapperUtils.reqParameToBean(request,ActRoadShowReportReqBean.class);

            // 查询
            log.info("ActRoadShowController.exportReportActRoadShow.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = actRoadShowService.exportReportActRoadShow(response, reqBean);
            log.info("ActRoadShowController.exportReportActRoadShow.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 活动现场--门店导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportActRoadShowTerminal.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="活动现场门店导出",description="活动现场门店导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportActRoadShowTerminal(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            String actId = request.getParameter("actId");
            // 导出
            log.info("ActRoadShowController.exportActRoadShowTerminal.request{}", JSON.toJSONString(actId));
            ServiceBean<String> result = actRoadShowService.exportActRoadShowTerminal(response, actId);
            log.info("ActRoadShowController.exportActRoadShowTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 活动现场--门店导入
     * @param request
     * @return
     */

    @RequestMapping(value = "/importActRoadShowTerminal.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="活动现场配置导入(POST请求)",description="活动现场配置导入",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse<String> importActRoadShowTerminal(
            @RequestParam(value = "file", required = false) MultipartFile multipartFile,
            HttpServletRequest request) {

        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            ActRoadShowTerminalReqBean reqBean =  MapperUtils.reqParameToBean(request, ActRoadShowTerminalReqBean.class);

            log.info("ActRoadShowController.importActRoadShowTerminal.request{}", JSON.toJSONString(reqBean));
            ServiceBean<String> result = actRoadShowService.importActRoadShowTerminal(multipartFile, reqBean);
            log.info("ActRoadShowController.importActRoadShowTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getResponse());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 营销上报明细-补录
     * @return
     */
    @RequestMapping(value = "/saveActRoadShowMakeup.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="营销上报明细-补录(POST请求)",description="营销上报明细-补录",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse saveActRoadShowMakeup(@RequestBody final BaseRequest<ActRePortWebReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRoadShowController.saveActRoadShowMakeup.request{}", JSON.toJSONString(req));
            ServiceBean result = actRoadShowService.saveActRoadShowMakeup(req.getRequest());
            log.info("ActRoadShowController.saveActRoadShowMakeup.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 营销上报明细-补录活动验证
     * @param req
     * @return
     */
    @RequestMapping(value = "/checkActShowInfo.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="营销上报明细-补录活动验证(POST请求)",description="营销上报明细-补录活动验证",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse checkActShowInfo(@RequestBody final BaseRequest<ActRePortWebReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRoadShowController.checkActShowInfo.request{}", JSON.toJSONString(req));
            ServiceBean<ActRePortWebResBean> result = actRoadShowService.checkActShowInfo(req.getRequest());
            log.info("ActRoadShowController.checkActShowInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 营销上报明细-补录门店验证
     * @param req
     * @return
     */
    @RequestMapping(value = "/checkActTerminal.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="营销上报明细-补录门店验证(POST请求)",description="营销上报明细-补录门店验证",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse checkActTerminal(@RequestBody final BaseRequest<ActRePortWebReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRoadShowController.checkActTerminal.request{}", JSON.toJSONString(req));
            ServiceBean result = actRoadShowService.checkActTerminal(req.getRequest());
            log.info("ActRoadShowController.checkActTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 营销上报明细-上报人验证
     * @param req
     * @return
     */
    @RequestMapping(value = "/checkAccountTerminal.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="营销上报明细-上报人验证(POST请求)",description="营销上报明细-上报人验证",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse checkAccountTerminal(@RequestBody final BaseRequest<ActRePortWebReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            log.info("ActRoadShowController.checkAccountTerminal.request{}", JSON.toJSONString(req));
            ServiceBean result = actRoadShowService.checkAccountTerminal(req.getRequest());
            log.info("ActRoadShowController.checkAccountTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
