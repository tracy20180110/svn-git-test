package com.biostime.samanage.controller.terminal;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.terminal.SaTerminalInfoReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalInfoResBean;
import com.biostime.samanage.service.terminal.SaTerminalInfoService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA门店查询
 * Date: 2017-02-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/sa/terminal/info")
public class SaTerminalInfoController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaTerminalInfoService SaTerminalInfoService;


    /**
     * SA门店查询列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaTerminalInfoList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA门店查询",description="SA门店查询",author="12804",model= ModelType.MKT,updateTime="2017-05-15")
    public @ResponseBody
    BaseResponse<Pager<SaTerminalInfoResBean>> querySaTerminalInfoList(@RequestBody final BaseRequest<SaTerminalInfoReqBean> req) {
        SaTerminalInfoReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SaTerminalInfoResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaTerminalInfoController.querySaTerminalInfoList.request{}", JSON.toJSONString(req));
            Pager<SaTerminalInfoResBean> pager = new Pager<SaTerminalInfoResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SaTerminalInfoResBean>> result = SaTerminalInfoService.querySaTerminalInfoList(pager, reqBean);
            logger.info("SaTerminalInfoController.querySaTerminalInfoList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA门店查询列表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaTerminalInfoList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA门店查询列表导出",description="SA门店查询列表导出",author="12804",model= ModelType.MKT,updateTime="2017-03-08")
    public @ResponseBody
    BaseResponse exportSaTerminalInfoList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaTerminalInfoReqBean reqBean = MapperUtils.reqParameToBean(request, SaTerminalInfoReqBean.class);

            // 查询
            logger.info("SaTerminalInfoController.exportSaTerminalInfoList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = SaTerminalInfoService.exportSaTerminalInfoList(response, reqBean);
            logger.info("SaTerminalInfoController.exportSaTerminalInfoList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


}
