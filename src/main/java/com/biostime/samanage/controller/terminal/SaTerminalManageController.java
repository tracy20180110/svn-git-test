package com.biostime.samanage.controller.terminal;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.single.adset.SaAdSetExternalResBean;
import com.biostime.samanage.bean.terminal.*;
import com.biostime.samanage.service.terminal.SaTerminalManageService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:SA与门店管理
 * Date: 2017-02-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/sa/terminal")
public class SaTerminalManageController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaTerminalManageService saTerminalManageService;

    /**
     * SA与门店管理导入列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaTerminalManageImportList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA与门店管理",description="SA与门店管理",author="12804",model= ModelType.MKT,updateTime="2017-01-25")
    public @ResponseBody
    BaseResponse<Pager<SaTerminalManageImportResBean>> querySaTerminalManageImportList(@RequestBody final BaseRequest<SaTerminalManageImportReqBean> req) {
        SaTerminalManageImportReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SaTerminalManageImportResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaTerminalManageController.querySaTerminalManageImportList.request{}", JSON.toJSONString(req));
            Pager<SaTerminalManageImportResBean> pager = new Pager<SaTerminalManageImportResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SaTerminalManageImportResBean>> result = saTerminalManageService.querySaTerminalManageImportList(pager, reqBean);
            logger.info("SaTerminalManageController.querySaTerminalManageImportList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA与门店管理导入列表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaTerminalManageImportList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA与门店管理导出",description="SA与门店管理导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportSaTerminalManageImportList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaTerminalManageImportReqBean reqBean = MapperUtils.reqParameToBean(request, SaTerminalManageImportReqBean.class);

            // 查询
            logger.info("SaTerminalManageController.exportSaTerminalManageImportList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saTerminalManageService.exportSaTerminalManageImportList(response, reqBean);
            logger.info("SaTerminalManageController.exportSaTerminalManageImportList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * SA与门店管理--门店导入
     * @param request
     * @return
     */

    @RequestMapping(value = "/importSaTerminalManage.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="SA与门店管理导入(POST请求)",description="SA与门店管理导入",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse<List> importSaTerminalManage(
            @RequestParam(value = "file", required = false) MultipartFile multipartFile,
            HttpServletRequest request) {

        BaseResponse<List> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            ServiceBean<List> result = saTerminalManageService.importSaTerminalManage(multipartFile, request);
            logger.info("SaTerminalManageController.importSaTerminalManage.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA与门店管理列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaTerminalManageList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA与门店管理",description="SA与门店管理",author="12804",model= ModelType.MKT,updateTime="2017-01-25")
    public @ResponseBody
    BaseResponse<Pager<SaTerminalManageResBean>> querySaTerminalManageList(@RequestBody final BaseRequest<SaTerminalManageReqBean> req) {
        SaTerminalManageReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SaTerminalManageResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaTerminalManageController.querySaTerminalManageList.request{}", JSON.toJSONString(req));
            Pager<SaTerminalManageResBean> pager = new Pager<SaTerminalManageResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SaTerminalManageResBean>> result = saTerminalManageService.querySaTerminalManageList(pager, reqBean);
            logger.info("SaTerminalManageController.querySaTerminalManageList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA与门店管理--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaTerminalManageList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA与门店管理导出",description="SA与门店管理导出",author="12804",model= ModelType.MKT,updateTime="2017-03-08")
    public @ResponseBody
    BaseResponse exportSaTerminalManageList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaTerminalManageReqBean reqBean = MapperUtils.reqParameToBean(request, SaTerminalManageReqBean.class);

            // 查询
            logger.info("SaTerminalManageController.exportSaTerminalManageList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saTerminalManageService.exportSaTerminalManageList(response, reqBean);
            logger.info("SaTerminalManageController.exportSaTerminalManageList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA与门店管理--删除
     * @param req
     * @return
     */
    @RequestMapping(value = "/deleteSaTerminalManage.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="SA与门店管理-删除",description="SA与门店管理-删除",author="12804",model= ModelType.MKT,updateTime="2017-02-18")
    public @ResponseBody
    BaseResponse deleteSaTerminalManage(@RequestBody final BaseRequest<SaTerminalManageEditReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            logger.info("SaTerminalManageController.editSaTerminalManage.request{}", JSON.toJSONString(req));
            ServiceBean result = saTerminalManageService.deleteSaTerminalManage(req.getRequest());
            logger.info("SaTerminalManageController.deleteSaTerminalManage.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA与门店管理-编辑
     * @param req
     * @return
     */
    @RequestMapping(value = "/editSaTerminalManage.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="SA与门店管理-编辑",description="SA与门店管理-编辑",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse editSaTerminalManage(@RequestBody final BaseRequest<SaTerminalManageEditReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("SaTerminalManageController.editSaTerminalManage.request{}", JSON.toJSONString(req));
            ServiceBean result = saTerminalManageService.editSaTerminalManage(req.getRequest());
            logger.info("SaTerminalManageController.editSaTerminalManage.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA撤销结束与门店的关系
     * @return
     */
    @RequestMapping(value = "/delSaTerminalRelation.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "SA撤销结束与门店的关系", description = "SA撤销结束与门店的关系", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<SaAdSetExternalResBean> delSaTerminalRelation(@RequestBody final BaseRequest<SaTerminalManageReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaTerminalManageController.delSaTerminalRelation.request{}", JSON.toJSONString(req));
            ServiceBean result = saTerminalManageService.delSaTerminalRelation(req.getRequest());
            logger.info("SaTerminalManageController.delSaTerminalRelation.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
