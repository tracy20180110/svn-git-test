package com.biostime.samanage.controller.terminal;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewReqBean;
import com.biostime.samanage.bean.terminal.SaTerminalManageNewResBean;
import com.biostime.samanage.service.terminal.SaTerminalManageNewService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:SA与门店管理
 * Date: 2017-02-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Controller
@RequestMapping("/sa/terminal/new")
public class SaTerminalManageNewController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaTerminalManageNewService saTerminalManageNewService;



    /**
     * SA与门店管理列表(新)
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaTerminalManageNewList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="SA与门店管理",description="SA与门店管理",author="12804",model= ModelType.MKT,updateTime="2017-01-25")
    public @ResponseBody
    BaseResponse<Pager<SaTerminalManageNewResBean>> querySaTerminalManageNewList(@RequestBody final BaseRequest<SaTerminalManageNewReqBean> req) {
        SaTerminalManageNewReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SaTerminalManageNewResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaTerminalManageNewController.querySaTerminalManageNewList.request{}", JSON.toJSONString(req));
            Pager<SaTerminalManageNewResBean> pager = new Pager<SaTerminalManageNewResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SaTerminalManageNewResBean>> result = saTerminalManageNewService.querySaTerminalManageNewList(pager, reqBean);
            logger.info("SaTerminalManageNewController.querySaTerminalManageNewList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA与门店管理--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSaTerminalManageNewList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="SA与门店管理导出",description="SA与门店管理导出",author="12804",model= ModelType.MKT,updateTime="2017-03-08")
    public @ResponseBody
    BaseResponse exportSaTerminalManageList(HttpServletRequest request, HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaTerminalManageNewReqBean reqBean = MapperUtils.reqParameToBean(request, SaTerminalManageNewReqBean.class);

            // 查询
            logger.info("SaTerminalManageNewController.exportSaTerminalManageNewList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = saTerminalManageNewService.exportSaTerminalManageNewList(response, reqBean);
            logger.info("SaTerminalManageNewController.exportSaTerminalManageNewList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * SA与门店绑定关系
     * @param req
     * @return
     */
    @RequestMapping(value = "/saveSaTerminalRelationNew.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="SA与门店绑定关系",description="SA与门店绑定关系",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse saveSaTerminalRelationNew(@RequestBody final BaseRequest<SaTerminalManageNewReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {
            logger.info("SaTerminalManageNewController.saveSaTerminalRelationNew.request{}", JSON.toJSONString(req));
            ServiceBean result = saTerminalManageNewService.saveSaTerminalRelationNew(req.getRequest());
            logger.info("SaTerminalManageNewController.saveSaTerminalRelationNew.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询门店是否存在
     * @return
     */
    @RequestMapping(value = "/queryTerminalInfo.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询门店信息", description = "查询门店信息", author = "12804", model = ModelType.MKT, updateTime = "2017-04-24")
    public
    @ResponseBody
    BaseResponse<SaTerminalManageNewResBean> queryTerminalInfo(@RequestBody final BaseRequest<SaTerminalManageNewReqBean> req) {
        BaseResponse<SaTerminalManageNewResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaTerminalManageNewController.queryTerminalInfo.request{}", JSON.toJSONString(req));
            ServiceBean<SaTerminalManageNewResBean> result = saTerminalManageNewService.queryTerminalInfo(req.getRequest());
            logger.info("SaTerminalManageNewController.queryTerminalInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
