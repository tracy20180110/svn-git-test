package com.biostime.samanage.controller.terminal;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.terminal.TerminalVisitQuarterResBean;
import com.biostime.samanage.bean.terminal.TerminalVisitReqBean;
import com.biostime.samanage.bean.terminal.TerminalVisitResBean;
import com.biostime.samanage.service.terminal.TerminalVisitService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:门店拜访考核报表
 * Date: 2017-02-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Controller
@RequestMapping("/terminal/visit")
public class TerminalVisitController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TerminalVisitService terminalVisitService;

    /**
     * 门店拜访考核月度查询
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryTerminalVisitList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="门店拜访考核月度查询",description="门店拜访考核月度查询",author="12804",model= ModelType.MKT,updateTime="2017-01-25")
    public @ResponseBody
    BaseResponse<Pager<TerminalVisitResBean>> queryTerminalVisitList(@RequestBody final BaseRequest<TerminalVisitReqBean> req) {
        TerminalVisitReqBean reqBean = req.getRequest();
        BaseResponse<Pager<TerminalVisitResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("TerminalVisitController.queryTerminalVisitList.request{}", JSON.toJSONString(req));
            Pager<TerminalVisitResBean> pager = new Pager<TerminalVisitResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<TerminalVisitResBean>> result = terminalVisitService.queryTerminalVisitList(pager, reqBean);
            logger.info("TerminalVisitController.queryTerminalVisitList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 门店拜访考核月度--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportTerminalVisitList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="门店拜访考核月度",description="门店拜访考核月度",author="12804",model= ModelType.MKT,updateTime="2017-03-08")
    public @ResponseBody
    BaseResponse exportTerminalVisitList(HttpServletRequest request, HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            TerminalVisitReqBean reqBean = MapperUtils.reqParameToBean(request, TerminalVisitReqBean.class);

            // 查询
            logger.info("TerminalVisitController.exportSaTerminalNewManageList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = terminalVisitService.exportTerminalVisitList(response, reqBean);
            logger.info("TerminalVisitController.exportSaTerminalNewManageList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询门店拜访岗位列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryTerminalVisitPostList.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询门店拜访岗位列表", description = "查询门店拜访岗位列表", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse<List<BaseKeyValueBean>> queryTerminalVisitPostList(@RequestBody final BaseRequest<TerminalVisitReqBean> req) {
        BaseResponse<List<BaseKeyValueBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            logger.info("TerminalVisitController.queryTerminalVisitPostList.request{}", JSON.toJSONString(req));
            ServiceBean<List<BaseKeyValueBean>> result = terminalVisitService.queryTerminalVisitPostList(req.getRequest());
            logger.info("TerminalVisitController.queryTerminalVisitPostList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 门店拜访考核季度查询
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryQuarterTerminalVisitList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="门店拜访考核季度查询",description="门店拜访考核季度查询",author="12804",model= ModelType.MKT,updateTime="2017-01-25")
    public @ResponseBody
    BaseResponse<Pager<TerminalVisitQuarterResBean>> queryTerminalQuarterVisitList(@RequestBody final BaseRequest<TerminalVisitReqBean> req) {
        TerminalVisitReqBean reqBean = req.getRequest();
        BaseResponse<Pager<TerminalVisitQuarterResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("TerminalVisitController.queryTerminalQuarterVisitList.request{}", JSON.toJSONString(req));
            Pager<TerminalVisitQuarterResBean> pager = new Pager<TerminalVisitQuarterResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<TerminalVisitQuarterResBean>> result = terminalVisitService.queryTerminalVisitQuarterList(pager, reqBean);
            logger.info("TerminalVisitController.queryTerminalQuarterVisitList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 门店拜访考核季度导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportQuarterTerminalVisitList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="门店拜访考核季度导出",description="门店拜访考核季度导出",author="12804",model= ModelType.MKT,updateTime="2017-03-08")
    public @ResponseBody
    BaseResponse exportTerminalVisitQuarterList(HttpServletRequest request, HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            TerminalVisitReqBean reqBean = MapperUtils.reqParameToBean(request, TerminalVisitReqBean.class);

            // 查询
            logger.info("TerminalVisitController.exportTerminalVisitQuarterList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = terminalVisitService.exportTerminalVisitQuarterList(response, reqBean);
            logger.info("TerminalVisitController.exportTerminalVisitQuarterList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
