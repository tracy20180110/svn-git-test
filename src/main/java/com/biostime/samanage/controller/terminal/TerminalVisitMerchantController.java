package com.biostime.samanage.controller.terminal;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.terminal.TerminalVisitMobReqBean;
import com.biostime.samanage.bean.terminal.TerminalVisitMobResBean;
import com.biostime.samanage.service.terminal.TerminalVisitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:门店拜访查询
 * Date: 2019-09-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/merchant/visit")
public class TerminalVisitMerchantController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TerminalVisitService terminalVisitService;

    /**
     * 查询门店拜访列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryTerminalVisitMobList.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="查询门店拜访列表",description="查询门店拜访列表",author="12804",model= ModelType.MKT,updateTime="2019-08-08")
    public @ResponseBody
    BaseResponse<List<TerminalVisitMobResBean>> queryTerminalVisitMobList(
            @RequestBody final BaseRequest<TerminalVisitMobReqBean> req
    ) {
        BaseResponse<List<TerminalVisitMobResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("TerminalVisitMerchantController.queryTerminalVisitMobList.request{}", JSON.toJSONString(req.getRequest()));
            ServiceBean<List<TerminalVisitMobResBean>> result = terminalVisitService.queryTerminalVisitMobList(req.getRequest());
            logger.info("TerminalVisitMerchantController.queryTerminalVisitMobList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
