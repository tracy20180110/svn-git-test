package com.biostime.samanage.controller.saPrize;

import com.alibaba.fastjson.JSON;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.saPrize.SaAssessIndexResBean;
import com.biostime.samanage.bean.saPrize.SaPrizeDetailReqBean;
import com.biostime.samanage.service.saPrize.SaPrizeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @title:SaAssessIndexController.java
 * @description:SA考核指标，根据门店汇总(外部调用)
 * @author W1038
 * @date 2018年1月26日 上午10:20:50
 */
@Controller
@RequestMapping("/saAssessIndex")
public class SaAssessIndexController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SaPrizeService saPrizeService;

	/**
	 * 查询SA考核指标信息，APP调用
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/querySaAssessIndexInfo.do", method = { RequestMethod.POST })
	public @ResponseBody BaseResponse<SaAssessIndexResBean> querySaAssessIndexInfo(
			@RequestBody final BaseRequest<SaPrizeDetailReqBean> req) {

		BaseResponse<SaAssessIndexResBean> resp = new BaseResponse<SaAssessIndexResBean>(DateUtils.makeDateSeqNo());
		try {
			logger.info("SaAssessIndexController.querySaAssessIndexInfo.request{}", JSON.toJSONString(req));
			ServiceBean<SaAssessIndexResBean> serviceBean = saPrizeService.querySaAssessIndexInfo(req.getRequest());
			logger.info("SaAssessIndexController.querySaAssessIndexInfo.response{}", JSON.toJSONString(serviceBean));
			resp.setResponse(serviceBean.getResponse());
			resp.setCode(serviceBean.getCode());
			resp.setDesc(serviceBean.getDesc());
			ExceptionUtils.setError(resp);
		} catch (Exception ex) {
			ExceptionUtils.setSystemError(resp, ex.getMessage());
			logger.error(ex.getMessage(), ex);
		}
		return resp;
	}
}
