package com.biostime.samanage.controller.saPrize;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.saPrize.SaPrizeDetailReqBean;
import com.biostime.samanage.bean.saPrize.SaPrizeDetailResBean;
import com.biostime.samanage.bean.saPrize.SaPrizeReqBean;
import com.biostime.samanage.bean.saPrize.SaPrizeResBean;
import com.biostime.samanage.service.saPrize.SaPrizeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取SA育婴顾问人员实时奖金
 * Date: 2016-6-20
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/saPrize")
public class SaPrizeController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaPrizeService saPrizeService;

    /**
     * 获取人员实时奖金列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaPrizeList.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="获取人员实时奖金",description="获取人员实时奖金",author="12804",model= ModelType.MKT,updateTime="2016-06-20")
    public @ResponseBody
    BaseResponse<List<SaPrizeResBean>> querySaPrizeList(
            @RequestBody final BaseRequest<SaPrizeReqBean> req
    ) {
        BaseResponse<List<SaPrizeResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaPrizeController.querySaPrizeList.request{}", JSON.toJSONString(req));
            ServiceBean<List<SaPrizeResBean>> result = saPrizeService.querySaPrizeList(req.getRequest());
            logger.info("SaPrizeController.querySaPrizeList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 获取人员实时奖金明细
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaPrizeDetailInfo.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="获取人员实时奖金明细",description="获取人员实时奖金明细",author="12804",model= ModelType.MKT,updateTime="2016-06-20")
    public @ResponseBody
    BaseResponse<List<SaPrizeDetailResBean>> querySaPrizeDetailInfo(
            @RequestBody final BaseRequest<SaPrizeDetailReqBean> req
    ) {
        BaseResponse<List<SaPrizeDetailResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaPrizeController.querySaPrizeDetailInfo.request{}", JSON.toJSONString(req));
            ServiceBean<List<SaPrizeDetailResBean>> result = saPrizeService.querySaPrizeDetailInfo(req.getRequest());
            logger.info("SaPrizeController.querySaPrizeDetailInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
