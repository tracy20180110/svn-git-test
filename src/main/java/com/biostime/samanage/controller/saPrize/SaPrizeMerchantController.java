package com.biostime.samanage.controller.saPrize;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.saPrize.SaPrizeDetailReqBean;
import com.biostime.samanage.bean.saPrize.SaPrizeDetailResBean;
import com.biostime.samanage.bean.saPrize.SaPrizeReqBean;
import com.biostime.samanage.bean.saPrize.SaPrizeResBean;
import com.biostime.samanage.service.saPrize.SaPrizeService;
import com.mama100.merchant.base.system.common.bean.BgmUserOfficeBean;
import com.mama100.merchant.session.OperatorInfoContext;
import com.mama100.merchant.session.bean.OperatorInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取SA育婴顾问人员实时奖金
 * Date: 2016-6-20
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/merchant/saPrize")
public class SaPrizeMerchantController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SaPrizeService saPrizeService;

    /**
     * 获取人员实时奖金列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaPrizeList.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="获取人员实时奖金",description="获取人员实时奖金",author="12804",model= ModelType.MKT,updateTime="2016-06-20")
    public @ResponseBody
    BaseResponse<List<SaPrizeResBean>> querySaPrizeList(
            @RequestBody final BaseRequest<SaPrizeReqBean> req
    ) {
        BaseResponse<List<SaPrizeResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SaPrizeReqBean srBean = req.getRequest();

            if(StringUtil.isNullOrEmpty(srBean.getOfficeCode())){
                OperatorInfo operatorInfo = OperatorInfoContext.get();
                BgmUserOfficeBean userOfficeBean = operatorInfo.getUserOffice();
                if(null != userOfficeBean) {
                    srBean.setOfficeCode(userOfficeBean.getOfficeCode());
                }
            }
            // 查询
            logger.info("SaPrizeMerchantController.querySaPrizeList.request{}", JSON.toJSONString(req));
            ServiceBean<List<SaPrizeResBean>> result = saPrizeService.querySaPrizeList(srBean);
            logger.info("SaPrizeMerchantController.querySaPrizeList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 获取人员实时奖金明细
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySaPrizeDetailInfo.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="获取人员实时奖金明细",description="获取人员实时奖金明细",author="12804",model= ModelType.MKT,updateTime="2016-06-20")
    public @ResponseBody
    BaseResponse<List<SaPrizeDetailResBean>> querySaPrizeDetailInfo(
            @RequestBody final BaseRequest<SaPrizeDetailReqBean> req
    ) {
        BaseResponse<List<SaPrizeDetailResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            Map<String,Object> map = new HashMap();
            SaPrizeDetailReqBean srBean = req.getRequest();
            OperatorInfo operatorInfo = OperatorInfoContext.get();
            if(null != operatorInfo){
                map.put("account",operatorInfo.getUser().getUserId());
                map.put("name",operatorInfo.getUser().getUserName());
                if(StringUtil.isNullOrEmpty(srBean.getAccount())){
                    srBean.setAccount(operatorInfo.getUser().getUserId());
                }
            }
            BgmUserOfficeBean userOfficeBean = operatorInfo.getUserOffice();
            if(StringUtil.isNullOrEmpty(srBean.getOfficeCode())){
                if(null != userOfficeBean) {
                    srBean.setOfficeCode(userOfficeBean.getOfficeCode());
                }
            }


            // 查询
            logger.info("SaPrizeMerchantController.querySaPrizeDetailInfo.request{}", JSON.toJSONString(req));
            ServiceBean<List<SaPrizeDetailResBean>> result = saPrizeService.querySaPrizeDetailInfo(srBean);
            logger.info("SaPrizeMerchantController.querySaPrizeDetailInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            resp.setOtherAttribute(map);
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
