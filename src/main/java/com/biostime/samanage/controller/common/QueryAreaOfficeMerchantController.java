package com.biostime.samanage.controller.common;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.common.AreaOfficeReqBean;
import com.biostime.samanage.bean.common.AreaOfficeResBean;
import com.biostime.samanage.service.common.QueryAreaOfficeService;
import com.mama100.merchant.base.system.common.bean.BgmUserOfficeBean;
import com.mama100.merchant.session.OperatorInfoContext;
import com.mama100.merchant.session.bean.OperatorInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:获取人员所属大区办事处
 * Date: 2016-6-20
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/merchant/areaOffice")
public class QueryAreaOfficeMerchantController {

    @Autowired
    private QueryAreaOfficeService queryAreaOfficeService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/queryAreaOffice.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="获取人员所属大区办事处",description="获取人员所属大区办事处",author="12804",model= ModelType.MKT,updateTime="2016-06-20")
    public @ResponseBody
    BaseResponse<List<AreaOfficeResBean>> queryByUserAreaOffice(@RequestBody final BaseRequest<AreaOfficeReqBean> req) {
        BaseResponse<List<AreaOfficeResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            AreaOfficeReqBean areaOfficeReqBean = req.getRequest();
            if(StringUtil.isNullOrEmpty(areaOfficeReqBean.getAreaOfficeCode())){
                OperatorInfo operatorInfo = OperatorInfoContext.get();
                BgmUserOfficeBean userOfficeBean = operatorInfo.getUserOffice();
                if(null != userOfficeBean ) {
                    areaOfficeReqBean.setAreaOfficeCode(userOfficeBean.getOfficeCode());
                }
            }

            // 查询
            logger.info("QueryAreaOfficeController.queryUserAreaOffice:request{}", JSON.toJSONString(req));
            ServiceBean<List<AreaOfficeResBean>> result = queryAreaOfficeService.queryUserAreaOffice(areaOfficeReqBean);
            logger.info("QueryAreaOfficeController.queryUserAreaOffice:response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
