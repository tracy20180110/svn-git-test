package com.biostime.samanage.controller.splitFlow;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.splitFlow.SaSplitFlowBean;
import com.biostime.samanage.service.splitFlow.SaSplitFlowService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SA分流报表
 */

@Controller
@RequestMapping("/splitFlow")
@Slf4j
public class SaSplitFlowController {

    @Autowired
    private SaSplitFlowService saSplitFlowService;

    /**
     * SA分流报表
     *
     * @param req
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/searchSaSplitFlow.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "searchSaSplitFlow", description = "SA客户明细报表", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<Pager<SaSplitFlowBean>> searchSaSplitFlow(
            @RequestBody final BaseRequest<SaSplitFlowBean> req, HttpServletRequest request,
            HttpServletResponse response) {
        BaseResponse<Pager<SaSplitFlowBean>> resp = new BaseResponse<Pager<SaSplitFlowBean>>(
                DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("searchSaSplitFlow:request parameters {}", JSON.toJSON(req));
            SaSplitFlowBean saSplitFlowBean = req.getRequest();
            Pager<SaSplitFlowBean> pager = new Pager<SaSplitFlowBean>(
                    saSplitFlowBean.getPageSize(), saSplitFlowBean.getPageNo());
            ServiceBean<Pager<SaSplitFlowBean>> result = saSplitFlowService.searchSaSplitFlow(pager, saSplitFlowBean);
            log.info("searchSaSplitFlow:response parameters:{}", JSON.toJSON(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 导出SA分流报表
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobilePhone
     * @param terminalCode
     * @param startTime
     * @param endTime
     * @param terminalChannelCode
     * @param chainChannelCode
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/exportSaSplitFlow.action", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "exportSaSplitFlow", description = "导出SA分流报表", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<String> exportSaSplitFlow(@RequestParam final String nursingConsultant, @RequestParam final String areaCode, @RequestParam final String officeCode, @RequestParam final String saCode, @RequestParam final String customerId, @RequestParam final String mobilePhone, @RequestParam final String terminalCode, 
            @RequestParam final String startTime, @RequestParam final String endTime, @RequestParam final String terminalChannelCode, @RequestParam final String chainChannelCode, HttpServletRequest request,
                                           HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse<String>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("exportSaSplitFlow:request parameters{}", JSON.toJSON("nursingConsultant:" + nursingConsultant));

            ServiceBean result = saSplitFlowService.exportSaSplitFlow(nursingConsultant, areaCode, officeCode, saCode, customerId, mobilePhone, terminalCode, startTime, endTime, terminalChannelCode, chainChannelCode, response);

            log.info("exportSaSplitFlow:response parameters {}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 保存SA和育婴顾问关系（给C端调用）
     *
     * @param saCode
     * @param terminalCode
     * @param customerId
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/addBstOtoCustSA.do", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "addBstOtoCustSA", description = "新增SA和育婴顾问关系", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<String> addBstOtoCustSA(@RequestParam final String saCode, @RequestParam final String terminalCode, @RequestParam final String customerId,
                                         @RequestParam final String relateCode, @RequestParam final String isNewcust, @RequestParam final String maintenanceType,
                                         @RequestParam final String sendCouponTerminal, @RequestParam final String operatorUser, HttpServletRequest request,
                                         HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse<String>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("addBstOtoCustSA:request parameters{}", JSON.toJSON("saCode:" + saCode + ";terminalCode:" + terminalCode + ";customerId:" + customerId + ";isNewcust:" + isNewcust
                    + ";maintenanceType:" + maintenanceType + ";sendCouponTerminal:" + sendCouponTerminal + ";operatorUser:" + operatorUser));

            ServiceBean result = saSplitFlowService.addBstOtoCustSA(saCode, terminalCode, customerId, relateCode, isNewcust, maintenanceType, sendCouponTerminal, operatorUser);

            log.info("addBstOtoCustSA:response parameters {}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
