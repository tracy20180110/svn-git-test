package com.biostime.samanage.controller.ftf;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.ftf.lecture.FtfLectureCodeResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerReqBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import com.biostime.samanage.service.ftf.FtfLecturerService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:FTF讲师
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/ftf/lecturer")
public class FtfLecturerController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FtfLecturerService ftfLecturerService;

    /**
     * FTF讲师列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryFtfLecturerList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="获取FTF讲师",description="FTF讲师",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse<Pager<FtfLecturerResBean>> queryFtfCourseList(@RequestBody final BaseRequest<FtfLecturerReqBean> req) {
        FtfLecturerReqBean reqBean = req.getRequest();
        BaseResponse<Pager<FtfLecturerResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfLecturerController.queryFtfLecturerList.request{}", JSON.toJSONString(req));

            Pager<FtfLecturerResBean> pager = new Pager<FtfLecturerResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<FtfLecturerResBean>> result = ftfLecturerService.queryFtfLecturerList(pager, reqBean);

            logger.info("FtfLecturerController.queryFtfLecturerList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF讲师列表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportFtfLecturerList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="FTF讲师列表导出",description="FTF讲师列表导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportFtfLecturerList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            FtfLecturerReqBean reqBean = MapperUtils.reqParameToBean(request, FtfLecturerReqBean.class);

            // 查询
            logger.info("FtfLecturerController.exportFtfLecturerList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = ftfLecturerService.exportFtfLecturerList(response, reqBean);
            logger.info("FtfLecturerController.exportFtfLecturerList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF讲师--状态修改
     * @param req
     * @return
     */
    @RequestMapping(value = "/changeStatusFtfLecturer.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="FTF讲师状态修改",description="FTF讲师状态修改",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse changeStatusFtfLecturer(@RequestBody final BaseRequest<FtfLecturerReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("FtfLecturerController.changeStatusFtfLecturer.request{}", JSON.toJSONString(req));
            ServiceBean result = ftfLecturerService.changeStatusFtfLecturer(req.getRequest());
            logger.info("FtfLecturerController.changeStatusFtfLecturer.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF讲师--编辑、新增
     * @return
     */
    @RequestMapping(value = "/saveFtfLecturer.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="FTF讲师编辑、新增",description="FTF讲师编辑、新增",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse saveFtfLecturer(@RequestBody final BaseRequest<FtfLecturerReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("FtfLecturerController.saveFtfLecturer.request{}", JSON.toJSONString(req));
            ServiceBean result = ftfLecturerService.saveFtfLecturer(req.getRequest());
            logger.info("FtfLecturerController.saveFtfLecturer.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 获取讲师名称工号
     * @return
     */
    @RequestMapping(value = "/queryLecturerCode.action", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "获取讲师名称工号", description = "获取讲师名称工号", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<List<FtfLectureCodeResBean>> queryLecturerCode(String keyword,String type) {
        BaseResponse<List<FtfLectureCodeResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfLecturerController.queryLecturerCode.request{}", JSON.toJSONString(keyword));
            ServiceBean<List<FtfLectureCodeResBean>> result = ftfLecturerService.queryLecturerCode(keyword,type);
            logger.info("FtfLecturerController.queryLecturerCode.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 讲师认证-导入
     * @param request
     * @return
     */
    @RequestMapping(value = "/importLecturerCheck.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="讲师认证导入(POST请求)",description="讲师认证导入",author="12804",model= ModelType.MKT,updateTime="2017-08-18")
    public @ResponseBody
    BaseResponse<String> importLecturerCheck(
            @RequestParam(value = "file", required = false) MultipartFile multipartFile,
            HttpServletRequest request) {

        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            FtfLecturerReqBean reqBean =  MapperUtils.reqParameToBean(request, FtfLecturerReqBean.class);

            logger.info("FtfLecturerController.importLecturerCheck.request{}", JSON.toJSONString(reqBean));
            ServiceBean<String> result = ftfLecturerService.importLecturerCheck(multipartFile, reqBean);
            logger.info("FtfLecturerController.importLecturerCheck.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getResponse());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
