package com.biostime.samanage.controller.ftf;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.ftf.sign.FtfSignUpDetailReqBean;
import com.biostime.samanage.bean.ftf.sign.FtfSignUpDetailResBean;
import com.biostime.samanage.service.ftf.FtfSignUpDetailService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:FTF活动报名信息查询
 * Date: 2017-01-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/ftf/singUp")
public class FtfSignUpDetailController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FtfSignUpDetailService ftfSignUpDetailService;

    /**
     * FTF活动报名信息查询
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryFtfSignUpDetailList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="FTF活动报名信息查询",description="FTF活动报名信息查询",author="12804",model= ModelType.MKT,updateTime="2017-01-25")
    public @ResponseBody
    BaseResponse<Pager<FtfSignUpDetailResBean>> queryFtfSignUpDetailList(@RequestBody final BaseRequest<FtfSignUpDetailReqBean> req) {
        FtfSignUpDetailReqBean reqBean = req.getRequest();
        BaseResponse<Pager<FtfSignUpDetailResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfSignUpDetailController.queryFtfSignUpDetailList.request{}", JSON.toJSONString(req));
            Pager<FtfSignUpDetailResBean> pager = new Pager<FtfSignUpDetailResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<FtfSignUpDetailResBean>> result = ftfSignUpDetailService.queryFtfSignUpDetailList(pager, reqBean);
            logger.info("FtfSignUpDetailController.queryFtfSignUpDetailList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF活动报名信息--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportFtfSignUpDetailList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="FTF活动报名信息导出",description="FTF活动报名信息导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportFtfSignUpDetailList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            FtfSignUpDetailReqBean reqBean = MapperUtils.reqParameToBean(request, FtfSignUpDetailReqBean.class);

            // 查询
            logger.info("FtfSignUpDetailController.exportFtfSignUpDetailList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = ftfSignUpDetailService.exportFtfSignUpDetailList(response, reqBean);
            logger.info("FtfSignUpDetailController.exportFtfSignUpDetailList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
