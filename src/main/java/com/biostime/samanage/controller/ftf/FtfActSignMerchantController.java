package com.biostime.samanage.controller.ftf;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.actreport.ActRePortListResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityReqBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoReqBean;
import com.biostime.samanage.bean.ftf.app.AccountInfoResBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumReqBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import com.biostime.samanage.bean.ftf.sign.FtfActSignInfoReqBean;
import com.biostime.samanage.bean.ftf.sign.FtfActSignListBean;
import com.biostime.samanage.bean.ftf.sign.FtfActSignReqBean;
import com.biostime.samanage.bean.ftf.sign.FtfActSignResBean;
import com.biostime.samanage.check.ftf.FtfActSignCheck;
import com.biostime.samanage.domain.ftf.FtfActivity;
import com.biostime.samanage.service.ftf.FtfActSignService;
import com.biostime.samanage.service.ftf.FtfActivityService;
import com.biostime.samanage.service.ftf.FtfCurriculumService;
import com.mama100.merchant.session.OperatorInfoContext;
import com.mama100.merchant.session.bean.OperatorInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动签到
 * Date: 2016-12-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/merchant/ftfact")
public class FtfActSignMerchantController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FtfActSignService ftfActSignService;

    @Autowired
    private FtfActivityService ftfActivityService;

    @Autowired
    private FtfCurriculumService ftfCurriculumService;

    /**
     * 获取活动列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryMobFtfActList.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="获取活动列表",description="获取活动列表",author="12804",model= ModelType.MKT,updateTime="2016-12-08")
    public @ResponseBody
    BaseResponse<List<FtfActSignResBean>> queryMobFtfActList(
            @RequestBody final BaseRequest<FtfActSignReqBean> req
    ) {
        BaseResponse<List<FtfActSignResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            /*FtfActSignReqBean srBean = req.getRequest();

            if(StringUtil.isNullOrEmpty(srBean.getOfficeCode())){
                OperatorInfo operatorInfo = OperatorInfoContext.get();
                BgmUserOfficeBean userOfficeBean = operatorInfo.getUserOffice();
                if(null != userOfficeBean) {
                    srBean.setOfficeCode(userOfficeBean.getOfficeCode());
                }
            }*/
            // 查询
            logger.info("FtfActSignMerchantController.queryMobFtfActList.request{}", JSON.toJSONString(req.getRequest()));
            ServiceBean<List<FtfActSignResBean>> result = ftfActSignService.queryMobFtfActList(req.getRequest());
            logger.info("FtfActSignMerchantController.queryMobFtfActList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 签到
     * @return
     */
    @RequestMapping(value = "/saveMobSign.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="签到",description="签到",author="12804",model= ModelType.MKT,updateTime="2016-10-10")
    public @ResponseBody
    BaseResponse saveMobSign(@RequestBody final BaseRequest<FtfActSignInfoReqBean> reqBean) {
        FtfActSignInfoReqBean fsiReqBeans = reqBean.getRequest();
        fsiReqBeans.setSeqNo(reqBean.getSeqNo());
        fsiReqBeans.setSourceSystem("HYT");
        BaseResponse resp = FtfActSignCheck.saveMobSign(fsiReqBeans);
        if (!resp.isOk()) {
            return resp;
        }
        try{
            OperatorInfo operatorInfo = OperatorInfoContext.get();
            fsiReqBeans.setUserId(operatorInfo.getUser().getUserId());
            /*String deviceId = operatorInfo.getMobDevice().getDeviceId();
            if(HytMobDevice.OS_TYPE_NAME_IOS.equalsIgnoreCase(operatorInfo.getMobDevice().getOs())){
                deviceId = deviceId.substring(0,13);
            }
            arReqBeans.setDeviceId(deviceId);
            Long operatorType = operatorInfo.getUser().getPost();
            if(null == operatorType){
                arReqBeans.setOperatorType("0"); //职位ID
            }else{
                arReqBeans.setOperatorType(String.valueOf(operatorType)); //职位ID
            }
            System.out.println("arReqBeans.getOperatorType()-----"+arReqBeans.getOperatorType());
            //获取登陆所属大区办事处
            BgmUserOfficeBean userOfficeBean = operatorInfo.getUserOffice();
            if(null != userOfficeBean) {
                if(StringUtil.isNullOrEmpty(arReqBeans.getOfficeCode())){
                    arReqBeans.setOfficeCode(userOfficeBean.getOfficeCode());
                }
            }*/

            // 查询
            logger.info("FtfActSignMerchantController.saveMobSign.request{}", JSON.toJSONString(fsiReqBeans));
            ServiceBean result = ftfActSignService.saveMobSign(fsiReqBeans);
            logger.info("FtfActSignMerchantController.saveMobSign.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 签到详情
     * @param actId
     * @return
     */
    @RequestMapping(value = "/querySignInfoList.do", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-0.0.1",name="签到详情",description="签到详情",author="12804",model= ModelType.MKT,updateTime="2016-12-08")
    public @ResponseBody
    BaseResponse<FtfActSignListBean> querySignInfoList(String actId,String createdBy) {
        BaseResponse<FtfActSignListBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfActSignMerchantController.querySignInfoList.request{}", JSON.toJSONString(actId));
            ServiceBean<FtfActSignListBean> result = ftfActSignService.querySignInfoList(actId,createdBy);
            logger.info("FtfActSignMerchantController.querySignInfoList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 上传销量
     * @return
     */
    @RequestMapping(value = "/saveMobSignReportSales.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="上传销量",description="上传销量",author="12804",model= ModelType.MKT,updateTime="2016-10-10")
    public @ResponseBody
    BaseResponse saveMobSignReportSales(@RequestBody final BaseRequest<FtfActSignInfoReqBean> reqBean) {
        FtfActSignInfoReqBean fsiReqBeans = reqBean.getRequest();
        BaseResponse resp = FtfActSignCheck.saveMobSign(fsiReqBeans);
        if (!resp.isOk()) {
            return resp;
        }
        try{
            /*OperatorInfo operatorInfo = OperatorInfoContext.get();
            fsiReqBeans.setUserId(operatorInfo.getUser().getUserId());*/

            // 查询
            logger.info("FtfActSignMerchantController.saveMobSignReportSales.request{}", JSON.toJSONString(fsiReqBeans));
            ServiceBean result = ftfActSignService.saveMobSignReportSales(fsiReqBeans);
            logger.info("FtfActSignMerchantController.saveMobSignReportSales.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询登陆人所属的SA门店(app中FTF活动备案)
     * @return
     */
    @RequestMapping(value = "/querySaTerminal.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询登陆人所属的SA门店", description = "查询登陆人所属的SA门店", author = "12804", model = ModelType.MKT, updateTime = "2020-10-26")
    public
    @ResponseBody
    BaseResponse<List<BaseKeyValueBean>> querySaTerminal(@RequestBody final BaseRequest<BaseKeyValueBean> reqBean) {
        BaseResponse<List<BaseKeyValueBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            logger.info("FtfActSignMerchantController.querySaTerminal.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<BaseKeyValueBean>> result = ftfActSignService.querySaTerminal(reqBean.getRequest());
            logger.info("FtfActSignMerchantController.querySaTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 保存活动备案信息(app中FTF活动备案)
     * @return
     */
    @RequestMapping(value = "/saveAppFtfActivity.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "保存活动备案信息", description = "保存活动备案信息", author = "12804", model = ModelType.MKT, updateTime = "2020-10-26")
    public @ResponseBody
    BaseResponse<FtfActivity> saveAppFtfActivity(@RequestBody final BaseRequest<FtfActivityReqBean> req, HttpServletRequest request) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {
            FtfActivityReqBean ftfActivityReqBean  = req.getRequest();
            // 查询
            logger.info("FtfActSignMerchantController.saveFtfActivity.request{}", JSON.toJSONString(req));
            ServiceBean<FtfActivity> result = ftfActivityService.saveFtfActivity(ftfActivityReqBean);
            logger.info("FtfActSignMerchantController.saveFtfActivity.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF讲师列表
     * @return
     */
    @RequestMapping(value = "/queryLecturerList.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="FTF讲师列表APP",description="FTF讲师列表APP",author="12804",model= ModelType.MKT,updateTime="2020-07-25")
    public @ResponseBody
    BaseResponse<List<FtfLecturerResBean>> queryLecturerList(@RequestBody final BaseRequest<FtfActivityReqBean> req) {
        BaseResponse<List<FtfLecturerResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfActSignMerchantController.queryLecturerList:request{}", JSON.toJSONString(req));
            ServiceBean<List<FtfLecturerResBean>> result = ftfActivityService.queryAppLecturerList(req.getRequest());
            logger.info("FtfActSignMerchantController.queryLecturerList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF课程--查询
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryFtfCurriculumList.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="FTF课程列表",description="FTF课程列表",author="12804",model= ModelType.MKT,updateTime="2016-11-25")
    public @ResponseBody
    BaseResponse<Pager<FtfCurriculumResBean>> queryFtfCurrdoiculumList(@RequestBody final BaseRequest<FtfCurriculumReqBean> req) {
        FtfCurriculumReqBean reqBean = req.getRequest();
        BaseResponse<Pager<FtfCurriculumResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfCurriculumController.queryFtfCurriculumList:request{}", JSON.toJSONString(req));
            Pager<FtfCurriculumResBean> pager = new Pager<FtfCurriculumResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<FtfCurriculumResBean>> result = ftfCurriculumService.queryFtfCurriculumList(pager,req.getRequest());
            logger.info("FtfCurriculumController.queryFtfCurriculumList:response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询门店是否存在
     * @return
     */
    @RequestMapping(value = "/queryTerminal.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询门店是否存在", description = "查询门店是否存在", author = "12804", model = ModelType.MKT, updateTime = "2017-04-24")
    public
    @ResponseBody
    BaseResponse<ActRePortListResBean> queryTerminal(String terminalCode) {
        BaseResponse<ActRePortListResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfActSignMerchantController.queryTerminal.request{}", JSON.toJSONString(terminalCode));
            ServiceBean<ActRePortListResBean> result = ftfActivityService.queryTerminal(terminalCode);
            logger.info("FtfActSignMerchantController.queryTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 上报中的人员列表
     * @return
     */
    @RequestMapping(value = "/queryAppReportAccountInfoList.do", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="上报中的人员列表",description="上报中的人员列表",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse<List<AccountInfoResBean>> queryAppReportAccountInfoList(@RequestBody final BaseRequest<AccountInfoReqBean> req) {
        BaseResponse<List<AccountInfoResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            ServiceBean<List<AccountInfoResBean>> result = ftfActivityService.queryAppReportAccountInfoList(req.getRequest());
            logger.info("FtfActSignMerchantController.queryFtfActivityList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

}
