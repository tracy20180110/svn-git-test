package com.biostime.samanage.controller.ftf;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.actreport.ActRePortListResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityReqBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityResBean;
import com.biostime.samanage.bean.ftf.activity.FtfActivityTemplateResBean;
import com.biostime.samanage.bean.ftf.lecture.FtfLecturerResBean;
import com.biostime.samanage.domain.ftf.FtfActivity;
import com.biostime.samanage.service.ftf.FtfActivityService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:FTF活动配置
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/ftf/activity")
public class FtfActivityController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FtfActivityService ftfActivityService;

    /**
     * FTF活动配置列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryFtfActivityList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="FTF活动列表",description="FTF活动列表",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse<Pager<FtfActivityResBean>> queryFtfActivityList(@RequestBody final BaseRequest<FtfActivityReqBean> req) {
        FtfActivityReqBean reqBean = req.getRequest();
        BaseResponse<Pager<FtfActivityResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfActivityController.queryFtfActivityList.request{}", JSON.toJSONString(req));
            Pager<FtfActivityResBean> pager = new Pager<FtfActivityResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<FtfActivityResBean>> result = ftfActivityService.queryFtfActivityList(pager, reqBean);
            logger.info("FtfActivityController.queryFtfActivityList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF活动列表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportFtfActivityList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="FTF课程列表导出",description="FTF课程列表导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportFtfActivityList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            FtfActivityReqBean reqBean = MapperUtils.reqParameToBean(request, FtfActivityReqBean.class);

            // 查询
            logger.info("FtfActivityController.exportFtfActivityList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = ftfActivityService.exportFtfActivityList(response, reqBean);
            logger.info("FtfActivityController.exportFtfActivityList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF活动--状态修改
     * @param req
     * @return
     */
    @RequestMapping(value = "/changeStatusFtfActivity.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="FTF课程状态修改",description="FTF课程状态修改",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse changeStatusFtfActivity(@RequestBody final BaseRequest<FtfActivityReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("FtfActivityController.changeStatusFtfActivity.request{}", JSON.toJSONString(req));
            ServiceBean result = ftfActivityService.changeStatusFtfActivity(req.getRequest());
            logger.info("FtfActivityController.changeStatusFtfActivity.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF活动--编辑、新增
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    @RequestMapping(value = "/saveFtfActivity.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="FTF课程编辑、新增",description="FTF课程编辑、新增",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse<FtfActivity> saveFtfActivity(@RequestBody final BaseRequest<FtfActivityReqBean> req, HttpServletRequest request) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {
            FtfActivityReqBean ftfActivityReqBean  = req.getRequest();
//            ftfActivityReqBean.setProjectName(request.getContextPath());
            // 查询
            logger.info("FtfActivityController.saveFtfActivity.request{}", JSON.toJSONString(req));
            ServiceBean<FtfActivity> result = ftfActivityService.saveFtfActivity(ftfActivityReqBean);
            logger.info("FtfActivityController.saveFtfActivity.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 获取SA对应的门店
     * @return
     */
    @RequestMapping(value = "/querySaTerminal.action", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "获取SA对应的门店", description = "获取SA对应的门店", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<ActRePortListResBean> querySaTerminal(String saCode) {
        BaseResponse<ActRePortListResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfActivityController.querySaTerminal.request{}", JSON.toJSONString(saCode));
            ServiceBean<ActRePortListResBean> result = ftfActivityService.querySaTerminal(saCode);
            logger.info("FtfActivityController.querySaTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 查询门店是否存在
     * @return
     */
    @RequestMapping(value = "/queryTerminal.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询门店是否存在", description = "查询门店是否存在", author = "12804", model = ModelType.MKT, updateTime = "2017-04-24")
    public
    @ResponseBody
    BaseResponse<ActRePortListResBean> queryTerminal(String terminalCode) {
        BaseResponse<ActRePortListResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfActivityController.queryTerminal.request{}", JSON.toJSONString(terminalCode));
            ServiceBean<ActRePortListResBean> result = ftfActivityService.queryTerminal(terminalCode);
            logger.info("FtfActivityController.queryTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询导购ID是否存在
     * @return
     */
    @RequestMapping(value = "/queryShoppingGuideId.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询导购ID是否存在", description = "查询导购ID是否存在", author = "12804", model = ModelType.MKT, updateTime = "2017-04-24")
    public
    @ResponseBody
    BaseResponse queryShoppingGuideId(String shoppingGuideId) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfActivityController.queryShoppingGuideId.request{}", JSON.toJSONString(shoppingGuideId));
            ServiceBean result = ftfActivityService.queryShoppingGuideId(shoppingGuideId);
            logger.info("FtfActivityController.queryShoppingGuideId.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF活动--FTF海报分享链接更改
     * @param req
     * @return
     */
    @RequestMapping(value = "/changeImageFtfActivity.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="FTF海报分享链接更改",description="FTF海报分享链接更改",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse changeImageFtfActivity(@RequestBody final BaseRequest<FtfActivityReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("FtfActivityController.changeImageFtfActivity.request{}", JSON.toJSONString(req));
            ServiceBean result = ftfActivityService.changeImageFtfActivity(req.getRequest());
            logger.info("FtfActivityController.changeImageFtfActivity.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF活动模板列表
     * @return
     */
    @RequestMapping(value = "/queryFtfActivityTemplateList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="FTF活动模板列表",description="FTF活动模板列表",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse<List<FtfActivityTemplateResBean>> queryFtfActivityTemplateList() {
        BaseResponse<List<FtfActivityTemplateResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            ServiceBean<List<FtfActivityTemplateResBean>> result = ftfActivityService.queryFtfActivityTemplateList();
            logger.info("FtfActivityController.queryFtfActivityList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF讲师列表
     * @return
     */
    @RequestMapping(value = "/queryLecturerList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="FTF讲师列表",description="FTF讲师列表",author="12804",model= ModelType.MKT,updateTime="2016-12-25")
    public @ResponseBody
    BaseResponse<List<FtfLecturerResBean>> queryLecturerList(@RequestBody final BaseRequest<FtfActivityReqBean> req) {
        BaseResponse<List<FtfLecturerResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            ServiceBean<List<FtfLecturerResBean>> result = ftfActivityService.queryLecturerList(req.getRequest());
            logger.info("FtfActivityController.queryFtfActivityList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

}
