package com.biostime.samanage.controller.ftf;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.ftf.Bnc6Product;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumReqBean;
import com.biostime.samanage.bean.ftf.curriculum.FtfCurriculumResBean;
import com.biostime.samanage.service.ftf.FtfCurriculumService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Describe:FTF课程
 * Date: 2016-11-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/ftf/curriculum")
public class FtfCurriculumController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FtfCurriculumService ftfCurriculumService;

    /**
     * FTF课程--查询
     * @param req
     * @return
     */
    @RequestMapping(value = "/queryFtfCurriculumList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="FTF课程列表",description="FTF课程列表",author="12804",model= ModelType.MKT,updateTime="2016-11-25")
    public @ResponseBody
    BaseResponse<Pager<FtfCurriculumResBean>> queryFtfCurriculumList(@RequestBody final BaseRequest<FtfCurriculumReqBean> req) {
        FtfCurriculumReqBean reqBean = req.getRequest();
        BaseResponse<Pager<FtfCurriculumResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("FtfCurriculumController.queryFtfCurriculumList:request{}", JSON.toJSONString(req));
            Pager<FtfCurriculumResBean> pager = new Pager<FtfCurriculumResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<FtfCurriculumResBean>> result = ftfCurriculumService.queryFtfCurriculumList(pager,req.getRequest());
            logger.info("FtfCurriculumController.queryFtfCurriculumList:response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF课程列表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportFtfCurriculumList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="FTF课程列表导出",description="FTF课程列表导出",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse exportFtfCurriculumList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            FtfCurriculumReqBean reqBean = MapperUtils.reqParameToBean(request, FtfCurriculumReqBean.class);

            // 查询
            logger.info("FtfCurriculumController.exportFtfCurriculumList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = ftfCurriculumService.exportFtfCurriculumList(response, reqBean);
            logger.info("FtfCurriculumController.exportFtfCurriculumList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF课程--状态修改
     * @param req
     * @return
     */
    @RequestMapping(value = "/changeStatusFtfCurriculum.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="FTF课程状态修改",description="FTF课程状态修改",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse changeStatusFtfCurriculum(@RequestBody final BaseRequest<FtfCurriculumReqBean> req) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("FtfCurriculumController.changeStatusFtfCurriculum.request{}", JSON.toJSONString(req));
            ServiceBean result = ftfCurriculumService.changeStatusFtfCurriculum(req.getRequest());
            logger.info("FtfCurriculumController.changeStatusFtfCurriculum.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * FTF课程--编辑、新增
     * @return
     */
    @RequestMapping(value = "/saveFtfCurriculum.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-1.2.0",name="FTF课程编辑、新增",description="FTF课程编辑、新增",author="12804",model= ModelType.MKT,updateTime="2016-08-18")
    public @ResponseBody
    BaseResponse saveFtfCurriculum(@RequestBody final BaseRequest<FtfCurriculumReqBean> req,HttpServletRequest request) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());

        if (!resp.isOk()) {
            return resp;
        }
        try {
            FtfCurriculumReqBean fcrBean  = req.getRequest();
            fcrBean.setProjectName(request.getContextPath());
            // 查询
            logger.info("FtfCurriculumController.saveFtfCurriculum.request{}", JSON.toJSONString(req));
            ServiceBean result = ftfCurriculumService.saveFtfCurriculum(req.getRequest());
            logger.info("FtfCurriculumController.saveFtfCurriculum.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询BNC6品商品
     * @return
     */
    @RequestMapping(value = "/queryLecturerCode.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询BNC6品商品", description = "查询BNC6品商品", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<List<Bnc6Product>> queryBnc6Product() {
        BaseResponse<List<Bnc6Product>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            ServiceBean<List<Bnc6Product>> result = ftfCurriculumService.queryBnc6Product();
            logger.info("FtfCurriculumController.queryBnc6Product.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
