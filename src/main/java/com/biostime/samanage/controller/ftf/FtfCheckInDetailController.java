package com.biostime.samanage.controller.ftf;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInAppDetailResBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInDetailReqBean;
import com.biostime.samanage.bean.ftf.checkIn.FtfCheckInDetailResBean;
import com.biostime.samanage.service.ftf.FtfCheckInDetailService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:FTF签到信息查询 Date: 2017-01-25 Time: 15:44 User: 12804 Version:1.0
 */
@Controller
@RequestMapping("/ftf/checkIn")
public class FtfCheckInDetailController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FtfCheckInDetailService ftfCheckInDetailService;

	/**
	 * FTF活动签到信息查询
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/queryFtfCheckInDetailList.action", method = { RequestMethod.POST })
	@AutoDocMethod(version = "v-0.0.1", name = "FTF签到信息查询", description = "FTF签到信息查询", author = "12804", model = ModelType.MKT, updateTime = "2017-01-25")
	public @ResponseBody BaseResponse<Pager<FtfCheckInDetailResBean>> queryFtfCheckInDetailList(
			@RequestBody final BaseRequest<FtfCheckInDetailReqBean> req) {
		FtfCheckInDetailReqBean reqBean = req.getRequest();
		BaseResponse<Pager<FtfCheckInDetailResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
		if (!resp.isOk()) {
			return resp;
		}
		try {
			// 查询
			logger.info("FtfCheckInDetailController.queryFtfCheckInDetailList.request{}", JSON.toJSONString(req));
			Pager<FtfCheckInDetailResBean> pager = new Pager<FtfCheckInDetailResBean>(reqBean.getPageSize(),
					reqBean.getPageNo());
			ServiceBean<Pager<FtfCheckInDetailResBean>> result = ftfCheckInDetailService
					.queryFtfCheckInDetailList(pager, reqBean);
			logger.info("FtfCheckInDetailController.queryFtfCheckInDetailList.response{}", JSON.toJSONString(result));
			resp.setResponse(result.getResponse());
			resp.setCode(result.getCode());
			resp.setDesc(result.getDesc());
			ExceptionUtils.setError(resp);
		} catch (Exception ex) {
			ExceptionUtils.setSystemError(resp, ex.getMessage());
			logger.error(ex.getMessage(), ex);
		}
		return resp;
	}

	/**
	 * FTF活动签到信息App端查询显示
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/queryFtfCheckInAppDetailList.action", method = { RequestMethod.POST })
	@AutoDocMethod(version = "v-0.0.1", name = "FTF活动签到信息App端查询显示", description = "FTF活动签到信息App端查询显示", author = "w1038", model = ModelType.MKT, updateTime = "2018-01-18")
	public @ResponseBody BaseResponse<FtfCheckInAppDetailResBean> queryFtfCheckInAppDetailList(
			@RequestBody final BaseRequest<FtfCheckInDetailReqBean> req) {
		FtfCheckInDetailReqBean reqBean = req.getRequest();
		BaseResponse<FtfCheckInAppDetailResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
		if (!resp.isOk()) {
			return resp;
		}
		try {
			logger.info("FtfCheckInDetailController.queryFtfCheckInAppDetailList.request{}", JSON.toJSONString(req));
			ServiceBean<FtfCheckInAppDetailResBean> result = ftfCheckInDetailService
					.queryFtfCheckInAppDetailList(reqBean);
			logger.info("FtfCheckInDetailController.queryFtfCheckInAppDetailList.response{}",
					JSON.toJSONString(result));
			resp.setResponse(result.getResponse());
			resp.setCode(result.getCode());
			resp.setDesc(result.getDesc());
			ExceptionUtils.setError(resp);
		} catch (Exception ex) {
			ExceptionUtils.setSystemError(resp, ex.getMessage());
			logger.error(ex.getMessage(), ex);
		}
		return resp;
	}

	/**
	 * FTF活动签到领取奖品
	 * @param reqBean
	 * @return
	 */
	@RequestMapping(value = "/receiveFtfCheckInAppPrizes.action", method = { RequestMethod.POST })
	@AutoDocMethod(version = "v-0.0.1", name = "FTF活动签到奖品领取", description = "FTF活动签到奖品领取", author = "w1038", model = ModelType.MKT, updateTime = "2018-01-24")
	public @ResponseBody BaseResponse receiveFtfCheckInAppPrizes(
			@RequestBody final BaseRequest<FtfCheckInDetailReqBean> reqBean) {
		BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
		try {
			logger.info("FtfCheckInDetailController.receiveFtfCheckInAppPrizes.request{}", JSON.toJSONString(reqBean));
			ServiceBean<String> serviceBean = ftfCheckInDetailService.receiveFtfCheckInAppPrizes(reqBean.getRequest());
			logger.info("FtfCheckInDetailController.receiveFtfCheckInAppPrizes.response{}",
					JSON.toJSONString(serviceBean));
			resp.setCode(serviceBean.getCode());
			resp.setDesc(serviceBean.getDesc());
			ExceptionUtils.setError(resp);
		} catch (Exception ex) {
			ExceptionUtils.setSystemError(resp, ex.getMessage());
			logger.error(ex.getMessage(), ex);
		}
		return resp;
	}

	/**
	 * FTF活动签到信息--导出
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/exportFtfCheckInDetailList.action", method = { RequestMethod.GET })
	@AutoDocMethod(version = "v-1.2.0", name = "FTF活动签到信息导出", description = "FTF活动签到信息导出", author = "12804", model = ModelType.MKT, updateTime = "2016-08-18")
	public @ResponseBody BaseResponse exportFtfCheckInDetailList(HttpServletRequest request,
			HttpServletResponse response) {
		BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
		if (!resp.isOk()) {
			return resp;
		}
		try {
			FtfCheckInDetailReqBean reqBean = MapperUtils.reqParameToBean(request, FtfCheckInDetailReqBean.class);

			// 查询
			logger.info("FtfCheckInDetailController.exportFtfCheckInDetailList.request{}", JSON.toJSONString(reqBean));
			ServiceBean result = ftfCheckInDetailService.exportFtfCheckInDetailList(response, reqBean);
			logger.info("FtfCheckInDetailController.exportFtfCheckInDetailList.response{}", JSON.toJSONString(result));
			resp.setCode(result.getCode());
			resp.setDesc(result.getDesc());
			ExceptionUtils.setError(resp);
		} catch (Exception ex) {
			ExceptionUtils.setSystemError(resp, ex.getMessage());
			logger.error(ex.getMessage(), ex);
		}
		return resp;
	}

}
