package com.biostime.samanage.controller.customer;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.customer.CustomerDetailBean;
import com.biostime.samanage.service.customer.SaCustomerDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SA品项新客报表
 */

@Controller
@RequestMapping("/customerDetail")
@Slf4j
public class SaCustomerDetailController {

    @Autowired
    private SaCustomerDetailService saCustomerDetailService;

    /**
     * SA品项新客报表（2020-01菜单已下线，使用新的菜单SaProductItemNewCustomerController）
     *
     * @param req
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/searchSaCustomerDetail.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "searchSaCustomerDetail", description = "SA客户明细报表", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<Pager<CustomerDetailBean>> searchSaCustomerDetail(
            @RequestBody final BaseRequest<CustomerDetailBean> req, HttpServletRequest request,
            HttpServletResponse response) {
        BaseResponse<Pager<CustomerDetailBean>> resp = new BaseResponse<Pager<CustomerDetailBean>>(
                DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("searchSaCustomerDetail:request parameters {}", JSON.toJSON(req));
            CustomerDetailBean customerDetailBean = req.getRequest();
            Pager<CustomerDetailBean> pager = new Pager<CustomerDetailBean>(
                    customerDetailBean.getPageSize(), customerDetailBean.getPageNo());
            ServiceBean<Pager<CustomerDetailBean>> result = saCustomerDetailService.searchSaCustomerDetail(pager, customerDetailBean);
            log.info("searchSaCustomerDetail:response parameters:{}", JSON.toJSON(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 导出SA品项新客报表
     *
     * @param nursingConsultant
     * @param areaCode
     * @param officeCode
     * @param saCode
     * @param customerId
     * @param mobile
     * @param srcLocName
     * @param recSrc
     * @param startTime
     * @param endTime
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/exportSaCustomerDetail.action", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "exportSaCustomerDetail", description = "导出SA客户明细", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<String> exportSaCustomerDetail(@RequestParam final String nursingConsultant, @RequestParam final String areaCode, @RequestParam final String officeCode, @RequestParam final String saCode, @RequestParam final String customerId, @RequestParam final String mobile, @RequestParam final String proBcc, @RequestParam final String srcLocName, @RequestParam final String recSrc, @RequestParam final String startTime, @RequestParam final String endTime, @RequestParam final String stage,@RequestParam final String buCode, @RequestParam final String parentingInstructor, HttpServletRequest request,
                                                HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse<String>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("exportSaCustomerDetail:request parameters{}", JSON.toJSON("nursingConsultant:" + nursingConsultant + ",areaCode:" + areaCode + ",officeCode:" + officeCode + ",saCode:" + saCode
                    + ",customerId:" + customerId + ",mobile:" + mobile + ",proBcc:" + proBcc + ",srcLocName:" + srcLocName
                    + ",recSrc:" + recSrc + ",startTime:" + startTime + ",endTime:" + endTime + ",stage:" + stage + ",parentingInstructor:" + parentingInstructor+ ",buCode:" + buCode));

            ServiceBean result = saCustomerDetailService.exportSaCustomerDetail(nursingConsultant, areaCode, officeCode, saCode, customerId, mobile, proBcc, srcLocName, recSrc, startTime, endTime, stage, buCode,parentingInstructor, response);

            log.info("exportSaCustomerDetail:response parameters {}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
