package com.biostime.samanage.controller.saPoint;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.saPoint.PageSaSalePointsBean;
import com.biostime.samanage.bean.saPoint.SaSalePointsBean;
import com.biostime.samanage.service.saPoint.SaSalePointsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SA销售积分
 */

@Controller
@RequestMapping("/salePoints")
@Slf4j
public class SaSalePointsController {

    @Autowired
    private SaSalePointsService saSalePointsService;

    /**
     * SA销售积分报表
     *
     * @param req
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/searchSaSalePoints.action", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "searchSaSalePoints", description = "SA销售积分报表", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<Pager<SaSalePointsBean>> searchSaSalePoints(
            @RequestBody final BaseRequest<PageSaSalePointsBean> req, HttpServletRequest request,
            HttpServletResponse response) {
        BaseResponse<Pager<SaSalePointsBean>> resp = new BaseResponse<Pager<SaSalePointsBean>>(
                DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("searchSaSalePoints:request parameters {}", JSON.toJSON(req));
            PageSaSalePointsBean pageSaSalePointsBean = req.getRequest();
            Pager<SaSalePointsBean> pager = new Pager<SaSalePointsBean>(
                    pageSaSalePointsBean.getPageSize(), pageSaSalePointsBean.getPageNo());
            ServiceBean<Pager<SaSalePointsBean>> result = saSalePointsService.searchSaSalePoints(pager, pageSaSalePointsBean.getSaSalePointsBean());
            log.info("searchSaSalePoints:response parameters:{}", JSON.toJSON(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 导出SA销售积分
     *
     * @param babyConsultant
     * @param areaCode
     * @param officeCode
     * @param startDate
     * @param endDate
     * @param bccCode
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/exportSaSalePoints.action", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "exportSaSalePoints", description = "导出SA销售积分", author = "w1014", model = ModelType.MKT, updateTime = "2016-6-23 12:30:33")
    public
    @ResponseBody
    BaseResponse<String> exportSaSalePoints(@RequestParam final String babyConsultant, @RequestParam final String areaCode, @RequestParam final String officeCode, @RequestParam final String startDate, @RequestParam final String endDate, @RequestParam final String bccCode,@RequestParam final String buCode, HttpServletRequest request,
                                             HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse<String>(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            log.info("exportSaSalePoints:request parameters{}", JSON.toJSON("babyConsultant:" + babyConsultant));

            ServiceBean result = saSalePointsService.exportSaSalePoints(babyConsultant, areaCode, officeCode, startDate, endDate, bccCode,buCode, response);

            log.info("exportSaSalePoints:response parameters {}", JSON.toJSON(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            log.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
