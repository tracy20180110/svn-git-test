package com.biostime.samanage.controller.external;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.caravan.CaravanReqBean;
import com.biostime.samanage.service.single.CaravanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Describe:SA大小篷车(外部)
 * Date: 2016-12-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/sa/external/caravan")
public class SaCaravanExternalController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private CaravanService caravanService;

    /**
     * 更新大小篷车OA流程审批状态
     * @return
     */
    @RequestMapping(value = "/editCaravanOaAuditStatus", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "更新大小篷车OA流程审批状态", description = "更新大小篷车OA流程审批状态", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse editCaravanOaAuditStatus(CaravanReqBean reqBean) {
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
           logger.info("SaCaravanExternalController.editCaravanOaAudit.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = caravanService.editCaravanOaAuditStatus(reqBean);
            logger.info("SaCaravanExternalController.editCaravanOaAudit.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


}
