package com.biostime.samanage.controller.external;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.actreport.ActRePortReqBean;
import com.biostime.samanage.bean.ftf.sign.FtfActSignInfoReqBean;
import com.biostime.samanage.bean.ftf.sign.FtfSignMiniAppReqBean;
import com.biostime.samanage.bean.ftf.sign.FtfSignMiniAppResBean;
import com.biostime.samanage.bean.single.event.SaEventReqBean;
import com.biostime.samanage.check.ftf.FtfActSignCheck;
import com.biostime.samanage.service.act.ActRePortService;
import com.biostime.samanage.service.ftf.FtfActSignService;
import com.biostime.samanage.service.ftf.FtfActivityService;
import com.biostime.samanage.service.single.SaEventDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动签到、上报(外部)
 * Date: 2016-12-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/sa/external")
public class SaFtfActExternalController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FtfActSignService ftfActSignService;

    @Autowired
    private ActRePortService actRePortService;

    @Autowired
    private FtfActivityService ftfActivityService;

    @Autowired
    private SaEventDetailService saEventDetailService;

    /**
     * 签到
     * @return
     */
    @RequestMapping(value = "/saveMobSign", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="签到",description="签到",author="12804",model= ModelType.MKT,updateTime="2016-10-10")
    public @ResponseBody
    BaseResponse saveMobSign(@RequestBody final BaseRequest<FtfActSignInfoReqBean> reqBean) {
        FtfActSignInfoReqBean fsiReqBeans = reqBean.getRequest();
        fsiReqBeans.setSeqNo(reqBean.getSeqNo());
        BaseResponse resp = FtfActSignCheck.saveMobSign(fsiReqBeans);
        if (!resp.isOk()) {
            return resp;
        }
        try{
            fsiReqBeans.setUserId(reqBean.getRequest().getUserId());
            fsiReqBeans.setSourceSystem(reqBean.getSourceSystem());

            // 查询
            logger.info("SaFtfActExternalController.saveMobSign.request{}", JSON.toJSONString(fsiReqBeans));
            ServiceBean result = ftfActSignService.saveMobSign(fsiReqBeans);
            logger.info("SaFtfActExternalController.saveMobSign.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 提交活动上报
     *
     * @return
     */
    @RequestMapping(value = "/submitActReport", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "提交活动上报", description = "提交活动上报", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse submitActRePort(@RequestBody final BaseRequest<ActRePortReqBean> reqBean) {

        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            ActRePortReqBean arReqBeans = reqBean.getRequest();
            arReqBeans.setUserId(reqBean.getRequest().getUserId());
            arReqBeans.setCreatedName(reqBean.getRequest().getCreatedName());
            arReqBeans.setDeviceId(reqBean.getRequest().getDeviceId());
//            arReqBeans.setType("3");
            arReqBeans.setMethodType("3"); //开班培训
            // 查询
            logger.info("SaFtfActExternalController.submitActRePort.request{}", JSON.toJSONString(arReqBeans));
            ServiceBean result = actRePortService.submitActRePort(arReqBeans);
            logger.info("SaFtfActExternalController.submitActRePort.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询SA线下课堂模板详情
     * @return
     */
    @RequestMapping(value = "/querySaFtfActTemplateInfo", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询SA线下课堂模板详情", description = "查询SA线下课堂模板详情", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<Map> querySaFtfActTemplateInfo(String templateId,String actId) {
        BaseResponse<Map> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaFtfActExternalController.FtfActivityTemplateInfoResBean.request{}", JSON.toJSONString(templateId),JSON.toJSONString(templateId));
            ServiceBean<Map> result = ftfActivityService.querySaFtfActTemplateInfo(templateId,actId);
            logger.info("SaFtfActExternalController.FtfActivityTemplateInfoResBean.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询FTF签到活动信息
     * @return
     */
    @RequestMapping(value = "/querySignFtfActInfo", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询FTF签到活动信息", description = "查询FTF签到活动信息", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<FtfSignMiniAppResBean> querySignFtfActInfo(@RequestBody final BaseRequest<FtfSignMiniAppReqBean> reqBean) {
        BaseResponse<FtfSignMiniAppResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaFtfActExternalController.querySignFtfActInfo.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<FtfSignMiniAppResBean> result = ftfActSignService.querySignFtfActInfo(reqBean.getRequest());
            logger.info("SaFtfActExternalController.querySignFtfActInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 小程序扫码签到
     * @return
     */
    @RequestMapping(value = "/saveSignMiniApp", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="小程序扫码签到",description="小程序扫码签到",author="12804",model= ModelType.MKT,updateTime="2019-06-10")
    public @ResponseBody
    BaseResponse saveSignMiniApp(@RequestBody final BaseRequest<FtfSignMiniAppReqBean> reqBean) {
        FtfSignMiniAppReqBean ftfSignMiniAppReqBean = reqBean.getRequest();
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try{

            // 查询
            logger.info("SaFtfActExternalController.saveSignMiniApp.request{}", JSON.toJSONString(ftfSignMiniAppReqBean));
            ServiceBean result = ftfActSignService.saveSignMiniApp(ftfSignMiniAppReqBean);
            logger.info("SaFtfActExternalController.saveSignMiniApp.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 妈妈100微信、营销通app接触签到
     * @return
     */
    @RequestMapping(value = "/saveSaEvent", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="妈妈100微信、营销通app接触签到",description="妈妈100微信、营销通app接触签到",author="12804",model= ModelType.MKT,updateTime="2019-06-10")
    public @ResponseBody
    BaseResponse saveSaEvent(@RequestBody final BaseRequest<SaEventReqBean> reqBean) {
        SaEventReqBean saEventReqBean = reqBean.getRequest();
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try{

            // 查询
            logger.info("SaFtfActExternalController.saveSaEvent.request{}", JSON.toJSONString(saEventReqBean));
            ServiceBean result = saEventDetailService.saveSaEvent(saEventReqBean);
            logger.info("SaFtfActExternalController.saveSaEvent.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
