package com.biostime.samanage.controller.external;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.single.adset.SaAdSetExternalResBean;
import com.biostime.samanage.service.single.SaAdSetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Describe:FTF活动签到、上报(外部)
 * Date: 2016-12-08
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/sa/external/adset")
public class SaAdSetExternalController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private SaAdSetService saAdSetService;

    /**
     * 验证SA服务器是否正常
     * @return
     */
    @RequestMapping(value = "/checkSamanageService", method = {RequestMethod.GET})
    @AutoDocMethod(version = "v-0.0.1", name = "验证SA服务器是否正常", description = "验证SA服务器是否正常", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<SaAdSetExternalResBean> checkSamanageService(String saCode) {
        BaseResponse<SaAdSetExternalResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaAdSetExternalController.querySaAdSetImage.request{}", JSON.toJSONString(saCode));
            ServiceBean<SaAdSetExternalResBean> result = saAdSetService.querySaAdSetImage(saCode);
            logger.info("SaAdSetExternalController.querySaAdSetImage.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询SA广告图片
     * @return
     */
    @RequestMapping(value = "/querySaAdSetImage", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "获取门店对应的渠道", description = "获取门店对应的渠道", author = "12804", model = ModelType.MKT, updateTime = "2016-10-10")
    public
    @ResponseBody
    BaseResponse<SaAdSetExternalResBean> querySaAdSetImage(String saCode) {
        BaseResponse<SaAdSetExternalResBean> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SaAdSetExternalController.querySaAdSetImage.request{}", JSON.toJSONString(saCode));
            ServiceBean<SaAdSetExternalResBean> result = saAdSetService.querySaAdSetImage(saCode);
            logger.info("SaAdSetExternalController.querySaAdSetImage.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

}
