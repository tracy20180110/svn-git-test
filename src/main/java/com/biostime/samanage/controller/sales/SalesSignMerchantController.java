package com.biostime.samanage.controller.sales;


import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.common.util.general.StringUtil;
import com.biostime.samanage.bean.sales.SalesSignMerchantReqBean;
import com.biostime.samanage.service.sales.SalesSignService;
import com.mama100.merchant.base.account.common.bean.mktmp.LoginChannelBean;
import com.mama100.merchant.base.system.common.bean.BgmUserOfficeBean;
import com.mama100.merchant.session.OperatorInfoContext;
import com.mama100.merchant.session.bean.OperatorInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Describe:促销员打卡(手机端)
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Slf4j
@Controller
@RequestMapping("/merchant/salesSign")
public class SalesSignMerchantController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SalesSignService salesSignService;

    /**
     * 查询是否在指定的门店
     *
     * @return
     */
    @RequestMapping(value = "/queryIsInTerminal.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "促销员打卡", description = "促销员打卡", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse queryIsInTerminal(@RequestBody final BaseRequest<SalesSignMerchantReqBean> reqBean) {

        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SalesSignMerchantController.queryIsInTerminal.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = salesSignService.queryIsInTerminal(reqBean.getRequest());
            logger.info("SalesSignMerchantController.queryIsInTerminal.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 促销员打卡(手机端)
     *
     * @return
     */
    @RequestMapping(value = "/saveSalesSign.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "促销员打卡", description = "促销员打卡", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse saveSalesSign(@RequestBody final BaseRequest<SalesSignMerchantReqBean> reqBean) {

        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            SalesSignMerchantReqBean salesSignMerchantReqBean = reqBean.getRequest();

            if(StringUtil.isNullOrEmpty(salesSignMerchantReqBean.getDepartmentCode())){
                OperatorInfo operatorInfo = OperatorInfoContext.get();
                BgmUserOfficeBean userOfficeBean = operatorInfo.getUserOffice();
                if(null != userOfficeBean) {
                    salesSignMerchantReqBean.setDepartmentCode(userOfficeBean.getOfficeCode());
                }
                List<LoginChannelBean> lcBean = operatorInfo.getChannels();
                if(null != lcBean){
                    if(lcBean.size() > 0){
                        salesSignMerchantReqBean.setChannelCode(lcBean.get(0).getChannelCode());
                    }
                }


            }

            // 查询
            logger.info("SalesSignMerchantController.saveSalesSign.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = salesSignService.saveSalesSign(reqBean.getRequest());
            logger.info("SalesSignMerchantController.saveSalesSign.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
