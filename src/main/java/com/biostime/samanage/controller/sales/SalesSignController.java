package com.biostime.samanage.controller.sales;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.Pager;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.sales.SalesSignReqBean;
import com.biostime.samanage.bean.sales.SalesSignResBean;
import com.biostime.samanage.service.sales.SalesSignService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:促销员打卡
 * Date: 2017-10-25
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Controller
@RequestMapping("/sales/sign")
public class SalesSignController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SalesSignService salesSignService;


    /**
     * 促销员打卡列表
     * @param req
     * @return
     */
    @RequestMapping(value = "/querySalesSignList.action", method = { RequestMethod.POST })
    @AutoDocMethod(version="v-0.0.1",name="促销员打卡列表",description="促销员打卡列表",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse<Pager<SalesSignResBean>> querySalesSignList(@RequestBody final BaseRequest<SalesSignReqBean> req) {
        SalesSignReqBean reqBean = req.getRequest();
        BaseResponse<Pager<SalesSignResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            // 查询
            logger.info("SalesSignController.querySalesSignList.request{}", JSON.toJSONString(req));
            Pager<SalesSignResBean> pager = new Pager<SalesSignResBean>(reqBean.getPageSize(),reqBean.getPageNo());
            ServiceBean<Pager<SalesSignResBean>> result = salesSignService.querySalesSignList(pager, reqBean);
            logger.info("SalesSignController.querySalesSignList.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 促销员打卡列表--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSalesSignList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="促销员打卡列表导出",description="促销员打卡列表导出",author="12804",model= ModelType.MKT,updateTime="2017-10-25")
    public @ResponseBody
    BaseResponse exportSalesSignList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SalesSignReqBean reqBean = MapperUtils.reqParameToBean(request, SalesSignReqBean.class);

            // 查询
            logger.info("SalesSignController.exportSalesSignList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = salesSignService.exportSalesSignList(response, reqBean);
            logger.info("SalesSignController.exportSalesSignList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


}
