package com.biostime.samanage.controller.sales;

import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.sales.SchedulingManageReqBean;
import com.biostime.samanage.service.sales.SchedulingService;
import com.biostime.samanage.util.MapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Describe:排班报表
 * Date: 2019-03-21
 * Time: 15:44
 * User: 12804
 * Version:1.0
 * @author 12804
 */
@Controller
@RequestMapping("/scheduling/report")
public class SchedulingController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SchedulingService schedulingservice;

    /**
     * 排班报表导出--导出
     * @param request
     * @return
     */
    @RequestMapping(value = "/exportSchedulingList.action", method = { RequestMethod.GET })
    @AutoDocMethod(version="v-1.2.0",name="排班报表导出",description="排班报表导出",author="12804",model= ModelType.MKT,updateTime="2019-03-25")
    public @ResponseBody
    BaseResponse exportSchedulingList(HttpServletRequest request,HttpServletResponse response) {
        BaseResponse<String> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {
            SchedulingManageReqBean reqBean = MapperUtils.reqParameToBean(request, SchedulingManageReqBean.class);

            // 查询
            logger.info("SchedulingController.exportSchedulingList.request{}", JSON.toJSONString(reqBean));
            ServiceBean result = schedulingservice.exportSchedulingList(response, reqBean);
            logger.info("SchedulingController.exportSchedulingList.response{}", JSON.toJSONString(result));
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
