package com.biostime.samanage.controller.sales;


import com.alibaba.fastjson.JSON;
import com.biostime.autodoc.annotations.AutoDocMethod;
import com.biostime.autodoc.annotations.ModelType;
import com.biostime.common.bean.BaseRequest;
import com.biostime.common.bean.BaseResponse;
import com.biostime.common.bean.ServiceBean;
import com.biostime.common.util.ExceptionUtils;
import com.biostime.common.util.date.DateUtils;
import com.biostime.samanage.bean.BaseKeyValueBean;
import com.biostime.samanage.bean.sales.SchedulingManageReqBean;
import com.biostime.samanage.bean.sales.SchedulingManageResBean;
import com.biostime.samanage.bean.sales.SchedulingReqBean;
import com.biostime.samanage.bean.sales.SchedulingResBean;
import com.biostime.samanage.service.sales.SchedulingService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Describe:排班(手机端)
 * Date: 2019-03-15
 * Time: 15:44
 * User: 12804
 * Version:1.0
 */
@Slf4j
@Controller
@RequestMapping("/merchant/scheduling")
public class SchedulingMerchantController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SchedulingService schedulingservice;

    /**
     * 查询排班详情
     *
     * @return
     */
    @RequestMapping(value = "/querySchedulingInfo.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询排班详情", description = "查询排班详情", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse<List<SchedulingResBean>> querySchedulingInfo(@RequestBody final BaseRequest<SchedulingReqBean> reqBean) {

        BaseResponse<List<SchedulingResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("SchedulingMerchantController.querySchedulingInfo.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<SchedulingResBean>> result = schedulingservice.querySchedulingInfo(reqBean.getRequest());
            logger.info("SchedulingMerchantController.querySchedulingInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }


    /**
     * 保存排班
     *
     * @return
     */
    @RequestMapping(value = "/saveScheduling.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "保存排班", description = "保存排班", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse saveScheduling(@RequestBody final BaseRequest<SchedulingReqBean> reqBean) {

        SchedulingReqBean schedulingReq = reqBean.getRequest();
        BaseResponse resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("SchedulingMerchantController.saveScheduling.request{}", JSON.toJSONString(schedulingReq));
            ServiceBean result = schedulingservice.saveScheduling(schedulingReq);
            logger.info("SchedulingMerchantController.saveScheduling.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询排班管理详情
     *
     * @return
     */
    @RequestMapping(value = "/querySchedulingManageInfo.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询排班管理详情", description = "查询排班管理详情", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse<List<SchedulingManageResBean>> querySchedulingManageInfo(@RequestBody final BaseRequest<SchedulingManageReqBean> reqBean) {

        BaseResponse<List<SchedulingManageResBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("SchedulingMerchantController.querySchedulingManageInfo.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<SchedulingManageResBean>> result = schedulingservice.querySchedulingManageInfo(reqBean.getRequest());
            logger.info("SchedulingMerchantController.querySchedulingManageInfo.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }

    /**
     * 查询人员与终端
     *
     * @return
     */
    @RequestMapping(value = "/queryAccountTerminal.do", method = {RequestMethod.POST})
    @AutoDocMethod(version = "v-0.0.1", name = "查询人员与终端", description = "查询人员与终端", author = "12804", model = ModelType.MKT, updateTime = "2016-10-26")
    public
    @ResponseBody
    BaseResponse<List<BaseKeyValueBean>> queryAccountTerminal(@RequestBody final BaseRequest<SchedulingReqBean> reqBean) {

        BaseResponse<List<BaseKeyValueBean>> resp = new BaseResponse(DateUtils.makeDateSeqNo());
        if (!resp.isOk()) {
            return resp;
        }
        try {

            // 查询
            logger.info("SchedulingMerchantController.queryAccountTerminal.request{}", JSON.toJSONString(reqBean.getRequest()));
            ServiceBean<List<BaseKeyValueBean>> result = schedulingservice.queryAccountTerminal(reqBean.getRequest());
            logger.info("SchedulingMerchantController.queryAccountTerminal.response{}", JSON.toJSONString(result));
            resp.setResponse(result.getResponse());
            resp.setCode(result.getCode());
            resp.setDesc(result.getDesc());
            ExceptionUtils.setError(resp);
        } catch (Exception ex) {
            ExceptionUtils.setSystemError(resp, ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return resp;
    }
}
